package com.oxygen.user.dao;

import java.util.List;

import com.oxygen.user.dto.UserRelateOrgDto;
import com.oxygen.user.model.UserRelateOrg;

/**
 * 用户组织的DAO
 * @author jason
 */
public interface UserRelateOrgMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserRelateOrg record);

    UserRelateOrg selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(UserRelateOrg record);
    
    /**
     * 根据用户ID删除用户的组织
     * @param userId
     * @return
     */
    int deleteByUserId(String userId);
    
    /**
     * 批量新增用户组织信息
     * @param userRelateOrgList
     * @return
     */
    int batchinsert(List<UserRelateOrg> userRelateOrgList);
    
    /**
     * 根据用户ID得到用户组织集合
     * @return
     */
    List<UserRelateOrgDto> getUserRelateOrgListByUserId(String userId);
}