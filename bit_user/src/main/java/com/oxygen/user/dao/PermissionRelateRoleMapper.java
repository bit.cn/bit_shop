package com.oxygen.user.dao;

import java.util.List;
import java.util.Map;

import com.oxygen.user.dto.PermissionRelateRoleDto;
import com.oxygen.user.model.PermissionRelateRole;

public interface PermissionRelateRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PermissionRelateRole record);

    PermissionRelateRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(PermissionRelateRole record);
    
    /**
	 * 根据角色ID删除菜单列表
	 * @param roleId
	 * @return
	 */
	int deleteByRoleId(int roleId);
	
	/**
	 * 根据角色ID菜单ID删除功能列表
	 * @param apiReq
	 * @return
	 */
	int deleteByRoleIdMenuId(Map<String, Object> apiReq);
	
	/**
     * 批量新增角色菜单关系信息表
     * @param permissionRelateRoleList
     * @return
     */
    int batchinsert(List<PermissionRelateRole> permissionRelateRoleList);
    
    /**
     * 根绝角色ID得到集合
     * @param roleId
     * @return
     */
    List<PermissionRelateRoleDto> getListByRoleId(int roleId);
    
    /**
     * 根据 （角色ID，菜单ID）得到功能列表
     * @param apiReq
     * @return
     */
    List<PermissionRelateRoleDto> getList(Map<String, Object> apiReq);
    
    
}