package com.oxygen.user.dao;

import java.util.List;
import java.util.Map;

import com.oxygen.user.dto.RolesDto;
import com.oxygen.user.model.Roles;

public interface RolesMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(Roles record);

	Roles selectByPrimaryKey(Integer id);

	int updateByPrimaryKeyWithBLOBs(Roles record);

	int updateByPrimaryKey(Roles record);

	/**** 查询角色列表 *************/
	List<RolesDto> queryRolesList(Map<String, Object> apiReq);
	
	
	RolesDto queryRolesById(Map<String, Object> apiReq);
	
	int queryRolesListCount(Map<String, Object> apiReq);
}