package com.oxygen.user.dao;

import java.util.List;
import java.util.Map;

import com.oxygen.dto.UserSimpleDto;
import com.oxygen.user.dto.UserDto;
import com.oxygen.user.model.Users;

public interface UsersMapper {
	int deleteByPrimaryKey(String id);

	int insert(Users record);

	Users selectByPrimaryKey(String id);

	int updateByPrimaryKey(Users record);

	/**
	 * 根据用户名得到用户信息
	 * 
	 * @param username
	 * @return
	 */
	UserDto getUserByName(Map<String, Object> apiReq);

	/**
	 * 根据手机号查询用户信息
	 * 
	 * @param apiReq
	 * @return
	 */
	Users getUserByPhone(Map<String, Object> apiReq);

	/**
	 * 
	 * @Title: queryUserList
	 * @author zhoujun 2016-7-7 下午2:25:20
	 * @Description: 查询用户列表<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-7-7 下午2:25:20
	 * </pre>
	 * @param @param apiReq
	 * @param @return
	 * @return List<Users>
	 */
	List<UserDto> queryUserList(Map<String, Object> apiReq);

	UserDto queryUserById(Map<String, Object> apiReq);

	int queryUserListCount(Map<String, Object> apiReq);

	UserSimpleDto getUserSimpleById(String userId);
	
	List<UserDto> getUserByIds (Map<String, Object> apiReq);
	
	int getUserByIdsCount(Map<String, Object> apiReq);
}