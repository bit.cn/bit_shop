package com.oxygen.user.dao;

import java.util.List;

import com.oxygen.user.dto.UserRelateRoleDto;
import com.oxygen.user.model.UserRelateRole;

/**
 * 用户与角色关联信息
 * @author jason
 */
public interface UserRelateRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserRelateRole record);

    UserRelateRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(UserRelateRole record);
    
    /**
     * 根据用户ID删除用户的所有角色
     * @param userId
     * @return
     */
    int deleteByUserId(String userId);
    
    /**
     * 批量新增用户角色信息
     * @param userRelateRoleList
     * @return
     */
    int batchinsert(List<UserRelateRole> userRelateRoleList);
    
    /**
     * 根据用户ID得到用户角色集合
     * @return
     */
    List<UserRelateRoleDto> getUserRelateRoleListByUserId(String userId);
}