package com.oxygen.user.dao;

import java.util.List;

import com.oxygen.user.dto.FunctionRelateMenuDto;
import com.oxygen.user.model.FunctionRelateMenu;

/**
 * 功能菜单关联信息DAO
 * @author jason
 */
public interface FunctionRelateMenuMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(FunctionRelateMenu record);
    
    /**
     * 批量新增功能菜单关系信息表
     * @param functionRelateMenuList
     * @return
     */
    int batchinsert(List<FunctionRelateMenu> functionRelateMenuList);

    FunctionRelateMenu selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(FunctionRelateMenu record);
    
    /**
     * 根据菜单ID得到功能菜单关联集合
     * @return
     */
    List<FunctionRelateMenuDto> getFunctionRelateMenuListByMenuId(Integer menuId);
    
    /**
	 * 根据菜单ID批量删除功能
	 * @param menuId
	 * @return
	 */
	int deleteByMenuId(Integer menuId);
}