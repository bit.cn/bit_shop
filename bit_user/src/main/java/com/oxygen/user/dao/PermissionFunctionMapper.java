package com.oxygen.user.dao;

import java.util.List;
import java.util.Map;

import com.oxygen.user.dto.PermissionFunctionDto;
import com.oxygen.user.model.PermissionFunction;

public interface PermissionFunctionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PermissionFunction record);

    PermissionFunction selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(PermissionFunction record);
    
    /**
     * 
    * @Title: queryFunction
    * @author zhoujun 2016-7-7 上午10:18:56
    * @Description: 查询权限列表<br>
    *          <h1>example:</h1> <br>
    *          请在这里添加使用例子
    * <pre>
    * Modify Reason:(修改原因,不需覆盖，直接追加.)
    *     zhoujun  2016-7-7 上午10:18:56
    * </pre>
    * @param @return
    * @return List<PermissionFunctionDto>
     */
    List<PermissionFunctionDto> queryFunction(Map<String, Object> apiReq);
    
    
    
    PermissionFunctionDto queryFunctionById(Map<String, Object> apiReq);
    
    
	int queryFunctionCount(Map<String, Object> apiReq);

    
}