package com.oxygen.user.dao;

import com.oxygen.user.model.OrgRelateRole;

public interface OrgRelateRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OrgRelateRole record);

    OrgRelateRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(OrgRelateRole record);
}