package com.oxygen.user.dao;

import java.util.List;

import com.oxygen.user.dto.PermissionMenusDto;
import com.oxygen.user.model.PermissionMenus;

public interface PermissionMenusMapper {
	int deleteByPrimaryKey(Integer id);

	long insert(PermissionMenus record);

	PermissionMenusDto selectByPrimaryKey(Integer id);

	int updateByPrimaryKey(PermissionMenus record);

	List<PermissionMenusDto> queryMenusList();
	
	/**
	 * 根据父节点ID得到菜单列表
	 * @param parentId
	 * @return
	 */
	List<PermissionMenusDto> getMenuByParenId(String parentId);
	
}