package com.oxygen.user.dao;

import java.util.List;

import com.oxygen.user.dto.OrganizationalDto;
import com.oxygen.user.model.Organizational;

public interface OrganizationalMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(Organizational record);

	/**
	 * 根据组织ID得到组织的详情
	 * @param id
	 * @return
	 */
	OrganizationalDto selectByPrimaryKey(Integer id);

	int updateByPrimaryKeyWithBLOBs(Organizational record);

	int updateByPrimaryKey(Organizational record);

	/*** 查询组织列表 ****/
	List<OrganizationalDto> queryOrganizationalList();

}