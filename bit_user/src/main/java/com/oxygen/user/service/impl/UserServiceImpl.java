package com.oxygen.user.service.impl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.oxygen.annotations.ApiService;
import com.oxygen.user.service.UserService;

@Service
@ApiService(descript = "用户账号")
public class UserServiceImpl extends BaseServiceImpl implements UserService {
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
}