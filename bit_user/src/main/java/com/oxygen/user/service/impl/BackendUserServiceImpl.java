package com.oxygen.user.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.oxygen.annotations.ApiMethod;
import com.oxygen.annotations.ApiParam;
import com.oxygen.annotations.ApiService;
import com.oxygen.dto.ApiRequest;
import com.oxygen.dto.ApiResponse;
import com.oxygen.dto.UserSimpleDto;
import com.oxygen.enums.ApiMsgEnum;
import com.oxygen.user.dao.FunctionRelateMenuMapper;
import com.oxygen.user.dao.OrgRelateRoleMapper;
import com.oxygen.user.dao.OrganizationalMapper;
import com.oxygen.user.dao.PermissionFunctionMapper;
import com.oxygen.user.dao.PermissionMenusMapper;
import com.oxygen.user.dao.PermissionRelateRoleMapper;
import com.oxygen.user.dao.RolesMapper;
import com.oxygen.user.dao.UserRelateOrgMapper;
import com.oxygen.user.dao.UserRelateRoleMapper;
import com.oxygen.user.dao.UsersMapper;
import com.oxygen.user.dto.FunctionRelateMenuDto;
import com.oxygen.user.dto.OrganizationalDto;
import com.oxygen.user.dto.PermissionFunctionDto;
import com.oxygen.user.dto.PermissionMenusDto;
import com.oxygen.user.dto.PermissionRelateRoleDto;
import com.oxygen.user.dto.RolesDto;
import com.oxygen.user.dto.UserDto;
import com.oxygen.user.dto.UserRelateOrgDto;
import com.oxygen.user.dto.UserRelateRoleDto;
import com.oxygen.user.enums.UserStatusEnum;
import com.oxygen.user.enums.UserTypeEnum;
import com.oxygen.user.model.FunctionRelateMenu;
import com.oxygen.user.model.OrgRelateRole;
import com.oxygen.user.model.Organizational;
import com.oxygen.user.model.PermissionFunction;
import com.oxygen.user.model.PermissionMenus;
import com.oxygen.user.model.PermissionRelateRole;
import com.oxygen.user.model.Roles;
import com.oxygen.user.model.UserRelateOrg;
import com.oxygen.user.model.UserRelateRole;
import com.oxygen.user.model.Users;
import com.oxygen.user.service.BackendUserService;
import com.oxygen.user.service.UserService;
import com.oxygen.util.Md5Util;
import com.oxygen.util.RandomIDUtil;

@Service
@ApiService(descript = "后台用户账号")
public class BackendUserServiceImpl extends BaseServiceImpl implements BackendUserService {
	private static final Logger logger = Logger.getLogger(BackendUserServiceImpl.class);
	// private static Object lock = new Object();

	@Autowired
	private UsersMapper usersMapper;

	@Autowired
	private OrganizationalMapper organizationalMapper;

	@Autowired
	private RolesMapper rolesMapper;

	@Autowired
	private PermissionFunctionMapper permissionFunctionMapper;

	@Autowired
	private OrgRelateRoleMapper orgRelateRoleMapper;

	@Autowired
	private FunctionRelateMenuMapper functionRelateMenuMapper;

	@Autowired
	private PermissionRelateRoleMapper permissionRelateRoleMapper;

	@Autowired
	private UserRelateOrgMapper userRelateOrgMapper;

	@Autowired
	private UserRelateRoleMapper userRelateRoleMapper;

	@Autowired
	private PermissionMenusMapper permissionMenusMapper;

	@Resource
	private PlatformTransactionManager platformTransactionManager;

	@Autowired
	private UserService userService;

	/**
	 * 后台管理系统登录
	 */
	@ApiMethod(value = "user.login", descript = "用户登录", apiParams = { @ApiParam(name = "username", isNull = false, descript = "用户名"), @ApiParam(name = "password", isNull = false, descript = "密码") })
	@Override
	public String login(ApiRequest apiReq) {
		try {
			if (this.checkNotEmptyParameter(apiReq, new String[] { "username", "password" })) {
				return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.MissParameterException));
			}
			String username = apiReq.getString("username");
			String userType = UserTypeEnum.backend.name();
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("username", username);
			paramMap.put("userType", userType);
			UserDto user = usersMapper.getUserByName(paramMap);
			if (null == user) {
				return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.UserNotExistedException));
			}
			String password = apiReq.getString("password");
			if (Md5Util.encodeString(password).equals(user.getPassword())) {
				// 根据用户信息查询用户的扩展信息
				List<UserRelateRoleDto> roleList = userRelateRoleMapper.getUserRelateRoleListByUserId(user.getId()); // 根据用户ID得到用户的角色列表
				user.setRoleList(roleList);
				List<PermissionRelateRoleDto> permissionRelateRoleList = new ArrayList<PermissionRelateRoleDto>();
				for (UserRelateRoleDto userRelateRoleDto : roleList) {
					permissionRelateRoleList.addAll(permissionRelateRoleMapper.getListByRoleId(userRelateRoleDto.getRoleId()));// 根据角色ID得到权限列表
				}

				List<PermissionRelateRoleDto> menuList = new ArrayList<PermissionRelateRoleDto>();
				List<PermissionRelateRoleDto> funList = new ArrayList<PermissionRelateRoleDto>();
				Map<String, Object> apiReq1 = new HashMap<String, Object>();
				for (PermissionRelateRoleDto permissionRelateRole : permissionRelateRoleList) {
					if (permissionRelateRole.getFunId() == 0) {
						menuList.add(permissionRelateRole);
					} else {
						apiReq1.put("id", permissionRelateRole.getFunId());
						PermissionFunctionDto permissionFunctionDto = permissionFunctionMapper.queryFunctionById(apiReq1);
						if (permissionFunctionDto != null) {
							permissionRelateRole.setFunName(permissionFunctionDto.getName());
						}
						funList.add(permissionRelateRole);
					}
				}
				for (PermissionRelateRoleDto permissionRelateRoleDto : menuList) {
					List<FunctionRelateMenuDto> functionRelateMenuList = functionRelateMenuMapper.getFunctionRelateMenuListByMenuId(permissionRelateRoleDto.getMenuId());
					permissionRelateRoleDto.setFunctionRelateMenuList(functionRelateMenuList);
				}
				user.setMenuList(menuList);
				user.setFunList(funList);

				return this.buildResponse(apiReq, new ApiResponse<UserDto>(ApiMsgEnum.SUCCESS, 1, user));
			} else {
				return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.UserPasswordException));
			}
		} catch (Exception e) {
			logger.error("login", e);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
		}
	}

	@ApiMethod(value = "user.register", descript = "用户注册", apiParams = { @ApiParam(name = "username", isNull = false, descript = "用户名"), @ApiParam(name = "password", isNull = true, descript = "密码"),
			@ApiParam(name = "realName", isNull = false, descript = "真实姓名"), @ApiParam(name = "nickName", isNull = false, descript = "昵称"),
			@ApiParam(name = "email", isNull = false, descript = "电子邮件"), @ApiParam(name = "mobilePhone", isNull = false, descript = "电话号码"),
			@ApiParam(name = "registSource", isNull = false, descript = "注册来源"), @ApiParam(name = "isSuperuser", isNull = false, descript = "是否是超级管理员"),
			@ApiParam(name = "userType", isNull = false, descript = "用户类型"), @ApiParam(name = "userTypeName", isNull = true, descript = "用户类型名称"),
			@ApiParam(name = "roleIds", isNull = true, descript = "角色"), @ApiParam(name = "orgIds", isNull = true, descript = "组织") })
	@Override
	public String register(ApiRequest apiReq) {
		Users user = (Users) apiReq.getBean(Users.class);
		String id = RandomIDUtil.getNewUUID();
		user.setId(id);
		user.setStatus(UserStatusEnum.activity.name()); // 激活状态
		user.setRegistTime(new Date());
		if (StringUtils.isBlank(user.getPassword())) {
			user.setPassword(Md5Util.encodeString("123456"));// 用户注册默认密码123456
		}
		// 保存用户信息
		int flag = usersMapper.insert(user);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.operationOrganizational", descript = "新增组织架构", apiParams = { @ApiParam(name = "name", isNull = false, descript = "组织名"),
			@ApiParam(name = "code", isNull = false, descript = "组织码"), @ApiParam(name = "parentId", isNull = false, descript = "父节点Id"), @ApiParam(name = "description", descript = "描述") })
	@Override
	public String operationOrganizational(ApiRequest apiReq) {
		// 新增组织架构
		TransactionStatus transactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
		try {
			Organizational organizational = (Organizational) apiReq.getBean(Organizational.class);
			organizational.setCreateTime(new Date());
			organizational.setUpdateTime(new Date());
			organizationalMapper.insert(organizational);
			platformTransactionManager.commit(transactionStatus);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		} catch (Exception e) {
			platformTransactionManager.rollback(transactionStatus);
			logger.error("operationMenu:", e);
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.operationRoles", descript = "新增角色", apiParams = { @ApiParam(name = "name", isNull = false, descript = "角色名称"), @ApiParam(name = "code", isNull = false, descript = "角色编码"),
			@ApiParam(name = "description", descript = "描述"), @ApiParam(name = "menuIds", descript = "菜单ID集合"), @ApiParam(name = "funIds", descript = "功能ID集合") })
	@Override
	public String operationRoles(ApiRequest apiReq) {
		TransactionStatus transactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
		try {
			Roles roles = (Roles) apiReq.getBean(Roles.class);
			// 新增角色
			rolesMapper.insert(roles);
			// 新增角色的相关菜单，功能信息
			Integer[] menuIds = apiReq.getIntArray("menuIds");
			Integer[] funIds = apiReq.getIntArray("funIds");
			List<PermissionRelateRole> permissionRelateRoleList = new ArrayList<PermissionRelateRole>();
			for (Integer menuId : menuIds) {
				PermissionRelateRole permissionRelateRole = new PermissionRelateRole();
				permissionRelateRole.setMenuId(menuId);
				permissionRelateRole.setRoleId(roles.getId());
				permissionRelateRole.setFunId(0);
				permissionRelateRoleList.add(permissionRelateRole);
			}
			for (Integer funId : funIds) {
				PermissionRelateRole permissionRelateRole = new PermissionRelateRole();
				permissionRelateRole.setFunId(funId);
				permissionRelateRole.setMenuId(0);
				permissionRelateRole.setRoleId(roles.getId());
				permissionRelateRoleList.add(permissionRelateRole);
			}
			// 批量删除角色的菜单和功能
			permissionRelateRoleMapper.deleteByRoleId(roles.getId());
			// 批量添加角色的菜单
			permissionRelateRoleMapper.batchinsert(permissionRelateRoleList);
			platformTransactionManager.commit(transactionStatus);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		} catch (Exception e) {
			platformTransactionManager.rollback(transactionStatus);
			logger.error("operationMenu:", e);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
		}
	}

	@ApiMethod(value = "user.operationPermissionFunction", descript = "新增权限", apiParams = { @ApiParam(name = "name", isNull = false, descript = "权限名称"),
			@ApiParam(name = "code", isNull = false, descript = "权限编号"), @ApiParam(name = "onclick", isNull = false, descript = "操作事件") })
	@Override
	public String operationPermissionFunction(ApiRequest apiReq) {
		PermissionFunction permissionFunction = (PermissionFunction) apiReq.getBean(PermissionFunction.class);
		// 新增权限
		int flag = permissionFunctionMapper.insert(permissionFunction);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.operationMenu", descript = "新增菜单", apiParams = { @ApiParam(name = "name", isNull = false, descript = "菜单名称"), @ApiParam(name = "code", isNull = false, descript = "菜单编码"),
			@ApiParam(name = "parentId", isNull = false, descript = "菜单父节点Id"), @ApiParam(name = "description", descript = "描述"), @ApiParam(name = "funIds", isNull = false, descript = "功能Id数组集合") })
	@Override
	public String operationMenu(ApiRequest apiReq) {
		TransactionStatus transactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
		try {
			PermissionMenus permissionMenus = (PermissionMenus) apiReq.getBean(PermissionMenus.class);
			permissionMenus.setCreateTime(new Date());
			permissionMenus.setUpdateTime(new Date());
			// 新增菜单
			permissionMenusMapper.insert(permissionMenus);
			// 批量新增菜单的功能信息
			Integer[] funIds = apiReq.getIntArray("funIds");
			List<FunctionRelateMenu> functionRelateMenuList = new ArrayList<FunctionRelateMenu>();
			for (Integer funId : funIds) {
				FunctionRelateMenu functionRelateMenu = (FunctionRelateMenu) apiReq.getBean(FunctionRelateMenu.class);
				functionRelateMenu.setFunId(funId);
				functionRelateMenu.setMenuId(permissionMenus.getId().intValue());
				functionRelateMenuList.add(functionRelateMenu);
			}
			functionRelateMenuMapper.batchinsert(functionRelateMenuList);
			platformTransactionManager.commit(transactionStatus);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		} catch (Exception e) {
			platformTransactionManager.rollback(transactionStatus);
			logger.error("operationMenu:", e);
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.operationUserRelateRole", descript = "新增用户角色关系", apiParams = { @ApiParam(name = "roleId", isNull = false, descript = "角色ID"),
			@ApiParam(name = "userId", isNull = false, descript = "用户ID") })
	@Override
	public String operationUserRelateRole(ApiRequest apiReq) {
		UserRelateRole userRelateRole = (UserRelateRole) apiReq.getBean(UserRelateRole.class);
		// 新建用户角色关系
		int flag = userRelateRoleMapper.insert(userRelateRole);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.operationUserRelateOrg", descript = "新增用户组织关系", apiParams = { @ApiParam(name = "orgId", isNull = false, descript = "组织ID"),
			@ApiParam(name = "userId", isNull = false, descript = "用户ID") })
	@Override
	public String operationUserRelateOrg(ApiRequest apiReq) {
		UserRelateOrg userRelateOrg = (UserRelateOrg) apiReq.getBean(UserRelateOrg.class);
		// 新建用户组织关系
		int flag = userRelateOrgMapper.insert(userRelateOrg);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.operationPermissionRelateRole", descript = "新增角色功能权限关系", apiParams = { @ApiParam(name = "roleId", isNull = false, descript = "角色ID"),
			@ApiParam(name = "funId", isNull = false, descript = "功能Id"), @ApiParam(name = "menuId", isNull = false, descript = "菜单Id") })
	@Override
	public String operationPermissionRelateRole(ApiRequest apiReq) {
		PermissionRelateRole permissionRelateRole = (PermissionRelateRole) apiReq.getBean(PermissionRelateRole.class);
		// 新建角色功能权限关系
		int flag = permissionRelateRoleMapper.insert(permissionRelateRole);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.operationOrgRelateRole", descript = "新增组织角色关系", apiParams = { @ApiParam(name = "roleId", isNull = false, descript = "角色ID"),
			@ApiParam(name = "orgId", isNull = false, descript = "组织Id") })
	@Override
	public String operationOrgRelateRole(ApiRequest apiReq) {
		OrgRelateRole orgRelateRole = (OrgRelateRole) apiReq.getBean(OrgRelateRole.class);
		// 新建用户角色关系
		int flag = orgRelateRoleMapper.insert(orgRelateRole);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.operationFunctionRelateMenu", descript = "新增功能菜单关系", apiParams = { @ApiParam(name = "menuId", isNull = false, descript = "菜单ID"),
			@ApiParam(name = "funId", isNull = false, descript = "功能Id"), @ApiParam(name = "funIds", isNull = false, descript = "功能Id数组集合") })
	@Override
	public String operationFunctionRelateMenu(ApiRequest apiReq) {
		FunctionRelateMenu functionRelateMenu = (FunctionRelateMenu) apiReq.getBean(FunctionRelateMenu.class);
		// 新增功能菜单关系
		int flag = functionRelateMenuMapper.insert(functionRelateMenu);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<String>(ApiMsgEnum.SUCCESS), flag);
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.batchAddFunctionRelateMenu", descript = "批量新增功能菜单关系", apiParams = { @ApiParam(name = "menuId", isNull = false, descript = "菜单ID"),
			@ApiParam(name = "funIds", isNull = false, descript = "功能Id数组集合") })
	@Override
	public String batchAddFunctionRelateMenu(ApiRequest apiReq) {
		try {
			Integer[] funIds = apiReq.getIntArray("funIds");
			List<FunctionRelateMenu> functionRelateMenuList = new ArrayList<FunctionRelateMenu>();
			for (Integer funId : funIds) {
				FunctionRelateMenu functionRelateMenu = (FunctionRelateMenu) apiReq.getBean(FunctionRelateMenu.class);
				functionRelateMenu.setFunId(funId);
				functionRelateMenuList.add(functionRelateMenu);
			}
			functionRelateMenuMapper.batchinsert(functionRelateMenuList);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		} catch (Exception e) {
			logger.error("batchAddFunctionRelateMenu:" + e);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
		}
	}

	/*** 查询操作 *******************************************************************************/

	@ApiMethod(value = "user.queryUserList", descript = "查询用户列表", apiParams = { @ApiParam(name = "username", isNull = true, descript = "用户名"),
			@ApiParam(name = "realName", isNull = true, descript = "真实姓名"), @ApiParam(name = "nickName", isNull = true, descript = "昵称"), @ApiParam(name = "email", isNull = true, descript = "电子邮件"),
			@ApiParam(name = "userType", isNull = true, descript = "用户类型"), @ApiParam(name = "mobilePhone", isNull = true, descript = "电话号码"), @ApiParam(descript = "页码", name = "page"),
			@ApiParam(descript = "每页多少条", name = "page_size") })
	@Override
	public String queryUserList(ApiRequest apiReq) {
		this.setDefaultPageSize(apiReq);
		List<UserDto> list = usersMapper.queryUserList(apiReq);
		int count = 0;
		if (null != list) {
			count = usersMapper.queryUserListCount(apiReq);
		}
		return this.buildResponse(apiReq, new ApiResponse<List<UserDto>>(ApiMsgEnum.SUCCESS, count, list));
	}

	@ApiMethod(value = "user.getOrganizational", descript = "查询组织列表")
	@Override
	public String getOrganizational(ApiRequest apiReq) {
		List<OrganizationalDto> list = organizationalMapper.queryOrganizationalList();
		int count = 0;
		if (null != list) {
			count = list.size();
		}
		return this.buildResponse(apiReq, new ApiResponse<List<OrganizationalDto>>(ApiMsgEnum.SUCCESS, count, list));
	}

	@ApiMethod(value = "user.getOrgById", descript = "根据组织ID得到组织的详情", apiParams = { @ApiParam(name = "id", isNull = false, descript = "菜单ID") })
	@Override
	public String getOrgById(ApiRequest apiReq) {
		try {
			int id = apiReq.getInt("id");
			OrganizationalDto OrganizationalDto = organizationalMapper.selectByPrimaryKey(id);
			return this.buildResponse(apiReq, new ApiResponse<OrganizationalDto>(ApiMsgEnum.SUCCESS, 1, OrganizationalDto));
		} catch (Exception e) {
			logger.error("getOrgById:" + e);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
		}

	}

	@ApiMethod(value = "user.getRoles", descript = "查询角色列表")
	@Override
	public String getRoles(ApiRequest apiReq) {

		this.setDefaultPageSize(apiReq);
		List<RolesDto> list = rolesMapper.queryRolesList(apiReq);
		int count = 0;
		if (null != list) {
			count = rolesMapper.queryRolesListCount(apiReq);
		}
		return this.buildResponse(apiReq, new ApiResponse<List<RolesDto>>(ApiMsgEnum.SUCCESS, count, list));

	}

	@ApiMethod(value = "user.getMenu", descript = "查询菜单列表")
	@Override
	public String getMenu(ApiRequest apiReq) {
		List<PermissionMenusDto> list = permissionMenusMapper.queryMenusList();
		for (PermissionMenusDto permissionMenusDto : list) {
			List<FunctionRelateMenuDto> functionRelateMenuDtoList = functionRelateMenuMapper.getFunctionRelateMenuListByMenuId(permissionMenusDto.getId());
			permissionMenusDto.setFunctionRelateMenuDtoList(functionRelateMenuDtoList);
		}
		int count = 0;
		if (null != list) {
			count = list.size();
		}
		return this.buildResponse(apiReq, new ApiResponse<List<PermissionMenusDto>>(ApiMsgEnum.SUCCESS, count, list));

	}

	@ApiMethod(value = "user.getFunction", descript = "查询权限列表")
	@Override
	public String getFunction(ApiRequest apiReq) {
		this.setDefaultPageSize(apiReq);
		List<PermissionFunctionDto> list = permissionFunctionMapper.queryFunction(apiReq);
		int count = 0;
		if (null != list) {
			count = permissionFunctionMapper.queryFunctionCount(apiReq);
		}
		return this.buildResponse(apiReq, new ApiResponse<List<PermissionFunctionDto>>(ApiMsgEnum.SUCCESS, count, list));

	}

	/***** 修改操作 ************************************************************************/

	@ApiMethod(value = "user.updateOrganizational", descript = "修改组织架构", apiParams = { @ApiParam(name = "id", isNull = false, descript = "组织Id"),
			@ApiParam(name = "name", isNull = false, descript = "组织名"), @ApiParam(name = "code", isNull = false, descript = "组织码"), @ApiParam(name = "parentId", isNull = false, descript = "父节点Id"),
			@ApiParam(name = "description", descript = "描述") })
	@Override
	public String updateOrganizational(ApiRequest apiReq) {
		Organizational organizational = (Organizational) apiReq.getBean(Organizational.class);
		// 修改组织架构
		int flag = organizationalMapper.updateByPrimaryKey(organizational);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.updateRoles", descript = "修改角色", apiParams = { @ApiParam(name = "id", isNull = false, descript = "角色Id"), @ApiParam(name = "name", isNull = false, descript = "角色名称"),
			@ApiParam(name = "code", isNull = false, descript = "角色编码"), @ApiParam(name = "description", descript = "描述"), @ApiParam(name = "menuIds", descript = "菜单ID集合"),
			@ApiParam(name = "funIds", descript = "功能ID集合") })
	@Override
	public String updateRoles(ApiRequest apiReq) {
		TransactionStatus transactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
		try {
			Roles roles = (Roles) apiReq.getBean(Roles.class);
			// 修改角色
			rolesMapper.updateByPrimaryKey(roles);
			// 新增角色的相关菜单，功能信息
			Integer[] menuIds = apiReq.getIntArray("menuIds");
			Integer[] funIds = apiReq.getIntArray("funIds");
			List<PermissionRelateRole> permissionRelateRoleList = new ArrayList<PermissionRelateRole>();
			for (Integer menuId : menuIds) {
				PermissionRelateRole permissionRelateRole = new PermissionRelateRole();
				permissionRelateRole.setMenuId(menuId);
				permissionRelateRole.setRoleId(roles.getId());
				permissionRelateRole.setFunId(0);
				permissionRelateRoleList.add(permissionRelateRole);
			}
			for (Integer funId : funIds) {
				PermissionRelateRole permissionRelateRole = new PermissionRelateRole();
				permissionRelateRole.setFunId(funId);
				permissionRelateRole.setMenuId(0);
				permissionRelateRole.setRoleId(roles.getId());
				permissionRelateRoleList.add(permissionRelateRole);
			}
			// 批量删除角色的菜单和功能
			permissionRelateRoleMapper.deleteByRoleId(roles.getId());
			// 批量添加角色的菜单
			permissionRelateRoleMapper.batchinsert(permissionRelateRoleList);
			platformTransactionManager.commit(transactionStatus);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		} catch (Exception e) {
			platformTransactionManager.rollback(transactionStatus);
			logger.error("operationMenu:", e);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
		}
	}

	@ApiMethod(value = "user.updatePermissionFunction", descript = "修改权限", apiParams = { @ApiParam(name = "id", isNull = false, descript = "权限Id"),
			@ApiParam(name = "name", isNull = false, descript = "权限名称"), @ApiParam(name = "code", isNull = false, descript = "权限编号"), @ApiParam(name = "onclick", isNull = false, descript = "操作事件") })
	@Override
	public String updatePermissionFunction(ApiRequest apiReq) {

		PermissionFunction permissionFunction = (PermissionFunction) apiReq.getBean(PermissionFunction.class);
		// 修改权限
		int flag = permissionFunctionMapper.updateByPrimaryKey(permissionFunction);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.updateMenu", descript = "修改菜单", apiParams = { @ApiParam(name = "id", isNull = false, descript = "菜单Id"), @ApiParam(name = "name", isNull = false, descript = "菜单名称"),
			@ApiParam(name = "code", isNull = false, descript = "菜单编码"), @ApiParam(name = "parentId", isNull = false, descript = "菜单父节点Id"), @ApiParam(name = "description", descript = "描述") })
	@Override
	public String updateMenu(ApiRequest apiReq) {
		TransactionStatus transactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
		try {
			PermissionMenus permissionMenus = (PermissionMenus) apiReq.getBean(PermissionMenus.class);
			permissionMenus.setUpdateTime(new Date());
			// 新增菜单
			permissionMenusMapper.updateByPrimaryKey(permissionMenus);

			// 批量删除菜单的功能信息
			functionRelateMenuMapper.deleteByMenuId(new Long(permissionMenus.getId()).intValue());

			// 批量新增菜单的功能信息
			Integer[] funIds = apiReq.getIntArray("funIds");
			List<FunctionRelateMenu> functionRelateMenuList = new ArrayList<FunctionRelateMenu>();
			for (Integer funId : funIds) {
				FunctionRelateMenu functionRelateMenu = (FunctionRelateMenu) apiReq.getBean(FunctionRelateMenu.class);
				functionRelateMenu.setFunId(funId);
				functionRelateMenu.setMenuId(permissionMenus.getId().intValue());
				functionRelateMenuList.add(functionRelateMenu);
			}
			functionRelateMenuMapper.batchinsert(functionRelateMenuList);
			platformTransactionManager.commit(transactionStatus);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		} catch (Exception e) {
			platformTransactionManager.rollback(transactionStatus);
			logger.error("operationMenu:", e);
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.updateUserRelateRole", descript = "修改用户角色关系", apiParams = { @ApiParam(name = "id", isNull = false, descript = "用户角色关系Id"),
			@ApiParam(name = "roleId", isNull = false, descript = "角色ID"), @ApiParam(name = "userId", isNull = false, descript = "用户ID") })
	@Override
	public String updateUserRelateRole(ApiRequest apiReq) {
		UserRelateRole userRelateRole = (UserRelateRole) apiReq.getBean(UserRelateRole.class);
		// 修改用户角色关系
		int flag = userRelateRoleMapper.updateByPrimaryKey(userRelateRole);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.updateUserRelateOrg", descript = "修改用户组织关系", apiParams = { @ApiParam(name = "id", isNull = false, descript = "用户组织关系Id"),
			@ApiParam(name = "roleId", isNull = false, descript = "角色ID"), @ApiParam(name = "userId", isNull = false, descript = "用户ID") })
	@Override
	public String updateUserRelateOrg(ApiRequest apiReq) {
		UserRelateOrg userRelateOrg = (UserRelateOrg) apiReq.getBean(UserRelateOrg.class);
		// 修改用户组织关系
		int flag = userRelateOrgMapper.updateByPrimaryKey(userRelateOrg);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.updatePermissionRelateRole", descript = "修改角色功能权限关系", apiParams = { @ApiParam(name = "id", isNull = false, descript = "角色功能权限关系Id"),
			@ApiParam(name = "roleId", isNull = false, descript = "角色ID"), @ApiParam(name = "funId", isNull = false, descript = "功能Id"),
			@ApiParam(name = "menuId", isNull = false, descript = "菜单Id") })
	@Override
	public String updatePermissionRelateRole(ApiRequest apiReq) {
		PermissionRelateRole permissionRelateRole = (PermissionRelateRole) apiReq.getBean(PermissionRelateRole.class);
		// 修改角色功能权限关系
		int flag = permissionRelateRoleMapper.updateByPrimaryKey(permissionRelateRole);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.updateOrgRelateRole", descript = "修改用户角色关系", apiParams = { @ApiParam(name = "id", isNull = false, descript = "用户角色关系Id"),
			@ApiParam(name = "roleId", isNull = false, descript = "角色ID"), @ApiParam(name = "orgId", isNull = false, descript = "组织Id") })
	@Override
	public String updateOrgRelateRole(ApiRequest apiReq) {
		OrgRelateRole orgRelateRole = (OrgRelateRole) apiReq.getBean(OrgRelateRole.class);
		// 修改用户角色关系
		int flag = orgRelateRoleMapper.updateByPrimaryKey(orgRelateRole);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.updateFunctionRelateMenu", descript = "修改功能菜单", apiParams = { @ApiParam(name = "id", isNull = false, descript = "功能菜单Id"),
			@ApiParam(name = "menuId", isNull = false, descript = "菜单ID"), @ApiParam(name = "funId", isNull = false, descript = "功能Id") })
	@Override
	public String updateFunctionRelateMenu(ApiRequest apiReq) {
		FunctionRelateMenu functionRelateMenu = (FunctionRelateMenu) apiReq.getBean(FunctionRelateMenu.class);
		// 修改功能菜单
		int flag = functionRelateMenuMapper.updateByPrimaryKey(functionRelateMenu);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	/****************************** 删除操作 ********************************************************/

	@ApiMethod(value = "user.deleteOrganizational", descript = "删除组织架构", apiParams = { @ApiParam(name = "id", isNull = false, descript = "组织Id") })
	@Override
	public String deleteOrganizational(ApiRequest apiReq) {
		int id = apiReq.getInt("id");
		logger.info("删除组织架构={}" + id);
		// 删除组织架构
		int flag = organizationalMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.deleteRoles", descript = "删除角色", apiParams = { @ApiParam(name = "id", isNull = false, descript = "角色Id") })
	@Override
	public String deleteRoles(ApiRequest apiReq) {
		int id = apiReq.getInt("id");
		logger.info(" 删除角色={}" + id);
		// 删除角色
		int flag = rolesMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.deletePermissionFunction", descript = "删除权限", apiParams = { @ApiParam(name = "id", isNull = false, descript = "权限Id") })
	@Override
	public String deletePermissionFunction(ApiRequest apiReq) {

		int id = apiReq.getInt("id");
		logger.info(" 删除菜单={}" + id);
		// 删除权限
		int flag = permissionFunctionMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.deleteMenu", descript = "删除菜单", apiParams = { @ApiParam(name = "id", isNull = false, descript = "菜单Id") })
	@Override
	public String deleteMenu(ApiRequest apiReq) {

		int id = apiReq.getInt("id");
		logger.info(" 删除菜单={}" + id);
		// 删除菜单
		int flag = permissionMenusMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.deleteUserRelateRole", descript = "删除用户角色关系", apiParams = { @ApiParam(name = "id", isNull = false, descript = "用户角色关系Id") })
	@Override
	public String deleteUserRelateRole(ApiRequest apiReq) {
		int id = apiReq.getInt("id");
		logger.info("删除用户角色关系={}" + id);
		// 删除用户角色关系
		int flag = userRelateRoleMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));

	}

	@ApiMethod(value = "user.deleteUserRelateOrg", descript = "删除用户组织关系", apiParams = { @ApiParam(name = "id", isNull = false, descript = "用户组织关系Id") })
	@Override
	public String deleteUserRelateOrg(ApiRequest apiReq) {
		int id = apiReq.getInt("id");
		logger.info("删除用户组织关系={}" + id);
		// 删除用户组织关系
		int flag = userRelateOrgMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.deletePermissionRelateRole", descript = "删除角色功能权限关系", apiParams = { @ApiParam(name = "id", isNull = false, descript = "角色功能权限关系Id") })
	@Override
	public String deletePermissionRelateRole(ApiRequest apiReq) {
		int id = apiReq.getInt("id");
		logger.info("删除角色功能权限关系={}" + id);
		// 删除角色功能权限关系
		int flag = permissionRelateRoleMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.deleteOrgRelateRole", descript = "删除用户角色关系", apiParams = { @ApiParam(name = "id", isNull = false, descript = "用户角色关系Id") })
	@Override
	public String deleteOrgRelateRole(ApiRequest apiReq) {
		int id = apiReq.getInt("id");
		logger.info("删除用户角色关系={}" + id);
		// 删除用户角色关系
		int flag = orgRelateRoleMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.deleteFunctionRelateMenu", descript = "删除功能菜单", apiParams = { @ApiParam(name = "id", isNull = false, descript = "功能菜单Id") })
	@Override
	public String deleteFunctionRelateMenu(ApiRequest apiReq) {

		int id = apiReq.getInt("id");
		logger.info("删除功能菜单={}" + id);
		// 删除功能菜单
		int flag = functionRelateMenuMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "user.queryUserById", descript = "查询用户详情", apiParams = { @ApiParam(name = "id", isNull = false, descript = "用户ID") })
	@Override
	public String queryUserById(ApiRequest apiReq) {
		UserDto userDto = usersMapper.queryUserById(apiReq);
		int count = 0;
		if (null != userDto) {
			count = 1;
			// 查询用户的角色列表
			List<UserRelateRoleDto> roleList = userRelateRoleMapper.getUserRelateRoleListByUserId(apiReq.getString("id"));
			userDto.setRoleList(roleList);
			// 查询用户的组织列表
			List<UserRelateOrgDto> orgList = userRelateOrgMapper.getUserRelateOrgListByUserId(apiReq.getString("id"));
			userDto.setOrgList(orgList);
		}
		return this.buildResponse(apiReq, new ApiResponse<UserDto>(ApiMsgEnum.SUCCESS, count, userDto));
	}

	@ApiMethod(value = "user.queryRolesById", descript = "查询角色详情", apiParams = { @ApiParam(name = "id", isNull = false, descript = "角色ID") })
	@Override
	public String queryRolesById(ApiRequest apiReq) {
		RolesDto rolesDto = rolesMapper.queryRolesById(apiReq);

		// 根据角色ID得到角色的关联菜单,功能集合
		List<PermissionRelateRoleDto> permissionRelateRoleList = permissionRelateRoleMapper.getListByRoleId(apiReq.getInt("id"));
		List<PermissionRelateRoleDto> menuList = new ArrayList<PermissionRelateRoleDto>();
		List<PermissionRelateRoleDto> funList = new ArrayList<PermissionRelateRoleDto>();
		for (PermissionRelateRoleDto permissionRelateRole : permissionRelateRoleList) {
			if (permissionRelateRole.getFunId() == 0) {
				menuList.add(permissionRelateRole);
			} else if (permissionRelateRole.getMenuId() == 0) {
				funList.add(permissionRelateRole);
			}
		}
		rolesDto.setMenuList(menuList);
		rolesDto.setFunList(funList);
		int count = 0;
		if (null != rolesDto) {
			count = 1;
		}
		return this.buildResponse(apiReq, new ApiResponse<RolesDto>(ApiMsgEnum.SUCCESS, count, rolesDto));
	}

	@ApiMethod(value = "user.queryFunctionById", descript = "查询权限详情", apiParams = { @ApiParam(name = "id", isNull = false, descript = "权限ID") })
	@Override
	public String queryFunctionById(ApiRequest apiReq) {

		PermissionFunctionDto permissionFunctionDto = permissionFunctionMapper.queryFunctionById(apiReq);
		int count = 0;
		if (null != permissionFunctionDto) {
			count = 1;
		}
		return this.buildResponse(apiReq, new ApiResponse<PermissionFunctionDto>(ApiMsgEnum.SUCCESS, count, permissionFunctionDto));
	}

	@ApiMethod(value = "user.updateUsers", descript = "用户修改", apiParams = { @ApiParam(name = "id", isNull = false, descript = "用户ID"), @ApiParam(name = "username", isNull = true, descript = "用户名"),
			@ApiParam(name = "password", isNull = true, descript = "密码"), @ApiParam(name = "realName", isNull = true, descript = "真实姓名"), @ApiParam(name = "nickName", isNull = true, descript = "昵称"),
			@ApiParam(name = "email", isNull = true, descript = "电子邮件"), @ApiParam(name = "isSuperuser", isNull = true, descript = "是否是超级管理员"),
			@ApiParam(name = "userTypeName", isNull = true, descript = "用户类型名称"), @ApiParam(name = "userType", isNull = true, descript = "用户类型"),
			@ApiParam(name = "roleIds", isNull = true, descript = "角色"), @ApiParam(name = "orgIds", isNull = true, descript = "组织") })
	@Override
	public String updateUsers(ApiRequest apiReq) {
		TransactionStatus transactionStatus = platformTransactionManager.getTransaction(new DefaultTransactionDefinition());
		try {
			Users users = (Users) apiReq.getBean(Users.class);
			// 用户修改
			usersMapper.updateByPrimaryKey(users);
			// 修改用户角色信息
			Integer[] roleIds = apiReq.getIntArray("roleIds");
			List<UserRelateRole> userRelateRoleList = new ArrayList<UserRelateRole>();
			for (Integer roleId : roleIds) {
				UserRelateRole userRelateRole = new UserRelateRole();
				userRelateRole.setRoleId(roleId);
				userRelateRole.setUserId(users.getId());
				userRelateRoleList.add(userRelateRole);
			}
			// 删除当前用户的所有角色
			userRelateRoleMapper.deleteByUserId(users.getId());
			// 批量新增用户角色
			userRelateRoleMapper.batchinsert(userRelateRoleList);
			// 删除当前用户的组织信息
			userRelateOrgMapper.deleteByUserId(users.getId());
			// 批量新增用户组织信息
			Integer[] orgIds = apiReq.getIntArray("orgIds");
			List<UserRelateOrg> userRelateOrgList = new ArrayList<UserRelateOrg>();
			for (Integer orgId : orgIds) {
				UserRelateOrg userRelateOrg = new UserRelateOrg();
				userRelateOrg.setOrgId(orgId);
				userRelateOrg.setUserId(users.getId());
				userRelateOrgList.add(userRelateOrg);
			}
			userRelateOrgMapper.batchinsert(userRelateOrgList);
			platformTransactionManager.commit(transactionStatus);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		} catch (Exception e) {
			platformTransactionManager.rollback(transactionStatus);
			logger.error("operationMenu:", e);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
		}
	}

	@ApiMethod(value = "user.getMenuById", descript = "根据菜单ID得到菜单的详情", apiParams = { @ApiParam(name = "id", isNull = false, descript = "菜单ID") })
	@Override
	public String getMenuById(ApiRequest apiReq) {
		try {
			int id = apiReq.getInt("id");
			PermissionMenusDto permissionMenusDto = permissionMenusMapper.selectByPrimaryKey(id);
			List<FunctionRelateMenuDto> functionRelateMenuDtoList = functionRelateMenuMapper.getFunctionRelateMenuListByMenuId(id);
			permissionMenusDto.setFunctionRelateMenuDtoList(functionRelateMenuDtoList);
			return this.buildResponse(apiReq, new ApiResponse<PermissionMenusDto>(ApiMsgEnum.SUCCESS, 1, permissionMenusDto));
		} catch (Exception e) {
			logger.error("getMenuById:" + e);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
		}
	}

	@ApiMethod(value = "user.getMenuByParenId", descript = "根据父节点查询菜单列表", apiParams = { @ApiParam(name = "parentId", isNull = false, descript = "菜单父节点Id") })
	public String getMenuByParenId(ApiRequest apiReq) {
		String parentId = apiReq.getString("parentId");
		List<PermissionMenusDto> list = permissionMenusMapper.getMenuByParenId(parentId);
		return this.buildResponse(apiReq, new ApiResponse<List<PermissionMenusDto>>(ApiMsgEnum.SUCCESS, list.size(), list));

	}

	@ApiMethod(value = "user.getFunctionListByMenuId", descript = "根据菜单ID得到功能列表", apiParams = { @ApiParam(name = "menuId", isNull = false, descript = "菜单Id") })
	@Override
	public String getFunctionListByMenuId(ApiRequest apiReq) {
		int menuId = apiReq.getInt("menuId");
		// 得到菜单的所有功能列表
		List<FunctionRelateMenuDto> functionRelateMenuList = functionRelateMenuMapper.getFunctionRelateMenuListByMenuId(menuId);
		return this.buildResponse(apiReq, new ApiResponse<List<FunctionRelateMenuDto>>(ApiMsgEnum.SUCCESS, functionRelateMenuList.size(), functionRelateMenuList));
	}

	@ApiMethod(value = "user.batchSavePermissionRelateRole", descript = "批量新增菜单角色关联功能信息", apiParams = { @ApiParam(name = "roleId", isNull = false, descript = "角色ID"),
			@ApiParam(name = "funIds", isNull = false, descript = "功能Ids"), @ApiParam(name = "menuId", isNull = false, descript = "菜单Id") })
	@Override
	public String batchSavePermissionRelateRole(ApiRequest apiReq) {
		try {
			// 根据角色ID和菜单ID删除功能列表
			permissionRelateRoleMapper.deleteByRoleIdMenuId(apiReq);
			Integer[] funIds = apiReq.getIntArray("funIds");
			List<PermissionRelateRole> permissionRelateRoleList = new ArrayList<PermissionRelateRole>();
			for (Integer funId : funIds) {
				PermissionRelateRole permissionRelateRole = new PermissionRelateRole();
				permissionRelateRole.setFunId(funId);
				permissionRelateRole.setMenuId(apiReq.getInt("menuId"));
				permissionRelateRole.setRoleId(apiReq.getInt("roleId"));
				permissionRelateRoleList.add(permissionRelateRole);
			}
			// 批量添加角色的菜单关联的功能
			permissionRelateRoleMapper.batchinsert(permissionRelateRoleList);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		} catch (Exception e) {
			logger.error("operationMenu:", e);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
		}
	}

	@ApiMethod(value = "user.getPermissionRelateRoleList", descript = "根据角色查询菜单的关联功能列表", apiParams = { @ApiParam(name = "roleId", isNull = true, descript = "角色ID"),
			@ApiParam(name = "menuId", isNull = true, descript = "菜单Id") })
	@Override
	public String getPermissionRelateRoleList(ApiRequest apiReq) {
		try {
			List<PermissionRelateRoleDto> permissionRelateRoleList = permissionRelateRoleMapper.getList(apiReq);
			return this.buildResponse(apiReq, new ApiResponse<List<PermissionRelateRoleDto>>(ApiMsgEnum.SUCCESS, permissionRelateRoleList.size(), permissionRelateRoleList));
		} catch (Exception e) {
			logger.error("operationMenu:", e);
			return this.buildResponse(apiReq, new ApiResponse<Object>(ApiMsgEnum.FAIL));
		}
	}

	@ApiMethod(value = "backend.user.getUserList", descript = "根据用户类型查询用户列表", apiParams = { @ApiParam(name = "userType", isNull = true, descript = "用户类型") })
	@Override
	public String getUserList(ApiRequest apiReq) {
		List<UserDto> list = usersMapper.queryUserList(apiReq);
		return this.buildResponse(apiReq, new ApiResponse<List<UserDto>>(ApiMsgEnum.SUCCESS, list.size(), list));
	}

	@ApiMethod(value = "backend.user.getUserSimple", descript = "根据用户Id获取用户简单信息", apiParams = { @ApiParam(name = "userId", isNull = true, descript = "用户id") })
	public String getUserSimple(ApiRequest apiReq) {
		UserSimpleDto userSimpleDto = this.usersMapper.getUserSimpleById(apiReq.getString("userId"));
		return this.buildResponse(apiReq, new ApiResponse<UserSimpleDto>(ApiMsgEnum.SUCCESS, 1, userSimpleDto));
	}
}
