package com.oxygen.user.service;

import com.oxygen.dto.ApiRequest;

/**
 * 后台用户服务接口
 * 
 */
public interface BackendUserService {
	String login(ApiRequest apiReq);

	/**
	 * 
	 * @Title: register
	 * @author zhoujun 2016-5-30 下午3:51:46
	 * @Description:用户注册<br>
	 *                       <h1>example:</h1> <br>
	 *                       请在这里添加使用例子
	 * 
	 *                       <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午3:51:46
	 *                       </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String register(ApiRequest apiReq);

	/**
	 * 
	 * @Title: operationOrganizational
	 * @author zhoujun 2016-5-30 下午3:57:58
	 * @Description:新增 组织架构<br>
	 *                 <h1>example:</h1> <br>
	 *                 请在这里添加使用例子
	 * 
	 *                 <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午3:57:58
	 *                 </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String operationOrganizational(ApiRequest apiReq);

	/**
	 * 
	 * @Title: operationRoles
	 * @author zhoujun 2016-5-30 下午3:59:10
	 * @Description: 新增角色<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午3:59:10
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String operationRoles(ApiRequest apiReq);

	/**
	 * 
	 * @Title: operationPermissionFunction
	 * @author zhoujun 2016-5-30 下午3:59:26
	 * @Description: 新增 权限<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午3:59:26
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String operationPermissionFunction(ApiRequest apiReq);

	/**
	 * 
	 * @Title: operationMenu
	 * @author zhoujun 2016-5-30 下午3:59:50
	 * @Description: 新增菜单<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午3:59:50
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String operationMenu(ApiRequest apiReq);

	/**
	 * 
	 * @Title: operationUserRelateRole
	 * @author zhoujun 2016-5-30 下午4:00:06
	 * @Description: 新增用户关系角色<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午4:00:06
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String operationUserRelateRole(ApiRequest apiReq);

	/**
	 * 
	 * @Title: operationUserRelateOrg
	 * @author zhoujun 2016-5-30 下午4:01:50
	 * @Description: 新增 用户关系组织<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午4:01:50
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String operationUserRelateOrg(ApiRequest apiReq);

	/**
	 * 
	 * @Title: operationPermissionRelateRole
	 * @author jason 2016-7-29 下午4:02:21
	 * @Description: 新增 权限角色关系<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     jason 2016-7-29 下午4:02:21
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String operationPermissionRelateRole(ApiRequest apiReq);

	/**
	 * 批量新增菜单角色关联功能信息
	 * 
	 * @param apiReq
	 * @return
	 */
	String batchSavePermissionRelateRole(ApiRequest apiReq);

	/**
	 * 
	 * @Title: operationOrgRelateRole
	 * @author zhoujun 2016-5-30 下午4:02:36
	 * @Description: 新增组织角色<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午4:02:36
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String operationOrgRelateRole(ApiRequest apiReq);

	/**
	 * 
	 * @Title: operationFunctionRelateMenu
	 * @author zhoujun 2016-5-30 下午4:03:31
	 * @Description: 新增 权限菜单关系 <br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午4:03:31
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String operationFunctionRelateMenu(ApiRequest apiReq);

	/**
	 * 批量新增菜单功能关系表信息
	 */
	String batchAddFunctionRelateMenu(ApiRequest apiReq);

	/*************************************************************************/
	/**
	 * 
	 * @Title: getOrganizational
	 * @author zhoujun 2016-5-30 下午4:03:51
	 * @Description: 查询组织架构列表<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午4:03:51
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String getOrganizational(ApiRequest apiReq);

	/**
	 * @Title: getOrgById
	 * @author jason 2016-7-18 下午4:03:51
	 * @Description: 查询组织详情信息<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *    jason 2016-7-18 下午4:03:51
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String getOrgById(ApiRequest apiReq);

	/**
	 * 
	 * @Title: getRoles
	 * @author zhoujun 2016-5-30 下午4:04:16
	 * @Description:查询角色列表 <br>
	 *                     <h1>example:</h1> <br>
	 *                     请在这里添加使用例子
	 * 
	 *                     <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午4:04:16
	 *                     </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String getRoles(ApiRequest apiReq);

	/**
	 * 
	 * @Title: getMenu
	 * @author zhoujun 2016-5-30 下午4:04:34
	 * @Description:查询菜单 列表<br>
	 *                   <h1>example:</h1> <br>
	 *                   请在这里添加使用例子
	 * 
	 *                   <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 下午4:04:34
	 *                   </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String getMenu(ApiRequest apiReq);

	/**
	 * 根据父节点ID得到菜单列表
	 * 
	 * @author jason
	 * @param apiReq
	 * @return
	 */
	String getMenuByParenId(ApiRequest apiReq);

	/**
	 * 
	 * @Title: getMenuById
	 * @author zhoujun 2016-07-12 下午4:04:34
	 * @Description:查询菜单详情<br>
	 *                         <h1>example:</h1> <br>
	 *                         请在这里添加使用例子
	 * 
	 *                         <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun 2016-07-12 下午4:04:34
	 *                         </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String getMenuById(ApiRequest apiReq);

	/**
	 * 
	 * @Title: getFunction
	 * @author zhoujun 2016-7-7 上午10:08:58
	 * @Description: 查询功能<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-7-7 上午10:08:58
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	public String getFunction(ApiRequest apiReq);

	/**
	 * 根据菜单ID得到功能列表
	 * 
	 * @author jason
	 * @param apiReq
	 * @return
	 */
	public String getFunctionListByMenuId(ApiRequest apiReq);

	/**
	 * 
	 * @Title: queryUserList
	 * @author zhoujun 2016-7-7 下午2:10:44
	 * @Description:查询用户<br>
	 *                       <h1>example:</h1> <br>
	 *                       请在这里添加使用例子
	 * 
	 *                       <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-7-7 下午2:10:44
	 *                       </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	public String queryUserList(ApiRequest apiReq);

	/**
	 * 
	 * @Title: queryUserById
	 * @author zhoujun 2016-7-8 下午2:05:31
	 * @Description: 根据用户ID查找用户信息<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-7-8 下午2:05:31
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	public String queryUserById(ApiRequest apiReq);

	/***** 修改用户信息 *********/
	String updateUsers(ApiRequest apiReq);

	/***** 组织架构操作 *********/
	String updateOrganizational(ApiRequest apiReq);

	/***** 角色构操作 *********/
	String updateRoles(ApiRequest apiReq);

	/**
	 * 
	 * @Title: updatePermissionFunction
	 * @author zhoujun 2016-5-30 上午11:56:03
	 * @Description: 修改 权限<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:56:03
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String updatePermissionFunction(ApiRequest apiReq);

	/**
	 * 
	 * @Title: updateMenu
	 * @author zhoujun 2016-5-30 上午11:55:42
	 * @Description: 修改 菜单操作<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:55:42
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String updateMenu(ApiRequest apiReq);

	/**
	 * 
	 * @Title: updateUserRelateRole
	 * @author zhoujun 2016-5-30 上午11:55:24
	 * @Description: 修改 用户关系角色 <br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:55:24
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String updateUserRelateRole(ApiRequest apiReq);

	/**
	 * 
	 * @Title: updateUserRelateOrg
	 * @author zhoujun 2016-5-30 上午11:55:04
	 * @Description: 修改 用户关系组织<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:55:04
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String updateUserRelateOrg(ApiRequest apiReq);

	/**
	 * 
	 * @Title: updatePermissionRelateRole
	 * @author zhoujun 2016-5-30 上午11:54:46
	 * @Description: 修改权限角色关系<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:54:46
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String updatePermissionRelateRole(ApiRequest apiReq);

	/**
	 * 
	 * @Title: updateOrgRelateRole
	 * @author zhoujun 2016-5-30 上午11:54:31
	 * @Description:修改组织角色<br>
	 *                         <h1>example:</h1> <br>
	 *                         请在这里添加使用例子
	 * 
	 *                         <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:54:31
	 *                         </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String updateOrgRelateRole(ApiRequest apiReq);

	/**
	 * 
	 * @Title: updateFunctionRelateMenu
	 * @author zhoujun 2016-5-30 上午11:12:11
	 * @Description: 修改 权限菜单<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:12:11
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String updateFunctionRelateMenu(ApiRequest apiReq);

	/**************** 删除操作 *********************************************************/

	/**
	 * 
	 * @Title: deleteOrganizational
	 * @author zhoujun 2016-5-30 上午11:10:24
	 * @Description: 删除组织架构<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:10:24
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String deleteOrganizational(ApiRequest apiReq);

	/**
	 * 
	 * @Title: deleteRoles
	 * @author zhoujun 2016-5-30 上午11:10:01
	 * @Description: 删除角色<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:10:01
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String deleteRoles(ApiRequest apiReq);

	/**
	 * 
	 * @Title: deletePermissionFunction
	 * @author zhoujun 2016-5-30 上午11:09:42
	 * @Description: 删除权限<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:09:42
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String deletePermissionFunction(ApiRequest apiReq);

	/**
	 * 
	 * @Title: deleteMenu
	 * @author zhoujun 2016-5-30 上午11:09:25
	 * @Description: 删除菜单<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:09:25
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String deleteMenu(ApiRequest apiReq);

	/**
	 * 
	 * @Title: deleteUserRelateRole
	 * @author zhoujun 2016-5-30 上午11:08:45
	 * @Description:删除用户关系角色<br>
	 *                           <h1>example:</h1> <br>
	 *                           请在这里添加使用例子
	 * 
	 *                           <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:08:45
	 *                           </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String deleteUserRelateRole(ApiRequest apiReq);

	/**
	 * 
	 * @Title: deleteUserRelateOrg
	 * @author zhoujun 2016-5-30 上午11:08:29
	 * @Description: 删除用户关系组织<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:08:29
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String deleteUserRelateOrg(ApiRequest apiReq);

	/**
	 * 
	 * @Title: deletePermissionRelateRole
	 * @author zhoujun 2016-5-30 上午11:04:53
	 * @Description: 删除权限角色关系<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:04:53
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String deletePermissionRelateRole(ApiRequest apiReq);

	/**
	 * 
	 * @Title: deleteOrgRelateRole
	 * @author zhoujun 2016-5-30 上午11:04:30
	 * @Description: 删除组织角色<br>
	 *               <h1>example:</h1> <br>
	 *               请在这里添加使用例子
	 * 
	 *               <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     zhoujun  2016-5-30 上午11:04:30
	 *               </pre>
	 * 
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String deleteOrgRelateRole(ApiRequest apiReq);

	/**
	 * @Title: deleteFunctionRelateMenu
	 * @author zhoujun 2016-5-30 上午11:03:09
	 * @Description: 删除权限菜单<br>
	 * @param @param
	 *            apiReq
	 * @param @return
	 * @return String
	 */
	String deleteFunctionRelateMenu(ApiRequest apiReq);

	String queryRolesById(ApiRequest apiReq);

	String queryFunctionById(ApiRequest apiReq);

	/**
	 * 根据角色查询菜单的关联功能列表
	 * 
	 * @param apiReq
	 * @return
	 */
	String getPermissionRelateRoleList(ApiRequest apiReq);

	/**
	 * 得到用户列表
	 * 
	 * @param apiReq
	 * @return
	 */
	public String getUserList(ApiRequest apiReq);
}
