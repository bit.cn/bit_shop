package com.oxygen.user.constants;

public class Constant {
	/**
	 * 推荐参数key
	 */
	public static final String RECOMMEND_GAME_PARAM_KEY = "recommend_game_param_key_";

	public static final String PARAM_PET_ACTIVE_ = "5010100";
	public static final String PARAM_PET_ACTIVE_BASE = "50102001";
	public static final String PARAM_PET_ACTIVE_ACQUIRE = "50103001";
	public static final String PARAM_PET_EXP_ACQUIRE = "50104001";
	public static final String PARAM_PET_SERVITOR_PARAM = "50104002";
	public static final String PARAM_PET_BOSS_PARAM = "50104003";

	public static final long PARAM_SCENE_AWARD_DIFF = 5 * 60 * 1000;
}
