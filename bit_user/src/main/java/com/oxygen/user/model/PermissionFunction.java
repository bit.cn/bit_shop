package com.oxygen.user.model;

import java.util.Date;

/**
 * 权限功能实体类
 * @author jason
 */
public class PermissionFunction {
    private Integer id;      //主键ID

    private String name;     //权限功能名称

    private String code;     //权限功能code
    
    private String onclick;  //按钮的操作

    private Date createTime; //创建时间

    private Date updateTime; //更新时间

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	public String getOnclick() {
		return onclick;
	}

	public void setOnclick(String onclick) {
		this.onclick = onclick;
	}
    
}