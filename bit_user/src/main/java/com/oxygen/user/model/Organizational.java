package com.oxygen.user.model;

import java.util.Date;

/**
 * 组织机构实体类
 * @author jason
 */
public class Organizational {
    private Integer id;          //主键ID

    private String name;         //组织名称

    private String code;         //组织代码

    private Integer parentId;    //父节点ID

    private Date createTime;     //创建时间

    private Date updateTime;     //更新时间

    private String description;  //描述
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}