package com.oxygen.user.model;

import java.util.Date;

/**
 * 权限菜单
 * @author jason
 */
public class PermissionMenus {
    private Long id;     	//主键ID

    private String name;    	//菜单名称

    private String code;    	//菜单CODE
    
    private String menuUrl;     //菜单地址

    private Integer parentId;	//上级菜单

    private Date createTime;    //创建时间

    private Date updateTime;    //更新时间
    
    private String isRoot;      //是否是根节点
    
    private String createId;    //创建者ID
   

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsRoot() {
        return isRoot;
    }

    public void setIsRoot(String isRoot) {
        this.isRoot = isRoot;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    
}