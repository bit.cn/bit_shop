package com.oxygen.user.dto;

import java.util.List;

/**
 * 权限菜单包装实体类
 * @author jason
 */
public class PermissionMenusDto {
	private Integer id;       //主键ID

	private String name;      //菜单名称

	private String code;      //菜单code
	
	private String menuUrl;   //菜单链接地址

	private Integer parentId; //父节点ID

	private String createTime;//创建时间

	private String updateTime;//更新时间
	
	private List<FunctionRelateMenuDto> functionRelateMenuDtoList;//菜单功能关联列表

	public Integer getId() {
	    return id;
	}

	public void setId(Integer id) {
	    this.id = id;
	}

	public String getName() {
	    return name;
	}

	public void setName(String name) {
	    this.name = name;
	}

	public String getCode() {
	    return code;
	}

	public void setCode(String code) {
	    this.code = code;
	}

	public String getMenuUrl() {
	    return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
	    this.menuUrl = menuUrl;
	}

	public Integer getParentId() {
	    return parentId;
	}

	public void setParentId(Integer parentId) {
	    this.parentId = parentId;
	}

	public String getCreateTime() {
	    return createTime;
	}

	public void setCreateTime(String createTime) {
	    this.createTime = createTime;
	}

	public String getUpdateTime() {
	    return updateTime;
	}

	public void setUpdateTime(String updateTime) {
	    this.updateTime = updateTime;
	}

	public List<FunctionRelateMenuDto> getFunctionRelateMenuDtoList() {
		return functionRelateMenuDtoList;
	}

	public void setFunctionRelateMenuDtoList(
			List<FunctionRelateMenuDto> functionRelateMenuDtoList) {
		this.functionRelateMenuDtoList = functionRelateMenuDtoList;
	}

	
}
