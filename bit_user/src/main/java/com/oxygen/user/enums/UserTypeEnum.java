package com.oxygen.user.enums;

/**
 * 用户类型数据字典 (H5:H5游戏短;backend:后台管理系统)
 * @author jason
 */
public enum UserTypeEnum {
	H5("H5游戏端"),backend("后台超管");
	
	private String name;
	UserTypeEnum(String name) {
		this.name = name;
	}
	public String getName() {
	    return name;
	}
}
