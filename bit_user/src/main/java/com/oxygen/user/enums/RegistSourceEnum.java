package com.oxygen.user.enums;

/**
 * 用户来源系统枚举(trusteeship:物业托管系统)
 * 
 * @author jason
 */
public enum RegistSourceEnum {

	trusteeship("trusteeship", "物业托管系统"), ownerSystem("ownerSystem", "业主管理系统"), backendSystem(
			"backendSystem", "后台管理系统");

	String name;
	String code;

	RegistSourceEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
