package com.oxygen.user.enums;


/**
 * 游戏来源
 * @author kevin
 */
public enum GameResourceEnum {
	freegame("freegame","免费游戏"), buygame("buygame","购买的游戏");
	
	private String code;
	private String name;
	
	GameResourceEnum(String code,String name) {
		this.code = code;
		this.name=name;
	}

	public String getCode() {
		return this.code;
	}

	public static String getCodeByName(String name) {
		GameResourceEnum[] enumArr = GameResourceEnum.values();
		for (GameResourceEnum aEnum : enumArr) {
			if (aEnum.name().equals(name)) {
				return aEnum.getCode();
			}
		}
		return null;
	}

}
