package com.oxygen.user.enums;

/**
 * 每一轮挑战状态枚举
 * 
 * @author jason
 */
public enum WheelChallengeStatusEnum {
	bet("bet", "下注"), complete("complete", "完成"), finish("finish", "结束"), alreadyreceive("alreadyreceive", "已领取"),expire("expire", "过期");

	String name;
	String code;

	WheelChallengeStatusEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
