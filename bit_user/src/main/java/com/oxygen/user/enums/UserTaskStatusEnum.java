package com.oxygen.user.enums;

/**
 * 用户任务状态
 * @author jason
 */
public enum UserTaskStatusEnum {
	notfinish("未完成"), notreceive("未领取"), finish("领取完成");
	
	private String name;
	UserTaskStatusEnum(String name) {
		this.name = name;
	}
	public String getName() {
	    return name;
	}
}
