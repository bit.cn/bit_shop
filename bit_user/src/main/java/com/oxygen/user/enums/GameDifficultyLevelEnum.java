package com.oxygen.user.enums;


/**
 * 游戏难度等级
 * 
 * @author kevin
 * 
 */
public enum GameDifficultyLevelEnum {
	A(1), B(2), C(3), D(4), E(5);

	private int code;

	GameDifficultyLevelEnum(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static int getCodeByName(String name) {
		GameDifficultyLevelEnum[] enumArr = GameDifficultyLevelEnum.values();
		for (GameDifficultyLevelEnum aEnum : enumArr) {
			if (aEnum.name().equals(name)) {
				return aEnum.getCode();
			}
		}
		return 0;
	}

}
