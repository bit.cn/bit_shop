package com.oxygen.user.enums;


/**
 * 是否是超管枚举(0:非超管1：超管)
 * @author jason
 */
public enum IsSuperuserEnum {

	NOTSUPERUSER(0), SUPERUSER(1);

	Integer id;

	IsSuperuserEnum(Integer _id) {
		this.id = _id;
	}

	public Integer getId() {
		return id;
	}
}
