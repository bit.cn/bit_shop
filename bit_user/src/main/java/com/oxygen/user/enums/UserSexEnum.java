package com.oxygen.user.enums;

import java.util.ArrayList;
import java.util.List;

public enum UserSexEnum {
	male, female, other;

	public static List<String> getList() {
		UserSexEnum[] enumArr = UserSexEnum.values();
		List<String> enumList = new ArrayList<String>();
		for (UserSexEnum e : enumArr) {
			enumList.add(e.name());
		}
		return enumList;
	}
}
