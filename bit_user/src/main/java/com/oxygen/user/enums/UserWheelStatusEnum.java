package com.oxygen.user.enums;

/**
 * 用户的轮盘抽奖状态枚举
 * @author jason
 *
 */
public enum UserWheelStatusEnum {
	drawin("drawin","抽奖中"), finish("finish","抽奖结束");

	private String code;
	private String name;

	UserWheelStatusEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}
}
