package com.oxygen.user.enums;

/**
 * @author Lucifer
 *
 */
public enum LoginAwardTypeEnum {

	game(1), prop(4), pet(5);

	private int value;

	private LoginAwardTypeEnum(int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}
}
