package com.soloyogame.anitoys.doc.web.controller.front;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.collect.Lists;
import com.soloyogame.anitoys.db.bean.StrongAdvertising;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.db.commond.Business;
import com.soloyogame.anitoys.db.commond.CartInfo;
import com.soloyogame.anitoys.db.commond.Catalog;
import com.soloyogame.anitoys.db.commond.Hotquery;
import com.soloyogame.anitoys.db.commond.IndexImg;
import com.soloyogame.anitoys.db.commond.Navigation;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.commond.PopularityAdvert;
import com.soloyogame.anitoys.db.commond.PopularityProductPlat;
import com.soloyogame.anitoys.db.commond.Product;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.doc.web.controller.front.util.LoginUserHolder;
import com.soloyogame.anitoys.service.BusinessService;
import com.soloyogame.anitoys.service.CatalogService;
import com.soloyogame.anitoys.service.HelpService;
import com.soloyogame.anitoys.service.PlatHotqueryService;
import com.soloyogame.anitoys.service.PlatIndexImgService;
import com.soloyogame.anitoys.service.PlatNavigationService;
import com.soloyogame.anitoys.service.PlatNewsService;
import com.soloyogame.anitoys.service.PlatTopPictureService;
import com.soloyogame.anitoys.service.PopularityAdvertService;
import com.soloyogame.anitoys.service.PopularityProductPlatService;
import com.soloyogame.anitoys.service.ProductService;
import com.soloyogame.anitoys.service.StrongAdvertisingService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;

/**
 * 前端首页
 * @author shaojian
 */
@Controller
@RequestMapping("/")
public class IndexAction 
{
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(IndexAction.class);
	@Autowired
	private RedisCacheProvider redisCacheProvider; 
	@Autowired
	private ProductService productService; 
	@Autowired
	private HelpService helpService;
	@Autowired
	private BusinessService businessService;     //商家服务
	@Autowired
	private PlatHotqueryService hotqueryService;
	@Autowired
	private CatalogService catalogService;
	@Autowired
	private PlatIndexImgService indexImgService;
	@Autowired
	private PlatNewsService platNewsService;
	@Autowired
	private StrongAdvertisingService strongAdvertisingService;
	@Autowired
	private PopularityProductPlatService popularityProductPlatService;
	@Autowired
	private PopularityAdvertService popularityAdvertService;
	@Autowired
	private PlatNavigationService navigationService;
	@Autowired
	private PlatTopPictureService topPictureService;
	
	@RequestMapping({"/", "/index"})
    public String index(ModelMap model) 
    {
        return "index";
    }
	
	/**
     * 拦截404错误跳转页面
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "to404")
    public String to404(ModelMap model) throws Exception 
    {
        logger.info("跳转404页面！");
        return "404Ftl";
    }
}
