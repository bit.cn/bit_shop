package com.soloyogame.anitoys.doc.web.controller.front.help;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.db.commond.CartInfo;
import com.soloyogame.anitoys.db.commond.Catalog;
import com.soloyogame.anitoys.db.commond.Help;
import com.soloyogame.anitoys.db.commond.Hotquery;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.commond.PlatBusinessRecommend;
import com.soloyogame.anitoys.db.commond.Product;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.doc.web.controller.front.BaseController;
import com.soloyogame.anitoys.doc.web.controller.front.util.LoginUserHolder;
import com.soloyogame.anitoys.service.CatalogService;
import com.soloyogame.anitoys.service.HelpService;
import com.soloyogame.anitoys.service.NewsService;
import com.soloyogame.anitoys.service.PlatBusinessRecommendService;
import com.soloyogame.anitoys.service.PlatHotqueryService;
import com.soloyogame.anitoys.service.PlatTopPictureService;
import com.soloyogame.anitoys.service.ProductService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;


/**
 * @author 索罗游
 */
@Controller
@RequestMapping("/front/helpsmanage/")
public class HelpAction extends BaseController<Help> 
{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(HelpAction.class);
    private static final String page_toList = "/manage/help/helpList";
    private static final String page_toEdit = "/manage/help/helpEdit";
    private static final String page_toAdd = "/manage/help/helpEdit";
    private HelpAction() {
        super.page_toList = page_toList;
        super.page_toAdd = page_toAdd;
        super.page_toEdit = page_toEdit;
    }
    
    
    @Autowired
	private HelpService helpService;
    @Autowired
	private RedisCacheProvider redisCacheProvider;
    @Autowired
	private ProductService productService;
    @Autowired
    private CatalogService catalogService;
    @Autowired
    private NewsService newsService;
    @Autowired
	private PlatBusinessRecommendService platBusinessRecommendService;//平台热门推荐商品服务接口
	@Autowired
	private PlatTopPictureService plattopPictureService;
	@Autowired
	private PlatHotqueryService plathotqueryService;


    @Autowired
	public HelpService getService() {
		return helpService;
	}

	public void setHelpService(HelpService helpService) {
		this.helpService = helpService;
	}
	
	@ModelAttribute("newsCatalogs")
	public List<Catalog> getNewsCatalogs()
	{
		// 加载文章目录
        List<Catalog> newCatalogs = loadCatalogNews();
		return newCatalogs;
	}

	@Override
	public void insertAfter(Help e) {
		e.clear();
	}
	
	/**
	 * 帮助中心
	 * @return
	 */
	@RequestMapping("/{helpCode}")
	public String help(@ModelAttribute("helpCode") @PathVariable("helpCode")String helpCode, ModelMap model) throws Exception 
	{
		logger.error("this.helpCode="+helpCode);
		int productCount=productService.selectProductCount();
		model.addAttribute("productCount", productCount);
		if(StringUtils.isBlank(helpCode))
		{
			return "help/help";
		}
		else if(helpCode.equals("index"))
		{
			return "help/help";
		}
		else
		{
			Help helpParam = new Help();
			helpParam.setId(helpCode);
			Help help = helpService.selectOne(helpParam);
			if(help==null)
			{
				throw new NullPointerException("根据code查询不到文章！");
			}
			
			String url = "/jsp/helps/"+help.getId()+".jsp";
			logger.error("url = " + url);
			model.addAttribute("newsInfoUrl", url);
			model.addAttribute("news", help);
			//加载平台推荐商品
	      	PlatBusinessRecommend platBusinessRecommend = new PlatBusinessRecommend();
	        platBusinessRecommend.setType(1);
	        platBusinessRecommend.setTop(4);
	        List<PlatBusinessRecommend> businessRecommends = platBusinessRecommendService.selectList(platBusinessRecommend);
		    model.addAttribute("businessRecommends", businessRecommends);
		    //热门查询
		  	Hotquery hotquery = new Hotquery();
		  	hotquery.setTop(6);
		  	List<Hotquery> hotqueryList = plathotqueryService.selectList(hotquery);
		  	model.addAttribute("hotqueryList", hotqueryList);
		    //加载大首页顶部图片
		  	TopPicture topPictureParam = new TopPicture();
		  	TopPicture topPicture = plattopPictureService.selectOne(topPictureParam);
		  	model.addAttribute("topPicture", topPicture);
			return "help/help";
		}
	}
	
	 /**
     * 加载文章目录
     * @return
     */
    private   List<Catalog> loadCatalogNews() 
    {
        List<Catalog> newCatalogs = new ArrayList<Catalog>();
        Serializable serializable;
        try {
            serializable =  redisCacheProvider.get(ManageContainer.CACHE_NEWSCATALOGS);
        } catch (Exception e) {
            serializable = null;
        }
        String nullValue = "null";
        if (serializable == null || serializable.toString().equals(nullValue)) {
            // 查询 文章目录
            Catalog c = new Catalog();
            c.setType("a");
            c.setPid("0");
            c.setShowInNav("y");
            newCatalogs = catalogService.selectList(c);
            if (newCatalogs != null && newCatalogs.size() > 0) {
                for (int i = 0; i < newCatalogs.size(); i++) {
                    Catalog catalog = newCatalogs.get(i);
                    //加载此目录下的所有文章列表
                    News news = new News();
                    news.setCatalogID(catalog.getId());
                    news.setStatus("y");
                    List<News> newsList = newsService.selectPlatList(news);
                    catalog.setNews(newsList);
                }
            }
        } else {
            newCatalogs =  JSON.parseArray(serializable.toString(), Catalog.class);
        }

        redisCacheProvider.put(ManageContainer.CACHE_NEWSCATALOGS, JSON.toJSONString(newCatalogs));

        return newCatalogs;
    }
}
