package com.soloyogame.anitoys.doc.web.controller.front.news;

import com.soloyogame.anitoys.db.commond.*;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.doc.web.controller.front.FrontBaseController;
import com.soloyogame.anitoys.doc.web.controller.front.util.LoginUserHolder;
import com.soloyogame.anitoys.doc.web.controller.front.util.RequestHolder;
import com.soloyogame.anitoys.service.*;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * 文章管理
 * @author shaojian
 */
@Controller("frontNewsAction")
public class NewsAction extends FrontBaseController<News> 
{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(NewsAction.class);
	
	@Autowired
	private NewsService newsService;                  //商家的新闻接口
	@Autowired
	private PlatNewsService platNewsService;          //平台的新闻接口
	@Autowired
	private RedisCacheProvider redisCacheProvider;
	@Autowired
	private ProductService productService; 
	@Autowired
    private HelpService helpService;
	@Autowired
	private TopPictureService topPictureService;
	@Autowired
	private PlatHotqueryService plathotqueryService;
	@Autowired
	private HotqueryService hotqueryService;
	@Autowired
	private CatalogService catalogService;
	@Autowired
	private BusinessRecommendService  businessRecommendService;//店铺推荐商品服务
	@Autowired
	private PlatBusinessRecommendService platBusinessRecommendService;//平台热门推荐商品服务接口
	@Autowired
	private PlatTopPictureService plattopPictureService;

	@Override
	public NewsService getService() {
		return newsService;
	}

	@ModelAttribute("newsCatalogs")
	public List<Catalog> getNewsCatalogs()
	{
		return loadNewCatalogs();
	}
	/**
	 * 加载平台的文章目录
	 */
	public List<Catalog> loadNewCatalogs() 
	{
		Catalog c = new Catalog();
		c.setType("a");
		c.setPid("0");
		c.setShowInNav("y");
		List<Catalog> newCatalogs = catalogService.selectList(c);
		if(newCatalogs!=null && newCatalogs.size()>0)
		{
			for(int i=0;i<newCatalogs.size();i++)
			{
				Catalog item = newCatalogs.get(i);
				//加载此目录下的所有文章列表
				News news = new News();
				news.setCatalogID(item.getId());
				news.setStatus("y");
				List<News> newsList = newsService.selectPlatList(news);
				item.setNews(newsList);
			}
		}
        return newCatalogs;
	}
	
	/**
	 * 商家的新闻列表
	 * @param model
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("news/list")
	public String newsList(ModelMap model, News e) throws Exception
	{
		String businessId=RequestHolder.getRequest().getParameter("businessId");
		if(StringUtils.isNotBlank(businessId))
		{
			//商铺的顶部图片
			TopPicture topPicture = new TopPicture();
			topPicture.setBusinessId(businessId);
			topPicture = topPictureService.selectFrontOne(topPicture);
			model.addAttribute("topPicture", topPicture);
			//商铺的店铺推荐
			BusinessRecommend  b= new BusinessRecommend();
			b.setBusinessId(businessId);
			b.setType(1);
	        List<BusinessRecommend> businessRecommends = businessRecommendService.selectList(b);
	        model.addAttribute("businessRecommends", businessRecommends);
		}
		else
		{
			businessId="";
		}
		String type=RequestHolder.getRequest().getParameter("type");
		e.setBusinessId(businessId);
		e.setType(type);
		PagerModel pager = selectPageList(getService(), e);
		pager.setPagerUrl("list");
		model.addAttribute("pager", pager);
		int productCount=productService.selectProductCount();
		model.addAttribute("e", e);
		model.addAttribute("productCount", productCount);
		model.addAttribute("businessId", businessId);
		model.addAttribute("type", type);
		//帮助支持列表
	     Help help = new Help();
	     help.setStatus("y");
	     List<Help> helpList = helpService.selectList(help);
	     model.addAttribute("helpList", helpList);
	     //商家的顶部图片
         TopPicture topPicture = new TopPicture();
         topPicture.setBusinessId(businessId);
         topPicture = topPictureService.selectFrontOne(topPicture);
         model.addAttribute("topPicture", topPicture);
         //商家的热门查询
       	Hotquery hotquery = new Hotquery();
       	hotquery.setBusinessId(businessId);
       	List<Hotquery> hotqueryList = hotqueryService.selectList(hotquery);
       	model.addAttribute("hotqueryList", hotqueryList);
		return "news/newsList";
	}
	
	/**
	 * 平台的文章列表
	 * @param model
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("platnews/list")
	public String platnewsList(ModelMap model, News e) throws Exception
	{
		String businessId=RequestHolder.getRequest().getParameter("businessId");
		String type=RequestHolder.getRequest().getParameter("type");
		if(businessId==null||"".equals(businessId))
		{
			businessId="";
		}
		e.setBusinessId(businessId);
		e.setType(type);
		int offset = 0;//分页偏移量
        if (RequestHolder.getRequest().getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(RequestHolder.getRequest().getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        e.setOffset(offset);
        PagerModel pager = platNewsService.selectPageList(e);
        if (pager == null) 
        {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)
                / pager.getPageSize());
        pager.setPagerUrl("list");
		model.addAttribute("pager", pager);
		int productCount=productService.selectProductCount();
		model.addAttribute("e", e);
		model.addAttribute("productCount", productCount);
		model.addAttribute("type", type);
		//加载平台推荐商品
      	PlatBusinessRecommend platBusinessRecommend = new PlatBusinessRecommend();
        platBusinessRecommend.setType(1);
        platBusinessRecommend.setTop(20);
        List<PlatBusinessRecommend> businessRecommends = platBusinessRecommendService.selectList(platBusinessRecommend);
	    model.addAttribute("businessRecommends", businessRecommends);
	    //热门查询
	  	Hotquery hotquery = new Hotquery();
	  	hotquery.setTop(6);
	  	List<Hotquery> hotqueryList = plathotqueryService.selectList(hotquery);
	  	model.addAttribute("hotqueryList", hotqueryList);
	    //加载大首页顶部图片
	  	TopPicture topPictureParam = new TopPicture();
	  	TopPicture topPicture = plattopPictureService.selectOne(topPictureParam);
	  	model.addAttribute("topPicture", topPicture);
		return "news/platnewsList";
	}
	
	/**
	 * 获取商家新闻详情
	 * @return
	 */
	@RequestMapping("news/{id}")
	public String newsInfo(@PathVariable("id")String id, ModelMap model) throws Exception
	{
		String businessId=RequestHolder.getRequest().getParameter("businessId");
		String type=RequestHolder.getRequest().getParameter("type");
		if(StringUtils.isNotBlank(businessId))
		{
			//商铺的顶部图片
			TopPicture topPicture = new TopPicture();
			topPicture.setBusinessId(businessId);
			topPicture = topPictureService.selectFrontOne(topPicture);
			model.addAttribute("topPicture", topPicture);
			//商铺的店铺推荐
			BusinessRecommend  b= new BusinessRecommend();
			b.setBusinessId(businessId);
			b.setType(1);
	        List<BusinessRecommend> businessRecommends = businessRecommendService.selectList(b);
	        model.addAttribute("businessRecommends", businessRecommends);
		}
		else
		{
			throw new NullPointerException("商家ID不可以为null!");
		}
		logger.error("NewsAction.newsInfo=== id="+id);
		if(StringUtils.isBlank(id))
		{
			throw new NullPointerException("文章ID不可以为null！");
		}
		News news =newsService.selectById(id);
		if(news==null)
		{
			throw new NullPointerException("商家情报不存在！");
		}
		
		String url = "/jsp/notices/"+news.getId()+".jsp";
		logger.error("url = " + url);
		news.setType(type);
		model.addAttribute("newsInfoUrl", url);
		model.addAttribute("news", news);
		News e=new News();
		e.setBusinessId(businessId);
		model.addAttribute("e", e);
	     //商家的顶部图片
         TopPicture topPicture = new TopPicture();
         topPicture.setBusinessId(businessId);
         topPicture = topPictureService.selectFrontOne(topPicture);
         model.addAttribute("topPicture", topPicture);
         //商家的热门查询
        Hotquery hotquery = new Hotquery();
       	hotquery.setBusinessId(businessId);
        List<Hotquery> hotqueryList = hotqueryService.selectList(hotquery);
        model.addAttribute("hotqueryList", hotqueryList);
        model.addAttribute("businessId", businessId);
        model.addAttribute("type", type);
		return "news/newsInfo";
	}
	
	
	/**
	 * 获取平台的新闻详情
	 * @return
	 */
	@RequestMapping("platnews/{id}")
	public String platnewsInfo(@PathVariable("id")String id, ModelMap model) throws Exception
	{
		logger.error("NewsAction.newsInfo=== id="+id);
		if(StringUtils.isBlank(id))
		{
			throw new NullPointerException("文章ID不可以为null!");
		}
		News news =platNewsService.selectPlatNewsById(id);
		if(news==null)
		{
			throw new NullPointerException("情报新闻不存在！");
		}
		
		String url = "/jsp/platnotices/"+news.getId()+".jsp";
		logger.error("url = " + url);
		model.addAttribute("newsInfoUrl", url);
		model.addAttribute("news", news);
		News e=new News();
		e.setBusinessId("");
		model.addAttribute("e", e);
	    //商品数量
		int productCount=productService.selectProductCount();
		model.addAttribute("productCount", productCount);
		//加载平台推荐商品
      	PlatBusinessRecommend platBusinessRecommend = new PlatBusinessRecommend();
        platBusinessRecommend.setType(1);
        platBusinessRecommend.setTop(20);
        List<PlatBusinessRecommend> businessRecommends = platBusinessRecommendService.selectList(platBusinessRecommend);
	    model.addAttribute("businessRecommends", businessRecommends);
	    //热门查询
	  	Hotquery hotquery = new Hotquery();
	  	hotquery.setTop(6);
	  	List<Hotquery> hotqueryList = plathotqueryService.selectList(hotquery);
	  	model.addAttribute("hotqueryList", hotqueryList);
	    //加载大首页顶部图片
	  	TopPicture topPictureParam = new TopPicture();
	  	TopPicture topPicture = plattopPictureService.selectOne(topPictureParam);
	  	model.addAttribute("topPicture", topPicture);
		return "news/platnewsInfo";
	}
	
	/**
	 * 帮助中心
	 * @return
	 */
	@RequestMapping("help/{helpCode}")
	public String help(@ModelAttribute("helpCode") @PathVariable("helpCode")String helpCode, ModelMap model) throws Exception 
	{
		logger.error("this.helpCode="+helpCode);
		int productCount=productService.selectProductCount();
		model.addAttribute("productCount", productCount);
		if(StringUtils.isBlank(helpCode))
		{
			return "help/help";
		}
		else if(helpCode.equals("index"))
		{
			return "help/help";
		}
		else
		{
			News newsParam = new News();
			newsParam.setCode(helpCode);
			News news = newsService.selectSimpleOne(newsParam);
			if(news==null)
			{
				throw new NullPointerException("根据code查询不到文章！");
			}
			model.addAttribute("news", news);
			//帮助支持列表
		     Help help = new Help();
		     help.setStatus("y");
		     List<Help> helpList = helpService.selectList(help);
		     model.addAttribute("helpList", helpList);
			return "help/help";
		}
	}
	
	/**
	 * 平台帮助中心
	 * @return
	 */
	@RequestMapping("plathelp/{helpCode}")
	public String plathelp(@ModelAttribute("helpCode") @PathVariable("helpCode")String helpCode, ModelMap model) throws Exception 
	{
		logger.error("this.helpCode="+helpCode);
		List<Product> productList = new ArrayList<Product>();
		Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
		CartInfo cartInfo = null;
		if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		int productCount=productService.selectProductCount();
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productCount", productCount);
		model.addAttribute("productList", productList);
		if(StringUtils.isBlank(helpCode))
		{
			return "help/help";
		}
		else if(helpCode.equals("index"))
		{
			return "help/help";
		}
		else
		{
			News newsParam = new News();
			newsParam.setCode(helpCode);
			News news = newsService.selectSimpleOne(newsParam);
			if(news==null)
			{
				throw new NullPointerException("根据code查询不到文章！");
			}
			model.addAttribute("news", news);
			//帮助支持列表
		     Help help = new Help();
		     help.setStatus("y");
		     List<Help> helpList = helpService.selectList(help);
		     model.addAttribute("helpList", helpList);
			return "help/help";
		}
	}
	
	/**
	 * 获取新闻详情
	 * @return
	 */
	@RequestMapping("blog/{id}")
	public String blog(@PathVariable("id")String id, ModelMap model) throws Exception
	{
		logger.error("NewsAction.newsInfo=== id="+id);
		if(StringUtils.isBlank(id))
		{
			throw new NullPointerException("id is null");
		}
		News news =newsService.selectById(id);
		if(news==null)
		{
			throw new NullPointerException();
		}
		
		String url = "/jsp/blog/"+news.getId()+".jsp";
		logger.error("url = " + url);
		model.addAttribute("newsInfoUrl", url);
		model.addAttribute("news", news);

		List<Product> productList = new ArrayList<Product>();
		Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
		CartInfo cartInfo = null;
		if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productList", productList);
		//帮助支持列表
	     Help help = new Help();
	     help.setStatus("y");
	     List<Help> helpList = helpService.selectList(help);
	     model.addAttribute("helpList", helpList);
		return "news/newsInfo";
	}
}
