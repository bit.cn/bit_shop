<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top1.ftl" as shopTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/bootstrap.min.css" type="text/css">
	<@htmlBase.htmlBase>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/main/index.css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/base.css" type="text/css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/css.css" />
	<link rel="stylesheet" href="${systemSetting().staticSource}/resource/validator-0.7.0/jquery.validator.css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
<style type="text/css">
				img {
				border: 0px;
			}
			
			.thumbnail_css {
				border-color: #f40;
			}
			
			.attr_css {
				font-size: 100%;
				float: left;
			}
			
			.left_product {
				font-size: 12px;
				max-height: 35px;
				overflow: hidden;
				text-overflow: ellipsis;
				-o-text-overflow: ellipsis;
			}
			
			.left_title {
				display: block;
				/* width: 280px; */
				overflow: hidden;
				white-space: nowrap;
				-o-text-overflow: ellipsis;
				text-overflow: ellipsis;
			}
			
			img.err-product {
				background: url(${systemSetting().defaultProductImg}) no-repeat 50% 50%;
				.lazy {
					display: none;
				}
				.centerImageCss {
					width: 289px;
					height: 190px;
				}
				.title {
					display: block;
					width: 280px;
					overflow: hidden;
					/*注意不要写在最后了*/
					white-space: nowrap;
					-o-text-overflow: ellipsis;
					text-overflow: ellipsis;
				}
				body {
					padding-top: 0px;
					padding-bottom: 0px;
					font-size: 12px;
					/*    	font-family: 微软雅黑, Verdana, sans-serif, 宋体; */
				}
				.hotSearch {
				cursor: pointer;
			}
		</style>
	<title>${news.title!""}</title>
</head>
<body>
<!--商城主页top页-->
<@shopTop.shopTop/>
<div id="wrap">
	<div class="container">
		<div class="row">
			<div class="col-xs-3">
				<#include "/catalog_superMenu.ftl">
				</br>
				<#include "/product/productlist_left_picScroll.ftl">
			</div>
			<div class="col-xs-9">
				<div class="row">
					<div class="col-xs-12">
						<ol class="breadcrumb">
						  <li><a href="${systemSetting().www}">首页</a></li>
						  <li><a href="${systemSetting().doc}/news/list">
						   <#if news.type?? && news.type='notice'>
						  	<a href="${systemSetting().doc}/news/list?type=notice&businessId=${businessId!""}">
						 		 最新情报
						  	</a>
						  	<#elseif news.type?? && news.type='blog'>
						 	 <a href="${systemSetting().doc}/news/list?type=blog&businessId=${businessId!""}">
						  	  官方日志
						  	</a>
						  	<#else>
						  	<a href="${systemSetting().doc}/news/list?type=notice&businessId=${businessId!""}">
						 		 最新情报
						  	</a>
						   </#if>
						  </a></li>
						  <li class="active">${news.title!""}</li>
						</ol>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						  	<h3><strong style="color:#dd4814;">${news.title!""}</strong></h3>
						  	<div style="text-align: right;font-size:16px;margin-right: 100px;"><small>${news.createAccount!""} 发布于:${news.createtime!""}</small></div>
						<hr style="margin-top: 5px;">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						${news.content!""}
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/hover.js"></script>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery.SuperSlide.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/new.js"></script>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript">
//搜索商品
function search(){
	var _key = $.trim($("#mq").val());
	if(_key==''){
		return false;
	}
$("#searchForm").submit();
}		
function defaultProductImg(){ 
	var img=event.srcElement; 
	img.src="${systemSetting().defaultProductImg}";
	img.onerror=null; //控制不要一直跳动 
}
function domore(){ 
$("#newType").show();   
}
$(function(){
$("#moreType").on("click",function(){
$("#newType").toggle();//显示隐藏切换
})
})

jQuery(".picScroll-top").slide({
				titCell: ".hd ul",
				mainCell: ".bd ul",
				autoPage: true,
				effect: "top",
				autoPlay: true,
				scroll: 2,
				vis: 5
			});
			jQuery(".slideTxtBox").slide();
			
			//搜索商品(整站搜索)
        function search() {
            var _key = $.trim($("#mq").val());
            if (_key == '') {
                return false;
            }
            $("#businessId").remove();
            $("#searchForm").attr("action","${systemSetting().search}/product/search.html")
            $("#searchForm").submit();
        }
        
        //搜索商品(本站搜索)
        function searchbusiness() {
            var _key = $.trim($("#mq").val());
            if (_key == '') {
                return false;
            }
            $("#searchForm").submit();
        }
        //热门推荐商品自动跳转
		jQuery(".picScroll-top").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,
				effect:"top",autoPlay:true,scroll:2,vis:5});
		jQuery(".slideTxtBox").slide();
</script>
</@htmlBase.htmlBase>
</body>
</html>
