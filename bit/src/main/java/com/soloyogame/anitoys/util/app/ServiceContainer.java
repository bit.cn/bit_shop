package com.soloyogame.anitoys.util.app;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;

import com.soloyogame.anitoys.util.FileTool;
import com.soloyogame.anitoys.util.XmlTool;


public class ServiceContainer 
{
public static Map<String,ServiceModel> serviceMap=new HashMap<String,ServiceModel>();
	
	public static ServiceModel getServiceModel(String serviceId)
	{
		return serviceMap.get(serviceId);
	}
	static
	{
		try 
		{
			init();
			inits();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void inits() throws Exception
	{
		String path=FileTool.getRealPath("/service/");
		ServiceModel service=new ServiceModel();
		ServiceParam param;
		List<Element> params;
		List<File>  files=FileTool.getfilelist(path);
		for(File f:files)
		{
			if(f.getName().contains("service-config")&&f.getName().endsWith("xml"))
			{
				Document doc=XmlTool.bulidXmlDocByFile(f.getPath());
				List<Element> services=doc.selectNodes("/config/services/service");
				for(Element e:services)
				{
					String url=e.getParent().attributeValue("url");
					service=new ServiceModel();
					service.setUrl(url);
					service.setId(e.attributeValue("id"));
					service.setName(e.attributeValue("name"));
					service.setBean(e.attributeValue("bean"));
					service.setMethod(e.attributeValue("method"));
					if(e.attributeValue("loggable")!=null)
					{
						service.setLogable(e.attributeValue("loggable").equals("true")?true:false);
					}
					params=new ArrayList();
					params=e.selectNodes("paramList/param");
					for(Element p:params)
					{
						param=new ServiceParam();
						param.setName(p.attributeValue("name"));
						String paramType=p.attributeValue("type");
						String[] paramTypes=paramType.split("\\|"); 
						if(paramTypes.length>1)
						{
							param.setType(Class.forName(paramTypes[0]));
							param.setListType(paramTypes[1]);
						}
						else
						{
							param.setType(Class.forName(p.attributeValue("type")));
						}
						service.addParam(param);
					}
					ServiceContainer.serviceMap.put(service.getId(), service);
				}
			}
		}
		
	}
	
	
	public static void  init() 
	{
		try
		{
			ServiceModel service=new ServiceModel();
			ServiceParam param;
			List<Element> params;
			String xmlPath=FileTool.getRealPath("/common/service-config.xml");
			Document doc=XmlTool.bulidXmlDocByFile(xmlPath);
			List<Element> services=doc.selectNodes("/config/services/service");
			for(Element e:services)
			{
				String url=e.getParent().attributeValue("url");
				service=new ServiceModel();
				service.setUrl(url);
				service.setId(e.attributeValue("id"));
				service.setName(e.attributeValue("name"));
				service.setBean(e.attributeValue("bean"));
				service.setMethod(e.attributeValue("method"));
				if(e.attributeValue("loggable")!=null)
				{
					service.setLogable(e.attributeValue("loggable").equals("true")?true:false);
				}
				params=new ArrayList();
				params=e.selectNodes("paramList/param");
				for(Element p:params)
				{
					param=new ServiceParam();
					param.setName(p.attributeValue("name"));
					String paramType=p.attributeValue("type");
					String[] paramTypes=paramType.split("\\|"); 
					if(paramTypes.length>1)
					{
						param.setType(Class.forName(paramTypes[0]));
						param.setListType(paramTypes[1]);
					}
					else
					{
						param.setType(Class.forName(p.attributeValue("type")));
					}
					service.addParam(param);
				}
			ServiceContainer.serviceMap.put(service.getId(), service);
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	}
	
	public static void main(String[] args) 
	{
		String s1="aa|bb";
		String[] s=s1.split("\\|");
		System.out.println(s);
	}
	

}
