package com.soloyogame.anitoys.util.qiniu;
import java.io.File;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.StringMap;

/**
 * 七牛的文件上传
 * @author shaojian
 */
public class QiniuUploadManager 
{
	/**
	 * 上传文件
	 * @return
	 */
	public static Response uploadFile(File f,String expectKey,String token,StringMap params)
	{
		UploadManager uploadManager = new UploadManager();
		Response response = null;
	    try 
	    {
	    	response = uploadManager.put(f, expectKey, token, params, null, true);
	    } 
	    catch (QiniuException e) 
	    {
			e.printStackTrace();
		}
		return response;
	}
	
	
	public static void main(String[] args)
	{
		//上传文件测试
		File f = new File("E:\\banner_04.jpg");
        assert f != null;
        String expectKey = "attached/image/20140321/banner_04.jpg";
        String token = TestConfig.testAuth.uploadToken(TestConfig.bucket, expectKey);
        StringMap params = new StringMap().put("x:foo", "foo_val");
        Response response = uploadFile(f, expectKey, token, params);
        try 
        {
			DefaultPutRet ret = response.jsonToObject(DefaultPutRet.class);
			StringMap m1 = response.jsonToMap();
        } 
        catch (QiniuException e) 
        {
			e.printStackTrace();
		}
	}
}
