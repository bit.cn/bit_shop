package com.soloyogame.anitoys.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesUtil {

	private static final Logger log = LoggerFactory.getLogger(PropertiesUtil.class);

	
	/**
	 * getProfilePath方法中会对此路径进行处理，加上profile的路径
	 * 如 
	 * 原路径为：config/abc.properties
	 */
	private static final String[] filterPath={"config/"};
	
	/**
	 * 用于缓存查询过的配置文件
	 */
	private static Map<String,Properties> proMap=new HashMap<String,Properties>();
	

	/**
	 * 获取profile对应的路径
	 * @param source
	 * @return
	 */
	public static String getProfilePath(String source){
		//source与mode都不为空
		if(StringUtils.isNotBlank(source)){
			for(String path:filterPath){
				//必须以此字符为前辍
				if(source.startsWith(path)){
					return source.replaceFirst(path, path+"/");
				}
			}
		}
		
		return source;
	}
	
	/**
	 * 通过路径返回properties文件,会进行profile处理
	 * @param source
	 * @return
	 */
	public static Properties findPro(String source){
		String path=getProfilePath(source);

		Properties pro=proMap.get(path);
		if(pro==null){
		
			InputStream is = ResourceUtil.getResourceAsStream(
					path, PropertiesUtil.class);
			
			 pro=new Properties();
			try {
				pro.load(is);
				proMap.put(path, pro);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return pro;
	}
	
	/**
	 * 通过路径返回properties文件,不会进行profile路径处理
	 * @param source
	 * @return
	 */
	public static Properties findCommonPro(String source){
		
		String path=source;
		
		Properties pro=proMap.get(path);
		if(pro==null){
		
			InputStream is = ResourceUtil.getResourceAsStream(
					path, PropertiesUtil.class);
			
			 pro=new Properties();
			try {
				pro.load(is);
				proMap.put(path, pro);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return pro;
	}
	
}
