package com.soloyogame.anitoys.util.cache;

import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis cache provider
 * @author shaijian
 */
public class RedisCacheProvider implements CacheProvider {
    private RedisTemplate<String, Serializable> redisTemplate;

    public RedisTemplate<String, Serializable> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<String, Serializable> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void deleteByPrex(String prex) {
        Set<String> keys = redisTemplate.keys("3*");
        redisTemplate.delete(keys);
    }

    public long delete(final Collection<String> keys) {
        return redisTemplate.execute(new RedisCallback<Long>() {
            public Long doInRedis(RedisConnection connection)
                    throws DataAccessException {
                long result = 0;
                for (String key : keys) {
                    result = connection.del(key.getBytes());
                }
                return result;
            }
        });
    }

    public void deleteBySuffix(String suffix) {
        Set<String> keys = redisTemplate.keys("*" + suffix);
        redisTemplate.delete(keys);
    }

    @Override
    public void put(final String key, final Serializable cacheObject) {
        redisTemplate.execute(new RedisCallback<Serializable>() {
            @Override
            public Serializable doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer<Serializable> value = (RedisSerializer<Serializable>) redisTemplate.getValueSerializer();
                connection.set(redisTemplate.getStringSerializer().serialize(key), value.serialize(cacheObject));
                return null;
            }
        });
    }

    @Override
    public Serializable get(final String key) {
        return redisTemplate.execute(new RedisCallback<Serializable>() {
            @Override
            public Serializable doInRedis(RedisConnection connection) throws DataAccessException {
                byte[] redisKey = redisTemplate.getStringSerializer().serialize(key);
                if (connection.exists(redisKey)) {
                    byte[] value = connection.get(redisKey);
                    Serializable valueSerial = (Serializable) redisTemplate.getValueSerializer().deserialize(value);
                    return valueSerial;
                }
                return null;
            }
        });
    }

    @Override
    public void remove(final String key) {
        redisTemplate.execute(new RedisCallback<Serializable>() {
            @Override
            public Serializable doInRedis(RedisConnection connection) throws DataAccessException {
                byte[] redisKey = redisTemplate.getStringSerializer().serialize(key);
                if (connection.exists(redisKey)) {
                    Long l = connection.del(redisKey);
                    return l;
                }
                return null;
            }
        });
    }

    /**
     * 清除所有缓存
     */
    @Override
    public void clear() {
        redisTemplate.execute(new RedisCallback<Serializable>() {
            @Override
            public Serializable doInRedis(RedisConnection connection)
                    throws DataAccessException {
                connection.multi();
                return null;
            }
        });
    }

    /**
     * 压栈
     *
     * @param key
     * @param cacheObject
     * @return
     */
    public Long push(String key, final Serializable cacheObject) {
        RedisSerializer<Serializable> valueSerializable = (RedisSerializer<Serializable>) redisTemplate.getValueSerializer(); //序列化value
        RedisSerializer<String> stringSerializable = redisTemplate.getStringSerializer();                                     //序列化key
        return redisTemplate.opsForList().leftPush(key, valueSerializable.serialize(cacheObject));
    }

    /**
     * 出栈
     *
     * @param key
     * @return
     */
    public Serializable pop(String key) {
        return redisTemplate.opsForList().leftPop(key);
    }

    /**
     * 入队
     *
     * @param key
     * @param cacheObject
     * @return
     */
    public Long in(String key, final Serializable cacheObject) {
        RedisSerializer<Serializable> valueSerializable = (RedisSerializer<Serializable>) redisTemplate.getValueSerializer(); //序列化value
        return redisTemplate.opsForList().rightPush(key, valueSerializable.serialize(cacheObject));
    }

    /**
     * 出队
     *
     * @param key
     * @return
     */
    public Serializable out(String key) {
        RedisSerializer<Serializable> valueSerializable = (RedisSerializer<Serializable>) redisTemplate.getValueSerializer(); //序列化value
        return redisTemplate.opsForList().leftPop(key);
    }

    /**
     * 栈/队列长
     *
     * @param key
     * @return
     */
    public Long length(String key) {
        return redisTemplate.opsForList().size(key);
    }

    /**
     * 范围检索
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    public List<Serializable> range(String key, int start, int end) {
        return redisTemplate.opsForList().range(key, start, end);
    }

    /**
     * 检索
     *
     * @param key
     * @param index
     * @return
     */
    public Serializable index(String key, long index) {
        return redisTemplate.opsForList().index(key, index);
    }

    /**
     * 置值
     *
     * @param key
     * @param index
     * @param value
     */
    public void set(String key, long index, Serializable cacheObject) {
        RedisSerializer<Serializable> valueSerializable = (RedisSerializer<Serializable>) redisTemplate.getValueSerializer(); //序列化value
        redisTemplate.opsForList().set(key, index, valueSerializable.serialize(cacheObject));
    }

    /**
     * 裁剪
     *
     * @param key
     * @param start
     * @param end
     */
    public void trim(String key, long start, int end) {
        redisTemplate.opsForList().trim(key, start, end);
    }

    /**
     * 写入缓存
     *
     * @param key
     * @param value
     * @return
     */
    public boolean set(final String key, Object value, Long expireTime) {
        boolean result = false;
        try {
            ValueOperations operations = redisTemplate.opsForValue();
            operations.set(key, value);
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}