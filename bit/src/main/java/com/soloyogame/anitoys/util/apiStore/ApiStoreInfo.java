package com.soloyogame.anitoys.util.apiStore;

/**
 * 百度快递查询
 * @author Administrator
 */
public class ApiStoreInfo {

    private String status;//状态(0.成功 205.没有信息)
    private String msg;//消息体
    private ApiStoreItem result;//数据体

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ApiStoreItem getResult() {
        return result;
    }

    public void setResult(ApiStoreItem result) {
        this.result = result;
    }
}
