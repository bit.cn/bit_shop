package com.soloyogame.anitoys.util;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * cookie设置工具类
 * @author shaojian
 */
public class CookieTool 
{
	private Cookie cookie;
	private int time=60*60; 			//默认1小时
	private String path="/";			//默认"/"
	private HttpServletRequest request;
	private HttpServletResponse response;
	private String domain=".anitoys.com";
	
	public CookieTool(HttpServletRequest request,HttpServletResponse response)
	{
		this.setRequest(request);
		this.setResponse(response);
	}
	
	//一般方法增加cookie
	public  void addCookie(String cookiename,String cookievalue)
	{
		    cookie=new Cookie(cookiename,cookievalue);
		    cookie.setPath(path);
		    cookie.setMaxAge(time);
		    cookie.setDomain(domain);
		    response.addCookie(cookie);
	}
	
	//一般方法获取cookie值
	public  String getCookieByname(String name)
	{
        Cookie sCookie=null;
        String svalue=null;
        String sname=null;
	    Cookie[] cookies=request.getCookies();
	    for(int i=0;i<cookies.length;i++)
        { 
        sCookie=cookies[i];
        sname=sCookie.getName();
	    if(sname.equals(name)) 
	    {
	    	svalue=sCookie.getValue();
	    	break;
	    }
	    }
	    return svalue;

}
	//静态方法新增cookie
	public static void setCookie(HttpServletResponse response,String cookiename,String cookievalue)
	{
	    Cookie cookie=new Cookie(cookiename,cookievalue);
	    cookie.setPath("/");
	    cookie.setMaxAge(60*60);//不设置的话，则cookies不写入硬盘,而是写在内存,只在当前页面有用,以秒为单位
	    cookie.setDomain(".anitoys.com");
	    response.addCookie(cookie);
    }
	
	//静态方法获取cookie
	public static String getCookieByname(Cookie[] cookies,String name)
	{
            Cookie sCookie=null;
		    String svalue=null;
		    String sname=null;
		    for(int i=0;i<cookies.length;i++)
		    { 
	            sCookie=cookies[i];
	            sname=sCookie.getName();
			    if(sname.equals(name)) {
		    	svalue=sCookie.getValue();
		    	break;
		    }
		}
		    return svalue;
	}
	
	//静态方法获取cookie
	public static String getCookieByname(HttpServletRequest request,String name)
	{
			Cookie[] cookies=request.getCookies();
            Cookie sCookie=null;
		    String svalue=null;
		    String sname=null;
            if(cookies!=null){
		    for(int i=0;i<cookies.length;i++)
		    { 
	            sCookie=cookies[i];
	            sname=sCookie.getName();
			    if(sname.equals(name)) 
			    {
			    	svalue=sCookie.getValue();
				    	 break;
			    }
		    }     
            }
		    return svalue;
	
	}
	
	public static  void clearCookie(HttpServletRequest request,HttpServletResponse response)
	{
		Cookie[] cookies=request.getCookies();
		if(cookies!=null){
		for(int i=0;i<cookies.length;i++)
		{
			cookies[i].setMaxAge(0);
			response.addCookie(cookies[i]);
		}
	}
	}
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	public void setCookie(Cookie cookie) {
		this.cookie = cookie;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public void setPath(String path) {
		this.path = path;
	}

}
