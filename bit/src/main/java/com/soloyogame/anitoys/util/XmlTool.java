package com.soloyogame.anitoys.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 * 使用基于dom4j的类库来操作xml
 * @author shaojian
 */
public class XmlTool {

	public static Document bulidXmlDocByFile(String filePath) {
		Document doc =null;
		FileInputStream in=null;
		InputStreamReader isr=null;
		BufferedReader reader=null; 
		try{
		 in = new FileInputStream(filePath);
		 isr=new InputStreamReader(in,FileTool.getEncode(filePath));
		 reader = new BufferedReader(isr);
		 String tempStr;
		 String xmlstr = "";
		while ((tempStr = reader.readLine()) != null) {
			xmlstr = xmlstr + tempStr;
		}		
		doc = DocumentHelper.parseText(xmlstr);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			if(reader!=null){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(in!=null){
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(isr!=null){
				try {
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return doc;
	}

	public static Document bulidXmlDoc(String xmlStr) throws Exception {
		Document doc = DocumentHelper.parseText(xmlStr);
		return doc;
	}

	public static void writeXmlToFlie(Document doc, String fileName,String encoding) {
		OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding(encoding);

        try {
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(fileName),format);
			xmlWriter.write(doc);
			xmlWriter.close();
		 } catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static String buildXml(HttpServletRequest request){
		 StringBuffer xml=new StringBuffer();
		 Enumeration  names=request.getParameterNames();
			
		 xml.append("<?xml version=\"1.0\" standalone=\"yes\"?>");
		 xml.append("<request>");
		 String name;
		 while(names.hasMoreElements()){
			 name=names.nextElement().toString();
			 xml.append("<"+name+">");
			 xml.append(request.getParameter(name));
			 xml.append("</"+name+">");
		 }
		 xml.append("</request>");
		return xml.toString();
	}
	public static void main(String[] args) throws Exception {
		for(int i=0;i<10;i++){
		//String xml="<?xml version='1.0' ?><root><node1 age=\"3\" name=\"汉字\"></node1></root>";
		//Document doc= XmlTool.bulidXmlDoc(xml);
		//doc.selectSingleNode("//root/node1[@name='n22']").setText("add by mac");
		//XmlTool.writeXmlToFlie(doc, "D://2.xml","UTF-8");
		String filePath=XmlTool.class.getResource("/template/test.xml").getPath();
		Document doc=XmlTool.bulidXmlDocByFile(filePath);
		//Element e=(Element)doc.selectSingleNode("/TXLife/TXLifeRequest/OLifE/Holding/Policy");
		String no=doc.selectSingleNode("/TXLife/TXLifeRequest/TransRefGUID").getText();
		//String id=e.attributeValue("id");
		XmlTool.writeXmlToFlie(doc, "c://text.xml", "UTF-8");
		//System.out.println(no);
		}

		
	}

}
