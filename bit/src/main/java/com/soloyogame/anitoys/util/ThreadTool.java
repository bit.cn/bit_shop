package com.soloyogame.anitoys.util;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Properties;

import com.soloyogame.anitoys.util.email.Mail;

/**
 * 线程工具类
 * @author 索罗游
 */
public class ThreadTool extends Thread implements Runnable 
{
	private int runtimes = 1;            //执行次数,当runtime<0时表示不终止
	private String classname;            //线程需要操作的类
	private String method;               //线程需要执行的方法
	private Class[]  methodVarTypes=null;
	private Object[] methodVarValues=null;
	private int pauseTime=0;
	public  int PTIMES = 0;
	private String ThreadName="MacThread";

   public  void  run() 
   {
		if (this.getRunTimes() > 0)
		{
			while (PTIMES < this.getRunTimes()) 
			{
				PTIMES++;
				this.executeMethod();
			}
		} 
		else 
		{
			while (true) 
			{
				this.executeMethod();
			}
		}

	}

	public void executeMethod() 
	{
		
		Object obj;
		try 
		{
			obj = Class.forName(this.getClassname()).newInstance();
			Class c = obj.getClass();
			Method method = c.getDeclaredMethod(this.getMethod(),this.getMethodVarTypes());
			method.invoke(obj,this.getMethodVarValues());
		 } 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
   
	public void start(){
	   new Thread(this).start();
	}
	public String getClassname() {
		return classname;
	}

	public void setRunClassName(String classname) {
		this.classname = classname;
	}

	public String getMethod() {
		return method;
	}

	public void setExeMethod(String method) {
		this.method = method;
	}

	public void setRunTimes(int runtimes) {
		this.runtimes = runtimes;
	}

	public int getRunTimes() {
		return runtimes;
	}

	public int getPauseTime() {
		return pauseTime;
	}

	public void setPauseTime(int pauseTime) {
		this.pauseTime = pauseTime;
	}

	public String getThreadName() {
		return ThreadName;
	}

	public void setThreadName(String threadName) {
		ThreadName = threadName;
	}

	public Class[] getMethodVarTypes() {
		return methodVarTypes;
	}

	public void setMethodVarTypes(Class[] methodVarTypes) {
		this.methodVarTypes = methodVarTypes;
	}

	public Object[] getMethodVarValues() {
		return methodVarValues;
	}

	public void setMethodVarValues(Object[] methodVarValues) {
		this.methodVarValues = methodVarValues;
	}
	public static void main(String[] args) 
	{
		ThreadTool thread = new ThreadTool();
		thread.setRunClassName("com.soloyogame.anitoys.util.email.SendCloud");
		thread.setExeMethod("send_template");
		Class[] classes=new Class[]{Mail.class};
		Mail mail = new Mail();
		mail.setSendTime(new Date());
		
		Properties  props = PropertiesUtil.findCommonPro("config.properties");
		final String url = props.getProperty("mailUrl");
		final String apiUser = props.getProperty("apiUser");
		final String apiKey = props.getProperty("apiKey");
		final String from = props.getProperty("from");
		final String fromname = props.getProperty("fromname");
		final String templateInvokeName = props.getProperty("templateInvokeName");
		
		mail.setUrl(url);
		mail.setApiKey(apiKey);        
		mail.setApiUser(apiUser);
		mail.setSender(from);
		mail.setName(fromname);//发件人地址
		mail.setTemplateInvokeName(templateInvokeName);
		mail.setReceiver("3114807004@qq.com");
		
		Object[] objs=new Object[]{mail};
		thread.setMethodVarTypes(classes); //设置现成方法参数类型
		thread.setMethodVarValues(objs);   //设置现成方法参数值
		thread.setThreadName("thread_1");
		thread.start();//启动线程
		

	}

}
