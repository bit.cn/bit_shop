package com.soloyogame.anitoys.util.app;

public class ServiceParam {
	
	public String name;
	public Class  type;
	public Object value;
	public String listType;
	public ServiceParam(){

	}
	public ServiceParam(String name,Object value){
		this.name=name;
		this.value=value;
		this.type=value.getClass();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Class getType() {
		return type;
	}
	public void setType(Class type) {
		this.type = type;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value; 
	}
	public String getListType() {
		return listType;
	}
	public void setListType(String listType) {
		this.listType = listType;
	}


}
