package com.soloyogame.anitoys.util;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;

/**
 * FastJson 序列化工具类
 * @author shaojian
 */
public class FastJsonUtil {

	private static JSON json = null;
	
//	static 
//	{
//		if (json == null) 
//		{
//			json = new JSON();
//		}
//	}
	
	/**
	 * 将对象转换成json格式
	 * @param ts
	 * @return
	 */
	public static String objectToJson(Object ts) 
	{
		String jsonStr = null;
		jsonStr = JSON.toJSONString(ts);
		return jsonStr;
	}

	/**
	 * turn json to object
	 * @param ts
	 * @return
	 */
	public static Object jo(String json)
	{
		return JSON.parse(json);
	}
	/**
	 * 主函数
	 * @param args
	 */
	public static void main(String[] args)
	{
	}
}
