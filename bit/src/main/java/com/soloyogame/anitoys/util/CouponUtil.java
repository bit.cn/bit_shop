package com.soloyogame.anitoys.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 优惠券编码工具类
 * @author 索罗游
 */
public class CouponUtil {

	/**
	 * 优惠券编码
	 * @param businessId   商家ID
	 * @param couponCount  优惠券发送数量
	 * @param i            优惠券序号
	 * @return
	 */
	public static String getCouponSnNum(String businessId,int couponCount,int i){
		int length1=0;
		int length2=0;
		int b1=1;
		int b2=1;
		int a1=0;
		int a2=0;
		for(int n=1;n<couponCount;n++){
		   b1=b1*10;
		   if(couponCount/b1==0){
		       length1=a1;
		       break;
		    }
		   else{
			   a1++;
		   }
		}
		
		for(int n=1;n<i;n++){
			   b2=b2*10;
			   if(i/b2==0){
			       length2=a2;
			       break;
			    }
			   else{
				   a2++;
			   }
			}
		
		
		String o = "";
		for(int m=0;m<length1-length2;m++){
			o=o+"0";
		}
		String couponSnNum = null;
		SimpleDateFormat dateFm = new SimpleDateFormat("yyyyMMddHH"); //格式化当前系统日期
		String dateTime = dateFm.format(new Date());
		couponSnNum = businessId+dateTime+o+i;
		return couponSnNum;
	}
	
	/**
	 * 测试主类
	 * @param args
	 */
	public static void main(String args[]){
		
		String s = getCouponSnNum("businessId",100, 6);
		System.out.println("s:"+s);
	}
}
