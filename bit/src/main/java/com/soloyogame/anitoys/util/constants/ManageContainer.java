package com.soloyogame.anitoys.util.constants;

/**
 * 全局变量
 *
 * @author shaojian
 */
public class ManageContainer {

    public static final String MENU_list = "menu_list";// 菜单信息
    public static final String MENUS = "menus";
    public static final String PROJECTS = "projects";
    public static final String USERS = "users";
    public static final String loginError = "loginError";//登陆错误

    public static final int PAGE_SIZE = 20;// 默认分页条数

    /**
     * 商品目录树
     */
    public static final String typeTree = "typeTree";

    public static final String news = "news";//内容key
    public static final String lable = "lable";//标签KEY
    public static final String sessionID = "sessionID";//sessionIDKEY
    public static final String imgList = "imgList";
    public static final String resource_menus = "resource_menus";                            //用户资源菜单
    public static final String user_resource_menus_button = "user_resource_menus_button";    //用户资源功能菜单
    public static final String SystemSetting = "SystemSetting";                              //资源菜单
    public static final String center_session_user_info = "center_session_user_info";	     //平台的用户信息
    public static final String manage_session_user_info = "manage_session_user_info";        //用户信息
    public static final String manage_session_business_info = "manage_session_business_info";//商家的信息

    /**
     * 登录的用户信息
     */
    public static String session_account_info = "session_account_info";
    //当前门户上的菜单
    public static final String current_menu = "current_menu";
    //我的二维码模块的子项菜单
    public static final String current_item_menu = "current_item_menu";
    public static final String product_images_spider = ",";            	   //商品图片分割符

    //	public static final String user_db_privilege = "user_db_privilege";//用户数据库权限
    public static final String db_privilege_select = "db_privilege_select";//数据库权限-查询
    public static final String db_privilege_insert = "db_privilege_insert";//数据库权限-添加
    public static final String db_privilege_update = "db_privilege_update";//数据库权限-修改
    public static final String db_privilege_delete = "db_privilege_delete";//数据库权限-删除

    public static final String db_privilege_insert_error = "权限受限：该用户没有添加数据的权限！";//数据库权限-添加-错误信息
    public static final String db_privilege_update_error = "权限受限：该用户没有修改数据的权限！";//数据库权限-修改-错误信息
    public static final String db_privilege_delete_error = "权限受限：该用户没有删除数据的权限！";//数据库权限-删除-错误信息

    public static final String action_db_error = "action_db_error";//action操作DB异常信息
    public static final String action_exception_error = "action_exception_error";//action异常的错误信息
    public static final String action_exception_stack_error = "action_exception_stack_error";//action异常栈的错误信息

    /**
     * 数据库操作
     */
    public static final String db_insert = "insert";
    public static final String db_update = "update";
    public static final String db_delete = "delete";

    public static final String not_this_method = "不支持此操作!";

    /**
     * 错误编码规则：ERROR_action名称_方法名称_错误操作
     */
    public static final String OrderAction_param_null = "参数非法！";
    public static final String OrderAction_selectById_null = "根据订单ID查询不到订单信息！";
    public static final String OrderAction_updatePayMonery_cancel = "操作非法:未审核和已审核的订单才能取消！";
    public static final String OrderAction_updatePayMonery_update = "操作非法：未审核的订单才能进行此操作！";
    public static final String OrderAction_updateOrderStatus_alreadyCancel = "操作非法:订单已处于取消状态！";
    public static final String OrderAction_updateOrderStatus_statusException = "操作非法:订单状态异常！";

    public static final String RoleAction_update_error = "非法操作！";

    //redis缓存商品信息的key
    public static final String product_xdkc = "_productspec_xdkc_";   							//下单库存的(productId加载下划线的前面，规格ID加载后面一个下划线)
    public static final String product_zfkc = "_productspec_zfkc_";   							//支付库存(productId加载下划线的前面，规格ID加载后面一个下划线)
    public static final String product_info = "_product";                						//商品基础信息
    public static final String product_xgsl = "_product_xgsl";       						    //限购数量
    public static final String product_spjs = "_product_spjs";        							//商品介绍
    public static final String account_product_xgsl = "_account_xgsl_product_";					//用户的限购数量(前面加用户ID后面加商品ID)

    public static final String PRODUCT_ATTRIBUTES = "_attributes";								//单个商品的所有属性   商品Id_attributes
    public static final String PRODUCT_BRAND = "_brand";										//单个品牌    品牌Id_brand
    public static final String PRODUCT_BUSINESSSHIPPING = "_business_shipping"; 				//商家支持的配送信息    品牌Id_brand
    public static final String PRODUCT_BUSINESS = "_business"; 									//商品所属店铺信息   BusinessId_business
    public static final String PRODUCT_BUSINESSRECOMMENDS = "_business_recommend"; 				//店铺推荐商品  BusinessId_recommend
    public static final String PRODUCT_SERVICEGUARANTEE = "_service_guarantee"; 				//商铺售后保障  BusinessId_service_guarantee
    public static final String PRODUCT_ATTRIBUTE_BUSINESS = "_product_attribute_business"; 		//产品属性  BusinessId_service_guarantee
    public static final String PRODUCT_COMMENT = "_product_comment"; 							//指定商品的评论  BusinessId_product_comment
    public static final String PRODUCT_ATTRIBUTELINKLIST = "_product_attributelinklist_"; 		//商品属性 businessId _product_attributelinklist_catalogId
    public static final String PRODUCT_ATTRIBUTELIST_CATALOG = "_product_attributelinklist"; 	//商品参数  productId_product_attributelinklist
    public static final String PRODUCT_CATALOG = "_product_catalog"; 							//商品目录  productId_product_catalog
    public static final String PRODUCT_SPEC = "_product_spec_";      							//商品规格  productId_product_spec_specId

    public static final String PRODUCT_TOPPICTURE = "_product_toppicture"; //顶部图片  BusinessId_product_spec

    public static final String CACHE_BUSINESSLIST = "cache_businesslist";  // 商家列表

    public static final String CACHE_NEWSCATALOGS = "cache_newscatalogs";  // 文章目录

    public static final String CACHE_HELPLIST = "cache_helplist";          // 帮助支持列表
    
    public static final String CACHE_SYSTEM_SETTING = "cache_systemSetting";           //系统设置
    
    public static final String CACHE_NEW_PRODUCTS = "cache_newProducts";               //最新商品
    
    public static final String CACHE_PRESALES_PRODUCTLIST = "cache_preSalesProductList";//预售商品列表
    
    public static final String CACHE_SPOTCOMMONITY_PRODUCTLIST = "cache_spotCommodityProductList";//现货商品列表
    
    public static final String CACHE_CATALOGS = "cache_catalogs";                       //商品目录

    public static final String CACHE_HOTQUERY_LIST   = "cache_hotqueryList";            //热门查询列表
    
    public static final String CACHE_INDEX_IMAGES="cache_index_images";                 //大首页轮播图
    
    public static final String CACHE_NOTICE_LIST = "cache_notice_list";                 //最新情报列表
    
    public static final String CACHE_NAVIGATION_PLAT = "cache_navigations_plat";        //平台的友情链接
    
    public static final String CACHE_TOPPICTURE_PLAT = "cache_toppicture_plat";         //平台顶部图片
    
    public static final String CACHE_STRONGADVERTISSING_LIST = "cache_strong_advertising_list";//强推广告列表
    
    public static final String CACHE_POPULARITY_PRODUCT_PLAT_LIST = "cache_popularity_product_plat_list";//人气商品列表
    
    public static final String CACHE_POPULARITY_ADVERT = "cache_popularity_avert";          //人气商品广告位
    
    public static final String CACHE_KEYVALUE_LIST = "manager_keyvalue_list";               //键值对
    
    public static final String COUPON_BINDING="_coupon_binding_";                           //绑定优惠券的标记key
    
    public static final String VOUCHER_BINGING="_voucher_binding_";                         //绑定抵扣券标记key
    
    public static final String PARENT_ORDER = "parentOrder";                                //虚拟订单的key
    
    public static final String ORDER_COUPON="_order_coupon";                                //订单绑定的优惠券标记
    
    public static final String ORDER_VOUCHER="_order_voucher";                              //订单绑定的抵扣券标记
    
    public static final String ORDER_RANK="_order_rank_";                                   //订单绑定的积分说标记
    
    //补款发送邮件的标记
    public static final String PRODUCT_BK_EMAIL_KEY="product_bk_email_key_"; //补款发送给邮件的KEY
}
