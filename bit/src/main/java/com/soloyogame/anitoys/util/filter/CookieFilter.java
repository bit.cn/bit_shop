package com.soloyogame.anitoys.util.filter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.soloyogame.anitoys.util.CookieTool;

/**
 * 拦截器设置cookie信息
 * @author shaojian
 */
public class CookieFilter implements Filter 
{  
    public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException 
    {  
        HttpServletRequest req = (HttpServletRequest) request;  
        HttpServletResponse resp = (HttpServletResponse) response;  
        Cookie[] cookies = req.getCookies();  
        int time=60*60; 			//默认1小时
    	String path="/";			//默认"/"
    	String domain=".anitoys.com";
        if (cookies != null) 
        {  
        	for(int i=0;i<cookies.length;i++)
        	{
        		Cookie  cookie = cookies[i];
                if (cookie != null) 
                {  
                	cookie.setPath(path);
         		    cookie.setMaxAge(time);
         		    cookie.setDomain(domain);
                    cookie.setSecure(true); 
                    resp.addCookie(cookie);
                    //Servlet 2.5不支持在Cookie上直接设置HttpOnly属性  
                    String value = cookie.getValue();  
                    StringBuilder builder = new StringBuilder();  
                    builder.append("JSESSIONID=" + value + "; ");  
                    builder.append("Secure; ");  
                    builder.append("HttpOnly; ");  
                    Calendar cal = Calendar.getInstance();  
                    cal.add(Calendar.HOUR, 1);  
                    Date date = cal.getTime();  
                    Locale locale = Locale.CHINA;  
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",locale);  
                    builder.append("Expires=" + sdf.format(date));  
                    resp.setHeader("Set-Cookie", builder.toString());  
                }  
        	}
        } 
        CookieTool.setCookie(resp, "haha", "he");
        chain.doFilter(req, resp);  
    }  
  
    public void destroy() {  
    }  
  
	@Override
	public void init(FilterConfig filterConfig) throws ServletException 
	{
	}  
}  
