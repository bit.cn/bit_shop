package com.soloyogame.anitoys.util.apiStore;

import java.util.List;
import java.util.Map;

/**
 * 百度快递查询
 *
 * @author Administrator
 *
 */
public class ApiStoreItem {

    private List<Map<String, Object>> list;//数据集合(key:time   key:status)
    private String issign;//是否签收(0.未签收   1.已签收)

    public List<Map<String, Object>> getList() {
        return list;
    }

    public void setList(List<Map<String, Object>> list) {
        this.list = list;
    }

    public String getIssign() {
        return issign;
    }

    public void setIssign(String issign) {
        this.issign = issign;
    }

}
