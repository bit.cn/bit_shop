package com.soloyogame.anitoys.util.app;


/**
 * 数据容器
 * @author Mac
 */
public class DataContainer {
	
	private String formId;
	private String queryId;
	private String rowNum;
	private String dataPanel;
	private String dataView;
	private String pageNum;
	private String pageNo;
	public 	String totalNum;
	private String condition;
	private String listHandler;
	private String queryBean;
	private String dataContainerId;
	
	public String getFormId() {
		return formId;
	}
	
	
	public void setFormId(String formId) {
		this.formId = formId;
	}
	
	
	public String getQueryId() {
		return queryId;
	}
	
	
	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}
	
	
	public String getRowNum() {
		return rowNum;
	}
	
	
	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}
	
	
	public String getDataPanel() {
		return dataPanel;
	}
	
	
	public void setDataPanel(String dataPanel) {
		this.dataPanel = dataPanel;
	}
	
	
	public String getDataView() {
		return dataView;
	}
	
	
	public void setDataView(String dataView) {
		this.dataView = dataView;
	}


	public String getPageNum() {
		return pageNum;
	}


	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}


	public String getPageNo() {
		return pageNo;
	}


	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}


	public String getCondition() {
		return condition;
	}


	public void setCondition(String condition) {
		this.condition = condition;
	}


	public String getListHandler() {
		return this.listHandler;
	}


	public void setListHandler(String listHandler) {
		this.listHandler = listHandler;
	}


	public String getQueryBean() {
		return this.queryBean;
	}


	public void setQueryBean(String queryBean) {
		this.queryBean = queryBean;
	}


	public String getDataContainerId() {
		return this.dataContainerId;
	}


	public void setDataContainerId(String dataContainerId) {
		this.dataContainerId = dataContainerId;
	}


	public String getTotalNum() {
		return this.totalNum;
	}


	public void setTotalNum(String totalNum) {
		this.totalNum = totalNum;
	}
	
}
