package com.soloyogame.anitoys.util.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soloyogame.anitoys.util.XstreamTool;

public class ServiceResult 
{
	private String serviceId;
	private boolean result;
	private String resultInfo;
	private Map params=new HashMap();
	public List<ServiceObject>  paramList=new ArrayList();
	public String toXml(){
		return XstreamTool.toXml(this);
	}
	
	public void put(String key,Object value){
		params.put(key, value);
	
	}
	public <T>T get(String key){
		return (T)params.get(key);
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public boolean isSuccess() { 
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public Map getParams() {
		return params;
	}
	public void setParams(Map params) {
		this.params = params;
	}
	public static void main(String[] args) {
		System.out.print(	boolean.class.getName());
	}
	public String getResultInfo() {
		return resultInfo;
	}
	public void setResultInfo(String resultInfo) {
		this.resultInfo = resultInfo;
	}

	
	public List<ServiceObject> getParamList() {
		return paramList;
	}

	public void setParamList(List<ServiceObject> paramList) {
		this.paramList = paramList;
	}

	public boolean isResult() {
		return result;
	}

}
