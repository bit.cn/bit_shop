package com.soloyogame.anitoys.util.usercenter;

import java.io.IOException;

import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

/**
 * 用户中心登录工具类
 * @author shaojian
 */
public class UserInfoUtil 
{
	/*@Autowired
	private static RedisCacheProvider redisCacheProvider; */
		//手机端
		public static HttpMethod loginForApp(String url,String mobile,String password) throws IOException
		{
	        PostMethod post = new PostMethod(url);
	        post.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=gbk"); 
	        NameValuePair[] param = { new NameValuePair("client_id","mobile"),
	                new NameValuePair("client_secret","mobile"),
	                new NameValuePair("grant_type","password"),
	                new NameValuePair("scope","read write"),
	                new NameValuePair("username","default"),
	                new NameValuePair("password",password),
	                //new NameValuePair("email","111111"),      
	                new NameValuePair("mobile",mobile)
	                } ;
	        post.setRequestBody(param);
	        post.releaseConnection();
	        return post;
	    }
		
		//pc卖家
		public static HttpMethod loginForUser(String url,String username,String password,String email,String mobile) throws IOException
		{
			if(username==null){
				username="default";
			}
			PostMethod post = new PostMethod(url);
			post.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=gbk"); 
			NameValuePair[] param = { new NameValuePair("client_id","seller"),
					new NameValuePair("client_secret","seller"),
					new NameValuePair("grant_type","password"),
					new NameValuePair("scope","read write"),
					new NameValuePair("username",username),
					new NameValuePair("password",password),
					new NameValuePair("email",email), 
					new NameValuePair("mobile",mobile)
			} ;
			post.setRequestBody(param);
			post.releaseConnection();
			return post;
		}
		//pc平台
		public static HttpMethod loginForUserPlat(String url,String username,String password,String email,String mobile) throws IOException
		{
			if(username==null){
				username="default";
			}
			PostMethod post = new PostMethod(url);
			post.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=gbk"); 
			NameValuePair[] param = { new NameValuePair("client_id","plat"),
					new NameValuePair("client_secret","plat"),
					new NameValuePair("grant_type","password"),
					new NameValuePair("scope","read write"),
					new NameValuePair("username",username),
					new NameValuePair("password",password),
					new NameValuePair("email",email), 
					new NameValuePair("mobile",mobile)
			} ;
			post.setRequestBody(param);
			post.releaseConnection();
			return post;
		}
		
		//pc买家
		public static HttpMethod loginForAccount(String url,String username,String password,String email,String mobile) throws IOException
		{
			if(username==null){
				username="default";
			}
			PostMethod post = new PostMethod(url);
	        post.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=gbk"); 
	        NameValuePair[] param = { new NameValuePair("client_id","buyer"),
	                new NameValuePair("client_secret","buyer"),
	                new NameValuePair("grant_type","password"),
	                new NameValuePair("scope","read write"),
	                new NameValuePair("username",username),
	                new NameValuePair("password",password),
	                new NameValuePair("email",email),      
	                new NameValuePair("mobile",mobile)
	                } ;
	        post.setRequestBody(param);
	        post.releaseConnection();
	        return post;
		}
		/**
		 * 参数: id:用户id  type:pc(pc前端) app(app端) user(pc后台)
		 * 返回值:0.未请求过用户中心   1.用户数据正常  2.用户登录已失效,请重新登录
		 */
		/*public static int chechkUser(String id,String type){
			String account=(String) redisCacheProvider.get(type+"account_"+id);
			if(StringTool.isNull(account)){
				return 0;
			}
			Map accountMap=JsonUtil.jsonToMap(account);
			long addTime=Long.parseLong((String) accountMap.get("addTime"));//登录时间
			long expires=Long.parseLong((String) accountMap.get("expires_in"));//过期时效
			 Date timeDate = new Date();
		     long t = timeDate.getTime();//当前时间
		     if(t<=addTime+expires){
		    	 return 2;    	 
		     }
		     return 1;
		}*/
}
