package com.soloyogame.anitoys.util.app;

import java.util.ArrayList;
import java.util.List;

import com.soloyogame.anitoys.util.HttpClient;
import com.soloyogame.anitoys.util.XstreamDateConverter;
import com.soloyogame.anitoys.util.XstreamTool;

public class ServiceRequest {
	private String serviceId;
	private List<ServiceParam> params=new ArrayList();
	private List<ServiceObject> paramList=new ArrayList();
	private String clientType="java"; //[ios|java]
	
	public ServiceRequest(String serviceId)
	{
		this.serviceId=serviceId;
	}
	public void addParam(String key,Object value){
		if(params==null)
			params = new ArrayList<ServiceParam>();
		params.add(new ServiceParam(key,value));
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
 
	
	public ServiceResult callService(){
		/*
		 * 1.使用xstream请求服务
		 * 2.返回xml自动转为ServiceResponse对象
		 * */
		String xml=XstreamTool.toXml(this,new XstreamDateConverter("yyyy-MM-dd HH:mm:ss"));
		HttpClient http=new HttpClient();
		ServiceModel service=ServiceContainer.getServiceModel(serviceId);
		http.setUrl(service.getUrl());
		
		http.addParameter("msg", xml);
		String xmlResult="";
		try {
			xmlResult=http.post(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		ServiceResult result=(ServiceResult)XstreamTool.toObj(xmlResult,new XstreamDateConverter("yyyy-MM-dd HH:mm:ss"));
		
		return result;
	}
	public List<ServiceParam> getParams() {
		return params;
	}
	public void setParams(List<ServiceParam> params) {
		this.params = params;
	}

	public List<ServiceObject> getParamList() {
		return paramList;
	}
	public void setParamList(List<ServiceObject> paramList) {
		this.paramList = paramList;
	}
	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	
	

}
