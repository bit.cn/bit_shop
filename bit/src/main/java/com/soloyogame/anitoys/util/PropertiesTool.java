package com.soloyogame.anitoys.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


public class PropertiesTool {
	private String configFile;
	private String encode="UTF-8";
	public static Map pmap=new HashMap();
	static{
		PropertiesTool.init();
	}
	public static void init(){
		String path=FileTool.getRealPath("/");
		List<File> files=FileTool.getfilelist(path);
		Properties prop;
		for(File f:files){
			if(f.getName().endsWith("properties")){
				prop = new Properties();
				InputStream is=null;
				try {
					is = new FileInputStream(f.getPath());
					prop.load(is);
					PropertiesTool.pmap.put(f.getName(), prop);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				finally{
					if(is!=null){
						try {
							is.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
	}
	
	
	public PropertiesTool(String configFile,String encode){
		this.setConfigFile(configFile);
		this.setEncode(encode);
	}
	public PropertiesTool(String configFile){
		this.setConfigFile(configFile);
		this.setEncode(encode);
	}

	public  String getProperty(String propertyKey) {
		Properties prop = (Properties)pmap.get(configFile);
		String propertyValue = prop.getProperty(propertyKey,encode);
		return propertyValue;
	}
	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	public void setEncode(String encode) {
		this.encode = encode;
	}
	public  void test(){
		
	}
	public static void main(String[] args) {
		for(int i=0;i<50000;i++){
			PropertiesTool p=new PropertiesTool("config.properties");
			String s=p.getProperty("db");
			//System.out.println(s);
			PropertiesTool p2=new PropertiesTool("log4j.properties");
			String s2=p2.getProperty("log4j.appender.stdout");
			//System.out.println(s2);
	}
		
	}

}
