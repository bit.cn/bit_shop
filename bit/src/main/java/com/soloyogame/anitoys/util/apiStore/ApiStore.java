package com.soloyogame.anitoys.util.apiStore;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;

/**
 * 百度快递
 *
 * @author Administrator
 *
 */
public class ApiStore {

    /**
     * type:快递代号 number:运单号
     *
     */
    public static ApiStoreInfo request(String type, String number) {
        String httpUrl = "http://apis.baidu.com/netpopo/express/express1";
        String httpArg = "type=" + type + "&number=" + number;
        BufferedReader reader = null;
        String result = null;
        StringBuffer sbf = new StringBuffer();
        httpUrl = httpUrl + "?" + httpArg;

        try {
            URL url = new URL(httpUrl);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setRequestMethod("GET");
            // 填入apikey到HTTP header
            connection.setRequestProperty("apikey", "9b3dcbd9306d6651b978178b9ca0264f");
            connection.connect();
            InputStream is = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String strRead = null;
            while ((strRead = reader.readLine()) != null) {
                sbf.append(strRead);
                sbf.append("\r\n");
            }
            reader.close();
            result = sbf.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ApiStoreInfo store = JSON.parseObject(result, ApiStoreInfo.class);
        return store;
    }

    public static void main(String[] agrs) throws Exception {
        ApiStoreInfo apiStoreInfo = request("YUNDA", "1201205716323");
        ApiStoreItem result = apiStoreInfo.getResult();
        System.out.println("Msg:" + apiStoreInfo.getMsg());
        System.out.println("status:" + apiStoreInfo.getStatus());
        if (result != null) {
            List<Map<String, Object>> list = result.getList();
            System.out.println("issign:" + result.getIssign());
            for (Map<String, Object> map : list) {
                System.out.println("time" + map.get("time"));
                System.out.println("status" + map.get("status"));
            }
        }
    }
}
