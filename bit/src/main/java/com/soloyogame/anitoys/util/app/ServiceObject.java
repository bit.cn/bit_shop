package com.soloyogame.anitoys.util.app;

/**
 * for IOS
 * 
 * 
 */

public class ServiceObject {
	private String key;
	private String type;
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) { 
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}


	
}
