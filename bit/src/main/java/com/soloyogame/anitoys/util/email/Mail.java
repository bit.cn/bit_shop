package com.soloyogame.anitoys.util.email;

import java.io.Serializable;
import java.util.Date;

/**  
 * Mail属性实体  
 * @author 邮件实体类 
 */  
@SuppressWarnings("serial")  
public class Mail implements Serializable
{
	public static final String ENCODEING = "UTF-8";  
	public String host;     	// 服务器地址  
	public String sender;   	// 发件人的邮箱  
	public String receiver; 	// 收件人的邮箱  
	public String receiverName; // 收件人姓名
	public String name;     	// 发件人昵称  
	public String username; 	// 账号  
	public String password; 	// 密码  
	public String subject; 		// 主题  
	public String message; 		// 信息(支持HTML)  
	public String money;
	//sendCloud需要的参数
	public String url;     		//请求的接口地址
	public String apiUser; 
	public String apiKey;
	public String templateInvokeName;//模板名称
	
	
	//sencloud邮件模板中的参数
	public String webName;           //网站名称
	public String orderCode;         //订单号
	public String orderUrl;          //订单链接
	public Date sendTime;            //邮件发送时间
	public String passwordUrl;       //修改密码的链接
	public String productName;       //商品名称
	public int daynum;               //补款天数
	public String vcode;             //验证码
	
	
	public Mail(String sender,String name,String money)
	{
		this.sender=sender;
		this.name=name;
		this.money=money;
	}
	
	public Mail(){
	}
  
    public String getVcode() {
		return vcode;
	}

	public void setVcode(String vcode) {
		this.vcode = vcode;
	}

	public int getDaynum() {
		return daynum;
	}

	public void setDaynum(int daynum) {
		this.daynum = daynum;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPasswordUrl() {
		return passwordUrl;
	}

	public void setPasswordUrl(String passwordUrl) {
		this.passwordUrl = passwordUrl;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getOrderUrl() {
		return orderUrl;
	}

	public void setOrderUrl(String orderUrl) {
		this.orderUrl = orderUrl;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getWebName() {
		return webName;
	}

	public void setWebName(String webName) {
		this.webName = webName;
	}

	public String getTemplateInvokeName() {
		return templateInvokeName;
	}

	public void setTemplateInvokeName(String templateInvokeName) {
		this.templateInvokeName = templateInvokeName;
	}

	public String getApiUser() {
		return apiUser;
	}

	public void setApiUser(String apiUser) {
		this.apiUser = apiUser;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getHost() {  
        return host;  
    }  
  
    public void setHost(String host) {  
        this.host = host;  
    }  
  
    public String getSender() {  
        return sender;  
    }  
  
    public void setSender(String sender) {  
        this.sender = sender;  
    }  
  
    public String getReceiver() {  
        return receiver;  
    }  
  
    public void setReceiver(String receiver) {  
        this.receiver = receiver;  
    }  
  
    public String getName() {  
        return name;  
    }  
  
    public void setName(String name) {  
        this.name = name;  
    }  
  
    public String getUsername() {  
        return username;  
    }  
  
    public void setUsername(String username) {  
        this.username = username;  
    }  
  
    public String getPassword() {  
        return password;  
    }  
  
    public void setPassword(String password) {  
        this.password = password;  
    }  
  
    public String getSubject() {  
        return subject;  
    }  
  
    public void setSubject(String subject) {  
        this.subject = subject;  
    }  
  
    public String getMessage() {  
        return message;  
    }  
  
    public void setMessage(String message) {  
        this.message = message;  
    }  
}
