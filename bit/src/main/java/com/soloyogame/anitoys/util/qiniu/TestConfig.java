package com.soloyogame.anitoys.util.qiniu;

import com.qiniu.util.Auth;



/**
 * 七牛测试配置类
 * @author shaojian
 */
public final class TestConfig 
{
    public static final Auth dummyAuth = Auth.create("lWRT2zzEd9BxiQy-Z4ydsUr7GObo6DC4gfm7W2Ho", "X-sQbkwoNolDIL2jygGxB1c2upYtp78-HtUa98p1");
    public static final Auth testAuth = Auth.create("lWRT2zzEd9BxiQy-Z4ydsUr7GObo6DC4gfm7W2Ho","X-sQbkwoNolDIL2jygGxB1c2upYtp78-HtUa98p1");
    public static final String bucket = "anitoysupload";
    public static final String key = "java-duke.svg";
    public static final String domain = "http://uploads.anitoys.com";

    private TestConfig() 
    {
    }

    public static boolean isTravis() 
    {
        return "travis".equals(System.getenv("QINIU_TEST_ENV"));
    }
}
