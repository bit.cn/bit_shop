package com.soloyogame.anitoys.util.excel;

import java.util.List;

/**
 * @see用于导出List类型的数据到excel
 * @author shaojian
 */
public class ExcelModel implements java.io.Serializable 
{
	private static final long serialVersionUID = -7618635454083372389L;
	public String sheetName;	 //工作表名
	public String fileTitle;	 //文件标题
	public String fileName; 	 //文件名
	public List<Object> datalist;//数据列表,元素为对象类型,可以为Map
	public String[] titles;      //标题
	public String[] fields;      //字段(与标题对应),格式为$[item.fieldName],可以嵌套
	public int[] widths;
	public String dateFormat;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public List<Object> getDatalist() {
		return datalist;
	}
	public void setDatalist(List<Object> datalist) {
		this.datalist = datalist;
	}
	public String[] getTitles() {
		return titles;
	}
	public void setTitles(String[] titles) {
		this.titles = titles;
	}
	public String[] getFields() {
		return fields;
	}
	public void setFields(String[] fields) {
		this.fields = fields;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public String getFileTitle() {
		return fileTitle;
	}
	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}
	public String getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	public int[] getWidths() {
		return widths;
	}
	public void setWidths(int[] widths) {
		this.widths = widths;
	}
	

}
