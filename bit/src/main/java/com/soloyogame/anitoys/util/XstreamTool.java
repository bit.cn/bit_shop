package com.soloyogame.anitoys.util;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.Element;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * xml与javabean互转工具
 * 使用XStream实现
 * @author shaojian
 */
public class XstreamTool 
{
	//Xstream简化类名格式扫描路径
	public static List<Class> scanClassList=new ArrayList();
	
	static
	{
		try 
		{
			init();
		}
		catch (Exception e) 
		{
		}
	}
	
	public static void init()
	{
		String filePath=FileTool.getRealPath("/common/xstream-config.xml");
		//String filePath="D:/apache-tomcat-6.0.29/webapps/jshop/WEB-INF/classes/common/xstream-config.xml";
		Document doc=XmlTool.bulidXmlDocByFile(filePath);
		List<Element> classes=doc.selectNodes("/config/classScan/class");
		String clazzName;
		for(Element clazz:classes)
		{ 
			clazzName=clazz.attributeValue("name");
			try 
			{
				XstreamTool.scanClassList.add(Class.forName(clazzName));
			} 
			catch (ClassNotFoundException e) 
			{
				//LogTool.warn(XstreamTool.class, "No /common/xstream-config.xml be found");
			}
		}
		
		//扫描包下的类,jar包尚未实现
		/**
		List<Element> packets=doc.selectNodes("/config/classScan/packet");
		String packName;
		for(Element packet:packets){ 
			packName=packet.attributeValue("name");
			List<String> cNames=ClassTool.getClassName(packName);
			for(String s:cNames){
				try {
					XstreamTool.scanClassList.add(Class.forName(s));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		**/
	}
	
	
	public static String toXml(Object model) {
		XStream xstream = new XStream(new DomDriver());
		xstream.registerConverter(new XstreamDateConverter("yyyy-MM-dd HH:mm:ss"));
		if(!XstreamTool.scanClassList.isEmpty()){
			for(Class c:XstreamTool.scanClassList){
				xstream.alias(c.getSimpleName(), c);//格式化类名
			}
		}
		String xml = xstream.toXML(model);

		return xml;

	}
	
	public static String toJson(Object model) 
	{
		XStream xstream = new XStream(new JettisonMappedXmlDriver()); 
		xstream.registerConverter(new XstreamDateConverter("yyyy-MM-dd HH:mm:ss"));
		if(!XstreamTool.scanClassList.isEmpty())
		{
			for(Class c:XstreamTool.scanClassList)
			{
				xstream.alias(c.getSimpleName(), c);//格式化类名
			}
		}
		String xml = xstream.toXML(model);

		return xml;

	}
	
	public static Object fromJsonToObj(String jsonString) 
	{
		Converter dateConverter= new XstreamDateConverter("yyyy-MM-dd HH:mm:ss"); 
		XStream xs = new XStream(new JettisonMappedXmlDriver());
		xs.registerConverter(dateConverter);
		if(!XstreamTool.scanClassList.isEmpty())
		{
			for(Class c:XstreamTool.scanClassList)
			{
				xs.alias(c.getSimpleName(), c);//格式化类名
			}
		}
		Object obj = xs.fromXML(jsonString);
		return obj;
	}
	
	public static Object fromJsonToObj(String jsonString,String dateFormat) 
	{
		Converter dateConverter= new XstreamDateConverter(dateFormat); 
		XStream xs = new XStream(new JettisonMappedXmlDriver());
		xs.registerConverter(dateConverter);
		if(!XstreamTool.scanClassList.isEmpty())
		{
			for(Class c:XstreamTool.scanClassList)
			{
				xs.alias(c.getSimpleName(), c);      //格式化类名
			}
		}
		Object obj = xs.fromXML(jsonString);
		return obj;
	}
	
	public static String toXml(Object model,Converter dateConverter) {
		//XStream xstream = new XStream();
		XStream xstream = new XStream(new DomDriver());
		xstream.registerConverter(dateConverter);
		if(!XstreamTool.scanClassList.isEmpty()){
			for(Class c:XstreamTool.scanClassList){
				xstream.alias(c.getSimpleName(), c);//格式化类名
			}
		}
		String xml = xstream.toXML(model);

		return xml;

	}

	public static Object toObj(String xmlString,Converter dateConverter) {

		XStream xs = new XStream(new DomDriver());
		xs.registerConverter(dateConverter);
		if(!XstreamTool.scanClassList.isEmpty()){
			for(Class c:XstreamTool.scanClassList){
				xs.alias(c.getSimpleName(), c);//格式化类名
			}
		}
		System.out.println(xmlString);
		Object ob = xs.fromXML(xmlString);

		return ob;

	}
	public static Object toObj(String xmlString) {

		XStream xs = new XStream(new DomDriver());
		if(!XstreamTool.scanClassList.isEmpty()){
			for(Class c:XstreamTool.scanClassList){
				xs.alias(c.getSimpleName(), c);//格式化类名
			}
		}
		Object ob = xs.fromXML(xmlString);
		return ob;

	}

	public static void main(String[] args) throws Exception {

		
		/***登录结果**/	
		/**
		List demoList=new ArrayList();
		demoList.add(new ChartModel());
		demoList.add(new ChartModel());
		demoList.add(new ChartModel());
		String json=XstreamTool.toJson(demoList);
		System.out.println(json);**/
		
		Map map=new HashMap();
		map.put("a", 1);
		map.put("c", "XXXXXXXXX");

		String json=XstreamTool.toJson(map);
		System.out.println(json);
	

	}

	

}
