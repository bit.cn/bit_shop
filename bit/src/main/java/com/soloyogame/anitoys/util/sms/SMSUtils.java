package com.soloyogame.anitoys.util.sms;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class SMSUtils 
{
	public static void main(String[] args) 
	{
		String mobile="13918549506";
		String content="测试短信【动漫商城】";
		send(mobile,content);
	}
	private String sendMessage(String mobile,String content)
	{
	    // just replace key here
	    Client client = Client.create();
	    client.addFilter(new HTTPBasicAuthFilter(
	        "api","key-e37fb9fdf2660be23f51e16ae5755bc8"));
	    WebResource webResource = client.resource(
	        "http://sms-api.luosimao.com/v1/send.json");
	    MultivaluedMapImpl formData = new MultivaluedMapImpl();
	    formData.add("mobile", mobile);
	    formData.add("message", content);
	    ClientResponse response =  webResource.type( MediaType.APPLICATION_FORM_URLENCODED ).
	    post(ClientResponse.class, formData);
	    String textEntity = response.getEntity(String.class);
	    int status = response.getStatus();
	    //System.out.print(textEntity);
	    //System.out.print(status);
	    return textEntity;
	}

	private String status()
	{
	    Client client = Client.create();
	    client.addFilter(new HTTPBasicAuthFilter(
	        "api","key-d609b769db914a4d959bae3414ed1f7X"));
	    WebResource webResource = client.resource( "http://sms-api.luosimao.com/v1/status.json" );
	    MultivaluedMapImpl formData = new MultivaluedMapImpl();
	    ClientResponse response =  webResource.get( ClientResponse.class );
	    String textEntity = response.getEntity(String.class);
	    int status = response.getStatus();
	    //System.out.print(status);
	    //System.out.print(textEntity);
	    return textEntity;
	}

/**
 * 发送短信
 * @param mobile
 * @param content
 */
public static void send(String mobile,String content)
{
	SMSUtils api = new SMSUtils();
    String httpResponse =  api.sendMessage(mobile,content);
     try 
     {
        JSONObject jsonObj = new JSONObject( httpResponse );
        int error_code = jsonObj.getInt("error");
        String error_msg = jsonObj.getString("msg");
        if(error_code==0)
        {
            System.out.println("发送短信成功.");
        }
        else
        {
            System.out.println("发送短信失败,code is "+error_code+",msg is "+error_msg);
        }
    } 
     catch (JSONException ex) 
     {
     }
   
    httpResponse =  api.sendMessage(mobile,content);
    try 
    {
        JSONObject jsonObj = new JSONObject( httpResponse );
        int error_code = jsonObj.getInt("error");
        if( error_code == 0 )
        {
            int deposit = jsonObj.getInt("deposit");
            System.out.println("Fetch deposit success :"+deposit);
        }
        else
        {
            String error_msg = jsonObj.getString("msg");
            System.out.println("Fetch deposit failed,code is "+error_code+",msg is "+error_msg);
        }
    } 
    catch (JSONException ex) 
    {
    }
	}
}
