package com.soloyogame.anitoys.util.app;

import java.util.ArrayList;
import java.util.List;

public class ServiceModel {
	public String id;
	public String name;
	public String bean;
	public String method;
	public String url; 
	public boolean logable=false;
	public List<ServiceParam> paramList=new ArrayList<ServiceParam>();
	public List<ServiceParam> returnParamList=new ArrayList<ServiceParam>();
	
	public void addParam(ServiceParam p){
		paramList.add(p);
	}
	public void addReturnParam(ServiceParam p){
		returnParamList.add(p);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBean() {
		return bean;
	}
	public void setBean(String bean) {
		this.bean = bean;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public List<ServiceParam> getParamList() {
		return paramList;
	}
	public void setParamList(List<ServiceParam> paramList) {
		this.paramList = paramList;
	}
	public List<ServiceParam> getReturnParamList() {
		return returnParamList;
	}
	public void setReturnParamList(List<ServiceParam> returnParamList) {
		this.returnParamList = returnParamList;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isLogable() {
		return this.logable;
	}
	public void setLogable(boolean logable) {
		this.logable = logable;
	}

}
