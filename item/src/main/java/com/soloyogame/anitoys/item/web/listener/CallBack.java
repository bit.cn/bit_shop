package com.soloyogame.anitoys.item.web.listener;

public interface CallBack {
	String callback() throws Exception;
}
