package com.soloyogame.anitoys.item.web.controller.front.message;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.service.MessageService;
import com.soloyogame.anitoys.db.commond.Message;
import com.soloyogame.anitoys.service.ProductService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.db.commond.Product;
import com.soloyogame.anitoys.db.commond.CartInfo;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.item.web.controller.front.FrontBaseController;
import com.soloyogame.anitoys.item.web.controller.front.util.LoginUserHolder;

/**
 * 消息 Controller
 * @author ZGY
 */
@Controller("frontMessageController")
@RequestMapping("messageAction")
public class MessageAction extends FrontBaseController<Message>
{
	@Autowired
	MessageService messageService;
	@Autowired
	private ProductService productService;
	@Autowired
	private RedisCacheProvider redisCacheProvider;
	@Override
	public Services<Message> getService() 
	{
		return messageService;
	}
	
	@RequestMapping("my_information")
	public String my_information(HttpServletRequest request,ModelMap model,Message e)
	{
		// 获取用户ID
		Account account = LoginUserHolder.getLoginAccount();
		int offset = 0;//分页偏移量
        if (request.getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(request.getParameter("pager.offset"));
        }
        if (offset < 0)
        {
        	offset = 0;
        }
        e.setAccountId(account.getAccount());
		PagerModel pager = messageService.selectPageList(e);
		pager.setPageSize(5);
		pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)
                / pager.getPageSize());
		pager.setPagerUrl("my_information");
		model.addAttribute("pager", pager);
		
		
				if (account == null || StringUtils.isBlank(account.getAccount())) 
				{
					return page_toLoginRedirect;
				}
				List<Product> productList = new ArrayList<Product>();
				CartInfo cartInfo = null;
				if(account==null)
				{
					cartInfo = (CartInfo) redisCacheProvider.get("myCart");
				}
				else
				{
					cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
				}
				if(cartInfo!=null)
				{
					productList = cartInfo.getProductList();
				} 
				else 
				{
					cartInfo = new CartInfo();
				}
				int productCount=productService.selectProductCount();
				
				model.addAttribute("cartInfo", cartInfo);
				model.addAttribute("productCount", productCount);
				model.addAttribute("productList", productList);
		return "/account/my_information";
	}
	
}
