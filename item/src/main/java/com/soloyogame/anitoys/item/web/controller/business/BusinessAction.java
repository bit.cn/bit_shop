package com.soloyogame.anitoys.item.web.controller.business;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.soloyogame.anitoys.item.web.controller.front.FrontBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.service.BusinessService;
import com.soloyogame.anitoys.db.commond.Business;
import com.soloyogame.anitoys.service.CatalogService;
import com.soloyogame.anitoys.db.commond.Catalog;
import com.soloyogame.anitoys.service.IndexImgService;
import com.soloyogame.anitoys.service.NavigationService;
import com.soloyogame.anitoys.db.commond.Navigation;
import com.soloyogame.anitoys.service.NewsService;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.service.ProductService;
import com.soloyogame.anitoys.db.commond.Product;
import com.soloyogame.anitoys.service.BrandService;
import com.soloyogame.anitoys.db.commond.Brand;
import com.soloyogame.anitoys.service.BusinessAdvertService;
import com.soloyogame.anitoys.db.commond.BusinessAdvert;
import com.soloyogame.anitoys.service.BusinessRecommendService;
import com.soloyogame.anitoys.db.commond.BusinessRecommend;
import com.soloyogame.anitoys.service.ChannelService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.db.commond.Channel;
import com.soloyogame.anitoys.db.commond.CartInfo;
import com.soloyogame.anitoys.item.web.controller.front.util.RequestHolder;



/**
 * catalog
 * 店铺信息管理
 * @author shaojian
 */
@Controller("frontBusinessAction")
@RequestMapping("business")
public class BusinessAction extends FrontBaseController<Business>
{
	@Autowired
	private BusinessService businessService;//商品服务
	@Autowired
	private BrandService brandService;      //商品品牌服务
	@Autowired
	private CatalogService catalogService;  //商品品牌服务
	@Autowired
	private ProductService productService;  //商品服务
	@Autowired
	private ChannelService channelService;  //导航服务
	@Autowired
	private BusinessRecommendService businessRecommendService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private RedisCacheProvider redisCacheProvider;
	@Autowired
	private IndexImgService indexImgService;
	@Autowired
	private BusinessAdvertService businessAdvertService;
	@Autowired
	private NavigationService nativeService;
	
	@Override
	public Services<Business> getService() {
		return businessService;
	}
	
	/**
	 * 查询指定的店铺信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/businessSelect")
	public String business(String businessId,ModelMap model) throws Exception
	{		
		//卖家首页的导航
		Channel channel = new Channel();
		channel.setBusinessId(businessId);
		List<Channel> channelList = channelService.selectList(channel);
		
		//推荐商品
		BusinessRecommend businessRecommend = new BusinessRecommend();
        businessRecommend.setType(1);
        businessRecommend.setBusinessId(businessId);
        List<BusinessRecommend> businessRecommendList = businessRecommendService.selectList(businessRecommend);
       
        
        Business business = businessService.selectById(businessId);
        String businessName = business.getBusinessName();
        
        
        //橱窗商品
        businessRecommend.setType(2);
        businessRecommend.setBusinessId(businessId);
        List<BusinessRecommend> businessRecommendList1 = businessRecommendService.selectList(businessRecommend);
        RequestHolder.getRequest().setCharacterEncoding("utf-8");
        //现货商品状态的值为1
      	Product spotp=new Product();
      	spotp.setProductType(1);
      	spotp.setTop(12);
      	spotp.setBusinessId(businessId);
      	List<Product> spotCommodityProducts=productService.selectList(spotp);
      	//预售货商品状态的值为2
      	/*Product product1=new Product();
      	product1.setProductType(2);
      	product1.setBusinessId(businessId);
      	List<Product> replenishmentProduct1=productService.selectList(product1);*/
        
        //补款商品状态的值为2 当前时间在补款开始时间和不快结束时间内 切延迟时间是否为空
      	Product product=new Product();
      	product.setProductType(2);
      	product.setTop(6);
      	product.setBusinessId(businessId);
      	product.setReplenishmentTime("true");
      	List<Product> replenishmentProduct1=productService.selectList(product);
      	List<Product> replenishmentProduct = new ArrayList<Product>();
      	for (Product product2 : replenishmentProduct1) 
      	{
			if(product2.getReplenishmentdeplaytime()==null)
			{
				 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				 String str =product2.getName();
				 if(str.length()>=18){
					 product2.setpDisplayName(str.substring(0, 11)+"...");
				 }else{
					 product2.setpDisplayName(product2.getName());
				 }
					
				if(product2.getReplenishmentTime()!=null && 
				   product2.getReplenishmentEndTime()!=null &&
				   sdf.parse(product2.getReplenishmentTime()).getTime()<new Date().getTime() && 
				   new Date().getTime()<sdf.parse(product2.getReplenishmentEndTime()).getTime())
				{
					replenishmentProduct.add(product2);
				}
			}
			else
			{
				 	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				 	 String str =product2.getName();
				 	 if(str.length()>=18){
						 product2.setpDisplayName(str.substring(0, 11)+"...");
					 }else{
						 product2.setpDisplayName(product2.getName());
					 }
					if(sdf.parse(product2.getReplenishmentTime()).getTime()<new Date().getTime() && 
							new Date().getTime()<sdf.parse(product2.getReplenishmentdeplaytime()).getTime())
					{
						replenishmentProduct.add(product2);
					}
			}
		}
      	
      	
      	/*//预售,补款放在一个list里面
      	List<Product> pList=new ArrayList<Product>();
      	if(replenishmentProduct1!=null){
      		pList.addAll(replenishmentProduct1);
      	}
      	if(replenishmentProduct!=null){
      		pList.addAll(replenishmentProduct);
      	}*/
      	
      	//最新商品列表
      	Product p = new Product();
		p.setTop(12);
		p.setIsnew(Product.Product_isnew_y);//最新
		p.setBusinessId(businessId);
		List<Product> newProductList = productService.selectList(p);
      	//人气商品
		Product hotp = new Product();
		hotp.setTop(6);
		hotp.setHot(true);   
		hotp.setBusinessId(businessId);
		List<Product> hotProducts = productService.selectList(hotp);
		//最新公告列表
      	News news = new News();
      	news.setType("notice");
      	news.setTop(6);
      	news.setBusinessId(businessId);
      	List<News> newsList = newsService.selectList(news);
       //最新情报列表
      	News newss = new News();
      	newss.setType("help");
      	newss.setTop(6);
      	newss.setBusinessId(businessId);
      	List<News> newsLists = newsService.selectList(newss);
     
      	//官网blog
      	News blog = new News();
      	blog.setBusinessId(businessId);
      	blog.setType("blog");
      	blog.setTop(6);
      	List<News> blogList = newsService.selectList(blog);
      	//店铺广告位
      	BusinessAdvert businessAdvert1=new BusinessAdvert();
      	businessAdvert1.setBusinessId(businessId);
      	List<BusinessAdvert> businessAdvertList=businessAdvertService.selectList(businessAdvert1);
      	List<BusinessAdvert> businessAdvertImageList=new ArrayList<BusinessAdvert>();
      	String advertImage2=null;
      	String advertImage3=null;
      	String advertImage4=null;
      	String advertLink2=null;
      	String advertLink3=null;
      	String advertLink4=null;
      	for (int i = 0; i < businessAdvertList.size(); i++) {
      		if (businessAdvertList.get(i).getAdvertPosition()==1) {
      			businessAdvertImageList.add(businessAdvertList.get(i));
			}
      		
		}
      	for (int i = 0; i < businessAdvertList.size(); i++) {
			if (businessAdvertList.get(i).getAdvertPosition()==2) {
				advertImage2=businessAdvertList.get(i).getAdvertImage();
				advertLink2=businessAdvertList.get(i).getAdvertLink();
						}
			if (businessAdvertList.get(i).getAdvertPosition()==3) {
				advertImage3=businessAdvertList.get(i).getAdvertImage();
				advertLink3=businessAdvertList.get(i).getAdvertLink();
			}
			if (businessAdvertList.get(i).getAdvertPosition()==4) {
				advertImage4=businessAdvertList.get(i).getAdvertImage();
				advertLink4=businessAdvertList.get(i).getAdvertLink();
			}


		}
        model.addAttribute("businessAdvertImageList",businessAdvertImageList);
      	model.addAttribute("advertImage2", advertImage2);
      	model.addAttribute("advertImage3", advertImage3);
      	model.addAttribute("advertImage4", advertImage4);
 
      	model.addAttribute("advertLink2", advertLink2);
      	model.addAttribute("advertLink3", advertLink3);
      	model.addAttribute("advertLink4", advertLink4);
        //查询品牌分类
		Catalog catalog=new Catalog();
	    List<Catalog>  c= catalogService.selectList(catalog);
	    //查询品牌型号
	    Brand brand=new Brand();
	    List<Brand> b=  brandService.selectList(brand);
	    model.addAttribute("hotProducts", hotProducts);
	    model.addAttribute("businessRecommendList", businessRecommendList);
	    model.addAttribute("businessId", businessId);
	    model.addAttribute("businessName", businessName);
	    model.addAttribute("businessRecommendList1", businessRecommendList1);
	    model.addAttribute("replenishmentProduct", replenishmentProduct);
	    model.addAttribute("channelList", channelList);
		model.addAttribute("catalog", c);
		model.addAttribute("brand", b);
		model.addAttribute("business", business);
		model.addAttribute("newsList", newsList);
		model.addAttribute("newsLists", newsLists);
		model.addAttribute("newProductList", newProductList);
		model.addAttribute("blogList", blogList);
		model.addAttribute("spotCommodityProducts", spotCommodityProducts);
		List<Product> productList = new ArrayList<Product>();
		//CartInfo cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		CartInfo cartInfo = getMyCart();
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productList", productList);
		//卖家的帮助中心
		Catalog catalog1 = new Catalog();
		catalog1.setType("a");
		catalog1.setPid("0");
		List<Catalog> newCatalogs = catalogService.selectList(catalog1);
		if(newCatalogs!=null && newCatalogs.size()>0)
		{
			for(int i=0;i<newCatalogs.size();i++)
			{
				Catalog item = newCatalogs.get(i);

				//加载此目录下的所有文章列表
				News news1 = new News();
				news.setCatalogID(item.getId());
				List<News> newsList1 = newsService.selectList(news1);
				item.setNews(newsList1);
			}
		}
		model.addAttribute("newCatalogs", newCatalogs);
		//商家的友情链接
		Navigation e=new Navigation();
		e.setBusinessId(businessId);
		List<Navigation> navigationList=new ArrayList<Navigation>();
		navigationList=nativeService.selectList(e);
		model.addAttribute("navigationList", navigationList);
		//商家的轮播图
//		logger.info("loadIndexImgs...");
//		IndexImg indexImg = new IndexImg();
//		List<IndexImg> indexImages = indexImgService.selectList(c);
//        systemManager.setIndexImages(indexImages);
		
		
		return "business/business2";
	}
	
	/*
	 * 查询补款商品
	 */
	@RequestMapping(value = "replenishmentShop")
	public String replenishmentShop(ModelMap model) throws Exception
	{		
		//补款商品状态的值为3
		Product product=new Product();
		product.setProductType(3);
	    List<Product> p=productService.selectList(product);
	    //现货商品的状态为1
	    Product products=new Product();
	    products.setProductType(1);
	    List<Product> px=productService.selectList(product);
	    model.addAttribute("product", p);
	    model.addAttribute("product", px);
		return "business/businessFirst";
	}
}
