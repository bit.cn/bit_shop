package com.soloyogame.anitoys.item.web.controller;

import com.soloyogame.anitoys.db.commond.KeyValueHelper;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

import java.util.List;

/**
 * Created by dylan on 15-1-26.
 */
public class KeyValueGetter implements TemplateMethodModelEx {
    @Override
    public Object exec(List arguments) throws TemplateModelException {
        return KeyValueHelper.get(arguments.get(0).toString());
    }
}
