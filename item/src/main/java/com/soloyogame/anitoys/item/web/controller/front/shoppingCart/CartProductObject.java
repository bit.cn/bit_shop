package com.soloyogame.anitoys.item.web.controller.front.shoppingCart;

import java.io.Serializable;
import java.util.List;

import com.soloyogame.anitoys.db.commond.Business;
import com.soloyogame.anitoys.db.commond.Product;


public class CartProductObject implements Serializable 
{
	private Business business;
	private List<Product> productList; //购物车中商品列表
	
	
	public Business getBusiness() {
		return business;
	}
	public void setBusiness(Business business) {
		this.business = business;
	}
	public List<Product> getProductList() {
		return productList;
	}
	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
}
