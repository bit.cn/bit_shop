<#macro detailMain title="" checkLogin=true>
	<style type="text/css">
		#new_style_main2{
		padding-top:28px;
		}
	</style>
	<!--商品状态-->
	<input type="hidden" id="status" name="status" value="${e.status!""}"/>
	<!--商品上架时间-->
	<input type="hidden" id="groundingTime" name="groundingTime" value="${e.groundingTime!""}"/>
	<!--商品下架时间-->
	<input type="hidden" id="offshelfTime" name="offshelfTime" value="${e.offshelfTime!""}"/>
	<!--商品发售时间-->
	<input type="hidden" id="saleatDate" name="saleatDate" value="${e.saleatDate!""}"/>
	<!--商品商品购买时间或者预售时间-->
	<input type="hidden" id="sellTime" name="sellTime" value="${e.sellTime!""}"/>
	<!--商品商品购买结束时间或预售结束时间-->
	<input type="hidden" id="sellEndTime" name="sellEndTime" value="${e.sellEndTime!""}"/>
	<#if e.productType==1>
		<input type="hidden" id="depositType" name="depositType" value="2"/>		        		
	<#elseif e.productType==2>
		<input type="hidden" id="depositType" name="depositType" value="1"/>
	</#if>
	
	
	<!--商品补款开始时间-->
	<input type="hidden" id="replenishmentTime" name="replenishmentTime" value="${e.replenishmentTime!""}"/>
	<!--商品补款结束时间-->
	<input type="hidden" id="replenishmentEndTime" name="replenishmentEndTime" value="${e.replenishmentEndTime!""}"/>
	<!--商品延迟补款时间-->
	<input type="hidden" id="replenishmentdeplaytime" name="replenishmentdeplaytime" value="${e.replenishmentdeplaytime!""}"/>
	<div>
		<div class="det_pre">
			<input type="hidden" value="${e.id}" name="id" id="id"/>
			<div style="font-weight: bold;font-size: 18px;">
				<span class="det_title">
					${e.mainCatalogName!""}
				</span>
				<span class="det_title_a">
					>${e.childrenCatalogName!""}
				</span>
			</div>
		</div>
	</div>
	<div class="det_img">
		<div class="col-xs-6" id="productMainDiv" style="width:100%;position:relative; padding:0px">
		<div id="imgtest" class="imgtest"></div>
		<div id="fdg_fdj" class="fdj" > </div>
			<#include "product_center_piclist_slide2.ftl"/>
		</div>
	<div class="det_par">
			<table class="det_table">
				<tr>
				  <td colspan="6" id="name"> 
				  	<span class="det_par_t" >
				  			<#if e.name??>
									<#if e.name?length gte 40>
										${e.name?substring(0,40)}...
									<#else>
										${e.name!""}
									</#if>
								</#if>
					</span>
				  </td>
				</tr>
				<tr>
				  <td class="det_td" colspan="6" id="keywords">
			      	<span class="det_par_t1" >
						<#if e.keywords??>
									<#if e.keywords?length gte 50>
										${e.keywords?substring(0,50)}...
									<#else>
										${e.keywords!""}
									</#if>
								</#if>
					</span>					
				  </td>
				</tr>
				<tr>
					<td class="det_td1" colspan="6" id="status">&nbsp;&nbsp;商品状态:
				 		 <label class="det_par_tone det_par_t2" id="statuId" >${e.productType}</label>
					</td>
				</tr>
				<tr>
					<td colspan="6" style="width:200px ;"  class="det_td1">
						<table>
							<tr>
								<td style="width:7%; font-size: 12px; text-align: right;font-weight: bold;">全款价格:</td>
								<td style="width:15% ; font-size: 16px; color: #FF8000;font-weight: bold;">￥<label id="nowPrice">${e.nowPrice!""}</label></td>
								<td style="width:9%; font-size: 12px; text-align: right;font-weight: bold;">下单价格:</td>
								<td style="width:15%; font-size: 16px; color: #FF8000;font-weight: bold;">￥
									<#if e.productType==1><label>${e.nowPrice!""}</label>
									<#else><label id="depositPrice">${e.depositPrice!""}</label></#if>
									<#if e.productType==1>
						        		<span></span>
									<#elseif e.productType==2>
								</td>
								<td style="width:9%; font-size: 16px; color: #FF8000;font-weight: bold;"><select name="select" style="width: 80px;font-size: 12px; height: 20px;" onchange="getdepositType(this)"> 
											<option value="1">定金预定</option>
											<option value="2">全款预定</option>
										</select>
									</#if>
								</td>
								<td style="width:15%;font-size: 12px; text-align: right;font-weight: bold;">送anitoys积分:</td>
								<td style="width:9%;font-size: 16px; color: #FF8000;font-weight: bold;"><label id="score">${e.score!""}</label></td>
							</tr>
						</table>
				    </td>
				</tr>
				<tr>
					<td class="det_td2" colspan="6">
						<span>
							配送至:
						</span>
						<select name="select" class="res_sel1"> 
							<option value="选择品牌">中国大陆，不包括港奥台</option>
							<#if businessShippingList??>
								<#list businessShippingList as item>
									<option value="${item.id!""}">${item.provinceName!""}</option>
								</#list>
							</#if>
					  	</select>
						 <span>
						 	有货
						 </span>
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<span id="expressAddress">
							发货地：<#if wareHouseList??><#if wareHouseList[0]??>${wareHouseList[0].wareHouseName!""}</#if></#if>
						</span>
					</td>
				</tr>
				<tr>
					<td height="50" colspan="6">
						<span>
							服 &nbsp;&nbsp;&nbsp;务:本品享受GSC上海的完善售后服务  <b href="#" onclick="spbz()"  style="color:#ff6600;">售后服务</b>
						</span>				
					</td>
				</tr>
				<tr>
					<td colspan="6" style="color: #A1A1A1;">
						— — — — — — — — — —
					 	— — — — — — — — — —
						— — — — — — — — — —
						— — — — — — — — — —
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<#if e.specJsonString??>
									<!-- 商品规格 -->
									<input type="hidden" name="specJsonString" id="specJsonString" value="${e.specJsonString}"/>
									<div style="border:0px solid red;height:30px;" class="spec" id="specDiv">
                                           <input type="hidden" id="selectSpec" value="0"/>
										<dl style="margin-top:10px;">
											<span style="float: left;">&nbsp;</span>

											<dd>
												<ul style="list-style: none;" id="specSize">
													<#list e.specSize as item>
														<li>${item!""}</li>
													</#list>
												</ul>
											</dd>
										</dl>

										<dl style="margin-top:10px;	margin-bottom:10px;">
											<span style="float: left;">&nbsp;</span>
											<dd>
												<ul style="list-style: none;" id="specColor">
													<#list e.specColor as item>
														<li>${item!""}</li>
													</#list>
												</ul>
											</dd>
										</dl>
									</div>
								</#if>
					</td>
				</tr>
				<tr class="det_td_new_4">
					<td class="det_td_new_3" >
						购买数量：
					</td>
					<td style="margin-top:10px;">
						<div>
						<button class="conut_sub" onclick="subtract() " >-</button>
						<input type="text" class="conut_t" value="1" size="4" maxlength="4" name="inputBuyNum" data-rule="购买数量;required;integer;" id="inputBuyNum" style="text-align: center;"/>
						<input type="hidden" name="productRestrictions" value="${e.productRestrictions!""}">
						<button class="conut_add" onclick="add()" style="cursor: pointer;">+</button>

						<label class="conut_t1">
						 	 &nbsp;&nbsp;&nbsp;
						 	 <#if e.productRestrictions!=0>(限购${e.productRestrictions!""}个)</#if>
						 	 <#if e.productRestrictions==0>(不限购)</#if>
						 	 <input id="addNumber" type="hidden" name="addNumber" value="${e.productRestrictions!""}">
						 	 <input id="stock" type="hidden" name="stock" value="${e.stock!""}">
						</label><br>
						</div>
					</td>
					<td colspan="3" class="new_kc_td_style" >
						<p class="sj_style_1">
						该商品即将上架
						</p>
						<!-- 超出库存提示语--> 
						<div id="exceedDivError" class="alert alert-danger fade in" style="display: none;margin-bottom: 0px;">
							<h4 id="exceedSpanError"></h4>
						</div>
						<input type="hidden" name="id" value="${e.id!""}"/>
						<input type="hidden" name="addCart" value="1"/>											
					</td>
				</tr>
				<tr>
					<td class="ljgm_style" style="padding: 20px 0px 0px 10px;display:none;"><a href="#" onclick="toPlacPayFromDetail()"><img src="${systemSetting().staticSource}/product/img/det/ok.png"/></a></td>
					<td class="ljgm_styles" style="padding: 20px 0px 0px 10px;display:none;"><a href="${systemSetting().my}/account/orders"><img src="${systemSetting().staticSource}/product/img/bkStart.png"/></a></td>
					<td class="ljgm_styles_1" style="padding: 20px 0px 0px 10px;display:none;"><a href="${systemSetting().my}/account/orders"><img src="${systemSetting().staticSource}/product/img/bkCenter.png"/></a></td>
					<td class="ljgm_styles_2" style="padding: 20px 0px 0px 10px;display:none;"><a href="${systemSetting().my}/account/orders"><img src="${systemSetting().staticSource}/product/img/spcutOrder.png"/></a></td>
					<td class="ljgm_styles_3" style="padding: 20px 0px 0px 10px;display:none;"><a href="${systemSetting().my}/account/orders"><img src="${systemSetting().staticSource}/product/img/qjd.png"/></a></td>
					<!--加入购物车操作-->
					<td class="jrcar_style" style="padding: 20px 0px 0px 125px;display:none;">
					<#if e.status==2 && e.stock gt 0>
						<a name="stockErrorTips" productid="${id!""}" href="#" data-toggle="tooltip" title="" data-placement="top" ></a>
						<a id="addToCartBtn">
							<img  onclick="addToCart()" src="${systemSetting().staticSource}/product/img/det/use.png"/>
						</a>
					<#else>
						<a>
							<img onclick="addToCart()" src="${systemSetting().staticSource}/product/img/det/use.png"/>
						</a>
					</#if>
					</td>
					
					<td class="favorite_product">
						<#if isFavorite?? && isFavorite ==0>
							<script type="text/javascript">
								alert("已收藏");
							</script>
						</#if>
						
					</td>
				</tr>
		  </table>
	</div>
	<div class="det_cus">
			<table class="det_table1" width="100%">
				<tr>
					<td  align="right" colspan="2">
							<#if business??>
							<img src="${systemSetting().imageRootPath}/${business.maxPicture!""}" width="194px" height="114px"/>
							</#if>
					</td>
				</tr>
				<tr>
					<td class="cus_t">
						<label class="cus_text">
							&nbsp; <#if business??>
						${business.businessName!""}
						</#if>
						</label>
					</td>
				</tr>
				<tr>
				<td class="" colspan="2" style="line-height:20px;padding-left:30px;">
				<#if business??>
				<a target="_blank" href="${business.customerAddress!""}">
						<img src="http://static.anitoys.com/shopindex/layout/img/omt/cuss.png" height="25"/>
				</a>
				</#if>
				</td>
				</tr>
				<tr>
					<td class="cus_t" colspan="2">
						<label class="cus_text1">
							<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${e.businessId!""}" 
								style="text-decoration:none;padding-left:45px;" class="cus_sto">&nbsp; 进入店铺
							</a>
						</label>
					</td>
				</tr>
				<!--店铺收藏-->
				<tr>
					<td class="cus_t2" colspan="2">
					<#if currentAccount()??>
						<#if isFavoriteBusi?? && isFavoriteBusi ==0>
							<script type="text/javascript">
								alert("已收藏");
							</script>
						</#if>
						<a  id="isnewBusinessF" style="display:block" onclick="addToFavoritedo(${e.businessId!""},${e.id!""});" >
							<img src="${systemSetting().staticSource}/product/img/det/col.png"/>
						</a>
					<#else >
					 	<a  id="addToLogin" style="display:block" onclick="addToLogin();" >
							<img src="${systemSetting().staticSource}/product/img/det/col.png"/>
						</a>
					</#if>
					<a style="display:none;" id="isnewBusiness" onclick="isCollections()">
                        <img src="${systemSetting().staticSource}/product/img/det/col2.png" />
					</a>
					
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="det_pre clearfix">
		<div class="st_name">
				<ul>
					<li>
						<p class="tuijian">店家推荐</p>
					</li>
					<!--店铺推荐-->
					<#if businessRecommendList??>
					<#list businessRecommendList as item>
						<li class="pd15">
							<a href="${systemSetting().item}/product/${item.productId!""}">
								<#if item??>
									<img style="width: 98%;" src="${systemSetting().imageRootPath}/${item.picture!""}"/>
								</#if>
							</a>
						<br/>
						</li>
					
					<div class="st_par_div">
						<li class="st_par_name" style="text-align:center;">
						<#if business??>
							<#if business.businessName??>
									<#if business.businessName?length gte 17>
										${business.businessName?substring(0,17)}...
									<#else>
										${business.businessName!""}
									</#if>
								 </#if>
						</#if>
						</li>
						<li class="st_par_name1" style="text-align:center;">
								<#if item.name??>
									<#if item.name?length gte 13>
										${item.name?substring(0,13)}...
									<#else>
										${item.name!""}
									</#if>
								 </#if>
						</li>
					</div>	
					</#list>
					</#if>	
				</ul>
			</div>
			<!--商品详情展示tab-->
			<#include "product_tab_slide.ftl"/>
	</div>
</div>
</div>
<form id="formPlacPay" name="formPlacPay" method="post" action="${systemSetting().orders}/order/toPlacPayFromDetail">
</form>

<script type="text/javascript">
	function toPlacPayFromDetail(){
		var _obj = $("input[name=inputBuyNum]");
		var quantity = _obj.val();
		//console.log("_obj="+_obj+",notifyCartFlg="+notifyCartFlg+",quantity="+quantity+",pid="+_obj.attr("pid"));
		var productRestrictions = $("input[name=productRestrictions]").val();
		if(productRestrictions>0 && quantity>productRestrictions)
		{
			alert("超出商品限购数量！");
			return;
		}

		if(!checkStockFunc()){
			return;
		}
		
		var _specIdHidden = $("#specIdHidden").val();
		var specJsonStringVal = $("#specJsonString").val();
		//如果规格存在
		console.log("specIdHidden = " + _specIdHidden);
		if(specJsonStringVal && specJsonStringVal.length>0){
		if(!_specIdHidden || _specIdHidden==''){
			//$("#addToCartBtn").attr("data-original-title","请选择商品规格！").tooltip('show');
			alert("请选择规格！");
			return;
			}
		}
		var _url = "${systemSetting().item}/order/checkPlacPayFrom?productId="+$("#productID").val()+ 
				"&buyCount=" + $("#inputBuyNum").val() + 
				"&buySpecID=" + $("#specIdHidden").val()+
				"&depositType="+$("#depositType").val(); 
		$.ajax({
		  type: 'POST',
		  url: _url,
		  data: {},
		  success: function(data){
		  	data = $.parseJSON(data);
		  	if(data.result=="notLogin") {
		  		window.location.href="${systemSetting().my}/account/login"
		  	}
		  	if(data.result=="ok") {
			  	var url = "${systemSetting().card}/order/toPlacPayFromDetail?productId="+$("#productID").val()+ 
				"&buyCount=" + $("#inputBuyNum").val() + 
				"&buySpecID=" + $("#specIdHidden").val()+
				"&depositType="+$("#depositType").val();  
				document.formPlacPay.action = url;
			 	$("#formPlacPay").submit();
		  	} else {
		  		alert(data.result);
		  	}
		 	
	 	 },
		  dataType: "text",
		  error:function(er){
			  console.log("addToFavorite.er="+er);
		  }
		});
	
	 }
	 
	 var depositPrice = ${e.depositPrice!""}; //定金全局变量
	 var nowPrice = ${e.nowPrice!""};          //全款价格全局变量
	 
	 //得到商品购买状态
	 function getdepositType(who){
	 	$("#depositType").val($(who).val());
	 	if($(who).val()=="1"){
	 		$("#depositPrice").text(depositPrice);
	 	}
	 	else if($(who).val()=="2"){
	 		$("#depositPrice").text($("#nowPrice").text());
	 	}
	 }
</script>
</#macro>