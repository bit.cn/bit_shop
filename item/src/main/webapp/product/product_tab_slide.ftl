<!-- 商品属性切换卡 -->
<div class="st_par_d" >
			<table class="st_par_table" border="1" cellspacing="0" cellpadding="0">
					<tr class="st_par_tr">
						<td colspan="4">
							<div id="divgg"><a  id="sptx" onclick="spgg()"  class="st_exps">商品介绍</a></div>
							&nbsp;&nbsp; <div id="divcs">
							<a id="aspcs" href="javascript:void(0);" onclick="spcs()">规格参数</a></div>
							&nbsp; &nbsp;
							<#--商品评论,需求待确认,暂时去除-->
							<#--<div id="divpj">-->
								<#--<a id="asppj" href="javascript:void(0);" onclick="sppj()">评价详情-->
									<#--<span class="pj_total">(${pager.total})</span>-->
								<#--</a>-->
							<#--</div>-->

							&nbsp;&nbsp; <div id="divsh"><a id="ashbz" onclick="spbz()">售后保障</a></div>
						</td>
					</tr>
			</table>
			<div class="st_par_d2"  id="spgg">
				<div class="st_par_d3">
				<table>
					<!--商品介绍-->
					<tr class="st_par_td">
						<td style="width:273px;padding-left:20px;" id="name">
								商品名称：
								<#if e.name??>
									<#if e.name?length gte 7>
										${e.name?substring(0,7)}...
									<#else>
										${e.name!""}
									</#if>
								</#if>
 						</td>
						<td style="margin-left:0px;">
							        一级分类：${e.mainCatalogName!""}
						</td>
						<td id="brandName" style="width:300px;padding-left:60px;">
								二级分类：${e.childrenCatalogName!""}
						</td>
					</tr>
					<!--规格参数-->
					<tr class="st_par_td"  >
						<td style="width:273px;padding-left:20px;" id="specColor">
								所属品牌：${e.brandName!""}
						</td>
						<td style="margin-left:0px;">
								店铺名称：<#if business??>${business.businessName!""}</#if>
						</td>
						<td style="padding-left:60px;">
							上架时间：&nbsp;${e.groundingTime!""}
						</td>
					</tr>
					<tr class="st_par_td">
						<td style="padding-left:20px;"  colspan="3">
							商品规格
						</td>
					</tr>
					<#if e.specList??>
						<#list e.specList as sp>
						<tr class="st_par_td">
							<td style="padding-left:20px;"  colspan="3">
								规格一：${sp.specColor!""}规格二：${sp.specSize!""}
							</td>
						</tr>
						</#list>
					</#if>
				</table>
				</div>
				<#--暂不显示商品介绍-->
				<#--<div class="st_par_d4"><@include_page path=productHTMLUrl/></div>-->
				<div class="st_par_d4">${e.productHTML}</div>
			</div>
			<div class="st_par_d3"  id="spcs">
				<div class="st_par_d4">
					<ul class="spxq_ul_style_2" style="width:100%;">
					<li class="spxq_li_style_1">主体</li>
					</ul>
					<ul class="spxq_ul_style_3" style="width:100%;">
					 <!--商品属性-->
                     <#if e.attrList?? && e.attrList?size gt 0>
                        <#list e.attrList as attr>
                             <li style="float: left;text-align:center;margin-left:0px;width:130px;border: 1px solid #D7D7D7;height:40px;border: 1px solid #D7D7D7;background-color:#eaeaea;">
                             ${attr.name!""} :
	                             <#list attr.attrList as item>
	                             <#if (item_index)<1 >
	                             	<li style="margin-left:130px;border: 1px solid #D7D7D7;"><input type="text" style="width:100%" readOnly="true"  value="${item.name}" name="parameterNames" />  
	                             </#if>
	                             </#list>
	                         </li>
                        </#list>
                    </#if>
                    <!--商品参数-->
                    <#if e.parametersList?? && e.parametersList?size gt 0>
                        <#list e.parametersList as param>
                             <li style="float: left;text-align:center;margin-left:0px;width:130px;border: 1px solid #D7D7D7;height:40px;border: 1px solid #D7D7D7;background-color:#eaeaea;">
                                 ${param.name!""} :
	                         </li>
	                         <li style="margin-left:130px;border: 1px solid #D7D7D7;"><input type="text" style="width:100%" readOnly="true"  value="${param.parameterValue!""}" name="parameterNames" />  
                        </#list>
                    </#if>
					</ul>
				</div>	
			</div>
			<!--商品评论 评价详情-->
			<#--需求待确认,暂时去除评论-->
			<#--<div id="sppj_new">	-->
				<#--<#list pager.list as item>		-->
				<#--<div class="pjxq_div_1">-->
					<#--<ul class="pjxq_ul_1">-->
						<#--<li><span>${item.content!""}</span><span style="float:right">${item.createdate!""}</span></li>-->
						<#--<li class="pjxq_li_5"><p class="pjxq_p_2" >卖家回复:${item.reply!""}</p></li>-->
					<#--</ul>			-->
				<#--</div>-->
				    <#--<#if pager.list??>-->
						<#--<#include "/pager.ftl"/>-->
					<#--<#else>-->
						<#--该商品暂无评论！-->
					<#--</#if>-->
				  <#--</#list>-->
			<#--</div>-->

			<div id="spsh" class="st_par_d3">
				<span class="st_par_td">
					<strong>${e.productSecurity!""}</strong>				
				</span>	
			</div>
				
</div>