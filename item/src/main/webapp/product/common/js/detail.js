// JavaScript Document   jason
$(document).ready(function(){ 
	//隐藏商品评价
	$("#sppj_new").hide();
    $(".pjxq_div_1").hide();
    $("#sppjss").hide();
  //隐藏售后保障
    $("#spsh").hide();
    $("#spshs").hide();
    $("#spshss").hide();
    //隐藏商品参数
    $("#spcs").hide();
    $("#spcss").hide();
    $("#spcsss").hide();
    //窗体加载 检查商品状态
    spStatus();
    //窗体加载商品规格样式
    $("#divgg").addClass("bg");
    //显示商家回复内容
    sjreply();
    //商品评价星级
    star();
    isfavoritess();
    
    $("#inputBuyNum").blur(function(){
    	var _obj =$("#inputBuyNum");
    	var quantity = _obj.val();
    	var stock = $("#stockHidden").val();
    	if(stock>0 && quantity>stock)
    	{
    		alert("超出商品库存数量！");
    		return;
    	}
    	var productRestrictions = parseInt(_obj.parent().find("input[name=productRestrictions]").val());
    	if(productRestrictions>0 && quantity>=productRestrictions)
    	{
    		alert("超出商品限购数量！");
    		return;
    	}
    });
})
function big(t){
	document.getElementById(t).style.height=40+"px"
	document.getElementById(t).style.background="#EEEEEE";
	document.getElementById(t).style.marginTop=0+"px";


}
function smell(t){
	document.getElementById(t).style.height=30+"px"
	document.getElementById(t).style.background="white";
	document.getElementById(t).style.marginTop=10+"px";

}

function fdd(t) {
	document.getElementById('imgtest').style.display="block";
	var e = t || window.event;
	var img_url = document.getElementById(t).src;
	document.getElementById("imgtest").innerHTML='<img src="'+img_url+'"id="show" style="width: 200%; height: 200%;" />'
	document.onmousemove=function(e){
		e=e? e:window.event;
		var x=parseInt(e.screenX)*(-1.5)+250;
		var y=parseInt(e.screenY)*(-1)+350;
		document.getElementById("show").style.marginLeft=x+"px";
		document.getElementById("show").style.marginTop=y+"px";
		//document.getElementById("imgtest2").style.marginLeft=parseInt(x)+"px"
		//document.getElementById("imgtest2").style.marginTop=parseInt(y)+"px"

	}

}
function th(t){
	var img_url = document.getElementById(t).src;
	document.getElementById("image").src=img_url;
}
function miss(t){
	document.getElementById(t).style.display="none";
}

//商品类型js
function spStatus(){
	var nowDate;
	var id=$(".det_par_tone").html();
	
	var arr1=$("#saleatDate").val().split(/-|:| /);
	var saleatDate=new Date(arr1[0],arr1[1],arr1[2],arr1[3],arr1[4],arr1[5]);		//发售时间
	
	var arr2=$("#replenishmentdeplaytime").val().split(/-|:| /);
	var replenishmentdeplaytime = new Date(arr2[0],arr2[1],arr2[2],arr2[3],arr2[4],arr2[5]);  //延迟补款时间
	
	var arr3=$("#replenishmentTime").val().split(/-|:| /);
	var replenishmentTime =new Date(arr3[0],arr3[1],arr3[2],arr3[3],arr3[4],arr3[5]);              //补款开始时间
	
	var arr4=$("#replenishmentEndTime").val().split(/-|:| /);
	var replenishmentEndTime =new Date(arr4[0],arr4[1],arr4[2],arr4[3],arr4[4],arr4[5]);         //补款结束时间
	
	var arr5=$("#sellTime").val().split(/-|:| /);
	var sellTime=new Date(arr5[0],arr5[1],arr5[2],arr5[3],arr5[4],arr5[5]);
	
	var arr6=$("#sellEndTime").val().split(/-|:| /);
	var sellEndTime=new Date(arr6[0],arr6[1],arr6[2],arr6[3],arr6[4],arr6[5]);
	
	var saleatDate1 = Date.parse(saleatDate);    //发售日期时间戳
	var sellTime1 = Date.parse(sellTime);
	var sellEndTime1 = Date.parse(sellEndTime);
	var replenishmentTime1 =  Date.parse(replenishmentTime);
	var replenishmentdeplaytime1 =  Date.parse(replenishmentdeplaytime);
	var replenishmentEndTime1 = Date.parse(replenishmentEndTime);      						   //补款结束时间
	//var sellTime = $("#sellTime").val().replace(/\-/g, "\/");                                //商品购买时间或者预售时间
	//var sellEndTime=$("#sellEndTime").val().replace(/\-/g, "\/");                            //商品购买结束时间或预售结束时间
	//var nowDate=Date.parse(new Date());
	var stock = $("#stock").val(); 
	var status = $("input[name=status]").val();
	if(stock=="0" || status=="3")
	{
		if (id=="1") 
		{
			$(".det_par_tone").html("贩售结束");
			$(".ljgm_styles_3").show();
			return;
		}
		if (id=="2") 
		{
			$(".det_par_tone").html("预售结束，等待补款");
		}
		return;
	}
	var systemSettingitem = $("#systemSettingitem").val();
	var getSystemTimeurl =systemSettingitem+"/product/getSystemTime";
	$.ajax({
		    
			url:getSystemTimeurl,
		    success:function(data) {
		    	var arr = data.split(/-|:| /);
		    	nowDate= Date.parse(new Date(arr[0],arr[1],arr[2],arr[3],arr[4],arr[5]));
		    	if(nowDate<saleatDate1 || $("#saleatDate").val()==null || $("#saleatDate").val()=="")
		    	{
		    		$(".det_par_tone").html("即将开售");
		    		return;
		    	}
		    	//现货
		    	if (id=="1") {
		    		if(nowDate>saleatDate1)
		    		{
		    			$(".det_par_tone").html("现货贩售中");
		    			if(nowDate<sellTime1)
			    		{
			    			$(".det_par_tone").html("即将开售");
			    			return;
			    		}
		    			if(sellTime1<nowDate && nowDate<sellEndTime1)
			    		{
			    			$(".det_par_tone").html("现货贩售中");
			    			$(".jrcar_style").show();
			    			return;
			    		}
		    			else if(sellTime1<nowDate && nowDate>sellEndTime1)
		    			{
		    				$(".det_par_tone").html("贩售结束");
		    				$(".ljgm_styles_3").show();
			    			return;
		    			}
		    			return;
		    		}
		    	} 
		    	//预售状态
		    	if (id=="2") {
		    		if(sellTime1<nowDate && nowDate<sellEndTime1)
		    		{
		    			$(".det_par_tone").html("预售中");
		    			$(".jrcar_style").show();
		    			return;
		    		}
		    		else if(nowDate<sellTime1)
		    		{
		    			$(".det_par_tone").html("即将开售");
		    			return;
		    		}
		    		if(sellTime1<nowDate && nowDate>sellEndTime1)
		    		{
		    			$(".det_par_tone").html("预售结束，等待补款");
		    			if($("#replenishmentdeplaytime").val()!=null && $("#replenishmentdeplaytime").val()!="")
		    			{
		    				if(replenishmentTime1<nowDate && nowDate<replenishmentdeplaytime1)
		    				{
		    					$(".det_par_tone").html("补款中");
		    					$(".ljgm_styles_1").show();
		    					return;
		    				}
		    				else
		    				{
		    					//补款开始
		    					$(".det_par_tone").html("补款结束，已截单");
		    					return;
		    				}
		    			}
		    			else
		    			{
		    				if(replenishmentTime1<nowDate && nowDate<replenishmentEndTime1)
		    				{
		    					$(".det_par_tone").html("补款开始");
		    					$(".ljgm_styles_1").show();
		    					return;
		    				}
		    				else if($("#replenishmentTime").val()=="" || $("#replenishmentTime").val()==null)
		    				{
		    					$(".det_par_tone").html("预售结束，等待补款");
				    			return;
		    				}
		    				else
		    				{
		    					//补款开始
		    					$(".det_par_tone").html("补款结束，已截单");
		    					$(".ljgm_styles_3").show();
		    					return;
		    				}
		    			}
		    			return;
		    		}
		    		if($("#replenishmentdeplaytime").val()!=null && $("#replenishmentdeplaytime").val()!="")
		    		{
		    			if(replenishmentTime1<nowDate && nowDate<replenishmentdeplaytime1)
		    			{
		    				$(".det_par_tone").html("补款中");
		    				$(".ljgm_styles_1").show();
		    				return;
		    			}
		    			else
		    			{
		    				//补款开始
		    				$(".det_par_tone").html("预售结束，等待补款");
		    				return;
		    			}
		    		}
		    		else
		    		{
		    			if(replenishmentTime1<nowDate && nowDate<replenishmentEndTime1)
		    			{
		    				$(".det_par_tone").html("补款开始");
		    				$(".ljgm_styles_1").show();
		    				return;
		    			}
		    			else
		    			{
		    				//补款开始
		    				$(".det_par_tone").html("补款结束，已截单");
		    				return;
		    			}
		    		}
		    	}
		    	if(id>"3")
		    	{
		    		$(".det_par_tone").html("");
		    		return;
		    	}
		    }
	});
}
//商家回复内容
function sjreply() {
    var reply=$(".reply_sj").html();
    if (reply="" || reply==null) {
		$(".pjxq_li_5").html("");
	}
}
//商品评价星级
function star() {
	 var star=$(".pjxq_star_p").html();
	 if (star==1) {
		 $(".pjxq_star_p").html("");
		 $(".pjxq_star_img1").addClass("pjxq_star_bg");
		 $(".pjxq_star_img2").addClass("pjxq_star_bg2");
		 $(".pjxq_star_img3").addClass("pjxq_star_bg2");
		 $(".pjxq_star_img4").addClass("pjxq_star_bg2");
		 $(".pjxq_star_img5").addClass("pjxq_star_bg2");
	}
	 if (star==2) {
		 $(".pjxq_star_p").html("");
		 $(".pjxq_star_img1").addClass("pjxq_star_bg");
		 $(".pjxq_star_img2").addClass("pjxq_star_bg");
		 $(".pjxq_star_img3").addClass("pjxq_star_bg2");
		 $(".pjxq_star_img4").addClass("pjxq_star_bg2");
		 $(".pjxq_star_img5").addClass("pjxq_star_bg2");
		}
	 if (star==3) {
		 $(".pjxq_star_p").html("");
		 $(".pjxq_star_img1").addClass("pjxq_star_bg");
		 $(".pjxq_star_img2").addClass("pjxq_star_bg");
		 $(".pjxq_star_img3").addClass("pjxq_star_bg");
		 $(".pjxq_star_img4").addClass("pjxq_star_bg2");
		 $(".pjxq_star_img5").addClass("pjxq_star_bg2");
		}
	 if (star==4) {
		 $(".pjxq_star_p").html("");
		 $(".pjxq_star_img1").addClass("pjxq_star_bg");
		 $(".pjxq_star_img2").addClass("pjxq_star_bg");
		 $(".pjxq_star_img3").addClass("pjxq_star_bg");
		 $(".pjxq_star_img4").addClass("pjxq_star_bg");
		 $(".pjxq_star_img5").addClass("pjxq_star_bg2");
		}
	 if (star==5) {
		 $(".pjxq_star_p").html("");
		 $(".pjxq_star_img1").addClass("pjxq_star_bg");
		 $(".pjxq_star_img2").addClass("pjxq_star_bg");
		 $(".pjxq_star_img3").addClass("pjxq_star_bg");
		 $(".pjxq_star_img4").addClass("pjxq_star_bg");
		 $(".pjxq_star_img5").addClass("pjxq_star_bg");
		}
}


function th(t){
    var img_url = document.getElementById(t).src;
    document.getElementById("image").src=img_url;
}
//点击商品评价
function sppj(){
	$("#divcs").removeClass("bg");
	$("#divsh").removeClass("bg");
	$("#divgg").removeClass("bg");
	$("#divpj").addClass("bg");
     $("#spgg").hide();
     $("#spggs").hide();
     $("#spggss").hide();
     $("#h5_style").hide();
    
     $("#sppj_new").show();
     $(".pjxq_div_1").show();
     $("#sppjss").show();
     $("#spsh").hide();
     $("#spshs").hide();
     $("#spshss").hide();
     $("#spcs").hide();
     $("#spcss").hide();
     $("#spcsss").hide();
}
//商品规格显示
function spgg(){
	$("#divcs").removeClass("bg");
	$("#divsh").removeClass("bg");
	$("#divpj").removeClass("bg");
	$("#divgg").addClass("bg");
	$("#sppj_new").hide();
	$(".pjxq_div_1").hide();
    $("#sppjss").hide();
    $("#h5_style").show();
    $("#spgg").show();
    $("#spggs").show();
    $("#spggss").show();
    $("#spsh").hide();
    $("#spshs").hide();
    $("#spshss").hide();
    $("#spcs").hide();
    $("#spcss").hide();
    $("#spcsss").hide();
}
//商品保障
function spbz(){
	$("#divcs").removeClass("bg");
	$("#divpj").removeClass("bg");
	$("#divgg").removeClass("bg");
	$("#divsh").addClass("bg");
	$("#spgg").hide();
	$("#spggs").hide();
	$("#spggss").hide();
	$("#h5_style").hide();
	$("#sppj_new").hide();
	$(".pjxq_div_1").hide();
    $("#sppjss").hide();
    $("#spsh").show();
    $("#spshs").show();
    $("#spshss").show();
    $("#spcs").hide();
    $("#spcss").hide();
    $("#spcsss").hide();
}
//商品参数
function spcs(){
	$("#divsh").removeClass("bg");
	$("#divgg").removeClass("bg");
	$("#divpj").removeClass("bg");
	$("#divcs").addClass("bg");
	$("#sppj_new").hide();
	$(".pjxq_div_1").hide();
    $("#sppjss").hide();
	$("#spgg").hide();
	$("#spggs").hide();
	$("#spggss").hide();
	$("#h5_style").hide();
	$("#spsh").hide();
	$("#spshs").hide();
	$("#spshss").hide();
    $("#spcs").show();
    $("#spcss").show();
    $("#spcsss").show();
}

//检查库存是否超出数量
function checkStockFunc(){
	var inputBuyNum = parseInt($("#inputBuyNum").val());
	var stockHidden = parseInt($("#stockHidden").val());
	if(inputBuyNum>stockHidden){
		alert("购买的商品数量大于库存数！");
		$('#new_style_main2').removeAttr('onclick');
		$('#jrcar_style').removeAttr('onclick');
		console.log("购买的商品数量大于库存数！");
		$("#inputBuyNum").val(1);
		return false;
	}
	return true;
}

//商品详情页点击点加号增加商品数量
function add(who) {	
	var inputBuyNum=$("#inputBuyNum").val();
	var addNumber=$("#addNumber").val();
	$("#inputBuyNum").val(parseInt(inputBuyNum)+1);
	if (parseInt(addNumber)>0 && parseInt($("#inputBuyNum").val())>addNumber) {
		$("#inputBuyNum").val(addNumber);
		alert("该商品限购"+addNumber+"个");
	}
	checkStockFunc();
}

//商品详情页点击减号减少商品数量
function subtract() {
	var inputBuyNum=$("#inputBuyNum").val();
	$("#inputBuyNum").val(inputBuyNum-1);
	if ($("#inputBuyNum").val()<=1) {
		$("#inputBuyNum").val(1);
	}
	checkStockFunc();
}

function change(t,g){
	if(t=="spsm"){
		document.getElementById(g).style.display = "block";
		document.getElementById(t).style.height = 40 + "px"
		document.getElementById(t).style.background = "white"
		document.getElementById(t).style.marginTop = -27 + "px"
	}
	else{
		document.getElementById(g).style.display = "block";
		document.getElementById(t).style.height = 40 + "px"
		document.getElementById(t).style.background = "white"
		document.getElementById(t).style.marginTop = -27 + "px"
		document.getElementById('line1').style.display="none";
		document.getElementById('spsm').style.height=28+"px"
		document.getElementById('spsm').style.background="#f3f3f3"
		document.getElementById('spsm').style.marginTop=-15+"px"
	}
}
function restore(t,g) {

	document.getElementById(g).style.display = "none";
	document.getElementById(t).style.height = 28 + "px"
	document.getElementById(t).style.background = "#f3f3f3"
	document.getElementById(t).style.marginTop = -15 + "px"


}
function pd(){
	document.getElementById('line1').style.display = "block";
	document.getElementById('spsm').style.height = 40 + "px"
	document.getElementById('spsm').style.background = "white"
	document.getElementById('spsm').style.marginTop = -27 + "px"
}
function isfavoritess(){
//	var isNewFavorite=eval(document.getElementById('getisnewfavorite')).value
//    var isFavoriteBusiness=eval(document.getElementById('isnewfavoriteBusiness')).value
//	if(isNewFavorite==1){
//		$('#addToFavoriteBtn').hide();
//		  $('#isFavoriteC').show();
//	}
//	else{
//		$('#addToFavoriteBtn').show();
//		  $('#isFavoriteC').hide();
//		
//	}
//	if(isFavoriteBusiness==1){
//		$('#isnewBusinessF').hide();
//		  $('#isnewBusiness').show();
//	}
//	else{
//		$('#isnewBusinessF').show();
//		  $('#isnewBusiness').hide();
//
//	
//	}
}

//添加商铺收藏
function addToFavoritedo(businessID,id){
	$("#isnewBusinessF").attr("disabled",'disabled');
	$('#isnewBusinessF').removeAttr('href');//去掉a标签中的href属性
	$('#isnewBusinessF').removeAttr('onclick');//去掉a标签中的onclick事件
	var systemSettingitem = $("#systemSettingitem").val();
	var _url = systemSettingitem+"/favorite/addFavoriteBusi?businessId="+businessID+"&id="+id;
	console.log("_url="+_url);
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  success: function(data){
		  console.log("addToFavoritedo.data="+data);
		  var _result = "商铺已成功添加到收藏夹！";
		  if(data=="0"){
			  _result = "商铺已成功添加到收藏夹！";
			
		  }else if(data=='1'){
			  _result = "已添加，无需重复添加！";
		  }else if(data=='-1'){//提示用户要先登陆
			  _result = "使用此功能需要先登陆！";
		  }
		    alert(_result);
		    $('#isnewBusinessF').hide();
			  $('#isnewBusiness').show();
		 
	  },
	  dataType: "text",
	  error:function(er){
		  console.log("addToFavorite.er="+er);
	  }
	});
}

//判断是否是正整数
function IsNum(s)
{
    if(s!=null){
        var r,re;
        re = /\d*/i; //\d表示数字,*表示匹配多个数字
        r = s.match(re);
        return (r==s)?true:false;
    }
    return false;
}

//通知购物车
function notifyCart(_obj){
	//var _url = "cart/notifyCart.html?currentBuyNumber="+_obj.val()+"&productID="+_obj.attr("pid")+"&date="+(new Date().getTime());
	var _url = basepath +"/cart/notifyCart.html?currentBuyNumber="+_obj.val()+"&productID="+_obj.attr("pid")+"&radom="+Math.random();
	console.log("_url="+_url);
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  cache:false,
	  success: function(data){
		  console.log("notifyCart.data="+data);
		  if(data.code=='notThisProduct'){
			  console.log("notifyCart.data.code=notThisProduct");
			  _obj.parent().find("a[name=stockErrorTips]").attr("data-original-title",data.msg).tooltip('show');
		  }else  if(data.code=='buyMore'){
			  console.log("notifyCart.data.code=buyMore");
			  alert("超出库存！");
			 // _obj.parent().find("a[name=stockErrorTips]").attr("data-original-title",data.msg).tooltip('show');
		  }else  if(data.code=='ok'){
			  console.log("notifyCart.data.code=ok");
			  var _tips_obj = _obj.parent().find("a[name=stockErrorTips]");
			  _tips_obj.tooltip('hide');
			  _tips_obj.attr("data-original-title",'');
			 // $("#totalPayMonery").text(data.amount);
			  $("#totalExchangeScore").text(data.amountExchangeScore);
			  $("#amount").val(data.amount);
			  $("#allbuyCount").val(data.amount);
			  _obj.parent().parent().parent().find("span[total0=total0]").text(data.total0);
		  }
	  },
	  dataType: "json",
	  error:function(er){
		  console.log("notifyCart.er="+er);
		  //$.each(er,function(index,value){
			//  console.log("index="+index+",value="+value);
		  //});
	  }
	});
}

//加入购物车
function addToCart(){
	var _obj = $("input[name=inputBuyNum]");
	var quantity = parseInt(_obj.val());
	//console.log("_obj="+_obj+",notifyCartFlg="+notifyCartFlg+",quantity="+quantity+",pid="+_obj.attr("pid"));
	var productRestrictions = parseInt($("input[name=productRestrictions]").val());
	if(productRestrictions>0 && quantity>productRestrictions)
	{
		alert("超出商品限购数量！");
		return;
	}
	var _specIdHidden = $("#specIdHidden").val();
	var specJsonStringVal = $("#specJsonString").val();
	//如果规格存在
	//console.log("specIdHidden = " + _specIdHidden);
	if(specJsonStringVal && specJsonStringVal.length>0){
		if(!_specIdHidden || _specIdHidden==''){
			alert("请选择规格！");
			return;
		}
	}
	
	if(!checkStockFunc()){
		return false;
	}
	var systemSettingitem = $("#systemSettingitem").val();
	var systemSettingmy = $("#systemSettingmy").val();
	var _url = systemSettingitem+"/cart/addToCart";
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {"productID":$("#productID").val(),"buyCount":$("#inputBuyNum").val(),"buySpecID":$("#specIdHidden").val(),"depositType":$("#depositType").val()},
	  success: function(data){
		  console.log("data="+data);
		if(data==0)
		{
			$('#myModal').modal('toggle');
		}
		else if(data==-1)
		{
			location.href=systemSettingmy + "/account/login.html";
		}
		else
		{
			console.log("出现错误。data.tips="+data.tips);
			if(data.tips!=null)
			{
				alert(data.tips);
			}
			
		}
	  },
	  dataType: "json",
	  error:function(e){
		  console.log("加入购物车失败！请联系站点管理员。异常:"+e);
		  alert("加入购物车失败！请联系客服寻求解决办法！");
	  }
	});
}

//最后一次检查库存
function checkStockLastTime(){
	var _url = basepath+"/cart/checkStockLastTime?radom="+Math.random();
	console.log("_url="+_url);
	var result;
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  async:false,
	  cache:false,
	  success: function(data){
		  console.log("notifyCart.data="+data);
		  
		  if(data=="-1"){
			  console.log("提示用户需要登录！");
			  $("#confirmOrderBtn").attr("data-original-title","需要先登陆，才能提交订单！").tooltip('show');
			  result = false;
			  
		  }else if(data.code=='login'){
			  console.log("notifyCart.data.code=login");
		  }else  if(data.code=='result'){
			  if(!data.list && !data.error){
				 console.log("true");
				 result = true;
			  }else{
				  $.each(data.list,function(index,value){
					  console.log("each>>"+index+","+value);
					  $("a[name=stockErrorTips]").each(function(){
						  console.log("each2>>"+value.id);
						  if($(this).attr("productid")==value.id){
							  $(this).attr("data-original-title",value.tips).tooltip('show');
						  }
					  });
				  });
				  console.log("false");
				  data.error = "按钮错误！";
				  $("#confirmOrderBtn").attr("data-original-title",data.error).tooltip('show');
				  result = false;
			  }
		  }
	  },
	  dataType: "json",
	  error:function(er){
		  console.log("notifyCart.er="+er);
		  result = false;
	  }
	});
	return result;
}
