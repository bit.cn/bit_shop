
<!-- 产品明细页面中间位置的图片列表 -->
<style type="text/css">
/* html{overflow-y:scroll;} */
/* body{margin:0; font:12px "\5B8B\4F53",san-serif;background:#ffffff;} */
div,ul,li{padding:0; margin:0;}
li{list-style-type:none;}
img{vertical-align:top;border:0;}

/* box */
.box{
/* 	width:366px; */
}
.tb-pic a{display:table-cell;text-align:center;vertical-align:middle;}
.tb-pic a img{vertical-align:middle;}
.tb-pic a{*display:block;*font-family:Arial;*line-height:1;}
.tb-thumb{margin:10px 0 0;overflow:hidden;}
.tb-thumb li{background:none repeat scroll 0 0 transparent;float:left;max-height:42px;max-width:42px;margin:0 6px 0 0;overflow:hidden;padding:1px;}
.tb-s310, .tb-s310 a{height:300px; border: 2px solid #f3f3f3;width: 260px;}
/*#new_style_Main_div{margin-left:-50px;}*/
.tb-s310, .tb-s310 img{height:300px;width: 260px;}
.tb-s310 a{*font-size:271px;}
.tb-s40 a{*font-size:35px;}
.tb-s40, .tb-s40 a{max-width: 40px;}
.tb-booth{border:0px solid #CDCDCD;position:relative;z-index:1;}
.tb-thumb .tb-selected{border: 2px solid #C30008;}
.tb-thumb .tb-selected div{background-color:#FFFFFF;border:medium none;}
.tb-thumb li div{border:1px solid #CDCDCD;}
div.zoomDiv{z-index:999;position:absolute;top:0px;left:0px;width:200px;height:200px;background:#ffffff;border:1px solid #CCCCCC;display:none;text-align:center;overflow:hidden;}
div.zoomMask{position:absolute;background:url("${basepath}/resource/js/jquery.imagezoom/images/mask.png") repeat scroll 0 0 transparent;cursor:move;z-index:1;
	width:20px;height:20px;
}
</style>
	<div class="tb-booth tb-pic tb-s310" id="new_style_Main_div" >
		<#list e.productImageList as img>
			<#if img_index==0>
				<a href="${systemSetting().imageRootPath}${img.image3!""}">
				<img src="${systemSetting().imageRootPath}${img.image2!""}" rel="${systemSetting().imageRootPath}${img.image3!""}" class="jqzoom" id="image" onmousemove="fdd('image')" onmouseout="miss('imgtest'),miss('fdg_fdj')"/>
				</a>
			</#if>
		</#list>
	</div>
	<ul class="tb-thumb" id="thumblist">
	<#list e.productImageList as img>
				<#if img_index==0>
					<li class="tb-selected">
				<#else>
					<li>
				</#if>
					<div class="tb-pic tb-s40" style="width: 50px;height: 50px;">    
						<a href="#"><img style="max-width: 50px;max-height: 50px;" src="${systemSetting().imageRootPath}${img.image1!""}"
						mid="${systemSetting().imageRootPath}${img.image2!""}"
						big="${systemSetting().imageRootPath}${img.image3!""}"></a>
					</div>
				</li>
	</#list>
	</ul>
</div>

<input type="hidden" id="getisnewfavorite" value="${isnewfavorite!""}"/>
<#if currentAccount()??>
<button id="addToFavoriteBtn" type="button" onclick="addToFavorite('${e.id!""}')" class="btn btn-primary btn-sm"  
		data-container="body" data-toggle="popover" data-placement="right" data-content="" style="  margin-top: 20px;">
		<span class="glyphicon glyphicon-flag" style="float:left"></span>  
		<b>收藏该商品</b> 
</button>
<button  class="btn btn-primary btn-sm" id="isFavoriteC" onclick="isCollection()"  style="margin-top: 20px;display:none;">
		<span class="glyphicon glyphicon-flag" style="float:left"></span>  <b    >已收藏</b>
</button>
 <#else >
 <button id="addToLogin" type="button" onclick="addToLogin()" class="btn btn-primary btn-sm"  
		data-container="body" data-toggle="popover" data-placement="right" data-content="" style="  margin-top: 20px;">
		<span class="glyphicon glyphicon-flag" style="float:left"></span>  
		<b>收藏该商品</b> 
</button>
</#if>



<script type="text/javascript">
//添加商品收藏
function addToFavorite(productID){
	var _url = "${basepath}/favorite/addFavoriteProduct?id="+productID+"&radom="+Math.random();
	console.log("_url="+_url);
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  success: function(data){
		  console.log("addToFavorite.data="+data);
		  var _result = "商品已成功添加到收藏夹！";
		  if(data=="0"){
			  _result = "商品已成功添加到收藏夹！";
		  }else if(data=='1'){
			  _result = "已添加，无需重复添加！";
		  }else if(data=='-1'){//提示用户要先登陆
			  _result = "使用此功能需要先登陆！";
		  }
		    alert(_result);
		  $('#addToFavoriteBtn').attr("data-content",_result).popover("toggle");
	  },
	  dataType: "text",
	  error:function(er){
		  console.log("addToFavorite.er="+er);
	  }
	});
}

//跳转到登陆
function addToLogin(){
	location.href="${systemSetting().my}/account/login";
}
</script>
