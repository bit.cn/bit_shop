<#macro htmlBase title="" jsFiles=[] cssFiles=[] staticJsFiles=[] staticCssFiles=[] checkLogin=true>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <#assign non_responsive2>y</#assign>
    <#assign responsive>${Session["responsive"]!""}</#assign>
    <#if responsive == "y">
        <#assign non_responsive2>n</#assign>
    <#elseif systemSetting().openResponsive == "n">
        <#assign non_responsive2>y</#assign>
    <#else >
        <#assign non_responsive2>n</#assign>
    </#if>
    <script>
        var basepath = "${basepath}";
        var staticSource = "${systemSetting().staticSource}/resource";
        var non_responsive2 = "${non_responsive2}";
        <#if currentUser()??>
            var login = true;
        var currentUser = "${currentUser().username}";
       <#else >
        var login = false;
        var currentUser = "";
        </#if>
    </script>
    <#if non_responsive2 != "y">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </#if>
    <meta name="description" content="${systemSetting().description}"/>
    <meta name="keywords" content="${systemSetting().keywords}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/x-icon" href="${systemSetting().shortcuticon}">
    <link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/common.css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/header/header.css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/footer/footer.css">
<#nested />
</#macro>
