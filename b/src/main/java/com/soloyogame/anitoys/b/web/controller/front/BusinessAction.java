package com.soloyogame.anitoys.b.web.controller.front;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soloyogame.anitoys.b.web.util.LoginUserHolder;
import com.soloyogame.anitoys.b.web.util.RequestHolder;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.db.commond.Brand;
import com.soloyogame.anitoys.db.commond.Business;
import com.soloyogame.anitoys.db.commond.BusinessAdvert;
import com.soloyogame.anitoys.db.commond.BusinessRecommend;
import com.soloyogame.anitoys.db.commond.CartInfo;
import com.soloyogame.anitoys.db.commond.Catalog;
import com.soloyogame.anitoys.db.commond.Channel;
import com.soloyogame.anitoys.db.commond.Hotquery;
import com.soloyogame.anitoys.db.commond.IndexImg;
import com.soloyogame.anitoys.db.commond.Navigation;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.commond.PopularityProduct;
import com.soloyogame.anitoys.db.commond.Product;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.service.BrandService;
import com.soloyogame.anitoys.service.BusinessAdvertService;
import com.soloyogame.anitoys.service.BusinessRecommendService;
import com.soloyogame.anitoys.service.BusinessService;
import com.soloyogame.anitoys.service.CatalogService;
import com.soloyogame.anitoys.service.ChannelService;
import com.soloyogame.anitoys.service.HelpService;
import com.soloyogame.anitoys.service.HotqueryService;
import com.soloyogame.anitoys.service.IndexImgService;
import com.soloyogame.anitoys.service.NavigationService;
import com.soloyogame.anitoys.service.NewsService;
import com.soloyogame.anitoys.service.PopularityProductService;
import com.soloyogame.anitoys.service.ProductService;
import com.soloyogame.anitoys.service.TopPictureService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;

/**
 * catalog
 * 店铺信息管理
 * @author shaojian
 * @param
 */
@Controller("frontBusinessAction")
@RequestMapping("business")
public class BusinessAction extends FrontBaseController<Business> 
{
	@Autowired
	private BusinessService businessService;//商品服务
	@Autowired
	private BrandService brandService;      //商品品牌服务
	@Autowired
	private CatalogService catalogService;  //商品品牌服务
	@Autowired
	private ProductService productService;  //商品服务
	@Autowired
	private ChannelService channelService;  //导航服务
	@Autowired
	private BusinessRecommendService businessRecommendService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private IndexImgService indexImgService;
	@Autowired
	private BusinessAdvertService businessAdvertService;
	@Autowired
	private NavigationService nativeService;
	@Autowired
	private HotqueryService hotqueryService;
	@Autowired
	private HelpService helpService;
	@Autowired
	private TopPictureService topPictureService;
	@Autowired
	private RedisCacheProvider redisCacheProvider;

	@Autowired
	private PopularityProductService popularityProductService;

	@Override
	public Services<Business> getService() {
		return businessService;
	}
	
	/**
	 * 查询指定的店铺信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/businessSelect")
	public String business(String businessId,ModelMap model) throws Exception
	{
		//如果商家ID为null则抛出异常
		if(StringUtils.isBlank(businessId))
		{
			throw new NullPointerException("商家ID不可以为null!");
		}
		Business business = businessService.selectById(businessId);
		if(business==null)
		{
			throw new NullPointerException("商家不存在!");
		}
		TopPicture topPicture = new TopPicture();
		topPicture.setBusinessId(businessId);
		topPicture = topPictureService.selectFrontOne(topPicture);
		model.addAttribute("topPicture", topPicture);
		//卖家首页的导航
		Channel channel = new Channel();
		channel.setBusinessId(businessId);
		List<Channel> channelList = channelService.selectList(channel);
		//推荐商品
		BusinessRecommend businessRecommend = new BusinessRecommend();
        businessRecommend.setType(1);
        businessRecommend.setBusinessId(businessId);
        List<BusinessRecommend> businessRecommendList = businessRecommendService.selectList(businessRecommend);
        
        String businessName = business.getBusinessName();
        
        //橱窗商品
        businessRecommend.setType(2);
        businessRecommend.setBusinessId(businessId);
        List<BusinessRecommend> businessRecommendList1 = businessRecommendService.selectList(businessRecommend);
        RequestHolder.getRequest().setCharacterEncoding("utf-8");
        //现货商品状态的值为1
      	Product spotp=new Product();
      	spotp.setProductType(1);
      	spotp.setTop(12);
      	spotp.setBusinessId(businessId);
      	List<Product> spotCommodityProducts=productService.selectFrontProductList(spotp);
      	//预售货商品状态的值为2
      	/*Product product1=new Product();
      	product1.setProductType(2);
      	product1.setBusinessId(businessId);
      	List<Product> replenishmentProduct1=productService.selectList(product1);*/
        
        //补款商品状态的值为2 当前时间在补款开始时间和不快结束时间内 切延迟时间是否为空
      	Product product=new Product();
      	product.setProductType(2);
      	product.setTop(6);
      	product.setBusinessId(businessId);
      	product.setReplenishmentTime("true");
      	List<Product> replenishmentProduct1=productService.selectFrontProductList(product);
      	List<Product> replenishmentProduct = new ArrayList<Product>();
      	for (Product product2 : replenishmentProduct1) 
      	{
			if(product2.getReplenishmentdeplaytime()==null)
			{
				 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				 String str =product2.getName();
				 if(str.length()>=18){
					 product2.setpDisplayName(str.substring(0, 11)+"...");
				 }else{
					 product2.setpDisplayName(product2.getName());
				 }
					
				if(product2.getReplenishmentTime()!=null && 
				   product2.getReplenishmentEndTime()!=null &&
				   sdf.parse(product2.getReplenishmentTime()).getTime()<new Date().getTime() && 
				   new Date().getTime()<sdf.parse(product2.getReplenishmentEndTime()).getTime())
				{
					replenishmentProduct.add(product2);
				}
			}
			else
			{
				 	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				 	 String str =product2.getName();
				 	 if(str.length()>=18){
						 product2.setpDisplayName(str.substring(0, 11)+"...");
					 }else{
						 product2.setpDisplayName(product2.getName());
					 }
					if(sdf.parse(product2.getReplenishmentTime()).getTime()<new Date().getTime() && 
							new Date().getTime()<sdf.parse(product2.getReplenishmentdeplaytime()).getTime())
					{
						replenishmentProduct.add(product2);
					}
			}
		}
      	
      	//最新商品列表
      	Product p = new Product();
		p.setTop(12);
		p.setIsnew(Product.Product_isnew_y);//最新
		p.setBusinessId(businessId);
		p.setProductType(2);
		List<Product> newProductList = productService.selectFrontProductList(p);
      	//人气商品
//		Product hotp = new Product();
//		hotp.setTop(6);
//		hotp.setHot(true);   
//		hotp.setBusinessId(businessId);
//		List<Product> hotProducts = productService.selectFrontProductList(hotp);
		//最新公告列表
      	News news = new News();
      	news.setType("notice");
      	news.setTop(6);
      	news.setBusinessId(businessId);
      	List<News> newsList = newsService.selectFrontList(news);
       //最新情报列表
      	News newss = new News();
      	newss.setType("help");
      	newss.setTop(6);
      	newss.setBusinessId(businessId);
      	List<News> newsLists = newsService.selectFrontList(newss);
     
      	//官网blog
      	News blog = new News();
      	blog.setBusinessId(businessId);
      	blog.setType("blog");
      	blog.setTop(6);
      	List<News> blogList = newsService.selectFrontList(blog);
      	//店铺广告位
      	BusinessAdvert businessAdvert1=new BusinessAdvert();
      	businessAdvert1.setBusinessId(businessId);
      	List<BusinessAdvert> businessAdvertList=businessAdvertService.selectList(businessAdvert1);
      	model.addAttribute("businessAdvertList", businessAdvertList);
      	
      	//热门查询
      	Hotquery hotquery = new Hotquery();
      	hotquery.setBusinessId(businessId);
      	List<Hotquery> hotqueryList = hotqueryService.selectList(hotquery);
      	model.addAttribute("hotqueryList", hotqueryList);
        //查询品牌分类
		Catalog catalog=new Catalog();
	    List<Catalog>  c= catalogService.selectList(catalog);
	    //查询品牌型号
	    Brand brand=new Brand();
	    List<Brand> b=  brandService.selectList(brand);
	   // model.addAttribute("hotProducts", hotProducts);
	    model.addAttribute("businessRecommendList", businessRecommendList);
	    model.addAttribute("businessId", businessId);
	    model.addAttribute("businessName", businessName);
	    model.addAttribute("businessRecommendList1", businessRecommendList1);
	    model.addAttribute("replenishmentProduct", replenishmentProduct);
	    model.addAttribute("channelList", channelList);
		model.addAttribute("catalog", c);
		model.addAttribute("brand", b);
		model.addAttribute("business", business);
		model.addAttribute("newsList", newsList);
		model.addAttribute("newsLists", newsLists);
		model.addAttribute("newProductList", newProductList);
		model.addAttribute("blogList", blogList);
		model.addAttribute("spotCommodityProducts", spotCommodityProducts);
//		//卖家的帮助中心
//		Catalog catalog1 = new Catalog();
//		catalog1.setType("a");
//		catalog1.setPid("0");
//		catalog1.setShowInNav("y");
//		catalog1.setIsDeleted("n");
//		List<Catalog> newCatalogs = catalogService.selectList(catalog1);
//		if(newCatalogs!=null && newCatalogs.size()>0)
//		{
//			for(int i=0;i<newCatalogs.size();i++)
//			{
//				Catalog item = newCatalogs.get(i);
//				//加载此目录下的所有文章列表
//				News news1 = new News();
//				news1.setCatalogID(item.getId());
//				news1.setBusinessId(businessId);
//				news1.setStatus("y");
//				news1.setTop(5);
//				List<News> newsList1 = newsService.selectFrontList(news1);
//				item.setNews(newsList1);
//			}
//		}
//		model.addAttribute("newCatalogs", newCatalogs);
		//商家的友情链接
		Navigation e=new Navigation();
		e.setBusinessId(businessId);
		List<Navigation> navigationList=new ArrayList<Navigation>();
		navigationList=nativeService.selectList(e);
		model.addAttribute("navigationList", navigationList);
		//商家的轮播图
		logger.info("loadIndexImgs...");
		IndexImg indexImg = new IndexImg();
		indexImg.setBusinessId(businessId);
		List<IndexImg> indexImages = indexImgService.selectList(indexImg);
		model.addAttribute("indexImages", indexImages);
		//人气商品列表
		PopularityProduct popularityProduct = new PopularityProduct();
		popularityProduct.setTop(10);
		popularityProduct.setBusinessId(businessId);
		List<PopularityProduct> popularityProductList = popularityProductService.selectList(popularityProduct);
		model.addAttribute("popularityProductList", popularityProductList);
		return "business/business";
	}
	
	/*
	 * 查询补款商品
	 */
	@RequestMapping(value = "replenishmentShop")
	public String replenishmentShop(ModelMap model) throws Exception
	{		
		//补款商品状态的值为3
		Product product=new Product();
		product.setProductType(3);
	    List<Product> p=productService.selectList(product);
	    //现货商品的状态为1
	    Product products=new Product();
	    products.setProductType(1);
	    List<Product> px=productService.selectList(product);
	    model.addAttribute("product", p);
	    model.addAttribute("product", px);
		return "business/businessFirst";
	}
	
	protected CartInfo getMyCart()
    {
    	//return (CartInfo) RequestHolder.getSession().getAttribute(FrontContainer.myCart);
    	Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
    	CartInfo cartInfo = null;
    	if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
    	return cartInfo;
    }
}
