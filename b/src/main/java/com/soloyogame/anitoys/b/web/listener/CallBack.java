package com.soloyogame.anitoys.b.web.listener;

public interface CallBack {
	String callback() throws Exception;
}
