<!DOCTYPE html>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="网上商店系统(jshop_plat) 个性化的网店系统 通用的个人网店系统">
		<meta name="keywords" content="网上商店系统(jshop_plat) 个性化的网店系统 通用的个人网店系统">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="stylesheet" href="../css/common/common.css">
		<link rel="stylesheet" href="../css/header/header.css">
	</head>

	<body>
		<div class="topNav">
			<div class="top_main clearfix">
				<div class="top_left">
					<a href="http://112.124.52.129:9990/jshop">"话不多说买买买"——欢迎来到anitoys</a>
					<span><a href="http://112.124.52.129:9990/jshop/account/login">请登录</a>
						<a href="http://112.124.52.129:9990/jshop/account/register" class="regist">免费注册</a>
					</span>
				</div>
				<div class="top_right">
					<ul>
						<li>
							<span>
							<a href="http://112.124.52.129:9990/jshop/account/orders">我的订单</a>
						</span>
						</li>
						<li>
							<span><a href="http://112.124.52.129:9990/jshop/account/orders?status=dbk">待补款订单<div style="position: absolute;top: -5px;left: 85px;background: #ff6600;border-radius: 3px;color: #FFF;padding: 2px 3px;text-align: center;font: 200 8px Arial, Helvetica, sans-serif;">
                      0
				         </div></a></span>
						</li>
						<li>
							<span>
							<a href="http://112.124.52.129:9990/jshop/account/orders?" target="_self" class="tran">我的anitoys</a>
							<div class="top_right_nav">
								<a href="http://112.124.52.129:9990/jshop/account/orders?status=dsh" target="_blank">已发货订单</a>
								<a href="http://112.124.52.129:9990/jshop/account/custService" target="_blank">售后订单</a>
								<a href="http://112.124.52.129:9990/jshop/coupon/coupons?status=0" target="_blank">优惠券</a>
								<a href="http://112.124.52.129:9990/jshop/favorite/searchUserFavoritePro" target="_blank">收藏的商品</a>
								<a href="http://112.124.52.129:9990/jshop/favorite/searchUserFavoriteBusi" target="_blank">收藏的商品</a>
							</div>
						</span>
						</li>
						<li>
							<span>
							<a href="http://112.124.52.129:9990/jshop/help/xsbz.html" target="_self" class="tran">帮助支持</a>
							<div class="top_right_nav">
								<a href="http://112.124.52.129:9990/jshop/help/xsbz.html" target="_self">帮助中心</a>
							
							</div>
						</span>
						</li>
						<li>
							<span>
							<a href="http://112.124.52.129:9990/jshop/search.html" target="_blank" class="tran">网站导航</a>
							<div class="top_right_nav imgS">
												<a href="../images/卖家大首页.html" target="_blank">
												<img src="../images/2016022811460383.jpg" style="width:100px;height:150px;">
												</a>
											
							</div>
						</span>
						</li>
						<li>
							<span>
						<a href="http://112.124.52.129:9990/jshop/business/businessSelect?businessId=1#" target="_blank" class="tran">关注anitoys</a>
							<div class="top_right_nav w100">
								<a href="http://112.124.52.129:9990/jshop/business/businessSelect?businessId=1#" target="_blank">
									<img src="../images/towcode.png">
								</a>
							</div>
						</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="omt_top"style="position: relative;">
		<div class="omt_top_use">
			<ul class="clearfix">
				<li> <a><img src="../images/logo.png"> </a></li>
				<li class="int_left"style="float:right">
					<form id="searchForm"method="post"action="http://112.124.52.129:9990/jshop/search.html"target="_blank">
						<input type="text"name="key"class="inputtext"value=""id="mq"maxlength="24"onfocus="if(this.value==&#39;ANI大搜查&#39;) {this.value=&#39;&#39;;}this.style.color=&#39;#333&#39;;"onblur="if(this.value==&#39;&#39;) {this.value=&#39;ANI大搜查&#39;;this.style.color=&#39;#999999&#39;;}">
						<button type="submit"onclick="search();"class="subt-btn">搜整站</button>
					</form>
				</li>
			</ul>
			<ul class="inf">
				<li style="float:right">
					<div class="menuc_right">
					 	<!--购物车-->
						<div class="car">
							<div class="carDesc">
								<span class="carlogo"><i>0</i></span>
								<span class="carm"><a href="http://112.124.52.129:9990/jshop/cart/cart.html">我的购物车</a></span>
							</div>
							<div class="carDetail">
								<div class="total">
									<span>共件0商品 <b>共计¥0.00</b></span>
									<a href="http://112.124.52.129:9990/jshop/cart/cart.html">查看购物车</a>
								</div>
							</div>
						</div>
				    </div>
				</li>
			</ul>
		</div>
	</div>
	</body>

</html>