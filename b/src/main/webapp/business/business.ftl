<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top1.ftl" as shopTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
<@htmlBase.htmlBase>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/business/business.css">
	<title>
	<#if businessName??>
		${businessName!""}
	</#if>
	</title>
</head>
<body>
<!--商城主页top页-->
<@shopTop.shopTop/>
<!--卖家导航-->
<div class="menuBar">
    <div class="menu">
        <ul class="clearfix">
            <li><a href="${systemSetting().www}"><b>首页</b></a></li>
            <!-- 类别作为菜单显示 -->
	        <#if channelList??>
	            <#list channelList as channel>
	                <li onmousemove="change_bg('li${channel.sortOrder}')" onmouseout="hy('li${channel.sortOrder}')">
		                <a href="${channel.channelUrl!" "}" target="_blank">
		                	<b>${channel.channelName!""}</b>
		                </a>
	                </li>
	            </#list>
	        </#if>
        </ul>
    </div>
</div>
<div class="pageWrap clearfix" style=" margin-top:20px;margin-bottom:40px;">
    <div class="pLeft">
        <!--橱窗商品-->
        <div class="brand clearfix" style="background:#f3f3f3">
            <div class="b_img2">
                <div class="slides-box">
                    <ul class="slides">
                    <#list indexImages as item>
                        <li>
	                        <a href="${item.link!""}">
	                        <img style="width:500px; height:360px;" src="${systemSetting().imageRootPath}/${item.picture}"/></a>
                        </li>
                    </#list>
                    </ul>
                    <ul class="pagination" style="position: absolute;"></ul>
                    <a href="javascript:void(0)" class="prev"></a>
                    <a href="javascript:void(0)" class="next"></a>
                </div>
            </div>
            <#if  businessAdvertList??>
            <div class="b_simg">
            	<#if businessAdvertList[0]??>
	            	<a href="${businessAdvertList[0].advertLink!""}">
	            		<img style="width:190px; height:360px;"src="${systemSetting().imageRootPath}/${businessAdvertList[0].advertImage!""}"/>
	            	</a>
            	</#if>
            </div>
            <div class="b_simg">
                <div style="margin-bottom:8px">
	                <#if businessAdvertList[1]??>
		                <a href="${businessAdvertList[1].advertLink!""}">
		               		 <img src="${systemSetting().imageRootPath}/${businessAdvertList[1].advertImage!""}"style="height:176px"/>
		                </a>
	                </#if>
                </div>
                <div>
                	<#if businessAdvertList[2]??>
		                <a href="${businessAdvertList[2].advertLink!""}">
		                	<img src="${systemSetting().imageRootPath}/${businessAdvertList[2].advertImage!""}"style="height:176px"/>
		                </a>
	                </#if>
                </div>
            </div>
        </div>
       </#if>
        <!--最新商品-->
        <div class="newProduct">
            <div class="common-title">最新商品 
            <a href="${systemSetting().lists}/special/newest.html?businessId=${businessId!""}"
                                              target="_Blank">更多&gt;&gt;</a></div>
            <div class="common commList">
                <ul class="clearfix">
                <#if newProductList??>
                    <#list newProductList as newProduct>
                        <#if (newProduct_index<12)>
                            <li>
                                <a target="_blank" href="${systemSetting().item}/product/${newProduct.id!" "}.html">
                                    <img src="${systemSetting().imageRootPath}/${newProduct.picture!""}">
                                    <span class="price">&yen;${newProduct.nowPrice!""}</span>
                                    <#if newProduct.productType?? && newProduct.productType==2>
                           			 <span class="ys">预售</span>
                            		</#if>
                                    <p class="p-desc">
                                    <#if newProduct.name??>
						            <#if newProduct.name?length gte 8>
										${newProduct.name?substring(0,8)}...
									<#else>
									${newProduct.name!""}
									</#if>
									</#if>
                                    </p>
                                </a>
                            </li>
                        </#if>
                    </#list>
                </#if>
                </ul>
            </div>
        </div>
        <!--人气商品-->
        <div class="hotProduct">
            <div class="common-title">
                人气商品 <a href="${systemSetting().lists}/special/hot.html?businessId=${businessId!""}" target="_Blank">更多&gt;&gt;</a>
            </div>
            <div class="hot-con commList">
                <ul class="clearfix">
                <#list popularityProductList as hotProducts>
                    <li>
                        <a href="${systemSetting().item}/product/${hotProducts.id!" "}.html">
                            <img src="${systemSetting().imageRootPath}/${hotProducts.picture!""}"/>
                            <span class="price">&yen;${hotProducts.nowPrice!""}</span>
                            <#if hotProducts.productType?? && hotProducts.productType==2>
                           	 <span class="ys">预售</span>
                            </#if>
                            <p class="p-desc">
                            		<#if hotProducts.name??>
						            <#if hotProducts.name?length gte 8>
										${hotProducts.name?substring(0,8)}...
									<#else>
									${hotProducts.name!""}
									</#if>
									</#if>
							</p>
                        </a>
                    </li>
                    <#if (hotProducts_index>2) >
                        <#break>
                    </#if>
                </#list>
                </ul>
            </div>
        </div>
        <!--现货商品-->
        <div class="recommonProduct">
            <div class="common-title">现货商品 <a
                    href="${systemSetting().lists}/special/spot.html?businessId=${businessId!""}"
                    target="_Blank">更多&gt;&gt;</a></div>
            <div class="common commList">
                <ul class="clearfix">
                <#if spotCommodityProducts??>
                    <#list spotCommodityProducts as recommend>
                        <#if (recommend_index<12)>
                            <li>
                                <a href="${systemSetting().item}/product/${recommend.id!" "}.html">
                                    <img src="${systemSetting().imageRootPath}/${recommend.picture!""}">
                                    <span class="price">&yen;${recommend.nowPrice!""}</span>
                                    <p class="p-desc">
                                    <#if recommend.name??>
						            <#if recommend.name?length gte 8>
										${recommend.name?substring(0,8)}...
									<#else>
									${recommend.name!""}
									</#if>
									</#if>
									</p>		
                                </a>
                            </li>
                        </#if>
                    </#list>
                </#if>
                </ul>
            </div>
        </div>
        <!--补款商品-->
        <div class="recommonProduct">
            <div class="common-title">补款商品 <a href="${systemSetting().lists}/special/replenishment.html?businessId=${businessId!""}&&code='replenishment'" target="_Blank">更多&gt;&gt;</a></div>
            <div class="common commList">
                <ul class="clearfix">
                <#if replenishmentProduct??>
                    <#list replenishmentProduct as recommend>
                        <#if (recommend_index<12)>
                            <li>
                                <a href="${systemSetting().item}/product/${recommend.id!" "}.html">
                                    <img src="${systemSetting().imageRootPath}/${recommend.picture}">
                                    <span class="price">&yen;${recommend.nowPrice}</span>
                                    <p class="p-desc">
                                    <#if recommend.name??>
						            <#if recommend.name?length gte 8>
										${recommend.name?substring(0,8)}...
									<#else>
									${recommend.name!""}
									</#if>
									</#if>
                                    </p>
                                </a>
                            </li>
                        </#if>
                    </#list>
                </#if>
                </ul>
            </div>
        </div>
    </div>
    <div class="pRight">
        <div class="info into-smell">
            <div class="info-title">补款商品信息<a href="${systemSetting().lists}/special/replenishment.html?businessId=${businessId!""}">&gt;&gt;</a></div>
            <div class="info-con pro">
                <ul>
                <#if replenishmentProduct??>
                    <#list replenishmentProduct as product>
                        <li>
                            <a class="clearfix" href="${systemSetting().item}/product/${product.id!" "}.html">
                                <div class="lf"><img src="${systemSetting().imageRootPath}/${product.picture}"/></div>
                                <div class="lr">
                                    <p class="gray2">
                                    <#if product.pDisplayName??>
						            <#if product.pDisplayName?length gte 8>
										${product.pDisplayName?substring(0,8)}...
									<#else>
									${product.pDisplayName!""}
									</#if>
									</#if>
                                    </p>
                                    <p>开始补款</p>
                                    <p class="gray">${product.replenishmentTime!""}</p>
                                </div>
                            </a>
                        </li>
                    </#list>
                </#if>
                </ul>
            </div>
        </div>
        <div class="info">
            <div class="info-title ">
            <#if businessName??>
            <#if businessName?length gte 14>
				${businessName?substring(0,14)}...
			<#else>
			${businessName!""}
			</#if>
			</#if>
            	的店铺最新情报<a href="${systemSetting().doc}/news/list?type=notice&businessId=${businessId!""}">更多&gt;&gt;</a></div>
            <div class="info-con">
                <ul>
                <#if newsList??>
                    <#list newsList as news>
                        <li>
                            <span>${news.createtime!""}</span>
                            <a href="${systemSetting().doc}/news/${news.id}?type=notice&businessId=${businessId!""}">
                            		<#if news.title??>
						            <#if news.title?length gte 25>
										${news.title?substring(0,25)}...
									<#else>
									${news.title!""}
									</#if>
									</#if>
                            </a>
                        </li>
                    </#list>
                </#if>
                </ul>
            </div>
        </div>
        <div class="info">
            <div class="info-title ">
	           <#if businessName??>
	            <#if businessName?length gte 14>
					${businessName?substring(0,14)}...
				<#else>
				${businessName!""}
				</#if>
			</#if>的店铺最新商品<a href="${systemSetting().lists}/special/newest.html?businessId=${businessId!""}">更多&gt;&gt;</a></div>
            <div class="info-con">
                <ul>
                <#if newProductList??>
                    <#list newProductList as newProduct>
                        <#if (newProduct_index<12)>
                            <li>
                                <span>${newProduct.groundingTime!""}</span>
                                <a href="${systemSetting().item}/product/${newProduct.id!" "}.html">
                                	<#if newProduct.name??>
						            <#if newProduct.name?length gte 8>
										${newProduct.name?substring(0,8)}...
									<#else>
									${newProduct.name!""}
									</#if>
									</#if>
                                </a>
                            </li>
                        </#if>
                    </#list>
                </#if>
                </ul>
            </div>
        </div>
        <div class="info">
            <div class="info-title z">推荐商品信息</div>
            <div class="info-con pro">
                    <ul>
                    <#if businessRecommendList??>
                    <#list businessRecommendList as businessRecommend>
                        <#if (businessRecommend_index<12)>
                            <li>
                                <span>${businessRecommend.groundingTime!""}</span>
                                <a href="${systemSetting().item}/product/${businessRecommend.productId!" "}.html">
                                <#if businessRecommend.name??>
						            <#if businessRecommend.name?length gte 8>
										${businessRecommend.name?substring(0,8)}...
									<#else>
									${businessRecommend.name!""}
									</#if>
									</#if>
                                </a>
                            </li>
                        </#if>
                    </#list>
                </#if>
                    </ul>
            </div>
        </div>
        <div class="info">
            <div class="info-title ">
            <#if businessName??>
	            <#if businessName?length gte 14>
					${businessName?substring(0,14)}...
				<#else>
				${businessName!""}
				</#if>
			</#if>官方日志<a href="${systemSetting().doc}/news/list?type=blog&businessId=${businessId!""}">更多&gt;&gt;</a></div>
            <div class="info-con log">
                <ul>
                <#if blogList??>
                    <#list blogList as news>
                        <li>
                            <span class="log-title">${news.createtime!""}</span>
                            <a href="${systemSetting().doc}/news/${news.id}?type=notice&businessId=${businessId!""}">${news.title!""}</a>
                        </li>
                    </#list>
                </#if>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--友情连接-->
<div class="links clearfix" style="clear: both;width: 1200px;margin: 0 auto;margin-top: 30px;">
			<hr style="margin: 0px;">
			<div class="row">
				<div class="col-xs-12" style="text-align: center;">
					<div style="text-align: center;margin: auto;">
					 <#if navigationList??>
                		<#list navigationList as item>
						<div style="float: left;margin: 5px;">
							<a href="${item.http!""}" target="_blank">${item.name!""}</a>
						</div>
						</#list>
            		</#if>
					</div>
				</div>
			</div>
		</div>
<form action="${systemSetting().card}/cart/delete" method="POST" id="formDelete">
	<input type="hidden" name="id">
</form>
<!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>

<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/js1.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/poposlides.js"></script>
<script>
    $(".slides").poposlides();
    
    	//搜索商品(整站搜索)
        function search() {
        $("#businessId").remove();
            $("#searchForm").attr("action","${systemSetting().search}/product/search.html")
            var _key = $.trim($("#mq").val());
            if (_key == '') {
                return false;
            }
            $("#searchForm").submit();
        }
        
        //搜索商品(本站搜索)
        function searchbusiness() {
            var _key = $.trim($("#mq").val());
            if (_key == '') {
                return false;
            }
            $("#searchForm").submit();
        }
		//单个删除购物车商品
		function deleteFromCart(productId){
			if(productId){
				$("#formDelete :hidden[name=id]").val(productId);
				$("#formDelete").submit();
			}
		}
</script>
</@htmlBase.htmlBase>
</body>
</html>
