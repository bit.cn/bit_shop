<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html> 
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<meta name="viewport" content="user-scalable=0, width=device-width">
<meta name="format-detection" content="telphone=no, email=no">
<title>订单详情</title>
<style>
html { -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100% }
body {font:100% "Hiragino Sans GB","Microsoft YaHei",Arial,sans-serif; background:#f9f7f7; -moz-user-select:none;-webkit-user-select:none;}
body, h1, h2, h3, h4, h5, p, ul, ol, dl, dd { margin: 0 }
ul,ol{ padding-left: 0; list-style-type: none; list-style-position: inside }
a img{ border: 0 }
.fix, .fix:after{ visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; overflow: hidden }

.main{margin:10px 0; width:100%}
.top{border-bottom:1px solid #e5e5e5; background:#fff}
.title,.money,li{border-top:1px solid #e5e5e5; padding:10px; background:#fff}
.title{font-size:large; font-weight:bold}
.title img{vertical-align:middle; margin-right:10px}
.money{font-size:50px; text-align:right; padding:15px 10px}
.money span{font-size:16px}
ul{margin-top:10px; border-bottom:1px solid #e5e5e5;}
li{position:relative}
li span{float:right; color:#8f8f8f; display:inline-block; width:70%; overflow:hidden; text-align:right}
.marked,.marked span{color:#ff4707}
</style>
</head>
<body>
<div class="main">
	<div class="top">
    	<div class="title"><img src="${userOrderDto.order_icon }" height="40" width="40"/>${userOrderDto.package_title }</div>
        <p class="money"><c:choose>
        <c:when test="${not empty userOrderDto.order_type and  userOrderDto.order_type =='recharge'}">+</c:when>
        <c:when test="${not empty userOrderDto.order_type and  userOrderDto.order_type =='buy'}">-</c:when>
        </c:choose>
        ${userOrderDto.order_price }<span>元</span></p>
    </div><!--.top-->
    
    <div class="bottom">
    	<ul>
        	<li>订单号<span>${userOrderDto.transaction_no }</span></li>
        	<li>订单名称<span>${userOrderDto.remark }</span></li>
        	<li>订单金额<span>${userOrderDto.order_price }元</span></li>
        	
        	<c:if test="${not empty userOrderDto.discount_price and  userOrderDto.discount_price !='0'}">
        	 	<li class="marked">折扣金额<span>${userOrderDto.discount_price }元</span></li>
        	</c:if>
        	
        	<c:if test="${not empty userOrderDto.ccplay_money and  userOrderDto.ccplay_money !='0'}">
        	 	<li>虫币支付<span>${userOrderDto.ccplay_money }虫币</span></li>
        	</c:if>
        	
        	<c:if test="${not empty userOrderDto.coupon_money and  userOrderDto.coupon_money !='0'}">
        	 	<li>代金券抵扣<span>${userOrderDto.coupon_money }元</span></li>
        	</c:if>
 
         	<c:if test="${not empty userOrderDto.pay_money and  userOrderDto.pay_money !='0'}">
        		 <li>实际支付<span>${userOrderDto.pay_money }元 </span></li>
        	</c:if>       
        		
         	<c:if test="${not empty userOrderDto.rebate_ccplay_money and  userOrderDto.rebate_ccplay_money !='0'}">
        		<li class="marked">充值赠送<span>${userOrderDto.rebate_ccplay_money }虫币</span></li>
        	</c:if>     
        	<c:if test="${not empty userOrderDto.rebate_coupon}">
        		<li class="marked">充值赠送<span>${userOrderDto.rebate_coupon }</span></li>
        	</c:if>
        	<li>支付方式<span>${userOrderDto.payment_channel_name }</span></li>
        	<li>创建时间<span>${userOrderDto.create_datetime }</span></li>
        </ul>
        
  		<c:if test="${not empty userOrderDto.recharge_user_name}">
	    	<ul>
	        	<li>对方账号<span>${userOrderDto.recharge_user_name }</span></li>
	        </ul>
        </c:if>
    </div><!--.bottom-->
</div><!--.main-->

</body>
</html>
