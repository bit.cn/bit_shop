<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>中奖信息</title>
    <link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/android_cc/css/site.css?v=2"/>
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/android_cc/css/ie_site.css?1415689350751"/>
    <![endif]-->
    
    <style type="text/css">
        body{ background: #f6ab00; }
    </style>

</head>
<body>
<div class="winners">
      <div class="rich_media">
        <div class="rich_media_inner">
        		<img src="http://static.ccplay.com.cn/static/android_cc/img/wheel_award.jpg" class="banner"/>
                <div class="list">
                      <table>
                <tbody>
                <tr>
                    <th>中奖账号</th>
                    <td>${win.userName}</td>
                </tr>
                <tr>
                    <th>账号ID</th>
                    <td>${win.userId}</td>
                </tr>
                <tr>
                    <th>抽中奖品</th>
                    <td>${win.summary}</td>
                </tr>
                <tr>
                    <td colspan="2" class="hints">
                        <b>友情提示：</b><br/>
                        获得实物奖品后请务必填写详细的联系方式，否则视为放弃领取奖品；<br/>
                        QQ：2802586192<br/>
                        邮箱：service@ccplay.cc
                    </td>
                </tr>

                </tbody></table>
                </div>
        </div>  
      </div>
</div>
</body>
</html>