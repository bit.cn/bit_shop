<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>最新中奖名单</title>
    <link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/android_cc/css/site.css?v=2"/>
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/android_cc/css/ie_site.css?1415689350751"/>
    <![endif]-->
    
    <style type="text/css">
        body{ background: #f6ab00; }
    </style>

</head>
<body>

<div class="rich_media">
    <div class="rich_media_inner winners">
        <img src="http://static.ccplay.com.cn/static/android_cc/img/wheel_winners.jpg" class="banner">
        <div class="list">
            <table>
                <tbody>
                <c:forEach items="${list}" var="item">
	                <tr onclick="location='http://android.ccplay.com.cn/api/v2/lotteries/winning/${item.id}'" <c:if test="${item.group==1}"> class="top-tr"</c:if>>
	                    <th <c:if test="${item.group==1}"> class="top"</c:if>>恭喜</th>
	                    <td>${item.username} (${item.user_id})</td>
	                    <th>抽中</th>
	                    <td>${item.title}</td>
	                </tr>
                </c:forEach>
             </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>