<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>${bulletin.title}</title>
    <link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/android_cc/css/site.css?1415689350751"/>
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/android_cc/css/ie_site.css?1415689350751"/>
    <![endif]-->
    
</head>
<body>

<div class="rich_media">
    <div class="rich_media_inner">
        <h2 class="rich_media_title" id="activity-name">${bulletin.title}</h2>
        <div class="rich_media_meta_list">
            <em class="rich_media_meta text">${bulletin.publish_date}</em>
            <em class="rich_media_meta name">${bulletin.userName}</em>
        </div>
        <div id="page-content">
            <div id="img-content">
                <div class="rich_media_content" id="js_content">
                    
                   ${bulletin.content}
                    
                </div>
            </div>
        </div>
    </div>
</div>

</body></html>