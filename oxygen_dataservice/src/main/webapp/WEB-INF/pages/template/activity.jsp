<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>${activity.title}</title>
    <link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/android_cc/css/site.css">
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/android_cc/css/ie_site.css"/>
    <![endif]-->
    
</head>
<body id="win"  onclick="open_activity(${activity.isActive},${global_flag});">
 ${activity.content}
	<script type="text/javascript">
	    function open_activity(isActive,global_flag){
	    	if(isActive){
		        try{
		        	if(global_flag){
			            window.lionJs.toActivity("#Intent;launchFlags=0x24000000;component=com.lion.market/.app.TurntableActivity;end",true);
		        	}else{
			        	var packageName="${activity.packageName}";
			        	var isCheckInstall=((packageName!=null && packageName!="")?true:false);
			        	var detailUrl="${activity.packageApiUrl}";
			        	var dialogTitle="友情提示";
			        	var dialogMsg="先安装《${activity.packageTitle}》，再来抽大奖吧！";
			            window.lionJs.toActivity("#Intent;launchFlags=0x24000000;component=com.lion.market/.app.TurntableActivity;S.id=${activity.lotteryId};end",true,isCheckInstall,packageName,detailUrl,dialogTitle,dialogMsg);
		        	}
		        }catch(e){
		            console.log(e);
		        }
	    	}
	    }
	</script>
</body></html>