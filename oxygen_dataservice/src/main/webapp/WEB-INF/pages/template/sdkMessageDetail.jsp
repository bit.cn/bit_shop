<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html> 
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<meta name="viewport" content="user-scalable=0, width=device-width">
<meta name="format-detection" content="telphone=no, email=no">
<title>SDK消息详情</title>
<style>
html { -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100% }
body {font:100% "Hiragino Sans GB","Microsoft YaHei",Arial,sans-serif;}
body, h1, h2, h3, h4, h5, p, ul, ol, dl, dd { margin: 0 }
ul,ol{ padding-left: 0; list-style-type: none; list-style-position: inside }
a img{ border: 0 }
.fix, .fix:after{ visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; overflow: hidden }

.dis-head{padding:15px 15px 0 15px; overflow:hidden}
.dis-head h1{font-size:150%; line-height:120%}
.dis-head span{font-size:12px; display:inline-block; line-height:none}
.dis-head .top span{padding-right:10px; color:#999}

.dis-main{padding:15px; overflow:hidden; font-size:100%; line-height:150%; Color: #222;}
.dis-main p {clear: both; padding-bottom:10px; text-align: justify;}
.dis-main img{width:100%}
.dis-main  * {max-width: 100%; box-sizing: border-box; -webkit-box-sizing: border-box; word-wrap: break-word}

.dis-game{border-top:20px solid #eee; border-bottom:20px solid #eee; padding-top:10px}
.dis-game a,.dis-list a{color:#333; text-decoration:none}
.dis-game .title{font-weight:bold; margin-left:10px}
.dis-game .title b{display:inline-block; background:#31B0D5; padding:0 5px ; margin-right:5px; font-weight:100; color:#fff; border-radius:.3em; font-size:12px}
.dis-game .btn{float:right; display:inline-block; padding:1px 10px; border:1px solid #e60000; border-radius:.3em; color:#e60000; margin-right:10px}
.dis-game .img{clear:both; display:block; height:140px; overflow:hidden; padding-top:10px}
.dis-game .img img{width:100%}

.dis-list{background:#fff}
.dis-list .tab-title{padding:10px 10px 10px 0; border-bottom:.5px solid #ddd; margin-left:10px; color:#e60000}
.dis-list li{clear:both; display:block}
.dis-list a{display:block; height:80px; position:relative; margin-left:10px; border-bottom:.5px solid #ddd;}
.dis-list .title{line-height:120%; display:block; padding-top:10px; margin-right:100px; height:40px; overflow:hidden}
.dis-list .img{width:80px; height:60px; position:absolute; top:10px; right:10px}
.dis-list .time{color:#ccc; font-size:12px}

.li-2,.li-3,.li-4,.li-5{width:50%; float:left!important; clear:none!important}
.li-3 a,.li-5 a{margin-left:0px; padding-left:10px; border-left:.5px solid #ddd}
.li-2 img,.li-3 img,.li-4 img,.li-5 img{display:none}
.li-2 .title,.li-3 .title,.li-4 .title,.li-5 .title{margin-right:10px}
li.more{text-align:center; padding:10px; margin-bottom:20px; border-bottom:none; color:#ccc}

.down-cc{background:#f5f5f5; padding:15px 15px 10px 15px}
.d-cc-box{position:relative}
.d-cc-logo img{height:30px}
.d-cc-install{position:absolute; right:0; top:0; display:inline-block; height:30px; line-height:30px; padding:0 10px; background:#06F; color:#fff; -moz-border-radius:5px; -webkit-border-radius:5px; border-radius:5px;}

@media only screen and (min-width : 800px) {
html{height:100%}
html{background:#f5f5f5;}
body{width:740px; margin:0 auto; background-color: #FFF;border-left: 1px solid #E8E8E8;border-right: 1px solid #E8E8E8}
.dis-head h1{font-size:170%}
.dis-head{padding:20px 20px 10px 20px}
.dis-main{padding:20px}
.dis-list .tab-title{border-bottom:1px solid #ddd}
.dis-list a{border-bottom:1px solid #ddd;}
.li-3 a,.li-5 a{border-left:1px solid #ddd}
}
</style>
</head> 

<body <c:if test="${sdkMessageDto.show_recharge==1 }">onclick='window.ccpayJs.toRecharge()'</c:if> >

<div class="page news-dis">
	<div class="dis-head">
    		<h1>${sdkMessageDto.title }</h1>
            <div class="top">
            	<span class="author">虫虫游戏</span>
            	<span class="time">${sdkMessageDto.create_datetime}</span>
                <!-- <span class="number">有1000+人看过</span> -->
            </div>
    </div><!--/dis-head-->
    
    <div class="dis-main">
           ${sdkMessageDto.content}
    </div><!--/dis-main-->

    
</div><!--/news-dis-->
</body>
</html>
