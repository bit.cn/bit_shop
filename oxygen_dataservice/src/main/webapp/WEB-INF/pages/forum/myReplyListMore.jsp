<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<c:set var="pagingUrl" value="${ctx}/api/v2/forum/topic/myReplyListMore?userId=${userId}"/>
<c:set var="nextUrl" value=""/>
<c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
	<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
</c:if>
<div id="dataDiv" nextUrl="${nextUrl}">
	<c:if test="${empty apiRsp.results}">
    	<p class="no-content">~还没回复~</p>
    </c:if>
	<c:forEach items="${apiRsp.results}" var="reply" >
    		<!--回复帖-->
    		<div class="reply-box my-reply">                   
                    <div class="dis-content">
                        <div class="reply-content-box">
                        <span class="user-img">
                        <img src="${reply.user.icon}"/></span>
                        <span class="user-name">${reply.user.display_name}</span>
                        <p class="reply-content">${reply.replyContent}</p>
                        </div>
                    </div>
                    <div class="other">
                        <span class="time">${reply.createDatetime}</span>                    
                    </div>
            </div><!--/回复帖-->
	</c:forEach>
</div>
