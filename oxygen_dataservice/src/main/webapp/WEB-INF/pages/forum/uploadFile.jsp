<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<%
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<c:set var="ctx" value="<%=basePath %>"/>
<!DOCTYPE html> 
<html>
<head>
<%@ include file="/WEB-INF/pages/common/css.jsp" %>
<title>帖子列表</title>
</head>
<body>
<div class="container">
    <input type="hidden" id="domain" value="http://7xoku8.com1.z0.glb.clouddn.com">
    <input type="hidden" id="uptoken_url" value="http://localhost:8080/api/v2/forum/uptoken">
    <input type="hidden" id="key" value="${key}">
    <input type="text" id="topicImages" name="images" value="">
    <div class="body">

	<a href="javascript:;" class="picIcon">点击</a>
    <div id="FileUploaded"></div>
    <div class="pic-box op" id="pic_box_id">
             <div id="container">
	        	<div class="pic">
	            	<span class="img" id="pickfiles" ><img  src="${ctx}/img/fml.png"/></span>
	            </div>  
            </div>
     </div>
</div>
</div>


<script type="text/javascript" src="${ctx}/js/qiniu_sdk/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${ctx}/js/qiniu_sdk/plupload/plupload.full.min.js"></script>
<script type="text/javascript" src="${ctx}/js/qiniu_sdk/plupload/i18n/zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/js/qiniu_sdk/qiniu.js"></script>
<script type="text/javascript" src="${ctx}/js/qiniu_sdk/main.js"></script>
<script type="text/javascript">
$(function() {  
   //上传
   $(".picIcon").click(function(){
	   $(".slideBox").addClass("op");
	   $(".pic-box").toggleClass("op");
   });   
});
</script>
</body>
</html>
