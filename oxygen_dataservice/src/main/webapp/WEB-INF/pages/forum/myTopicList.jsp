<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<%
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<c:set var="ctx" value="<%=basePath %>"/>
<!DOCTYPE html> 
<html>
<head>
<%@ include file="/WEB-INF/pages/common/css.jsp" %>
<title>帖子列表</title>
</head>
<body>
<div class="page out list-page" id="user-page">

<!--头部-->
<div id="header">
        <a href="javascript:void();" onclick="close_activity();" data-rel="auto" class="top-btn l icon-back">返回</a>
        <ul class="quick-menu">
        <c:choose>
			<c:when test="${userId eq current_user_id}">
	        	<li class="on"><a href="#">我的话题</a></li>
	        	<li><a href="${ctx}/api/v2/forum/topic/myReplyList?userId=${userId}&current_user_id=${current_user_id}">我回复的</a></li>
			</c:when>
			<c:otherwise>
				<li class="on"><a href="#">他的话题</a></li>
	        	<li><a href="${ctx}/api/v2/forum/topic/myReplyList?userId=${userId}&current_user_id=${current_user_id}">他回复的</a></li>
			</c:otherwise>
		</c:choose>
        </ul>
</div><!-- /头部 --> 

<div id="wrapper">
	<div id="scroller">
      
      
          <!--版块-->
          <div class="content">
                  <dl>
                      <dt><img src="${forum.user.icon }"/></dt>
                      <dd>
                          <h3>${forum.user.display_name }</h3>
                          <p><span>话题${forum.topicCount }</span><span>回复${forum.replyCount }</span></p>
                      </dd>
                  </dl>
          </div><!--/版块--> 
          
          <div class="posts-list">
            	<%@ include file="/WEB-INF/pages/forum/myTopicListMore.jsp" %>
          </div><!--/帖子列表-->
          
         <c:if test="${not empty apiRsp and apiRsp.totalPages>0 and apiRsp.curPage!=apiRsp.totalPages}">

         	<div class="loading-box">
		    	<a href="javascript:;" class="cl db tc bde p10 bg-white m5 more" style="display:none" id="loadingDiv"><img src="http://static.ccplay.com.cn/static/common/img/loading_red.gif" width="20"/></a>
		    	<a href="javascript:;" class="cl db tc bde p10 bg-white m5 more" style="display:" id="moreDiv">查看更多</a>
	    	</div>
	    </c:if> 
          

	</div>
</div>

</div>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>
<script>
$(function(){
	$("#moreDiv").loadingMore({
		dataDivId:'dataDiv'
	});
});

function close_activity(){
    try{
         window.lionJs.finish();
    }catch(e){
        console.log(e);
    }
}
</script>
</body>
</html>