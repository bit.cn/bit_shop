<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<%
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<c:set var="ctx" value="<%=basePath %>"/>
<!DOCTYPE html> 
<html>
<head>
<%@ include file="/WEB-INF/pages/common/css.jsp" %>
<title>帖子列表</title>
</head>
<body>
		<c:if test="${('packageDetail' eq fromFlag) and (not empty objectId)}">
		 	<div class="page out list-page index-list-page "  id="list-page"><!-- .app -->
		</c:if>
	   <c:if test="${empty fromFlag}">
		 	<div class="page out list-page"  id="list-page"><!-- .app -->
		</c:if>

<!--头部-->
<div id="header">
		<c:if test="${'game' eq forum.areaType and (empty fromFlag)}">
		 	 <a href="${ctx}/api/v2/forum/gameForumlist?parentAreaId=${forum.parentAreaId}&current_user_id=${userId}" data-rel="auto" class="top-btn l icon-back">返回</a>
		</c:if>
		<c:if test="${('freedom' eq forum.areaType) or ('packageDetail' eq fromFlag) }">
		 	<a href="javascript:void();" onclick="close_activity();" data-rel="auto" class="top-btn l icon-back">返回</a>
		</c:if>
        <ul class="quick-menu">
        	<li class="on"><a href="javascript:;">全部</a></li>
        	<li><a href="${ctx}/api/v2/forum/topic/newList?areaId=${areaId}&user_id=${userId}">新鲜</a></li>
        	<li><a href="${ctx}/api/v2/forum/topic/essenceList?areaId=${areaId}&user_id=${userId}">精华</a></li>
        </ul>
</div><!-- /头部 --> 

<div id="wrapper">
	<div id="scroller">
      
      
          <!--版块-->
          <div class="content">
                  <dl>
                      <dt><img src="${forum.areaImage}"/></dt>
                      <dd>
	                        <h3>${forum.areaName }</h3>
		                    <p><span>话题${forum.topicCount }</span><span>帖数${forum.replyCount}</span></p>
                      </dd>
                  </dl>
          </div><!--/版块--> 
          
          <!--置顶帖-->
          <div class="top-posts">
	           <c:forEach items="${topList}" var="topic" >
	              <a href="${ctx}/api/v2/forum/topic/topicDetail?topicId=${topic.id}&current_user_id=${userId}"><span><em class="tag">置顶</em>${topic.topicTitle}</span></a>
	           </c:forEach>
          </div><!--/置顶帖-->
          
          <!--帖子列表-->
          <div class="posts-list">
            	<%@ include file="/WEB-INF/pages/forum/topicListMore.jsp" %>
          </div><!--/帖子列表-->
          
         <c:if test="${not empty apiRsp and apiRsp.totalPages>0 and apiRsp.curPage!=apiRsp.totalPages}">

         	<div class="loading-box">
		    	<a href="javascript:;" style="display:none" id="loadingDiv"><img src="http://static.ccplay.com.cn/static/common/img/loading_red.gif"/></a>
		    	<a href="javascript:;" style="display:" id="moreDiv">查看更多</a>
	    	</div>
	    </c:if>  

	</div><!--/scroller-->
</div><!--/wrapper-->

<div class="user-btn">
	<a href="${ctx}/api/v2/forum/topic/goToAddTopic?areaId=${areaId}&userId=${userId}&fromFlag=${fromFlag}" class="new-post-btn">发布话题</a>
</div>
</div><!--/page-->

<%@ include file="/WEB-INF/pages/common/footer.jsp" %>
<script>
$(function(){
	$("#moreDiv").loadingMore({
		dataDivId:'dataDiv'
	});
});

function close_activity(){
    try{
         window.lionJs.finish();
    }catch(e){
        console.log(e);
    }
}
</script>
</body>
</html>