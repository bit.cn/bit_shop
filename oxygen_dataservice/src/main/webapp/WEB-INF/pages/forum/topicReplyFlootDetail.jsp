<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<%
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<c:set var="ctx" value="<%=basePath %>"/>
<!DOCTYPE html> 
<html>
<head>
<%@ include file="/WEB-INF/pages/common/css.jsp" %>
<title>帖子详情</title>
</head>
<body>
<div class="page out list-dis user-reply-notice-page" id="user-reply-notice-page">

<!--头部-->
<div id="header">
		<c:if test="${'replyToMe' eq fromFlag }">
		 	<a href="${ctx}/api/v2/forum/topic/replyToMeList?&current_user_id=${userId}" data-rel="auto" class="top-btn l icon-back">返回</a>
		</c:if>
		 <c:if test="${empty fromFlag or 'packageDetail' eq fromFlag}">
		 	<a href="${ctx}/api/v2/forum/topic/topicDetail?topicId=${floot.topicId}&current_user_id=${userId}&fromFlag=${fromFlag}"  data-rel="auto" class="top-btn l icon-back">返回</a>
		</c:if>
        <h1>${floot.floot}楼</h1>
</div><!-- /头部 --> 

<div id="wrapper">
	<div id="scroller">

    <!--回复列表-->
    <div class="reply-notice-list" >
    		<!--回复帖-->
    		<div class="reply-box my-reply" >
                    <div class="dis-content">
                       <div class="reply-content-box" id="floot_reply">
	                        <span class="user-img" onclick="open_activity(${floot.user.user_id})" >
	                        <img src="${floot.user.icon}"/></span>
	                        <span class="user-name">${floot.user.display_name}</span>
	                        <em class="tag lv">Lv.${floot.user.level}</em>
	                        <p class="reply-content">${floot.replyContent}</p>
                        </div>
                       <c:if test="${not empty floot.images}">
			                <div class="thumbnail">
			                	<c:forEach items="${floot.images}" var="pic" >
				                     <a href="${pic}"><img src="${pic}"/></a>
			                   </c:forEach>
			                </div>
		                </c:if>
                    </div>
                    <div class="other">
                        <span class="time">${floot.createDatetime}</span>
                        <c:if test="${(not empty userId and floot.user.user_id==userId) or (floot.user.user_id==userId)}">
		               	  <span class="delete-reply" onclick="MsgBox.remove('确定删除这条回复吗？','reply-${reply.id}','${ctx}/api/v2/forum/topic/delFlootOfReply?id=${reply.id}','');">删除</span>
		               	</c:if>
                    </div>
            </div><!--/回复帖-->
          <%@ include file="/WEB-INF/pages/forum/replyFlootMore.jsp" %>  
    </div> <!--/回复列表-->   
    
    <!--加载按钮-->
         <c:if test="${not empty apiRsp and apiRsp.totalPages>0 and apiRsp.curPage!=apiRsp.totalPages}">
         	<div class="loading-box">
		    	<a href="javascript:;" style="display:none" id="loadingDiv"><img src="http://static.ccplay.com.cn/static/common/img/loading_red.gif"/></a>
		    	<a href="javascript:;" style="display:" id="moreDiv">查看更多</a>
	    	</div>
	    </c:if>  
      
	</div><!--/scroller-->
</div><!--/wrapper-->

</div><!--/page-->

<!--回帖-->
<div class="fwin_dialog_replyForm op">
    <div class="g-mask"></div>
    <div class="replyForm">
          <form id="replyForm" name="replyForm" action="${ctx}/api/v2/forum/topic/addFlootReply" method="post">
          <input type="hidden" name="topicId" value="${floot.topicId}">
          <input type="hidden" name="replyId" value="${floot.id}">
          <input type="hidden" id="user_token" name="userToken">
              <div class="sendRCon">
                  <textarea id="sendRCon-content" name="replyContent" class="sendRCon-content" placeholder="回帖是一种美德..."></textarea>
              </div>
              <div class="sendNav">
                  <ul>
                      <li><a href="javascript:;" class="operatIcon"></a></li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li><a href="javascript:;" class="Btn cancelNewBtn">取消</a></li>
                      <li><a href="javascript:void();" onclick="replyTopic();" class="Btn sendBtn">发送</a></li>
                  </ul>
              </div>
          </form>
</div><!--/replyForm-->
    
    
    <!--表情-->
    <div id="slideBox" class="slideBox op" role="slideBox">
        <div class="bd">
        	<ul class="expreCon expreConNew">
                    <li class="bg1"> <a href="javascript:;" title="e_100"></a> <a href="javascript:;" title="e_101"></a> <a href="javascript:;" title="e_102"></a> <a href="javascript:;" title="e_103" class=""></a> <a href="javascript:;" title="e_104"></a> <a href="javascript:;" title="e_105"></a> <a href="javascript:;" title="e_106"></a> <a href="javascript:;" title="e_107"></a> <a href="javascript:;" title="e_108"></a> <a href="javascript:;" title="e_109"></a> <a href="javascript:;" title="e_110"></a> <a href="javascript:;" title="e_111"></a> <a href="javascript:;" title="e_112"></a> <a href="javascript:;" title="e_113"></a> <a href="javascript:;" title="e_114"></a> <a href="javascript:;" title="e_115"></a> <a href="javascript:;" title="e_116"></a> <a href="javascript:;" title="e_117"></a> <a href="javascript:;" title="e_118"></a> <a href="javascript:;" title="e_119"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
                    <li class="bg2"> <a href="javascript:;" title="e_120"></a> <a href="javascript:;" title="e_121"></a> <a href="javascript:;" title="e_122"></a> <a href="javascript:;" title="e_123"></a> <a href="javascript:;" title="e_124"></a> <a href="javascript:;" title="e_125"></a> <a href="javascript:;" title="e_126"></a> <a href="javascript:;" title="e_127"></a> <a href="javascript:;" title="e_128"></a> <a href="javascript:;" title="e_129"></a> <a href="javascript:;" title="e_130"></a> <a href="javascript:;" title="e_131"></a> <a href="javascript:;" title="e_132"></a> <a href="javascript:;" title="e_133"></a> <a href="javascript:;" title="e_134"></a> <a href="javascript:;" title="e_135"></a> <a href="javascript:;" title="e_136"></a> <a href="javascript:;" title="e_137"></a> <a href="javascript:;" title="e_138"></a> <a href="javascript:;" title="e_139"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
                    <li class="bg3"> <a href="javascript:;" title="e_140"></a> <a href="javascript:;" title="e_141"></a> <a href="javascript:;" title="e_142"></a> <a href="javascript:;" title="e_143"></a> <a href="javascript:;" title="e_144"></a> <a href="javascript:;" title="e_145"></a> <a href="javascript:;" title="e_146"></a> <a href="javascript:;" title="e_147"></a> <a href="javascript:;" title="e_148"></a> <a href="javascript:;" title="e_149"></a> <a href="javascript:;" title="e_150"></a> <a href="javascript:;" title="e_151"></a> <a href="javascript:;" title="e_152"></a> <a href="javascript:;" title="e_153"></a> <a href="javascript:;" title="e_154"></a> <a href="javascript:;" title="e_155"></a> <a href="javascript:;" title="e_156"></a> <a href="javascript:;" title="e_157"></a> <a href="javascript:;" title="e_158"></a> <a href="javascript:;" title="e_159"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
                    <li class="bg4"> <a href="javascript:;" title="e_160"></a> <a href="javascript:;" title="e_161"></a> <a href="javascript:;" title="e_162"></a> <a href="javascript:;" title="e_163"></a> <a href="javascript:;" title="e_164"></a> <a href="javascript:;" title="e_165"></a> <a href="javascript:;" title="e_166"></a> <a href="javascript:;" title="e_167"></a> <a href="javascript:;" title="e_168"></a> <a href="javascript:;" title="e_169"></a> <a href="javascript:;" title="e_170"></a> <a href="javascript:;" title="e_171"></a> <a href="javascript:;" title="e_172"></a> <a href="javascript:;" title="e_173"></a> <a href="javascript:;" title="e_174"></a> <a href="javascript:;" title="e_175"></a> <a href="javascript:;" title="e_176"></a> <a href="javascript:;" title="e_177"></a> <a href="javascript:;" title="e_178"></a> <a href="javascript:;" title="e_179"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
                    <li class="bg5"> <a href="javascript:;" title="e_180"></a> <a href="javascript:;" title="e_181"></a> <a href="javascript:;" title="e_182"></a> <a href="javascript:;" title="e_183"></a> <a href="javascript:;" title="e_184"></a> <a href="javascript:;" title="e_185"></a> <a href="javascript:;" title="e_186"></a> <a href="javascript:;" title="e_187"></a> <a href="javascript:;" title="e_188"></a> <a href="javascript:;" title="e_189"></a> <a href="javascript:;" title="e_190"></a> <a href="javascript:;" title="e_191"></a> <a href="javascript:;" title="e_192"></a> <a href="javascript:;" title="e_193"></a> <a href="javascript:;" title="e_194"></a> <a href="javascript:;" title="e_195"></a> <a href="javascript:;" title="e_196"></a> <a href="javascript:;" title="e_197"></a> <a href="javascript:;" title="e_198"></a> <a href="javascript:;" title="e_199"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
             </ul>
        </div>
        <div class="hd">
            <ul></ul>
        </div>
    </div><!--/表情--> 



</div><!--/回帖-->


<!--提示-->
<div class="popup correct dn">发布成功</div>
<!--  .correct成功   .error失败  .warning警告-->

<%@ include file="/WEB-INF/pages/common/footer.jsp" %>

<script>
$(function(){$("#moreDiv").loadingMore({dataDivId:"dataDiv"})});function replyTopic(){try{var b=$("#sendRCon-content").val().trim();if(b==""){$("#sendRCon-content").focus();return}var a=window.lionJs.getToken(true);if(a!=""){$("#user_token").val(a);$("#replyForm").ajaxForm({success:function(e,g){var f=g.data;if(f&&f.isSuccess){var d=window.lionJs.getUserId();$("#sendRCon-content").val("");$(".fwin_dialog_replyForm").addClass("op");$(".slideBox").addClass("op");$(".popup").show();setTimeout(function(){$(".popup").hide();location.replace("${ctx}/api/v2/forum/topic/topicReplyDetail?replyId=${floot.id}&user_id="+d)},1500)}else{alert(f.msg)}}})}}catch(c){alert(c)}}var TouchSlide=function(aV){aV=aV||{};var aU={slideCell:aV.slideCell||"#touchSlide",titCell:aV.titCell||".hd li",mainCell:aV.mainCell||".bd",effect:aV.effect||"left",autoPlay:aV.autoPlay||!1,delayTime:aV.delayTime||200,interTime:aV.interTime||2500,defaultIndex:aV.defaultIndex||0,titOnClassName:aV.titOnClassName||"on",autoPage:aV.autoPage||!1,prevCell:aV.prevCell||".prev",nextCell:aV.nextCell||".next",pageStateCell:aV.pageStateCell||".pageState",pnLoop:"undefined "==aV.pnLoop?!0:aV.pnLoop,startFun:aV.startFun||null,endFun:aV.endFun||null,switchLoad:aV.switchLoad||null},aT=document.getElementById(aU.slideCell.replace("#",""));if(!aT){return !1}var aS=function(t,s){t=t.split(" ");var r=[];s=s||document;var q=[s];for(var p in t){0!=t[p].length&&r.push(t[p])}for(var p in r){if(0==q.length){return !1}var o=[];for(var n in q){if("#"==r[p][0]){o.push(document.getElementById(r[p].replace("#","")))}else{if("."==r[p][0]){for(var m=q[n].getElementsByTagName("*"),l=0;l<m.length;l++){var k=m[l].className;k&&-1!=k.search(new RegExp("\\b"+r[p].replace(".","")+"\\b"))&&o.push(m[l])}}else{for(var m=q[n].getElementsByTagName(r[p]),l=0;l<m.length;l++){o.push(m[l])}}}}q=o}return 0==q.length||q[0]==s?!1:q},aR=function(g,e){var i=document.createElement("div");i.innerHTML=e,i=i.children[0];var h=g.cloneNode(!0);return i.appendChild(h),g.parentNode.replaceChild(i,g),aK=h,i},aQ=function(d,c){!d||!c||d.className&&-1!=d.className.search(new RegExp("\\b"+c+"\\b"))||(d.className+=(d.className?" ":"")+c)},aP=function(d,c){!d||!c||d.className&&-1==d.className.search(new RegExp("\\b"+c+"\\b"))||(d.className=d.className.replace(new RegExp("\\s*\\b"+c+"\\b","g"),""))},aO=aU.effect,aN=aS(aU.prevCell,aT)[0],aM=aS(aU.nextCell,aT)[0],aL=aS(aU.pageStateCell)[0],aK=aS(aU.mainCell,aT)[0];if(!aK){return !1}var aj,ai,aJ=aK.children.length,aI=aS(aU.titCell,aT),aH=aI?aI.length:aJ,aG=aU.switchLoad,aF=parseInt(aU.defaultIndex),aE=parseInt(aU.delayTime),aD=parseInt(aU.interTime),aC="false"==aU.autoPlay||0==aU.autoPlay?!1:!0,aB="false"==aU.autoPage||0==aU.autoPage?!1:!0,aA="false"==aU.pnLoop||0==aU.pnLoop?!1:!0,az=aF,ay=null,ax=null,av=null,au=0,at=0,ar=0,aq=0,ap=/hp-tablet/gi.test(navigator.appVersion),ao="ontouchstart" in window&&!ap,an=ao?"touchstart":"mousedown",am=ao?"touchmove":"",al=ao?"touchend":"mouseup",ak=aK.parentNode.clientWidth,ah=aJ;if(0==aH&&(aH=aJ),aB){aH=aJ,aI=aI[0],aI.innerHTML="";var ag="";if(1==aU.autoPage||"true"==aU.autoPage){for(var af=0;aH>af;af++){ag+="<li>"+(af+1)+"</li>"}}else{for(var af=0;aH>af;af++){ag+=aU.autoPage.replace("$",af+1)}}aI.innerHTML=ag,aI=aI.children}"leftLoop"==aO&&(ah+=2,aK.appendChild(aK.children[0].cloneNode(!0)),aK.insertBefore(aK.children[aJ-1].cloneNode(!0),aK.children[0])),aj=aR(aK,'<div class="tempWrap" style="overflow:hidden; position:relative;"></div>'),aK.style.cssText="width:"+ah*ak+"px;position:relative;overflow:hidden;padding:0;margin:0;";for(var af=0;ah>af;af++){aK.children[af].style.cssText="display:table-cell;vertical-align:top;width:"+ak+"px"}var ae=function(){"function"==typeof aU.startFun&&aU.startFun(aF,aH)},ad=function(){"function"==typeof aU.endFun&&aU.endFun(aF,aH)},ac=function(e){var d=("leftLoop"==aO?aF+1:aF)+e,g=function(i){for(var h=aK.children[i].getElementsByTagName("img"),j=0;j<h.length;j++){h[j].getAttribute(aG)&&(h[j].setAttribute("src",h[j].getAttribute(aG)),h[j].removeAttribute(aG))}};if(g(d),"leftLoop"==aO){switch(d){case 0:g(aJ);break;case 1:g(aJ+1);break;case aJ:g(0);break;case aJ+1:g(1)}}},ab=function(){ak=aj.clientWidth,aK.style.width=ah*ak+"px";for(var d=0;ah>d;d++){aK.children[d].style.width=ak+"px"}var c="leftLoop"==aO?aF+1:aF;aa(-c*ak,0)};window.addEventListener("resize",ab,!1);var aa=function(e,d,g){g=g?g.style:aK.style,g.webkitTransitionDuration=g.MozTransitionDuration=g.msTransitionDuration=g.OTransitionDuration=g.transitionDuration=d+"ms",g.webkitTransform="translate("+e+"px,0)translateZ(0)",g.msTransform=g.MozTransform=g.OTransform="translateX("+e+"px)"},L=function(b){switch(aO){case"left":aF>=aH?aF=b?aF-1:0:0>aF&&(aF=b?0:aH-1),null!=aG&&ac(0),aa(-aF*ak,aE),az=aF;break;case"leftLoop":null!=aG&&ac(0),aa(-(aF+1)*ak,aE),-1==aF?(ax=setTimeout(function(){aa(-aH*ak,0)},aE),aF=aH-1):aF==aH&&(ax=setTimeout(function(){aa(-ak,0)},aE),aF=0),az=aF}ae(),av=setTimeout(function(){ad()},aE);for(var d=0;aH>d;d++){aP(aI[d],aU.titOnClassName),d==aF&&aQ(aI[d],aU.titOnClassName)}0==aA&&(aP(aM,"nextStop"),aP(aN,"prevStop"),0==aF?aQ(aN,"prevStop"):aF==aH-1&&aQ(aM,"nextStop")),aL&&(aL.innerHTML="<span>"+(aF+1)+"</span>/"+aH)};if(L(),aC&&(ay=setInterval(function(){aF++,L()},aD)),aI){for(var af=0;aH>af;af++){!function(){var b=af;aI[b].addEventListener("click",function(){clearTimeout(ax),clearTimeout(av),aF=b,L()})}()}}aM&&aM.addEventListener("click",function(){(1==aA||aF!=aH-1)&&(clearTimeout(ax),clearTimeout(av),aF++,L())}),aN&&aN.addEventListener("click",function(){(1==aA||0!=aF)&&(clearTimeout(ax),clearTimeout(av),aF--,L())});var F=function(d){clearTimeout(ax),clearTimeout(av),ai=void 0,ar=0;var c=ao?d.touches[0]:d;au=c.pageX,at=c.pageY,aK.addEventListener(am,f,!1),aK.addEventListener(al,aw,!1)},f=function(d){if(!ao||!(d.touches.length>1||d.scale&&1!==d.scale)){var c=ao?d.touches[0]:d;if(ar=c.pageX-au,aq=c.pageY-at,"undefined"==typeof ai&&(ai=!!(ai||Math.abs(ar)<Math.abs(aq))),!ai){switch(d.preventDefault(),aC&&clearInterval(ay),aO){case"left":(0==aF&&ar>0||aF>=aH-1&&0>ar)&&(ar=0.4*ar),aa(-aF*ak+ar,0);break;case"leftLoop":aa(-(aF+1)*ak+ar,0)}null!=aG&&Math.abs(ar)>ak/3&&ac(ar>-0?-1:1)}}},aw=function(b){0!=ar&&(b.preventDefault(),ai||(Math.abs(ar)>ak/10&&(ar>0?aF--:aF++),L(!0),aC&&(ay=setInterval(function(){aF++,L()},aD))),aK.removeEventListener(am,f,!1),aK.removeEventListener(al,aw,!1))};aK.addEventListener(an,F,!1)};TouchSlide({slideCell:"#slideBox",titCell:".hd ul",mainCell:".bd ul",effect:"leftLoop",interTime:4000,autoPage:true});$(function(){$(".thumbnail a").touchTouch();$("#floot_reply").click(function(){$(".fwin_dialog_replyForm").removeClass("op");$("#sendRCon-content").focus()});$(".fwin_dialog_replyForm .cancelNewBtn,.fwin_dialog_replyForm .g-mask").click(function(){$(".fwin_dialog_replyForm").addClass("op")});$(".operatIcon").click(function(){$(".slideBox").toggleClass("op")});$(".expreCon a").addClass("a_face");$(".expreCon a:last-child").removeClass("a_face");$(".expreCon .a_face").click(function(){var a=$(this).attr("title");$("#sendRCon-content").insertContent("["+a+"]");$("#sendRCon-content").blur()})});function replace_em(a){a=a.replace(/\[e_([0-9]*)\]/g,'<img src="${ctx}/img/smiley/e$1.gif" border="0" class="smiley" />');return a}function delContent(){var a=$("#sendRCon-content").val().length;$("#sendRCon-content").val($("#sendRCon-content").val().substring(0,a-1))}function close_activity(){try{window.lionJs.finish()}catch(a){console.log(a)}}function open_activity(a){try{window.lionJs.toActivity("#Intent;component=com.lion.gameUnion/.user.app.FriendInfoActivity;S.friend_id="+a+";end")}catch(b){console.log(b)}};
</script>
</body>
</html>
