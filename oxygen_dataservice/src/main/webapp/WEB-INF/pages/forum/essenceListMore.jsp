<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<c:set var="pagingUrl" value="${ctx}/api/v2/forum/topic/essenceListMore?areaId=${areaId}&user_id=${userId}"/>
<c:set var="nextUrl" value=""/>
<c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
	<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
</c:if>
<div id="dataDiv" nextUrl="${nextUrl}">
	<c:forEach items="${apiRsp.results}" var="topic" >
   			 <a href="${ctx}/api/v2/forum/topic/topicDetail?topicId=${topic.id}&current_user_id=${userId}">
                <p class="describe">
                <span class="title"><em class="tag">精</em>${topic.topicTitle}</span><br/>
                ${topic.topicSummary}</p>
                <c:if test="${not empty topic.images}">
	                <div class="thumbnail">
	                	<c:forEach items="${topic.images}" var="pic" >
		                    <span><img data-original="${pic}" class="lazy" /></span>
	                   </c:forEach>
	                </div>
                </c:if>
                <div class="other">
                    <span class="author">${topic.user.display_name}</span>
                    <span class="reply">${topic.replyCount}</span>
                    <span class="time">${topic.updateDatetime}</span>
                </div>
            </a>
	</c:forEach>
</div>
