<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
 <!--回帖-->
<div class="fwin_dialog_replyForm op">
	<div class="g-mask"></div>
    <div class="replyForm">
          <form id="replyForm" name="replyForm" action="${ctx}/api/v2/forum/topic/addTopic" method="post">
          <input type="hidden" name="areaId" value="${areaId}">
          <input type="hidden" id="user_token" name="userToken">
			<input type="hidden" id="domain" value="http://static.ccplay.com.cn">
    		<input type="hidden" id="uptoken_url" value="${ctx}/api/v2/forum/uptoken">
   		 	<input type="hidden" id="key" value="${key}">
          	<input type="hidden" id="topicImages"  name="images" value="">
              <div class="sendRCon">
              	  <input type="text" maxlength="30" class="sendRCon-title" id="topicTitle_id" name="topicTitle" placeholder="标题" />
                  <textarea id="sendRCon-content" maxlength="1000" name="topicContent" class="sendRCon-content" placeholder="说两句吧..."></textarea>
              </div>
              <div class="sendNav">
                  <ul>
                      <li><a href="javascript:;" class="operatIcon"></a></li>
                      <li><a href="javascript:;" class="picIcon"></a></li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li><a href="javascript:;" class="Btn cancelNewBtn">取消</a></li>
                      <a href="javascript:addTopic();" class="Btn sendBtn">发送</a></li>
                  </ul>
              </div>
          </form>
    </div><!--/replyForm-->
    <!--表情-->
    <div id="slideBox" class="slideBox op" role="slideBox">
        <div class="bd">
            <ul class="expreCon expreConNew">
                    <li class="bg1"> <a href="javascript:;" title="e_100"></a> <a href="javascript:;" title="e_101"></a> <a href="javascript:;" title="e_102"></a> <a href="javascript:;" title="e_103" class=""></a> <a href="javascript:;" title="e_104"></a> <a href="javascript:;" title="e_105"></a> <a href="javascript:;" title="e_106"></a> <a href="javascript:;" title="e_107"></a> <a href="javascript:;" title="e_108"></a> <a href="javascript:;" title="e_109"></a> <a href="javascript:;" title="e_110"></a> <a href="javascript:;" title="e_111"></a> <a href="javascript:;" title="e_112"></a> <a href="javascript:;" title="e_113"></a> <a href="javascript:;" title="e_114"></a> <a href="javascript:;" title="e_115"></a> <a href="javascript:;" title="e_116"></a> <a href="javascript:;" title="e_117"></a> <a href="javascript:;" title="e_118"></a> <a href="javascript:;" title="e_119"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
                    <li class="bg2"> <a href="javascript:;" title="e_120"></a> <a href="javascript:;" title="e_121"></a> <a href="javascript:;" title="e_122"></a> <a href="javascript:;" title="e_123"></a> <a href="javascript:;" title="e_124"></a> <a href="javascript:;" title="e_125"></a> <a href="javascript:;" title="e_126"></a> <a href="javascript:;" title="e_127"></a> <a href="javascript:;" title="e_128"></a> <a href="javascript:;" title="e_129"></a> <a href="javascript:;" title="e_130"></a> <a href="javascript:;" title="e_131"></a> <a href="javascript:;" title="e_132"></a> <a href="javascript:;" title="e_133"></a> <a href="javascript:;" title="e_134"></a> <a href="javascript:;" title="e_135"></a> <a href="javascript:;" title="e_136"></a> <a href="javascript:;" title="e_137"></a> <a href="javascript:;" title="e_138"></a> <a href="javascript:;" title="e_139"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
                    <li class="bg3"> <a href="javascript:;" title="e_140"></a> <a href="javascript:;" title="e_141"></a> <a href="javascript:;" title="e_142"></a> <a href="javascript:;" title="e_143"></a> <a href="javascript:;" title="e_144"></a> <a href="javascript:;" title="e_145"></a> <a href="javascript:;" title="e_146"></a> <a href="javascript:;" title="e_147"></a> <a href="javascript:;" title="e_148"></a> <a href="javascript:;" title="e_149"></a> <a href="javascript:;" title="e_150"></a> <a href="javascript:;" title="e_151"></a> <a href="javascript:;" title="e_152"></a> <a href="javascript:;" title="e_153"></a> <a href="javascript:;" title="e_154"></a> <a href="javascript:;" title="e_155"></a> <a href="javascript:;" title="e_156"></a> <a href="javascript:;" title="e_157"></a> <a href="javascript:;" title="e_158"></a> <a href="javascript:;" title="e_159"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
                    <li class="bg4"> <a href="javascript:;" title="e_160"></a> <a href="javascript:;" title="e_161"></a> <a href="javascript:;" title="e_162"></a> <a href="javascript:;" title="e_163"></a> <a href="javascript:;" title="e_164"></a> <a href="javascript:;" title="e_165"></a> <a href="javascript:;" title="e_166"></a> <a href="javascript:;" title="e_167"></a> <a href="javascript:;" title="e_168"></a> <a href="javascript:;" title="e_169"></a> <a href="javascript:;" title="e_170"></a> <a href="javascript:;" title="e_171"></a> <a href="javascript:;" title="e_172"></a> <a href="javascript:;" title="e_173"></a> <a href="javascript:;" title="e_174"></a> <a href="javascript:;" title="e_175"></a> <a href="javascript:;" title="e_176"></a> <a href="javascript:;" title="e_177"></a> <a href="javascript:;" title="e_178"></a> <a href="javascript:;" title="e_179"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
                    <li class="bg5"> <a href="javascript:;" title="e_180"></a> <a href="javascript:;" title="e_181"></a> <a href="javascript:;" title="e_182"></a> <a href="javascript:;" title="e_183"></a> <a href="javascript:;" title="e_184"></a> <a href="javascript:;" title="e_185"></a> <a href="javascript:;" title="e_186"></a> <a href="javascript:;" title="e_187"></a> <a href="javascript:;" title="e_188"></a> <a href="javascript:;" title="e_189"></a> <a href="javascript:;" title="e_190"></a> <a href="javascript:;" title="e_191"></a> <a href="javascript:;" title="e_192"></a> <a href="javascript:;" title="e_193"></a> <a href="javascript:;" title="e_194"></a> <a href="javascript:;" title="e_195"></a> <a href="javascript:;" title="e_196"></a> <a href="javascript:;" title="e_197"></a> <a href="javascript:;" title="e_198"></a> <a href="javascript:;" title="e_199"></a> <a href="javascript:;" onclick="delContent()"></a> </li>
                  </ul>
        </div>
        <div class="hd">
            <ul></ul>
        </div>
    </div><!--/表情--> 
	<div id="FileUploaded"></div>
    <div class="pic-box op">
    		 <div id="pic_box_id">
    		 </div>
             <div id="container">
	        	<div class="pic">
	            	<span class="img" id="pickfiles" ><img src="${ctx}/img/fml.png"/></span>
	            </div>  
            </div>
     </div>

</div><!--/回帖-->
<div class="popup correct dn">发布成功</div>
