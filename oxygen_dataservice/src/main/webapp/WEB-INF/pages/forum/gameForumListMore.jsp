<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<c:set var="pagingUrl" value="${ctx}/api/v2/forum/gameForumlistMore?parentAreaId=${parentAreaId }&userId=${userId}"/>
<c:set var="nextUrl" value=""/>
<c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
	<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
</c:if>

<div id="dataDiv" nextUrl="${nextUrl}">
	<c:if test="${empty apiRsp.results}">
    	  <div class="posts-list">
              <p class="no-content">~还没有话题~</p>
          </div>
    </c:if>
	<c:forEach items="${apiRsp.results}" var="forum" >
            <li><a href="${ctx}/api/v2/forum/topic/listAll?areaId=${forum.areaId}&current_user_id=${userId}"><img  data-original="${forum.icon}" class="lazy" /><span>${forum.title}</span></a></li>
	</c:forEach>
</div>
