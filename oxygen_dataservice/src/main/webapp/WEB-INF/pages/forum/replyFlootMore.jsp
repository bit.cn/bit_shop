<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<c:set var="pagingUrl" value="${ctx}/api/v2/forum/topic/topicFlootReplyList?replyId=${floot.id}&user_id=${userId}&topicUserId=${topicUserId}"/>
<c:set var="nextUrl" value=""/>
<c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
	<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
</c:if>
<div class="reply-reply-list" >
	<div id="dataDiv" nextUrl="${nextUrl}">
		<c:if test="${empty apiRsp.results}">
	    	<p class="no-content">~还没有人回复~</p>
	    </c:if>
		     <c:forEach items="${apiRsp.results}" var="reply" >
		            <div class="list" id="reply-${reply.id}">
		             <span class="user-name">${reply.user.display_name} <em class="tag lv">Lv.${reply.user.level}</em></span>
		             <p class="reply-content">${reply.replyContent}</p>
		               <div class="other">
		               	<span class="time">${reply.createDatetime}</span>
		               	 <c:if test="${(not empty userId and topicUserId==userId) or (reply.createUserId==userId)}">
		               	  <span class="delete-reply" onclick="MsgBox.remove('确定删除这条回复吗？','reply-${reply.id}','${ctx}/api/v2/forum/topic/delFlootOfReply?id=${reply.id}','');">删除</span>
		               	</c:if>
		               </div>
		           </div>
		       </c:forEach>
	  </div>
</div>
