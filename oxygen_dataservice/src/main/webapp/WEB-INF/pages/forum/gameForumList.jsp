<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<%
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<c:set var="ctx" value="<%=basePath %>"/>
<!DOCTYPE html> 
<html>
<head>
<%@ include file="/WEB-INF/pages/common/css.jsp" %>
<title>帖子列表</title>
</head>
<body>
<div class="page out game-page" id="game-page">

<!--头部-->
<div id="header">
        <a href="javascript:void();" onclick="close_activity();" data-rel="auto" class="top-btn l icon-back">返回</a>
        <h1>游戏讨论区</h1>
</div><!-- /头部 --> 

<div id="wrapper">
	<div id="scroller">
          <div class="game-list">
          		<ul>
                	<%@ include file="/WEB-INF/pages/forum/gameForumListMore.jsp" %>
                </ul>
          </div>
          
         <c:if test="${not empty apiRsp and apiRsp.totalPages>0 and apiRsp.curPage!=apiRsp.totalPages}">
         	<div class="loading-box">
		    	<a href="javascript:;" class="cl db tc bde p10 bg-white m5 more" style="display:none" id="loadingDiv"><img src="http://static.ccplay.com.cn/static/common/img/loading_red.gif" width="20"/></a>
		    	<a href="javascript:;" class="cl db tc bde p10 bg-white m5 more" style="display:" id="moreDiv">查看更多</a>
	    	</div>
	    </c:if> 
          
	</div><!--/scroller-->
</div><!--/wrapper-->

</div><!--/page-->


<%@ include file="/WEB-INF/pages/common/footer.jsp" %>
<script>
$(function(){
	$("#moreDiv").loadingMore({
		dataDivId:'dataDiv'
	});

});
 
function close_activity(){
    try{
         window.lionJs.finish();
    }catch(e){
        console.log(e);
    }
}
</script>
</body>
</html>
