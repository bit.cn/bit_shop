<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<c:set var="pagingUrl" value="${ctx}/api/v2/forum/topic/topicReplyList?topicId=${topicId}&user_id=${userId}&topicUserId=${topicUserId}&fromFlag=${fromFlag}"/>
<c:set var="nextUrl" value=""/>
<c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
	<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
</c:if>
<div id="dataDiv" nextUrl="${nextUrl}">
	<c:if test="${empty apiRsp.results}">
    	<p class="no-content">~还没有人回复~ <span class="reply">我来回复</span></p>
    </c:if>
	<c:forEach items="${apiRsp.results}" var="reply" >
    		<!--回复帖-->
    		<div class="reply-box my-reply" id="reply-${reply.id}">    
    				            
                    <div class="dis-content">
                        <div class="reply-content-box">
                        <span class="user-img" onclick="open_activity(${reply.user.user_id})" >
                        <img src="${reply.user.icon}"/></span>
                        <span class="user-name">
	                        ${reply.user.display_name}
	                        <em class="tag lv">Lv.${reply.user.level}</em>
	                        <c:if test="${topicUserId == reply.user.user_id}">
	                        	<em class="tag">楼主</em>
	                        </c:if>
	                        <a href="${ctx}/api/v2/forum/topic/topicReplyDetail?topicId=${topicId}&user_id=${userId}&replyId=${reply.id}&topicUserId=${topicUserId}&fromFlag=${fromFlag}">
	                         <b title="回复" class="b-reply"></b>
	                        </a> 
                        </span>
                        
                        <p class="reply-content">${reply.replyContent}</p>
                        </div>
                       <c:if test="${not empty reply.images}">
			                <div class="thumbnail">
			                	<c:forEach items="${reply.images}" var="pic" >
				                    <a href="${pic}"><img data-original="${pic}" class="lazy" /></a>
			                   </c:forEach>
			                </div>
		                </c:if>
                    </div>
                    <div class="other">
                        <span class="time">${reply.floot}F ${reply.createDatetime}</span>
                        <c:if test="${(not empty userId and topicUserId==userId) or (reply.user.user_id==userId)}">
                    		<span class="delete-reply" onclick="MsgBox.remove('确定删除这条回复吗？','reply-${reply.id}','${ctx}/api/v2/forum/topic/delTopicReply?replyId=${reply.id}','');">删除</span>       
                    	</c:if> 
            				
                    </div>
                    <div class="reply-reply-list" id="reply-floot-list-${reply.id}">
                    <c:forEach items="${reply.replyList}" var="replyOfReply" >
                         <div class="list" id="reply-floot-list-${replyOfReply.id}">
		                        <span class="user-name">${replyOfReply.user.display_name}</span>
		                        <em class="tag lv">Lv.${replyOfReply.user.level}</em>
		                        <p class="reply-content">${replyOfReply.replyContent}</p>
                            <div class="other">
                            	<span class="time">${replyOfReply.createDatetime}</span>
                            	<c:if test="${(not empty userId and topicUserId==userId) or (replyOfReply.user.user_id==userId)}">
                    				<span class="delete-reply" onclick="MsgBox.remove('确定删除这条回复吗？','reply-floot-list-${replyOfReply.id}','${ctx}/api/v2/forum/topic/delFlootOfReply?id=${replyOfReply.id}','');">删除</span>       
                    			</c:if> 
                            </div>
                        </div>
                    </c:forEach>
                    
                    	<c:if test="${not empty reply.moreCount}">
                    		<div class="list more"><a href="javascript:void(0);" onclick="LoadUtil.loadToTarget('reply-floot-list-${reply.id}','${ctx}/api/v2/forum/topic/replyFlootLoad?replyId=${reply.id}&user_id=${userId}&topicUserId=${topicUserId}','all');" onclick="replyFlootLoad">更多回复(${reply.moreCount})...</a></div>
                    	</c:if>
                    </div>
            </div><!--/回复帖-->
	</c:forEach>
</div>
