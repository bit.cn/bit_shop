<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<%
String path = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<c:forEach items="${apiRsp.results}" var="reply" >
       <div class="list" id="reply-${reply.id}">
        <span class="user-name">${reply.user.display_name}<em class="tag lv">Lv.${reply.user.level}</em></span>
        <p class="reply-content">${reply.replyContent}</p>
          <div class="other">
          	<span class="time">${reply.createDatetime}</span>
          	 <c:if test="${(not empty userId and topicUserId==userId) or (reply.user.user_id==userId)}">
          	<span class="delete-reply" onclick="MsgBox.remove('确定删除这条回复吗？','reply-${reply.id}','<%=path%>/api/v2/forum/topic/delFlootOfReply?id=${reply.id}');">删除</span>
          	</c:if>
          </div>
      </div>
  </c:forEach>
  <c:if test="${not empty apiRsp and apiRsp.count>10}">
	  <div class="list more"><a href="<%=path%>/api/v2/forum/topic/topicReplyDetail?user_id=${userId}&replyId=${replyId}&topicUserId=${topicUserId}">更多回复(${(apiRsp.count-10)})...</a></div>
</c:if>

