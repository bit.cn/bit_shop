<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<c:set var="pagingUrl" value="${ctx}/api/v2/forum/topic/topicList?areaId=${areaId}&user_id=${userId}&fromFlag=${fromFlag}"/>
<c:set var="nextUrl" value=""/>
<c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
	<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
</c:if>

<div id="dataDiv"  class="posts-list" nextUrl="${nextUrl}">
	<c:if test="${empty apiRsp.results}">
              <p class="no-content">~还没有话题~</p>
    </c:if>
	<c:forEach items="${apiRsp.results}" var="topic" >
   			 <a href="${ctx}/api/v2/forum/topic/topicDetail?topicId=${topic.id}&current_user_id=${userId}&fromFlag=${fromFlag}">
                <p class="describe">
                <span class="title">
                <c:if test="${topic.isElite==1}">
                	<em class="tag">精</em>
                </c:if>
                <c:if test="${topic.isFire==1}">
                	<em class="tag">火</em>
                </c:if>
                ${topic.topicTitle}</span><br/>
                ${topic.topicSummary}</p>
                <c:if test="${not empty topic.images}">
	                <div class="thumbnail">
	                	<c:forEach items="${topic.images}" var="pic" >
		                    <span><img data-original="${pic}" class="lazy"/></span>
	                   </c:forEach>
	                </div>
                </c:if>
                <div class="other">
                    <span class="author">${topic.user.display_name}</span>
                    <span class="reply">${topic.replyCount}</span>
                    <span class="time">${topic.lastReplyDatetime}</span>
                </div>
            </a>
	</c:forEach>
</div>
