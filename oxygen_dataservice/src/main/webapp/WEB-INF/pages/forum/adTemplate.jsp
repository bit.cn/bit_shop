<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
    <!--焦点图-->
    <div id="slideBox" class="slideBox" role="slideBox">
        <div class="bd">
            <ul>
	            <c:forEach items="${topAdvList}" var="ad" >
	            	<li><a href="${ad.page_url}"><img src="${ad.cover}" alt="${ad.title}"/></a></li>
	            </c:forEach>
            </ul>
        </div>
        <div class="hd">
            <ul></ul>
        </div>
    </div><!--/焦点图--> 

