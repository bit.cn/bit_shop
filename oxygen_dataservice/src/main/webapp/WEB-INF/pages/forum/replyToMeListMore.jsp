<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<c:set var="pagingUrl" value="${ctx}/api/v2/forum/topic/replyToMeListMore?userId=${userId}"/>
<c:set var="nextUrl" value=""/>
<c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
	<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
</c:if>
<div id="dataDiv" nextUrl="${nextUrl}">
	<c:forEach items="${apiRsp.results}" var="reply" >
   			 <div class="reply-box my-reply">
   			 <c:if test="${'topic' eq reply.replyType}">
				<c:set var="url" value="${ctx}/api/v2/forum/topic/topicDetail?topicId=${reply.objectId}&current_user_id=${userId}&fromFlag=replyToMe"/>
			 </c:if>
			  <c:if test="${'floot' eq reply.replyType}">
				<c:set var="url" value="${ctx}/api/v2/forum/topic/topicReplyDetail?user_id=${userId}&replyId=${reply.objectId}&fromFlag=replyToMe"/>
			 </c:if>
			 <a href="${url}">   
                    <div class="dis-content">
                        <div class="reply-content-box">
                        <span class="user-img">
                        <img src="${reply.fromUser.icon}"/>
                        </span><span class="user-name">${reply.fromUser.display_name}<em class="tag lv">Lv.${reply.fromUser.level}</em></span>
                        <p class="reply-content">${reply.replyContent}</p></div>
                        <c:if test="${not empty reply.images}">
			                <div class="thumbnail">
			                	<c:forEach items="${reply.images}" var="pic" >
				                    <span><img src="${pic}" width="200px" height="200px" /></span>
			                   </c:forEach>
			                </div>
		                </c:if>
                        <div class="reply-list-title">
                        		原帖：${reply.mainContent}
                        </div>
                    </div>
                 </a>
                    <div class="other">
                        <span class="time">${reply.replyTime}</span>
                    	<span class="location">${reply.areaName}</span>
                    </div>
            </div><!--/回复帖-->
	</c:forEach>
</div>
