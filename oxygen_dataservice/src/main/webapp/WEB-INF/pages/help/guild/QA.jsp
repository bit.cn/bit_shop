<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="renderer" content="webkit">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<title>帮助与反馈</title>
<link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/ios_wap/css/site.css"/>
</head> 
<body> 

<div class="page-main">
      
      
      <!--反馈问题 开始-->
      <div class="feedback">
          <span>我要吐槽</span>
          <form id="feedbackForm" class="form" action="${ctx}/help/saveFeedback" method="post">
          	<input type="hidden" id="kind" name="kind" value="default">
          	<input type="hidden" id="apiKey" name="apiKey" value="${api_key}">
          	<input type="hidden" id="authorization_token" name="authorization_token" value="${authorization_token}">
          	<input type="hidden" id="package_name" name="package_name" value="${package_name}">
          	<input type="hidden" id="version_name" name="version_name" value="${version_name}">
              <textarea class="textarea" id="comment" name="comment" placeholder="我要吐槽，不吐不快..."></textarea>
              <span class="textCount"></span>
              <input type="button" class="submit" name="" value="吐槽一下" />
          </form>
          <div class="submit-ok">吐槽成功！</div>
          <div class="submit-error"></div>
      </div>
      <!--反馈问题 结束-->

      <!--常见问题 开始-->
      <div class="qa-box">
          <span>常见吐槽</span>
          <div class="qa-list">
                <h3><b></b>为什么要创建公会？</h3>
                <p>虫虫公会有最公正的公会评判标准、最强大的曝光平台、最丰富的公会活动奖励和特权。</p>
                <h3><b></b>为什么要加入公会？</h3>
                <p>这里到处都是你的生死兄弟！他们为荣誉而战，为兄弟而战。带上你的兄弟姐妹一起加入我们吧。</p>
                <h3><b></b>如何赚金币、金条、金砖？</h3>
                <p>每天参与做任务可以获得金币奖励，加入游戏公会可以获得金条奖励，创建和维护公会可以获得金条、金砖奖励。</p>
                <h3><b></b>金币、金条、金砖有什么用？</h3>
                <p>拥有金币、金条、金砖的用户可以参与刮奖、抽大奖等活动，领取公会礼包需要消耗金条。更多好玩的功能程序猿还在开发中，包容~各种求！</p>
                <h3><b></b>如何获取经验值并提升账号等级？</h3>
                <p>通过每日在社区里发布话题、回复话题、点赞或在加入、创建的公会里发布消息等操作都可以提高账号的经验值，以此来提升账号等级。</p>
                <h3><b></b>其他问题</h3>
                <p>骚年，你有问题？欢迎拼命吐槽或给我们写邮件(feedback@ccplay.cc)</p>
          </div>
      </div>
      <!--常见问题 结束-->


</div><!--page-main-->
<script src="http://static.ccplay.com.cn/static/common/js/jquery.min.js"></script>
<script>
function getApiJson(data){
	if(data==null || !data){
		return null;
	}
	for(var index in data){
		var apiRsp=data[index];
		return apiRsp;
	}
}
$(function(){
   $(".qa-list h3").click(function(){
		$(this).toggleClass("on").next("p").slideToggle(300).siblings("p").slideUp("slow");
		$(this).siblings().removeClass("on");
	});	

	$(".submit").bind('click', function() {
		if($("#comment").val() && $("#comment").val()!=""){
			var url=$("#feedbackForm").attr("action");
			var data=$("#feedbackForm").serialize();
			$.ajax({
				url: url,
				data: data,
				type: "post",
				dataType: "json",
				success: function(data){
					var apiRsp=getApiJson(data);
					if(apiRsp){
						if(apiRsp.isSuccess){
							$(".textCount").text("");
							$("#comment").val("");
							$(".form").slideUp();
							$(".submit-ok").slideDown();
							setTimeout(function(){$('.submit-ok').slideUp();$('.form').slideDown(500);},1500);
						}else{
							$(".submit-error").html(apiRsp.msg);
							$(".form").slideUp();
							$(".submit-error").slideDown();
							setTimeout(function(){$('.submit-error').slideUp();$('.form').slideDown(500);},1500);
						}
					}
				}
			});
		}else{
			$("#comment").focus();
		}
	});	
	
	
	$(".textarea").keydown(function(){
		var curLength=$(".textarea").val().length;	
		if(curLength>=141){
			var num=$(".textarea").val().substr(0,140);
			$(".textarea").val(num);
		}
		else{
			$(".textCount").text(140-$(".textarea").val().length)
		}
	});
	
	
});
</script>
</body>
</html>
