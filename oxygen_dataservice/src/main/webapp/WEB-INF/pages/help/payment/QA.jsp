<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="renderer" content="webkit">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<title>帮助与反馈</title>
<link rel="stylesheet" type="text/css" href="http://static.ccplay.com.cn/static/ios_wap/css/site.css"/>
</head> 
<body> 

<div class="page-main">
      <!--常见问题 开始-->
      <div class="qa-box">
          <span>常见问题</span>
          <div class="qa-list">
          		<h3><b></b>充值常见问题</h3>
                <p>
                	<b>充值成功后，游戏货币没有到账？</b><br/>
                    在提示充值成功后，3-5分钟内游戏货币发放到账，如未到账，请及时联系我们的客服，QQ:2802586192
                </p>
                
                <h3><b></b>关于虫币？</h3>
                <p>
                    <b>什么是虫币？</b><br/>
                    虫币是在虫虫充值平台通过银行卡等支付接口在线充值到虫虫帐户内的消费币形式，并非任何代币票券。<br/>
                    <b>虫币有什么用处</b><br/>
                    虫币可用在所有支持虫币支付的游戏中购买任何装备等商品。
                </p>
                
                <h3><b></b>关于代金券？</h3>
                <p>
                    <b>虫虫代金券</b><br/>
                    在所有游戏里可以抵减代金券面值等额的订单金额。<br/>
                    <b>游戏代金券</b><br/>
                    仅在指定的游戏里可以抵减代金券面值等额的订单金额。
                </p>
                
                <h3><b></b>支付宝</h3>
                <p>若在支付宝页面出现支付问题，可拨打支付宝客服热线：95188（服务时间：9:00—24:00），支付完成后，建议您在3-5分钟后查看到账情况。</p>
                
                <h3><b></b>银联</h3>
                <p>若在银联页面出现支付问题，可拨打银联客服热线：95516（服务时间：全天24小时），支付完成后，建议您在3-5分钟后查看到账情况。</p>
                
                <h3><b></b>微信</h3>
                <p>若在微信页面出现支付问题，可拨打微信客服热线：0755-86010333，支付完成后，建议您在3-5分钟后查看到账情况。</p>
                
                <h3><b></b>QQ钱包</h3>
                <p>若在QQ钱包页面出现支付问题，可拨打QQ钱包客服热线：0571-83763333，支付完成后，建议您在3-5分钟后查看到账情况。</p>
                
                <h3><b></b>财付通</h3>
                <p>若在财付通页面出现支付问题，可拨打财付通客服热线：0755-86013860，支付完成后，建议您在3-5分钟后查看到账情况。</p>
                
                <h3><b></b>虫币支付</h3>
                <p>若在虫币页面出现支付问题，可拨打虫虫客服热线：021-32575177，支付完成后，建议您在3-5分钟后查看到账情况。</p>
                
                <h3><b></b>其他问题</h3>
                <p>骚年，你有问题？欢迎拼命反馈或给我们写邮件(feedback@ccplay.cc)</p>
          </div>
      </div>
      <!--常见问题 结束-->      
      
      <!--反馈问题 开始-->
      <div class="feedback">
          <span>我要吐槽</span>
          <form id="feedbackForm" class="form" action="${ctx}/help/saveFeedback" method="post">
          	<input type="hidden" id="kind" name="kind" value="default">
          	<input type="hidden" id="apiKey" name="apiKey" value="${api_key}">
          	<input type="hidden" id="authorization_token" name="authorization_token" value="${authorization_token}">
          	<input type="hidden" id="package_name" name="package_name" value="${package_name}">
          	<input type="hidden" id="version_name" name="version_name" value="${version_name}">
              <textarea class="textarea" id="comment" name="comment" placeholder="我要吐槽，不吐不快..."></textarea>
              <span class="textCount"></span>
              <input type="button" class="submit" name="" value="吐槽一下" />
          </form>
          <div class="submit-ok">吐槽成功！</div>
          <div class="submit-error"></div>
      </div>
      <!--反馈问题 结束-->

</div><!--page-main-->
<script src="http://static.ccplay.com.cn/static/common/js/jquery.min.js"></script>
<script>
function getApiJson(data){
	if(data==null || !data){
		return null;
	}
	for(var index in data){
		var apiRsp=data[index];
		return apiRsp;
	}
}
$(function(){
   $(".qa-list h3").click(function(){
		$(this).toggleClass("on").next("p").slideToggle(300).siblings("p").slideUp("slow");
		$(this).siblings().removeClass("on");
	});	

	$(".submit").bind('click', function() {
		if($("#comment").val() && $("#comment").val()!=""){
			var url=$("#feedbackForm").attr("action");
			var data=$("#feedbackForm").serialize();
			$.ajax({
				url: url,
				data: data,
				type: "post",
				dataType: "json",
				success: function(data){
					var apiRsp=getApiJson(data);
					if(apiRsp){
						if(apiRsp.isSuccess){
							$(".textCount").text("");
							$("#comment").val("");
							$(".form").slideUp();
							$(".submit-ok").slideDown();
							setTimeout(function(){$('.submit-ok').slideUp();$('.form').slideDown(500);},1500);
						}else{
							$(".submit-error").html(apiRsp.msg);
							$(".form").slideUp();
							$(".submit-error").slideDown();
							setTimeout(function(){$('.submit-error').slideUp();$('.form').slideDown(500);},1500);
						}
					}
				}
			});
		}else{
			$("#comment").focus();
		}
	});	
	
	
	$(".textarea").keydown(function(){
		var curLength=$(".textarea").val().length;	
		if(curLength>=141){
			var num=$(".textarea").val().substr(0,140);
			$(".textarea").val(num);
		}
		else{
			$(".textCount").text(140-$(".textarea").val().length)
		}
	});
	
	
});
</script>
</body>
</html>
