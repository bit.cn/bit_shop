/*global Qiniu */
/*global plupload */
/*global FileProgress */
/*global hljs */

$(function() {
    var uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',
        browse_button: 'pickfiles',
        container: 'container',
        drop_element: 'container',
        max_retries: 3,
        max_file_size: '10mb',
//        filters: {
//        	  mime_types : [
//        	    { title : "选择图片", extensions : "jpg,gif,png" }
//        	  ]
//        	},
        flash_swf_url: 'plupload/Moxie.swf',
        dragdrop: true,
        chunk_size: '4mb',
        uptoken_url: $('#uptoken_url').val(),
        domain: $('#domain').val(),
        // downtoken_url: '/downtoken',
        unique_names: false,
        save_key: false,
        auto_start: true,
        init: {
            'FilesAdded': function(up, files) {

            },
            'BeforeUpload': function(up, file) {
            },
            'UploadProgress': function(up, file) {

            },
            'UploadComplete': function() {

            },
            'FileUploaded': function(up, file, info) {
            	var id=new Date().getTime();
                var res = $.parseJSON(info);
                var domain = up.getOption('domain');
                var url = domain + '/'+encodeURI(res.key);
                var imageView = '?imageView2/1/w/100/h/100';
                var img=encodeURI(res.key).replace("media/", "");
                var html='<div class="pic" id="'+id+'"><span class="img"><img src="'+url+imageView+'"/></span><span class="delete" onclick="removeImage(\''+id+'\',\''+img+'\');"></span></div>';
                $("#pic_box_id").prepend(html);
                var images=$("#topicImages").val();
                images+=img+",";
                $("#topicImages").val(images);
                
            },
            'Error': function(up, err, errTip) {
            	alert(errTip);
            },
             'Key': function(up, file) {
                 var key = $('#key').val()+'/'+file.name;
                 return key;
             }
        }
    });
});
