package com.ccplay.dataservice.exceptions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.oxygen.dto.ApiFinalResponse;
import com.oxygen.util.JsonUtil;
import com.oxygen.web.WebHelper;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler
	public String handleException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
		ApiFinalResponse<String> apiFinalRsp = new ApiFinalResponse<String>();
		//		apiFinalRsp.setIsSuccess(ApiMsgEnum.MissParameterException.getIsSuccess());
		//		apiFinalRsp.setCode(ApiMsgEnum.MissParameterException.getCode());
		apiFinalRsp.setMsg(ex.getMessage());
		String retJson = JsonUtil.objectToJson(apiFinalRsp);
		return WebHelper.outputJson(retJson, response);
	}
}