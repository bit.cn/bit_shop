package com.ccplay.dataservice.web;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import com.oxygen.web.SuperDispatcherServlet;

public class BaseDispatcherServlet extends SuperDispatcherServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1548307833744311704L;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		//SDKConfig.getConfig().loadPropertiesFromSrc();// 从classpath加载acp_sdk.properties文件
	}
}
