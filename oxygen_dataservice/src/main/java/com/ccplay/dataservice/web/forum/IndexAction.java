package com.ccplay.dataservice.web.forum;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ccplay.dataservice.service.CdnService;
import com.ccplay.dataservice.service.UserProxy;
import com.ccplay.dataservice.web.BaseAction;
import com.oxygen.util.DateTimeUtil;

/**
 * 社区
 * 
 * @author kevin
 * 
 */
@Controller
@RequestMapping(value = { "/api/v2/forum" })
public class IndexAction extends BaseAction {
	@Autowired
	private CdnService cdnService;
	@Autowired
	private UserProxy userProxy;

	@RequestMapping(value = { "/uploadFile", "/uploadFile/" })
	public String uploadFile(HttpServletRequest req) {
		String key = "/forum/" + req.getParameter("userId") + "/" + DateTimeUtil.formatDateTime(new Date(), "yyyy/MM/dd/HH-mm-ss-SSS");
		req.setAttribute("userId", req.getParameter("userId"));
		req.setAttribute("key", key);
		return "/forum/uploadFile";
	}

	@RequestMapping(value = { "/testFile", "/testFile/" })
	public String testFile(HttpServletRequest req) {
		String key = "forum/" + req.getParameter("userId") + "/" + DateTimeUtil.formatDateTime(new Date(), "yyyy/MM/dd/HH-mm-ss-SSS");
		req.setAttribute("userId", req.getParameter("userId"));
		req.setAttribute("key", key);
		return "/forum/testFile";
	}

	@RequestMapping(value = { "/uptoken", "/uptoken/" })
	public void uptoken(HttpServletRequest req, HttpServletResponse response) {
		String token = cdnService.getToken(null);
		String json = "{\"uptoken\": \"" + token + "\"}";
		this.outputJson(json, response);
	}

}
