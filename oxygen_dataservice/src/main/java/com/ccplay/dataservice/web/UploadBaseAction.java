package com.ccplay.dataservice.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import com.oxygen.components.ServletUploadFileItem;
import com.oxygen.util.DateTimeUtil;
import com.oxygen.util.RandomIDUtil;
import com.ccplay.dataservice.dto.FileDto;
import com.ccplay.dataservice.dto.FileUploadResult;
import com.ccplay.dataservice.service.CdnService;

public class UploadBaseAction {
	protected static final Logger loger = Logger.getLogger(UploadBaseAction.class);

	protected static final String MEDIA_PREFIX = "media/";

	@Value("${file.server.tempDir}")
	protected String fileServerTempDir;

	@Value("${file.server.rootDir}")
	protected String fileServerRootDir;

	@Value("${file.upload.imageMaxSize}")
	protected int fileUploadImageMaxSize;

	@Value("${file.upload.imageExt}")
	protected String fileUploadImageExt;

	@Value("${file.upload.imageMimeType}")
	protected String fileUploadImageMimeType;

	@Value("${file.upload.imageDefaultDirName}")
	protected String fileUploadImageDefaultDirName;

	@Value("${file.upload.userDirName}")
	protected String fileUploadUserDirName;

	@Value("${file.upload.guildDirName}")
	protected String fileUploadGuildDirName;

	@Autowired
	private CdnService cdnService;

	/**
	 * 上传图片（包括同步CND）
	 * 
	 * @param req
	 * @param fileDir
	 * @return
	 * @throws Exception
	 * @author Daniel
	 */
	protected FileUploadResult uploadImage(FileItem fileItem, String fileDir) throws Exception {
		String retMsg = "图片上传失败";
		int flag = this.checkImageFile(fileItem);
		if (flag == 1) {
			if (StringUtils.isEmpty(fileDir)) {
				String timeStr = DateTimeUtil.formatDateTime(new Date(), "yyyy/MM/dd/HHmm-ss-SSS");
				fileDir = fileUploadImageDefaultDirName + File.separator + timeStr + File.separator;
			}
			FileDto fileDto = this.writeFileToLocalServer(fileItem, fileDir, true);
			if (fileDto != null) {
				cdnService.putFileToCdn(fileDto.getPath(), fileDto.getAbsolutePath());
				return new FileUploadResult(true, fileDto);
			}
		} else if (flag == -2 || flag == -3) {
			retMsg = "图片格式只能为" + this.fileUploadImageExt;
		}
		return new FileUploadResult(false, null, retMsg);
	}

	private int checkImageFile(FileItem fileItem) {
		if (fileItem == null) {
			return 0;
		}
		if (!this.fileUploadImageMimeType.contains(fileItem.getContentType())) {
			return -3;
		}
		String ext = fileItem.getName();
		ext = ext.substring(ext.lastIndexOf(".") + 1).toLowerCase();
		if (!this.fileUploadImageExt.contains(ext)) {
			return -2;
		}
		return 1;
	}

	private FileDto writeFileToLocalServer(FileItem fileItem, String fileDir, boolean rename) {
		FileDto fileDto = new FileDto();
		// 文件保存目录路径
		String uploadPath = fileServerRootDir + fileDir;
		// 创建文件夹
		File dirFile = new File(uploadPath);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		OutputStream out = null;
		InputStream in = null;
		String fileName = fileItem.getName();
		String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
		String fileUUID = RandomIDUtil.getNewUUID();
		String newFileName = fileName;
		if (rename) {
			newFileName = fileUUID + "." + fileExt;
		}
		// 上传
		File uploadedFile = null;
		// String md5 = null;
		try {
			uploadedFile = new File(uploadPath, newFileName);
			out = new FileOutputStream(uploadedFile);
			in = fileItem.getInputStream();
			byte buf[] = new byte[1024];// 可以修改 1024 以提高读取速度
			int length = 0;
			while ((length = in.read(buf)) > 0) {
				out.write(buf, 0, length);
				out.flush();
			}
			// md5 = Md5Util.encodeFile(uploadedFile);
		} catch (Exception e) {
			loger.error("uploadFile Exception:", e);
		} finally {
			try {
				out.close();
				in.close();
			} catch (IOException e) {
				loger.error("uploadFile IOException:", e);
			}
		}
		fileDto.setId(fileUUID);
		fileDto.setSourceName(fileName);
		fileDto.setDestName(newFileName);
		fileDto.setMimeType(fileItem.getContentType());
		fileDto.setFileExt(fileExt);
		fileDto.setPath(fileDir + newFileName);
		fileDto.setSize(fileItem.getSize());
		// fileDto.setMd5(md5);
		// fileDto.setPreviewUrl(fileServer + fileDto.getPath());
		fileDto.setAbsolutePath(uploadedFile.getPath());
		fileDto.setSizeStr(this.formatFileSize(fileItem.getSize()));
		return fileDto;
	}

	private String formatFileSize(long size) {
		if (size == 0) {
			return "0MB";
		}
		BigDecimal gb = new BigDecimal(size).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024"));
		if (gb.doubleValue() < 1) {
			return gb.multiply(new BigDecimal("1024")).setScale(2, RoundingMode.HALF_UP) + "MB";
		} else {
			return gb.setScale(2, RoundingMode.HALF_UP).toString() + "GB";
		}

	}

	/**
	 * 文件上传对象
	 * 
	 * @param request
	 * @param fileSizeMax
	 * @return
	 * @throws Exception
	 */
	protected ServletUploadFileItem getUploadFileItem(final HttpServletRequest request, long fileSizeMax) throws Exception {
		// 创建临时文件夹
		File dirTempFile = new File(this.fileServerTempDir);
		if (!dirTempFile.exists()) {
			dirTempFile.mkdirs();
		}
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(10 * 1024 * 1024); // 设定使用内存超过5M时，将产生临时文件并存储于临时目录中。
		factory.setRepository(dirTempFile); // 设定存储临时文件的目录。
		ServletUploadFileItem uploadItem = new ServletUploadFileItem();
		uploadItem.setFileItemFactory(factory);
		uploadItem.setFileSizeMax(fileSizeMax * 1024 * 1024);
		uploadItem.setHeaderEncoding("utf-8");
		// upload.setProgressListener(new ProgressListener() {
		// @Override
		// public void update(long pBytesRead, long pContentLength, int pItems)
		// {
		// Map<String, Long> retMap = new HashMap<String, Long>();
		// retMap.put("loaded", pBytesRead);
		// retMap.put("total", pContentLength);
		// String uploaderLocation = request.getParameter("uploaderLocation");
		// WebUtils.setSessionAttribute(request, "ProgressListener-" +
		// uploaderLocation, retMap);
		// }
		// });
		uploadItem.parse(request);
		return uploadItem;
	}
}
