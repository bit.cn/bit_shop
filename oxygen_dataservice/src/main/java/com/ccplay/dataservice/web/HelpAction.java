package com.ccplay.dataservice.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.oxygen.web.WebHelper;

@Controller
@RequestMapping(value = "/help")
public class HelpAction extends BaseAction {

	@RequestMapping(value = "/about")
	public String about() {
		return "/help/about";
	}

	@RequestMapping(value = "/guild/QA")
	public String guildQA(HttpServletRequest req) {
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/help/guild/QA";
	}

	@RequestMapping(value = "/guild/serviceterm")
	public String guildServiceterm(HttpServletRequest req) {
		return "/help/guild/serviceterm";
	}

	@RequestMapping(value = "/payment/QA")
	public String paymentQA(HttpServletRequest req) {
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/help/payment/QA";
	}

	@RequestMapping(value = "/payment/serviceterm")
	public String paymentServiceterm(HttpServletRequest req) {
		return "/help/payment/serviceterm";
	}
}