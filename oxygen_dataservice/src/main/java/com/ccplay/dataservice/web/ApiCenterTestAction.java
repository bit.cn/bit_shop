package com.ccplay.dataservice.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.oxygen.constants.Constant;
import com.oxygen.enums.ApiMethodEnum;
import com.oxygen.enums.ApiMsgEnum;
import com.oxygen.enums.ApiServerEnum;
import com.oxygen.util.HttpClientUtil;
import com.oxygen.util.JsonUtil;
import com.oxygen.util.SimpleTokenUtil;
import com.oxygen.web.WebHelper;

@Controller
@RequestMapping(value = "/apitest")
public class ApiCenterTestAction {

	@RequestMapping(value = { "", "/" })
	public String apitest(ModelMap modelMap) {
		modelMap.addAttribute("functionCodeCatalogMap", ApiServerEnum.getApiServerEnumMap());
		modelMap.addAttribute("apiMsgMap", ApiMsgEnum.getAll());
		return "/apitest";
	}

	/**
	 * 根据类目获取API列表
	 * 
	 * @param catalog
	 * @param rsp
	 * @author Daniel
	 */
	@RequestMapping(value = "/getFunctionListByCatalog")
	public void getFunctionListByCatalog(@RequestParam String catalog, HttpServletResponse rsp) {
		Map<String, String> paramMap = ApiMethodEnum.getApiMethodMapByServer(catalog);
		String json = JsonUtil.objectToJson(paramMap);
		WebHelper.outputJson(json, rsp);
	}

	/**
	 * 根据API CODE获取参数列表
	 * 
	 * @param functionCode
	 * @param rsp
	 * @author Daniel
	 */
	@RequestMapping(value = "/getParamsByFunctionCode")
	public void getParamsByFunctionCode(@RequestParam String functionCode, HttpServletResponse rsp) {
		Map<String, String> paramMap = ApiMethodEnum.getApiParamMapByMethod(functionCode);
		String json = JsonUtil.objectToJson(paramMap);
		WebHelper.outputJson(json, rsp);
	}

	@RequestMapping(value = "/doApiTest")
	public String doApiTest(HttpServletRequest req, HttpServletResponse rsp) {
		String apiKey = req.getParameter("apiKey");
		String apiSecret = req.getParameter("apiSecret");
		String functioncode = req.getParameter("functionCodeInput");
		if (functioncode == null || "".equals(functioncode)) {
			functioncode = req.getParameter("functionCodeSelection");
		}
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		String[] paramNameArr = req.getParameterValues("paramName");
		String[] paramValueArr = req.getParameterValues("paramValue");
		if (paramNameArr != null && paramNameArr.length > 0) {
			int n = 0;
			for (String paramName : paramNameArr) {
				if (paramName != null && !"".equals(paramName.trim())) {
					paramsMap.put(paramName, paramValueArr[n]);
				}
				n++;
			}
		}
		paramsMap.put(Constant.API_KEY, apiKey);
		String token = SimpleTokenUtil.buildToken(paramsMap, functioncode, apiSecret);
		paramsMap.put(Constant.ACCESS_TOKEN, token);

		Map<String, Object> functionCodeMap = new HashMap<String, Object>();
		functionCodeMap.put(functioncode, paramsMap);
		// paramsMap.put("functionCode", functioncode);
		// String key = makeNginxKey(paramsMap);

		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put(Constant.DATA, JsonUtil.objectToJson(functionCodeMap, Map.class));

		HttpClientUtil clientUtil = new HttpClientUtil();
		StringBuffer defaultApiServer = new StringBuffer();
		defaultApiServer.append(req.getScheme() + "://");
		defaultApiServer.append(req.getServerName());
		if (req.getServerPort() != 80) {
			defaultApiServer.append(":" + req.getServerPort());
		}
		if (!StringUtils.isEmpty(req.getContextPath()) && !"/".equals(req.getContextPath())) {
			defaultApiServer.append(req.getContextPath());
		}
		defaultApiServer.append("/apicenter").append("/").append(functioncode).append("/").append(token);
		String retJson = clientUtil.doHttpPost(defaultApiServer.toString(), dataMap);
		return WebHelper.outputJson(retJson, rsp);
	}

	@RequestMapping(value = "/doApiToken")
	public String doApiToken(HttpServletRequest req, HttpServletResponse rsp) {
		String apiKey = req.getParameter("apiKey");
		String apiSecret = req.getParameter("apiSecret");
		String functioncode = req.getParameter("functionCodeInput");
		if (functioncode == null || "".equals(functioncode)) {
			functioncode = req.getParameter("functionCodeSelection");
		}
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		String[] paramNameArr = req.getParameterValues("paramName");
		String[] paramValueArr = req.getParameterValues("paramValue");
		if (paramNameArr != null && paramNameArr.length > 0) {
			int n = 0;
			for (String paramName : paramNameArr) {
				if (paramName != null && !"".equals(paramName.trim())) {
					paramsMap.put(paramName, paramValueArr[n]);
				}
				n++;
			}
		}
		paramsMap.put(Constant.API_KEY, apiKey);
		String token = SimpleTokenUtil.buildToken(paramsMap, functioncode, apiSecret);
		paramsMap.put(Constant.ACCESS_TOKEN, token);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put(functioncode, paramsMap);
		return WebHelper.outputJson(JsonUtil.objectToJson(data), rsp);
	}
}
