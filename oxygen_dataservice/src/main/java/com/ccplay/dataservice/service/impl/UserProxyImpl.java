package com.ccplay.dataservice.service.impl;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.oxygen.constants.Constant;
import com.oxygen.dto.UserDto;
import com.oxygen.enums.ApiMethodEnum;
import com.oxygen.util.JsonUtil;
import com.ccplay.dataservice.service.UserProxy;
import com.google.gson.reflect.TypeToken;

@Service
public class UserProxyImpl extends BaseServiceImpl implements UserProxy {

	private UserDto _getUser(String apiKey, Integer userId, String authorization_token) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (userId != null) {
			paramMap.put("user_id", userId);
		}
		if (!StringUtils.isEmpty(authorization_token)) {
			paramMap.put(Constant.AUTH_TOKEN, authorization_token);
		}
		paramMap.put(Constant.JUST_NEED_RESULT, true);//
		String retJson = this.callUserApi(apiKey, ApiMethodEnum.USER_GETPROFILE.getCode(), paramMap);
		Type type = new TypeToken<Map<String, UserDto>>() {
		}.getType();
		Map<String, UserDto> retMap = JsonUtil.jsonToObject(retJson, type);
		if (retMap != null && retMap.get(ApiMethodEnum.USER_GETPROFILE.getCode()) != null) {
			return retMap.get(ApiMethodEnum.USER_GETPROFILE.getCode());
		}
		return null;
	}

	/**
	 * 获取用户信息
	 * 
	 * @param apiKey
	 * @param authorization_token
	 * @return
	 */
	@Override
	public UserDto getUser(String apiKey, String authorization_token) {
		return this._getUser(apiKey, null, authorization_token);
	}

}
