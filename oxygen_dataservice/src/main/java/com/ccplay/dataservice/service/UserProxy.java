package com.ccplay.dataservice.service;

import com.oxygen.dto.UserDto;

/**
 * 用户服务代理
 * 
 * @author Daniel
 */
public interface UserProxy {
	UserDto getUser(String apiKey, String authorization_token);
}
