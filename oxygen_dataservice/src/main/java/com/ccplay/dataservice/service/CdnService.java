package com.ccplay.dataservice.service;

import com.qiniu.api.io.PutRet;

public interface CdnService {
	PutRet putFileToCdn(String key, String fileFullPath);

	String getToken(String bucketName);
}
