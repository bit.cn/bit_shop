package com.ccplay.dataservice.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;

import com.oxygen.service.impl.SuperServiceImpl;
import com.oxygen.web.SuperDispatcherServlet;

public abstract class BaseServiceImpl extends SuperServiceImpl {
	@Value("${ccplay.user.api.server}")
	private String ccplayUserApiServer;
	protected static final Map<String, String> head = new HashMap<String, String>();

	public BaseServiceImpl() {
		int port = SuperDispatcherServlet.getlocalPort();
		head.put("referer", "ccplay_dataservice/" + port);
	}

	/**
	 * 调用用户API
	 * 
	 * @param apiKey
	 * @param functionCode
	 * @param inMap
	 * @return
	 * @author Daniel
	 */
	protected String callUserApi(String apiKey, String functionCode, Map<String, Object> inMap) {
		return this.callApi(ccplayUserApiServer, apiKey, functionCode, inMap, head);
	}
}
