package com.ccplay.dataservice.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ccsc")
public class CdnOperateCallback implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6091210355610934640L;

	private List<CdnOperateCallbackItem> itemList;

	@XmlElement(name = "item_id")
	public List<CdnOperateCallbackItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<CdnOperateCallbackItem> itemList) {
		this.itemList = itemList;
	}

}
