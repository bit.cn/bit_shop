package com.ccplay.dataservice.dto;

public class SdkMessageDto {

	private Integer id;
	private String title;
	private String summary;
	private Integer package_id;
	private Integer show_popup;
	private Integer show_recharge;
	private String content;
	private String create_datetime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Integer getPackage_id() {
		return package_id;
	}

	public void setPackage_id(Integer package_id) {
		this.package_id = package_id;
	}

	public Integer getShow_popup() {
		return show_popup;
	}

	public void setShow_popup(Integer show_popup) {
		this.show_popup = show_popup;
	}

	public Integer getShow_recharge() {
		return show_recharge;
	}

	public void setShow_recharge(Integer show_recharge) {
		this.show_recharge = show_recharge;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreate_datetime() {
		return create_datetime;
	}

	public void setCreate_datetime(String create_datetime) {
		this.create_datetime = create_datetime;
	}

}
