package com.ccplay.dataservice.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class CdnOperateCallbackItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3900930762639775862L;

	private String id;

	private String op_name;
	private String op_status;

	@XmlAttribute(name = "value")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOp_name() {
		return op_name;
	}

	public void setOp_name(String op_name) {
		this.op_name = op_name;
	}

	public String getOp_status() {
		return op_status;
	}

	public void setOp_status(String op_status) {
		this.op_status = op_status;
	}

}
