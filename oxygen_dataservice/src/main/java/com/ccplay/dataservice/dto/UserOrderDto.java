package com.ccplay.dataservice.dto;

import java.io.Serializable;

public class UserOrderDto implements Serializable {

	private static final long serialVersionUID = 8972834186944939002L;

	private Integer order_id;
	private String create_datetime;
	private Integer user_id;
	private String user_name;
	private String transaction_no;
	private Integer package_id;
	private String package_title;
	private Integer product_id;

	// 订单名称
	private String remark;

	// 订单金额
	private String order_price;

	// 打折金额
	private String discount_price;

	// 银联，支付宝...等支付
	private String pay_money;

	// 虫币支付
	private String ccplay_money;

	// 代金券
	private String coupon_money;

	private String payment_channel_code;
	private String payment_channel_name;

	private String order_type;

	// 订单状态
	private String order_status;

	// 充值赠送
	private String rebate_ccplay_money;

	private String rebate_coupon;

	private String order_icon;

	// 充值人ID
	private Integer recharge_user_id;

	// 充值人
	private String recharge_user_name;

	private String detail_link;

	public String getRebate_coupon() {
		return rebate_coupon;
	}

	public void setRebate_coupon(String rebate_coupon) {
		this.rebate_coupon = rebate_coupon;
	}

	public Integer getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}

	public String getCreate_datetime() {
		return create_datetime;
	}

	public void setCreate_datetime(String create_datetime) {
		this.create_datetime = create_datetime;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getTransaction_no() {
		return transaction_no;
	}

	public void setTransaction_no(String transaction_no) {
		this.transaction_no = transaction_no;
	}

	public Integer getPackage_id() {
		return package_id;
	}

	public void setPackage_id(Integer package_id) {
		this.package_id = package_id;
	}

	public String getPackage_title() {
		return package_title;
	}

	public void setPackage_title(String package_title) {
		this.package_title = package_title;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOrder_price() {
		return order_price;
	}

	public void setOrder_price(String order_price) {
		this.order_price = order_price;
	}

	public String getPayment_channel_code() {
		return payment_channel_code;
	}

	public void setPayment_channel_code(String payment_channel_code) {
		this.payment_channel_code = payment_channel_code;
	}

	public String getOrder_icon() {
		return order_icon;
	}

	public void setOrder_icon(String order_icon) {
		this.order_icon = order_icon;
	}

	public String getOrder_type() {
		return order_type;
	}

	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}

	public String getPay_money() {
		return pay_money;
	}

	public void setPay_money(String pay_money) {
		this.pay_money = pay_money;
	}

	public String getRebate_ccplay_money() {
		return rebate_ccplay_money;
	}

	public void setRebate_ccplay_money(String rebate_ccplay_money) {
		this.rebate_ccplay_money = rebate_ccplay_money;
	}

	public String getRecharge_user_name() {
		return recharge_user_name;
	}

	public void setRecharge_user_name(String recharge_user_name) {
		this.recharge_user_name = recharge_user_name;
	}

	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	public String getPayment_channel_name() {
		return payment_channel_name;
	}

	public void setPayment_channel_name(String payment_channel_name) {
		this.payment_channel_name = payment_channel_name;
	}

	public String getCcplay_money() {
		return ccplay_money;
	}

	public void setCcplay_money(String ccplay_money) {
		this.ccplay_money = ccplay_money;
	}

	public String getCoupon_money() {
		return coupon_money;
	}

	public void setCoupon_money(String coupon_money) {
		this.coupon_money = coupon_money;
	}

	public Integer getRecharge_user_id() {
		return recharge_user_id;
	}

	public void setRecharge_user_id(Integer recharge_user_id) {
		this.recharge_user_id = recharge_user_id;
	}

	public String getDetail_link() {
		return detail_link;
	}

	public void setDetail_link(String detail_link) {
		this.detail_link = detail_link;
	}

	public String getDiscount_price() {
		return discount_price;
	}

	public void setDiscount_price(String discount_price) {
		this.discount_price = discount_price;
	}

}
