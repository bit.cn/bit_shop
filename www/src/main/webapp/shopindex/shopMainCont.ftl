<#macro shopMainCont queryType productList title>
<!--人气商品-->
<#if queryType=='hot'>
<div class="ani_product clearfix">
				<#if popularityAdvert??>
				<div class="ani_title">
					<div class="title hot"><a href="${systemSetting().lists}/special/hot.html" target="_Blank">更多&gt;&gt;</a></div>
					<div class="title_con">
						<a href="${popularityAdvert.advertLink!""}.html" target="_blank">
						<img src="${systemSetting().imageRootPath!""}/${popularityAdvert.advertImage!""}" />
						<div class="title_desc">
							<h3 style="text-align: center;">
							<#if popularityAdvert.title??>
								<#if popularityAdvert.title?length gte 17>
									${popularityAdvert.title?substring(0,17)}...
								<#else>
									${popularityAdvert.title!""}
								</#if>
							</#if>
							</h3>
						</div>
						</a>
					</div>
				</div>
				</#if>
				<div class="ani_detail bor_ff6600">
				<#list productList as item>
					<div class="d_product">
						<#if item.type!=0 && item.type==2>
							<div class="ysbg"></div>
						</#if>
						<p><span>NEW!</span>${item.groundingTime!""} UP</p>
						<a href="${systemSetting().item}/product/${item.productId}.html" target="_blank" title="${item.name!""}"><img src="${systemSetting().imageRootPath!""}/${item.picture!""}" /></a>
						<div class="pdesc">
							<a href="${systemSetting().item}/product/${item.productId}.html" target="_blank" title="${item.name!""}">${item.name!""}</a>
						</div>
						<div class="price">￥${item.nowPrice!"0.00"}</div>
					</div>
				</#list>	
				</div>
			</div>
</#if>
<!--最新登场商品-->
<#if queryType=='new'>
<div class="ani_product clearfix">
		<#if productList[0]??>
		<div class="ani_title">
					<div class="title new"><a href="${systemSetting().lists}/special/new.html" target="_blank">更多&gt;&gt;</a></div>
					<div class="title_con">
						<img src="${systemSetting().imageRootPath!""}/${productList[0].maxPicture!""}" />
						<div class="title_desc">
							<p style="text-align: left;"><span>NEW!</span>${productList[0].groundingTime!""} UP</p>
							<h3 style="text-align: left;"><a href="${systemSetting().item}/product/${productList[0].id!""}.html" target="_blank">
							<#if productList[0].name??>
								<#if productList[0].name?length gte 17>
									${productList[0].name?substring(0,17)}...
								<#else>
									${productList[0].name!""}
								</#if>
							</#if>
							</a></h3>
							<strong style="text-align: left;">￥${productList[0].nowPrice!"0.00"}</strong>
						</div>
					</div>
				</div>
				</#if>
				<div class="ani_detail bor_52a0e8">
					<#list productList as item>
					<div class="d_product">
						<#if item.productType!=0 && item.productType==2>
							<div class="ysbg"></div>
						</#if>
						<p><span>NEW!</span>${item.groundingTime!""} UP</p>
						<a href="${systemSetting().item}/product/${item.id}.html" title="${item.name!""}" target="_blank"><img src="${systemSetting().imageRootPath!""}/${item.maxPicture!""}"/></a>
						<div class="pdesc">
							<a href="${systemSetting().item}/product/${item.id}.html" title="${item.name!""}" target="_blank">
							<#if item.name??>
								<#if item.name?length gte 17>
									${item.name?substring(0,17)}...
								<#else>
									${item.name!""}
								</#if>
							</#if>
							</a>
						</div>
						<div class="price">￥${item.nowPrice!"0.00"}</div>
					</div>
					</#list>
				</div>
			</div>
</#if>
<!--预售商品-->
<#if queryType=='presale'>
<div class="ani_product clearfix">
				<div class="ani_title">
				<#if productList[0]??>
					<div class="title booking"><a href="${systemSetting().lists}/special/presale.html" target="_blank">更多&gt;&gt;</a></div>
					<div class="title_con">
						<img src="${systemSetting().imageRootPath!""}/${productList[0].maxPicture!""}" />
						<div class="title_desc">
							<div class="ys"></div>
							<p><span>NEW!</span>${productList[0].groundingTime!""} UP</p>
							<h3><a href="${systemSetting().item}/product/${productList[0].id!""}.html" target="_blank">
							<#if productList[0].name??>
								<#if productList[0].name?length gte 17>
									${productList[0].name?substring(0,17)}...
								<#else>
									${productList[0].name!""}
								</#if>
							</#if>
							</a></h3>
							<strong>￥${productList[0].nowPrice!"0.00"}</strong>
						</div>
					</div>
				</#if>
				</div>
			 <div class="ani_detail bor_52a0e8">
					<#list productList as item>
					<div class="d_product">
						<p><span>NEW!</span>${item.groundingTime!""} UP</p>
						<a href="${systemSetting().item}/product/${item.id}.html" title="${item.name!""}" target="_blank"><img src="${systemSetting().imageRootPath!""}/${item.maxPicture!""}"/></a>
						<div class="pdesc">
							<a href="${systemSetting().item}/product/${item.id}.html" title="${item.name!""}" target="_blank">
							<#if item.name??>
								<#if item.name?length gte 17>
									${item.name?substring(0,17)}...
								<#else>
									${item.name!""}
								</#if>
							</#if>
							</a>
						</div>
						<div class="price">￥${item.nowPrice!"0.00"}</div>
					</div>
					</#list>
				</div>
				</div>
			<div class="contbanner clearfix">
				<div class="cb_left">
					<a href="javascript:void(0);" style="cursor:default" target="_blank"><img src="${systemSetting().staticSource}/shopindex/layout/img/pic/anitoysimg_100.jpg" width="595" height="80"></a>
				</div>
				<div class="cb_right">
					<a href="javascript:void(0);" style="cursor:default" target="_blank"><img src="${systemSetting().staticSource}/shopindex/layout/img/pic/anitoysimg_102.jpg"></a>
				</div>
			</div>
			</#if>
		<!--现货商品-->
		<#if queryType=='spot'>
		<div class="ani_product">
				<div class="spot_title"><a href="${systemSetting().lists}/special/spot.html" target="_blank">更多&gt;&gt;</a></div>
				<div class="spot_detail clearfix">
				    <#list productList as item>
					<div class="spot">
						<a href="${systemSetting().item}/product/${item.id}.html" target="_blank">
							<img src="${systemSetting().imageRootPath!""}/${item.maxPicture!""}" />
							<div class="hide">
								<p>${item.groundingTime!""} UP</p>
								<span>
								<#if item.name??>
									<#if item.name?length gte 17>
										${item.name?substring(0,17)}...
									<#else>
										${item.name!""}
									</#if>
								</#if>
								</span>
								<strong>￥${item.nowPrice!"0.00"}</strong>
							</div>
						</a>
						</div>
					</#list>
				</div>
			</div>
</#if>
</#macro>
