<#macro shopMenuBannerNew title="" checkLogin=true>
<div class="mbn_menu">
	<#list systemManager().catalogs as item>
    <ul>
      <li class="sb">
      <span><a href="${basepath}/catalog/${item.code}.html">${item.name}</a></span><#if item.children??><#list item.children as sItem><a href="${basepath}/catalog/${sItem.code}.html">${sItem.name}</a>/</#list></#if>
      <div class="mbntk">
      	<ul>
      	<li><div class="mbntk_left">
          <ul>
          	<#if item.children??>
        	  	<#list item.children as sItem>
					<li>
		       			<a href="${basepath}/catalog/${sItem.code}.html">${sItem.name}</a>
					</li>
				</#list>
			</#if>
          </ul>
          <ul>
          	<a href="./detail.html"><img src="${systemSetting().staticSource}/shopindex/layout/img/pic/banner_02.jpg" width="540" height="168"></a>
          </ul>
          <div class="clear"></div>
        </div></li>
        <li><div class="mbntk_right">
        	<ul>
        	<li><a href="./search.html"><img src="${systemSetting().staticSource}/shopindex/layout/img/pic/anitoysimg_07.jpg" width="340" height="80"></a></li>
            <li><a href="./search.html"><img src="${systemSetting().staticSource}/shopindex/layout/img/pic/anitoysimg_11.jpg" width="340"  height="80"></a></li>
            <li><a href="./search.html"><img src="${systemSetting().staticSource}/shopindex/layout/img/pic/anitoysimg_09.jpg" width="340"  height="80"></a></li>
            </ul>
        </div></li>
        </ul>
        </div>
      </li>
    </ul>
    </#list>
  </div>
  <div class="mbn_banner">
    <div id="playBox">
      <div class="pre"></div>
      <div class="next"></div>
      <div class="smalltitle">
        <ul>
          <li class="thistitle"></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
      <!--轮播图-->
      <ul class="oUlplay">
      	<#list systemManager().indexImages as item>
		   <li>
		          <#if item.link??>
			          <a href="${item.link!""}" target="_blank">
			          	<img  width="634" height="278" src="${systemSetting().imageRootPath}/${item.picture!""}" >
			          </a>
		          <#else>
                      <img style="max-width: 100%;" src="${systemSetting().imageRootPath}/${item.picture!""}" >
		          </#if>
		    </li>
		</#list>
      </ul>
    </div>
  </div>
  <div class="mbn_new">
    <div class="mbnn_tit"><a href="${systemSetting().doc}/news/list">更多 ></a>最新情报<span>NEWS</span></div>
    <ul>
    	<#list systemManager().noticeList as item>
			<li><span>${item.createtime!""}</span><a href="${systemSetting().doc}/platnews/${item.id}" target="_blank" title="${item.title!""}">
				<#if item_index==0>[新]</#if>${item.title!""}</a>
			</li>
		</#list>      
    </ul>
  </div>
</#macro>
