<#import "/shopindex/common/ftl/shopBasePage.ftl" as shopBasePage>
<#import "/shopindex/common/ftl/shopTop.ftl" as shopTop>
<#import "/shopindex/common/ftl/shopHeadbg.ftl" as shopHeadbg>
<#import "/shopindex/common/ftl/shopChannel.ftl" as shopChannel>
<#import "/shopindex/common/ftl/shopFooter.ftl" as shopFooter>
<#import "/shopindex/shopMainCont.ftl" as shopMainCont>
<#import "/shopindex/shopContbot.ftl" as shopContbot>
<@shopBasePage.shopBasePage>
<head>
<title>商城主页</title>
</head>
<body>
<!--商城主页top页-->
<@shopTop.shopTop/>
<!--搜索站-->
<@shopHeadbg.shopHeadbg/>
<div class="pageWrap">
<!--导航-->
	<@shopChannel.shopChannel/>
	<!-- 人气商品、ANI强推、最新登场、预售、现货 -->
	<@shopMainCont.shopMainCont queryType="hot" productList=${popularityProductPlatList} title="人气商品"/>
	
	<!--"ANI强推-->
	<div class="ani_product clearfix">
				<div class="ani_title">
					<div class="title recomend"><a href="${systemSetting().lists}/special/sale.html" target="_blank">更多&gt;&gt;</a></div>
					<div class="title_con">
						<img class="h_491" src="${systemSetting().imageRootPath!""}/${systemManager().strongAdvertisingList[0].advertImage!""}" />
						<div class="title_desc">
							<#if systemManager().strongAdvertisingList[0]??>
								<a class="td_a" href="${strongAdvertisingList[0].advertLink!""}" target="_blank">${systemManager().strongAdvertisingList[0].title!""}</a>
							</#if>
						</div>
					</div>
				</div>
				<div class="ani_detail bor_ef3537">
					 <#list strongAdvertisingList as item>
						<#if (item_index>=1)>
						<div class="d_recomend">
							<div class="botDesc">
								<a href="${item.advertLink!""}" target="_blank">${item.title!""}</a>
							</div>
							<a class="mD"><img src="${systemSetting().imageRootPath!""}/${item.advertImage!""}" /></a>
						</div>
					</#if>
					</#list>	
				</div>
			</div>
	
	<@shopMainCont.shopMainCont queryType="new" productList=${newProducts} title="最新登场"/>
	<@shopMainCont.shopMainCont queryType="presale" productList=${preSalesProductList} title="预售商品"/>
	<@shopMainCont.shopMainCont queryType="spot" productList=${spotCommodityProducts} title="现货商品"/>
	<!-- 店铺链接 -->
	<div class="marginbot30">
		<@shopContbot.shopContbot/>
	</div>
</div>
<form action="${systemSetting().card}/cart/delete" method="POST" id="formDelete">
	<input type="hidden" name="id">
</form>
<script type="javascript">
			//搜索商品
			function search(){
				var _key = $.trim($("#mq").val());
				if(_key==''){
					return false;
				}
				$("#searchForm").submit();
			}
			//单个删除购物车商品
			function deleteFromCart(productId){
				if(productId){
					$("#formDelete :hidden[name=id]").val(productId);
					$("#formDelete").submit();
				}
			}
	</script>
<!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>
</body>
</@shopBasePage.shopBasePage>