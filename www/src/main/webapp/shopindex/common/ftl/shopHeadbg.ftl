<!--搜索栏-->
<#macro shopHeadbg>
<div class="header" style="margin-top:0px;">
			<div class="header_main clearfix"  style="background: #fff url(${systemSetting().imageRootPath}${systemManager().topPicture.topPicture}) no-repeat;">
				<div class="logo">
					<a href="${systemSetting().www}"><img src="${systemSetting().staticSource}/shopindex/common/img/logo.gif" border="0" /></a>
				</div>
				<!--search查询输入框-->
				<div class="search clearfix">
					<div class="searchInput">
						<form id="searchForm" method="get" accept-charset="utf-8" onsubmit="document.charset='utf-8';" action="${systemSetting().search}/search.html" target="_blank">
						<input type="text" name="key"  value="${key!""}" id="mq" value="ANI大搜查" maxlength="24" onfocus="if(this.value=='ANI大搜查') {this.value='';}this.style.color='#333';" onblur="if(this.value=='') {this.value='ANI大搜查';this.style.color='#999999';}" />
						<button type="submit" onclick="search();" target="_Blank">搜整站</button>
						</form>
					</div>
					<div class="hotSearch">
						<span>
						热门搜索：
						<#list systemManager().hotqueryList as item>
	      					<a href="${item.url}">${item.key1!""}</a>
						</#list>
						</span>
					</div>
				</div>
				<div class="header_nav">
					<div class="nav_title">所有周边[共有周边${productCount!""}个]</div>
					<!--大首页导航-->
					<div class="nav_box" style="display:none;">
						<ul>
							<li><a href="#" target="_top" class="home">首页</a></li>
							<li><a href="#" target="_top">折扣大卖场</a></li>
							<li><a href="#" target="_top">限定旗舰店</a></li>
							<li><a href="#" target="_top">活动抢先看</a></li>
						</ul>
					</div>
					<!--购物车-->
					<div class="car">
						<div class="carDesc">
							<span class="carlogo"><i>${productList?size}</i></span>
							<span class="carm"><a href="${systemSetting().card}/cart/cart.html">我的购物车</a></span>
						</div>
						<div class="carDetail">
            			<#if productList?? && productList?size gt 0>
          					<#list productList as item>
							<div class="carItem">
								<div class="img"><img src="${systemSetting().imageRootPath}/${item.picture}" /></div>

								<div class="itemDetail">
									<a href="${systemSetting().www}/cart/cart.html">${item.name!""}</a>
									<span>有特典</span>
									<span>¥ ${item.nowPrice}</span>
									<a href="#" onclick="deleteFromCart('${item.id!""}')">删除</a>
								</div>
							</div>
				  			</#list>
            			</#if>
							<div class="total">
								<span>共${productList?size}件商品 <b>共计¥${cartInfo.amount!""}</b></span>
								<a href="${systemSetting().www}/cart/cart.html">查看购物车</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  <script type="javascript">
//搜索商品
function search(){
	var _key = $.trim($("#mq").val());
	if(_key==''){
		return false;
	}
	$("#searchForm").submit();
}
</script>
</#macro>
