<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top.ftl" as shopTop>
<#import "/shopindex/common/ftl/shopChannel.ftl" as shopChannel>
<#import "/shopindex/shopMainCont.ftl" as shopMainCont>
<#import "/shopindex/shopContbot.ftl" as shopContbot>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/main/index.css">
	<title>索罗游商城</title>
</head>
<body>
	<!--商城主页top页-->
	<@shopTop.shopTop/>
	
	<!--导航，轮播图，最新情报-->
	<div class="pageWrap">
	<@shopChannel.shopChannel/>
	<!-- 人气商品、ANI强推、最新登场、预售、现货 -->
	<@shopMainCont.shopMainCont queryType="hot" productList=popularityProductPlatList title="人气商品"/>
		<!--"ANI强推-->
			<div class="ani_product clearfix">
				<div class="ani_title">
					<div class="title recomend"></div>
					<div class="title_con">
						<#if strongAdvertisingList[0]??>
								<img class="h_491" src="${systemSetting().imageRootPath!""}/${strongAdvertisingList[0].advertImage!""}" />
						</#if>
						<div class="title_desc" style="text-align: center;">
							<#if strongAdvertisingList[0]??>
								<a style="text-align: center;margin-left: 0px;" class="td_a" href="${strongAdvertisingList[0].advertLink!""}" target="_blank">
								<#if strongAdvertisingList[0].title??>
									<#if strongAdvertisingList[0].title?length gte 17>
										${strongAdvertisingList[0].title?substring(0,17)}...
									<#else>
										${strongAdvertisingList[0].title!""}
									</#if>
								</#if>
								</a>
							</#if>
						</div>
					</div>
				</div>
				<div class="ani_detail bor_ef3537">
					 <#list strongAdvertisingList as item>
						<#if (item_index>=1)>
						<div class="d_recomend">
							<div class="botDesc">
								<a href="${item.advertLink!""}" target="_blank">${item.title!""}</a>
							</div>
							<a href="${item.advertLink!""}"class="mD"><img src="${systemSetting().imageRootPath!""}/${item.advertImage!""}" /></a>
						</div>
					</#if>
					</#list>	
				</div>
			</div>
			<@shopMainCont.shopMainCont queryType="new" productList=newProducts title="最新登场"/>
			<@shopMainCont.shopMainCont queryType="presale" productList=preSalesProductList title="预售商品"/>
			<@shopMainCont.shopMainCont queryType="spot" productList=spotCommodityProducts title="现货商品"/>
			<!-- 店铺链接 -->
			<div class="marginbot30">
				<@shopContbot.shopContbot/>
			</div>
	</div>
	<!-- 友情链接 -->
	<div class="ani_product clearfix">
		<hr style="margin: 0px;">
			<div class="row" >
				<div class="col-xs-12" style="text-align: center;">
						<div style="text-align: center;margin: auto;">
							<#if navigations??>
							    <#list navigations as item>
	                                <div style="float: left;margin: 5px;">
	                                    <a href="${item.http!""}" target="_blank">${item.name!""}</a>
	                                </div>
							    </#list>
							</#if>
						</div>				
				</div>
			</div>
	    </div>
		<!-- 商城主页footer页 -->
		<@shopFooter.shopFooter/>
		<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
		<script src="${systemSetting().staticSource}/static/frontend/v1/js/hover.js"></script>
		<script type="javascript">
			//搜索商品 function search(){ var _key = $.trim($("#mq").val()); if(_key==''){ return false; } $("#searchForm").submit(); }
		</script>
</@htmlBase.htmlBase>
</body>
</html>
