<#macro shopTop checkLogin=true>
		<div class="topNav">
			<div class="top_main clearfix">
				<div class="top_left">
					<a href="${systemSetting().www}">"话不多说买买买"——欢迎来到anitoys</a>
					 <#if currentAccount()??>
					     <span class="sn-login"">欢迎回来，${currentAccount().account}</span>
					     <a href="${systemSetting().my}/account/exit" class="sn-regist">退出系统</a>
				    <#else >
					 <span>
						 <a href="${systemSetting().my}/account/login">请登陆</a>
						 <a href="${systemSetting().my}/account/register" class="regist">免费注册</a>
					 </span>
					</#if>
				</div>
				<div class="top_right">
					<ul>
				         <li><span><a href="${systemSetting().my}/account/orders">我的订单</a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?status=dbk">待补款订单<div class="bkddnum">
						   <#if Session.waitOrder?exists>   
		                     ${Session.waitOrder!""}
		                   <#else >
                      		0
                   			</#if>
				         </div></a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?" class="tran" target="_self">我的anitoys</a>
				          <div class="top_right_nav">
				              <a href="${systemSetting().my}/account/orders?status=dsh" target="_self">已发货订单</a>
				              <a href="${systemSetting().my}/account/custService" target="_self">售后订单</a>
				              <a href="${systemSetting().my}/coupon/coupons?status=0" target="_self">优惠券</a>
				              <a href="${systemSetting().my}/favorite/searchUserFavoritePro" target="_self">收藏的商品</a>
				              <a href="${systemSetting().my}/favorite/searchUserFavoriteBusi" target="_self">收藏的店铺</a>
				          </div>
				          </span>
				        </li>
						<li>
							<span>
								<a href="javascript:void(0);" target="_self" class="tran">帮助支持</a>
								<div class="top_right_nav">
								 <#if helpList??> 
								    <#list helpList as item>
											<a href="${systemSetting().doc}/front/helpsmanage/${item.id}.html" target="_self">
											<#if item.title?length gte 7>
												${item.title?substring(0,7)}...
											<#else>
												${item.title!""}
											</#if>
											
											</a>
									</#list>
                   		 		<#else >
                   		 		</#if>
								</div>
							</span>
						</li>
						<li>
							<span>
								<a href="javascript:void(0);" target="_Blank" class="tran">网站导航</a>
								<#if businessList??>
									<div class="top_right_nav imgS">
										<#list businessList as item>
												<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${item.id!""}"  target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"/>
												</a>
									</#list>
									</#if>
								</div>
							</span>
						</li>
						<li>
						<span>
							<a href="javascript:void(0);" target="_blank" class="tran">关注anitoys</a>
							<div class="top_right_nav w100">
								<a href="javascript:void(0);" target="_blank">
									<img src="${systemSetting().staticSource}/shopindex/layout/img/pic/towcode.png"/>
								</a>
							</div>
						</span>
					</li>
					</ul>
				</div>
			</div>
		</div>
		<!--搜索站-->
		<div class="header" style="margin-top:0px;">
			<div class="header_main clearfix"  style="background: #fff url(<#if topPicture??><#if topPicture.topPicture??>${systemSetting().imageRootPath}${topPicture.topPicture}</#if></#if>)no-repeat;">
				<div class="logo">
					<a href="${systemSetting().www}"><img src="${systemSetting().staticSource}/shopindex/common/img/logo.gif" border="0" /></a>
				</div>
				<!--search查询输入框-->
				<div class="search clearfix">
					<div class="searchInput">
						<form id="searchForm" method="get" accept-charset="utf-8" onsubmit="document.charset='utf-8';" action="${systemSetting().search}/search.html" target="_blank">
							<input type="text" name="key"  value="${key!""}" id="mq" value="ANI大搜查" maxlength="24" onfocus="if(this.value=='ANI大搜查') {this.value='';}this.style.color='#333';" onblur="if(this.value=='') {this.value='ANI大搜查';this.style.color='#999999';}" />
							<button type="submit" onclick="search();" target="_Blank">搜整站</button>
						</form>
					</div>
					<div class="hotSearch">
						<span>
						热门搜索：
							<#if hotqueryList??>
		      					<#list hotqueryList as item>
		      						<a href="${item.url}">${item.key1!""}</a>
								</#list>
							</#if>
						</span>
					</div>
				</div>
				<div class="header_nav">
					<div class="nav_title">所有周边[共有周边${productCount!""}个]</div>
					<!--大首页导航-->
					<div class="nav_box" style="display:none;">
						<ul>
							<li><a href="#" target="_top" class="home">首页</a></li>
							<li><a href="#" target="_top">折扣大卖场</a></li>
							<li><a href="#" target="_top">限定旗舰店</a></li>
							<li><a href="#" target="_top">活动抢先看</a></li>
						</ul>
					</div>
					<!--购物车-->
					<div class="car">
						<div class="carDesc">
							<span class="carlogo"><i>${productList?size}</i></span>
							<span class="carm"><a href="${systemSetting().card}/cart/cart.html">我的购物车</a></span>
						</div>
						<div class="carDetail">
							<#if productList?? && productList?size gt 0>
          					<#list productList as item>
							<div class="carItem">
								<div class="img"><img src="${systemSetting().imageRootPath}/${item.picture}" /></div>

								<div class="itemDetail">
									<a href="${systemSetting().item}/product/${item.id!""}.html">${item.name!""}</a>
									<span>有特典</span>
									<span>¥ ${item.nowPrice}</span>
									<a href="#" onclick="deleteFromCart('${item.pSpecId!""}')">删除</a>
								</div>
							</div>
				  			</#list>
            			</#if>
            			<div class="total">
							<span>共${productList?size}件商品 <b>共计¥${cartInfo.amount!""}</b></span>
							<a href="${systemSetting().card}/cart/cart.html">查看购物车</a>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<form action="${systemSetting().card}/cart/delete" method="POST" id="formDelete">
			<input type="hidden" name="id">
		</form>
		<script type="text/javascript">
		//单个删除购物车商品
		function deleteFromCart(productId){
			if(productId){
				$("#formDelete :hidden[name=id]").val(productId);
				$("#formDelete").submit();
			}
		}
		</script>
</#macro>