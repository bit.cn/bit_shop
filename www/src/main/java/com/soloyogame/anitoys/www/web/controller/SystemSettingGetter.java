package com.soloyogame.anitoys.www.web.controller;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.commond.SystemSetting;
import com.soloyogame.anitoys.service.SystemSettingService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 获取系统参数的配置
 * Created by dylan on 15-1-15.
 */
public class SystemSettingGetter implements TemplateMethodModelEx 
{
	    @Autowired
	    private SystemSettingService systemSettingService;
	    @Autowired
	    private RedisCacheProvider redisCacheProvider;

	    @Override
	    public Object exec(List arguments) throws TemplateModelException 
	    {
	    	SystemSetting systemSetting =JSON.parseObject(redisCacheProvider.get(ManageContainer.CACHE_SYSTEM_SETTING).toString(), SystemSetting.class);
	    	if(systemSetting==null)
	    	{
	    		systemSetting = systemSettingService.selectOne(new SystemSetting());
	    	}
	        return systemSetting;
	    }
}
