package com.soloyogame.anitoys.www.web.controller.front;


import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soloyogame.anitoys.service.ProductService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;

/**
 * 前端首页
 * @author shaojian
 */
@Controller
@RequestMapping("/")
public class IndexAction 
{
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(IndexAction.class);
	@Autowired
	private RedisCacheProvider redisCacheProvider; 
	@Autowired
	private ProductService productService; 
	
	@RequestMapping({"/", "/index"})
    public String index(ModelMap model) 
    {
		int productCount=productService.selectProductCount();
		model.addAttribute("productCount", productCount);
        return "index";
    }
	
		/**
	     * 拦截404错误跳转页面
	     * @param model
	     * @return
	     * @throws Exception
	     */
	    @RequestMapping(value = "to404")
	    public String to404(ModelMap model) throws Exception 
	    {
	        logger.info("跳转404页面！");
	        return "404Ftl";
	    }

}
