package com.soloyogame.anitoys.www.web.listener;

public interface CallBack {
	String callback() throws Exception;
}
