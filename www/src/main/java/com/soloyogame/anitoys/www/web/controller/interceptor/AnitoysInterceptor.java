package com.soloyogame.anitoys.www.web.controller.interceptor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.soloyogame.anitoys.db.bean.StrongAdvertising;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.db.commond.Business;
import com.soloyogame.anitoys.db.commond.CartInfo;
import com.soloyogame.anitoys.db.commond.Catalog;
import com.soloyogame.anitoys.db.commond.Help;
import com.soloyogame.anitoys.db.commond.Hotquery;
import com.soloyogame.anitoys.db.commond.IndexImg;
import com.soloyogame.anitoys.db.commond.Navigation;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.commond.PopularityAdvert;
import com.soloyogame.anitoys.db.commond.PopularityProductPlat;
import com.soloyogame.anitoys.db.commond.Product;
import com.soloyogame.anitoys.db.commond.SystemSetting;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.service.BusinessService;
import com.soloyogame.anitoys.service.CatalogService;
import com.soloyogame.anitoys.service.HelpService;
import com.soloyogame.anitoys.service.PlatHotqueryService;
import com.soloyogame.anitoys.service.PlatIndexImgService;
import com.soloyogame.anitoys.service.PlatNavigationService;
import com.soloyogame.anitoys.service.PlatNewsService;
import com.soloyogame.anitoys.service.PlatTopPictureService;
import com.soloyogame.anitoys.service.PopularityAdvertService;
import com.soloyogame.anitoys.service.PopularityProductPlatService;
import com.soloyogame.anitoys.service.ProductService;
import com.soloyogame.anitoys.service.StrongAdvertisingService;
import com.soloyogame.anitoys.service.SystemSettingService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;
import com.soloyogame.anitoys.www.web.util.LoginUserHolder;

/**
 * @author 索罗游
 */
public class AnitoysInterceptor implements HandlerInterceptor 
{

        private static final Logger LOGGER = LoggerFactory.getLogger(AnitoysInterceptor.class);

        @Autowired
        private BusinessService businessService;
        @Autowired
        private CatalogService catalogService;
        @Autowired
        private HelpService helpService;
        @Autowired
        private SystemSettingService systemSettingService;
        @Autowired
        private RedisCacheProvider redisCacheProvider;
        @Autowired
    	private PlatNewsService platNewsService;
        @Autowired
    	private ProductService productService; 
        @Autowired
    	private PlatHotqueryService hotqueryService;
        @Autowired
        private SystemSetting systemSetting;
        @Autowired
    	private PlatIndexImgService indexImgService;
        @Autowired
    	private PlatNavigationService navigationService;
        @Autowired
    	private PlatTopPictureService topPictureService;
        @Autowired
    	private StrongAdvertisingService strongAdvertisingService;
        @Autowired
    	private PopularityAdvertService popularityAdvertService;
        @Autowired
    	private PopularityProductPlatService popularityProductPlatService;

        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception 
        {
            // 加载 商家列表
            List<Business> businessList = loadBusinessList();
            request.setAttribute("businessList", businessList);
            
            // 加载文章目录
            List<Catalog> newCatalogs = loadCatalogNews();
            request.setAttribute("newsCatalogs", newCatalogs);

            // 加载帮助支持
            List<Help> helpList = loadHelpList();
            request.setAttribute("helpList", helpList);
            
            //最新登场
    		List<Product> newProducts = loadNewProducts();
    		request.setAttribute("newProducts", newProducts);
    		
            //预售商品
            List<Product> preSalesProductList = loadPreSalesProducts();
            request.setAttribute("preSalesProductList", preSalesProductList);
           
            //现货商品
            List<Product> spotCommodityProducts = loadSpotCommodityProducts();
            request.setAttribute("spotCommodityProducts", spotCommodityProducts);
           
            systemSetting=loadSystemSetting();
            
            //加载购物车信息
            CartInfo cartInfo = getMyCart();
            List<Product> productList = new ArrayList<Product>();
            if (cartInfo != null) 
            {
                productList = cartInfo.getProductList();
            } 
            else 
            {
                cartInfo = new CartInfo();
            }
            request.setAttribute("cartInfo", cartInfo);
            request.setAttribute("productList", productList);
            
        	//商品目录
    		List<Catalog> catalogs = loadCatalogs(true);
    		request.setAttribute("catalogs", catalogs);
    		
    		//热门查询
    		List<Hotquery> hotqueryList =loadHotquery();
    		request.setAttribute("hotqueryList", hotqueryList);
    		
    		//轮播图
    		List<IndexImg> indexImages = loadPlatIndexImgs();
    		request.setAttribute("indexImages", indexImages);
    		
    		//加载友情链接
    		List<Navigation> navigations = loadNavigations();
    		request.setAttribute("navigations", navigations);
    		
    		//加载最新情报
    		List<News> noticeList = loadNotices();
    		request.setAttribute("noticeList", noticeList);
    		
    		//加载大首页顶部图片
    		TopPicture topPicture = loadTopPicture();
    		request.setAttribute("topPicture", topPicture);
    		
    		//加载强推广告列表
    		List<StrongAdvertising> strongAdvertisingList =loadstrongAdvertising();
    		request.setAttribute("strongAdvertisingList", strongAdvertisingList);
    		
    		//加载人气商品列表
    		List<PopularityProductPlat> popularityProductPlatList = loadPopularityProductPlat();
    		request.setAttribute("popularityProductPlatList", popularityProductPlatList);
           
    		//人气商品广告位
    		PopularityAdvert popularityAdvert = loadPopularityAdvertPicture();
    		request.setAttribute("popularityAdvert", popularityAdvert);
    		return true;
        }

        /**
         * postHandle(controller处理过后 会进入此方法)
         * @Title: postHandle
         * @Description:
         * @param @param request
         * @param @param response
         * @param @param handler
         * @param @param modelAndView
         * @param @throws Exception
         * @throws
         */
        @Override
        public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {


        }

        @Override
        public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

        }
        
        /**
    	 * 加载人气商品广告位
    	 */
    	public PopularityAdvert loadPopularityAdvertPicture()
    	{ 
    		PopularityAdvert popularityAdvert = new PopularityAdvert();
            Serializable serializable;
            try 
            {
                serializable =  redisCacheProvider.get(ManageContainer.CACHE_POPULARITY_ADVERT);
            } 
            catch (Exception e) 
            {
                serializable = null;
            }
            String nullValue = "null";
            if (serializable == null || serializable.toString().equals(nullValue)) 
            {
            	PopularityAdvert popularityAdvertParam = new PopularityAdvert();
        		popularityAdvert = popularityAdvertService.selectOne(popularityAdvertParam);
            }
            else
            {
            	popularityAdvert = JSON.parseObject(serializable.toString(), PopularityAdvert.class);
            }
            redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_ADVERT, JSON.toJSONString(popularityAdvert));
            return popularityAdvert;
    	}
        
        /**
    	 * 加载人气商品列表
    	 */
    	public List<PopularityProductPlat>  loadPopularityProductPlat()
    	{ 
    		Serializable serializable;
   		 	List<PopularityProductPlat> popularityProductPlatList = new ArrayList<PopularityProductPlat>();
            try 
            {
                serializable =  redisCacheProvider.get(ManageContainer.CACHE_POPULARITY_PRODUCT_PLAT_LIST);
            } 
            catch (Exception e) 
            {
                serializable = null;
            }
            String nullValue = "null";
            if (serializable == null || serializable.toString().equals(nullValue)) 
            {
            	PopularityProductPlat popularityProductPlat = new PopularityProductPlat();
        		popularityProductPlat.setTop(10);
        		popularityProductPlatList = popularityProductPlatService.selectList(popularityProductPlat);
            }
            else
            {
            	popularityProductPlatList =  JSON.parseArray(serializable.toString(), PopularityProductPlat.class);
            }
            redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_PRODUCT_PLAT_LIST, JSON.toJSONString(popularityProductPlatList));
			return popularityProductPlatList;
    	} 
        
        /**
    	 * 加载强推广告列表
    	 */
    	public List<StrongAdvertising> loadstrongAdvertising()
    	{ 
    		Serializable serializable;
   		 	List<StrongAdvertising> strongAdvertisingList = new ArrayList<StrongAdvertising>();
            try 
            {
                serializable =  redisCacheProvider.get(ManageContainer.CACHE_STRONGADVERTISSING_LIST);
            } 
            catch (Exception e) 
            {
                serializable = null;
            }
            String nullValue = "null";
            if (serializable == null || serializable.toString().equals(nullValue)) 
            {
            	 StrongAdvertising strongAdvertising = new StrongAdvertising();
          		 strongAdvertisingList = strongAdvertisingService.selectList(strongAdvertising);
            }
            else
            {
            	strongAdvertisingList =  JSON.parseArray(serializable.toString(), StrongAdvertising.class);
            }
            redisCacheProvider.put(ManageContainer.CACHE_STRONGADVERTISSING_LIST, JSON.toJSONString(strongAdvertisingList));
			return strongAdvertisingList;
    	} 
        
        /**
    	 * 加载大首页的图片
    	 */
    	public TopPicture loadTopPicture()
    	{ 
    		TopPicture topPicture = new TopPicture();
            Serializable serializable;
            try 
            {
                serializable =  redisCacheProvider.get(ManageContainer.CACHE_TOPPICTURE_PLAT);
            } 
            catch (Exception e) 
            {
                serializable = null;
            }
            String nullValue = "null";
            if (serializable == null || serializable.toString().equals(nullValue)) 
            {
            	TopPicture topPictureParam = new TopPicture();
            	topPicture = topPictureService.selectOne(topPictureParam);
            }
            else
            {
            	topPicture = JSON.parseObject(serializable.toString(), TopPicture.class);
            }
            redisCacheProvider.put(ManageContainer.CACHE_TOPPICTURE_PLAT, JSON.toJSONString(topPicture));
            return topPicture;
    	}
        
        /**
    	 * 加载平台的友情链接
    	 */
    	public List<Navigation> loadNavigations() 
    	{
            Serializable serializable;
   		 	List<Navigation> navigations = new ArrayList<Navigation>();
            try 
            {
                serializable =  redisCacheProvider.get(ManageContainer.CACHE_NAVIGATION_PLAT);
            } 
            catch (Exception e) 
            {
                serializable = null;
            }
            String nullValue = "null";
            if (serializable == null || serializable.toString().equals(nullValue)) 
            {
            	Navigation nav = new Navigation();
        		nav.setPosition("bottom");
        		navigations = navigationService.selectList(nav);
            }
            else
            {
            	navigations =  JSON.parseArray(serializable.toString(), Navigation.class);
            }
            redisCacheProvider.put(ManageContainer.CACHE_NAVIGATION_PLAT, JSON.toJSONString(navigations));
			return navigations;
    	}
        
        /**
    	 * 加载系统通知
    	 */
    	public List<News> loadNotices() 
    	{
    		 Serializable serializable;
    		 List<News> noticeList = new ArrayList<News>();
             try 
             {
                 serializable =  redisCacheProvider.get(ManageContainer.CACHE_NOTICE_LIST);
             } 
             catch (Exception e) 
             {
                 serializable = null;
             }
             String nullValue = "null";
             if (serializable == null || serializable.toString().equals(nullValue)) 
             {
            	News news = new News();
         		news.setOffset(0);
         		news.setPageSize(10);
         		noticeList = platNewsService.selectPlatNoticeList(news);
             }
             else
             {
            	 noticeList =  JSON.parseArray(serializable.toString(), News.class);
             }
             redisCacheProvider.put(ManageContainer.CACHE_NOTICE_LIST, JSON.toJSONString(noticeList));
 			return noticeList;
    	}
        
        /**
    	 * 加载平台门户滚动图片列表
    	 */
    	public List<IndexImg> loadPlatIndexImgs() 
    	{
            Serializable serializable;
   		 	List<IndexImg> indexImages = new ArrayList<IndexImg>();
            try 
            {
                serializable =  redisCacheProvider.get(ManageContainer.CACHE_INDEX_IMAGES);
            } 
            catch (Exception e) 
            {
                serializable = null;
            }
            String nullValue = "null";
            if (serializable == null || serializable.toString().equals(nullValue)) 
            {
            	IndexImg c = new IndexImg();
        		indexImages = indexImgService.selectList(c);
            }
            else
            {
            	indexImages =  JSON.parseArray(serializable.toString(), IndexImg.class);
            }
            redisCacheProvider.put(ManageContainer.CACHE_INDEX_IMAGES, JSON.toJSONString(indexImages));
			return indexImages;
    	}
        
        /**
    	 * 加载热门查询列表
    	 */
    	public List<Hotquery>  loadHotquery()
    	{
    		 Serializable serializable;
    		 List<Hotquery> hotqueryList = new ArrayList<Hotquery>();
             try 
             {
                 serializable =  redisCacheProvider.get(ManageContainer.CACHE_HOTQUERY_LIST);
             } 
             catch (Exception e) 
             {
                 serializable = null;
             }
             String nullValue = "null";
             if (serializable == null || serializable.toString().equals(nullValue)) 
             {
            	 hotqueryList = hotqueryService.selectList(new Hotquery());
             }
             else
             {
            	 hotqueryList =  JSON.parseArray(serializable.toString(), Hotquery.class);
             }
            redisCacheProvider.put(ManageContainer.CACHE_HOTQUERY_LIST, JSON.toJSONString(hotqueryList));
			return hotqueryList;
    	}
        
        /**
    	 * 加载目录列表,树形结构
    	 * @param flag 是否加载商品数据。true：加载，false:不加载
    	 * @throws Exception
    	 */
    	public  List<Catalog> loadCatalogs(boolean flag) throws Exception 
    	{
    		 Serializable serializable;
        	 List<Catalog> catalogs = new ArrayList<Catalog>();
             try 
             {
                 serializable =  redisCacheProvider.get(ManageContainer.CACHE_CATALOGS);
             } 
             catch (Exception e) 
             {
                 serializable = null;
             }
             String nullValue = "null";
             if (serializable == null || serializable.toString().equals(nullValue)) 
             {
            	 catalogs = loadCatalogs2("p");
             }
             else
             {
            	 catalogs =  JSON.parseArray(serializable.toString(), Catalog.class);
             }
            redisCacheProvider.put(ManageContainer.CACHE_CATALOGS, JSON.toJSONString(catalogs));
			return catalogs;
    	}
        
        /**
         * 加载最新商品
         * @return
         */
        public List<Product> loadNewProducts()
        {
        	 Serializable serializable;
        	 List<Product> newProducts = new ArrayList<Product>();
             try 
             {
                 serializable =  redisCacheProvider.get(ManageContainer.CACHE_NEW_PRODUCTS);
             } 
             catch (Exception e) 
             {
                 serializable = null;
             }
             String nullValue = "null";
             if (serializable == null || serializable.toString().equals(nullValue)) 
             {
            	 newProducts = loadProducts(1);
             }
             else
             {
            	 newProducts =  JSON.parseArray(serializable.toString(), Product.class);
             }
            redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, JSON.toJSONString(newProducts));
			return newProducts;
        }
        
        /**
         * 加载预售商品
         * @return
         */
        public List<Product> loadPreSalesProducts()
        {
        	 Serializable serializable;
        	 List<Product> preSalesProductList = new ArrayList<Product>();
             try 
             {
                 serializable =  redisCacheProvider.get(ManageContainer.CACHE_PRESALES_PRODUCTLIST);
             } 
             catch (Exception e) 
             {
                 serializable = null;
             }
             String nullValue = "null";
             if (serializable == null || serializable.toString().equals(nullValue)) 
             {
            	 preSalesProductList = loadProducts(4);
             }
             else
             {
            	 preSalesProductList =  JSON.parseArray(serializable.toString(), Product.class);
             }
            redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, JSON.toJSONString(preSalesProductList));
			return preSalesProductList;
        }
        
        /**
         * 加载现货商品
         * @return
         */
        public List<Product> loadSpotCommodityProducts()
        {
        	 Serializable serializable;
        	 List<Product> spotCommodityProducts  = new ArrayList<Product>();
             try 
             {
                 serializable =  redisCacheProvider.get(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST);
             } 
             catch (Exception e) 
             {
                 serializable = null;
             }
             String nullValue = "null";
             if (serializable == null || serializable.toString().equals(nullValue)) 
             {
            	 spotCommodityProducts = loadProducts(5);
             }
             else
             {
            	 spotCommodityProducts =  JSON.parseArray(serializable.toString(), Product.class);
             }
            redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST, JSON.toJSONString(spotCommodityProducts));
			return spotCommodityProducts;
        }
        
        /**
         * 加载系统配置信息
         */
        public SystemSetting loadSystemSetting() 
        {
        	 Serializable serializable;
             try 
             {
                 serializable =  redisCacheProvider.get(ManageContainer.CACHE_SYSTEM_SETTING);
             } 
             catch (Exception e) 
             {
                 serializable = null;
             }
             String nullValue = "null";
             if (serializable == null || serializable.toString().equals(nullValue)) 
             {
            	 systemSetting = systemSettingService.selectOne(new SystemSetting());
             }
             else
             {
            	 systemSetting = JSON.parseObject(serializable.toString(), SystemSetting.class);
             }
             redisCacheProvider.put(ManageContainer.CACHE_SYSTEM_SETTING, JSON.toJSONString(systemSetting));
             return systemSetting;
        }


        /**
         * 得到底部的文章分类集合
         * @return
         */
        private   List<Catalog> loadCatalogNews() 
        {
            List<Catalog> newCatalogs = new ArrayList<Catalog>();
            Serializable serializable;
            try 
            {
                serializable =  redisCacheProvider.get(ManageContainer.CACHE_NEWSCATALOGS);
            } 
            catch (Exception e) 
            {
                serializable = null;
            }
            String nullValue = "null";
            if (serializable == null || serializable.toString().equals(nullValue)) 
            {
                // 查询 文章目录
                Catalog c = new Catalog();
                c.setType("a");
                c.setPid("0");
                c.setShowInNav("y");
                newCatalogs = catalogService.selectList(c);
                if (newCatalogs != null && newCatalogs.size() > 0) 
                {
                    for (int i = 0; i < newCatalogs.size(); i++) 
                    {
                        Catalog catalog = newCatalogs.get(i);
                        //加载此目录下的所有文章列表
                        News news = new News();
                        news.setCatalogID(catalog.getId());
                        news.setStatus("y");
                        List<News> newsList = platNewsService.selectPlatList(news);
                        catalog.setNews(newsList);
                    }
                }
            } 
            else 
            {
                newCatalogs =  JSON.parseArray(serializable.toString(), Catalog.class);
            }
            redisCacheProvider.put(ManageContainer.CACHE_NEWSCATALOGS, JSON.toJSONString(newCatalogs));
            return newCatalogs;
        }

        /**
         * 加载商家列表
         * @return List<Business>
         */
        private   List<Business> loadBusinessList() 
        {
            Business business = new Business();
            List<Business> businessList = new ArrayList<Business>();
            Serializable serializable;
            try 
            {
                serializable =  redisCacheProvider.get(ManageContainer.CACHE_BUSINESSLIST);
            } 
            catch (Exception e) 
            {
                serializable = null;
            }
            String nullValue = "null";
            if (serializable == null || serializable.toString().equals(nullValue)) 
            {
                // 查询 商家列表
                businessList = businessService.selectList(business);
            } 
            else 
            {
                businessList =  JSON.parseArray(serializable.toString(), Business.class);
            }
            redisCacheProvider.put(ManageContainer.CACHE_BUSINESSLIST, JSON.toJSONString(businessList));
            return businessList;
        }

    /**
     * 帮助支持列表
     * @return List<Help>
     */
    private List<Help> loadHelpList() 
    {
        List<Help> helpList = new ArrayList<Help>();
        Serializable serializable;
        try 
        {
            serializable =  redisCacheProvider.get(ManageContainer.CACHE_HELPLIST);
        } 
        catch (Exception e) 
        {
            serializable = null;
        }
        String nullValue = "null";
        if (serializable == null || serializable.toString().equals(nullValue) || JSON.parseArray(serializable.toString(), Help.class).size()==0) 
        {
            //	帮助支持列表
            Help help = new Help();
            help.setStatus("y");
            helpList = helpService.selectList(help);
        } 
        else 
        {
            helpList =  JSON.parseArray(serializable.toString(), Help.class);
        }
        redisCacheProvider.put(ManageContainer.CACHE_HELPLIST, JSON.toJSONString(helpList));
        return helpList;
    }
    
    protected CartInfo getMyCart() 
    {
        Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
        CartInfo cartInfo = null;
        if (account == null) 
        {
            cartInfo = (CartInfo) redisCacheProvider.get("myCart");
        } 
        else 
        {
            cartInfo = (CartInfo) redisCacheProvider.get("user_" + account.getId() + "Cart");
        }
        return cartInfo;
    }
    
    /**
	 * 原来递归的方式修改为非递归方式。
	 * 非递归方法查询商品/文章目录结构，并且自动排序。
	 * @param type
	 */
	private List<Catalog> loadCatalogs2(String type)
	{
        List<Catalog> catalogs = Lists.newLinkedList();
		Catalog cc = new Catalog();
		cc.setType(type);
		List<Catalog> catalogsList = catalogService.selectList(cc);
		if(catalogsList!=null)
		{

			Map<String, Catalog> map = new HashMap<String, Catalog>();
			for(Iterator<Catalog> it = catalogsList.iterator();it.hasNext();)
			{
				Catalog item = it.next();

				if(StringUtils.isNotBlank(item.getPid()) && item.getPid().equals("0"))
				{
					//是否在导航栏显示中文化
					if(item.getShowInNav().equals(Catalog.catalog_showInNav_y))
					{
						item.setShowInNavStr("是");
					}
					map.put(item.getId(), item);
					it.remove();
				}
			}

			for(Iterator<Catalog> it = catalogsList.iterator();it.hasNext();)
			{
				Catalog item = it.next();
				if(StringUtils.isNotBlank(item.getPid()))
				{
//							list.add(item);
					Catalog rootItem = map.get(item.getPid());
					if(rootItem!=null)
					{
						if(rootItem.getChildren()==null)
						{
							rootItem.setChildren(new LinkedList<Catalog>());
						}
						rootItem.getChildren().add(item);
					}
					it.remove();
				}
			}

			for(Iterator<Entry<String, Catalog>> it = map.entrySet().iterator();it.hasNext();)
			{
				catalogs.add(it.next().getValue());
			}

			//对主类别和子类别进行排序
			Collections.sort(catalogs, new Comparator<Catalog>() 
			{
				public int compare(Catalog o1, Catalog o2) 
				{
					if (o1.getOrder1() > o2.getOrder1()) 
					{
						return 1;
					} 
					else if (o1.getOrder1() < o2.getOrder1()) 
					{
						return -1;
					}
					return 0;
				}
			});

			for(int i=0;i<catalogs.size();i++)
			{
				if(catalogs.get(i).getChildren()==null)
				{
					continue;
				}
				Collections.sort(catalogs.get(i).getChildren(), new Comparator<Catalog>() {
					public int compare(Catalog o1, Catalog o2) 
					{
						if (o1.getOrder1() > o2.getOrder1()) 
						{
							return 1;
						} 
						else if (o1.getOrder1() < o2.getOrder1()) 
						{
							return -1;
						}
						return 0;
					}
				});
			}
		}
        return catalogs;
	}
	
		// 加载商品
		private List<Product> loadProducts(int type) 
		{
			Product p = new Product();
			//最新商品
			if (type == 1) 
			{
				p.setTop(10);
				p.setIsnew(Product.Product_isnew_y);//最新
			} 
			//特价商品
			else if (type == 2) 
			{
				p.setTop(8);
				p.setSale(Product.Product_sale_y);//特价
			} 
			//热门商品
			else if (type == 3) 
			{
				p.setTop(10);
				p.setHot(true);                   //热门
			}
			//预售商品
			else if (type == 4) 
			{
				p.setTop(10);
				p.setProductType(2);              //预售
			}
			//现货商品
			else if (type == 5) 
			{
				p.setTop(16);
				p.setProductType(1);              //现货
			}		
			return productService.selectFrontProductList(p);
		}
		
		/**
		 * 加载平台的文章目录
		 */
		private List<Catalog> loadNewCatalogs() 
		{
			Catalog c = new Catalog();
			c.setType("a");
			c.setPid("0");
			c.setShowInNav("y");
			c.setIsDeleted("n");
			List<Catalog> newCatalogs = catalogService.selectList(c);
			if(newCatalogs!=null && newCatalogs.size()>0)
			{
				for(int i=0;i<newCatalogs.size();i++)
				{
					Catalog item = newCatalogs.get(i);

					//加载此目录下的所有文章列表
					News news = new News();
					news.setCatalogID(item.getId());
					news.setStatus("y");
					List<News> newsList = platNewsService.selectPlatList(news);
					item.setNews(newsList);
				}
			}
			return newCatalogs;
		}
}
