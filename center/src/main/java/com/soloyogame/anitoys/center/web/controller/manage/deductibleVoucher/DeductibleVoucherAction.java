package com.soloyogame.anitoys.center.web.controller.manage.deductibleVoucher;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.soloyogame.anitoys.center.web.controller.BaseController;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.db.commond.AccountDeductibleVoucher;
import com.soloyogame.anitoys.db.commond.DeductibleVoucher;
import com.soloyogame.anitoys.service.AccountDeductibleVoucherService;
import com.soloyogame.anitoys.service.AccountService;
import com.soloyogame.anitoys.service.DeductibleVoucherService;
import com.soloyogame.anitoys.util.KeyTool;


/**
 * 折扣券管理
 * xukezhen
 * 
 */
@Controller
@RequestMapping("/manage/deductibleVoucher/")
public class DeductibleVoucherAction extends BaseController<DeductibleVoucher> {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DeductibleVoucherAction.class);
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private DeductibleVoucherService deductibleVoucherService;

	@Autowired
	private AccountService accountService;
	@Autowired
	private AccountDeductibleVoucherService accountDeductibleVoucherService;
    
    
	private static final String page_toList = "/manage/deductibleVoucher/deductibleVoucherList";
	private static final String page_toAdd = "/manage/deductibleVoucher/deductibleVoucherEdit";
	private static final String page_toSend = "/manage/deductibleVoucher/deductibleVoucherSend";
	
	private DeductibleVoucherAction() {
		super.page_toList = page_toList;
		super.page_toAdd = page_toAdd;
	}
	
	 public DeductibleVoucherService getDeductibleVoucherService() {
		return deductibleVoucherService;
	}

	public void setDeductibleVoucherService(
			DeductibleVoucherService deductibleVoucherService) {
		this.deductibleVoucherService = deductibleVoucherService;
	}


	public AccountDeductibleVoucherService getAccountDeductibleVoucherService() {
		return accountDeductibleVoucherService;
	}

	public void setAccountDeductibleVoucherService(
			AccountDeductibleVoucherService accountDeductibleVoucherService) {
		this.accountDeductibleVoucherService = accountDeductibleVoucherService;
	}
	public AccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}

	@Override
		public DeductibleVoucherService getService() {
			return deductibleVoucherService;
		}
	 
	 
	 @Override
		public String insert(HttpServletRequest request, DeductibleVoucher e, RedirectAttributes flushAttrs) throws Exception {
		 	e.setDeductibleSn(KeyTool.creatKey());
			return super.insert(request, e, flushAttrs);
		}


	 /**
	  *新增折扣券
	  */
	 @RequestMapping("saveDeductible")
	 public String editAddress(ModelMap model, DeductibleVoucher e)
	 {
		 e.setDeductibleSn(KeyTool.creatKey());
		 deductibleVoucherService.insert(e);
		 return "redirect:selectList?type=Y";
	 }
	 
	 /**
		 * 进入派发折扣券页面
		 */
		@RequestMapping("tosendDeductible")
		public String tosendCoupon(HttpServletRequest request, @ModelAttribute("e") DeductibleVoucher e, ModelMap model) throws Exception
		{
			e = getService().selectOne(e);
		    model.addAttribute("e", e);
			return page_toSend;
		} 
		

		/**
		 * 发送折扣券
	     * @return
	     * @throws Exception
	     */
	    @RequestMapping(value = "sendDeductible",method = RequestMethod.POST)
	    public String sendCoupon(HttpServletRequest request, @ModelAttribute("e") AccountDeductibleVoucher e, RedirectAttributes flushAttrs) throws Exception 
	    {
	    	System.out.println(e.toString());
	    	System.out.println("aa:"+e.getDeductibleId());
	    	System.out.println("bb:"+e.getDeductibleCount());
	    	System.out.println("cc:"+(e.getDeductibleCount()-1));
	    	
	    
	    	
	    	DeductibleVoucher d = new DeductibleVoucher();
	    	d.setId(e.getDeductibleId());
	    	d.setDeductibleCount(e.getDeductibleCount()-1);
	    	
	    	getService().update(d);
	    	accountDeductibleVoucherService.insert(e);
	        addMessage(flushAttrs, "操作成功！");
	        return "redirect:selectList";
	    }
	 
	    /**
		 * 根据会员ID模糊查询会员列表
		 * @param accountId
		 * @param model
		 * @return
		 */
		@RequestMapping("getAccountList")
		public void getAccountList(String accountId, HttpServletResponse response)
		{
			List<Account> accountList = accountService.getAccountList(accountId);
			PrintWriter out = null;
			response.setContentType("text/html;charset-utf-8");
			try 
			{
				out = response.getWriter();                            
				if(accountList!=null && accountList.size()!=0)
				{
					Gson gson = new Gson(); 
					String gsonString = gson.toJson(accountList);
					out.write(gsonString);
					out.close();
				}
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
}
