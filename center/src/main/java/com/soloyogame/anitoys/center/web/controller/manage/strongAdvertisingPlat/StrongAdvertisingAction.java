package com.soloyogame.anitoys.center.web.controller.manage.strongAdvertisingPlat;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.soloyogame.anitoys.center.web.controller.BaseController;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.bean.StrongAdvertising;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.StrongAdvertisingService;


/**
 * @author 索罗游
 */
@Controller
@RequestMapping("/manage/strongAdvertising")
public class StrongAdvertisingAction extends BaseController<StrongAdvertising> 
{
	private static final long serialVersionUID = 1L;
	private static final String page_toList = "/manage/strongAdvertisingPlat/strongAdvertisingPlatList";
	private static final String page_toEdit = "/manage/strongAdvertisingPlat/strongAdvertisingPlatEdit";
	private static final String page_toAdd = "/manage/strongAdvertisingPlat/strongAdvertisingPlatEdit";
	private StrongAdvertisingAction() {
		super.page_toList = page_toList;
		super.page_toAdd = page_toAdd;
		super.page_toEdit = page_toEdit;
	}
	
	@Autowired
	private StrongAdvertisingService strongAdvertisingService;

	public Services<StrongAdvertising> getService() {
		return strongAdvertisingService;
	}

	public void setStrongAdvertisingService(
			StrongAdvertisingService strongAdvertisingService) {
		this.strongAdvertisingService = strongAdvertisingService;
	}


	@Override
	public void insertAfter(StrongAdvertising e) {
		e.clear();
	}

	@Override
	protected void selectListAfter(PagerModel pager) {
		pager.setPagerUrl("selectList");
	}

	
	 /**
     * app平台推荐图的更新数据的方法，子类可以通过重写此方法实现个性化的需求。
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "updateApps", method = RequestMethod.POST)
    public String updateApps(HttpServletRequest request, @ModelAttribute("e") StrongAdvertising e, RedirectAttributes flushAttrs) throws Exception 
    {
    	if(!strongAdvertisingService.updateOrderIsValid(e)){//检查顺序是否重复，是否超过最大的数目
    		addMessage(flushAttrs, "操作失败，排序重复！");
    	    return "redirect:selectList";
    	}
    	getService().update(e);
        insertAfter(e);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
    /**
     * app轮播图的插入数据方法，子类可以通过重写此方法实现个性化的需求。
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "insertApp",method = RequestMethod.POST)
    public String insertApp(HttpServletRequest request, @ModelAttribute("e") StrongAdvertising e, RedirectAttributes flushAttrs) throws Exception 
    {
    	if(!strongAdvertisingService.orderIsValid(e.getAdvertPosition())){//检查顺序是否重复，是否超过最大的数目
    		addMessage(flushAttrs, "操作失败，排序重复或超过最大的广告位置数！");
    	    return "redirect:selectList";
    	}
        getService().insert(e);
        insertAfter(e);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
   
    /**
     * 公共的分页方法
     * @return
     * @throws Exception
     */
    @RequestMapping("selectList")
    public String selectList(HttpServletRequest request, @ModelAttribute("e") StrongAdvertising e) throws Exception 
    {
        /**
         * 由于prepare方法不具备一致性，加此代码解决init=y查询的时候条件不被清除干净的BUG
         */
        this.initPageSelect();

        setParamWhenInitQuery(e);

        int offset = 0;//分页偏移量
        if (request.getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(request.getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        e.setOffset(offset);
        PagerModel pager = getService().selectPageList(e);
        if (pager == null) 
        {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)/ pager.getPageSize());

        selectListAfter(pager);
        request.setAttribute("pager", pager);
        return page_toList;
    }
	
    /**
	 * 同步缓存
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("syncCache")
	public String syncCache(HttpServletRequest request, StrongAdvertising img) throws Exception{
		return super.selectList(request, img);
	}
	
	/**
     * 公共的批量删除数据的方法，子类可以通过重写此方法实现个性化的需求。
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "deletes", method = RequestMethod.POST)
    public String deletes(HttpServletRequest request, String[] ids, @ModelAttribute("e") StrongAdvertising e, RedirectAttributes flushAttrs) throws Exception
    {
    	strongAdvertisingService.deletess(ids);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
	
}
