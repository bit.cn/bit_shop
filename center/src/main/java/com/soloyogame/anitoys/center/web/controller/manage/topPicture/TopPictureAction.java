package com.soloyogame.anitoys.center.web.controller.manage.topPicture;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soloyogame.anitoys.center.web.controller.BaseController;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.service.PlatTopPictureService;



/**
 * 大首页的顶部图片Action
 * @author 索罗游
 */
@Controller
@RequestMapping("/manage/topPicture/")
public class TopPictureAction extends BaseController<TopPicture> 
{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(TopPictureAction.class);
    private static final String page_toList = "/manage/topPicture/topPictureList";
    private static final String page_toEdit = "/manage/topPicture/topPictureEdit";
    private static final String page_toAdd = "/manage/topPicture/topPictureEdit";
    
    private TopPictureAction() 
    {
        super.page_toList = page_toList;
        super.page_toAdd = page_toAdd;
        super.page_toEdit = page_toEdit;
    }
    @Autowired
	private PlatTopPictureService topPictureService;

    @Autowired
	public PlatTopPictureService getService() {
		return topPictureService;
	}

	public void TopPictureService(PlatTopPictureService topPictureService) {
		this.topPictureService = topPictureService;
	}

	@Override
	public void insertAfter(TopPicture e) {
		e.clear();
	}
}
