package com.soloyogame.anitoys.center.web;

import com.soloyogame.anitoys.db.commond.SystemSetting;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

import java.util.List;

/**
 * 获取系统参数的配置
 * Created by dylan on 15-1-15.
 */
public class SystemSettingGetter implements TemplateMethodModelEx {
    @Override
    public Object exec(List arguments) throws TemplateModelException {
        SystemSetting systemSetting = new SystemSetting();
        systemSetting.setId("1");
        systemSetting.setSystemCode("anitoys");
        systemSetting.setWww("http://localhost:8080/center");
        systemSetting.setLog("http://localhost:8080/center");
        systemSetting.setTitle("anitoys");
        systemSetting.setDescription("anitoys");
        systemSetting.setKeywords("anitoys");
        systemSetting.setShortcuticon("http://localhost:8080/center");
        systemSetting.setAddress("XXX省XXX市XXX区XXX号XX弄301");
        systemSetting.setTel("1213434");
        systemSetting.setEmail("jpsl2012@163.com");
        systemSetting.setIcp("@ICP备");
        systemSetting.setCloseMsg("..........");
        systemSetting.setIsopen("true");
        systemSetting.setManageHttp("http://business.rc.anitoys.com");
        systemSetting.setImageRootPath("http://uploads.anitoys.com");
        systemSetting.setDefaultProductImg("http://static.anitoys.com/");
        systemSetting.setManageLeftTreeLeafIcon("http://static.anitoys.com/");
        systemSetting.setStatisticsCode("<script type=\"text/javascript\">var cnzz_protocol = ((\"https:\" == document.location.protocol) ? \" https://\" : \" http://\");document.write(unescape(\"%3Cspan id='cnzz_stat_icon_1000234875'%3E%3C/span%3E%3Cscript src='\" + cnzz_protocol + \"s96.cnzz.com/z_stat.php%3Fid%3D1000234875%26show%3Dpic' type='text/javascript'%3E%3C/script%3E\"));</script>");
        systemSetting.setOpenResponsive("y");
        systemSetting.setName("anitoys");
        systemSetting.setCard("http://localhost:8080/center");
        systemSetting.setDoc("http://localhost:8080/center");
        systemSetting.setBusiness("http://localhost:8080/center");
        systemSetting.setMy("http://localhost:8080/center");
        systemSetting.setOrders("http://localhost:8080/center");
        systemSetting.setSearch("http://localhost:8080/center");
        systemSetting.setItem("http://localhost:8080/center");
        systemSetting.setLists("http://localhost:8080/center");
        systemSetting.setPassport("http://localhost:8080/center");
        systemSetting.setPay("http://localhost:8080/center");
        systemSetting.setStaticSource("http://static.anitoys.com/resource");
        systemSetting.setBusinessFront("http://localhost:8080/center");
        systemSetting.setVersion("1.0");
        return systemSetting;
    }
}
