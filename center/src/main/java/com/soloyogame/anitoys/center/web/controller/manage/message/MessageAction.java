package com.soloyogame.anitoys.center.web.controller.manage.message;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.center.web.controller.BaseController;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.db.commond.KeyValueHelper;
import com.soloyogame.anitoys.db.commond.Keyvalue;
import com.soloyogame.anitoys.db.commond.Message;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.AccountService;
import com.soloyogame.anitoys.service.KeyvalueService;
import com.soloyogame.anitoys.service.MessageService;

/**
 * 消息
 * @author dylan
 */
@Controller
@RequestMapping("/manage/message/")
public class MessageAction extends BaseController<Message> {
	private static final long serialVersionUID = 1L;
    @Autowired
	private MessageService messageService;
    @Autowired
   	private KeyvalueService keyvalueService;
    @Autowired
    private AccountService accountService;

    private static final String page_toList = "/manage/message/messageList";
    private static final String page_toAdd = "/manage/message/messageInsert";
    private static final String page_toEdit = "/manage/message/messageEdit";
    MessageAction(){
        super.page_toList = page_toList;// not support
        super.page_toAdd = page_toAdd;// not support
        super.page_toEdit = page_toEdit;
    }

    @Override
	protected void selectListAfter(PagerModel pager) {
		pager.setPagerUrl("selectList");
	}

	

 
	@Override
	public void insertAfter(Message e) {
		e.clear();
	}

	@Override
    @RequestMapping(value = "insert", method = RequestMethod.POST)
	public String insert(HttpServletRequest request, Message e, RedirectAttributes flushAttrs) throws Exception 
	{
		String title = e.getTitle();
		String content=e.getContent();
		e.setTitle(title);
		e.setContent(content);
		e.setIsNew(1);
		e.setStatus(1);
		logger.debug("plat add a message ---->"+JSON.toJSONString(e));
		Account account = accountService.selectAccount(e.getAccountId());
		e.setAccountId(account.getId());
		super.insert(request, e, flushAttrs);
		loadKeyValue();
		return "redirect:selectList";
	}
	
	/**
	 * 加载key-value键值对
	 */
	public void loadKeyValue() {
		logger.info("load...");
		KeyValueHelper.load(keyvalueService.selectList(new Keyvalue()));
	}

	@Override
    @RequestMapping(value = "update", method = RequestMethod.GET)
	public String update(HttpServletRequest request, Message e, RedirectAttributes flushAttrs) throws Exception {
		String title = e.getTitle();
		title = new String(title.getBytes("iso8859-1"),"utf-8");
		String content=e.getContent();
		content = new String(content.getBytes("iso8859-1"),"utf-8");
		e.setTitle(title);
		e.setContent(content);
		super.update(request, e, flushAttrs);
//		KeyValueHelper.load(getPager().getList());
		return "redirect:selectList";
	}
	@Override
	public Services<Message> getService() {
		return messageService;
	}
	
}
