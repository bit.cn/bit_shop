package com.soloyogame.anitoys.center.web.controller.manage.system;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.soloyogame.anitoys.center.web.controller.BaseController;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.bean.Dict;
import com.soloyogame.anitoys.db.bean.DictItem;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.DictItemService;
import com.soloyogame.anitoys.service.DictService;
import com.soloyogame.anitoys.util.StringTool;

/**
 * 字典管理控制类
 * @author shaojian
 */
@Component
@RequestMapping("/manage/dict/")
public class DictManageAction extends BaseController<Dict> 
{
	private static final long serialVersionUID = 1L;
	private static final String page_toList = "/manage/system/dict/dictList";
	private static final String page_toAdd = "/manage/system/dict/editDict";
    private static final String page_toEdit = "/manage/system/dict/editDict";
    private final String page_toAddOrUpdate = "/manage/system/dict/addOrUpdate";
    
    public DictManageAction() 
	{
		 super.page_toList = page_toList;
	     super.page_toEdit = page_toAdd;
	     super.page_toAdd = page_toEdit;
	}
	
    @Autowired
	private DictService dictService;
    @Autowired
	private DictItemService dictItemService;
    
    @Override
	 public Services<Dict> getService() 
	 {
	    return dictService;
	 }
    
    public DictItemService getDictItemService() {
		return dictItemService;
	}



	public void setDictItemService(DictItemService dictItemService) {
		this.dictItemService = dictItemService;
	}

	/**
     * 查询分页列表
     */
    @Override
   	protected void selectListAfter(PagerModel pager) 
    {
   		pager.setPagerUrl("selectList");
   	}
    
    /**
	 * 根据dictId 得到数据字典的根节点
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("getDictItemsByDictId")
    @ResponseBody
	public String getDictItemsByDictId(HttpServletRequest request) throws Exception 
	{
		String dictId = request.getParameter("dictId");
		List<DictItem> dictItems = dictService.loadDictItems(dictId);
		return writeDictItems(dictItems);
	}
    
    //输出字典到页面
  	private String writeDictItems(List<DictItem> root) throws IOException
  	{
  		JSONArray json = JSONArray.fromObject(root);
  		String jsonStr = json.toString();
  		try 
  		{
  			return jsonStr;
  		} 
  		catch (Exception e) 
  		{
  			e.printStackTrace();
  		}
  		return jsonStr;
  	}
  	
  	/**
	 * 转到 添加/修改字典 页面
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("toAddOrUpdate")
	public String toAddOrUpdate(String id, ModelMap model) throws Exception
	{
    	DictItem dictItem = dictService.selectDictItemById(id);
        model.addAttribute("e", dictItem);
		return page_toAddOrUpdate;
	}
    
    /**
	 * 添加主类/修改主类、添加子类
	 * @return
	 * @throws IOException
	 */
    @RequestMapping(value = "doAddOrUpdate", method = RequestMethod.POST)
    @ResponseBody
	public String doAddOrUpdate(HttpServletRequest request,@ModelAttribute("e") DictItem e) throws IOException
	{
    	if(StringTool.isNull(e.getId()))
    	{
    		 dictItemService.insert(e);
    	}
    	else
    	{
    		String childrenid = request.getParameter("childrenid");
			String childrenpid = request.getParameter("childrenpid");
			String childrendic_id = request.getParameter("childrendic_id");
			String childrenitem_code = request.getParameter("childrenitem_code");
			String childrenitem_name = request.getParameter("childrenitem_name");
			String childrenitem_value = request.getParameter("childrenitem_value");
			String childrencore_value = request.getParameter("childrencore_value");
			String childrenitem_extra = request.getParameter("childrenitem_extra");
			String childrenorder_no = request.getParameter("childrenorder_no");
    		dictItemService.update(e);
    		DictItem children = new DictItem();
    		children.setId(childrenid);
    		children.setPid(childrenpid);
    		children.setDic_id(childrendic_id);
    		children.setItem_code(childrenitem_code);
    		children.setItem_name(childrenitem_name);
    		children.setItem_value(childrenitem_value);
    		children.setCore_value(childrencore_value);
    		children.setItem_extra(childrenitem_extra);
    		children.setOrder_no(Integer.valueOf(childrenorder_no));
    		dictItemService.insert(children);
    	}
			return "0";
	}
    
    /**
     * 重写根据主键ID删除对象
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "deleteById")
    public String deleteById(HttpServletRequest request, String id, RedirectAttributes flushAttrs) throws Exception 
    {
    	Dict dict = new Dict();
    	dict.setDic_id(id);
    	dictService.delete(dict);
		List<DictItem> dictItemList = dictItemService.loadDictItems(id);
		for (DictItem dictItem : dictItemList) 
		{
			dictItemService.delete(dictItem);
		}
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
    
    /**
	 * 对树的删除操作
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
	public String delete(HttpServletRequest request) throws Exception
	{
		String ids = request.getParameter("ids");
		if(StringUtils.isBlank(ids))
		{
			throw new Exception("非法请求！");
		}
		logger.error("delete.ids="+ids+",deleteParent="+request.getParameter("deleteParent"));
		dictItemService.deletesDictItemNode(ids,request.getParameter("deleteParent"));
		
		//删除成功返回1
		return "1";
	}
}