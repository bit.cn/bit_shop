package com.soloyogame.anitoys.center.web.controller.manage.indexImg;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.soloyogame.anitoys.center.web.controller.BaseController;
import com.soloyogame.anitoys.center.web.util.RequestHolder;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.IndexImg;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.PlatIndexImgService;

/**
 * 滚动图片
 * @author 索罗游
 */
@Controller
@RequestMapping("/manage/indexImg/")
public class IndexImgAction extends BaseController<IndexImg> 
{
	private static final long serialVersionUID = 1L;
	private static final String page_toList = "/manage/indexImg/indexImgList";
	private static final String page_toEdit = "/manage/indexImg/indexImgEdit";
	private static final String page_toAdd = "/manage/indexImg/indexImgEdit";
	private IndexImgAction() {
		super.page_toList = page_toList;
		super.page_toAdd = page_toAdd;
		super.page_toEdit = page_toEdit;
	}
	@Autowired
	private PlatIndexImgService imgService;

	@Override
	public Services<IndexImg> getService() {
		return imgService;
	}

	@Override
	public void insertAfter(IndexImg e) {
		e.clear();
	}

	@Override
	protected void selectListAfter(PagerModel pager) {
		pager.setPagerUrl("selectList");
	}

	//上传文件
	@Deprecated
	private void uploadImage(MultipartFile image, IndexImg e) throws IOException
	{
		if(image==null)
		{
			return;
		}
		String imageName = String.valueOf(System.currentTimeMillis()) + ".jpg";
		String realpath = RequestHolder.getSession().getServletContext().getRealPath("/indexImg/");
		logger.info("realpath: " + realpath);
		if (image != null) 
		{
			File savefile = new File(new File(realpath), imageName);
			if (!savefile.getParentFile().exists()) 
			{
				savefile.getParentFile().mkdirs();
			}
			image.transferTo(savefile);
			
//			FileUtils.copyFile(image, savefile);
//			ActionContext.getContext().put("message", "文件上传成功");
		}
//		SystemInfo sInfo = SystemSingle.getInstance().getSystemInfo();
//		String url = sInfo.getWww_ip() + "/file/img/" + imageName;
		String url = "/indexImg/" + imageName;
		e.setPicture(url);
		image = null;
	}
	
	/**
	 * 同步缓存
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("syncCache")
	public String syncCache(HttpServletRequest request, IndexImg img) throws Exception{
//		SystemSingle.getInstance().sync(Container.imgList);
		return super.selectList(request, img);
	}

	@Override
	public String insert(HttpServletRequest request, IndexImg e,RedirectAttributes flushAttrs) throws Exception 
	{
		List<IndexImg> list = imgService.selectPageList(new IndexImg()).getList();
		int totle = list == null ? 0 : list.size();
		if(totle > 7){//最多8张
			addMessage(flushAttrs, "添加失败，最多只能上传8张！");
	   		return "redirect:selectList";
		}
		IndexImg indexImgParam = new IndexImg();
		indexImgParam.setOrder1(e.getOrder1());
		IndexImg indexImg = imgService.selectOne(indexImgParam);
		if(indexImg!=null){
			addMessage(flushAttrs, "不得设置重复的顺序，增加失败！");
   		 return "redirect:selectList";
		}
		String realpath = RequestHolder.getSession().getServletContext().getRealPath("/");
		if(e.getPicture() != null)
		{
			File img = new File(realpath+e.getPicture());
			if(img != null)
			{
				logger.debug("__++_+_+_+_+_+_+_+_+_+_+_+_+_+_+");
//				if(ImageUtils.imageSizeIsAvailable(img, 630, 180))
//				{
//					logger.error("门户轮播图尺寸过大^^^^^^^^^^^^^^^^^^^^^^^^^^--->门户轮播图尺寸过大！！！");
//					addMessage(flushAttrs, "图片尺寸过大，请按照要求提交图片！");
//			        return "redirect:selectList";
//				}
//				else
//				{
//					logger.debug("图片尺寸整合适！！！！！！！！！！！！！！！");
//				}
			}
		}
		return super.insert(request, e, flushAttrs);
	}
	/**
	 * 检查总条数
	 * @return
	 */
	@RequestMapping("totle")
	@ResponseBody
	public Object totle(){
		Map<String, Integer> resultMap = new HashMap<String, Integer>();
		List<IndexImg> list = imgService.selectPageList(new IndexImg()).getList();
		int totle = list == null ? 0 : list.size();
		resultMap.put("totle", totle);
		return resultMap;
	}
	/**
	 * 检查排序唯一性
	 * @param e
	 * @return
	 */
	@RequestMapping("order")
	@ResponseBody
	public Object order(IndexImg e){
		Map<String, String> resultMap = new HashMap<String, String>();
		IndexImg indexImgParam = new IndexImg();
		indexImgParam.setOrder1(e.getOrder1());
		IndexImg indexImg = imgService.selectOne(indexImgParam);
		resultMap.put("status", indexImg == null ? "ok":"error");
		return resultMap;
	}
}
