package com.soloyogame.anitoys.center.web.controller.interceptor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.commond.KeyValueHelper;
import com.soloyogame.anitoys.db.commond.Keyvalue;
import com.soloyogame.anitoys.db.commond.SystemSetting;
import com.soloyogame.anitoys.service.KeyvalueService;
import com.soloyogame.anitoys.service.SystemSettingService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

/**
 * @author 索罗游
 */
public class AnitoysInterceptor implements HandlerInterceptor 
{
        private static final Logger LOGGER = LoggerFactory.getLogger(AnitoysInterceptor.class);

        @Autowired
        private SystemSetting systemSetting;
        @Autowired
        private SystemSettingService systemSettingService;
        @Autowired
        private RedisCacheProvider redisCacheProvider;
        @Autowired
    	private KeyvalueService keyvalueService;

        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception 
        {
        	systemSetting=loadSystemSetting();
            //加载键值对
            loadKeyValue();
            return true;
        }

        /**
         * postHandle(controller处理过后 会进入此方法)
         * @Title: postHandle
         * @Description:
         * @param @param request
         * @param @param response
         * @param @param handler
         * @param @param modelAndView
         * @param @throws Exception
         * @throws
         */
        @Override
        public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {


        }

        @Override
        public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

        }
   
    
    /**
	 * 加载key-value键值对
	 */
	public List<Keyvalue> loadKeyValue() 
	{
		 List<Keyvalue> keyVaueList = new ArrayList<Keyvalue>();
         Serializable serializable;
         try 
         {
             serializable =  redisCacheProvider.get(ManageContainer.CACHE_KEYVALUE_LIST);
         } 
         catch (Exception e) 
         {
             serializable = null;
         }
         String nullValue = "null";
         if (serializable == null || serializable.toString().equals(nullValue)) 
         {
        	 keyVaueList = keyvalueService.selectList(new Keyvalue());
         } 
         else 
         {
        	 keyVaueList =  JSON.parseArray(serializable.toString(), Keyvalue.class);
         }
         redisCacheProvider.put(ManageContainer.CACHE_KEYVALUE_LIST, JSON.toJSONString(keyVaueList));
         KeyValueHelper.load(keyVaueList);
         return keyVaueList;
	}
	
	 /**
     * 加载系统配置信息
     */
    public SystemSetting loadSystemSetting() 
    {
    	 Serializable serializable;
         try 
         {
             serializable =  redisCacheProvider.get(ManageContainer.CACHE_SYSTEM_SETTING);
         } 
         catch (Exception e) 
         {
             serializable = null;
         }
         String nullValue = "null";
         if (serializable == null || serializable.toString().equals(nullValue)) 
         {
        	 systemSetting = systemSettingService.selectOne(new SystemSetting());
         }
         else
         {
        	 systemSetting = JSON.parseObject(serializable.toString(), SystemSetting.class);
         }
         redisCacheProvider.put(ManageContainer.CACHE_SYSTEM_SETTING, JSON.toJSONString(systemSetting));
         return systemSetting;
    }


}
