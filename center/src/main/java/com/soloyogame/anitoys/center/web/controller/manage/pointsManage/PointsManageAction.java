package com.soloyogame.anitoys.center.web.controller.manage.pointsManage;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.soloyogame.anitoys.center.web.controller.BaseController;
import com.soloyogame.anitoys.center.web.util.LoginUserHolder;
import com.soloyogame.anitoys.center.web.util.RequestHolder;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.bean.User;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.db.commond.AccountFinance;
import com.soloyogame.anitoys.db.commond.AccountPointsLog;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.AccountFinanceService;
import com.soloyogame.anitoys.service.AccountService;
import com.soloyogame.anitoys.service.PointsManageService;



/**
 * 积分管理
 * @author jiangyongzhi
 */
@Controller
@RequestMapping("/manage/pointsManage/")
public class PointsManageAction extends BaseController<Account>{
	private static final Logger logger = LoggerFactory.getLogger(PointsManageAction.class);
    @Autowired
	private AccountService accountService;
    @Autowired
    PointsManageService pointsManageService;
    @Autowired
    AccountFinanceService accountFinanceService;
    
    private static final String page_toList = "/manage/pointsManage/pointsManage";
    private static final String page_toEdit = "/manage/pointsManage/pointsEdit";
    private static final String page_toDetail = "/manage/pointsManage/pointsDetail";
    private PointsManageAction(){
        super.page_toList = page_toList;
        super.page_toEdit = page_toEdit;
        super.page_toAdd = page_toAdd;
    }
    @Override
	protected void selectListAfter(PagerModel pager) {
		pager.setPagerUrl("selectList");
	}
    
	@Override
    public Services<Account> getService() {
        return accountService;
    }

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}
	
	@RequestMapping("toPointsManage")
	public String toPointsManage(ModelMap model) throws Exception
	{
		return page_toList;
	}
	
	/**
	 * 新增积分初始化
	 */
	@RequestMapping("toPointsAdd")
	public String toEdit(@ModelAttribute("e") Account e, ModelMap model) throws Exception
	{
		User user = LoginUserHolder.getLoginUser();
		if (user == null || StringUtils.isBlank(user.getUsername())) 
		{
			throw new NullPointerException("登陆超时！");
		}
		
		e = getService().selectOne(e);
		AccountFinance accountFinance= accountFinanceService.selAccountFinanceByUserId(e.getId());
		if(accountFinance==null){
			e.setAmount("0");
			e.setScore(0);
		}else{
			e.setAmount(String.valueOf(accountFinance.getAmount()));
			e.setScore(accountFinance.getRank());		
		}
		model.addAttribute("e",e);
		return page_toEdit;
	}
	
	/**
	 * 查看用户部分信息及积分,余额等
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("selectList")
	public String selectList(HttpServletRequest request, @ModelAttribute("e") Account e) throws Exception
	{
		int offset = 0;//分页偏移量
        if (request.getParameter("pager.offset") != null) {
            offset = Integer
                    .parseInt(request.getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        e.setOffset(offset);
          PagerModel pager = accountService.selectPageListFinance(e);
        if (pager == null) {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)
                / pager.getPageSize());
        pager.setPagerUrl("selectList");
        request.setAttribute("pager", pager);    
		return page_toList;
	}
	/**
	 * 查看积分详情
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("toPointsDetail")
	public String toDetail(HttpServletRequest request, @ModelAttribute("e") AccountPointsLog e) throws Exception
	{
		int offset = 0;//分页偏移量
		if (request.getParameter("pager.offset") != null) {
			offset = Integer
					.parseInt(request.getParameter("pager.offset"));
		}
		if (offset < 0)
			offset = 0;
		e.setOffset(offset);
		PagerModel pager = pointsManageService.selectPageList(e);
		if (pager == null) {
			pager = new PagerModel();
		}
		// 计算总页数
		pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)
				/ pager.getPageSize());
		
		pager.setPagerUrl("toPointsDetail");
		request.setAttribute("pager", pager);
		request.setAttribute("userId", e.getUserId());
		return page_toDetail;
	}
	
	/**
	 * 新增积分确认
	 * @param e
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("addPoinst")
	public String addPoinst(@ModelAttribute("e") Account e, ModelMap model, RedirectAttributes flushAttrs) throws Exception
	{
		User user = LoginUserHolder.getLoginUser();
		if (user == null || StringUtils.isBlank(user.getUsername())) 
		{
			throw new NullPointerException("登陆超时！");
		}
		String description = e.getRankName();//增加积分原因（该字段暂使用）
		Integer addScore = 0;
		if(!StringUtils.isBlank(RequestHolder.getRequest().getParameter("addScore")))
				addScore = Integer.valueOf(RequestHolder.getRequest().getParameter("addScore"));
		logger.debug("----------用户："+e.getAccount()+",增加addScore:"+addScore);
		e = getService().selectOne(e);
		logger.debug("----------用户："+e.getAccount()+",初始积分score:"+e.getScore());
		AccountPointsLog accLog = new AccountPointsLog();
		e.setScore(e.getScore() + addScore);
		logger.debug("----------用户："+e.getAccount()+",新增后积分score:"+e.getScore());
		try {
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("type", 2);
			map.put("id", e.getId());
			map.put("rank", addScore);
			int a = accountFinanceService.updateAccountScore(map);
			String message = "操作失败！";
			if(a>0){
				//账户积分日志记录
				accLog.setId("0");
				accLog.setUserId(e.getId());
//				accLog.setScore(e.getScore());
//				accLog.setPointCount(addScore);
				accLog.setRankCount(addScore);
				accLog.setIsOrder(0);
				accLog.setDescription(description);
//				accLog.setRemark("y");
				accLog.setRemarkUserId(user.getId());
				accLog.setRemarkUserName(user.getUsername());
				pointsManageService.insert(accLog);
				message = "操作成功！";
			}
			flushAttrs.addFlashAttribute("message", message);
			
		} catch (Exception e2) {
			logger.error(e2.getMessage(),e);
			flushAttrs.addFlashAttribute("message", "系统异常，操作失败！");
		}
		return "redirect:selectList";
	}
	
}
