package com.soloyogame.anitoys.center.web.controller.manage.popularityAdvert;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.soloyogame.anitoys.center.web.controller.BaseController;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.PopularityAdvert;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.PopularityAdvertService;


/**
 * @author 索罗游
 * 人气商品广告位
 */
@Controller
@RequestMapping("/manage/popularityAdver/")
public class PopularityAdvertAction extends BaseController<PopularityAdvert> 
{
	private static final long serialVersionUID = 1L;
	private static final String page_toList = "/manage/popularityAdvertPlat/popularityAdvertPlatList";
	private static final String page_toEdit = "/manage/popularityAdvertPlat/popularityAdvertPlatEdit";
	private static final String page_toAdd = "/manage/popularityAdvertPlat/popularityAdvertPlatEdit";
	private PopularityAdvertAction() {
		super.page_toList = page_toList;
		super.page_toAdd = page_toAdd;
		super.page_toEdit = page_toEdit;
	}
	@Autowired
	private PopularityAdvertService popularityAdvertService;

	@Override
	public Services<PopularityAdvert> getService() {
		return popularityAdvertService;
	}

	@Override
	public void insertAfter(PopularityAdvert e) {
		e.clear();
	}

	@Override
	protected void selectListAfter(PagerModel pager) {
		pager.setPagerUrl("selectList");
	}

	
	 /**
     * app平台推荐图的更新数据的方法，子类可以通过重写此方法实现个性化的需求。
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "updateApps", method = RequestMethod.POST)
    public String updateApps(HttpServletRequest request, @ModelAttribute("e") PopularityAdvert e, RedirectAttributes flushAttrs) throws Exception 
    {
    	getService().update(e);
        insertAfter(e);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
    /**
     * app轮播图的插入数据方法，子类可以通过重写此方法实现个性化的需求。
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "insertApp",method = RequestMethod.POST)
    public String insertApp(HttpServletRequest request, @ModelAttribute("e") PopularityAdvert e, RedirectAttributes flushAttrs) throws Exception 
    {
        getService().insert(e);
        insertAfter(e);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
   
    /**
     * 公共的分页方法
     * @return
     * @throws Exception
     */
    @RequestMapping("selectList")
    public String selectList(HttpServletRequest request, @ModelAttribute("e") PopularityAdvert e) throws Exception 
    {
        /**
         * 由于prepare方法不具备一致性，加此代码解决init=y查询的时候条件不被清除干净的BUG
         */
        this.initPageSelect();

        setParamWhenInitQuery(e);

        int offset = 0;//分页偏移量
        if (request.getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(request.getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        e.setOffset(offset);
        PagerModel pager = getService().selectPageList(e);
        if (pager == null) 
        {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)/ pager.getPageSize());

        selectListAfter(pager);
        request.setAttribute("pager", pager);
        return page_toList;
    }
	
    /**
	 * 同步缓存
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("syncCache")
	public String syncCache(HttpServletRequest request, PopularityAdvert img) throws Exception{
		return super.selectList(request, img);
	}
}
