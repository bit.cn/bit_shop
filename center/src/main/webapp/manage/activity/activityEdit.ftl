<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="商品促销">
<style>
	.dfsfdsfsf label{
		display: inline;
	}
</style>
<#if (!type??)>
<div class="alert alert-info">
	提示：对【商品促销】的添加/修改不会立即生效，需要到系统管理--缓存管理页面点击【加载活动+活动商品列表】按钮，才能生效。
</div>
</#if>
<form action="${basepath}/manage/activity" theme="simple" id="form" class="form-horizontal">

	<table class="table table-bordered">
		<tr style="display: none;">
			<td><input type="hidden"  value='${imageList!""}' name="imageByProId" style="width:1500px"  id="imageByProId"/></td>
		</tr>
		<tr>
			<td colspan="2" style="background-color: #dff0d8;text-align: center;">
				<strong>活动编辑</strong>
			</td>
		</tr>
		<tr style="display: none;">
			<td>id</td>
			<td><input  value="${e.id!""}" name="id" label="id" id="id"/></td>
		</tr>
		<tr>
			<td style="text-align: right;width: 200px;">活动名称</td>
			<td style="text-align: left;">
				<input type="text" value="${e.name!""}" name="name" id="name" data-rule="活动名称:required;name;length[1~45];"/>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;">活动开始时间</td>
			<td style="text-align: left;">
				<input id="startDate" style="height:30px" class="Wdate search-query " type="text" name="startDate"
				value="${e.startDate!""}" data-rule="活动开始时间:required;startDate;"
				onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'2020-10-01\'}'})"/>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;">活动结束时间</td>
			<td style="text-align: left;">
				<input id="endDate"  style="height:30px" class="Wdate search-query " type="text" name="endDate"
				value="${e.endDate!""}" data-rule="活动结束时间:required;endDate;"
				onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'2020-10-01'})"/>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;">状态</td>
			<td style="text-align: left;">
				<#assign map = {'n':'禁用','y':'启用'}>
                <select id="status" name="status"  class="input-small" data-rule="状态:required;status;">
					<#list map?keys as key>
                        <option value="${key}" <#if e.status?? && e.status==key>selected="selected" </#if>>${map[key]}</option>
					</#list>
                </select>
			</td>
		</tr>
		
		<tr>
			<td style="text-align: right;">活动关联商品</td>
			<td style="text-align: left;">
				<input type="text" readonly="true" value="${e.productID!""}" id="productID" name="productID"  style="width:80%;" />
				<a href="#myModal"  role="button" data-toggle="modal"  class="btn btn-success" onclick="loadPro();" >
						添加
					</a>
			</td>
		</tr>
		
		<tr>
			<td style="text-align: right;">活动类型</td>
			<td style="text-align: left;">

				<#assign map = {'c':'促销活动','j':'积分兑换','t':'团购活动'}>
                <select id="activityType" name="activityType"  class="input-small" data-rule="活动类型:required;activityType;">
					<#list map?keys as key>
                        <option value="${key}" <#if e.activityType?? && e.activityType==key>selected="selected" </#if>>${map[key]}</option>
					</#list>
                </select>
				<p>
				<div id="discountDiv" style="display: none;">
					<p>优惠方式：
						<#assign map = {'-':'-','r':'减免','d':'折扣','s':'双倍积分'}>
                        <select id="discountType" name="discountType"  class="input-small" data-rule="优惠方式:required;discountType;">
							<#list map?keys as key>
                                <option value="${key}" <#if e.discountType?? && e.discountType==key>selected="selected" </#if>>${map[key]}</option>
							</#list>
                        </select>
					<p>折扣/减价：<input type="text" value="${e.discount!""}" id="discount" name="discount"  class="input-small" data-rule="折扣/减价:required;discount;"/></p>
				</div>
				
				<div id="exchangeScoreDiv" style="display: none;">
					兑换积分：<input type="text" value="${e.exchangeScore!""}" id="exchangeScore" name="exchangeScore"  class="input-small" />
				</div>
				
				<div id="minGroupCountDiv" style="display: none;">
					<p>最低团购人数：<input type="text" value="${e.minGroupCount!""}" id="minGroupCount" name="minGroupCount"  class="input-small" /></p>
					<p>团购价：<input type="text" value="${e.tuanPrice!""}" id="tuanPrice" name="tuanPrice"  class="input-small" /></p>
				</div>
			</td>
		</tr>
<!-- 		<tr id="discountTypeTr" style="display: none;"> -->
<!-- 			<td style="text-align: right;">优惠方式</td> -->
<!-- 			<td style="text-align: left;"> -->
<#--<%-- 				<s:select list="#{'':'','r':'减免','d':'折扣','s':'双倍积分'}" id="discountType" name="discountType"  listKey="key" listValue="value"   --%>-->
<#--<%-- 				data-rule="优惠方式:required;discountType;"/> --%>-->
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr id="discountTr" style="display: none;"> -->
<!-- 			<td style="text-align: right;">discount</td> -->
<!-- 			<td style="text-align: left;"> -->
<#--<%-- 				<s:textfield id="discount" name="discount"  cssClass="input-small" /> --%>-->
<!-- 			</td> -->
<!-- 		</tr> -->
		<tr>
			<td style="text-align: right;">最大购买数量</td>
			<td style="text-align: left;">
                <input type="text" value="${e.maxSellCount!""}" id="maxSellCount" name="maxSellCount"  class="input-small"  data-rule="最大购买数量:required;integer;maxSellCount;"/>
				<br>
				(0：表示不限制)
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td style="text-align: left;">
				<#if (e.id??)>
                    <button method="update" class="btn btn-success" style="width:180px;">
                        <i class="icon-ok icon-white" ></i> 保存
                    </button>
				<#else>
                    <button method="insert" class="btn btn-success"  style="width:180px;">
                        <i class="icon-ok icon-white"></i> &nbsp;&nbsp;新增&nbsp;&nbsp;
                    </button>
				</#if>
			</td>
		</tr>
	</table>
</form>
<!-- 模态框（Modal） 商品列表-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
   <div class="modal-dialog"  style="width: 1480px;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">
             	商品列表
            </h4>
         </div>
         <div class="modal-body" style="width: 1450px;">
        	 <form action="" method="post" theme="simple" id="modalform" name="modalform">
            	<iframe id="iframeSon" name="iframeSon" src="${basepath}/manage/activity/toActivityCommodity" style="width: 700px;height:600px;">
            	</iframe>
            	<div  style="float:right;overflow:auto;width: 650px;height:600px;margin-left:20px; border:1px solid #000">
	            	<table class="table table-bordered table-hover"  id="tab1">
		            		<tr style="background-color: #dff0d8">
								<th width="100" style="display:none;">商品Id</th>
								<th width="100">商品名称</th>
								<th width="100">商品所属店铺名称</th>
								<th style="width: 50px;">操作</th>
								<th width="100"  style="display:none;">附图片</th>
							</tr>
	            	</table>
            	</div>
             </form>
         </div>
         <div class="modal-footer" style="margin-right:120px">
            <button type="button" class="btn btn-default"
               data-dismiss="modal" style="width:100px;">关闭
            </button>
            <button type="button" class="btn btn-primary" style="width:100px;" onClick="confirm();">
             	  确定
            </button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>

<!-- 模态框（Modal） 附图片列表-->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" 
   aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
   <div class="modal-dialog" style="width: 860px;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" 
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">
               	附图片
            </h4>
         </div>
         <div class="modal-body">
        		<iframe id="iframeProImages" name="iframeProImages" src="${basepath}/manage/activity/toActivityProImages" style="width: 500px;height:600px;">
            	</iframe>
            	<div  style="float:right;overflow:auto;width: 300px;height:600px;margin-left:20px; border:1px solid #000">
	            	<div style="color:#31708F;background-color: #D9EDF7;border-color: #BCE8F1;padding: 15px;margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;">已选择的附图片</div>
	            	<table  class="table table-bordered "id="tabImages1">
		            		
	            	</table>
            	</div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn btn-primary" style="width:100px;" onClick="imagesSave();">
               确定
            </button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>

<script type="text/javascript">
	//当前选择行的Index
	var selectRows =0;
	//初始化标识
	var count =0;
	//保存附图片
	function imagesSave(){
		var rows=tabImages1.rows;
		var ss = "";
		for(var i=0;i<rows.length;i++) 
		{
			ss += rows[i].cells[0].innerHTML +",";
		}
		//把已选择的附图片用,分割存到对应商品的table中;
		tab1.rows[selectRows].cells[4].innerHTML = ss.substr(0,ss.length-1);
		$('#myModal1').modal('hide')
	 }

	//保存商品
	function confirm(){
		//每个商品相对应商品附图片的List
		var objList=new Array();
		var rows=tab1.rows;
		if(rows.length <= 1){
			alert("请先选择商品！");
			return false;
		}
		var ss = "";
		for(var i=1;i<rows.length;i++) 
		{
			//商品对象
			var obj=new Object();
			//商品ID
			obj["productID"]=rows[i].cells[0].innerHTML
			//商品已选附图片
			obj["images"]=rows[i].cells[4].innerHTML
			objList.push(obj);
			ss += rows[i].cells[0].innerHTML +"|";
		}
		//把已选择的所有商品用|拼接回显到修改页面
		document.getElementById("productID").value = ss.substr(0,ss.length-1);
		//把一个活动对应的商品,且商品对应的所有附图片，用List保存
		document.getElementById("imageByProId").value = JSON.stringify(objList);
		$('#myModal').modal('hide')
	   
	}

function loadPro(){	
	//修改时初始化已选择的商品	
	$.ajax({	
 		 type: "POst",
 		 url: "${basepath}/manage/activity/toProductListByIds",
 		 data: {ids:$("#productID").val()},
 		 dataType:"JSON",
 		 success: function(data){
 			 var rows=tab1.rows;
 		 	 if(rows.length<=1){
 		 	 	//循环所有商品进行页面显示
 		 		 $.each(data, function(commentIndex, comment){
		 		 	var newTr = tab1.insertRow();
					var newTd0 = newTr.insertCell();
					var newTd1 = newTr.insertCell();
					var newTd2 = newTr.insertCell();
					var newTd3 = newTr.insertCell();
					var newTd4 = newTr.insertCell();
					newTd0.style.display = "none";
					newTd4.style.display = "none";
					//设置列内容和属性
					newTd0.innerHTML = comment["id"];
					newTd1.innerHTML = comment["name"];
					if(typeof(comment["businessName"]) != "undefined"){
						newTd2.innerHTML = comment["businessName"];
					}
					newTd3.innerHTML = '<input type="button" onclick="deletePro(this);" class="btn btn-success" value="移除"><input type="button"  data-toggle="modal" data-target="#myModal1" class="btn btn-success" value="添加附图片"  onclick="openProImages(this);"  style="margin-left:20px">';
 		 	 	
 		 		 })
 		 	 }
 		 	 //如果是第一次初始化时并加载对应商品的附图片
 		 	 if(count < 1){
				var data = eval($("#imageByProId").val());
				for(var jj =0;jj < data.length;jj++){
					 var rows=tab1.rows;
					 for(var k=1;k<rows.length;k++) {
					 	if(data[jj].productID == rows[k].cells[0].innerHTML){
					 		tab1.rows[k].cells[4].innerHTML = data[jj].images;
					 	}
					 }
				}
				count++;
			}
 		 }
 	})
	//alert($("#productID").val());
}

//添加一个附图片
function addImages(imagesUrl,number){
	var rows=tabImages1.rows;
	for(var i=0;i<rows.length;i++) 
	{
		if(imagesUrl==rows[i].cells[0].innerHTML){
			alert('您已添加该图片！');
			return;
		}
	}
	var newTr=tabImages1.insertRow();
	var newTd0 = newTr.insertCell();
	var newTd1 = newTr.insertCell();
	newTd0.style.display = "none";
	newTd0.innerHTML = imagesUrl;
	newTd1.innerHTML='<a onclick="deleteImages(this);" ><img style="height:300px;width:260px;" alt="" title="移除该商品" src="${systemSetting().imageRootPath}'+imagesUrl+'"}" onerror="nofind()"/></a>'
}

//添加一个商品
function addPro(id,name,businessName){
	var rows=tab1.rows;
	for(var i=1;i<rows.length;i++) 
	{
		if(id==rows[i].cells[0].innerHTML){
			alert('您已添加该商品！');
			return;
		}
	} 
	//添加一行
	var newTr = tab1.insertRow();
	//添加两列
	var newTd0 = newTr.insertCell();
	var newTd1 = newTr.insertCell();
	var newTd2 = newTr.insertCell();
	var newTd3 = newTr.insertCell();
	var newTd4 = newTr.insertCell();
	newTd0.style.display = "none";
	newTd4.style.display = "none";
	//设置列内容和属性
	newTd0.innerHTML = id;
	newTd1.innerHTML = name;
	newTd2.innerHTML = businessName;
	newTd3.innerHTML = '<input type="button" onclick="deletePro(this);" class="btn btn-success" value="移除"><input type="button"  data-toggle="modal" data-target="#myModal1" class="btn btn-success" value="添加附图片"  onclick="openProImages(this);"  style="margin-left:20px">';
	newTd4.innerHTML = images;
}

//移除一个商品
function deletePro(r){
	tab1.deleteRow(r.parentNode.parentNode.rowIndex);
}

//移除一张附图片
function deleteImages(r){
	tabImages1.deleteRow(r.parentNode.parentNode.rowIndex);
}

//添加附图片
function openProImages(r){	
	var rows=tabImages1.rows;
	var rowNum=rows.length;
	//清空当前窗口保存的附图片
	for(var i=0;i<rowNum;) 
	{
		 tabImages1.deleteRow(i);
		 rowNum--;
	}
	//得到当前所选商品的附图片
	var images= new Array();
	images = tab1.rows[r.parentNode.parentNode.rowIndex].cells[4].innerHTML.split(',');
	//循环当前所选商品的附图片集合进行附图片已选页面的初始化
	for (image=0;image<images.length ;image++ ) 
	{ 
		if(images[image] != ""){
			var newTr=tabImages1.insertRow();
			var newTd0 = newTr.insertCell();
			var newTd1 = newTr.insertCell();
			newTd0.style.display = "none";
			newTd0.innerHTML =images[image];
			newTd1.innerHTML='<a onclick="deleteImages(this);" ><img style="height:300px;width:260px;" alt="" title="移除该商品" src="${systemSetting().imageRootPath}'+images[image]+'"}" onerror="nofind()"/></a>'
		}
	}
	//记录当前点击的商品Index
	selectRows = r.parentNode.parentNode.rowIndex;
	var rows = tab1.rows
	var id = rows[r.parentNode.parentNode.rowIndex].cells[0].innerHTML;
	var myiframe = document.getElementById("iframeProImages");
	myiframe.src = "${basepath}/manage/activity/toActivityProImages?id="+id;
	$.ajax({
 		 type: "POst",
 		 url: "${basepath}/manage/activity/toActivitySelectProImages",
 		 data: {activityId:$("#id").val(),proId:id},
 		 dataType:"JSON",
 		 success: function(data){
 			 var rows=tabImages1.rows;
 		 	 if(rows.length<1){
 		 	 	for(var j =0;j < data.length;j++){
		 		 	var newTr=tabImages1.insertRow();
					var newTd0 = newTr.insertCell();
					var newTd1 = newTr.insertCell();
					newTd0.style.display = "none";
					newTd0.innerHTML = data[j].images;
					newTd1.innerHTML='<a onclick="deleteImages(this);" ><img style="height:300px;width:260px;" alt="" title="移除该商品" src="${systemSetting().imageRootPath}'+data[j].images+'"}" onerror="nofind()"/></a>'
 		 	 	}
 		 	}
 		 }
 	})
}

$(function(){
	ceqweqeqwe();
	$("#activityType").change(function(){
		ceqweqeqwe();
	});
	
	var checkboxValue = $("#__multiselect_form_e_accountRange").val();
	//console.log("checkboxValue="+checkboxValue);
	if(checkboxValue){
		var arr = checkboxValue.split(",");
		for(var i=0;i<arr.length;i++){
			$("#checkboxArr").find("input[type=checkbox]").each(function(){
				//console.log("value="+$(this).attr("value")+",attr[i]="+arr[i]);
				if($(this).attr("value")==$.trim(arr[i])){
					$(this).attr("checked",true);
				}
			});
		}
	}
});
function ceqweqeqwe(){
	var v = $("#activityType").val();
	//console.log("v="+v);
	if(v=="c"){
		$("#discountDiv").show();
		$("#exchangeScoreDiv").hide();
		$("#minGroupCountDiv").hide();
	}else if(v=="j"){
		$("#discountDiv").hide();
		$("#exchangeScoreDiv").show();
		$("#minGroupCountDiv").hide();
	}else if(v=="t"){
		$("#discountDiv").hide();
		$("#exchangeScoreDiv").hide();
		$("#minGroupCountDiv").show();
	}else{
		$("#discountDiv").hide();
		$("#exchangeScoreDiv").hide();
		$("#minGroupCountDiv").hide();
	}
}
</script>
</@page.pageBase>