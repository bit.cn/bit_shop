
<#assign pg = JspTaglibs["/WEB-INF/jsp/pager-taglib.tld"]/>
 <link rel="shortcut icon" type="image/x-icon" href="${systemSetting().shortcuticon}">
 <link rel="stylesheet" href="${staticpath}/bootstrap3.3.4/css/bootstrap.min.css"  type="text/css">
 <link rel="stylesheet" href="${staticpath}/jquery-ui-1.11.2/jquery-ui.css">
 <link rel="stylesheet" href="${staticpath}/validator-0.7.0/jquery.validator.css" />
<script type="text/javascript" src="${staticpath}/js/jquery-1.9.1.min.js"></script>
<style type="text/css">
.product-name {
	display: inline-block;
	width: 250px;
	overflow: hidden; /*注意不要写在最后了*/
	white-space: nowrap;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
}
.pageLink {
	border: 1px solid #dddddd;
	padding: 4px 12px;
	text-decoration: none;
}

.selectPageLink {
	border: 1px solid #0088cc;
	padding: 4px 12px;
	color: #0088cc;
	background-color: #dddddd;
	text-decoration: none;
}
</style>

<form action="${basepath}/manage/activity"  method="post" theme="simple"
	<div >
		<tr>
			<td>
			<div class="alert alert-info">全部附图片</div>
				<table class="table table-bordered table-hover" >
					<#if list??>
					<#list 0..((list?size)-1)/2 as item>
						<tr style="">
							<td><a  onclick='addImages("${list[item_index*2]!""}","${item_index*2}")' >
								<img style="height:300px;width:210px;" alt="" title= "添加该商品" src="${systemSetting().imageRootPath}${list[item_index*2]!""}" onerror="nofind()"/>
							</a><td>
							<#if (item_index+1) *2 <= list?size>
							<td><a  onclick="addImages('${list[item_index*2+1]!""}','${item_index*2+1}')" >
								<img style="height:300px;width:210px;" alt="" title= "添加该商品" src="${systemSetting().imageRootPath}${list[item_index*2+1]!""}" onerror="nofind()"/>
							</a></td>
							</#if>
						</tr>
					</#list>
					</#if>
				</table>
			</td>
		</tr>
	</div>
</from>


<script>
function addImages(imagesUrl,number){
	parent.addImages(imagesUrl,number);
}

$(function(){
	
	$("#firstCheckbox").on("click",function(){
		console.log("check="+$(this).prop("checked"));
		if($(this).prop("checked")){
			$("input[type=checkbox]").prop("checked",true);
		}else{
			$("input[type=checkbox]").prop("checked", false);
		}
	});
});
</script>