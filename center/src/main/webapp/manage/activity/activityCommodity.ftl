<#import "/manage/tpl/pageBase.ftl" as page>
<#assign pg = JspTaglibs["/WEB-INF/jsp/pager-taglib.tld"]/>
 <link rel="shortcut icon" type="image/x-icon" href="${systemSetting().shortcuticon}">
 <link rel="stylesheet" href="${staticpath}/bootstrap3.3.4/css/bootstrap.min.css"  type="text/css">
 <link rel="stylesheet" href="${staticpath}/jquery-ui-1.11.2/jquery-ui.css">
 <link rel="stylesheet" href="${staticpath}/validator-0.7.0/jquery.validator.css" />
<script type="text/javascript" src="${staticpath}/js/jquery-1.9.1.min.js"></script>
<style type="text/css">
.product-name {
	display: inline-block;
	width: 250px;
	overflow: hidden; /*注意不要写在最后了*/
	white-space: nowrap;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
}
.pageLink {
	border: 1px solid #dddddd;
	padding: 4px 12px;
	text-decoration: none;
}

.selectPageLink {
	border: 1px solid #0088cc;
	padding: 4px 12px;
	color: #0088cc;
	background-color: #dddddd;
	text-decoration: none;
}
</style>
<form action="${basepath}/manage/activity"  method="post" theme="simple"
	<div >
		<table class="table-bordered table-hover" width="100%" height = "100px" style="margin-top: 0px;">
			<tr>
				<td style="text-align: right;padding-right:10px">商品编号</td>
				<td style="text-align: left;padding-left:20px"><input id="proid" type="text"  value="${id!""}" name="id"  class="search-query input-small"
							id="id" /></td>
			</tr>
			<tr>
				<td style="text-align: right;padding-right:10px">商品名称</td>
				<td style="text-align: left;padding-left:20px" ><input id="proname" type="text" value="${name!""}" name="name"  class="input-small"
			</tr>
			<tr>
				<td ></td>
				<td style="padding-left:20px"><a style="width:100px;"  class="btn btn-primary" onclick="toActivityCommodity()">
								<i class="icon-search icon-white"></i> 查询
				</a></td>
		</tr>
		</table>
		<tr>
			<td>
				<table class="table table-bordered table-hover">
					
					<tr style="background-color: #dff0d8">
						<!--<th width="100"><input type="checkbox" id="firstCheckbox" /></th>-->
						<th width="200">商品名称</th>
						<th width="200">商品所属店铺名称</th>
						<th style="width: 50px;">操作</th>
					</tr>
					<#list pager.list as item>
						<tr>
							<!--<td><input type="checkbox"  name="proIds" value="${item.id!""}" /></td>-->
							<td nowrap="nowrap">&nbsp;${item.name!""}</td>
							<td nowrap="nowrap">&nbsp;${item.businessName!""}</td>
							<td nowrap="nowrap">
								<input type="button" onclick="add('${item.id}','${item.name}','${item.businessName!""}')" class="btn btn-success" value="添加">
							</td>
						</tr>
					</#list>
					<tr>
						<td colspan="16" style="text-align: center;">
						<#include "/manage/system/pager.ftl"/></td>
					</tr>
				</table>
			</td>
		</tr>
	</div>
</from>


<script>
function toActivityCommodity(){
	location.reload("${basepath}/manage/activity/toActivityCommodity?id="+$('#proid').val()+"&name="+$('#proname').val());
}

function add(id,name,businessName){
	parent.addPro(id,name,businessName);
}

$(function(){
	
	$("#firstCheckbox").on("click",function(){
		console.log("check="+$(this).prop("checked"));
		if($(this).prop("checked")){
			$("input[type=checkbox]").prop("checked",true);
		}else{
			$("input[type=checkbox]").prop("checked", false);
		}
	});
});
</script>