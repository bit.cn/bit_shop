<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="积分管理 / 积分详情">
	<form action="${basepath}/manage/pointsManage" method="post" theme="simple">
				<table class="table table-bordered">
					<tr>
						<td colspan="16">
							<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
								<#import "pointsPager.ftl" as pag>
								<@pag.pga userId="${userId?if_exists }"/>
							</div>
						</td>
					</tr>
				</table>
				<table class="table table-bordered table-hover">
					<tr style="background-color: #dff0d8">
						<th style="display: none;">积分日志编号</th>
						<th nowrap="nowrap">会员账号</th>
						<#--<th nowrap="nowrap">会员积分</th>-->
						<th nowrap="nowrap">积分变动</th>
						<th nowrap="nowrap">日期</th>
						<th nowrap="nowrap">描述</th>
					</tr>
					<#list pager.list as item>
						<tr>
							<td style="display: none;">&nbsp;${item.id!""}</td>
							<td>&nbsp;${item.userId!""}</td>
							<#--<td>&nbsp;${item.score!""}</td>-->
							<td nowrap="nowrap">&nbsp;${item.rankCount!""}</td>
							<td nowrap="nowrap">&nbsp;
								<#if item.addTime??>
									${item.addTime}
								<#else>
								 	&nbsp;
								</#if>
							</td>
							<td nowrap="nowrap">&nbsp;${item.description!""}</td>
						</tr>
					</#list>
					<tr>
						<td colspan="17" style="text-align: center;">
						<#import "pointsPager.ftl" as pag>
						<@pag.pga userId="${userId?if_exists }"/>
						</td>
					</tr>
				</table>
	</form>
</@page.pageBase>