<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="积分管理 / 增加积分">
<form action="${basepath}/manage/pointsManage" method="post" theme="simple" id="form" >
	<table class="table table-bordered">
		<tr>
			<td colspan="2" style="background-color: #dff0d8;text-align: center;">
				<strong>会员积分编辑</strong>
			</td>
		</tr>
		<tr style="display: none;">
			<td>id</td>
			<td><input type="hidden" value="${e.id?if_exists}" name="id" id="id"/></td>
		</tr>
		<tr>
			<td style="text-align: right;width:400px;">会员账号</td>
			<td style="text-align: left;"><input type="text" value="${e.account?if_exists}" id="account" disabled="disabled"/></td>
		</tr>
		<tr>
			<td style="text-align: right;">会员昵称</td>
			<td style="text-align: left;"><input type="text" value="${e.nickname?if_exists}" disabled="disabled"
					id="minScore" /></td>
		</tr>
		<tr>
			<td style="text-align: right;">当前积分</td>
			<td style="text-align: left;"><input type="text" value="${e.score?if_exists}" disabled="disabled"
					id="maxScore" /></td>
		</tr>
		<tr>
			<td style="text-align: right;">修改积分</td>
			<td style="text-align: left;"><input type="text" value="" name="addScore" data-rule="增加积分:required;integer;maxScore;length[1~10];"
					id="maxScore" /></td>
		</tr>
		<tr>
			<td style="text-align: right;">原因</td>   
			<td style="text-align: left;"><textarea style="width:400px;height:100px;" name="rankName" id="description" data-rule="原因:string;maxScore;length[1~200];"></textarea></td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2">
                    <button method="addPoinst" class="btn btn-success">
                        <i class="icon-ok icon-white"></i> 保存
                    </button>
			</td>
		</tr>
	</table>
</form>

</@page.pageBase>