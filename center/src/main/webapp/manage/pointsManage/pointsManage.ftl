<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="积分管理">
	<form action="${basepath}/manage/pointsManage" method="post" theme="simple">
				<table class="table table-bordered">
					<tr>
						<td style="text-align: right;vertical-align:middle;width:100px;" nowrap="nowrap">会员账号</td>
						<td style="text-align: left;width:200px;">
                            <input type="text" value="${e.account!""}" name="account" class="input-medium" />
						</td>
						<td colspan="16">
							<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
								<i class="icon-search icon-white"></i> 查询
							</button>
								
							<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
								<#include "/manage/system/pager.ftl"/>
							</div>
						</td>
					</tr>
				</table>
				
				<table class="table table-bordered table-hover">
					<tr style="background-color: #dff0d8">
						<th style="display: none;">会员编号</th>
						<th nowrap="nowrap">会员账号</th>
						<th nowrap="nowrap">会员昵称</th>
						<th nowrap="nowrap">会员真实姓名</th>
						<th nowrap="nowrap">会员等级</th>
						<th nowrap="nowrap">积分</th>
						<th nowrap="nowrap">操作</th>
					</tr>
					<#list pager.list as item>
						<tr>
							<td style="display: none;">&nbsp;${item.account!""}</td>
							<td>
								${item.account!""}
							</td>
							<td>&nbsp;
								${item.nickname!""}
							</td>
							<td nowrap="nowrap">&nbsp;${item.trueName!""}</td>
							<td nowrap="nowrap">&nbsp;${item.rankName!""}</td>
							<td>&nbsp;${item.score!""}</td>
							<td nowrap="nowrap">
                                	【<a href="toPointsAdd?id=${item.id!""}">修改积分</a>】&nbsp;&nbsp;
                                	【<a href="toPointsDetail?userId=${item.id!""}">积分详情</a>】
							</td>
						</tr>
					</#list>
					<tr>
						<td colspan="17" style="text-align: center;"><#include "/manage/system/pager.ftl"/></td>
					</tr>
				</table>
	</form>
</@page.pageBase>