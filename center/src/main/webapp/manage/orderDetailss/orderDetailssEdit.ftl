<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="订单管理">
<style>
.simpleOrderReport{
	font-weight: 700;font-size: 16px;color: #f50;
}
</style>
<form action="${basepath}/manage/orderDetailss" method="post" theme="simple">
	<input type="hidden" value="${e.id!""}" name="id"/>
	
	<div id="tabs">
		<ul>
			<li><a href="#tabs-1">购买基本信息</a></li>
			<li><a href="#tabs-2">买家物流信息</a></li>
			<li><a href="#tabs-3">订单的状态信息</a></li>
			<li><a href="#tabs-4">订单支付信息</a></li>
		</ul>
		<div id="tabs-1">
			<table class="table table-bordered">
				<tr style="background-color: #dff0d8">
					<th>订单编号</th>
					<th>商家ID</th>
					<th>商家名称</th>
					<th>商品名</th>
					<th>商品单价</th>
					<th>购买数量</th>
				</tr>
				<tr>
					<td>&nbsp;${e.orderCode!""}</td>
					<td>&nbsp;${e.businessId!""}</td>
					<td>&nbsp;${e.businessName!""}</td>
					<td>&nbsp;
					<#if e.productName??>
								<#if e.productName?length gte 50>
									${e.productName?substring(0,50)}...
								<#else>
									${e.productName!""}
								</#if>
						</#if>
					</td>
					<td>&nbsp;${e.price!""}</td>
					<td>&nbsp;${e.number!""}</td>
				</tr>
				
			</table>
			
		</div>
		
		<div id="tabs-2">
			<table class="table table-bordered">
				<tr style="background-color: #dff0d8">
					<th>购买人ID</th>
					<th>收货地址</th>
					<th>联系方式</th>
					<th>收货人姓名</th>
					<th>快递方式</th>
					<th>快递单号</th>
				</tr>
				
				<tr>
					<td>&nbsp;${e.account!""}</td>
					<td>&nbsp;${e.shipaddress!""}</td>
					<td>&nbsp;${e.phone!""}</td>
					<td>&nbsp;${e.shipname!""}</td>
					<td>&nbsp;${e.carry!""}</td>
					<td>&nbsp;${e.areaCode!""}</td>
				</tr>
			</table>
		</div>
		
		<div id="tabs-3">
			<table class="table table-bordered">
				<tr style="background-color: #dff0d8">
					<th>下单时间</th>
					<th>预订时间</th>
					<th>补款时间</th>
					<th>完成付款时间</th>
					<th>是否售后</th>
					<th>订单状态</th>
					
				</tr>
				
				<tr>
					<td>&nbsp;${e.createdate!""}</td>
					<td>&nbsp;${e.reservrTime!""}</td>
					<td>&nbsp;${e.replenishmentTime!""}</td>
					<td>&nbsp;${e.finishPayTime!""}</td>
					<td>&nbsp;
					<#if e.isSaleorder??&&e.isSaleorder==1> 售后订单
					<#elseif e.isSaleorder??&&e.isSaleorder==0> 非售后订单
					<#else> ${e.isSaleorder!""}
					</#if>
					</td>
					
					<td>&nbsp; 
					<#if e.status??&&e.status=="init"> 未审核
					<#elseif e.status??&&e.status=="pass"> 已审核
					<#elseif e.status??&&e.status=="send"> 已发货
					<#elseif e.status??&&e.status=="sign"> 已签收
					<#elseif e.status??&&e.status=="cancel"> 已取消
					<#elseif e.status??&&e.status=="file"> 已归档
					<#elseif e.status??&&e.status=="finish"> 交易完成
					<#else> ${e.status!""}
					</#if>
					
					</td>
				</tr>
			</table>
		</div>
		
		<div id="tabs-4">
			<table class="table table-bordered">
				<tr style="background-color: #dff0d8">
					<th>订单总额</th>
					<th>运费金额</th>
					<th>买家积分使用</th>
					<th>买家优惠券抵扣</th>
					<th>买家平台抵扣券的抵扣</th>
					<th>买家站内余额使用</th>
					<th>单品总额</th>
					
				</tr>
				
				<tr>
					<td>&nbsp;${e.amount!""}</td>
					<td>&nbsp;${e.fee!""}</td>
					<td>&nbsp;${e.amountExchangeScore!""}</td>
					<td>&nbsp;${e.coupons!""}</td>
					<td>&nbsp;${e.deductible!""}</td>
					<td>&nbsp;${e.balancePrice!""}</td>
					<td>&nbsp;${e.total0!""}</td>
				</tr>
			</table>
		</div>
	
	
	<div align="center">
		<a  class="btn btn-success btn-sm" href="selectList?type=Y">返回</a>
	</div>
</form>

<script>
$(function() {
	$( "#tabs" ).tabs({
		//event: "mouseover"
	});
	$("#cancelPayBtn").click(function(){
		$("#addPayDiv").slideUp();
		$("#addPayBtn").show();
		//$("#buttons").find("input[type=button]").each(function(){
			//$(this).attr("disabled","");
		//});
		return false;
	});
	$("#cancelUpdatePayMoneryBtn").click(function(){
		$("#updatePayMoneryDiv").slideUp();
		$("#updatePayMoneryBtn").show();
		return false;
	});
});
function addPayFunc(){
	$("#addPayDiv").slideDown();
	$("#addPayBtn").hide();
	//$("#buttons").find("input[type=button]").each(function(){
		//$(this).attr("disabled","disabled");
	//});
	return false;
}
function updatePayMoneryFunc(){
	$("#updatePayMoneryDiv").slideDown();
	$("#updatePayMoneryBtn").hide();
	return false;
}

</script>

</@page.pageBase>