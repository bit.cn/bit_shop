<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="订单管理">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<script type="text/javascript">
	$(function() {
		$("#treegrid").treegrid({"treeColumn":1});
		
		function c1(f) {
			$(":checkbox").each(function() {
				$(this).attr("checked", f);
			});
		}
		$("#firstCheckbox").click(function() {
			if ($(this).attr("checked")) {
				c1(true);
			} else {
				c1(false);
			}
		});
	});
	function deleteSelect() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定删除选择的记录?");
	}
	function updateInBlackList() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定将选择的记录拉入新闻黑名单吗?");
	}
</script>
	<form action="${basepath}/manage/orderDetailss" method="post" theme="simple">
		<table class="table table-bordered"  >
			<tr>
				<td style="width: 8%;vertical-align:middle;">订单编号</td>
				<td style="width: 410px;"><input type="text" value="${e.id!""}" name="id" class="search-query input-small"/></td>
				<td style="vertical-align:middle;">支付状态</td>
				<td style="width: 200px;">
					<#assign map = {'':'','n':'未支付','y':'完全支付'}>
                    <select id="paystatus" name="paystatus" class="search-query input-medium">
						<#list map?keys as key>
                            <option value="${key}" <#if e.paystatus?? && e.paystatus==key>selected="selected" </#if>>${map[key]}</option>
						</#list>
                    </select>
					</td>
				<td style="width: 8%;vertical-align:middle;">订单状态</td>
				<td >
					<#assign map = {'':'','init':'未审核','pass':'已审核','send':'已发货','sign':'已签收','cancel':'已取消','file':'已归档'}>
                    <select id="status" name="status" class="search-query input-medium">
						<#list map?keys as key>
                            <option value="${key}" <#if e.status?? && e.status==key>selected="selected" </#if>>${map[key]}</option>
						</#list>
                    </select></td>
			</tr>
			<tr>
                <td style="width: 8%;vertical-align:middle;">用户账号</td>
				<td><input type="text"  value="${e.account!""}" name="account" class="search-query input-small"/></td>
                <td style="width: 8%;vertical-align:middle;">店铺名称</td>
				<td colspan="3"><input type="text"  value="${e.businessName!""}" name="businessName" class="search-query input-small"/></td>
			</tr>
			<tr>
				<td style="width: 8%;vertical-align:middle;">时间范围</td>
				<td colspan="5"><input id="d4311" style="height:30px" class="Wdate search-query input-small" type="text" name="startDate"
					value="${e.startDate!""}"
					onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01\'}'})"/>
					~ 
					<input id="d4312" class="Wdate search-query input-small" style="height:30px" type="text" name="endDate"
					value="${e.endDate!""}"
					onFocus="WdatePicker({minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01'})"/>
				</td>
			
			
			
			</tr>
			<tr>
				<td colspan="6">
						<button method="selectLists" class="btn btn-primary" onclick="selectList(this)">
							<i class="icon-search icon-white"></i> 查询
						</button>
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
						<#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>
				
				
		<table id="treegrid" title="订单目录" class="table tree table-bordered" style="min-width:800px;min-height:250px">
			<tr style="background-color: #dff0d8">
				<th width="20" data-options="field:'id'" nowrap="nowrap"><input type="checkbox" id="firstCheckbox"/></th>
				<th width="100" data-options="field:'id'" nowrap="nowrap">订单号</th>
				<th width="100" data-options="field:'businessName'" nowrap="nowrap">商家名称</th>
				<th width="100" data-options="field:'productName'" nowrap="nowrap">商品名</th>
				<th width="100" data-options="field:'quantity'" nowrap="nowrap">购买数量</th>
				<th width="100" data-options="field:'account'" nowrap="nowrap">购买人</th>
				<th width="100" data-options="field:'createdate'" nowrap="nowrap">下单时间</th>
				<th width="100" data-options="field:'completeTime'" nowrap="nowrap">完成付款时间</th>
				<th width="100" data-options="field:'status'" nowrap="nowrap">订单状态</th>
				<th width="100" data-options="field:'amount'" nowrap="nowrap">订单总额</th>
				
				<th width="100px">操作</th>
			</tr>
			<#list pager.list as item>
				<tr class="treegrid-${item.id} ${(item.parentId=="0")?string("","treegrid-parent-"+item.parentId)}">
					<td><input type="checkbox" name="ids"
						value="${item.id!""}" /></td>
					<td>
						${item.orderCode!""}
						<#if item.lowStocks?? && item.lowStocks=="y"><font color="red">【缺货】</font></#if>
					</td>
					
					<td>${item.businessName!""}</td>
					<td>
					<#if item.productName??>
								<#if item.productName?length gte 50>
									${item.productName?substring(0,50)}...
								<#else>
									${item.productName!""}
								</#if>
						</#if>
					</td>
					<td>${item.number!""}</td>
					<td>${item.accountName!""}</td>
					<td>${item.createdate!""}</td>
					<td>${item.finishPayTime!""}</td>
					<td>
					<#if item.status??&&item.status=="init"> 未审核
					<#elseif item.status??&&item.status=="pass"> 已审核
					<#elseif item.status??&&item.status=="send"> 已发货
					<#elseif item.status??&&item.status=="sign"> 已签收
					<#elseif item.status??&&item.status=="cancel"> 已取消
					<#elseif item.status??&&item.status=="file"> 已归档
					<#elseif item.status??&&item.status=="finish"> 交易完成
					<#else> ${item.status!""}
					</#if>
						
						
					</td>
					<td>${item.amount!""}</td>
					<td><a href="toEdit?id=${item.id}">详情</a>
					
					</td>
					
					
				</tr>
			</#list>
			<tr>
				<td colspan="11" style="text-align: center;">
					<#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>




	</form>
	
	<link rel="stylesheet" type="text/css" href="${systemSetting().staticSource}/jquery-treegrid/css/jquery.treegrid.css">
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.js"></script>
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.bootstrap3.js"></script>
</@page.pageBase>