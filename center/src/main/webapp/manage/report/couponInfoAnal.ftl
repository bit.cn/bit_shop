<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="优惠信息分析">
<style type="text/css">
.product-name {
	display: inline-block;
	width: 250px;
	overflow: hidden; /*注意不要写在最后了*/
	white-space: nowrap;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
}
</style>
<form action="${basepath}/manage/report/selectDtlList" method="post" theme="simple">
		<table class="table table-bordered table-condensed">
		<tr>			
			<td style="text-align: left;vertical-align:middle;">商家名称</td>
				<td style="text-align: left;" ><input type="text"  value="${businessName!""}" name="businessName"  class="input-small"
						id="name" />
			</td>
			<td style="text-align: left;vertical-align:middle;">优惠券类型</td>
			<td style="text-align: left;">
                    <#assign map = {"1":'抵扣券',"0":'优惠券'}>
                    <select id="couponType" name="couponType" class="input-medium"/>           
                    	<#list map?keys as key>
                            <option value="${key}" selected="selected">${map[key]}</option>
                        </#list>    
                    </select>    
			</td>
			
				
			<td style="text-align: left;vertical-align:middle;">排序类型</td>
			<td style="text-align: left;">
                    <#assign map1 = {"useCount":'使用数量',"receiveCount":'领取数量',"couponCount":'发放数量'}>
                    <select id="indexType" name="indexType" class="input-medium">
                        <#list map1?keys as key>
                            <option value="${key}" selected="selected">${map1[key]}</option>
                        </#list>
                    </select>
			</td>
		</tr>
		<tr>
			<td style="text-align: left;vertical-align:middle;" nowrap="nowrap">发放日期</td>
			<td style="text-align: left;">
					<input id="d4311" class="Wdate search-query input-small" style="height:30px" type="text" name="startDate"
					value="${startDate!""}"
					onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01\'}'})"/>
					~ 
					<input id="d4312" class="Wdate search-query input-small" style="height:30px" type="text" name="endDate"
					value="${endDate!""}"
					onFocus="WdatePicker({minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01'})"/>
			</td>
			<td colspan="4">
					<button  type="submit" class="btn btn-primary">
							<i class="icon-search icon-white"></i> 查询
					</button>
			</td>
			
		</tr>
	</table>
</form>
<div>
	<div class="head_mk_pak">
	<table class="table table-bordered table-condensed table-hover">
			<tr style="background-color: #dff0d8">				
				<td>优惠券名称</td>
				<td>商家名称</td>				
				<td>发放数量</td>
				<td>领取数量</td>
				<td>使用数量</td>	
			</tr>
			<#if pager ??>
			<#if pager.list ??>
			<#list pager.list as item>
			<#if item ??>
			<tr class="aft_tr1">				
				<td class="aft_t1">
					${item.couponName!""}
				</td>
				<td class="aft_t1">
					${item.businessName!""}
				</td>
				<td class="aft_t1">
					${item.couponCount!""}
				</td>
				<td class="aft_t1">${item.receiveCount!""}</td>
				<td class="aft_t1">${item.useCount!""}</td>
			</tr>
			</#if>
			</#list>
			</#if>
			</#if>
		</table>
	</div>
	<div>
			<tr>
				<td colspan="70" style="text-align: center;">
					<#if pager??>
                    	<#include "/manage/system/pager.ftl"/></td>
                    </#if>
			</tr>	
	</div>
</div>
	
</@page.pageBase>
