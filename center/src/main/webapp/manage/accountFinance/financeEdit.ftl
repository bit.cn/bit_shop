<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="余额管理">
<form action="${basepath}/manage/accountFinance" method="post" theme="simple" id="form" >
	<table class="table table-bordered">
		<tr>
			<td colspan="2" style="background-color: #dff0d8;text-align: center;">
				<strong>会员账户余额编辑</strong>
			</td>
		</tr>
		<tr style="display: none;">
			<td>id</td>
			<td><input type="hidden" value="${e.id?if_exists}" name="id" id="id"/><input type="hidden" value="${e.amount?if_exists}" name="amount" id="amount"/>
				<input type="hidden" value="2" name="amountType" id="amountType"/>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;width:400px;vertical-align:middle;">会员账号</td>
			<td style="text-align: left;"><input type="text" value="${e.account?if_exists}" id="account" disabled="disabled"/></td>
		</tr>
		<tr>
			<td style="text-align: right;vertical-align:middle;">会员昵称</td>
			<td style="text-align: left;"><input type="text" value="${e.nickname?if_exists}" disabled="disabled"
					id="minScore" /></td>
		</tr>
		<tr>
			<td style="text-align: right;vertical-align:middle;">当前余额</td>
			<td style="text-align: left;"><input type="text" value="${e.amount?if_exists}" disabled="disabled"
					id="maxAmount" /></td>
		</tr>
		<tr>
			<td style="text-align: right;vertical-align:middle;">修改余额</td>
			<td style="text-align: left;vertical-align:middle;"><input type="text" value="" name="addAmount" data-rule="修改余额:required;double;maxAmount;length[1~10];"
					/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align: right;vertical-align:middle;">原因</td>   
			<td style="text-align: left;vertical-align:middle;"><textarea style="width:400px;height:100px;" name="rankName" id="description" data-rule="原因:string;maxAmount;length[1~200];"></textarea></td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="2">
                    <button method="addFinance" class="btn btn-success">
                        <i class="icon-ok icon-white"></i> 保存
                    </button>
                    &nbsp;<a href="selectList?type=Y">返回</a>
			</td>
		</tr>
	</table>
</form>
</@page.pageBase>