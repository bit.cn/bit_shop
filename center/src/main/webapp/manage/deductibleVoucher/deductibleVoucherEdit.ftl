<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="折扣券管理">
<style>
#insertOrUpdateMsg{
border: 0px solid #aaa;margin: 0px;position: fixed;top: 0;width: 100%;
background-color: #d1d1d1;display: none;height: 30px;z-index: 9999;font-size: 18px;color: red;
}
.btnCCC{
	background-image: url("../img/glyphicons-halflings-white.png");
	background-position: -288px 0;
}
</style>
<script>
$(function(){
	<#if e.id??>
	var id = "${e.id}";
	$("#btnStatic").click(function(){
		$.post("${basepath}/freemarker/create?method=staticNewsByID&id="+id, null ,function(response){
			alert(response == "success" ? "操作成功！" : "操作失败!");
		});
	});
	</#if>
});

function detailsUpdate(){
	 var bng = document.getElementById('receiveTime').value;
     var end =  document.getElementById('endReceiveTime').value;
     var receiveV =  document.getElementById('receiveV');
     var endReceiveV =  document.getElementById('endReceiveV');

	 receiveV.innerText="";
	 endReceiveV.innerText="";
     if(bng == "" && end == ""){
    	 receiveV.innerText="折扣券发放时间不能为空！";
    	 endReceiveV.innerText="折扣券结束领取时间不能为空！";
         return false;
     }else if(bng == ""){
    	 receiveV.innerText="折扣券发放时间不能为空！";
         return false;
     }else if(end == ""){
    	 endReceiveV.innerText="折扣券结束领取时间不能为空！";
         return false;
     }
     
     var bngDate = new Date(bng);
     var endDate = new Date(end);
     var days = (endDate.getTime()-bngDate.getTime())/24/60/60/1000;
		 if (days < 0) {
	    	 endReceiveV.innerText="折扣券结束领取时间不能小于折扣券发放时间！";
		 	return false;
		 }else{
		 	 //alert("干！");
			 //detailsUpdateaaa();
			 return true;
		 }
  }

function detailsUpdateaaa(){
 	var _url = "saveDeductible";
    var _form = $("#form");
	_form.attr("action",_url);
	_form.submit();
}

</script>
	<form action="${basepath}/manage/deductibleVoucher" theme="simple" name="form" id="form" method="post">
		<input type="hidden" value="${e.id!""}" id="id"/>
		<table class="table table-bordered">
			<tr style="background-color: #dff0d8">
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>折扣券编辑 </strong>
				</td>
			</tr>
 	 	
			<tr>
				<td style="text-align: right;">折扣券名称</td>
				<td>
					<input type="text"  value="${e.deductibleName!''}" name="deductibleName" data-rule="折扣券名称:required;"  id="deductibleName" />
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">折扣券类型</td>
				<td>
				<#assign map = {'1':'抵价券'}>
                 <select id="status" name="status" class="input-medium">
                      <#list map?keys as key>
                        <option value="${key}" <#if e.deductibleType?? && (e.deductibleType+'')==key>selected="selected" </#if>>${map[key]}</option>
                       </#list>
                   </select>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">折扣券优惠值</td>
				<td>
					<input type="text"  value="${e.deductibleValue!''}" data-rule="折扣券优惠值:required;integer[+];"name="deductibleValue"  id="deductibleValue" />
				</td>
			</tr>
			 
			<tr>
				<td style="text-align: right;">折扣券发放时间</td>
				<td>
				<input id="receiveTime" style="height:30px" class="Wdate search-query input-small" type="text" name="receiveTime"
					value="${e.receiveTime!""}"
					onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'endReceiveTime\')||\'2020-10-01\'}'})"/>
					<span id="receiveV" style="color:red"></span>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">折扣券结束领取时间</td>
				<td>
				<input id="endReceiveTime" style="height:30px" class="Wdate search-query input-small" type="text" name="endReceiveTime"
					value="${e.endReceiveTime!""}"
					onFocus="WdatePicker({minDate:'#F{$dp.$D(\'receiveTime\')}',maxDate:'2020-10-01'})"/>
					<span id="endReceiveV" style="color:red"></span>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">折扣券发行数量</td>
				<td>
					<input type="text"  value="${e.deductibleCount!0}" name="deductibleCount" data-rule="折扣券名称:required;integer[+];"  id="deductibleCount" />
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">是否有效</td>
				<td>
				<#assign map = {'1':'是'}>
                 <select id="status" name="status" class="input-medium">
                      <#list map?keys as key>
                        <option value="${key}" <#if e.isUse?? && (e.isUse+'')==key>selected="selected" </#if>>${map[key]}</option>
                       </#list>
                   </select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;">
				<button method="insert" class="btn btn-success" onclick="return detailsUpdate();">新增 </button>
				</td>
			</tr>
			
		</table>
	</form>
<script type="text/javascript">
	$(function() {
		//$("#title").focus();
		selectDefaultCatalog();
	});
	function doSubmitFunc(obj){
	alert("adoahva");
			var m = $(obj).attr("name");
			console.log(m);
			console.log(m.split(":")[1]+".action");
			
			$("#form").on("valid.form", function(e, form){
				var _formAction = $("#form").attr("action");
				var aa = _formAction.substring(0,_formAction.lastIndexOf("/")+1);
				console.log(aa);
				
				var lastFormAction = aa + m.split(":")[1]+".action";
				$("#form").attr("action",lastFormAction);
				
				console.log($("#form").attr("action"));
				console.log(this.isValid);
				//form.submit();
			});
	}
	
	
	
	function doSubmitFuncByLink(obj){
		var _href = $(obj).attr("href");
		var _form = $("#form");
		_form.attr("action",_href);
		
		console.log("_href="+_href);
		
		$("#form").on("valid.form", function(e, form){
			console.log("this.isValid="+this.isValid);
			
			
			//_form.submit();
		});
		//_form.submit();
		return false;
	}
</script>
</@page.pageBase>