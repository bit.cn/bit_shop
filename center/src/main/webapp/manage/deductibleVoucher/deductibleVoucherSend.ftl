<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="折扣券管理">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
       //得到会员的列表
	   function getAccountList(who){
	   	var accountId = $(who).val();
	   	$.getJSON("${basepath}/manage/coupon/getAccountList", {accountId: accountId}, function(data){
	      $("#customerList").html("");
	      $("#customerList").parent().show()
		  $.each(data, function(i,item){
			$("#customerList").append("<li ondblclick='getAccount(this)'>"+item.id+"</li>");
		  });
		});
	   }
	   
	   //选中会员
	   function getAccount(who){
	   	$("#accountId").val($(who).text());
	   	 $("#customerList").html("");
	   	 $("#customerList").parent().hide();
	   }
	   
	   function sendAccountDeductible(){
	   	$("#form").submit();
	   }
</script>
<form action="${basepath}/manage/deductibleVoucher/"  theme="simple" name="form" id="form" method="post">
<div >
	<input id="deductibleCount" name="deductibleCount" value="${e.deductibleCount}" style="display: none;"/>
	<table class="table table-bordered">
			<tr style="background-color: #dff0d8">
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>派发折扣券给用户</strong>
				</td>
			</tr>
	<tr>
		<td style="text-align: right;vertical-align:middle;width:500px">
			用户ID: 
		</td>
		<td>
		<input style="text-align: left" id="accountId" name="userId" class="input_t2" data-rule="用户ID:required;"type="text" onkeydown="getAccountList(this)"/>
			<div style="margin-left:55px;width:180px;height:80px;background-color:rgb(243, 243, 243);overflow: auto;display:none;">
				<ul id="customerList">
				</ul>
			</div>
		</td>
	</tr>
	<tr>
		<td style="text-align: right;vertical-align:middle;">折扣券ID: 
		</td>
		<td>
		<input style="text-align: left" name="deductibleId"  data-rule="折扣券ID:required;" type="text" value="${e.id!""}" />
		</td>
	</tr>
	<tr>
		<td style="text-align: right;vertical-align:middle;">折扣券编号: 
		</td>
		<td><input style="text-align: left" name="deductibleSn" type="text" value="${e.deductibleSn!""}" />
		</td>
	</tr>
	</table>
	<p style="text-align: right;vertical-align:middle;width:550px">
        <button method="sendDeductible" class="btn btn-success" >
        <i class="icon-ok icon-white"></i> 完成
    </button></p>
	</div>
</form>
</@page.pageBase>