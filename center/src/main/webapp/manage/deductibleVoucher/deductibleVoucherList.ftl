<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="折扣券管理">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
	<form action="${basepath}/manage/deductibleVoucher" method="post" theme="simple" id="form" name="form">
		<input type="hidden" value="${e.id!""}" id="id"/>
		<table class="table table-bordered" >
			<tr>
				<td style="text-align: left;vertical-align:middle;">折扣券号码</td>
				<td style="text-align: left;"><input type="text" value="${e.deductibleSn!""}" class="input-medium search-query" name="deductibleSn"/></td>
				<td style="text-align: left;vertical-align:middle;">折扣券名称</td>
				<td style="text-align: left;"><input type="text" value="${e.deductibleName!""}" class="input-medium search-query" name="deductibleName"/></td>
			</tr>
			<tr>
				<td colspan="4">

					<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
						<i class="icon-search icon-white"></i> 查询
					</button>
						
					<a href="toAdd" class="btn btn-success">
						<i class="icon-plus-sign icon-white"></i> 添加
					</a>
					
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
                        <#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>

		<table class="table table-bordered table-hover">
			<tr style="background-color: #dff0d8">
				<th style="text-align: center" width="120px">ID</th>
				<th style="text-align: center">折扣券号码</th>
				<th style="text-align: center">折扣券名称</th>
				<th style="text-align: center">折扣券优惠值</th>
				<th style="text-align: center">折扣券优发行数量</th>
				<th style="text-align: center" width="100px;">操作</th>
			</tr>

            <#list pager.list as item>
				<tr style="text-align: center">
					<td >${item.id!""}</td>
					<td>
						 ${item.deductibleSn!""}
					</td>
					<td nowrap="nowrap"> ${item.deductibleName!""}</td>
					<td nowrap="nowrap"> ${item.deductibleValue!""}</td>
					<td nowrap="nowrap"> ${item.deductibleCount!""}</td>
					<td>
					<#if item.deductibleCount!=0>
						<a href="tosendDeductible?id=${item.id!""}">派发</a>
					</#if>
					</td>
				</tr>
            </#list>

			<tr>
				<td colspan="6" style="text-align: center;font-size: 12px;">
                    <#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
	</form>
	

<SCRIPT type="text/javascript">
$(function(){
	selectDefaultCatalog();
});
function selectDefaultCatalog(){
	var _catalogID = $("#catalogID").val();
	console.log("_catalogID="+_catalogID);
	if(_catalogID!='' && _catalogID>0){
		$("#catalogSelect").attr("value",_catalogID);
	}
}
</SCRIPT>
</@page.pageBase>