<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="优惠券审批">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<form action="${basepath}/manage/couponAppr" name="form" id="form" method="post" theme="simple">
	<input type="hidden" value="${e.type!""}" name="type" id="_type" />
		<table class="table table-bordered table-condensed table-hover">
		  	<tr>
				<td>商家名称</td>
				<td>
					<input type="text"  value="${e.businessName!""}" name="businessName"  class="input-small" id="businessName" />
				</td>
				<td style="width: 120px;vertical-align:middle;">优惠券有效期时间范围</td>
				<td><input id="d4311" class="Wdate search-query input-small" style="height:30px" type="text" name="startTime"
					value="${e.startTime!""}"
					onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01\'}'})"/>
					---- 
					<input id="d4312" class="Wdate search-query input-small" style="height:30px" type="text" name="endTime"
					value="${e.endTime!""}"
					onFocus="WdatePicker({minDate:'#F{$dp.$D(\'d4311\')}',maxTime:'2020-10-01'})"/>
				</td>
				<td colspan="16">
					<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
						<i class="icon-search icon-white"></i> 查询
					</button>							
				</td>
			</tr>
		</table>
		<input type="hidden" value="${e.id!""}" id="id"/>
		
		<table class="table table-bordered table-hover">
			<tr style="background-color: #dff0d8">
				<th style="text-align: center">商家ID</th>
				<th style="text-align: center">商家名称</th>
				<th style="text-align: center">申请优惠券类型</th>
				<th style="text-align: center">申请优惠券名称</th>
				<th style="text-align: center">申请优惠券数量</th>
				<th style="text-align: center">申请优惠券面值(折扣)</th>
				<th style="text-align: center">申请优惠券有效期</th>
				<th style="text-align: center">审批状态</th>
				<th style="text-align: center;width:30%;">审批</th>
			</tr>

            <#list pager.list as item>
				<tr style="text-align: center">
					<td nowrap="nowrap">${item.businessId!""}</td>
					<td nowrap="nowrap">${item.businessName!""}</td>
					<td nowrap="nowrap">
						<#if item.couponType?? && item.couponType==1>抵价券</#if>
						<#if item.couponType?? && item.couponType==2>折价券</#if>
					</td>
					<td nowrap="nowrap">${item.couponName!""}</td>
					<td nowrap="nowrap">${item.couponCount!""}</td>
					<td nowrap="nowrap">${item.couponValue!""}</td>
					<td nowrap="nowrap">${item.startTime!""}······${item.endTime!""}</td>
					<td nowrap="nowrap">
						<#if item.auditStatus??&&item.auditStatus==0> 未审批
						<#elseif item.auditStatus??&&item.auditStatus==1> 通过
						<#elseif item.auditStatus??&&item.auditStatus==2> 未通过
						<#else> ${item.auditStatus!""}
						</#if>
					</td>
					
					<td>
					<#if item.auditStatus==0>
						<button class="btn btn-warning" onclick="return passId(1,'${item.id}');"><i class="icon-edit icon-white"></i>通过</button>
						<button class="btn btn-warning" onclick="return passId(2,'${item.id}');"><i class="icon-edit icon-white"></i>不通过</button>
					<#else>
						<button class="btn btn-warning" disabled><i class="icon-edit icon-white"></i>通过</button>
						<button class="btn btn-warning" disabled><i class="icon-edit icon-white"></i>不通过</button>
					</#if>
					</td>
				</tr>
            </#list>

			<tr>
				<td colspan="9" style="text-align: center;font-size: 12px;">
                    <#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
	</form>
	

<SCRIPT type="text/javascript">
$(function(){
	selectDefaultCatalog();
});
function selectDefaultCatalog(){
	var _catalogID = $("#catalogID").val();
	console.log("_catalogID="+_catalogID);
	if(_catalogID!='' && _catalogID>0){
		$("#catalogSelect").attr("value",_catalogID);
	}
}

function passId(type,id) {
		try{
				var _url = "passByID?id="+id+"&auditStatus="+type;
				
				$.ajax({
				  type: 'POST',
				  url: _url,
				  data: {},
				  async:false,
				  success: function(data){
					  console.log("ajax.data="+data);
					  if(data){
						  
						var _form = $("#form");
						_form.attr("action","selectList");
						_form.submit();
					  }
					  jQuery.unblockUI();
				  },
				  dataType: "text",
				  error:function(){
					  	jQuery.unblockUI();
						alert("加载失败，请联系管理员。");
				  }
				});
		}catch(e){
			console.log("eee="+e);
		}
		return false;
	}
</SCRIPT>
</@page.pageBase>