<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="折扣券管理">
<style>
#insertOrUpdateMsg{
border: 0px solid #aaa;margin: 0px;position: fixed;top: 0;width: 100%;
background-color: #d1d1d1;display: none;height: 30px;z-index: 9999;font-size: 18px;color: red;
}
.btnCCC{
	background-image: url("../img/glyphicons-halflings-white.png");
	background-position: -288px 0;
}
</style>
<script>
$(function(){
	<#if e.id??>
	var id = "${e.id}";
	$("#btnStatic").click(function(){
		$.post("${basepath}/freemarker/create?method=staticNewsByID&id="+id, null ,function(response){
			alert(response == "success" ? "操作成功！" : "操作失败!");
		});
	});
	</#if>
});
</script>
	<form action="${basepath}/manage/coupon" namespace="/manage" theme="simple" name="form" id="form" method="post">
		<input type="hidden" value="${e.id!""}" id="id"/>
		<table class="table table-bordered">
			<tr>
				<td colspan="2" style="text-align: center;">
                    <button method="insert" class="btn btn-success">
                       <i class="icon-ok icon-white"></i> 新增
                     </button>
				</td>
			</tr>
			<tr style="background-color: #dff0d8">
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>折扣券编辑 </strong>
				</td>
			</tr>
			<tr style="display: none;">
				<td>id</td>
				<td><input type="hidden" value="${e.id!""}" name="id" label="id" /></td>
			</tr>
			
			<tr>
				<td style="text-align: right;">折扣券号码</td>
				<td>
					<input type="text"  value="${e.couponSn!""}" name="couponSn"  id="couponSn" />
				</td>
			</tr>
			
			<tr>
				<td style="text-align: right;">商家编号</td>
				<td>
					<input type="text"  value="${e.businessId!""}" name="businessId" id="businessId" />
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">优惠券名称</td>
				<td>
					<input type="text"  value="${e.couponName!""}" name="couponName" id="couponName" />
				</td>
			</tr>
			
		</table>
	</form>
<script type="text/javascript">
	$(function() {
		//$("#title").focus();
		selectDefaultCatalog();
	});
	function doSubmitFunc(obj){
			var m = $(obj).attr("name");
			console.log(m);
			console.log(m.split(":")[1]+".action");
			
			$("#form").on("valid.form", function(e, form){
				var _formAction = $("#form").attr("action");
				var aa = _formAction.substring(0,_formAction.lastIndexOf("/")+1);
				console.log(aa);
				
				var lastFormAction = aa + m.split(":")[1]+".action";
				$("#form").attr("action",lastFormAction);
				
				console.log($("#form").attr("action"));
				console.log(this.isValid);
				//form.submit();
			});
	}
	
	
	
	function doSubmitFuncByLink(obj){
		var _href = $(obj).attr("href");
		var _form = $("#form");
		_form.attr("action",_href);
		
		console.log("_href="+_href);
		
		$("#form").on("valid.form", function(e, form){
			console.log("this.isValid="+this.isValid);
			
			
			//_form.submit();
		});
		//_form.submit();
		return false;
	}
</script>
</@page.pageBase>