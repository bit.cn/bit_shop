<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="折扣券管理">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
	<form action="${basepath}/manage/discount" method="post" theme="simple" id="form" name="form">
		<input type="hidden" value="${e.id!""}" id="id"/>
		<table class="table table-bordered">
			<tr>
				<td>折扣券号码</td>
				<td><input type="text" value="${e.couponSn!""}" class="input-medium search-query" name="couponSn"/></td>
				<td>折扣券名称</td>
				<td>${e.couponName!""}</td>
				<td>折扣券类型</td>
				<td>
					${e.couponType!""}
				</td>
			</tr>
			<tr>
				<td colspan="16">

					<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
						<i class="icon-search icon-white"></i> 查询
					</button>
						
					<a href="toAdd" class="btn btn-success">
						<i class="icon-plus-sign icon-white"></i> 添加
					</a>
					
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
                        <#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>

		<table class="table table-bordered table-hover">
			<tr style="background-color: #dff0d8">
				<th width="20px"><input type="checkbox" id="firstCheckbox" /></th>
				<th width="120px">ID</th>
				<th>折扣券号码</th>
				<th>折扣券名称</th>
				<th width="130px">折扣券类型</th>
				<th width="80px">启用状态</th>
				<th width="60px;">操作</th>
			</tr>

            <#list pager.list as item>
				<tr>
					<td><input type="checkbox" name="ids" value="${item.id!""}" /></td>
					<td >${item.id!""}</td>
					<td>
						 ${item.couponSn!""}
					</td>
					<td nowrap="nowrap"> ${item.couponName!""}</td>
					<td nowrap="nowrap"> ${item.couponType!""}</td>
					<td nowrap="nowrap"> ${item.enableStatus!""}</td>
					<td>
						<a href="toEdit?id=${item.id!""}">派发</a>
					</td>
				</tr>
            </#list>

			<tr>
				<td colspan="17" style="text-align: center;font-size: 12px;">
                    <#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
	</form>
	

<SCRIPT type="text/javascript">
$(function(){
	selectDefaultCatalog();
});
function selectDefaultCatalog(){
	var _catalogID = $("#catalogID").val();
	console.log("_catalogID="+_catalogID);
	if(_catalogID!='' && _catalogID>0){
		$("#catalogSelect").attr("value",_catalogID);
	}
}
</SCRIPT>
</@page.pageBase>