<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="会员管理">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id=" ">
	<div id=" ">
		<div align="left" style="margin-bottom:10px;margin-top:-5px;">
			<a  class="btn btn-success btn-sm" href="selectList?type=Y"  > &nbsp;返回&nbsp; </a>
		</div>
		<div class="alert alert-info" style="width: 95%;margin-bottom: 2px;text-align: left;">
			<strong>设置会员密码：</strong>
		</div>
		<div style="margin-top:10px;">
			<td>
				<tr>
					<input type="text" id="inputNewPwd" placeholder="请输入新密码" />
				</tr>
				<tr>
				<a id="btnChangePwd" class="go">保存</a>
				</tr>
			</td>
		</div>
</div>
<script type="text/javascript">
	$("#btnChangePwd").click(function(){
		var newPwd = $('#inputNewPwd').val();
		if(newPwd == null || newPwd.trim() == '')
		{
			alert('请正确输入密码');
			return;
		}
		var _url = "${basepath}/manage/account/saveNewPwd/${id}";
	$.ajax({
	  type: 'POST',
	  url: _url,
	 data:{"id": ${id},"newPwd" : newPwd},
            success:function(data){
           	    if(data == "${id}"){
           	    	alert("设置密码成功");
           	    } else{
           	    	alert("设置密码失败");
           	    } 
           	    
            } ,
            error: function(){
                alert("服务器处理异常");
            }         
         });
	});
</script>

</@page.pageBase>