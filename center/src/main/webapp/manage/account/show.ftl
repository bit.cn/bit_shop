<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="会员管理">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />




<div id=" ">
	<div id=" ">
		<div align="left" style="margin-bottom:10px;margin-top:-5px;">
			<a  class="btn btn-success btn-sm" href="selectList?type=Y"  > &nbsp;返回&nbsp; </a>
		</div>
		<div class="alert alert-info" style="width: 95%;margin-bottom: 2px;text-align: left;">
			<strong>会员信息：</strong>
		</div>
		<table class="table table-bordered" style="width: 95%;vertical-align:middle;">
					<tr style="display: none;">
						<td>id</td>
						<td><input type="hidden" value="${e.id!""}" name="id" label="id" id="id"/></td>
					</tr>
					<tr>
						<td style="text-align: right;">会员类型</td>
						<td style="text-align: left;">
							<#if e.accountType?? && e.accountType=="qq">
								<span class="badge badge-warning">${e.accountTypeName!""}</span>
								<img alt="" src="${systemSetting().staticSource}/images/mini_qqLogin.png">
							<#elseif  e.accountType?? && e.accountType=="sinawb">
								<span class="badge badge-warning">${e.accountTypeName!""}</span>
								<img alt="" src="${systemSetting().staticSource}/images/mini_sinaWeibo.png">
							<#elseif  e.accountType?? && e.accountType=="alipay">
								<span class="badge badge-warning">alipay</span>
								<img alt="" src="${systemSetting().staticSource}/images/alipay_fastlogin.jpg">
							<#else>
								<span class="badge badge-warning">anitoys_pc</span>
							</#if>
						
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">用户名</td>   
						<td style="text-align: left;">${e.account!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">真实姓名</td>   
						<td style="text-align: left;">${e.trueName!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">性别</td>
						<td style="text-align: left;"><#if e.sex?? && e.sex=="m">男	<#elseif e.sex?? && e.sex=="f">女<#elseif e.sex?? && e.sex=="s">保密</#if></td>
					</tr>
					
					<tr>
						<td style="text-align: right;">生日</td>
						<td style="text-align: left;">${e.birthday!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">账户积分</td>
						<td style="text-align: left;">${accountFinance.rank!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">账户余额</td>
						<td style="text-align: left;">${accountFinance.amount!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">所在地区</td>
						<td style="text-align: left;">${e.city!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">详细地址</td>
						<td style="text-align: left;">${e.address!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">邮政编码</td>
						<td style="text-align: left;">${e.zip!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">手机</td>
						<td style="text-align: left;">${e.phone!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">email</td>
						<td style="text-align: left;">${e.email!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">电话</td>
						<td style="text-align: left;">${e.tel!""}</td>
					</tr>				
					<tr>
						<td style="text-align: right;">QQ</td>
						<td style="text-align: left;">${e.qq!""}</td>
					</tr>
					<tr>
						<td style="text-align: right;">微博</td>
						<td style="text-align: left;">${e.microblog!""}</td>
					</tr>			
				</table>
	
	</div>
	
		<form action="${basepath}/manage/account" name="forms" id="forms" method="post" theme="simple">
			
			<div class="alert alert-info" style="width: 95%;margin-bottom: 2px;text-align: left;">
				<strong>购买：</strong>
			</div>
				
				<input type="text"  value="${e.id!""}" name="id"  id="id" size="20" maxlength="20" style="display: none;"/>
				<table class="table table-bordered" style="width: 95%;vertical-align:middle;">
					<tr>
						<td style="width: 600px;">时间范围：<input readonly id="d4311" style="height:30px" class="Wdate search-query input-small" type="text" name="startDate"
							value="${e.startDate!""}"
							onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01\'}'})"/>
							~ 
							<input readonly id="d4312" class="Wdate search-query input-small" style="height:30px" type="text" name="endDate"
							value="${e.endDate!""}"
							onFocus="WdatePicker({minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01'})"/>
						</td>
						<td colspan="2" >
							<button class="icon-search icon-white" onclick="return detailsSelect('${e.id}');"><i class="icon-edit icon-white"></i>查询</button>
						</td>
					</tr>
					</table>
					<table class="table table-bordered" style="width: 95%;vertical-align:middle;">
					<tr  >
						<th width="100" data-options="field:'parentId'" nowrap="nowrap" style=" background-color: #dff0d8">订单号</th>
						<th width="100" data-options="field:'account'" nowrap="nowrap" style=" background-color: #dff0d8">购买时间</th>
						<th width="100" data-options="field:'ptotal'" nowrap="nowrap" style=" background-color: #dff0d8">订单总额</th>
					</tr>
					<#list pager.list as item>
						<tr  >
							<td>${item.orderCode!""}</td>
							<td>${item.createdate!""}</td>
							<td>${item.amount!""}</td>
						</tr>
					</#list>
					<tr>
						<td colspan="3" style="text-align: center;">
							<#import "/manage/system/pagerByVal.ftl" as pag>
							<@pag.pga id="${e.id}"/>
						</td>
					</tr>
				</table>
		</form>
			
			<br/><br/><br/><br/><br/>
			
</div>

<script type="text/javascript">
$(function() {
	$( "#tabs" ).tabs({
		//event: "mouseover"
	});
	
});
	//详情
	function detailsSelect(id){
	 	var _url = "showAccount?id="+id+"&selegeOrder=y"; 
        var _form = $("#forms");
		_form.attr("action",_url);
		_form.submit();
	}
</script>

</@page.pageBase>