<#import "/manage/tpl/pageBase.ftl" as page/>
<@page.pageBase currentMenu="商家管理">
<script type="text/javascript">
	$(function() {
		$("#title").focus();
		//$('#cc').combotree('setValue', "随遇而安随遇而安随遇而安随遇而安随遇而安");
	});

	function onSubmit() {
		if ($.trim($("#name").val()) == "") {
			alert("名称不能为空!");
			$("#title").focus();
			return false;
		}
	}
function PreviewImage(imgFile) 
   { 
     var path; 
     if(document.all)//IE 
     { 
      imgFile.select(); 
      path = document.selection.createRange().text; 
      document.getElementById("imgPreview").innerHTML=""; 
      document.getElementById("imgPreview").style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")";//使用滤镜效果 
     } 
     else//FF 
     { 
      path = URL.createObjectURL(imgFile.files[0]);
      document.getElementById("imgPreview").innerHTML = "<img style='width:100px; height:100px;' src='"+path+"'/>"; 
     } 
   } 
	
</script>
	<form action="${basepath}/manage/business/insertBusiness" namespace="/manage" theme="simple" name="form" id="form" method="post"  enctype="multipart/form-data" >
		<input id="id" value="${e.id!""}" style="display: none;"/>
		<table class="table table-bordered" style="width: 95%;margin: auto;">
			<tr style="background-color: #dff0d8">
				<td colspan="4" style="background-color: #dff0d8;text-align: center;">
					<strong>添加商家</strong>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;width:220px">账号</td>
				<td style="text-align: left;"><input type="text" value="${e.username!""}" name="username"  id="username" data-rule="账号;required;username;remote[uniqueUsername, id]" size="20"/>
				</td>
				<td style="text-align: right;">密码</td>
				<td style="text-align: left;"><input type="text" value="${e.password!""}" name="password"  id="password" size="20" data-rule="密码;required;password"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;width:220px">商家名称</td>
				<td style="text-align: left;"><input type="text"  data-rule="商家名称;required;businessName" value="${e.businessName!""}" name="businessName"  id="businessName" size="20"/>
				</td>
				<td style="text-align: right;">商家营业执照编号</td>
				<td style="text-align: left;"><input type="text"  value="${e.businessLicense!""}" name="businessLicense"  id="businessLicense" size="20"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">商家组织机构代码证编号</td>
				<td style="text-align: left;"><input type="text"  value="${e.businessOrcc!""}" name="businessOrcc"  id="businessOrcc" size="20"/>
				</td>
				<td style="text-align: right;">商家税务登记证编号</td>
				<td style="text-align: left;"><input type="text"  value="${e.businesstTrc!""}" name="businesstTrc"  id="businesstTrc" size="20"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">商企业法人名称</td>
				<td style="text-align: left;"><input type="text"  value="${e.businessApName!""}" name="businessApName"  id="businessApName" size="20"/>
				</td>
				<td style="text-align: right;">商家法人身份证编号</td>
				<td style="text-align: left;"><input type="text"  value="${e.businessApNo!""}" name="businessApNo"  id="businessApNo" size="20"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">企业联系电话</td>
				<td style="text-align: left;"><input type="text"  value="${e.businessPhone!""}" name="businessPhone"  id="businessPhone" size="20"/>
				</td>
				<td style="text-align: right;">企业工作地址</td>
				<td style="text-align: left;"><input type="text"  value="${e.businessAddress!""}" name="businessAddress"  id="businessAddress" size="20"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">商家联系人手机号码</td>
				<td style="text-align: left;"><input type="text"  value="${e.businessMobile!""}" name="businessMobile"  id="businessMobile" size="20"/>
				</td>
				<td style="text-align: right;">企业联系人</td>
				<td style="text-align: left;"><input type="text"  value="${e.businessContact!""}" name="businessContact"  id="businessContact" size="20"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">客服1</td>
				<td style="text-align: left;"><input type="text"  value="${e.service1!""}" name="service1"  id="service1" size="20"/>
				</td>
				<td style="text-align: right;">客服2</td>
				<td style="text-align: left;"><input type="text"  value="${e.service2!""}" name="service2"  id="service2" size="20"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">客服3</td>
				<td style="text-align: left;"><input type="text"  value="${e.service3!""}" name="service3"  id="service3" size="20"/>
				</td>
				<td style="text-align: right;">客服4</td>
				<td style="text-align: left;"><input type="text"  value="${e.service4!""}" name="service4"  id="service4" size="20"/>
				</td>
			</tr>
			<tr>
			<td style="text-align: right;">商家所在区域编号</td>
				<td style="text-align: left;">
				<input type="text"  value="${e.regionID!0}" name="regionID"  id="regionID" size="20"/>
				</td>
				<td style="text-align: right;">商家所在区域名称</td>
				<td style="text-align: left;">
					<input type="text"  value="${e.regionName!""}" name="regionName"  id="regionName" size="20"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">商家关闭原因</td>
				<td style="text-align: left;"><input type="text"  value="${e.closeReason!""}" name="closeReason"  id="closeReason" size="20"/>
				</td>
				<td style="text-align: right;">商家显示排序</td>
				<td style="text-align: left;">
				<input type="text"  value="${e.sortOrder!0}" name="sortOrder"  id="sortOrder" size="20" data-rule="商家显示排序;required;sortOrder;remote[uniqueCode, id]"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">商家模板主题</td>
				<td style="text-align: left;"><input type="text"  value="${e.theme!""}" name="theme" id="theme" size="20"/>
				</td>
				<td style="text-align: right;">店铺入住时间</td>
				<td style="text-align: left;">
				
				<input id="addTime" style="height:30px" class="Wdate search-query input-small" type="text" name="addTime"
					value="${e.addTime!""}"
					onFocus="WdatePicker({maxDate:'#F{\'2020-10-01\'||\'2020-10-01\'}'})"/>
					
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">客服地址</td>
				<td colspan="3" style="text-align: left;"><input type="text"  value="${e.customerAddress!""}" name="customerAddress" data-rule="客服地址;required;customerAddress"  id="customerAddress" size="20" maxlength="255"/>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">大图片地址</td>
				<td style="text-align: left;">
					<input type="file" name="fileName" id="imgFile" onChange = "javascirpt:cutImge(this)" data-rule="大图片地址;required;fileName"/><br>
				   	<input type="hidden" id="x" name="x"/>
		            <input type="hidden" id="y" name="y"/>
		            <input type="hidden" id="w" name="w"/>
		            <input type="hidden" id="h" name="h"/> 
		            <!--<div>
		            	<img id="target" width="300"/>
		            </div> 
					<div class="modal-footer">
							<input type="button" onclick="reCutImg();" value="重新裁剪"/>
		        			<input type="button" onclick="confirmCutImg();" value="确认裁剪"/>
		    		</div>-->
				</td>
				<td style="text-align: right;">商家状态</td>
				<td style="text-align: left;" >
				
				 <#assign map = {'1':'有效','2':'无效'}>
                        <select id="status" name="status" class="input-medium">
                            <#list map?keys as key>
                                <option value="${key}" <#if e.status?? && (e.status+'')==key>selected="selected" </#if>>${map[key]}</option>
                            </#list>
                        </select>
				</td>
			
			</tr>
			<tr>
				<td style="text-align: right;">banner图片</td>
				<td colspan="3" style="text-align: left;">
					<input type="file" name="bannerName" id="bannerName" onchange="javascript:setBannerImagePreview();" data-rule="banner图片;required;bannerName"/><br>
					<input value="${e.bannerPicture!""}" name="bannerPicture" id="bannerPicture" style="display: none;" />
				</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: center;">
				<button class="btn-style-01" onclick="return detailsUpdate();">新增</button>
                     &nbsp;&nbsp;
					<a  class="btn btn-success" href="selectList?type=Y"><i class="icon-ok icon-white"></i>返回</a>
				</td>
			</tr>
		</table>
	</form>

<script type="text/javascript">{
	$(function(){
		selectDefaultCatalog();
		$("#name").blur(function(){getCode();});
	});
   	//修改
	function detailsUpdate(){
	 	var _url = "insertBusiness?type=m";
        var _form = $("#form");
		_form.attr("action",_url);
		_form.submit();
	}
	function selectDefaultCatalog(){
		var _catalogID = $("#catalogID").val();
		console.log("selectDefaultCatalog._catalogID="+_catalogID);
		//if(_catalogID!='' && _catalogID>0){
			$("#catalogSelect").val(_catalogID);
		//}
	}
	function catalogChange(obj){
		var _pid = $(obj).find("option:selected").attr("pid");
		console.log("_pid="+_pid);
		if(!(_pid && _pid==0)){
			alert("不能选择子类!");
			selectDefaultCatalog();
			return false;
		}
	}
	function getCode(){
		var _name = $("#name").val();
		//var _url = "catalog!autoCode.action?e.name="+_name;
		var _url = "autoCode";
		$.ajax({
	 		type: 'POST',
	  		url: _url,
	  		data: {"name":_name},
	  		dataType:"text",
	  		//async:false,
	  		success: function(data){
		  		if(!data){return null;}
		  		console.log("data="+data);
		  		$("#code").val(data);
	  		},
	  		error:function(){
		  		console.log("加载数据失败，请联系管理员。");
	  		}
		});
	}}
    //确认裁剪
    function reCutImg(){
		    if(api != null){
		    	//alert('禁用选择框');
		    	api.enable();
		    	return;
		    }
		    alert('jcrop is null');
    }
    //重新裁剪
    function confirmCutImg(){
		    if(api != null){
		    	//alert('禁用选择框');
		    	api.disable();
		    	return;
		    }
		    alert('jcrop is null');
    }
    //加载裁剪工具
	function cutImge(input){
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = function (e){
                    $('#target').removeAttr('src');
                    $('#target').attr('src', e.target.result);
                    //api = $.Jcrop('#target', {
                    //    setSelect: [ 10, 10, 100, 100 ],
                    //    aspectRatio: 1,
                    //    onSelect: updateCoords
                   // });
                };
                //if (api != undefined) {
                   // api.destroy();
               // }
            }
            function updateCoords(obj) {
                $("#x").val(obj.x);
                $("#y").val(obj.y);
                $("#w").val(obj.w);
                $("#h").val(obj.h);
            };
   }
</script>
</@page.pageBase>