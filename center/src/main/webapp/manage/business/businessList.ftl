<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="商家管理">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
	<form action="${basepath}/manage/business" name="form" id="form" method="post" theme="simple">
		<table class="table table-bordered">
			<tr>
				<td style="width: 8%;vertical-align:middle;">商家编号</td>
				<td><input type="text" value="${e.id!""}" class="input-medium search-query" name="id"/></td>
				<td style="width: 8%;vertical-align:middle;">商家状态</td>
				<td>
				  <#assign map = {'0':'全部','1':'有效','2':'无效'}>
                    <select id="status" name="status" class="search-query input-medium">
						<#list map?keys as key>
                            <option value="${key}" <#if e.status?? && (e.status+'')==key>selected="selected" </#if>>${map[key]}</option>
						</#list>
                    </select>
					</td>
				<td style="width: 120px;vertical-align:middle;">入驻时间范围</td>
				<td><input id="d4311" class="Wdate search-query input-small" style="height:30px" type="text" name="startDate"
					value="${e.startDate!""}"
					onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01\'}'})"/>
					~ 
					<input id="d4312" class="Wdate search-query input-small" style="height:30px" type="text" name="endDate"
					value="${e.endDate!""}"
					onFocus="WdatePicker({minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01'})"/>
				</td>
			</tr>
			
			<tr>
				<td colspan="6">
						<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
							<i class="icon-search icon-white"></i> 查询
						</button>
						
						<a href="toAdd" class="btn btn-success">
						<i class="icon-plus-sign icon-white"></i> 添加
						</a>
						
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
						<#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>
				
		<table class="table table-bordered table-hover">
			<tr style="background-color: #dff0d8">
				<th>商家编号</th>
				<th>商家显示排序</th>
				<th>商家名称</th>
				<th>商企业法人名称</th>
				<th>商家账号</th>
				<th>企业联系人</th>
				<th>商家状态</th>
				<th width="60px">操作</th>
			</tr>
			<#list pager.list as item>
				<tr>
					<td>${item.id!""}</td>
					<td>${item.sortOrder!""}</td>
					<td>${item.businessName!""}</td>
					<td>${item.businessApName!""}</td>
					<td>${item.username!""}</td>
					<td>${item.businessContact!""}</td>
					<td>
					<#if item.status??&&item.status==1> 有效
					<#elseif item.status??&&item.status==2> 无效
					<#else> ${item.status!""}
					</#if>
					</td>
					
					<td style="width: 200px;">
					
					<button class="btn btn-warning" onclick="return detailsSelect('${item.id}');"><i class="icon-edit icon-white"></i>详情</button>
					<button class="btn btn-warning" onclick="return editSelect('${item.id}');"><i class="icon-edit icon-white"></i>编辑</button>
					
					<#if item.status??&&item.status==1>
						<button class="btn btn-warning" onclick="return disable('${item.id}',2,'${item.regionID}','${item.sortOrder}');"><i class="icon-edit icon-white"></i>禁用</button>
					<#elseif item.status??&&item.status==2>
						<button class="btn btn-warning" onclick="return disable('${item.id}',1,'${item.regionID}','${item.sortOrder}');"><i class="icon-edit icon-white"></i>启用</button>
					</#if>
					</td>
				</tr>
			</#list>
			<tr>
				<td colspan="8" style="text-align: center;">
					<#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
		
		
	</form>
	
	

<SCRIPT type="text/javascript">
$(function(){
	selectDefaultCatalog();
});
function selectDefaultCatalog(){
	var _catalogID = $("#catalogID").val();
	console.log("_catalogID="+_catalogID);
	if(_catalogID!='' && _catalogID>0){
		$("#catalogSelect").attr("value",_catalogID);
	}
}

	//编辑
	function editSelect(id){
        var _url = "toEdit?id="+id+"&type=m";
        var _form = $("#form");
		_form.attr("action",_url);
		_form.submit();
	}
	
	//详情
	function detailsSelect(id){
	
	 	var _url = "toEdit?id="+id+"&type=p";
        var _form = $("#form");
		_form.attr("action",_url);
		_form.submit();
	
	}

//禁用
function disable(id,status,regionID,sortOrder) {
		try{
				var _url = "disableByID?id="+id+"&status="+status+"&regionID="+regionID+"&sortOrder="+sortOrder;
				
				$.ajax({
				  type: 'POST',
				  url: _url,
				  data: {},
				  async:false,
				  success: function(data){
					  console.log("ajax.data="+data);
					  if(data){
						  
						var _form = $("#form");
						_form.attr("action","selectList");
						_form.submit();
					  }
					  jQuery.unblockUI();
				  },
				  dataType: "text",
				  error:function(){
					  	jQuery.unblockUI();
						alert("加载失败，请联系管理员。");
				  }
				});
		}catch(e){
			console.log("eee="+e);
		}
		return false;
	}
</SCRIPT>
</@page.pageBase>