<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="商家管理">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
     
	   function updateBusiness(){
	   	$("#form").submit();
	   }
	   
	   //修改
	function detailsUpdate(id){
	 	var _url = "deposit?type=m";
        var _form = $("#form");
		_form.attr("action",_url);
		_form.submit();
	}
</script>
	<#if e.type??&&e.type=="m">
	<form action="${basepath}/manage/business/deposit" namespace="/manage" theme="simple" name="form" id="form" method="post"  enctype="multipart/form-data" >
	<div>
				<input type="text"  value="${e.id!""}" name="id"  id="id" size="20" style="display: none;"/>
			<table class="table table-bordered" style="width: 95%;vertical-align:middle;">
				<tr>
					<td style="text-align: right;width:220px">商家名称</td>
					<td style="text-align: left;"><input type="text"  value="${e.businessName!""}" name="businessName"  id="businessName" size="20" data-rule="商家名称;required;businessName"/>
					</td>
					<td style="text-align: right;">商家营业执照编号</td>
					<td style="text-align: left;"><input type="text"  value="${e.businessLicense!""}" name="businessLicense"  id="businessLicense" size="20"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">商家组织机构代码证编号</td>
					<td style="text-align: left;"><input type="text"  value="${e.businessOrcc!""}" name="businessOrcc"  id="businessOrcc" size="20"/>
					</td>
					<td style="text-align: right;">商家税务登记证编号</td>
					<td style="text-align: left;"><input type="text"  value="${e.businesstTrc!""}" name="businesstTrc"  id="businesstTrc" size="20"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">商企业法人名称</td>
					<td style="text-align: left;"><input type="text"  value="${e.businessApName!""}" name="businessApName"  id="businessApName" size="20"/>
					</td>
					<td style="text-align: right;">商家法人身份证编号</td>
					<td style="text-align: left;"><input type="text"  value="${e.businessApNo!""}" name="businessApNo"  id="businessApNo" size="20"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">企业联系电话</td>
					<td style="text-align: left;"><input type="text"  value="${e.businessPhone!""}" name="businessPhone"  id="businessPhone" size="20"/>
					</td>
					<td style="text-align: right;">企业工作地址</td>
					<td style="text-align: left;"><input type="text"  value="${e.businessAddress!""}" name="businessAddress"  id="businessAddress" size="20"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">商家联系人手机号码</td>
					<td style="text-align: left;"><input type="text"  value="${e.businessMobile!""}" name="businessMobile"  id="businessMobile" size="20"/>
					</td>
					<td style="text-align: right;">企业联系人</td>
					<td style="text-align: left;"><input type="text"  value="${e.businessContact!""}" name="businessContact"  id="businessContact" size="20"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">客服1</td>
					<td style="text-align: left;"><input type="text"  value="${e.service1!""}" name="service1"  id="service1" size="20"/>
					</td>
					<td style="text-align: right;">客服2</td>
					<td style="text-align: left;"><input type="text"  value="${e.service2!""}" name="service2"  id="service2" size="20"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">客服3</td>
					<td style="text-align: left;"><input type="text"  value="${e.service3!""}" name="service3"  id="service3" size="20"/>
					</td>
					<td style="text-align: right;">客服4</td>
					<td style="text-align: left;"><input type="text"  value="${e.service4!""}" name="service4"  id="service4" size="20"/>
					</td>
				</tr>
				<tr>
				<td style="text-align: right;">商家所在区域编号</td>
					<td style="text-align: left;"><input type="text"  value="${e.regionID!0}" name="regionID"  id="regionID" size="20"/>
					</td>
					<td style="text-align: right;">商家所在区域名称</td>
					<td style="text-align: left;"><input type="text"  value="${e.regionName!""}" name="regionName"  id="regionName" size="20"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">商家关闭原因</td>
					<td style="text-align: left;"><input type="text"  value="${e.closeReason!""}" name="closeReason"  id="closeReason" size="20"/>
					</td>
					<td style="text-align: right;">商家显示排序</td>
					<td style="text-align: left;"><input type="text"  value="${e.sortOrder!"0"}" name="sortOrder"  id="sortOrder" data-rule="商家显示排序;required;sortOrder;remote[uniqueCode, id]" size="20"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">商家模板主题</td>
					<td style="text-align: left;"><input type="text"  value="${e.theme!""}" name="theme" id="theme" size="20"/>
					</td>
					<td style="text-align: right;">店铺入住时间</td>
					<td style="text-align: left;">
						<input id="addTime" style="height:30px" class="Wdate search-query input-small" type="text" name="addTime"
						value="${e.addTime!""}"
						onFocus="WdatePicker({maxDate:'#F{\'2020-10-01\'||\'2020-10-01\'}'})"/>
					</td>
				</tr>
			<tr>
				<td style="text-align: right;">客服地址</td>
				<td colspan="3" style="text-align: left;"><input type="text"  value="${e.customerAddress!""}" data-rule="客服地址;required;customerAddress" name="customerAddress"  id="customerAddress" size="20"/>
				</td>
			</tr>
				<tr>
					<td style="text-align: right;">大图片地址</td>
					<td style="text-align: left;">
					<input type="file" name="fileName" id="fileName" onchange="javascript:setImagePreview();" />
					<input value="${e.maxPicture!""}" name="maxPicture" id="maxPicture" style="display: none;"  data-rule="大图片;required;maxPicture"/> <br>
					<img src="${systemSetting().imageRootPath}/${(e.maxPicture)!''}"height="140px"  width="140px"/>
					</td>
					<td style="text-align: right;">商家状态</td>
					<td style="text-align: left;">
					 <#assign map = {'1':'有效','2':'无效'}>
	                        <select id="status" name="status" class="input-medium">
	                            <#list map?keys as key>
	                                <option value="${key}" <#if e.status?? && (e.status+'')==key>selected="selected" </#if>>${map[key]}</option>
	                            </#list>
	                        </select>
					</td>
				
				</tr>
				<tr>
				<td style="text-align: right;">banner图片</td>
				<td colspan="3" style="text-align: left;">
					<input type="file" name="bannerName" id="bannerName" onchange="javascript:setBannerImagePreview();" />
					<input value="${e.bannerPicture!""}" name="bannerPicture" id="bannerPicture" style="display: none;" /> <br>
					<img src="${systemSetting().imageRootPath}/${(e.bannerPicture)!''}"height="140px"  width="140px"/>
				</td>
			</tr>
				<tr>
					<td colspan="4" style="text-align: center;">
					<button class="btn-style-01" onclick="return detailsUpdate('${e.id}');">完成</button>
					</td>
				</tr>
			</table>
		</div>
		
	</form>
			
		<#elseif e.type??&&e.type=="p">
		<div>
			<a  class="btn btn-success btn-sm" href="selectList?type=Y">返回</a>
		</div>
		<div style="width: 95%;vertical-align:middle;">
			<div class="btn-warning" style="width: 95%;margin-bottom: 2px;padding:5px"><h4>商家基本信息</h4></div>
			<table class="table table-bordered" style="width: 95%;vertical-align:middle;">
			<tr>
				<td style="text-align: right;">商家名称</td>
				<td style="text-align: left;">${e.businessName!""}</td>
				<td style="text-align: right;">商家营业执照编号</td>
				<td style="text-align: left;">${e.businessLicense!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">商家组织机构代码证编号</td>
				<td style="text-align: left;">${e.businessOrcc!""}</td>
				<td style="text-align: right;">商家税务登记证编号</td>
				<td style="text-align: left;">${e.businesstTrc!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">商企业法人名称</td>
				<td style="text-align: left;">${e.businessApName!""}</td>
				<td style="text-align: right;">商家法人身份证编号</td>
				<td style="text-align: left;">${e.businessApNo!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">企业联系电话</td>
				<td style="text-align: left;">${e.businessPhone!""}</td>
				<td style="text-align: right;">企业工作地址</td>
				<td style="text-align: left;">${e.businessAddress!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">商家联系人手机号码</td>
				<td style="text-align: left;">${e.businessMobile!""}</td>
				<td style="text-align: right;">企业联系人</td>
				<td style="text-align: left;">${e.businessContact!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">客服1</td>
				<td style="text-align: left;">${e.service1!""}</td>
				<td style="text-align: right;">客服2</td>
				<td style="text-align: left;">${e.service2!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">客服3</td>
				<td style="text-align: left;">${e.service3!""}
				</td>
				<td style="text-align: right;">客服4</td>
				<td style="text-align: left;">${e.service4!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">商家所在区域编号</td>
				<td style="text-align: left;">${e.regionID!""}</td>
				<td style="text-align: right;">商家所在区域名称</td>
				<td style="text-align: left;">${e.regionName!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">商家关闭原因</td>
				<td style="text-align: left;">${e.closeReason!""}</td>
				<td style="text-align: right;">商家显示排序</td>
				<td style="text-align: left;">${e.sortOrder!"0"}</td>
			</tr>
			<tr>
				<td style="text-align: right;">商家模板主题</td>
				<td style="text-align: left;">${e.theme!""}</td>
				<td style="text-align: right;">店铺入住时间</td>
				<td style="text-align: left;">${e.addTime!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">客服地址</td>
				<td colspan="3" style="text-align: left;">${e.customerAddress!""}</td>
			</tr>
			<tr>
				<td style="text-align: right;">大图片地址</td>
				<td style="text-align: left;"><img src="${systemSetting().imageRootPath}/${(e.maxPicture)!''}" height="140px"  width="140px" data-rule="大图片地址;required;fileName"/> </td>
				<td style="text-align: right;">商家状态</td>
				<td style="text-align: left;" >
					<#if e.status??&&e.status==2> 无效
					<#elseif e.status??&&e.status==1> 有效
					<#else> ${e.status!""}
					</#if>
				</td>
			
			</tr>
			<tr>
				<td style="text-align: right;">banner图片</td>
				<td colspan="3" style="text-align: left;">
					<img src="${systemSetting().imageRootPath}/${(e.bannerPicture)!''}" height="140px"  width="140px" data-rule="banner图片;required;bannerName"/>
				</td>
			</tr>
		</table>
		</div>
			
			<form action="${basepath}/manage/business" name="forms" id="forms" method="post" theme="simple">
			
			<div style="width: 95%;vertical-align:middle">
				<div class="btn-warning" style="width: 95%;margin-bottom: 2px;padding:5px"><h4>商家订单信息</h4></div>
				<input type="text"  value="${e.id!""}" name="id"  id="id" size="20" style="display: none;"/>
				<table class="table table-bordered" style="width: 95%;vertical-align:middle;">
					<tr>
						<!--<td>查询范围：<#assign map = {'0':'请选择','1':'本周','2':'本月','3':'自定义'}>
		                   <select id="qType" name="qType" onchange="show('1')" class="input-medium">
		                     <#list map?keys as key>
		                        <option value="${key}" <#if e.qType?? && (e.qType+'')==key>selected="selected" </#if>>${map[key]}</option>
		                      </#list>
		                     </select>
						</td>-->
					
						<td style="width: 600px;">时间范围：<input readonly id="d4311" style="height:30px" class="Wdate search-query input-small" type="text" id="startDates" name="startDates"
							value="${e.startDates!""}"
							onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01\'}'})"/>
							~ 
							<input readonly id="d4312" class="Wdate search-query input-small" style="height:30px" type="text" name="endDates"
							value="${e.endDates!""}"
							onFocus="WdatePicker({minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01'})"/>
						</td>
						<td colspan="2" >
							<button class="icon-search icon-white" onclick="return detailsSelect('${e.id}');"><i class="icon-edit icon-white"></i>查询</button>
						</td>
					</tr>
					</table>
					<table class="table table-bordered" style="width: 95%;vertical-align:middle;">
					<tr  style="text-align: left;width:200px;background-color: #dff0d8">
						<th width="100" data-options="field:'parentId'" nowrap="nowrap">订单号</th>
						<th width="100" data-options="field:'account'" nowrap="nowrap">购买人ID</th>
						<th width="100" data-options="field:'ptotal'" nowrap="nowrap">订单总额</th>
					</tr>
					<#list pager.list as item>
						<tr class="treegrid-${item.id} ${(item.parentId=="0")?string("","treegrid-parent-"+item.parentId)}">
							<td>${item.orderCode!""}</td>
							<td>${item.accountName!""}</td>
							<td>${item.ptotal!""}</td>
						</tr>
					</#list>
					
			<tr>
				<td colspan="3" style="text-align: center;">
					<#import "/manage/system/pagerByVal.ftl" as pag>
					<@pag.pga id="${e.id}"/>
				</td>
			</tr>
				</table>
			</div>
		</form>
			
			<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
			
		</#if>
	
<SCRIPT type="text/javascript">
$(function(){
	selectDefaultCatalog();
});
function selectDefaultCatalog(){
	var _catalogID = $("#catalogID").val();
	console.log("_catalogID="+_catalogID);
	if(_catalogID!='' && _catalogID>0){
		$("#catalogSelect").attr("value",_catalogID);
	}
}
	
	//详情
	function detailsSelect(id){
	 	var _url = "toEdit?id="+id+"&type=p";
        var _form = $("#forms");
		_form.attr("action",_url);
		_form.submit();
	}

function show(index) {
	if (index == "1") {
	alert("hello word!111");
    }else if(index == "2") {
 	alert("hello word!2222");
    }else{
    alert("hello word!3333");
	}
}

function setImagePreview() {
var docObj=document.getElementById("fileName");

if(docObj.files &&docObj.files[0]){
	document.getElementById("maxPicture").value = docObj.value;
}else{
	docObj.select();
	var imgSrc = document.selection.createRange().text;
	document.getElementById("maxPicture").value = imgSrc;
}
return true;
}
	
function setBannerImagePreview() {
	var docObj=document.getElementById("bannerName");

	if(docObj.files &&docObj.files[0]){
		document.getElementById("bannerPicture").value = docObj.value;
	}else{
		docObj.select();
		var imgSrc = document.selection.createRange().text;
		document.getElementById("bannerPicture").value = imgSrc;
	}
	return true;
}
</SCRIPT>
</@page.pageBase>