<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="大首页图片管理">
<style>
#insertOrUpdateMsg{
border: 0px solid #aaa;margin: 0px;position: fixed;top: 0;width: 100%;
background-color: #d1d1d1;display: none;height: 30px;z-index: 9999;font-size: 18px;color: red;
}
.btnCCC{
	background-image: url("../img/glyphicons-halflings-white.png");
	background-position: -288px 0;
}
</style>
<script>
$(function(){
	<#if e.id??>
	var id = "${e.id}";
	</#if>
});
</script>
	<form action="${basepath}/manage/topPicture" namespace="/manage" theme="simple" name="form" id="form" method="post">
		<table class="table table-bordered">
			<tr>
				<td colspan="2" style="text-align: center;">
					<#if e.id??>
                        <button method="update" class="btn btn-success">
                            <i class="icon-ok icon-white"></i> 保存
                        </button>
                        <#else>
                        <button method="insert" class="btn btn-success">
                            <i class="icon-ok icon-white"></i> 新增
                        </button>
					</#if>
				</td>
			</tr>
			<tr style="display: none;">
				<td>id</td>
				<td><input type="hidden" value="${e.id!""}" name="id" label="id" /></td>
			</tr>
			<tr>
				<th>图片地址</th>
				<td style="text-align: left;" colspan="3">
					<input type="button" name="filemanager" value="浏览图片" class="btn btn-warning"/>
					<input type="text"  value="${e.topPicture!""}" name="topPicture"  id="topPicture" ccc="imagesInput" style="width: 600px;" data-rule="图片地址:required;topPicture;" />
					<#if e.topPicture??>
						<a target="_blank" href="${systemSetting().imageRootPath}${e.topPicture!""}">
							<img style="max-width: 50px;max-height: 50px;" alt="" src="${systemSetting().imageRootPath}${e.topPicture!""}">
						</a>
					</#if>
				</td>
			</tr>
		</table>
	</form>
<script>
//删除图片主路径
function clearRootImagePath(picInput){
	var _pifeSpan = $("#pifeSpan").text();
	var _imgVal = picInput.val();
	picInput.val(_imgVal.substring(_imgVal.indexOf("/attached/")));
}

KindEditor.ready(function (K) {
        var editor = K.editor({
            fileManagerJson: '${systemSetting().staticSource}/kindeditor-4.1.7/jsp/file_manager_json.jsp',
            allowFileManager: true
        });
	
		//上传图片
        K('input[name=filemanager]').click(function () {
            var imagesInputObj = $(this).parent().children("input[ccc=imagesInput]");
            editor.loadPlugin('image', function () {
                editor.plugin.imageDialog({
                    viewType: 'VIEW',
                    dirName: 'image',
                    clickFn: function (url, title) {
                        imagesInputObj.val(url);
                        editor.hideDialog();
                        clearRootImagePath(imagesInputObj);//$("#picture"));
                    }
                });
            });
        });
	
});
</script>
</@page.pageBase>