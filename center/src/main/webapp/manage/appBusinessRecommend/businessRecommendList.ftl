<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="App商品管理">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
	<form action="${basepath}/app/businessRecommend/" method="post" theme="simple" id="form" name="form">
		<table class="table table-bordered">
			<tr>
			</tr>
			<tr>
				<td colspan="16">
				
					
					<a href="#myModal" role="button" class="btn" data-toggle="modal">添加推荐商品</a>
			

					<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
						<i class="icon-search icon-white"></i> 查询
					</button>
						
					
					
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
                        <#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>

		<table class="table table-bordered table-hover">
			<tr style="background-color: #dff0d8">
				<th width="20px"><input type="checkbox" id="firstCheckbox" /></th>
				<th width="120px">ID</th>
				<th>商品名称</th>
				<th>商品现价</th>
				<th>推荐商品</th>
				<th width="60px;">操作</th>
			</tr>

            <#list pager.list as item>
				<tr>
					<td><input type="checkbox" name="ids"
						value="${item.id!""}" /></td>
						<td style="display: none;">&nbsp;${item.id!""}</td>
					<td >${item.id!""}</td>
					<td>&nbsp;${item.name!""}</td>
					<td>&nbsp;${item.nowPrice!""}</td>
					<td>
					<#if item.type?? && item.type==1>
							&nbsp;推荐商品
			        <#elseif item.type?? && item.type==2> 
			                &nbsp;预售商品
						   </#if>
					</td>
					<td>
						<a href="deleteById?id=${item.id!""}">删除</a>
					</td>
				</tr>
            </#list>

			<tr>
				<td colspan="17" style="text-align: center;font-size: 12px;">
                    <#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
	</form>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" style="width: 70%;">
      <div class="modal-content" style="height:600px;">
         <div class="modal-header">
            <button type="button" class="close"
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">
             	商品列表
            </h4>
         </div>
         <div class="modal-body">
        	 <form action="" method="post" theme="simple" id="modalform" name="modalform">
            	<iframe id="iframeSon" name="iframeSon" src="${basepath}/app/businessRecommend/selectProductList?init=y" style="width: 100%;height:400px;">
            	</iframe>
             </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default"
               data-dismiss="modal">关闭
            </button>
            <button type="button" method="saveBusinessRecommend" id="tj" class="btn btn-primary" onClick="return saveplatRecommend(this,'1','确定提交选择的记录?');">
             	  添加推荐商品
            </button>
            <button type="button" method="saveBusinessRecommend" id="ys" class="btn btn-primary" onClick="return saveplatRecommend(this,'2','确定提交选择的记录?');">
             	  添加预售商品
            </button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
<script>
	function saveplatRecommend(obj,types,tip){
		console.log("submitIDs...");
		alert($(window.frames["iframeSon"].document).find("input:checked").size());
		var size = $(window.frames["iframeSon"].document).find("input:checked").size();
		if (size == 0) {
			alert("请先选择要操作的内容！");
			return false;
		} 

		var ids = "";
		for(var i=0;i<size;i++)
		{
		    if(ids=="")
		    {
		    	ids=$(window.frames["iframeSon"].document).find("input:checked").eq(i).val();
		    }
		    else
		    {
		    	ids=ids+","+$(window.frames["iframeSon"].document).find("input:checked").eq(i).val();
		    }
		}
		
		if(confirm(tip)){
			createMark();
			var modalform = $("#modalform");
			$("#modalform").attr("action","${basepath}/app/businessRecommend/"+$(obj).attr("method")+"?ids="+ids+"&type=${type!""}"+"&types="+types);
			$("#modalform").submit();
		}
		return false;
}
	
</script>
</@page.pageBase>