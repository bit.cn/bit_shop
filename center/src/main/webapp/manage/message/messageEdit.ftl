<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="消息管理">
<form action="${basepath}/manage/message/update"  theme="simple" id="form" name="form" method="post">
			<table class="table table-bordered">
				<tr style="background-color: #dff0d8">
					<td colspan="2" style="background-color: #dff0d8;text-align: center;">
						<strong>消息编辑</strong>
					</td>
				</tr>
				<tr style="display: none;">
					<td>id</td>
					<td><input type="hidden" name="id" value="${e.id!""}" id="id"></td>
				</tr>
				<tr>
					<td style="text-align: right;">消息标题</td>
					<td style="text-align: left;"><input type="text" name="title" value="${e.title!""}" id="title" /></td>
				</tr>
				<tr>
					<td style="text-align: right;">消息内容</td>
					<td style="text-align: left;"> 
					<textarea name="content" value="" id="content" style="width:500px;" rows="5">${e.content!""}</textarea>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">消息类型</td>
					<td>
				  <#assign map = {'1':'所有人','2':'会员'}>
                    <select id="messageType" name="messageType" class="search-query input-medium">
						<#list map?keys as key>
                            <option value="${key}" <#if e.messageType?? && (e.messageType+'')==key>selected="selected" </#if>>${map[key]}</option>
						</#list>
                    </select>
					</td>
				</tr>
				<tr id="accountId" style="display: none">
					<td style="text-align: right;">会员ID</td>
					<td style="text-align: left;"><input type="text" name="accountId" value="${e.accountId!""}" id="accountId" /></td>
				    </tr>
				<tr>
				<tr>
					<td colspan="2" style="text-align: center;">
						<#if e.id??>
							<a  class="btn btn-success" onclick="document.getElementById('form').submit()"
								<i class="icon-ok icon-white"></i> 保存
							</a>
							<#else>
								<button method="insert" class="btn btn-success" >
									<i class="icon-ok icon-white"></i> 新增
								</button>
						</#if>
					</td>
				</tr>
			</table>
	</form>

</@page.pageBase>