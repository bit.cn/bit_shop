<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="消息管理">
<script src="${systemSetting().staticSource}/js/jquery-1.9.1.min.js"></script>
<form action="${basepath}/manage/message/insert"  theme="simple" id="form" name="form" method="post">
			<table class="table table-bordered">
				<tr style="background-color: #dff0d8">
					<td colspan="2" style="background-color: #dff0d8;text-align: center;">
						<strong>消息编辑</strong>
					</td>
				</tr>
				<tr style="display: none;">
					<td>id</td>
					<td><input type="hidden" name="id" value="${e.id!""}" id="id"></td>
				</tr>
				<tr>
					<td style="text-align: right;">消息标题</td>
					<td style="text-align: left;"><input type="text" name="title" value="${e.title!""}" id="title" /></td>
				</tr>
				<tr>
					<td style="text-align: right;">消息内容</td>
					<td style="text-align: left;"><textarea name="content" value="${e.content!""}" id="content" style="width:500px;" rows="5"></textarea></td>
				</tr>
					
				<tr>
					<td style="text-align: right;">消息类型</td>
					<td>
				  <#assign map = {'1':'所有人','2':'会员'}>
                    <select id="messageType" name="messageType" class="search-query input-medium" onchange="func()">
						<#list map?keys as key>
                            <option value="${key}" <#if e.messageType?? && (e.messageType+'')==key>selected="selected" </#if>>${map[key]}</option>
						</#list>
                    </select>
					</td>
				</tr>
			<tr id="accountId" style="display: none">
					<td style="text-align: right;">会员账号</td>
					<td style="text-align: left;"><input type="text" name="accountId" value="" id="accountId" /></td>
				    </tr>
				<tr>
					<td colspan="2" style="text-align: center;">
						
							<a  class="btn btn-success" onclick="document.getElementById('form').submit()"
								<i class="icon-ok icon-white"></i> 新增
							</a>
					
					</td>
				</tr>
			</table>
	</form>
<script type="text/javascript">
function func(){
 var messageType=$("#messageType").val();
	if(messageType==1){
	$("#accountId").hide();
	}
	else{
		$("#accountId").show();

	}
	
}
</script>
</@page.pageBase>