<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="字典管理">
<div style="text-align: center; border: 0px solid #999;margin: auto;">
	<div style="text-align: center; border: 0px solid #999;
		margin: auto;">
		<form action="${basepath}/manage/dict" theme="simple" method="post">
		        <input type="hidden" name="dic_id" id="dic_id" value="${e.dic_id!""}"/>
				<table class="table table-bordered table-condensed">
					<tr>
						<th style="text-align: right;">字典名称:</th>
						<td><input type="text" value="${e.dic_name!""}" name="dic_name"/></td>
						<th style="text-align: right;">字典代码:</th>
						<td><input type="text" value="${e.dic_code!""}" name="dic_code" data-rule="字典代码:required;code;length[1~25]"/></td>
					</tr>
					<tr>
						<td colspan="4">
						<table class="table table-bordered" >
							<tr class="warning" style="text-align: left;">
								<td colspan="2">
									<div>
										<font color="red">在父菜单下的所有子菜单全部勾选的情况下，是否级联删除父菜单：<input type="checkbox" id="deleteParent"></font><br>
										提示：点击菜单项，此处则能编辑该菜单项或增加顶级菜单或子菜单项。<br> 
										<#if e.dic_id??>
					                        <button method="update" class="btn btn-success">
					                            <i class="icon-ok icon-white"></i> 保存
					                        </button>
					                        <#else>
					                        <button method="insert" class="btn btn-success">
					                            <i class="icon-ok icon-white"></i> 新增
					                        </button>
										</#if>
										<input type="button" id="deleteMenus" value="删除选择的菜单" class="btn btn-danger"/>
										<font color="red">(默认只删除叶子菜单)</font>
										[<a id="expandOrCollapseAllBtn" href="#" title="展开/折叠全部资源" onclick="return false;">展开/折叠</a>]
										<span onclick="addDictItem()"><input type="button" id="addDict" value="+字典项" class="btn btn-danger"/></span>
									</div>
								</td>
							</tr>
							<tr>
								<td style="width:20%;">
									<div>
										<div id="loadImg" style="text-align: center;">
											<img alt="菜单加载中......" src="${systemSetting().staticSource}/images/loader.gif">资源加载中...
										</div>			
										<ul id="dictTree" style="display: none;" class="ztree"></ul>
									</div>
								</td>
								<td>
									<iframe src="" width="100%" id="iframeMenuEdit" height="800">
										点击菜单项，此处则能编辑该菜单项或增加顶级菜单或子菜单项。
									</iframe>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
		</form>
	</div>
</div>
<script>
$(function()
{
	var setting = 
	{
			check: {
				enable: true,
				title: "item_name",
                name:"item_name",
				dblClickExpand: false
			},callback: {
				onClick: function(e,treeId, treeNode) {
	                var zTree = $.fn.zTree.getZTreeObj("dictTree");
	                var selectedNode = treeObj.getSelectedNodes()[0];  
	                zTree.expandNode(treeNode);
	            },
				onMouseDown: function (event, treeId, treeNode) {
					if(treeNode!=null)
					{
						 var url = "toAddOrUpdate?id="+treeNode.id;
		                if(true){
		                    $("#iframeMenuEdit").attr("src",url);
		                    return;
		                }
		                $("#dfsfsf").val(treeNode.id);
		                document.form1.action = url;
		                document.form1.submit();
					}
	            }
			},
	        data:{
	            key:{
	                url:"_url"
	            }
	        }
	};
	
//字典树的新增与更新判断	
<#if e.dic_id??>
loadMenusTree();
<#else>
$("#loadImg").hide();
$("#dictTree").html("<li id=\"treeDemo2_2\" class=\"level0\" tabindex=\"0\" hidefocus=\"true\" treenode=\"\">"+
"<a id=\"treeDemo2_2_a\" class=\"level0\" treenode_a=\"\"  target=\"rightFrame\" style=\"\" title=\"字典管理\">"+
"<span id=\"treeDemo2_2_ico\" title=\"\" treenode_ico=\"\" class=\"button ico_close\" style=\"\"></span>"+
"<span id=\"treeDemo2_2_span\">字典项</span>"+
"</a></li>");
$("#dictTree").show();
</#if>

//加载菜单树
function loadMenusTree(){
	$.ajax({
		url:"${basepath}/manage/dict/getDictItemsByDictId?dictId=${e.dic_id!""}",
					type:"post",
					dataType:"text",
					success:function(data, textStatus){
						var zNodes = eval('('+data+')');
						if(zNodes.length==0)
						{
							$("#loadImg").hide();
							$("#dictTree").html("<li id=\"treeDemo2_2\" class=\"level0\" tabindex=\"0\" hidefocus=\"true\" treenode=\"\">"+
							"<a id=\"treeDemo2_2_a\" class=\"level0\" treenode_a=\"\"  target=\"rightFrame\" style=\"\" title=\"字典管理\">"+
							"<span id=\"treeDemo2_2_ico\" title=\"\" treenode_ico=\"\" class=\"button ico_close\" style=\"\"></span>"+
							"<span id=\"treeDemo2_2_span\">字典项</span>"+
							"</a></li>");
							$("#dictTree").show();
						}
						else
						{
							setting.data.key.title="item_name";
							setting.data.key.name="item_name";
							$.fn.zTree.init($("#dictTree"), setting, zNodes);
							$("#loadImg").hide();
							$("#dictTree").show();
						}
					},
					error:function(){
						alert("error");
					}
				});
			}
			
			//删除栏目
			$("#deleteMenus").click(function(){
				
				if(!confirm("确定删除选择的菜单项?")){
					return false;
				}
// 				alert("deleteMenus...");
				var ids = "";
				var treeObj = $.fn.zTree.getZTreeObj("dictTree");
				var nodes = treeObj.getCheckedNodes(true);
				if(nodes.length==0){
					return false;
				}
				for(var i=0;i<nodes.length;i++){
// 					alert(nodes[i].id);
					ids+=nodes[i].id+",";
				}
				
				$.ajax({
					url:"${basepath}/manage/dict/delete",
					type:"post",
					data:{ids:ids,deleteParent:$("#deleteParent").attr("checked")?"1":"-1"},
					dataType:"text",
					success:function(data){
// 						var zNodes = eval('('+data+')');
// 						$.fn.zTree.init($("#dictTree"), setting, zNodes);
						if(data==1){
							loadMenusTree();
						}else{
							alert("删除菜单失败！");
						}
					},
					error:function(){
						alert("删除菜单失败！");
					}
				});
			});
			
			
			//点击菜单项
			var expandAllFlg = true;
			function expandNode(e) {
				var zTree = $.fn.zTree.getZTreeObj("dictTree"),
				type = e.data.type,
				nodes = zTree.getSelectedNodes();

				if (type == "expandAll") {
					zTree.expandAll(true);
				} else if (type == "collapseAll") {
					zTree.expandAll(false);
				} else if (type == "expandOrCollapse") {
					zTree.expandAll(expandAllFlg);
					expandAllFlg = !expandAllFlg;
				} else {
					if (type.indexOf("All")<0 && nodes.length == 0) {
						alert("请先选择一个父节点");
					}
					var callbackFlag = $("#callbackTrigger").attr("checked");
					for (var i=0, l=nodes.length; i<l; i++) {
						zTree.setting.view.fontCss = {};
						if (type == "expand") {
							zTree.expandNode(nodes[i], true, null, null, callbackFlag);
						} else if (type == "collapse") {
							zTree.expandNode(nodes[i], false, null, null, callbackFlag);
						} else if (type == "toggle") {
							zTree.expandNode(nodes[i], null, null, null, callbackFlag);
						} else if (type == "expandSon") {
							zTree.expandNode(nodes[i], true, true, null, callbackFlag);
						} else if (type == "collapseSon") {
							zTree.expandNode(nodes[i], false, true, null, callbackFlag);
						}
					}
				}
			}
			$("#expandOrCollapseAllBtn").bind("click", {type:"expandOrCollapse"}, expandNode);
		});
		
		//添加字典项
		function addDictItem(){
			 var url = "toAddOrUpdate";
		     $("#iframeMenuEdit").attr("src",url);
		}
</script>
</@page.pageBase>