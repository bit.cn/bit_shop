<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="字典管理">
<script>
	$(function(){
        var table = $('#dataTables-example').DataTable({
            "ajax": {
				url:"loadData",
				dataSrc:"list"
            },
			columns:[
                {name:"dic_id", "orderable": false, title:'<input type="checkbox" id="firstCheckbox"/>', data:"dic_id",render:function ( data, type, row, meta ) {
                    // 'sort', 'type' and undefined all just use the integer
                    return '<input type="checkbox" name="ids" value="'+data+'"/>';
                }},
				{name:"dic_name", title:"字典名称", data:"dic_name"},
				{name:"dic_code", title:"字典代码", data:"dic_code"},
                {name:"oper", title:"操作", data:"dic_id",render: function (data, type, row, meta) {
                    return '<a href="${basepath}/manage/dict/toEdit?id=' + data + '">编辑</a><a href="${basepath}/manage/dict/deleteById?id=' + data + '">删除</a>';
                }}
			]
        });
	});
</script>
<form action="${basepath}/manage/user" method="post">
	<table class="table table-bordered table-condensed">
		<tr>
			<td style="text-align: right;">状态</td>
			<td style="text-align: left;" >
                <select name="status" id="status" class="input-small">
                    <option value="">全部</option>
                    <option value="y">启用</option>
                    <option value="n">禁用</option>
                </select>
			</td>
		</tr>
		<tr>
			<td colspan="11">
            <#if checkPrivilege("/manage/user/search") >
					<button method="selectList" id="btnSearch" class="btn btn-primary" table-id="dataTables-example" onclick="return selectList(this)">
						<i class="icon-search icon-white"></i> 查询
					</button>
             </#if>
                <a href="${basepath}/manage/dict/toAdd" class="btn btn-success"><i class="icon-plus-sign icon-white"></i> 添加</a>
				<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
                   <#include "/manage/system/pager.ftl"/>
				</div>

			</td>
		</tr>
	</table>
    <table class="table table-bordered table-hover" id="dataTables-example">
    </table>
        </td>
		</tr>
	</table>
</form>
</@page.pageBase>