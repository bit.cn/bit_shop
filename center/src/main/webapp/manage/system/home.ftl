<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="首页">
<style>
    /*a:link {text-decoration:underline;}*/
    /*a:visited {text-decoration:underline;}*/
    /*a:hover {text-decoration:underline;}*/
    /*a:active {text-decoration:underline;}*/

    .font-focus{
        font-weight: 700;font-size: 16px;color: #f50 !important;text-decoration: underline;
    }
</style>
<script>
    $(function() {
        $( "#tabs" ).tabs({
            //event: "mouseover"
        });
    });
</script>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-2" style="font-size: 14px;">基本设置</a></li>
                <li><a href="#tabs-3" style="font-size: 14px;">图片设置</a></li>
            </ul>
            <div id="tabs-2">
                <table class="table table-condensed">
                    <tr>
                        <td style="text-align: right;">系统版本:</td>
                        <td style="text-align: left;">${systemSetting().getVersion() }</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">系统代号:</td>
                        <td style="text-align: left;">${systemSetting().getSystemCode() }</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">名称:</td>
                        <td style="text-align: left;">${systemSetting().getName() }</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">简介:</td>
                        <td style="text-align: left;">${systemSetting().getWww() }</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">log:</td>
                        <td style="text-align: left;">${systemSetting().getLog() }</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">网站标题:</td>
                        <td style="text-align: left;">${systemSetting().getTitle() }</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">description:</td>
                        <td style="text-align: left;">
                        ${systemSetting().getDescription() }
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">keywords:</td>
                        <td style="text-align: left;">
                        ${systemSetting().getKeywords() }
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">shortcuticon:</td>
                        <td style="text-align: left;">
                        ${systemSetting().getShortcuticon() }
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">联系地址:</td>
                        <td style="text-align: left;">
                        ${systemSetting().getAddress() }
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">联系电话:</td>
                        <td style="text-align: left;">
                        ${systemSetting().getTel() }
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">邮箱:</td>
                        <td style="text-align: left;">
                        ${systemSetting().getEmail() }
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">备案号:</td>
                        <td style="text-align: left;">
                        ${systemSetting().getIcp() }
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">是否开放网站:</td>
                        <td style="text-align: left;">
                            <input type="checkbox" disabled="disabled"  checked="${systemSetting().getIsopen() }" value="${systemSetting().getIsopen() }"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">关闭信息:</td>
                        <td style="text-align: left;">
                        ${systemSetting().getCloseMsg() }
                        </td>
                    </tr>
                </table>
            </div>
            <div id="tabs-3">
                <table class="table table-condensed">
                    <tr>
                        <td style="text-align: right;">图片根路径</td>
                        <td style="text-align: left;" >${systemSetting().getImageRootPath() }</td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- tab end -->
</@page.pageBase>