drop table if exists product;

/*==============================================================*/
/* Table: product                                               */
/*==============================================================*/
create table product
(
   id                   int(11) not null comment '主键ID',
   goods_id             int(11) comment '货品ID',
   product_code         varchar(255) comment '商品编码',
   product_name         varchar(200) comment '商品名称',
   introduce            varchar(500) comment '商品简介',
   hit                  int(11) comment '点击量',
   picture              varchar(200) comment '商品列表小图片地址',
   max_picture          varchar(200) comment '大图片地址',
   price                decimal(10,2) comment '原价',
   now_price            decimal(10,2) comment ' 促销价 当没有规格的时候商品的价格',
   sell_count           int(11) comment '销量',
   stock                int(11) default 0 comment '库存',
   status               varchar(100) comment '商品状态(新增，已上架，已下架)',
   product_restrictions int(11) comment '商品限购数量',
   catalog_id           int(11) comment '分类ID',
   brand_id             int(11) comment '商品品牌ID',
   business_id          int(11) comment '商家ID',
   shelves_time         timestamp comment '上架时间',
   shelf_time           timestamp comment '下架时间',
   sell_time            timestamp comment '商品购买开始时间或者预售开始时间',
   sell_end_time        timestamp comment '商品购买结束时间或预售结束时间',
   replenishment_time   timestamp comment '商品补款开始时间',
   replenishment_end_time timestamp comment '商品补款结束时间',
   deposit_price        decimal(10,2) comment '预售价格',
   balance_price        decimal(10,2) comment '尾款价格',
   search_key           varchar(100) comment '搜索关键字',
   saleat_date          date comment '商品发售日期',
   product_weight       decimal(10,2) comment '商品称重',
   unit                 varchar(32) comment '商品单位',
   product_type         varchar(30) comment '商品类型',
   express_id           int(11) comment '商品发货仓库ID',
   service_id           int(11) comment '商品售后服务ID',
   product_total        int(11) comment '商品库存总量',
   create_id            varchar(100) comment '创建者ID',
   update_id            varchar(100) comment '更新者ID',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table product comment '产品表';

alter table product add constraint FK_Reference_1 foreign key (goods_id)
      references t_goods (id) on delete restrict on update restrict;
      
      
drop table if exists product_extend;

/*==============================================================*/
/* Table: product_extend                                        */
/*==============================================================*/
create table product_extend
(
   id                   int(11) not null comment '主键ID',
   product_id           int(11) comment '商品ID',
   is_new               varchar(20) comment '是否新品',
   is_sale              varchar(20) comment '是否特价',
   is_hot               varchar(20) comment '是否热销产品',
   is_evaluate          varchar(20) comment '是否可以评论',
   is_time_promotion    varchar(20) comment '是否限时促销',
   product_title        varchar(100) comment '商品页面标题',
   auxiliary_title      varchar(100) comment '商品辅助标题',
   primary key (id)
);

alter table product_extend comment '商品的扩展属性表';

alter table product_extend add constraint FK_Reference_2 foreign key (product_id)
      references product (id) on delete restrict on update restrict;
      
drop table if exists product_extend;

/*==============================================================*/
/* Table: product_extend                                        */
/*==============================================================*/
create table product_extend
(
   id                   int(11) not null comment '主键ID',
   product_id           int(11) comment '商品ID',
   is_new               varchar(20) comment '是否新品',
   is_sale              varchar(20) comment '是否特价',
   is_hot               varchar(20) comment '是否热销产品',
   is_evaluate          varchar(20) comment '是否可以评论',
   is_time_promotion    varchar(20) comment '是否限时促销',
   product_title        varchar(100) comment '商品页面标题',
   auxiliary_title      varchar(100) comment '商品辅助标题',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table product_extend comment '商品的扩展属性表';

alter table product_extend add constraint FK_Reference_2 foreign key (product_id)
      references product (id) on delete restrict on update restrict;
drop table if exists product_category;

/*==============================================================*/
/* Table: product_category                                      */
/*==============================================================*/
create table product_category
(
   id                   int(11) not null comment '主键ID',
   parent_id            int4 comment '父类型ID',
   category_name        varchar(100) comment '类别名称',
   category_code        varchar(100) comment '类别code',
   category_sequence    int(11) comment '序列号',
   show_in_nav          varchar(10) comment '是否在导航显示(是否显示在首页的导航条上(y:显示,n:不显示)',
   category_image       varchar(200) comment '分类图片',
   is_deleted           varchar(10) comment '是否已经删除',
   category_font_css    varchar(100) comment '分类文字样式',
   summary              varchar(100) comment '类别简介',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table product_category comment '商品类别表';

drop table if exists product_attribute;

/*==============================================================*/
/* Table: product_attribute                                     */
/*==============================================================*/
create table product_attribute
(
   id                   int(11) not null comment '主键Id',
   product_id           int(11) comment '商品ID',
   attribute_options_id int(11) comment '属性选项ID',
   attribute_name       varchar(100) comment '属性名称',
   attribute_options_value varchar(100) comment '属性选项值',
   attribute_options_name char(10) comment '属性选项名称',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table product_attribute comment '商品属性';

alter table product_attribute add constraint FK_Reference_5 foreign key (attribute_options_id)
      references attribute_options (id) on delete restrict on update restrict;

alter table product_attribute add constraint FK_Reference_6 foreign key (product_id)
      references product (id) on delete restrict on update restrict;

drop table if exists attribute_options;

/*==============================================================*/
/* Table: attribute_options                                     */
/*==============================================================*/
create table attribute_options
(
   id                   int(11) not null comment '主键ID',
   attribute_id         int(11) comment '属性ID',
   attribute_options_value varchar(500) comment '属性选项值(对于无属性选项的直接录入自定义属性值)',
   attribute_options_name varchar(100) comment '属性选项名称',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table attribute_options comment '属性选项';

alter table attribute_options add constraint FK_Reference_4 foreign key (attribute_id)
      references attribute (id) on delete restrict on update restrict;

drop table if exists attribute;

/*==============================================================*/
/* Table: attribute                                             */
/*==============================================================*/
create table attribute
(
   id                   int(11) not null comment '主键id',
   attribute_name       varchar(100) comment '属性名称',
   attribute_type       varchar(30) comment '属性类别(有选项，自定义)',
   category_id          int(11) comment '类别ID',
   parent_id            int(11) comment '父级属性id',
   attribute_sequence   int(11) comment '属性序列号',
   is_show              varchar(10) comment '是否显示',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table attribute comment '属性';


drop table if exists product_spec;

/*==============================================================*/
/* Table: product_spec                                          */
/*==============================================================*/
create table product_spec
(
   id                   int(11) not null comment '主键ID',
   product_id           int(11) comment '商品ID',
   spec_options_id      int(11) comment '规格选项ID',
   spec_name            varchar(100) comment '规格名称',
   spec_options_value   varchar(100) comment '规格选项值',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table product_spec comment '商品规格信息';

alter table product_spec add constraint FK_Reference_11 foreign key (spec_options_id)
      references spec_options (id) on delete restrict on update restrict;

alter table product_spec add constraint FK_Reference_12 foreign key (product_id)
      references product (id) on delete restrict on update restrict;

drop table if exists spec;

/*==============================================================*/
/* Table: spec                                                  */
/*==============================================================*/
create table spec
(
   id                   int(11) not null comment '主键ID',
   spec_name            varchar(100) comment '规格名称',
   spec_code            varchar(30) comment '规格code',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table spec comment '规格';


drop table if exists spec_options;

/*==============================================================*/
/* Table: spec_options                                          */
/*==============================================================*/
create table spec_options
(
   id                   int(11) not null comment '主键ID',
   spec_id              int(11) comment '规格ID',
   spec_options_value   varchar(100) comment '规格选项值',
   spec_options_name    varchar(100) comment '规格选项名称',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table spec_options comment '规格选项';

alter table spec_options add constraint FK_Reference_8 foreign key (spec_id)
      references spec (id) on delete restrict on update restrict;

drop table if exists goods;

/*==============================================================*/
/* Table: goods                                                 */
/*==============================================================*/
create table goods
(
   id                   int(11) not null comment '主键ID',
   goods_name           varchar(100) comment '货品名称',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);



drop table if exists goods_spec;

/*==============================================================*/
/* Table: goods_spec                                            */
/*==============================================================*/
create table goods_spec
(
   id                   int(11) not null comment '主键ID',
   goods_id             int(11) comment '货品ID',
   spec_id              int(11) comment '规格ID',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table goods_spec comment '货品规格';

alter table goods_spec add constraint FK_Reference_10 foreign key (spec_id)
      references spec (id) on delete restrict on update restrict;

alter table goods_spec add constraint FK_Reference_9 foreign key (goods_id)
      references t_goods (id) on delete restrict on update restrict;

drop table if exists brand;

/*==============================================================*/
/* Table: brand                                                 */
/*==============================================================*/
create table brand
(
   id                   int(11) not null comment '主键ID',
   brand_name           varchar(300) comment '品牌名称',
   brand_introduce      text comment '品牌简介',
   brand_logo           varchar(200) comment '品牌logo',
   is_show              varchar(10) comment '是否显示',
   is_deleted           varchar(10) comment '是否已经删除',
   brand_sequence       int(11) comment '属性序列号',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table brand comment '品牌';
