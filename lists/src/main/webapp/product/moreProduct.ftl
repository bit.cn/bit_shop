<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top.ftl" as shopTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
	<head>
		<@htmlBase.htmlBase>
		<title>更多商品</title>
		<link rel="stylesheet" href="${systemSetting().staticSource}css/base.css" type="text/css">
		<link rel="stylesheet" href="${systemSetting().staticSource}css/docs.css" type="text/css">
		<link rel="stylesheet" href="${systemSetting().staticSource}css/css.css" />
		<link rel="stylesheet" href="${systemSetting().staticSource}css/common/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="${systemSetting().staticSource}resource/validator-0.7.0/jquery.validator.css">
		<link href="${systemSetting().staticSource}css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
			<style type="text/css">
			img {
				border: 0px;
			}
			
			.thumbnail_css {
				border-color: red;
			}
			
			.attr_css {
				font-size: 100%;
				float: left;
			}
			
			.left_product {
				font-size: 12px;
				max-height: 35px;
				overflow: hidden;
				text-overflow: ellipsis;
				-o-text-overflow: ellipsis;
			}
			
			.lazy {
				display: none;
			}
			.centerImageCss {
				width: 289px;
				height: 190px;
			}
			
			.title {
				display: block;
				width: 280px;
				overflow: hidden;
				/*注意不要写在最后了*/
				white-space: nowrap;
				-o-text-overflow: ellipsis;
				text-overflow: ellipsis;
			}
			
			body {
				padding-top: 0px;
				padding-bottom: 0px;
				font-size: 12px;
				/*    	font-family: 微软雅黑, Verdana, sans-serif, 宋体; */
			}
			.hotSearch {
				cursor: pointer;
			}
		</style>
	</head>
	<!--商城主页top页-->
	<@shopTop.shopTop/>
		<script type="javascript">
			//搜索商品 function search(){ var _key = $.trim($("#key").val()); if(_key==''){ return false; } $("#searchForm").submit(); }
		</script>
		<div class="container" style="width:1200px;">
			<div class="row">
				<div class="col-xs-3">
					<#include "/catalog_superMenu.ftl">
						</br>
					<#include "/product/productlist_left_picScroll.ftl">
				</div>

				<div class="col-xs-9">
					<!-- 导航栏 -->
				<div class="row">
					<#if e.mainCatalogName??>
						<div style="border: 0px solid;text-align: left;">
							<div>
								<ol class="breadcrumb" style="margin-bottom: 0px;">
								  <li class="active">
								  	<a href="${basepath}/catalog/${mainCatalogCode}.html">
								  		${e.mainCatalogName!""}
									</a>
								  </li>
								  <#if e.childrenCatalogName??>
									  <li class="active"><a href="#">${e.childrenCatalogName!""}</a></li>
								  </#if>
								</ol>
							</div>
						</div>
					</#if>
				</div>
				
					<div class="row">
					<!-- 商品展示 -->
						<#if productList1?? && productList1?size gt 0>
						<#list productList1 as item>
						<div class="col-xs-3" style="padding: 5px;text-align: center;">
							<div class="thumbnail thumbnail_css" style="width: 100%; display: block;">
								<div style="height: 150px;border: 0px solid;position: relative;">
									<a href="${systemSetting().item}/product/${item.id!""}.html" target="_blank">
										<img class="lazy" style="border: 0px;display: block;margin: auto;
										max-height: 100%;max-width: 100;" border="0" 
										src="${systemSetting().imageRootPath}${item.picture!""}" 
										data-original="${systemSetting().imageRootPath}${item.picture!""}">
									</a>
								</div>
								<div style="height: 40px;">
									<div class="col-xs-12 left_product">
										<div class="row">
											<a style="cursor: pointer;" 
											href="${systemSetting().item}/product/${item.id!""}.html" target="_blank" target="_blank" 
											target="_blank" title="${item.name!""}">
												${item.name!""}
											</a>
										</div>
										<div class="row">
											<a style="cursor: pointer;" href="http://112.124.52.129:9990/jshop/product/10331.html" target="_blank" title="粘土人 缠流子">
												李勇的店铺
											</a>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<b style="font-weight: bold;color: #cc0000;">
											￥${item.nowPrice!""}
										</b>
									</div>
								</div>
								</div>
							</#list>
							</div>
							<#else>
                                                                                    抱歉，没有找到<font color='#f40'>${key!""}</font>相关的宝贝!
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="font-size: 14px;font-weight: normal;">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="font-size: 16px;font-weight: normal;text-align: center;">
                                                <div class="panel-body" style="font-size: 16px;font-weight: normal;">
                                                    <span class="glyphicon glyphicon-ok"></span>
                                                    <span class="text-success">您可以尝试换一个关键词或者换一个分类。</span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
						</#if>
						</div>
						<div class="row" style="margin-top: 10px;">
							<div class="col-xs-12" style="border: 0px solid;text-align: right;">
								<#if productList1??>
									<#include "/pager.ftl"/>
								</#if>
							</div>
						</div>
					 </div>
				   </div>
				</div>
			</div>
	<!-- 友情链接 -->
	<!--
	<div class="ani_product clearfix">
		<hr style="margin: 0px;">
			<div class="row" >
				<div class="col-xs-12" style="text-align: center;">
						<div style="text-align: center;margin: auto;">
							<#if systemManager().navigations??>
							    <#list systemManager().navigations as item>
	                                <div style="float: left;margin: 5px;">
	                                    <a href="http://${item.http!""}" target="_blank">${item.name!""}</a>
	                                </div>
							    </#list>
							</#if>
						</div>				
				</div>
			</div>
	    </div>
	    </div>
	    </div>
	    -->
	    </div>
		<!-- 商城主页footer页 -->
		<@shopFooter.shopFooter/>
		<script src="${systemSetting().staticSource}/js/common/z_stat.php" type="text/javascript"></script>
		<script src="${systemSetting().staticSource}/js/common/core.php" charset="utf-8" type="text/javascript"></script>
	    <script type="text/javascript" src="${systemSetting().staticSource}/js/new.js"></script>
	    <script src="${systemSetting().staticSource}/js/common/jquery.SuperSlide.js"></script>
		<script src="${systemSetting().staticSource}/js/index.js"></script>
		<script type="text/javascript">
			$(function() {
				//商品鼠标移动效果
				$("div[class=thumbnail]").hover(function() {
					$(this).addClass("thumbnail_css");
				}, function() {
					$(this).removeClass("thumbnail_css");
				});
				$(".col-xs-3 img").addClass("carousel-inner img-responsive img-rounded");
			});
			jQuery(".picScroll-top").slide({
				titCell: ".hd ul",
				mainCell: ".bd ul",
				autoPage: true,
				effect: "top",
				autoPlay: true,
				scroll: 2,
				vis: 5
			});
			jQuery(".slideTxtBox").slide();
			function defaultProductImg() {
				var img = event.srcElement;
				img.src = "http://112.124.52.129:9990/jshop";
				img.onerror = null; //控制不要一直跳动 
			}
		</script>
<!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>
</@htmlBase.htmlBase>
</body>
</html>