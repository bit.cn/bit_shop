<#import "/manage/tpl/sellerPageBase.ftl" as sellerPage>
<#import "/product/common/ftl/shopBasePage.ftl" as shopBasePage>
<#import "/product/common/ftl/shopTop.ftl" as shopTop>
<#import "/product/common/ftl/shopHeadbg.ftl" as shopHeadbg>
<#import "/product/common/ftl/shopChannel.ftl" as shopChannel>
<#import "/product/common/ftl/shopFooter.ftl" as shopFooter>
<#import "/product/detailMain.ftl" as detailMain>
<@shopBasePage.shopBasePage>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${systemSetting().staticSource}/shopindex/css/index.css" rel="stylesheet" type="text/css">
<link href="${systemSetting().staticSource}/product/css/detail.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="${systemSetting().shortcuticon}">
<style type="text/css">
.topCss {
	height: 28px;
	line-height: 28px;
	background-color: #f8f8f8;
	border-bottom: 1px solid #E6E6E6;
	padding-left: 9px;
	font-size: 14px;
	font-weight: bold;
	position: relative;
	margin-top: 0px;
}
.left_product{
	font-size: 12px;display: inline-block;overflow: hidden;text-overflow: ellipsis;-o-text-overflow: ellipsis;white-space: nowrap;max-width: 150px;
}
img.err-product {
background: url(${systemSetting().defaultProductImg}) no-repeat 50% 50%;
}

.nowPrice{
	color: #F00;
	font-family: "微软雅黑";
	font-size: 20px;
}

.spec li{
	float: left;
	position: relative;
	margin: 0 4px 4px 0;
	line-height: 20px;
	vertical-align: middle;
	padding: 1px;
	border: 1px solid #ccc;
	cursor: pointer;
	min-width: 50px;
    min-height: 10px;
    text-align: center;
}

.specSelectCss{
	border: 2px solid #ff0000;
	color:red;
}

.specNotAllowed{
	color: #CDCDCD;
	cursor: not-allowed;
}

.lazy {
  display: none;
}
</style>
<script>
function defaultProductImg(){
	if(1==1){
		return;
	}
	var img=event.srcElement; 
	img.src="${systemSetting().defaultProductImg}";
	img.onerror=null; //控制不要一直跳动 
}
</script>
<script type="text/javascript" src="${systemSetting().staticSource}/product/common/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/resource/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/product/common/js/detail.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/product/common/js/js1.js"></script>

<title>商城主页</title>
</head>
<body>
<div class="backtop"><a href="javasctipt:scroll(0,0);">
<img src="${systemSetting().staticSource}/shopindex/common/image/top.jpg" width="45" height="45"></a></div>
<!--商城主页top页-->
<@shopTop.shopTop/>
<!--搜索站-->
<@shopHeadbg.shopHeadbg/>
<!--导航-->
<@shopChannel.shopChannel/>
<div>
	<!-- 商品详情主页 -->
	<@detailMain.detailMain/>
</div>
<!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>
<!-- Modal 加入购物车的弹出层 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><b>提示信息:</b></h4>
      </div>
      <div class="modal-body" style="color: #7ABD54;font:normal 24px">
        <h3><span class="glyphicon glyphicon-ok"></span>&nbsp;商品已成功加入购物车！</h3>
      </div>
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" onclick="javascript:$('#myModal').modal('hide');">继续购物</button>
        	<a id="addProductToCartErrorTips" href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="购买的商品超出库存数！"></a>
        	<button class="btn btn-primary" data-dismiss="modal" value="去购物车结算" onclick="toCart();">
        		<span class="glyphicon glyphicon-usd"></span>去购物车结算
        	</button>
      </div>
    </div>
  </div>
</div>
</body>
	<input type="hidden" value="${e.name!""}" id="productName">
	<input type="hidden" value="${e.id!""}" id="productID">
	<input type="hidden" value="${e.nowPrice!""}" id="nowPriceHidden">
	<input type="hidden" value="${e.stock!""}" id="stockHidden">
	<input type="hidden" id="specIdHidden">
	
<script type="text/javascript" src="${systemSetting().staticSource}/resource/bootstrap/js/bootstrap.min.js"></script>
<script src="${systemSetting().staticSource}/resource/js/product.js"></script>
<script src="${systemSetting().staticSource}/resource/js/front.js"></script>
<script src="${systemSetting().staticSource}/resource/js/superSlide/jquery.SuperSlide.js"></script>
<script src="${systemSetting().staticSource}/resource/js/jquery.imagezoom/js/jquery.imagezoom.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(document).ready(function(){
	$("#thumblist li a").click(function(){
		$(this).parents("li").addClass("tb-selected").siblings().removeClass("tb-selected");
		$(".jqzoom").attr('src',$(this).find("img").attr("mid"));
		$(".jqzoom").attr('rel',$(this).find("img").attr("big"));
	});
});
});
</script>
<script>
$(function() {
	$(".backtop").click(function(){
		$('html,body').animate({scrollTop: '0px'}, 800);
	});
	$("#addToCartBtn").removeAttr("disabled");
	$("#addToFavoriteBtn").removeAttr("disabled");
	
	jQuery(".slideTxtBox").slide();
	var ww = $("#productMainDiv").width();
	console.log("aww="+ww);
	$("#mainBox00").css("width",ww+"px");
	$("#mainBox00").find("img[name=box_img]").css("max-width",ww+"px");
	
	var specJsonStringVal = '${e.specJsonString!""}';
	
	//如果规格存在
	if(specJsonStringVal && specJsonStringVal.length>0){
		console.log("specJsonStringVal = " + specJsonStringVal);
		var specJsonStringObject = eval('('+specJsonStringVal+')');
		
		for(var i=0;i<specJsonStringObject.length;i++){
			console.log("specJsonStringObject = " + specJsonStringObject[i].specColor);
		}

		//规格被点击，则标记选中和不选中
		$("#specDiv li").click(function(){
			console.log("规格被点击。" + $(this).hasClass("specSelectCss"));
			document.getElementById("selectSpec").value=1;
			if($(this).hasClass("specNotAllowed")){
				console.log("由于规格被禁用了，直接返回。");
				return;
			}
			
			$(this).parent().find("li").not(this).each(function(){
				$(this).removeClass("specSelectCss");
				$(this).attr("disabled","disabled");
			});
			if($(this).is(".specSelectCss")){
				console.log("removeClass specSelectCss");
				$(this).removeClass("specSelectCss");
				
				//如果当前点击的是尺寸，则释放所有的颜色的禁用状态；如果点击的是颜色，则释放所有的尺寸禁用状态
				if($(this).parent().attr("id")=="specSize"){
					console.log("当前点击的是尺寸。");
					//释放所有颜色的鼠标禁用状态
					$("#specColor li").each(function(){
						$(this).removeClass("specNotAllowed");
					});
				}else if($(this).parent().attr("id")=="specColor"){
					console.log("当前点击的是颜色。");
					//释放所有颜色的鼠标禁用状态
					$("#specSize li").each(function(){
						$(this).removeClass("specNotAllowed");
					});
				}else{
					console.log("当前点击的东东不明确。");
				}
			}else{
				console.log("addClass specSelectCss");
				$(this).addClass("specSelectCss");
			}
			
			var parentID = $(this).parent().attr("id");
			console.log("parentID = " + parentID);
			
			if($("#specSize li").hasClass("specSelectCss") && $("#specColor li").hasClass("specSelectCss")){
				console.log("都选中了。");
				
				console.log("选中的文本："+$("#specSize .specSelectCss").html());
				//找出对应的规格
				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					if(specItem.specSize==$("#specSize .specSelectCss").html() 
							&& specItem.specColor==$("#specColor .specSelectCss").html()){
						console.log("找到了规格对象。");
						//改变商品的价格和库存数
						$("#nowPrice").text(specItem.specPrice);
						$("#stock_span_id").text(specItem.specStock);
						$("#specIdHidden").val(specItem.id);
						console.log("选中的规格ID="+$("#specIdHidden").val());
						break;
					}
				}
				//specNotAllowed
			}else if($("#specSize li").hasClass("specSelectCss")){
				resetProductInfo();
				//尺寸被选中了一个，则将于该尺寸不匹配的颜色禁用掉。
				console.log("尺寸被选中了一个，则将于该尺寸不匹配的颜色禁用掉。");
				//找出对应的规格
				var colorArr = [];//与选中的规格相匹配的颜色集合
				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					if(specItem.specSize==$("#specSize .specSelectCss").html()){
						$("#specIdHidden").val(specItem.id);
						$("#nowPrice").text(specItem.specPrice);
						$("#stock_span_id").text(specItem.specStock);
						colorArr.push(specItem.specColor);
					}
				}
				
				//释放所有颜色的鼠标禁用状态
				$("#specColor li").each(function(){
					$(this).removeClass("specNotAllowed");
				});
				
				//找出于选择的尺寸不匹配的颜色，将其禁用掉。
				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					var hanFind = false;
					for(var j=0;j<colorArr.length;j++){
						if(specItem.specColor==colorArr[j]){
							hanFind = true;
							break;
						}
					}
					
					if(!hanFind){
						console.log("禁掉的颜色有："+specItem.specColor);
						
						$("#specColor li").each(function(){
							console.log("text="+$(this).text());
							if($(this).text()==specItem.specColor){
								console.log("找到了。");
								$(this).addClass("specNotAllowed");
								return false;
							}
						});
					}
				}
				
			}else if($("#specColor li").hasClass("specSelectCss")){
				resetProductInfo();
				//颜色被选中了一个，则将于该颜色不匹配的尺寸禁用掉。
				console.log("颜色被选中了一个，则将于该颜色不匹配的尺寸禁用掉。");
				
				//找出对应的规格
				var sizeArr = [];//与选中的规格相匹配的颜色集合
				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					if(specItem.specColor==$("#specColor .specSelectCss").html()){
						sizeArr.push(specItem.specSize);
					}
				}
				
				//释放所有颜色的鼠标禁用状态
				$("#specSize li").each(function(){
					$(this).removeClass("specNotAllowed");
				});
				
				//找出于选择的尺寸不匹配的颜色，将其禁用掉。
				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					var hanFind = false;
					for(var j=0;j<sizeArr.length;j++){
						if(specItem.specSize==sizeArr[j]){
							hanFind = true;
							break;
						}
					}
					
					if(!hanFind){
						console.log("禁掉的尺寸有："+specItem.specSize);
						$("#specSize li").each(function(){
							console.log("text="+$(this).text());
							if($(this).text()==specItem.specSize){
								console.log("找到了。");
								$(this).addClass("specNotAllowed");
								return false;
							}
						});
					}
				}
				
			}else{
				console.log("都没选中。");
				resetProductInfo();
			}
			
		});
	}
	
});

//重置商品信息
function resetProductInfo(){
	console.log("resetProductInfo..."+$("#nowPriceHidden").val());
	//设置值为商品原价格
	$("#nowPrice").text($("#nowPriceHidden").val());
	$("#stock_span_id").text($("#stockHidden").val());
	$("#specIdHidden").val("");
}

//去购物车结算
function toCart(){
	window.location.href = "${basepath}/cart/cart.html";
}

var options={
		animation:true,
		trigger:'hover', //触发tooltip的事件
		show: 500, hide: 100
	};
//添加商品收藏
function addToFavorite(productID){
	var _url = "${basepath}/favorite/addFavoriteProduct?id="+productID+"&radom="+Math.random();
	console.log("_url="+_url);
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  success: function(data){
		  console.log("addToFavorite.data="+data);
		  var _result = "商品已成功添加到收藏夹！";
		  if(data=="0"){
			  _result = "商品已成功添加到收藏夹！";
			
		  }else if(data=='1'){
			  _result = "已添加，无需重复添加！";
		  }else if(data=='-1'){//提示用户要先登陆
			  _result = "使用此功能需要先登陆！";
		  }
		    alert(_result);
		      $('#addToFavoriteBtn').hide();
			  $('#isFavoriteC').show();
		 
	  },
	  dataType: "text",
	  error:function(er){
		  console.log("addToFavorite.er="+er);
	  }
	});
}



//商品收藏
function isCollection(){
alert("该商品已收藏");
return;
}

//店铺收藏
function isCollections(){
alert("该店铺已收藏");
return;
}
function emailNotifyProduct(obj){
	var _receiveEmail = $("#receiveEmail").val();
	if($.trim(_receiveEmail).length==0){
		$("#receiveEmail").focus();
		return;
	}
	
	var _url = "${basepath}/product/insertEmailNotifyProductService.html?receiveEmail="+_receiveEmail+"&productID="+$("#productID").val()+"&productName="+$("#productName").val();
	console.log("_url="+_url);
	$(obj).attr({"disabled":"disabled"});
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  success: function(data){
		  console.log("emailNotifyProduct.data="+data);
		  var _result = "到货通知添加成功！";
		  if(data=="0"){
		  }else if(data=='-1'){//提示用户要先登陆
			  _result = "使用此功能需要先登陆！";
		  }
		  $("#emailNotifyProduct_input").hide();
		  $("#emailNotifyProductDiv").html(_result);
		  console.log(_result);
	  },
	  dataType: "text",
	  error:function(er){
		  console.log("emailNotifyProduct.er="+er);
		  $("#emailNotifyProductDiv").html("添加到货通知失败，请联系站点管理员！");
	  }
	});
}

//显示礼品详情
function showGiftDetail(){
	if($("#giftDetailDiv").is(':hidden')){
		$("#giftDetailDiv").slideDown(1000);		
	}else{
		$("#giftDetailDiv").slideUp(1000);
	}
}

</script>

<!-- baidu fenxiang -->
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"slide":{"type":"slide","bdImg":"0","bdPos":"right","bdTop":"100"}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
<!-- Baidu Button END -->
http://demo1.cssmoban.com/cssthemes3/cpts_181_ff/js/move-top.js
</@shopBasePage.shopBasePage>