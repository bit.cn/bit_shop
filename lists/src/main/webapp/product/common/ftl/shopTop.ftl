<#macro shopTop checkLogin=true>
<div class="top">
  <div class="top_main">
    <div class="tm_left">
   	<a href="${systemSetting().www}" class="logoDescs" target="_balnk">&quot;话不多说买买买&quot;——欢迎来到anitoys</a>
    <#if currentAccount()??>
        <span id="loginOrRegSpan">
		     <span class="sn-login"">欢迎回来，${currentAccount().account}</span>
		     <a href="${basepath}/account/exit" class="sn-regist">退出系统</a>
		 </span>
	  <#else >
		 <span id="loginOrRegSpan">
			 <a href="${basepath}/account/login" class="sn-login">请登陆</a>
			 <a href="${basepath}/account/register" class="sn-regist">免费注册</a>
		 </span>
		</#if>
	</div>
    <div class="tm_right">
      <ul>
      <#if currentAccount()??>
      <li class="order"><a href="${basepath}/account/orders">我的订单</a></li>
         <li class="grzx"><a href="${basepath}/account/orders?status=dbk">待补款订单<div class="bkddnum">
         <!--
         <#if waitOrder?exists>   
                     ${waitOrder!""}
                   <#else >
                      0
                   </#if>
          -->
         </div></a></li>
         <li class="scj"><a href="#" class="xljt">我的anitoys</a>
          <div class="scjtk">
            <ul>
              <li><a href="${basepath}/account/orders?status=dsh" target="_blank">已发货订单</a></li>
              <li><a href="${basepath}/account/custService" target="_blank">售后订单</a></li>
              <li><a href="${basepath}/coupon/coupons?status=0" target="_blank">优惠券</a></li>
              <li><a href="${basepath}/favorite/searchUserFavoritePro" target="_blank">收藏的商品</a></li>
              <li><a href="${basepath}/favorite/searchUserFavoriteBusi" target="_blank">收藏的店铺</a></li>
            </ul>
          </div>
        </li>
       <#else >
        <li class="order"><a href="${basepath}/account/login">我的订单</a></li>
         <li class="grzx"><a href="${basepath}/account/login">待补款订单<div class="bkddnum">
         <!--
         <#if waitOrder?exists>   
                     ${waitOrder!""}
                   <#else >
                      0
                   </#if>
          -->
         </div></a></li>
         <li class="scj"><a href="${basepath}/account/login" class="xljt">我的anitoys</a>
          <div class="scjtk">
            <ul>
              <li><a href="${basepath}/account/login" target="_blank">已发货订单</a></li>
              <li><a href="${basepath}/account/login" target="_blank">售后订单</a></li>
              <li><a href="${basepath}/account/login" target="_blank">优惠券</a></li>
              <li><a href="${basepath}/account/login" target="_blank">收藏的商品</a></li>
              <li><a href="${basepath}/account/login" target="_blank">收藏的店铺</a></li>
            </ul>
          </div>
        </li>
		</#if>
        <li class="bzzc"><a href="#" class="xljt02">帮助支持</a>
          <div class="bztk">
            <ul>
              <li><a href="#" target="_blank">帮助中心</a></li>
            </ul>
          </div>
        </li>
        <li class="wzdh"><a "${basepath}/search.html" target="_blank" class="xljt02">网站导航</a>
          <div class="dhtk">
            <dl>
              <#list systemManager().businessList as item>
										<#if item_index<3>
												<dd><a href="${systemSetting().business}/business/businessSelect?businessId=${item.id!""}"  target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"  style="width:100px;height:150px;"/>
												</a></dd>
										</#if>
											
									</#list>
            </dl>
          </div>
        </li>
        <li class="wzdh"><a href="#" class="xljt">关注anitoys</a>
          <div class="gzani">
            <ul>
              <li><img src="${systemSetting().staticSource}/shopindex/layout/img/pic/towcode.png" width="100" height="100"></li>
              <br/>
              
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
</#macro>
