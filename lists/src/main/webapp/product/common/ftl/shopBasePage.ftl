<#macro shopBasePage title="" jsFiles=[] cssFiles=[] staticJsFiles=[] staticCssFiles=[] checkLogin=true>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <#assign non_responsive2>y</#assign>
    <#assign responsive>${Session["responsive"]!""}</#assign>
    <#if responsive == "y">
        <#assign non_responsive2>n</#assign>
    <#elseif systemSetting().openResponsive == "n">
        <#assign non_responsive2>y</#assign>
    <#else >
        <#assign non_responsive2>n</#assign>
    </#if>
    <#assign style>${RequestParameters.style!""}</#assign>
    <#if style=="">
        <#assign style>${systemSetting().style}</#assign>
    </#if>
    
    <script>
        var basepath = "${basepath}";
        var staticpath = "${staticpath}";
        var non_responsive2 = "${non_responsive2}";
        <#if currentUser()??>
            var login = true;
        var currentUser = "${currentUser().username}";
        <#else >
        var login = false;
        var currentUser = "";
        </#if>
    </script>
    <#if non_responsive2 != "y">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </#if>
    <meta name="description" content="${systemSetting().description}"/>
    <meta name="keywords" content="${systemSetting().keywords}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${title!"JEESHOP"}</title>
    <link rel="shortcut icon" type="image/x-icon" href="${systemSetting().shortcuticon}">
    <#--<script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery-1.4.2.min.js"></script>-->
    <script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery-1.9.1.min.js"></script>
    <#--<script type="text/javascript" src="${systemSetting().staticSource}/resource/zTree3.5/js/jquery.ztree.all-3.5.min.js"></script>-->

    <script type="text/javascript" src="${systemSetting().staticSource}/resource/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery.blockUI.js"></script>
    <#--<script type="text/javascript" src="${systemSetting().staticSource}/resource/js/manage.js"></script>-->

    <#--<script src="${systemSetting().staticSource}/resource/jquery-jquery-ui/jquery-1.5.1.js"></script>-->
    <#--<script src="${systemSetting().staticSource}/resource/jquery-jquery-ui/ui/jquery.ui.core.js"></script>-->
    <#--<script src="${systemSetting().staticSource}/resource/jquery-jquery-ui/ui/jquery.ui.widget.js"></script>-->
    <#--<script src="${systemSetting().staticSource}/resource/jquery-jquery-ui/ui/jquery.ui.tabs.js"></script>-->
    <!-- jquery validator -->

    <script type="text/javascript" src="${systemSetting().staticSource}/resource/validator-0.7.0/jquery.validator.js"></script>
    <script type="text/javascript" src="${systemSetting().staticSource}/resource/validator-0.7.0/local/zh_CN.js"></script>

    <#--<script type="text/javascript" src="${systemSetting().staticSource}/resource/My97DatePicker/WdatePicker.js"></script>-->

    <#--<link rel="stylesheet" href="${systemSetting().staticSource}/resource/kindeditor-4.1.7/themes/default/default.css" />-->
    <#--<script charset="utf-8" src="${systemSetting().staticSource}/resource/kindeditor-4.1.7/kindeditor-min.js"></script>-->
    <#--<script charset="utf-8" src="${systemSetting().staticSource}/resource/kindeditor-4.1.7/lang/zh_CN.js"></script>-->
    <script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="${systemSetting().staticSource}/resource/js/superMenu/js/new.js"></script>
    <link href="${systemSetting().staticSource}/resource/js/slideTab2/css/lanrenzhijia.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="${systemSetting().staticSource}/resource/js/slideTab2/js/lanrenzhijia.js"></script>
    

	<link rel="stylesheet" href="${systemSetting().staticSource}/resource/css/sticky-footer.css"  type="text/css">
	<#assign style>${style!""}</#assign>
	<#if style=="">
	    <#assign style>${systemSetting().style}</#assign>
	</#if>
	<link rel="stylesheet" href="${systemSetting().staticSource}/resource/bootstrap3.3.4/css/${style}/bootstrap.min.css"  type="text/css">

	</head>
    
    
    <!--通用CSS-->
    <link href="${systemSetting().staticSource}/shopindex/common/css/style.css" rel="stylesheet" type="text/css">
    <link href="${systemSetting().staticSource}/shopindex/common/css/index.css" rel="stylesheet" type="text/css">
    
    <!--通用的JS-->
    <script type="text/javascript" src="${systemSetting().staticSource}/shopindex/common/js/lrtk.js"></script>
</head>
<#nested />
</html>
</#macro>
