<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>访问出错了</title>
		<style>
			body{font: 12px/20px "Microsoft Yahei", arial, tahoma, MS Gothic, sans-serif;}
			.errorBox{ position: absolute; top: 50%; left: 50%; margin-top: -270px; margin-left: -420px; border: 1px solid #ccc; padding: 20px; width: 800px; height: 500px;background: #d9d9d9;}
			.error{position:relative;background: url(../images/error.png) no-repeat scroll -100px 0; height: 500px; width: 800px;}
			.error a{position: absolute; left: 220px; bottom: 140px; display: block; width: 120px; height: 30px; text-align: center; line-height: 30px; border: 1px solid #D7D7D7; background: #d9d9d9; color: #2894FF; text-decoration: none;}
		</style>
	</head>
	<body>
		<div class="errorBox">
			<div class="error">
				<a href="javascript:history.go(-1)">返回上一页</a>
			</div>
		</div>
	</body>
</html>
