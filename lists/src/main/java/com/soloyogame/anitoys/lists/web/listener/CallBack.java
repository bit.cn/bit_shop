package com.soloyogame.anitoys.lists.web.listener;

public interface CallBack {
	String callback() throws Exception;
}
