package com.soloyogame.anitoys.lists.web.controller;

import java.util.List;

import com.soloyogame.anitoys.lists.web.util.LoginUserHolder;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

/**
 * 获取当前登录的用户(前端用户)
 * @author dylan
 */
public class CurrentAccountGetter implements TemplateMethodModelEx {
    @Override
    public Object exec(List arguments) throws TemplateModelException {
        return LoginUserHolder.getLoginAccount();
    }
}
