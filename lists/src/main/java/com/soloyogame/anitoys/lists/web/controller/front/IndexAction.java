package com.soloyogame.anitoys.lists.web.controller.front;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 前端首页
 * @author shaojian
 */
@Controller
@RequestMapping("/")
public class IndexAction 
{
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(IndexAction.class);
	/**
     * 拦截404错误跳转页面
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "to404")
    public String to404(ModelMap model) throws Exception 
    {
        logger.info("跳转404页面！");
        return "404Ftl";
    }
}
