package com.soloyogame.anitoys.lists.web.controller.front.news;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.db.commond.CartInfo;
import com.soloyogame.anitoys.db.commond.Catalog;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.commond.Product;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.lists.web.controller.front.FrontBaseController;
import com.soloyogame.anitoys.lists.web.util.LoginUserHolder;
import com.soloyogame.anitoys.lists.web.util.RequestHolder;
import com.soloyogame.anitoys.service.CatalogService;
import com.soloyogame.anitoys.service.NewsService;
import com.soloyogame.anitoys.service.ProductService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;

/**
 * 文章管理
 * @author shaojian
 */
@Controller("frontNewsAction")
public class NewsAction extends FrontBaseController<News> 
{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(NewsAction.class);
	
	@Autowired
	private NewsService newsService;
	@Autowired
	private RedisCacheProvider redisCacheProvider;
	@Autowired
	private ProductService productService; 
	
	@Autowired
	private CatalogService catalogService;

	@Override
	public NewsService getService() {
		return newsService;
	}

	@ModelAttribute("newsCatalogs")
	public List<Catalog> getNewsCatalogs()
	{
		return loadNewCatalogs() ;   
	}
	
	/**
	 * 加载平台的文章目录
	 */
	public List<Catalog> loadNewCatalogs() 
	{
		Catalog c = new Catalog();
		c.setType("a");
		c.setPid("0");
		c.setShowInNav("y");
		List<Catalog> newCatalogs = catalogService.selectList(c);
		if(newCatalogs!=null && newCatalogs.size()>0)
		{
			for(int i=0;i<newCatalogs.size();i++)
			{
				Catalog item = newCatalogs.get(i);

				//加载此目录下的所有文章列表
				News news = new News();
				news.setCatalogID(item.getId());
				news.setStatus("y");
				List<News> newsList = newsService.selectPlatList(news);
				item.setNews(newsList);
			}
		}
        return newCatalogs;
	}
	
	@RequestMapping("news/list")
	public String newsList(ModelMap model, News e) throws Exception
	{
		String businessId=RequestHolder.getRequest().getParameter("businessId");
		if(businessId==null||"".equals(businessId))
		{
			businessId="";
		}
		String type=RequestHolder.getRequest().getParameter("type");
		e.setBusinessId(businessId);
		e.setType(type);
		PagerModel pager = selectPageList(getService(), e);
		pager.setPagerUrl("list");
		model.addAttribute("pager", pager);
		List<Product> productList = new ArrayList<Product>();
		Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
		CartInfo cartInfo = null;
		if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		int productCount=productService.selectProductCount();
		model.addAttribute("e", e);
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productCount", productCount);
		model.addAttribute("productList", productList);
		return "news/newsList";
	}
	
	@RequestMapping("platnews/list")
	public String platnewsList(ModelMap model, News e) throws Exception
	{
		String businessId=RequestHolder.getRequest().getParameter("businessId");
		String type=RequestHolder.getRequest().getParameter("type");
		if(businessId==null||"".equals(businessId))
		{
			businessId="";
		}
		e.setBusinessId(businessId);
		e.setType(type);
		int offset = 0;//分页偏移量
        if (RequestHolder.getRequest().getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(RequestHolder.getRequest().getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        e.setOffset(offset);
        PagerModel pager = newsService.selectPlatNewsPageList(e);
        if (pager == null) 
        {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)
                / pager.getPageSize());
		model.addAttribute("pager", pager);
		List<Product> productList = new ArrayList<Product>();
		Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
		CartInfo cartInfo = null;
		if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		int productCount=productService.selectProductCount();
		model.addAttribute("e", e);
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productCount", productCount);
		model.addAttribute("productList", productList);
		return "news/platnewsList";
	}
	
	/**
	 * 获取新闻详情
	 * @return
	 */
	@RequestMapping("/news/{id}")
	public String newsInfo(@PathVariable("id")String id, ModelMap model) throws Exception
	{
		logger.error("NewsAction.newsInfo=== id="+id);
		if(StringUtils.isBlank(id))
		{
			throw new NullPointerException("id is null");
		}
		News news =newsService.selectById(id);
		if(news==null)
		{
			throw new NullPointerException();
		}
		
		String url = "/jsp/notices/"+news.getId()+".jsp";
		logger.error("url = " + url);
		model.addAttribute("newsInfoUrl", url);
		model.addAttribute("news", news);

		List<Product> productList = new ArrayList<Product>();
		Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
		CartInfo cartInfo = null;
		if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		News e=new News();
		e.setBusinessId("");
		model.addAttribute("e", e);
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productList", productList);
		return "news/newsInfo";
	}
	
	
	/**
	 * 获取平台的新闻详情
	 * @return
	 */
	@RequestMapping("/platnews/{id}")
	public String platnewsInfo(@PathVariable("id")String id, ModelMap model) throws Exception
	{
		logger.error("NewsAction.newsInfo=== id="+id);
		if(StringUtils.isBlank(id))
		{
			throw new NullPointerException("id is null");
		}
		News news =newsService.selectPlatNewsById(id);
		if(news==null)
		{
			throw new NullPointerException();
		}
		
		String url = "/jsp/platnotices/"+news.getId()+".jsp";
		logger.error("url = " + url);
		model.addAttribute("newsInfoUrl", url);
		model.addAttribute("news", news);

		List<Product> productList = new ArrayList<Product>();
		Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
		CartInfo cartInfo = null;
		if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		News e=new News();
		e.setBusinessId("");
		model.addAttribute("e", e);
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productList", productList);
		return "news/platnewsInfo";
	}
	
	/**
	 * 帮助中心
	 * @return
	 */
	@RequestMapping("help/{helpCode}")
	public String help(@ModelAttribute("helpCode") @PathVariable("helpCode")String helpCode, ModelMap model) throws Exception 
	{
		logger.error("this.helpCode="+helpCode);
		List<Product> productList = new ArrayList<Product>();
		Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
		CartInfo cartInfo = null;
		if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		int productCount=productService.selectProductCount();
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productCount", productCount);
		model.addAttribute("productList", productList);
		if(StringUtils.isBlank(helpCode))
		{
			return "help/help";
		}
		else if(helpCode.equals("index"))
		{
			return "help/help";
		}
		else
		{
			News newsParam = new News();
			newsParam.setCode(helpCode);
			News news = newsService.selectSimpleOne(newsParam);
			if(news==null)
			{
				throw new NullPointerException("根据code查询不到文章！");
			}
			
			String url = "/jsp/helps/"+news.getId()+".jsp";
			logger.error("url = " + url);
			model.addAttribute("newsInfoUrl", url);
			model.addAttribute("news", news);
			return "help/help";
		}
	}
	
	/**
	 * 平台帮助中心
	 * @return
	 */
	@RequestMapping("plathelp/{helpCode}")
	public String plathelp(@ModelAttribute("helpCode") @PathVariable("helpCode")String helpCode, ModelMap model) throws Exception 
	{
		logger.error("this.helpCode="+helpCode);
		List<Product> productList = new ArrayList<Product>();
		Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
		CartInfo cartInfo = null;
		if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		int productCount=productService.selectProductCount();
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productCount", productCount);
		model.addAttribute("productList", productList);
		if(StringUtils.isBlank(helpCode))
		{
			return "help/help";
		}
		else if(helpCode.equals("index"))
		{
			return "help/help";
		}
		else
		{
			News newsParam = new News();
			newsParam.setCode(helpCode);
			News news = newsService.selectSimpleOne(newsParam);
			if(news==null)
			{
				throw new NullPointerException("根据code查询不到文章！");
			}
			
			String url = "/jsp/plathelps/"+news.getId()+".jsp";
			logger.error("url = " + url);
			model.addAttribute("newsInfoUrl", url);
			model.addAttribute("news", news);
			return "help/help";
		}
	}
	
	/**
	 * 获取新闻详情
	 * @return
	 */
	@RequestMapping("/blog/{id}")
	public String blog(@PathVariable("id")String id, ModelMap model) throws Exception
	{
		logger.error("NewsAction.newsInfo=== id="+id);
		if(StringUtils.isBlank(id))
		{
			throw new NullPointerException("id is null");
		}
		News news =newsService.selectById(id);
		if(news==null)
		{
			throw new NullPointerException();
		}
		
		String url = "/jsp/blog/"+news.getId()+".jsp";
		logger.error("url = " + url);
		model.addAttribute("newsInfoUrl", url);
		model.addAttribute("news", news);

		List<Product> productList = new ArrayList<Product>();
		Account account = LoginUserHolder.getLoginAccount();    //得到登录用户的信息
		CartInfo cartInfo = null;
		if(account==null)
		{
			cartInfo = (CartInfo) redisCacheProvider.get("myCart");
		}
		else
		{
			cartInfo = (CartInfo) redisCacheProvider.get("user_"+account.getId()+"Cart");
		}
		if(cartInfo!=null)
		{
			productList = cartInfo.getProductList();
		} 
		else 
		{
			cartInfo = new CartInfo();
		}
		model.addAttribute("cartInfo", cartInfo);
		model.addAttribute("productList", productList);
		return "news/newsInfo";
	}
}
