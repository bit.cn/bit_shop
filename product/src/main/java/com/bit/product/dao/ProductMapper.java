package com.bit.product.dao;

import java.util.List;
import java.util.Map;

import com.bit.product.dto.ProductDto;
import com.bit.product.model.Product;
import com.oxygen.dto.ApiRequest;

/**
 * 商品DAO
 * @author jason
 */
public interface ProductMapper {

	/**
	 * 添加
	 * @param record
	 * @return
	 */
	int insert(Product product);

	/**
	 * 条件添加
	 * @param product
	 * @return
	 */
	int insertSelective(Product product);

	/**
	 * 根据主键查询
	 * @param id
	 * @return
	 */
	Product selectByPrimaryKey(Integer id);

	/**
	 * 条件修改
	 * @param product
	 * @return
	 */
	int updateByPrimaryKeySelective(Product product);

	/**
	 *  非条件修改
	 * @param product
	 * @return
	 */
	int updateByPrimaryKey(Product product);
	
	/**
	 * 根据主键删除
	 * @param id
	 * @return
	 */
	int deleteByPrimaryKey(Integer id);

	
	// /**
	// * 删除
	// * @param e
	// * @return
	// */
	// public int delete(Product e);
	//
	
	//
	// /**
	// * 查询一条记录
	// * @param e
	// * @return
	// */
	// public Product selectOne(Product product);
	//
	 /**
	 * 分页查询商品列表
	 * @param paramMap
	 * @return
	 */
	 public List<ProductDto> selectPageList(Map<String,Object> paramMap);
	
	 /**
	 * 分页查询商品列表的总数量
	 * @param paramMap
	 * @return
	 */
	 public int selectPageListCount(Map<String,Object> paramMap);
	
	 /**
	  * 根据条件查询所有
	  * @param e
	  * @return
	  */
	 public List<ProductDto> selectList(Map<String,Object> paramMap);
}