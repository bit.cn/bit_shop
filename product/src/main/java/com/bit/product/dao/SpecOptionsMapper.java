package com.bit.product.dao;

import com.bit.product.model.SpecOptions;

public interface SpecOptionsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SpecOptions record);

    int insertSelective(SpecOptions record);

    SpecOptions selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SpecOptions record);

    int updateByPrimaryKey(SpecOptions record);
}