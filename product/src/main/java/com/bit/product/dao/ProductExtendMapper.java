package com.bit.product.dao;

import com.bit.product.model.ProductExtend;

public interface ProductExtendMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductExtend record);

    int insertSelective(ProductExtend record);

    ProductExtend selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductExtend record);

    int updateByPrimaryKey(ProductExtend record);
}