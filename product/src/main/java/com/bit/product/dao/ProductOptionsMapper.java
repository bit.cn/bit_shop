package com.bit.product.dao;

import com.bit.product.model.ProductOptions;
public interface ProductOptionsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductOptions record);

    int insertSelective(ProductOptions record);

    ProductOptions selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductOptions record);

    int updateByPrimaryKey(ProductOptions record);
}