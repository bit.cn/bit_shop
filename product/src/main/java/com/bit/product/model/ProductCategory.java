package com.bit.product.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class ProductCategory implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 父类型ID
     */
    private Integer parentId;

    /**
     * 类别名称
     */
    private String categoryName;

    /**
     * 类别code
     */
    private String categoryCode;

    /**
     * 序列号
     */
    private Integer categorySequence;

    /**
     * 是否在导航显示(是否显示在首页的导航条上(y:显示,n:不显示)
     */
    private String showInNav;

    /**
     * 分类图片
     */
    private String categoryImage;

    /**
     * 是否已经删除
     */
    private String isDeleted;

    /**
     * 分类文字样式
     */
    private String categoryFontCss;

    /**
     * 类别简介
     */
    private String summary;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public Integer getCategorySequence() {
        return categorySequence;
    }

    public void setCategorySequence(Integer categorySequence) {
        this.categorySequence = categorySequence;
    }

    public String getShowInNav() {
        return showInNav;
    }

    public void setShowInNav(String showInNav) {
        this.showInNav = showInNav;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCategoryFontCss() {
        return categoryFontCss;
    }

    public void setCategoryFontCss(String categoryFontCss) {
        this.categoryFontCss = categoryFontCss;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}