package com.bit.product.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class ProductAttribute implements Serializable {
    /**
     * 主键Id
     */
    private Integer id;

    /**
     * 商品ID
     */
    private Integer productId;

    /**
     * 属性选项ID
     */
    private Integer attributeOptionsId;

    /**
     * 属性名称
     */
    private String attributeName;

    /**
     * 属性选项值
     */
    private String attributeOptionsValue;

    /**
     * 属性选项名称
     */
    private String attributeOptionsName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getAttributeOptionsId() {
        return attributeOptionsId;
    }

    public void setAttributeOptionsId(Integer attributeOptionsId) {
        this.attributeOptionsId = attributeOptionsId;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeOptionsValue() {
        return attributeOptionsValue;
    }

    public void setAttributeOptionsValue(String attributeOptionsValue) {
        this.attributeOptionsValue = attributeOptionsValue;
    }

    public String getAttributeOptionsName() {
        return attributeOptionsName;
    }

    public void setAttributeOptionsName(String attributeOptionsName) {
        this.attributeOptionsName = attributeOptionsName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}