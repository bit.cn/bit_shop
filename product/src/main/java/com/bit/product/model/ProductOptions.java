package com.bit.product.model;

import java.util.Date;

public class ProductOptions {
    private Integer id;

    private Integer attributeId;

    private String attributeOptionsValue;

    private String attributeOptionsName;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    public String getAttributeOptionsValue() {
        return attributeOptionsValue;
    }

    public void setAttributeOptionsValue(String attributeOptionsValue) {
        this.attributeOptionsValue = attributeOptionsValue == null ? null : attributeOptionsValue.trim();
    }

    public String getAttributeOptionsName() {
        return attributeOptionsName;
    }

    public void setAttributeOptionsName(String attributeOptionsName) {
        this.attributeOptionsName = attributeOptionsName == null ? null : attributeOptionsName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}