package com.bit.product.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class ProductSpec implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 商品ID
     */
    private Integer productId;

    /**
     * 规格选项ID
     */
    private Integer specOptionsId;

    /**
     * 规格名称
     */
    private String specName;

    /**
     * 规格选项值
     */
    private String specOptionsValue;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getSpecOptionsId() {
        return specOptionsId;
    }

    public void setSpecOptionsId(Integer specOptionsId) {
        this.specOptionsId = specOptionsId;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getSpecOptionsValue() {
        return specOptionsValue;
    }

    public void setSpecOptionsValue(String specOptionsValue) {
        this.specOptionsValue = specOptionsValue;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}