package com.bit.product.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class Attribute implements Serializable {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 属性名称
     */
    private String attributeName;

    /**
     * 属性类别(有选项，自定义)
     */
    private String attributeType;

    /**
     * 类别ID
     */
    private Integer categoryId;

    /**
     * 父级属性id
     */
    private Integer parentId;

    /**
     * 属性序列号
     */
    private Integer attributeSequence;

    /**
     * 是否显示
     */
    private String isShow;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(String attributeType) {
        this.attributeType = attributeType;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getAttributeSequence() {
        return attributeSequence;
    }

    public void setAttributeSequence(Integer attributeSequence) {
        this.attributeSequence = attributeSequence;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}