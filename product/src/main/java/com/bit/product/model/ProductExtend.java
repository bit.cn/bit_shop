package com.bit.product.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class ProductExtend implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 商品ID
     */
    private Integer productId;

    /**
     * 是否新品
     */
    private String isNew;

    /**
     * 是否特价
     */
    private String isSale;

    /**
     * 是否热销产品
     */
    private String isHot;

    /**
     * 是否可以评论
     */
    private String isEvaluate;

    /**
     * 是否限时促销
     */
    private String isTimePromotion;

    /**
     * 商品页面标题
     */
    private String productTitle;

    /**
     * 商品辅助标题
     */
    private String auxiliaryTitle;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getIsNew() {
        return isNew;
    }

    public void setIsNew(String isNew) {
        this.isNew = isNew;
    }

    public String getIsSale() {
        return isSale;
    }

    public void setIsSale(String isSale) {
        this.isSale = isSale;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getIsEvaluate() {
        return isEvaluate;
    }

    public void setIsEvaluate(String isEvaluate) {
        this.isEvaluate = isEvaluate;
    }

    public String getIsTimePromotion() {
        return isTimePromotion;
    }

    public void setIsTimePromotion(String isTimePromotion) {
        this.isTimePromotion = isTimePromotion;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getAuxiliaryTitle() {
        return auxiliaryTitle;
    }

    public void setAuxiliaryTitle(String auxiliaryTitle) {
        this.auxiliaryTitle = auxiliaryTitle;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}