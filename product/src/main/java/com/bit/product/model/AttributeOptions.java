package com.bit.product.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class AttributeOptions implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 属性ID
     */
    private Integer attributeId;

    /**
     * 属性选项值(对于无属性选项的直接录入自定义属性值)
     */
    private String attributeOptionsValue;

    /**
     * 属性选项名称
     */
    private String attributeOptionsName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    public String getAttributeOptionsValue() {
        return attributeOptionsValue;
    }

    public void setAttributeOptionsValue(String attributeOptionsValue) {
        this.attributeOptionsValue = attributeOptionsValue;
    }

    public String getAttributeOptionsName() {
        return attributeOptionsName;
    }

    public void setAttributeOptionsName(String attributeOptionsName) {
        this.attributeOptionsName = attributeOptionsName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}