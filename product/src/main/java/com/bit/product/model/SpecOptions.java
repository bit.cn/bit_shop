package com.bit.product.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class SpecOptions implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 规格ID
     */
    private Integer specId;

    /**
     * 规格选项值
     */
    private String specOptionsValue;

    /**
     * 规格选项名称
     */
    private String specOptionsName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecOptionsValue() {
        return specOptionsValue;
    }

    public void setSpecOptionsValue(String specOptionsValue) {
        this.specOptionsValue = specOptionsValue;
    }

    public String getSpecOptionsName() {
        return specOptionsName;
    }

    public void setSpecOptionsName(String specOptionsName) {
        this.specOptionsName = specOptionsName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}