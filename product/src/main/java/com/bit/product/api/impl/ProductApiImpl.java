package com.bit.product.api.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.bit.product.api.ProductApi;
import com.bit.product.dao.ProductMapper;
import com.bit.product.dto.ProductDto;
import com.oxygen.annotations.ApiService;
import com.oxygen.dto.ApiRequest;

@Service
@ApiService(descript = "商品接口")
public class ProductApiImpl implements ProductApi {
	
	private static final Logger logger = Logger.getLogger(ProductApiImpl.class);
	
	@Resource
	private ProductMapper productMapper;

	@Override
	public List<ProductDto> getProductPageList(ApiRequest apiRequest) {
		return null;
	}

	@Override
	public List<ProductDto> getProductList(ApiRequest apiRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProductDto getProductInfo(ApiRequest apiRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int insertProduct(ApiRequest apiRequest) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateProduct(ApiRequest apiRequest) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteProduct(ApiRequest apiRequest) {
		// TODO Auto-generated method stub
		return 0;
	}
}