package com.bit.product.api;

import java.util.List;

import com.bit.product.dto.ProductDto;
import com.oxygen.dto.ApiRequest;

/**
 * 商品服务接口
 * @author user
 */
public interface ProductApi {
	
	/**
	 * 分页查询商品列表
	 * @param apiReq
	 * @return
	 */
	public List<ProductDto> getProductPageList(ApiRequest apiRequest);
	
	/**
	 * 查询所有的商品列表
	 * @param apiReq
	 * @return
	 */
	public List<ProductDto> getProductList(ApiRequest apiRequest);
	
	/**
	 * 查询商品详情
	 * @param apiRequest
	 * @return
	 */
	public ProductDto getProductInfo(ApiRequest apiRequest);
	
	/**
	 * 添加商品信息
	 * @param apiRequest
	 * @return
	 */
	public int insertProduct(ApiRequest apiRequest);
	
	/**
	 * 更新商品信息
	 * @param apiRequest
	 * @return
	 */
	public int updateProduct(ApiRequest apiRequest);
	
	/**
	 * 删除商品信息
	 * @param apiRequest
	 * @return
	 */
	public int deleteProduct(ApiRequest apiRequest);
}
