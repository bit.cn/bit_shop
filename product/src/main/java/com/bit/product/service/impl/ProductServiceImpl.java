package com.bit.product.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bit.product.dao.ProductMapper;
import com.bit.product.dto.ProductDto;
import com.bit.product.model.Product;
import com.bit.product.service.ProductService;
import com.oxygen.annotations.ApiMethod;
import com.oxygen.annotations.ApiParam;
import com.oxygen.annotations.ApiService;
import com.oxygen.dto.ApiRequest;
import com.oxygen.dto.ApiResponse;
import com.oxygen.enums.ApiMsgEnum;

@Service
@ApiService(descript = "商品服务接口")
public class ProductServiceImpl extends BaseServiceImpl implements ProductService {
	private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);

	@Autowired
	private ProductMapper productMapper;

	@ApiMethod(value = "product.getProductPageList", descript = "分页查询商品列表", apiParams = { @ApiParam(descript = "页码", name = "page"), @ApiParam(descript = "每页多少条", name = "page_size") })
	@Override
	public String getProductPageList(ApiRequest apiRequest) {
		List<ProductDto> productList = productMapper.selectPageList(apiRequest);
		int count = productMapper.selectPageListCount(apiRequest);
		return this.buildResponse(apiRequest, new ApiResponse<List<ProductDto>>(ApiMsgEnum.SUCCESS, count, productList));
	}

	@ApiMethod(value = "product.getProductList", descript = "查询商品列表", apiParams = {})
	@Override
	public String getProductList(ApiRequest apiRequest) {
		List<ProductDto> productList = productMapper.selectList(apiRequest);
		return this.buildResponse(apiRequest, new ApiResponse<List<ProductDto>>(ApiMsgEnum.SUCCESS, productList.size(), productList));
	}

	@ApiMethod(value = "product.getProductInfo", descript = "查询商品", apiParams = { @ApiParam(descript = "商品ID", name = "id")})
	@Override
	public String getProductInfo(ApiRequest apiRequest) {
		int id = apiRequest.getInt("id");
		Product product = productMapper.selectByPrimaryKey(id);
		return this.buildResponse(apiRequest, new ApiResponse<Product>(ApiMsgEnum.SUCCESS, 1, product));
	}

	@ApiMethod(value = "product.insertProduct", descript = "添加商品", apiParams = { @ApiParam(descript = "货品ID", name = "goodsId"), @ApiParam(descript = "商品CODE", name = "productCode"),
			@ApiParam(descript = "商品名称", name = "productName"), @ApiParam(descript = "商品简介", name = "introduce"), @ApiParam(descript = "商品列表小图片地址", name = "picture"),
			@ApiParam(descript = "商品大图片地址", name = "maxPicture"), @ApiParam(descript = "原价", name = "price"), @ApiParam(descript = "促销价格", name = "nowPrice"),
			@ApiParam(descript = "销量", name = "sellCount"), @ApiParam(descript = "库存", name = "stock"), @ApiParam(descript = "商品状态", name = "status"),
			@ApiParam(descript = "商品限购数量", name = "productRestrictions"), @ApiParam(descript = "分类ID", name = "catalogId"), @ApiParam(descript = "商品品牌ID", name = "brandId"),
			@ApiParam(descript = "商家ID", name = "businessId"), @ApiParam(descript = "上架时间", name = "shelvesTime"), @ApiParam(descript = "下架时间", name = "shelfTime"),
			@ApiParam(descript = "商品购买开始时间或者预售开始时间", name = "sellTime"), @ApiParam(descript = "商品购买结束时间或预售结束时间", name = "sellEndTime"), @ApiParam(descript = "商品补款开始时间", name = "replenishmentTime"),
			@ApiParam(descript = "商品补款结束时间", name = "replenishmentEndTime"), @ApiParam(descript = "预售价格", name = "depositPrice"), @ApiParam(descript = "尾款价格", name = "balancePrice"),
			@ApiParam(descript = "搜索关键字", name = "searchKey"), @ApiParam(descript = "商品发售日期", name = "saleatDate"), @ApiParam(descript = "商品称重", name = "productWeight"),
			@ApiParam(descript = "商品单位", name = "unit"), @ApiParam(descript = "商品类型", name = "productType"), @ApiParam(descript = "商品发货仓库ID", name = "expressId"),
			@ApiParam(descript = "商品售后服务ID", name = "serviceId"), @ApiParam(descript = "商品库存总量", name = "productTotal"), @ApiParam(descript = "商品称重", name = "productWeight"),
			@ApiParam(descript = "商品称重", name = "productWeight"), @ApiParam(descript = "商品称重", name = "productWeight"), @ApiParam(descript = "商品称重", name = "productWeight"),
			@ApiParam(descript = "商品称重", name = "productWeight"), @ApiParam(descript = "商品称重", name = "productWeight"), @ApiParam(descript = "商品称重", name = "productWeight"), })
	@Override
	public String insertProduct(ApiRequest apiRequest) {
		Product product = (Product) apiRequest.getBean(Product.class);
		product.setCreateTime(new Date());
		product.setUpdateTime(new Date());
		int flag = productMapper.insertSelective(product);
		if (flag > 0) {
			return this.buildResponse(apiRequest, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiRequest, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "product.updateProduct", descript = "更新商品", apiParams = { @ApiParam(descript = "商品ID", name = "id"), @ApiParam(descript = "货品ID", name = "goodsId"),
			@ApiParam(descript = "商品CODE", name = "productCode"), @ApiParam(descript = "商品名称", name = "productName"), @ApiParam(descript = "商品简介", name = "introduce"),
			@ApiParam(descript = "商品列表小图片地址", name = "picture"), @ApiParam(descript = "商品大图片地址", name = "maxPicture"), @ApiParam(descript = "原价", name = "price"),
			@ApiParam(descript = "促销价格", name = "nowPrice"), @ApiParam(descript = "销量", name = "sellCount"), @ApiParam(descript = "库存", name = "stock"), @ApiParam(descript = "商品状态", name = "status"),
			@ApiParam(descript = "商品限购数量", name = "productRestrictions"), @ApiParam(descript = "分类ID", name = "catalogId"), @ApiParam(descript = "商品品牌ID", name = "brandId"),
			@ApiParam(descript = "商家ID", name = "businessId"), @ApiParam(descript = "上架时间", name = "shelvesTime"), @ApiParam(descript = "下架时间", name = "shelfTime"),
			@ApiParam(descript = "商品购买开始时间或者预售开始时间", name = "sellTime"), @ApiParam(descript = "商品购买结束时间或预售结束时间", name = "sellEndTime"), @ApiParam(descript = "商品补款开始时间", name = "replenishmentTime"),
			@ApiParam(descript = "商品补款结束时间", name = "replenishmentEndTime"), @ApiParam(descript = "预售价格", name = "depositPrice"), @ApiParam(descript = "尾款价格", name = "balancePrice"),
			@ApiParam(descript = "搜索关键字", name = "searchKey"), @ApiParam(descript = "商品发售日期", name = "saleatDate"), @ApiParam(descript = "商品称重", name = "productWeight"),
			@ApiParam(descript = "商品单位", name = "unit"), @ApiParam(descript = "商品类型", name = "productType"), @ApiParam(descript = "商品发货仓库ID", name = "expressId"),
			@ApiParam(descript = "商品售后服务ID", name = "serviceId"), @ApiParam(descript = "商品库存总量", name = "productTotal"), @ApiParam(descript = "商品称重", name = "productWeight"),
			@ApiParam(descript = "商品称重", name = "productWeight"), @ApiParam(descript = "商品称重", name = "productWeight"), @ApiParam(descript = "商品称重", name = "productWeight"),
			@ApiParam(descript = "商品称重", name = "productWeight"), @ApiParam(descript = "商品称重", name = "productWeight"), @ApiParam(descript = "商品称重", name = "productWeight"), })
	@Override
	public String updateProduct(ApiRequest apiRequest) {
		Product product = (Product) apiRequest.getBean(Product.class);
		product.setUpdateTime(new Date());
		int flag = productMapper.updateByPrimaryKeySelective(product);
		if (flag > 0) {
			return this.buildResponse(apiRequest, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiRequest, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

	@ApiMethod(value = "product.deleteProduct", descript = "删除商品", apiParams = { @ApiParam(descript = "商品ID", name = "id")})
	@Override
	public String deleteProduct(ApiRequest apiRequest) {
		int id = apiRequest.getInt("id");
		int flag = productMapper.deleteByPrimaryKey(id);
		if (flag > 0) {
			return this.buildResponse(apiRequest, new ApiResponse<Object>(ApiMsgEnum.SUCCESS));
		}
		return this.buildResponse(apiRequest, new ApiResponse<Object>(ApiMsgEnum.FAIL));
	}

}