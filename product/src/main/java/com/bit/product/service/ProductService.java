package com.bit.product.service;

import java.util.List;

import com.bit.product.dto.ProductDto;
import com.oxygen.dto.ApiRequest;

/**
 * 商品服务接口
 * @author jason
 */
public interface ProductService {
	
	/**
	 * 分页查询商品列表
	 * @param apiReq
	 * @return
	 */
	public String getProductPageList(ApiRequest apiRequest);
	
	/**
	 * 查询所有的商品列表
	 * @param apiReq
	 * @return
	 */
	public String getProductList(ApiRequest apiRequest);
	
	/**
	 * 查询商品详情
	 * @param apiRequest
	 * @return
	 */
	public String getProductInfo(ApiRequest apiRequest);
	
	/**
	 * 添加商品信息
	 * @param apiRequest
	 * @return
	 */
	public String insertProduct(ApiRequest apiRequest);
	
	/**
	 * 更新商品信息
	 * @param apiRequest
	 * @return
	 */
	public String updateProduct(ApiRequest apiRequest);
	
	/**
	 * 删除商品信息
	 * @param apiRequest
	 * @return
	 */
	public String deleteProduct(ApiRequest apiRequest);
}
