package com.soloyogame.anitoys.cart.web.listener;

public interface CallBack {
	String callback() throws Exception;
}
