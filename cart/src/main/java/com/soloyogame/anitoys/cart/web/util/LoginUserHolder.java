package com.soloyogame.anitoys.cart.web.util;

import javax.servlet.http.HttpSession;

import com.soloyogame.anitoys.db.bean.User;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.util.constants.ManageContainer;
import com.soloyogame.anitoys.util.front.FrontContainer;

/**
 * Created by shaojian on 15-11-30.
 */
public class LoginUserHolder 
{
	public static User getLoginUser()
    {
        HttpSession session = RequestHolder.getSession();
        return session == null ? null : (User)session.getAttribute(ManageContainer.manage_session_user_info);
    }
    
    public static Account getLoginAccount()
    {
        HttpSession session = RequestHolder.getSession();
        Account account = (Account)session.getAttribute(FrontContainer.USER_INFO);
        return session == null ? null : account;
    }
}
