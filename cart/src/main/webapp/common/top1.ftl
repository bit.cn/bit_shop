<#macro shopTop checkLogin=true>
	<body>
		<div class="topNav">
			<div class="top_main clearfix">
				<div class="top_left">
					  <a href="${systemSetting().www}">"话不多说买买买"——欢迎来到anitoys</a>
				        <#if currentAccount()??>
				            <span class="sn-logn"">
				           	 欢迎回来，${currentAccount().account}
				            </span>
				            <a href="${systemSetting().passport}/account/exit" class="sn-regist">退出系统</a>
									<span style="display: none;">
										${currentAccount().account!""}
				                            (${currentAccount().loginType!""})
									 </span>
				        <#else>
				            <span><a href="${systemSetting().passport}/account/login">请登录</a>
				                <a href="${systemSetting().passport}/account/register" class="regist">免费注册</a>
				            </span>
				        </#if>
				</div>
				<div class="top_right">
					<ul>
                <li>
						<span>
							<a href="${basepath}/account/orders">我的订单</a>
						</span>
                </li>
                <li>
						<span><a href="${basepath}/account/orders?status=dbk">待补款订单
                            <div style="position: absolute;top: -5px;left: 85px;background: #ff6600;border-radius: 3px;color: #FFF;padding: 2px 3px;text-align: center;font: 200 8px Arial, Helvetica, sans-serif;">
                            <#if waitOrder?exists>
                            ${waitOrder!""}
                            <#else >
                                0
                            </#if>
                            </div>
                        </a></span>
                </li>
                <li>
						<span>
							<a href="${systemSetting().my}/account/orders?" target="_self" class="tran">我的anitoys</a>
							<div class="top_right_nav">
                                <a href="${basepath}/account/orders?status=dsh" target="_blank">已发货订单</a>
                                <a href="${basepath}/account/custService" target="_blank">售后订单</a>
                                <a href="${basepath}/coupon/coupons?status=0" target="_blank">优惠券</a>
                                <a href="${basepath}/favorite/searchUserFavoritePro" target="_blank">收藏的商品</a>
                                <a href="${basepath}/favorite/searchUserFavoriteBusi" target="_blank">收藏的商品</a>
                            </div>
						</span>
                </li>
                <li>
						<span>
							<a href="${systemSetting().doc}/help/xsbz.html" target="_self" class="tran">帮助支持</a>
							<div class="top_right_nav">
                            <#if helpList??> 
								    <#list helpList as item>
											<a href="${systemSetting().doc}/front/helpsmanage/${item.id}.html" target="_self">
											<#if item.title?length gte 7>
												${item.title?substring(0,7)}...
											<#else>
												${item.title!""}
											</#if>
											
											</a>
									</#list>
                   		 		<#else >
                   		 		</#if>
                            </div>
						</span>
                </li>
                <li>
						<span>
							<a href="${basepath}/search.html" target="_blank" class="tran">网站导航</a>
							<div class="top_right_nav imgS">
                            <#list systemManager().businessList as item>
                                <#if item_index<3>
                                    <a href="${systemSetting().business}/business/businessSelect?businessId=${item.id!""}"
                                       target="_blank">
                                        <img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"
                                             style="width:100px;height:150px;"/>
                                    </a>
                                </#if>

                            </#list>
                            </div>
						</span>
                </li>
                <li>
						<span>
						<a href="#" target="_blank" class="tran">关注anitoys</a>
							<div class="top_right_nav w100">
                                <a href="#" target="_blank">
                                    <img src="${systemSetting().staticSource}/shopindex/layout/img/pic/towcode.png"/>
                                </a>
                            </div>
						</span>
            	    </li>
            		</ul>
				</div>
			</div>
		</div>
		<div class="header">
			<div class="header_main clearfix"
         		style="background: #fff url(${systemSetting().imageRootPath}/${topPicture.topPicture!""}) no-repeat">
        	<div class="logo">
            	<a href="${basepath}"><img src="${systemSetting().staticSource}/business/img/logo.gif"/></a>
       		 </div>
				<div class="search clearfix">
					<div class="searchInput">
		                <form id="searchForm" method="post" action="${systemSetting().search}/product/searchBusiness.html">
		                    <input type="text" name="key" value="" placeholder="请输入商品关键字" accesskey="s" autocomplete="off" id="mq" role="combobox" aria-haspopup="true" class="combobox-input" onfocus="if(this.value==&#39;ANI大搜查&#39;) {this.value=&#39;&#39;;}this.style.color=&#39;#333&#39;;"
		                           onblur="if(this.value==&#39;&#39;) {this.value=&#39;ANI大搜查&#39;;this.style.color=&#39;#999999&#39;;}" maxlength="24">
		                    <input type="hidden" id="businessId" name="businessId" value=${businessId!""}/>
		                    <button type="submit" onclick="search();">搜本店</button>
		                    <button type="submit" class="all" >搜整站</button>
		                </form>
            		</div>
				</div>
				<div class="header_nav">
					<!--购物车-->
					<div class="car">
						<div class="carDesc">
                    		<span class="carlogo"><i>${productList?size}</i></span>
                    		<span class="carm"><a href="${systemSetting().card}/cart/cart.html">我的购物车</a></span>
                		</div>
						<div class="carDetail">
							<#if productList?? && productList?size gt 0>
		                    	<#list productList as item>
		                        	<div class="carItem">
			                            <div class="img"><img src="${systemSetting().imageRootPath}/${item.picture}"/></div>
			                            <div class="itemDetail">
			                                <a href="${systemSetting().www}/cart/cart.html">${item.name!""}</a>
			                                <span>有特典</span>
			                                <span>¥ ${item.nowPrice}</span>
			                                <a href="#" onclick="deleteFromCart('${item.id!""}')">删除</a>
			                            </div>
		                        	</div>
		                    	</#list>
                			</#if>
                			<#--<div class="gwcbtn">-->
		                    <div class="total">
		                    <span>共件${productList?size}商品 <b>共计¥${cartInfo.amount!""}</b></span>
		                        <a href="${systemSetting().www}/cart/cart.html">查看购物车</a>
		                    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</#macro>