<#macro shopTop checkLogin=true>
		<div class="topNav">
			<div class="top_main clearfix">
				<div class="top_left">
					<a href="${systemSetting().www}">"话不多说买买买"——欢迎来到anitoys</a>
					 <#if currentAccount()??>
					     <span class="sn-login"">欢迎回来，${currentAccount().account}</span>
					     <a href="${systemSetting().my}/account/exit" class="sn-regist">退出系统</a>
				    <#else >
					 <span>
						 <a href="${systemSetting().my}/account/login">请登陆</a>
						 <a href="${systemSetting().my}/account/register" class="regist">免费注册</a>
					 </span>
					</#if>
				</div>
				<div class="top_right">
					<ul>
					<#if currentAccount()??>
				         <li><span><a href="${systemSetting().my}/account/orders">我的订单</a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?status=dbk">待补款订单<div class="bkddnum">
				  <#if waitOrder?exists>   
                     ${waitOrder!""}
                   <#else >
                      0
                   </#if>
				         </div></a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?" class="tran" target="_self">我的anitoys</a>

				          <div class="top_right_nav">
				              <a href="${systemSetting().my}/account/orders?status=dsh" target="_self">已发货订单</a>
				              <a href="${systemSetting().my}/account/custService" target="_self">售后订单</a>
				              <a href="${systemSetting().my}/coupon/coupons?status=0" target="_self">优惠券</a>
				              <a href="${systemSetting().my}/favorite/searchUserFavoritePro" target="_self">收藏的商品</a>
				              <a href="${systemSetting().my}/favorite/searchUserFavoriteBusi" target="_self">收藏的店铺</a>
				          </div>
				          </span>
				        </li>
				       <#else >
				         <li><span><a href="${systemSetting().my}/account/login">我的订单</a></span></li>
				         <li><span><a href="${systemSetting().my}/account/login">待补款订单<div class="bkddnum">
				         <#if waitOrder?exists>   
                     		${waitOrder!""}
                   		 <#else >
                      		0
                   		 </#if>
				         </div></a></span></li>
				         <li><span><a href="${systemSetting().my}/account/login?"  target="_self" class="xljt">我的anitoys</a>
				          <div class="top_right_nav">
				            <ul>
				              <a href="${systemSetting().my}/account/login" target="_self">已发货订单</a>
				              <a href="${systemSetting().my}/account/login" target="_self">售后订单</a>
				              <a href="${systemSetting().my}/account/login" target="_self">优惠券</a>
				              <a href="${systemSetting().my}/account/login" target="_self">收藏的商品</a>
				              <a href="${systemSetting().my}/account/login" target="_self">收藏的店铺</a>
				          </div>
				          </span>
				        </li>
						</#if>
						<li>
							<span>
								<a href="#" target="_self" class="tran">帮助支持</a>
								<div class="top_right_nav">
								 <#if helpList??> 
								    <#list helpList as item>
											<a href="${systemSetting().doc}/front/helpsmanage/${item.id}.html" target="_self">${item.title}</a>
									</#list>
                   		 		<#else >
                   		 		</#if>
								</div>
							</span>
						</li>
						<li>
							<span>
								<a href="${basepath}/search.html" target="_Blank" class="tran">网站导航</a>
								<!--
								<div class="company_all clearfix" style="height:200px;">
									 <#list systemManager().businessList as item>
											<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${item.id!""}" style="height:80px;" target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"  width="80" height="60"/>
											</a>
									</#list>
								</div>
								-->
								<div class="top_right_nav imgS">
									<#list systemManager().businessList as item>
												<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${item.id!""}"  target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"  style="width:100px;height:150px;"/>
												</a>
									</#list>
								</div>
								
							<!--
								<div class="top_right_nav imgS">
									<#list systemManager().businessList as item>
										<#if item_index<3>
												<a href="${systemSetting().business}/business/businessSelect?businessId=${item.id!""}"  target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"  style="width:100px;height:150px;"/>
												</a>
										</#if>
									</#list>
								</div>
								-->
								
							</span>
						</li>
						<li>
						<span>
							<a href="#" target="_blank" class="tran">关注anitoys</a>
							<div class="top_right_nav w100">
								<a href="#" target="_blank">
									<img src="${systemSetting().staticSource}/shopindex/layout/img/pic/towcode.png"/>
								</a>
							</div>
						</span>
					</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="omt_top"style="position: relative;">
		<div class="omt_top_use">
			<ul class="clearfix">
				<li> <a href="${systemSetting().www}"><img src="${systemSetting().staticSource}/shopindex/layout/img/omt/logo.png"> </a></li>
				<li class="int_left"style="float:right">
					<form id="searchForm"method="post"action="${systemSetting().search}/search.html"target="_blank">
						<input type="text"name="key"class="inputtext"value=""id="mq"maxlength="24"onfocus="if(this.value==&#39;ANI大搜查&#39;) {this.value=&#39;&#39;;}this.style.color=&#39;#333&#39;;"onblur="if(this.value==&#39;&#39;) {this.value=&#39;ANI大搜查&#39;;this.style.color=&#39;#999999&#39;;}">
						<button type="submit"onclick="search();"class="subt-btn">搜整站</button>
					</form>
				</li>
			</ul>
			<ul class="inf">
				<li style="float:right">
					<div class="menuc_right">
					 <!--购物车-->
					 <#if currentAccount()??>
					<div class="car">
						<div class="carDesc">
							<span class="carlogo"><i>${productList?size}</i></span>
							<span class="carm"><a href="${systemSetting().card}/cart/cart.html">我的购物车</a></span>
						</div>
						<div class="carDetail">
							<#if productList?? && productList?size gt 0>
          					<#list productList as item>
								<div class="carItem">
									<div class="img"><img src="${systemSetting().imageRootPath}/${item.picture}" /></div>
	
									<div class="itemDetail">
										<a href="${systemSetting().www}/cart/cart.html">${item.name!""}</a>
										<span>有特典</span>
										<span>¥ ${item.nowPrice}</span>
										<a href="#" onclick="deleteFromCart('${item.id!""}')">删除</a>
									</div>
								</div>
				  			</#list>
            				</#if>
	            			<div class="total">
								<span>共${productList?size}件商品 <b>共计¥${cartInfo.amount!""}</b></span>
								<a href="${systemSetting().www}/cart/cart.html">查看购物车</a>
							</div>
						</div>
					</div>
					<#else >
					<div class="car">
						<div class="carDesc">
							<span class="carlogo"><i>0</i></span>
							<span class="carm"><a href="${systemSetting().card}/cart/cart.html">我的购物车</a></span>
						</div>
						<div class="carDetail">
	            			<div class="total">
								<span>共0件商品 <b>共计0</b></span>
								<a href="${systemSetting().www}/cart/cart.html">查看购物车</a>
							</div>
						</div>
					</div>
					 </#if>
				    </div>
				</li>
			</ul>
		</div>
	</div>
</#macro>