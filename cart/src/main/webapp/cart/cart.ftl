<#import "/ftl/htmlBase.ftl" as htmlBase>
<!DOCTYPE html>
<html>
<head>
<@htmlBase.htmlBase>
		<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/main/index.css">
		<link href="${systemSetting().staticSource}/static/frontend/v1/css/shoppingCart1.css" rel="stylesheet" type="text/css" />
		<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
		<title>购物车</title>
	</head>
	<body>
		<form id="placForm" name="placForm" method="post" theme="simple" action="${systemSetting().card}/order/plac">
			<div class="topNav">
			<div class="top_main clearfix">
				<div class="top_left">
					<a href="${systemSetting().www}">"话不多说买买买"——欢迎来到anitoys</a>
					 <#if currentAccount()??>
					     <span class="sn-login"">欢迎回来，${currentAccount().account}</span>
					     <a href="${systemSetting().my}/account/exit" class="sn-regist">退出系统</a>
				    <#else >
					 <span>
						 <a href="${systemSetting().my}/account/login">请登陆</a>
						 <a href="${systemSetting().my}/account/register" class="regist">免费注册</a>
					 </span>
					</#if>
				</div>
				<div class="top_right">
					<ul>
				         <li><span><a href="${systemSetting().my}/account/orders">我的订单</a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?status=dbk">待补款订单<div class="bkddnum">
						   <#if waitOrder?exists>   
		                     ${waitOrder!""}
		                   <#else >
                      		0
                   			</#if>
				         </div></a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?" class="tran" target="_self">我的anitoys</a>
				          <div class="top_right_nav">
				              <a href="${systemSetting().my}/account/orders?status=dsh" target="_self">已发货订单</a>
				              <a href="${systemSetting().my}/account/custService" target="_self">售后订单</a>
				              <a href="${systemSetting().my}/coupon/coupons?status=0" target="_self">优惠券</a>
				              <a href="${systemSetting().my}/favorite/searchUserFavoritePro" target="_self">收藏的商品</a>
				              <a href="${systemSetting().my}/favorite/searchUserFavoriteBusi" target="_self">收藏的店铺</a>
				          </div>
				          </span>
				        </li>
						<li>
							<span>
								<a href="javascript:void(0);" target="_self" class="tran">帮助支持</a>
								<div class="top_right_nav">
								 <#if helpList??> 
								    <#list helpList as item>
											<a href="${systemSetting().doc}/front/helpsmanage/${item.id}.html" target="_self">
											<#if item.title?length gte 7>
												${item.title?substring(0,7)}...
											<#else>
												${item.title!""}
											</#if>
											
											</a>
									</#list>
                   		 		<#else >
                   		 		</#if>
								</div>
							</span>
						</li>
						<li>
							<span>
								<a href="javascript:void(0);" target="_Blank" class="tran">网站导航</a>
								<#if businessList??>
									<div class="top_right_nav imgS">
										<#list businessList as item>
											<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${item.id!""}"  target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"  style="width:100px;height:150px;"/>
											</a>
									</#list>
									</#if>
								</div>
							</span>
						</li>
						<li>
						<span>
							<a href="javascript:void(0);" target="_blank" class="tran">关注anitoys</a>
							<div class="top_right_nav w100">
								<a href="javascript:void(0);" target="_blank">
									<img src="${systemSetting().staticSource}/shopindex/layout/img/pic/towcode.png"/>
								</a>
							</div>
						</span>
					</li>
					</ul>
				</div>
			</div>
			</div>
			<div class="header clearfix">
				<div class="logo"><a href="${systemSetting().www}">
				<img src="${systemSetting().staticSource}/shopindex/cart/image/aniToys.png" />
				</a>
				<div class="cart">购物车</div>
				</div>
				<div class="nav">
					<img src="${systemSetting().staticSource}/shopindex/cart/image/titlea.png" />
				</div>
			</div>
			<div class="cartDetail">
				<div class="cdTitle">
					<ul class="clearfix">
						<li class="li_1">
							<input type="checkbox" id="firstCheckbox"  class="firstCheckbox"/><span>全选</span>
						</li>
						<li class="li_2">
							产品信息
						</li>
						<li class="li_3">
							单价
						</li>
						<li class="li_4">
							数量
						</li>
						<li class="li_5">
							小计
						</li>
						<li class="li_6">
							操作
						</li>
				</ul>
				</div>
				<#if cartProductObjectList?? && cartProductObjectList?size gt 0>
	   			<#list cartProductObjectList as cartProductObject> 
				<div class="cartList">
					<div class="listTitle">
						<ul class="clearfix">
							<li class="li_1_1">
								<input type="checkbox" id="businessbox" class="businessbox"/>
								<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${cartProductObject.business.id!""}">
									<img src="${systemSetting().staticSource}/shopindex/cart/image/goodsmile.png" />
								</a>
							</li>
							<li class="li_11">
								<#if cartProductObject.business??>
									<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${cartProductObject.business.id!""}">${cartProductObject.business.businessName!""}</a>
									<a href="http://${cartProductObject.business.customerAddress!""}"><img src="${systemSetting().staticSource}/images/qq.png"></a>
								</#if>
							</li>
						</ul>
					</div>
					<#assign productList=cartProductObject.productList/> 
					<#if productList?? && productList?size gt 0>
					<#list productList as item>
					<div class="listCon">
						<ul class="clearfix">
							<li class="li_1_2">
								<input type="checkbox"  name="ids" value="${item.pSpecId!""}"/>
							</li>
							<li class="li_2">
								<div class="img"><img src="${systemSetting().imageRootPath}/${item.picture!""}"/></div>
								<div class="desc">
									<span><a target="_blank" href="${systemSetting().item}/product/${item.id!""}.html">${item.name!""}</a></span>
									<span></span>
								</div>
								<div class="size">
									<#if item.buySpecInfo??>
									  <br>商品规格：规格一:${item.buySpecInfo.specSize!""}<br /><p class="p_style_2">规格二：${item.buySpecInfo.specColor!""}</p>			
						 		    </#if>
								</div>
							</li>
							<li class="li_3">
								  <span>
									<#if item.productType==1>
										<div><label>全款:</label><span>${item.nowPrice!""}</span></div>
									<#elseif item.productType==2>
										<div><label>全款:</label><span>${item.nowPrice!""}</span></div>
								  		<div><label>定金:</label><span>${item.depositPrice!""}</span></div>	
								  		<div><label>尾款:</label><span>${item.balancePrice!""}</span></div>
									</#if>
								  </span>		
							</li>
							<li class="li_4">
								<div class="counter">
									<button  type='button' class="prev" onclick="subFunc(this,true)" >-</button>
									<input style="text-align: center;" class="pid_${item.id!""}" name="inputBuyNum" value="${item.buyCount!""}" size="4" maxlength="4" specId="${item.buySpecInfo.id}" pid="${item.id!""}" readonly="readonly"/>
									<input type="hidden" name="productRestrictions" value="${item.productRestrictions!""}"/>
									<input type="hidden" name="stock" value="${item.buySpecInfo.specStock!""}"/>
									<button  type='button' class="next" onclick="addFunc(this,true)">+</button>
								</div>
							<div class="conterText" style="height:20px;line-height: 20px;text-align: center;"><#if item.productRestrictions==0>不限购</#if><#if item.productRestrictions!=0>限购${item.productRestrictions}个</#if></div>
							</li>
							<li class="li_5">
								<#if item.productType==1>
									<span total0="total0">${item.total0!""}</span>
								<#elseif item.productType==2>
									<span total0="total0">${item.total0!""}</span>
									 <#if item.depositType='1'>
									 	定金支付
									 	<#elseif item.depositType=='2'>
									 	全款支付
									 </#if>
								</#if>
							</li>
							<li class="li_6">
								<a href="javascript:void(0);" onclick="addToFavorite('${item.id!""}',this)">加入收藏夹</a>
							<br />
							<a href="javascript:void(0);"  onclick="javascript:deleteFromCart('${item.pSpecId!""}')">删除</a>
							</li>
						</ul>
					</div>
					</#list>
					</div>
					<#else>
							<div class="bs-callout bs-callout-danger author" style="text-align: left;font-size: 22px;margin: 20px 100px;>
								<span class="glyphicon glyphicon-info-sign"></span>&nbsp;您的购物车是空的，赶紧去看看有什么好宝贝吧...
							</div>
					</#if>
				</#list>
				<#else>
						<div class="bs-callout bs-callout-danger author" style="text-align: left;font-size: 22px;margin: 20px 100px;>
							<span class="glyphicon glyphicon-info-sign"></span>&nbsp;您的购物车是空的，赶紧去看看有什么好宝贝吧...
						</div>
				</#if>
			</div>
			<div class="total">
				<input type="hidden" name="allbuyCount" id="allbuyCount" value="${cartInfo.allbuyCount!""}"/>
				<input type="hidden" name="amount" id="amount" value="${cartInfo.amount!""}"/>
				<ul>
					<li><input type="checkbox" id="firstCheckbox" class="firstCheckbox"/><span>全选</span></li>
					<li><a href="javascript:deletesFromCart()">删除选中的商品</a></li>
					<li><a href="javascript:addToFavorites(this)">移入收藏夹</a></li>
					<li>已选择<i id="productSize">0</i>件商品</li>
					<li>总价(不含运费)&yen;<i id="totalPayMonery">0</i></li>
					<li><a class="go" href="javascript:toPlacFromCart()">去结算</a></li>
				</ul>
		</div>
	</form>
<form action="${systemSetting().card}/cart/delete" method="POST" id="formDelete">
	<input type="hidden" name="id">
</form>
<script type="text/javascript" src="${systemSetting().staticSource}/js/common/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/product.js"></script>
<!-- <script src="http://localhost:8080/cart/resource/product.js"></script> -->
<script src="${systemSetting().staticSource}/js/hover.js"></script>
<script type="text/javascript">
var basepath = "${basepath}";
$(function() {
	//整个购物车全选全不选
	$(".firstCheckbox").on("click",function(){
		console.log("check="+$(this).prop("checked"));
		if($(this).prop("checked")){
			$("input[type=checkbox]").prop("checked",true);
			var inputBuyNumSize = $("input[name=inputBuyNum]").size(); 
			var productSize = 0;
			var totalPayMonerySize = $("span[total0=total0]").size();
			var totalPayMonery = 0;
			for(var i=0;i<inputBuyNumSize;i++)
			{
				productSize = productSize+parseInt($("input[name=inputBuyNum]").eq(i).val());
			}
			for(var i=0;i<totalPayMonerySize;i++)
			{
				totalPayMonery = totalPayMonery+parseInt($("span[total0=total0]").eq(i).text());
			}
			$("#productSize").text(productSize);
		    $("#totalPayMonery").text(totalPayMonery);
		}else{
			$("#productSize").text(0);
			$("#totalPayMonery").text(0);
			$("input[type=checkbox]").prop("checked", false);
		}
	});
	
	//购物车整个商家全选全不选
	$(".businessbox").on("click",function(){
		console.log("check="+$(this).prop("checked"));
		var obj = $(this).parents(".cartList");
		if($(this).prop("checked")){
            obj.find("input[type=checkbox]").prop("checked",true);
			var inputBuyNumSize = obj.find("input[name=inputBuyNum]").size();
			var productSize = parseInt($("#productSize").text());
			var totalPayMonerySize = obj.find("span[total0=total0]").size();
			var totalPayMonery = parseInt($("#totalPayMonery").text());
			for(var i=0;i<inputBuyNumSize;i++)
			{
				productSize = productSize+parseInt(obj.find("input[name=inputBuyNum]").eq(i).val());
			}
			for(var i=0;i<totalPayMonerySize;i++)
			{
				totalPayMonery = totalPayMonery+parseInt(obj.find("span[total0=total0]").eq(i).text());
			}
			$("#productSize").text(productSize);
		    $("#totalPayMonery").text(totalPayMonery);
		}else{
            obj.find("input[type=checkbox]").prop("checked", false);
			var inputBuyNumSize = obj.find("input[name=inputBuyNum]").size();
			var productSize = parseInt($("#productSize").text());
			var totalPayMonerySize = obj.find("span[total0=total0]").size();
			var totalPayMonery = parseInt($("#totalPayMonery").text());
			for(var i=0;i<inputBuyNumSize;i++)
			{
				productSize = productSize-parseInt(obj.find("input[name=inputBuyNum]").eq(i).val());
			}
			for(var i=0;i<totalPayMonerySize;i++)
			{
				totalPayMonery = totalPayMonery-parseInt(obj.find("span[total0=total0]").eq(i).text());
			}
			$("#productSize").text(productSize);
		    $("#totalPayMonery").text(totalPayMonery);
		}
	});
	

	$("div[address=address]").click(function(){
		$("div[address=address]").removeClass("alert-info");
		
		$(this).addClass("alert-info");
		$(this).find("input[type=radio]").attr("checked",true);
	});
	
	$("#confirmOrderBtn").removeAttr("disabled");
	
	//单个商品选择后计算
	$("input[name=ids]").on("click",function(){
		if($(this).prop("checked")){
			$("#productSize").text(parseInt($("#productSize").text())+parseInt($(this).parent().parent().find("input[name=inputBuyNum]").val()));
		    $("#totalPayMonery").text(parseInt($("#totalPayMonery").text())+parseInt($(this).parent().parent().find("span[total0=total0]").text()));
		    if($(this).parent().parent().parent().parent().find("input[name=ids]").size()==$(this).parent().parent().parent().parent().find("input[name=ids]:checked").size()){
		    	$(this).parent().parent().parent().parent().find(".businessbox").attr("checked",true);
		    }
		}else{
			$("#productSize").text(parseInt($("#productSize").text())-parseInt($(this).parent().parent().find("input[name=inputBuyNum]").val()));
			$("#totalPayMonery").text(parseInt($("#totalPayMonery").text())-parseInt($(this).parent().parent().find("span[total0=total0]").text()));
			 if($(this).parent().parent().parent().parent().find("input[name=ids]").size()!=$(this).parent().parent().parent().parent().find("input[name=ids]:checked").size()){
		    	$(this).parent().parent().parent().parent().find(".businessbox").attr("checked",false);
		    }
		}
	});
});

//单个删除购物车商品
function deleteFromCart(productId){
	if(productId){
	  if(confirm('是否删除购物车中的商品？')){
	 	$("#formDelete :hidden[name=id]").val(productId);
		$("#formDelete").submit();
	  }
	}
}
//批量删除购物车商品
function deletesFromCart(){
	if($("input[name=ids]:checked").size()>0)
	{
		if(confirm('是否删除购物车中的商品？')){
	 		$("form[theme=simple]").attr("action","${systemSetting().card}/cart/deletes");
			$("form[theme=simple]").submit();
	  	}
	}
	else
	{
		alert("请选择商品！");
		return;
	}
}

//提交订单事件
function confirmOrder(){
	var submitFlg = true;
	
	//如果存在错误，则直接抖动
	$("input[name='inputBuyNum']").each(function(){
		var _tips_obj = $(this).parent().find("a[name=stockErrorTips]");
		//if(_tips_obj.is(":visible")){
		console.log(_tips_obj.attr("data-original-title"));
		var _tipsTitle = _tips_obj.attr("data-original-title");
		if(_tipsTitle && _tipsTitle!=''){
			_tips_obj.tooltip('show');
			submitFlg = false;
		}
	});
	console.log("submitFlg="+submitFlg);
	
	//ajax检查购物车中商品的数量是否合法
	var aaa=checkStockLastTime();
	console.log("aaa="+aaa);
	if(!aaa){console.log("not ok");
		return false;
	}
	//submitFlg = false;
	return submitFlg;
}

//添加商品收藏
function addToFavorite(productID,who){
	var _url = "${systemSetting().item}/favorite/addFavoriteProduct?id="+productID+"&radom="+Math.random();
	console.log("_url="+_url);
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  success: function(data){
		  console.log("addToFavorite.data="+data);
		  var _result = "商品已成功添加到收藏夹！";
		  if(data=="0"){
			  _result = "商品已成功添加到收藏夹！";
		  }else if(data=='1'){
			  _result = "已添加，无需重复添加！";
		  }else if(data=='-1'){//提示用户要先登陆
			  _result = "使用此功能需要先登陆！";
		  }
		  alert(_result);
		  $(who).text("已收藏");
		  $(who).attr("disabled","disabled");
		  $(who).css("cursor", "default");
		  $(who).removeAttr('onclick');//去掉a标签中的onclick事件
		  $(who).removeAttr('href');//去掉a标签中的href属性
	  },
	  dataType: "text",
	  error:function(er){
		  console.log("addToFavorite.er="+er);
	  }
	});
}

//批量添加商品收藏
function addToFavorites(who){
	if($("input[name=ids]:checked").size()>0)
	{
		$("form[theme=simple]").attr("action","${systemSetting().item}/favorite/addFavoriteProducts");
		$("form[theme=simple]").submit();
		 alert("收藏成功！");
	}
	else
	{
		alert("请选择商品！");
		return;
	}
	 
}

//去结算按钮点击事件
function toPlacFromCart() {
	//document.placForm.action = "${systemSetting().card}/order/plac";
	//$("#placForm").submit();
	if($("input[name=ids]:checked").size()>0)
	{	
		$("form[theme=simple]").attr("action","${systemSetting().card}/order/plac");
		$("form[theme=simple]").submit();
	} else {
		alert("请选择商品！");
		return;
	}
}
</script>
</@htmlBase.htmlBase>
</body>
</html>