<#import "/shopindex/common/ftl/shopBasePage.ftl" as shopBasePage>
<#import "/shopindex/common/ftl/shopTop.ftl" as shopTop>
<#import "/account/myTop.ftl" as myTop>
<#import "/shopindex/common/ftl/shopFooters.ftl" as shopFooters>
<#import "/account/myLeft.ftl" as myLeft>
<@shopBasePage.shopBasePage/>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${systemSetting().staticSource}/shopindex/common/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/shopindex/layout/css/omt.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery-1.8.2.min.js"></script>
<title>商品评价</title>
</head>
<style type="text/css">
.topCss {
	height: 28px;
	line-height: 28px;
	background-color: #f8f8f8;
	border-bottom: 1px solid #E6E6E6;
	padding-left: 9px;
	font-size: 14px;
	font-weight: bold;
	position: relative;
	margin-top: 0px;
}
.left_product{
	font-size: 12px;display: inline-block;overflow: hidden;text-overflow: ellipsis;-o-text-overflow: ellipsis;white-space: nowrap;max-width: 150px;
}
img.err-product {
background: url(${systemSetting().defaultProductImg}) no-repeat 50% 50%;
}

</style>
<script type="text/javascript">
function checkRate(){
	var context = document.getElementById("context");
	if(context.value.length<=0){
		alert("您还没有进行过任何点评！");
		return false;
	}
	return true;
}
</script>
<!--商城主页top页-->
<@shopTop.shopTop/>
<@myTop.myTop/>
	<div class="container" style="text-align:center">
		<div class="row">
			<div class="col-xs-12">
				<#if e.rateOrderdetailList??>
                    <p class="text-success">您可以对下面的商品进行点评，点评后还可以获得一定的积分哦!</p>
                    <hr style="margin-top: 10px;">

                    <form action="${systemSetting().orders}/order/doRate?orderid=${e.id!""}" id="form" method="post" onsubmit="return checkRate();">
						<#list e.rateOrderdetailList as item>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="media">

                                        <a class="pull-left" href="${systemSetting().item}/product/${item.productID!""}" target="_blank" title="${item.productName!""}">
                                            <img class="media-object err-product" style="width: 100px;height: 100px;border: 0px;" alt="" src="${systemSetting().imageRootPath}/${item.picture!""}" onerror="nofind()"/>
                                        </a>
                                        <div class="media-body">
                                            <textarea id="context" class="form-control" name="product_${item.productID!""}" rows="4" cols="80"></textarea>
                                            <h5 class="media-heading">
											<input type="file" name="commentImage_${item.productID!""}" id="commentImage"  value=" 选择图片   " accept="image/jpeg,image/gif,image/x-ms-bmp,image/x-png" ></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
						</#list>

                        <div style="text-align: center;">
                            <input type="submit" id="rateBtn2" class="btn btn-primary" value="我来点评"/>
                        </div>
                    </form>
				<#else>
                    <p class="text-success">感谢您的评价!</p>
                    <hr style="margin-top: 10px;">


                    <div class="panel panel-default">
                        <div class="panel-body" style="font-size: 16px;font-weight: normal;text-align: center;">
                            <div class="panel-body" style="font-size: 16px;font-weight: normal;text-align: center;">
                                <div class="text-success" style="font-size: 16px;font-weight: 700;">
                                    <span class="glyphicon glyphicon-ban-circle"></span>&nbsp;您好，您当前的订单已经被点评过了！
                                </div>
                            </div>
                        </div>
                    </div>
				</#if>
			</div>
		</div>
	</div>
	<!-- 商城主页footer页 -->
<@shopFooters.shopFooters/>

