package com.soloyogame.anitoys.db.commond;

/**
 * 商品库存变化 必输 orderId
 *
 * @author wuxiongxiong 2015年12月19日下午1:32:22
 */
public class ProductStock {

    private int productID;             // 购买商品ID  
    private int productBuyNumber;      // 购买商品数量
    private String orderId;  			   // t_order.id  订单编号   （必输）

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getProductBuyNumber() {
        return productBuyNumber;
    }

    public void setProductBuyNumber(int productBuyNumber) {
        this.productBuyNumber = productBuyNumber;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

}
