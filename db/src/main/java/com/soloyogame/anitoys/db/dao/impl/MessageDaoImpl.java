package com.soloyogame.anitoys.db.dao.impl;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.Message;
import com.soloyogame.anitoys.db.dao.MessageDao;
import com.soloyogame.anitoys.db.page.PagerModel;

import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.List;

@Repository
public class MessageDaoImpl implements MessageDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    @Override
    public int insert(Message e) {
        return dao.insert("manage.message.insert", e);
    }

    @Override
    public int delete(Message e) {
        return dao.delete("manage.message.delete", e);
    }

    @Override
    public int update(Message e) {
        return dao.update("manage.message.update", e);
    }

    public int deletes(String[] ids) {
        Message e = new Message();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    @Override
    public Message selectOne(Message e) {
        return (Message) dao.selectOne("manage.message.selectOne", e);
    }

    @Override
    public PagerModel selectPageList(Message e) {
        return dao.selectPageList("manage.message.selectPageList", "manage.message.selectPageCount", e);
    }

    @Override
    public List<Message> selectList(Message e) {
        return dao.selectList("manage.message.selectList", e);
    }

    @Override
    public int deleteById(int id) {
        return dao.delete("manage.message.deleteById", id);
    }

    @Override
    public Message selectById(String id) {
        return (Message) dao.selectOne(id);
    }
    
    /**
     * 个人中心前端查询消息列表
     */
	public PagerModel selectFrontPageList(Message e) 
	{
		return dao.selectPageList("front.message.selectPageList",
				"front.message.selectPageCount", e);
	}

}
