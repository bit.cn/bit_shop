package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;
import java.util.Date;

import com.soloyogame.anitoys.db.page.PagerModel;


/**
 * 优惠券编号表
 * @author shaojian
 */
public class CouponSn extends PagerModel implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private String id;
	private String couponSn;    	                //优惠券号码
	private String couponName;                      //优惠券名称
	private Date receiveTime;                     //优惠券领取时间
	private String isOnline;                        //线上领取还是线下领取（1：线上领取，2：线下领取）
	private int couponType;                         //优惠券类型
	private int receiveStatus;                      //领取状态(1:已经领取2：未领取)
	private String couponSnNum;                     //优惠券编号编码    
	private String businessId;                      //商家ID
	
	
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getCouponSnNum() {
		return couponSnNum;
	}
	public void setCouponSnNum(String couponSnNum) {
		this.couponSnNum = couponSnNum;
	}
	public int getCouponType() {
		return couponType;
	}
	public void setCouponType(int couponType) {
		this.couponType = couponType;
	}
	public int getReceiveStatus() {
		return receiveStatus;
	}
	public void setReceiveStatus(int receiveStatus) {
		this.receiveStatus = receiveStatus;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCouponSn() {
		return couponSn;
	}
	public void setCouponSn(String couponSn) {
		this.couponSn = couponSn;
	}
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public Date getReceiveTime() {
		return receiveTime;
	}
	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}
	public String getIsOnline() {
		return isOnline;
	}
	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}
}
