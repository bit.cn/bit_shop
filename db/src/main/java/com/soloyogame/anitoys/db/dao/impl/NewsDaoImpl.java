package com.soloyogame.anitoys.db.dao.impl;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.dao.NewsDao;
import com.soloyogame.anitoys.db.page.PagerModel;

import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.List;

@Repository
public class NewsDaoImpl implements NewsDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    @Override
	public void updateDownOrUp(News news) {
		dao.update("manage.news.updateDownOrUp",news);
	}

    @Override
	public int selectCount(News news) {
		return (Integer) dao.selectOne("manage.news.selectCount",news);
	}

    public PagerModel selectPageList(News e) {
        return dao.selectPageList("manage.news.selectPageList",
                "manage.news.selectPageCount", e);
    }

    /**
     * 加载商家的新闻列表
     */
    public List selectList(News e) {
        return dao.selectList("manage.news.selectList", e);
    }

    /**
     * 加载平台的新闻列表
     */
    public List<News> selectPlatList(News e) {
        return dao.selectList("manage.platnews.selectList", e);
    }

    public News selectOne(News e) {
        return (News) dao.selectOne("manage.news.selectOne", e);
    }

    public int delete(News e) {
        return dao.delete("manage.news.delete", e);
    }

    public int update(News e) {
        return dao.update("manage.news.update", e);
    }

    /**
     * 批量删除用户
     *
     * @param ids
     */
    public int deletes(String[] ids) {
        News e = new News();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(News e) {
        return dao.insert("manage.news.insert", e);
    }

    /**
     * @param bInfo
     */
    public List<News> getLoseList(News bInfo) {
        return dao.selectList("manage.news.getLoseList", bInfo);
    }

    @Override
    public List<News> selecIndexNews(News e) {
        return dao.selectList("manage.news.selecIndexNews", e);
    }

    @Override
    public int deleteById(int id) {
        return dao.delete("manage.news.deleteById", id);
    }

    @Override
    public List<String> selectAllMd5() {
        return dao.selectList("manage.news.selectAllMd5");
    }

    @Override
    public void updateInBlackList(String e) {
        dao.update("updateInBlackList", e);
    }

    @Override
    public void sync(News news) {
        dao.update("manage.news.sync", news);
    }

    public News selectPlatById(String id) {
        return (News) dao.selectOne("manage.platnews.selectById", id);
    }

    public News selectById(String id) {
        return (News) dao.selectOne("manage.news.selectById", id);
    }

    @Override
    public List<News> selectNoticeList(News news) {
        return dao.selectList("manage.news.selectNoticeList", news);
    }

    @Override
    public List<News> selectPlatNoticeList(News news) {
        return dao.selectList("manage.platnews.selectNoticeList", news);
    }

    @Override
    public News selectSimpleOne(News news) {
        return (News) dao.selectOne("manage.news.selectSimpleOne", news);
    }

    /**
     * 查询平台文章
     *
     * @param news
     * @return
     */
    public News selectPlatNewsById(String id) {
        return (News) dao.selectOne("manage.platnews.selectById", id);
    }

    /**
     * 平台的文章分页查询
     *
     * @param e
     * @return
     */
    public PagerModel selectPlatNewsPageList(News e) {
        return dao.selectPageList("manage.platnews.selectPageList",
                "manage.platnews.selectPageCount", e);
    }
    
    /**
	 * 商家首页加载商家的新闻列表
	 */
	public List<News> selectFrontList(News e) 
	{
		return dao.selectList("front.news.selectList", e);
	}

}
