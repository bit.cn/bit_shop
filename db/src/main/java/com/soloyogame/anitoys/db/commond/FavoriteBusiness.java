package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;

public class FavoriteBusiness extends com.soloyogame.anitoys.db.bean.FavoriteBusiness implements Serializable {

    private static final long serialVersionUID = 1L;
    private Product product;

    public void clear() {
        super.clear();
        if (product != null) {
            product.clear();
        }
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
