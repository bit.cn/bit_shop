package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.dao.PlatNewsDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

@Repository("platnewsDao")
public class PlatNewsDaoImpl implements PlatNewsDao {

    @Resource
    private BaseDao dao;
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    @Override
	public void updateDownOrUp(News news) {
    	redisCacheProvider.put(ManageContainer.CACHE_NOTICE_LIST, null);
		dao.update("manage.platnews.updateDownOrUp",news);
	}

    @Override
	public int selectCount(News news) {
		return (Integer) dao.selectOne("manage.platnews.selectCount",news);
	}

    public PagerModel selectPageList(News e) {
        return dao.selectPageList("manage.platnews.selectPageList",
                "manage.platnews.selectPageCount", e);
    }

    /**
     * 加载商家的新闻列表
     */
    public List selectList(News e) {
        return dao.selectList("manage.platnews.selectList", e);
    }

    /**
     * 加载平台的新闻列表
     */
    public List<News> selectPlatList(News e) {
        return dao.selectList("manage.platnews.selectList", e);
    }

    public News selectOne(News e) {
        return (News) dao.selectOne("manage.platnews.selectOne", e);
    }

    public int delete(News e) {
    	redisCacheProvider.put(ManageContainer.CACHE_NOTICE_LIST, null);
        return dao.delete("manage.platnews.delete", e);
    }

    public int update(News e) {
    	redisCacheProvider.put(ManageContainer.CACHE_NOTICE_LIST, null);
        return dao.update("manage.platnews.update", e);
    }

    /**
     * 批量删除用户
     *
     * @param ids
     */
    public int deletes(String[] ids) {
        News e = new News();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(News e) {
    	redisCacheProvider.put(ManageContainer.CACHE_NOTICE_LIST, null);
        return dao.insert("manage.platnews.insert", e);
    }

    /**
     * @param bInfo
     */
    public List<News> getLoseList(News bInfo) {
        return dao.selectList("manage.platnews.getLoseList", bInfo);
    }

    @Override
    public List<News> selecIndexNews(News e) {
        return dao.selectList("manage.platnews.selecIndexNews", e);
    }

    @Override
    public int deleteById(int id) {
    	redisCacheProvider.put(ManageContainer.CACHE_NOTICE_LIST, null);
        return dao.delete("manage.platnews.deleteById", id);
    }

    @Override
    public List<String> selectAllMd5() {
        return dao.selectList("manage.platnews.selectAllMd5");
    }

    @Override
    public void updateInBlackList(String e) {
    	redisCacheProvider.put(ManageContainer.CACHE_NOTICE_LIST, null);
        dao.update("updateInBlackList", e);
    }

    @Override
    public void sync(News news) {
    	redisCacheProvider.put(ManageContainer.CACHE_NOTICE_LIST, null);
        dao.update("manage.platnews.sync", news);
    }

    public News selectPlatById(String id) {
        return (News) dao.selectOne("manage.platnews.selectById", id);
    }

    @Override
    public List<News> selectNoticeList(News news) {
        return dao.selectList("manage.platnews.selectNoticeList", news);
    }

    @Override
    public List<News> selectPlatNoticeList(News news) {
        return dao.selectList("manage.platnews.selectNoticeList", news);
    }

    @Override
    public News selectSimpleOne(News news) {
        return (News) dao.selectOne("manage.platnews.selectSimpleOne", news);
    }

    /**
     * 查询平台文章
     *
     * @param news
     * @return
     */
    public News selectPlatNewsById(String id) {
        return (News) dao.selectOne("manage.platnews.selectById", id);
    }

    @Override
    public News selectById(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PagerModel selectPlatNewsPageList(News news) {
        // TODO Auto-generated method stub
        return null;
    }

}
