package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;

public class OrderWork extends com.soloyogame.anitoys.db.bean.OrderWork implements Serializable {
    private static final long serialVersionUID = 1L;
    private String parentId;                //父订单编号

    private String firstExchangeScore; //买家第一次积分使用
    private String secondExchangeScore;//买家第二次积分使用
    private String firstDeductible;//买家第一次平台抵扣券的抵扣
    private String secondDeductible;//买家第二次平台抵扣券的抵扣
    private String firstAmountCount;//买家第一次站内余额使用
    private String secondAmountCount;//买家第二次站内余额使用
    private String firstPayAccount;//买家第一次现金支付金额
    private String secondPayAcount;//买家第二次现金支付金额

    public String getSecondPayAcount() {
        return secondPayAcount;
    }

    public void setSecondPayAcount(String secondPayAcount) {
        this.secondPayAcount = secondPayAcount;
    }

    public String getFirstPayAccount() {
        return firstPayAccount;
    }

    public void setFirstPayAccount(String firstPayAccount) {
        this.firstPayAccount = firstPayAccount;
    }

    public String getSecondAmountCount() {
        return secondAmountCount;
    }

    public void setSecondAmountCount(String secondAmountCount) {
        this.secondAmountCount = secondAmountCount;
    }

    public String getFirstAmountCount() {
        return firstAmountCount;
    }

    public void setFirstAmountCount(String firstAmountCount) {
        this.firstAmountCount = firstAmountCount;
    }

    public String getSecondDeductible() {
        return secondDeductible;
    }

    public void setSecondDeductible(String secondDeductible) {
        this.secondDeductible = secondDeductible;
    }

    public String getFirstDeductible() {
        return firstDeductible;
    }

    public void setFirstDeductible(String firstDeductible) {
        this.firstDeductible = firstDeductible;
    }

    public String getSecondExchangeScore() {
        return secondExchangeScore;
    }

    public void setSecondExchangeScore(String secondExchangeScore) {
        this.secondExchangeScore = secondExchangeScore;
    }

    public String getFirstExchangeScore() {
        return firstExchangeScore;
    }

    public void setFirstExchangeScore(String firstExchangeScore) {
        this.firstExchangeScore = firstExchangeScore;
    }

    public void clear() {
        super.clear();
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }


}
