package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;

/**
 * 优惠券
 *
 * @author shaojian
 */
public class Coupon extends com.soloyogame.anitoys.db.bean.Coupon implements Serializable 
{

    private static final long serialVersionUID = 1L;

    private String account;

    private String expireTime;
    
    private String usestatus;     //优惠券使用状态(0.待领取1.已领取2.已使用3.已过期)

    
	public String getUsestatus() {
		return usestatus;
	}

	public void setUsestatus(String usestatus) {
		this.usestatus = usestatus;
	}

	public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }
}
