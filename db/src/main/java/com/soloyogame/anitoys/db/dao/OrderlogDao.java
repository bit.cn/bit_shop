package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.AccountPointsLog;
import com.soloyogame.anitoys.db.commond.AccountRankLog;
import com.soloyogame.anitoys.db.commond.Orderlog;

public interface OrderlogDao extends DaoManager<Orderlog> {

    /**
     * 订单日志记录
     */
    public int addOrderlog(Orderlog orderlog);

    int selectCount(Orderlog orderlog);

    int addAccountPointsLog(AccountPointsLog accountPointsLog);

    int addAccountRankLog(AccountRankLog accountRankLog);
}
