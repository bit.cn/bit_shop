package com.soloyogame.anitoys.db.bean;

import java.util.ArrayList;
import java.util.List;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * （新老项目字段一致） 新版 字典项
 *
 * @author shaojian
 */
public class DictItem extends PagerModel {
    //orm相关属性

    private String id;
    private String pid;
    private String dic_id;
    //自含属性
    private String item_code; //代码 如: m
    private String item_name; //内容 如：男
    private String item_value;//本系统值   如: 1
    private String core_value;//核心系统对应值 如:001
    private String item_extra;
    private int order_no;     //排序

    private List<DictItem> children;// 子集合

    //private Dict dict;
    /**
     * 获取下级子项列表
     */
    public List<DictItem> getChildItemList(Dict dict) {
        List<DictItem> children = new ArrayList();
        for (DictItem item : dict.getItems()) {
            if (item.getPid().equals(this.getId())) {
                children.add(item);
            }
        }
        return children;
    }

    /**
     * 获取下级子项通过代码
     *
     * @param dict
     * @param childCode
     * @return
     */
    public DictItem getChildItemByCode(Dict dict, String childCode) {
        List<DictItem> children = this.getChildItemList(dict);
        DictItem dictItem = new DictItem();
        for (DictItem item : children) {
            if (item.getItem_code().equals(childCode)) {
                dictItem = item;
                break;
            }
        }
        return dictItem;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_value() {
        return item_value;
    }

    public void setItem_value(String item_value) {
        this.item_value = item_value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getDic_id() {
        return dic_id;
    }

    public void setDic_id(String dic_id) {
        this.dic_id = dic_id;
    }

    public String getCore_value() {
        return core_value;
    }

    public void setCore_value(String core_value) {
        this.core_value = core_value;
    }

    public String getItem_extra() {
        return item_extra;
    }

    public void setItem_extra(String item_extra) {
        this.item_extra = item_extra;
    }

    public int getOrder_no() {
        return order_no;
    }

    public void setOrder_no(int order_no) {
        this.order_no = order_no;
    }

    public List<DictItem> getChildren() {
        return children;
    }

    public void setChildren(List<DictItem> children) {
        this.children = children;
    }

}
