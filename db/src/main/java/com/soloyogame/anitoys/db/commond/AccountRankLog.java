package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;

import com.soloyogame.anitoys.db.page.PagerModel;

public class AccountRankLog extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;     			//主键id
    private String userId;			//用户id
    private int amountType;			//余额消费类型(暂不用)
    private double amountCount;		//余额变动值
    private int isOrder;			//是否订单生成(0.否  1.是)
    private String orderId;			//订单id
    private String description;		//描述
    private String remark;			//管理员增加的备注
    private String remarkUserId;	//修改余额的用户id
    private String remarkUserName;	//修改余额的用户名
    private String bussinessId;		//修改余额的商家编号
    private String businessName;	//修改余额的商家名
    private String addTime;			//添加的时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getAmountType() {
        return amountType;
    }

    public void setAmountType(int amountType) {
        this.amountType = amountType;
    }

    public double getAmountCount() {
        return amountCount;
    }

    public void setAmountCount(double amountCount) {
        this.amountCount = amountCount;
    }

    public int getIsOrder() {
        return isOrder;
    }

    public void setIsOrder(int isOrder) {
        this.isOrder = isOrder;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemarkUserId() {
        return remarkUserId;
    }

    public void setRemarkUserId(String remarkUserId) {
        this.remarkUserId = remarkUserId;
    }

    public String getRemarkUserName() {
        return remarkUserName;
    }

    public void setRemarkUserName(String remarkUserName) {
        this.remarkUserName = remarkUserName;
    }

    public String getBussinessId() {
        return bussinessId;
    }

    public void setBussinessId(String bussinessId) {
        this.bussinessId = bussinessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }
}
