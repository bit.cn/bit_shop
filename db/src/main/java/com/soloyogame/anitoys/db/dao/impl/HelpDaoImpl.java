package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.Help;
import com.soloyogame.anitoys.db.dao.HelpDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

import javax.annotation.Resource;

/**
 * 帮助支持前端显示
 * @author 索罗游
 */
@Repository("helpDaoManage")
public class HelpDaoImpl implements HelpDao 
{

    @Resource
    private BaseDao dao;
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public int update(Help e) {
    	redisCacheProvider.put(ManageContainer.CACHE_HELPLIST, null);
        return dao.update("manage.help.update", e);
    }

    /**
     * 批量删除用户
     *
     * @param ids
     */
    public int deletes(String[] ids) {
        Help e = new Help();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Help e) {
    	redisCacheProvider.put(ManageContainer.CACHE_HELPLIST, null);
        return dao.insert("manage.help.insert", e);
    }

    public int deleteById(int id) {
    	redisCacheProvider.put(ManageContainer.CACHE_HELPLIST, null);
        return dao.delete("manage.help.deleteById", id);
    }

    public Help selectById(String id) {
        return (Help) dao.selectOne("manage.help.selectById", id);
    }

    public int selectCount(Help help) {
        return (Integer) dao.selectOne("manage.help.selectCount", help);
    }

    public PagerModel selectPageList(Help e) {
        return dao.selectPageList("manage.help.selectPageList",
                "manage.help.selectPageCount", e);
    }

    public List selectList(Help e) {
        return dao.selectList("manage.help.selectList", e);
    }

    public Help selectOne(Help e) {
        return (Help) dao.selectOne("manage.help.selectOne", e);
    }

    public int delete(Help e) {
    	redisCacheProvider.put(ManageContainer.CACHE_HELPLIST, null);
        return dao.delete("manage.help.delete", e);
    }

    /**
     * @param bInfo
     */
    public List<Help> getLoseList(Help bInfo) {
        return dao.selectList("manage.help.getLoseList", bInfo);
    }

    @Override
    public List<Help> selecIndexHelp(Help e) {
        return dao.selectList("manage.help.selecIndexNews", e);
    }

    @Override
    public void sync(Help help) {
    	redisCacheProvider.put(ManageContainer.CACHE_HELPLIST, null);
        dao.update("manage.help.sync", help);
    }

    @Override
    public void updateDownOrUp(Help help) {
    	redisCacheProvider.put(ManageContainer.CACHE_HELPLIST, null);
    	dao.update("manage.help.updateDownOrUp",help);
    }
    
    /**
     * 前端查询帮助支持列表
     * @param e
     * @return
     */
    public List<Help> selectFrontList(Help e) {
		return dao.selectList("front.help.selectList", e);
	}
}
