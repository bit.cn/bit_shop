package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 店铺收藏表（新老项目字段一致）
 *
 * @author wuxiongxiong 2015年12月9日下午8:09:45
 */
public class FavoriteBusiness extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;

//	private int id ;  				//主键id
    private String account;  		//会员账号
    private String accountId;//会员id
    private String businessId; 	//商家编号
    private String businessName;	//商家名称
    private String keywords;		//文章关键字
    private String description;	//文章描述
    private String addTime; 		//商品添加时间
    private String editTime;		//商品编辑时间

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getEditTime() {
        return editTime;
    }

    public void setEditTime(String editTime) {
        this.editTime = editTime;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

}
