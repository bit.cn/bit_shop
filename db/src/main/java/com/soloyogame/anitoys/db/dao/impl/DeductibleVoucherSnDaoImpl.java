/**
 * 
 */
package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.DeductibleVoucherSn;
import com.soloyogame.anitoys.db.dao.DeductibleVoucherSnDao;
import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * @author kacey
 * 
 */
@Repository("deductibleVoucherSnDao")
public class DeductibleVoucherSnDaoImpl implements DeductibleVoucherSnDao {
	@Resource
	private BaseDao dao;

	public void setDao(BaseDao dao) {
		this.dao = dao;
	}

	@Override
	public int insert(DeductibleVoucherSn e) {
		//设置编号
		return dao.insert("manage.deductibleVoucherSn.insert", e);
	}

	@Override
	public int delete(DeductibleVoucherSn e) {
		return dao.delete("manage.deductibleVoucherSn.delete", e);
	}

	@Override
	public int update(DeductibleVoucherSn e) {
		return dao.update("manage.deductibleVoucherSn.update", e);
	}

	@Override
	public DeductibleVoucherSn selectOne(DeductibleVoucherSn e) {
		return (DeductibleVoucherSn) dao.selectOne("manage.deductibleVoucherSn.selectOne", e);
	}

	@Override
	public PagerModel selectPageList(DeductibleVoucherSn e) {
		return dao.selectPageList("manage.deductibleVoucherSn.selectPageList",
                "manage.deductibleVoucherSn.selectPageCount", e);
	}

	@Override
	public List<DeductibleVoucherSn> selectList(DeductibleVoucherSn e) {
		 return dao.selectList("manage.deductibleVoucherSn.selectList", e);
	}

	@Override
	public int deleteById(int id) {
		 return dao.delete("manage.deductibleVoucherSn.deleteById", id);
	}

	@Override
	public DeductibleVoucherSn selectById(String id) {
		return (DeductibleVoucherSn) dao.selectOne("manage.deductibleVoucherSn.selectById", id);
	}
	
    public int deletes(String[] ids) {
    	DeductibleVoucherSn e = new DeductibleVoucherSn();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

	/**
	 * 查找一条有效的优惠券
	 * @param e
	 * @return
	 */
	@Override
	public DeductibleVoucherSn findEffectiveDeductibleSnCode(DeductibleVoucherSn e) {
		return (DeductibleVoucherSn) dao.selectOne("front.deductibleSn.findEffectiveDeductibleSnCode", e);
	}
}
