package com.soloyogame.anitoys.db.bean;

//（新老项目字段一致）
import java.io.Serializable;
import com.soloyogame.anitoys.db.QueryModel;

/**
 * 账户安全表
 *
 * @author 邵健
 */
public class AccountSecurity extends QueryModel implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户编号
     */
    private String userId;
    /**
     * 证件类型
     */
    private String cardType;
    /**
     * 证件号码
     */
    private String cardNO;
    /**
     * 用户邮箱
     */
    private String email;
    /**
     * 邮箱是否已激活。y:已激活,n:未激活。默认n
     */
    private String emailIsActive;
    /**
     * 绑定的手机
     */
    private String userPhone;
    /**
     * 真实姓名
     */
    private String trueName;
    /**
     * 账号密码
     */
    private String userPassword;
    /**
     * 用户登录密码盐
     */
    private String userpasswordSalt;
    /**
     * 安全级别
     */
    private String securityLevel;
    /**
     * 修改密码是否发送邮件 0:否 1:是
     */
    private String isUpPwdSend;
    /**
     * 修改手机是否发送邮件
     */
    private String isUpTelSend;

    public String getIsUpPwdSend() {
        return isUpPwdSend;
    }

    public void setIsUpPwdSend(String isUpPwdSend) {
        this.isUpPwdSend = isUpPwdSend;
    }

    public String getIsUpTelSend() {
        return isUpTelSend;
    }

    public void setIsUpTelSend(String isUpTelSend) {
        this.isUpTelSend = isUpTelSend;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNO() {
        return cardNO;
    }

    public void setCardNO(String cardNO) {
        this.cardNO = cardNO;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailIsActive() {
        return emailIsActive;
    }

    public void setEmailIsActive(String emailIsActive) {
        this.emailIsActive = emailIsActive;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserpasswordSalt() {
        return userpasswordSalt;
    }

    public void setUserpasswordSalt(String userpasswordSalt) {
        this.userpasswordSalt = userpasswordSalt;
    }

    public String getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(String securityLevel) {
        this.securityLevel = securityLevel;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
