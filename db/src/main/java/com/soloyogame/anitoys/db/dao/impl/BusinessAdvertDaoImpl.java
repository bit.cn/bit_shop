package com.soloyogame.anitoys.db.dao.impl;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.db.commond.BusinessAdvert;
import com.soloyogame.anitoys.db.dao.BusinessAdvertDao;

import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.List;

@Repository("businessAdvertDaoManage")
public class BusinessAdvertDaoImpl implements BusinessAdvertDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    @Override
    public int insert(BusinessAdvert e) {

        return dao.insert("manage.businessAdvert.insert", e);
    }

    /**
     * 批量删除用户
     *
     * @param ids
     */
    public int deletes(String[] ids) {
        BusinessAdvert e = new BusinessAdvert();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    @Override
    public int delete(BusinessAdvert e) {
        return dao.delete("manage.businessAdvert.delete", e);
    }

    @Override
    public int update(BusinessAdvert e) {
        return dao.update("manage.businessAdvert.update", e);
    }

    @Override
    public BusinessAdvert selectOne(BusinessAdvert e) {
        return (BusinessAdvert) dao.selectOne("manage.businessAdvert.selectOne", e);
    }

    @Override
    public PagerModel selectPageList(BusinessAdvert e) {
        return dao.selectPageList("manage.businessAdvert.selectPageList",
                "manage.businessAdvert.selectPageCount", e);
    }

    @Override
    public List<BusinessAdvert> selectList(BusinessAdvert e) {
        return dao.selectList("manage.businessAdvert.selectList", e);
    }

    @Override
    public int deleteById(int id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public BusinessAdvert selectById(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void deletess(String[] ids) {
        BusinessAdvert e = new BusinessAdvert();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }

    }

}
