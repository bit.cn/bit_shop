package com.soloyogame.anitoys.db.bean;
//新老项目字段一致

import java.io.Serializable;

import com.soloyogame.anitoys.db.page.PagerModel;
/**
 * 商家收藏
 * @author 邵健
 */
public class CollectionBusiness extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String accountId;// 用户ID
    private String businessId;// 收藏的店铺id
    private String businessName;// 收藏的店铺名字
    private String keywords;// 收藏的关键字
    private String description;// 收藏描述
    private String addTime;// 收藏时间
    private String editTime;// 编辑时间

    public void clear() {
        super.clear();
        accountId = null;
        businessId = null;
        businessName = null;
        keywords = null;
        description = null;
        addTime = null;
        editTime = null;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getEditTime() {
        return editTime;
    }

    public void setEditTime(String editTime) {
        this.editTime = editTime;
    }

}
