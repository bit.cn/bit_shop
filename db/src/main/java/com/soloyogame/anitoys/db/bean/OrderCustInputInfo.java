package com.soloyogame.anitoys.db.bean;

public class OrderCustInputInfo {

    private String orderCode; 			//子订单号

    private String expressCode; 		// 配送方式code

    private String expressName; 		// 配送方式Name

    private String fee; 	       		// 配送费

    private String otherRequirement; 	// 子订单备注

    private String ptotal;		        //合计

    private String couponsId;		    //优惠券id

    private String couponValue;		    //优惠券值

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getExpressCode() {
        return expressCode;
    }

    public void setExpressCode(String expressCode) {
        this.expressCode = expressCode;
    }

    public String getExpressName() {
        return expressName;
    }

    public void setExpressName(String expressName) {
        this.expressName = expressName;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getOtherRequirement() {
        return otherRequirement;
    }

    public void setOtherRequirement(String otherRequirement) {
        this.otherRequirement = otherRequirement;
    }

    public String getPtotal() {
        return ptotal;
    }

    public void setPtotal(String ptotal) {
        this.ptotal = ptotal;
    }

    public String getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(String couponsId) {
        this.couponsId = couponsId;
    }

    public String getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(String couponValue) {
        this.couponValue = couponValue;
    }

    @Override
    public String toString() {
        return "OrderCustInputInfo [orderCode=" + orderCode + ", expressCode="
                + expressCode + ", expressName=" + expressName + ", fee=" + fee
                + ", otherRequirement=" + otherRequirement + ", ptotal="
                + ptotal + ", couponsId=" + couponsId + ", couponValue="
                + couponValue + "]";
    }
}
