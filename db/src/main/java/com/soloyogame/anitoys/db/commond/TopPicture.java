package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;

/**
 * 大首页图片实体类
 *
 * @author 索罗游
 */
public class TopPicture extends com.soloyogame.anitoys.db.bean.TopPicture implements Serializable {

    private static final long serialVersionUID = 1L;
}
