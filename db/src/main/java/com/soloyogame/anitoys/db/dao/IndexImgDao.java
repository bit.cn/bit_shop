package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.IndexImg;

import java.util.List;

/**
 * 图片信息DAO接口
 *
 * @author shaojian
 */
public interface IndexImgDao extends DaoManager<IndexImg> {

    /**
     * 加载图片显示到门户
     *
     * @param i
     * @return
     */
    List<IndexImg> getImgsShowToIndex(int i);

    /**
     * 加载平台的门户商品
     *
     * @param i
     * @return
     */
    public List<IndexImg> getPlatImgsShowToIndex(int i);

}
