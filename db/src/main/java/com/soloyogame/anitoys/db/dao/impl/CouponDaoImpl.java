package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.Coupon;
import com.soloyogame.anitoys.db.dao.CouponDao;
import com.soloyogame.anitoys.db.page.PagerModel;


/*
 * 优惠券管理
 * @author kuangy 20151209
 */
@Repository("couponDaoManage")
public class CouponDaoImpl implements CouponDao {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BaseDao dao;

    public PagerModel selectPageList(Coupon e) {
        e.setAuditStatus(1);
        return dao.selectPageList("manage.coupon.selectPageList",
                "manage.coupon.selectPageCount", e);
    }

    public List selectList(Coupon e) {
        return dao.selectList("manage.coupon.selectList", e);
    }

    public Coupon selectOne(Coupon e) {
        return (Coupon) dao.selectOne("manage.coupon.selectOne", e);
    }

    public int delete(Coupon e) {
        return dao.delete("manage.coupon.delete", e);
    }

    public int update(Coupon e) {
        return dao.update("manage.coupon.update", e);
    }

    public int deletes(String[] ids) {
        Coupon e = new Coupon();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Coupon e) {
        return dao.insert("manage.coupon.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.coupon.deleteById", id);
    }

    @Override
    public Coupon selectById(String id) {
        return (Coupon) dao.selectOne("manage.coupon.selectById", id);
    }

    public int updateBySn(Map<String, Object> map) {
        return dao.update("manage.coupon.updateBySn", map);
    }


    /*
	 * 查询可领取的优惠券
	 *
     */
    @Override
    public PagerModel couponReceiveList(Coupon e) {
        return dao.selectPageList("front.coupon.selectCouponReceiveList", "front.coupon.selectCouponCount", e);
    }

    /*
	 * 查询可领取的优惠券(程版)
	 *
     */
    public PagerModel selectCouponByReceive(Coupon e) {
        return dao.selectPageList("front.coupon.selectCouponByReceive", "front.coupon.selectCouponCountByReceive", e);
    }

    /**
     * 领取优惠券(程版)
     */
    public int updateStatus(Coupon e) {
        return dao.update("front.coupon.updateStatus", e);
    }

    /*
	 * 领取优惠券
	 *
     */
    @Override
    public int insertCoupon(Coupon coupon) throws Exception {
        logger.info("CouponDaoImpl.insertCoupon start");

        int insertNo = 0;
        try {
            insertNo = dao.insert("front.coupon.insert", coupon);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.info("CouponDaoImpl.insertCoupon end");

        return insertNo;
    }

    @Override
    public int insertVoucher(Coupon e) throws Exception {
        return dao.insert("front.coupon.insertVoucher", e);
    }

    /**
	 * 查询我的优惠券 
	 */
    @Override
	public PagerModel selectFrontPageList(Coupon e) 
	{
		return dao.selectPageList("front.coupon.selectPageList", "front.coupon.selectPageCount", e);
	}

}
