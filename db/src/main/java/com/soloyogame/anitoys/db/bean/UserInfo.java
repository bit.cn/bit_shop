package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.QueryModel;
/**
 * 用户信息
 * @author 邵健
 */
public class UserInfo extends QueryModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;               //主键ID
    private String username;         //帐号
    private String password;         //密码
    private String createtime;       //创建时间
    private String updatetime;       //最后修改时间
    private String createAccount;    //创建人
    private String updateAccount;    //最后修改人
    private String status;           //状态（y:启用,n:禁用；默认y）
    private String rid;              //角色ID
    private String nickname;         //昵称
    private String email;            //邮箱
    private String mobile;
    private int businessId;

    public void clear() {
        super.clear();
        id = null;               //主键ID
        username = null;         //帐号
        password = null;         //密码
        createtime = null;       //创建时间
        updatetime = null;       //最后修改时间
        createAccount = null;    //创建人
        updateAccount = null;    //最后修改人
        status = "y";           //状态（y:启用,n:禁用；默认y）
        rid = null;              //角色ID
        nickname = null;         //昵称
        email = null;            //邮箱
        mobile = null;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getCreateAccount() {
        return createAccount;
    }

    public void setCreateAccount(String createAccount) {
        this.createAccount = createAccount;
    }

    public String getUpdateAccount() {
        return updateAccount;
    }

    public void setUpdateAccount(String updateAccount) {
        this.updateAccount = updateAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

}
