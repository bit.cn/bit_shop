package com.soloyogame.anitoys.db.commond;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 查询用户收藏店铺返回结果
 *
 * @author wuxiongxiong 2015年12月11日下午8:45:05
 */
public class FavoriteBusinessResult {

    private String id;
    private String productID;				//商品ID
    private String productName; 			//商品名称
    private String nowPrice;				//商品价格
    private String businessPicture; 		//店铺图片
    private String picture; 				//商品图片
    private String businessId;				//店铺ID
    private String businessName;			//店铺名称
    private List<Product> businessProduct;

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getNowPrice() {
        return nowPrice;
    }

    public void setNowPrice(String nowPrice) {
        this.nowPrice = nowPrice;
    }

    public String getBusinessPicture() {
        return businessPicture;
    }

    public void setBusinessPicture(String businessPicture) {
        this.businessPicture = businessPicture;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public List<Product> getBusinessProduct() {
        if (this.businessProduct == null) {
            this.businessProduct = new LinkedList<Product>();
        }
        return businessProduct;
    }

    public void setBusinessProduct(List<Product> businessProduct) {
        this.businessProduct = businessProduct;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
