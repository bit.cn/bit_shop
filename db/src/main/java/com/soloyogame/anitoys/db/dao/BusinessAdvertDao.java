package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.BusinessAdvert;

public interface BusinessAdvertDao extends DaoManager<BusinessAdvert> {

    public void deletess(String[] ids);
}
