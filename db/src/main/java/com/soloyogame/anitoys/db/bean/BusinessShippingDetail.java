package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.QueryModel;

/**
 * 商家物流运费详情实体类（新老项目字段一致）
 *
 * @author shaojian
 */
public class BusinessShippingDetail extends QueryModel implements Serializable {

    private String id;              	//主键ID
    private String provinceId;      	//省份Id
    private String provinceName;    	//省份名称
    private double fristPrice;      	//首重价格
    private double stepPrice;       	//快递增重
    private String firstHeavy;     	//首重
    private String stepHeavy;           //递增规格
    private String businessShippingId;  //商家物流编号

    public String getFirstHeavy() {
        return firstHeavy;
    }

    public void setFirstHeavy(String firstHeavy) {
        this.firstHeavy = firstHeavy;
    }

    public String getStepHeavy() {
        return stepHeavy;
    }

    public void setStepHeavy(String stepHeavy) {
        this.stepHeavy = stepHeavy;
    }

    public String getBusinessShippingId() {
        return businessShippingId;
    }

    public void setBusinessShippingId(String businessShippingId) {
        this.businessShippingId = businessShippingId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public double getFristPrice() {
        return fristPrice;
    }

    public void setFristPrice(double fristPrice) {
        this.fristPrice = fristPrice;
    }

    public double getStepPrice() {
        return stepPrice;
    }

    public void setStepPrice(double stepPrice) {
        this.stepPrice = stepPrice;
    }
}
