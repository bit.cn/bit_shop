package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.db.dao.PlatTopPictureDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

/**
 * 平台首页顶部图片DAO
 * @author 索罗游
 */
@Repository("platTopPictureDao")
public class PlatTopPictureDaoImpl implements PlatTopPictureDao {

    @Resource
    private BaseDao dao;
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public int insert(TopPicture e) {
    	redisCacheProvider.put(ManageContainer.CACHE_TOPPICTURE_PLAT,null);
        return dao.insert("manage.topPictureplat.insert", e);
    }

    public int delete(TopPicture e) {
    	redisCacheProvider.put(ManageContainer.CACHE_TOPPICTURE_PLAT,null);
        return dao.delete("manage.topPictureplat.delete", e);
    }

    public int update(TopPicture e) {
    	redisCacheProvider.put(ManageContainer.CACHE_TOPPICTURE_PLAT,null);
        return dao.update("manage.topPictureplat.update", e);
    }

    public TopPicture selectOne(TopPicture e) {
        return (TopPicture) dao.selectOne("manage.topPictureplat.selectOne", e);
    }

    public PagerModel selectPageList(TopPicture e) {
        return dao.selectPageList("manage.topPictureplat.selectPageList",
                "manage.topPictureplat.selectPageCount", e);
    }

    public List<TopPicture> selectList(TopPicture e) {
        return dao.selectList("manage.topPictureplat.selectList", e);
    }

    public int deleteById(int id) {
    	redisCacheProvider.put(ManageContainer.CACHE_TOPPICTURE_PLAT,null);
        return dao.delete("manage.topPictureplat.deleteById", id);
    }

    public TopPicture selectById(String id) {
        return (TopPicture) dao.selectOne("manage.topPictureplat.selectById", id);
    }
}
