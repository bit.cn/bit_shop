package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.AccountSecurity;
import com.soloyogame.anitoys.db.commond.Product;
import com.soloyogame.anitoys.db.commond.ProductStockInfo;
import com.soloyogame.anitoys.db.dao.ProductDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

@Repository
public class ProductDaoImpl implements ProductDao {

    @Resource
    private BaseDao dao;
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Product e) {
        return dao.selectPageList("manage.product.selectPageList",
                "manage.product.selectPageCount", e);
    }

    public int deleteById(int id) {
    	//清空最新商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
        return dao.delete("manage.product.deleteById", id);
    }

    @Override
    public List<Product> selectParameterList(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProductStockInfo> selectStockList(Product product) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Product> selectListProductHTML(Product product) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Product> selectProductListByIds(Product p) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
	public Product selectByIds(int id) {		
		return (Product) dao.selectOne("front.product.selectByIds", id);
	}

    @Override
    public List<Product> selectHotSearch(Product p) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Product> loadHotProductShowInSuperMenu(Product product) {
        return dao.selectList("front.product.loadHotProductShowInSuperMenu", product);
    }

    @Override
	public void updateHit(Product p) {
		dao.update("front.product.updateHit",p);
		//清空最新商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
		//清空现货商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
		//清空现货商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
	}

    @Override
    public List<Product> selectPageLeftHotProducts(Product product) {
        return dao.selectList("front.product.selectPageLeftHotProducts", product);
    }

    @Override
    public List<Product> selectActivityProductList(Product product) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * 商城商品总数
     */
    public int selectProductCount() 
    {
    	return  (Integer) dao.selectOne("front.product.selectProductCount");
    }

    @Override
    public void deleteAttributeLinkByProductID(int parseInt) {
        dao.delete("manage.product.deleteAttributeLinkByProductID", parseInt);
        //清空最新商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
        //清空现货商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
        //清空现货商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
    }

    //平台的商品DAO
    public List selectList(Product e) {
        return dao.selectList("manage.product.selectList", e);
    }

    public Product selectOne(Product e) {
        return (Product) dao.selectOne("manage.product.selectOne", e);
    }

    public int delete(Product e) {
    	//清空最新商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
        return dao.delete("manage.product.delete", e);
    }

    public int update(Product e) {
    	//清空最新商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
        return dao.update("manage.product.update", e);
    }

    public int deletes(String[] ids) {
        Product e = new Product();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Product e) {
    	//清空最新商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
        return dao.insert("manage.product.insert", e);
    }

    public Product selectById(String id) {
        return (Product) dao.selectOne("manage.product.selectById", id);
    }

    @Override
    public List<Product> selectStockByIDs(List<String> productIDs) {
        return dao.selectList("manage.product.selectStockByIDs", productIDs);
    }

    @Override
    public List<Product> selectProByIDs(List<String> productIDs) {
        return dao.selectList("manage.product.selectProByIDs", productIDs);
    }

    @Override
    public int selectOutOfStockProductCount() {
        return (Integer) dao.selectOne("manage.product.selectOutOfStockProductCount");
    }

    public void updateImg(Product p) {
    	//清空最新商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
    	//清空现货商品缓存
    	redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
        dao.update("manage.product.updateImg", p);
    }

    public void updateProductStatus(Product p) {
        dao.update("manage.product.updateProductStatus", p);
        //清空最新商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
        //清空现货商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
        //清空现货商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);	
    }

    public void updateProductBindActivityId(Product pro) {
        dao.update("manage.product.updateProductBindActivityId", pro);
        //清空最新商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
        //清空现货商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
        //清空现货商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
    }

    public void updateResetThisProductActivityID(String activityID) {
        dao.update("manage.product.updateResetThisProductActivityID", activityID);
        //清空最新商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
        //清空现货商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
        //清空现货商品缓存
        redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
    }

    @Override
	public void updateStockAfterPaySuccess(Product product) {
		dao.update("front.product.updateStockAfterPaySuccess",product);
		//清空最新商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
		//清空现货商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
		//清空现货商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
	}
    
    /**
	 * 更新商品库存
	 * wuxiongxiong
	 * @param ps,商品ID、购买数量
	 * @return
	 */
	@Override
	public int updateProductStock(String orderId) 
	{
		int i = 0;
		dao.update("front.order.updateStock" , orderId);
		//清空最新商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
		//清空现货商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
		//清空现货商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
		return i ;
	}
	
	/**
	 * 刷新商品的销量
	 * @param orderId
	 * @return
	 */
	@Override
	public int updateSellcount(String orderId) 
	{
		int i = 0;
		dao.update("front.order.updateSellcount" , orderId);
		//清空最新商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_NEW_PRODUCTS, null);
		//清空现货商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_PRESALES_PRODUCTLIST, null);
		//清空现货商品缓存
		redisCacheProvider.put(ManageContainer.CACHE_SPOTCOMMONITY_PRODUCTLIST,null);
		return i ;
	}

    //www大平台的查询商品列表
    public List selectFrontProductList(Product e) {
        return dao.selectList("front.product.selectList", e);
    }

    /**
     * list前段查询商品分页列表
     * @param e
     * @return
     */
    public PagerModel selectFrontPageList(Product e) {
		return dao.selectPageList("front.product.selectPageList",
				"front.product.selectPageCount", e);
	}

    /**
     * 根据商品ID得到购买此商品的用户信息（id,email）
     * @param productId
     * @return
     */
	public List<AccountSecurity> selectAccountEmailByProductId(String productId) 
	{
		return dao.selectList("manage.product.selectAccountEmailByProductId",productId);
	}
}
