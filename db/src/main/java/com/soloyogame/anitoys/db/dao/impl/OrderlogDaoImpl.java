package com.soloyogame.anitoys.db.dao.impl;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.AccountPointsLog;
import com.soloyogame.anitoys.db.commond.AccountRankLog;
import com.soloyogame.anitoys.db.commond.Orderlog;
import com.soloyogame.anitoys.db.dao.OrderlogDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class OrderlogDaoImpl implements OrderlogDao {

    @Resource
    private BaseDao dao;

    public PagerModel selectPageList(Orderlog e) {
        return dao.selectPageList("front.orderlog.selectPageList",
                "front.orderlog.selectPageCount", e);
    }

    public List selectList(Orderlog e) {
        return dao.selectList("front.orderlog.selectList", e);
    }

    public Orderlog selectOne(Orderlog e) {
        return (Orderlog) dao.selectOne("front.orderlog.selectOne", e);
    }

    public int delete(Orderlog e) {
        return dao.delete("front.orderlog.delete", e);
    }

    public int update(Orderlog e) {
        return dao.update("front.orderlog.update", e);
    }

    public int deletes(String[] ids) {
        Orderlog e = new Orderlog();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Orderlog e) {
        return dao.insert("front.orderlog.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.orderlog.deleteById", id);
    }

    @Override
    public Orderlog selectById(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int selectCount(Orderlog orderlog) {
        return (Integer) dao.selectOne("front.orderlog.selectCount", orderlog);
    }

    @Override
    public int addAccountPointsLog(AccountPointsLog accountPointsLog) {
        return dao.insert("front.orderlog.addAccountPointsLog", accountPointsLog);
    }

    @Override
    public int addAccountRankLog(AccountRankLog accountRankLog) {
        return dao.insert("front.orderlog.addAccountRankLog", accountRankLog);
    }

    @Override
    public int addOrderlog(Orderlog orderlog) {
        return 0;
    }
}
