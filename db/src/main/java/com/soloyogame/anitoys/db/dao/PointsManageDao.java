package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.AccountPointsLog;

/**
 * 会员账户积分日志
 *
 * @author jiangyongzhi
 */
public interface PointsManageDao extends DaoManager<AccountPointsLog> {

}
