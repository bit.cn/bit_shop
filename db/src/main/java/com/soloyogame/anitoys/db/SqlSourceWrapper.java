/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soloyogame.anitoys.db;

import java.lang.reflect.Field;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.SqlSource;
import org.springframework.util.ReflectionUtils;

/**
 *
 * @author earl.k.wang
 */
public class SqlSourceWrapper implements SqlSource {
        private SqlSource origin;
        public SqlSourceWrapper(SqlSource origin) {
            this.origin = origin;
        }
        @Override
        public BoundSql getBoundSql(Object parameterObject) {
            BoundSql boundSql = origin.getBoundSql(parameterObject);
            String sql = boundSql.getSql();
            Field sqlField = null;
            try {
                sqlField = BoundSql.class.getDeclaredField("sql");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            ReflectionUtils.makeAccessible(sqlField);
            ReflectionUtils.setField(sqlField, boundSql, sql.toLowerCase());
            return boundSql;
        }
    }