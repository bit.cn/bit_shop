package com.soloyogame.anitoys.db.bean;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 平台权限
 *
 * @author shaojian
 */
public class PrivilegePlat extends PagerModel {

    private String id;    		//ID
    private String rid;   		//角色ID
    private String mid;   		//资源ID

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public void clear() {
        // TODO Auto-generated method stub
        this.id = null;
        this.mid = null;
        this.rid = null;
    }

}
