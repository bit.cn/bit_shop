package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.UserInfo;
import com.soloyogame.anitoys.db.dao.UserInfoDao;
import com.soloyogame.anitoys.db.page.PagerModel;

@Repository("userInfoDaoManage")
public class UserInfoDaoImpl implements UserInfoDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    @Override
    public int insert(UserInfo e) {
        return dao.insert("manage.userInfo.insert", e);
    }

    @Override
    public int delete(UserInfo e) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int update(UserInfo e) {
        return dao.update("manage.userInfo.update", e);
    }

    @Override
    public UserInfo selectOne(UserInfo e) {
        return (UserInfo) dao.selectOne("manage.userInfo.selectOne", e);
    }

    @Override
    public PagerModel selectPageList(UserInfo e) {
        return dao.selectPageList("manage.userInfo.selectPageList",
                "manage.userInfo.selectPageCount", e);
    }

    @Override
    public List<UserInfo> selectList(UserInfo e) {
        return dao.selectList("manage.userInfo.selectList", e);
    }

    @Override
    public UserInfo selectById(String id) {
        return (UserInfo) dao.selectOne("manage.userInfo.selectById", id);
    }

    @Override
    public int insertByBusiness(UserInfo e) {
        return dao.insert("manage.userInfo.insertByBusiness", e);
    }

    @Override
    public int deleteById(int id) {
        return 0;
    }

}
