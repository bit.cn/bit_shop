package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.page.PagerModel;
//新老项目字段一致
/**
 * 平台推荐商品
 * @author 邵健
 */
public class PlatRecommend extends PagerModel implements Serializable {

    private String name;           //商品名称
    private String nowPrice;       //现价
    private String picture;        //小图片地址
    private String productId;      //商品Id
    private int type;              //1：推荐商品  2：推荐商品
    private String businessId;//店铺id

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNowPrice() {
        return nowPrice;
    }

    public void setNowPrice(String nowPrice) {
        this.nowPrice = nowPrice;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

}
