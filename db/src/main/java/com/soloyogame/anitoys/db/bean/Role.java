package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 角色（新老项目字段一致）
 *
 * @author shaojian
 */
public class Role extends PagerModel implements Serializable {

    private String id;                            //ID
    private String role_name;                     //角色名称
    private String role_desc;                     //角色描述
    private String role_dbPrivilege;              //数据库权限
    private String status;                        //角色状态，如果角色被禁用，则该角色下的所有的账号都不能使用，除非角色被解禁。(y:启用，n:禁用；默认y)

    public static final String role_status_y = "y";//启用
    public static final String role_status_n = "n";//禁用

    private String privileges;
    private String insertOrUpdate = "1";           // 1:新增,2:修改

    private String businessId;

    public void clear() {
        id = null;
        role_name = null;
        role_desc = null;
        role_dbPrivilege = null;
        status = null;

        privileges = null;
        insertOrUpdate = null;
        businessId = null;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getRole_dbPrivilege() {
        return role_dbPrivilege;
    }

    public void setRole_dbPrivilege(String role_dbPrivilege) {
        this.role_dbPrivilege = role_dbPrivilege;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getRole_desc() {
        return role_desc;
    }

    public void setRole_desc(String role_desc) {
        this.role_desc = role_desc;
    }

    public String getPrivileges() {
        return privileges;
    }

    public void setPrivileges(String privileges) {
        this.privileges = privileges;
    }

    public String getInsertOrUpdate() {
        return insertOrUpdate;
    }

    public void setInsertOrUpdate(String insertOrUpdate) {
        this.insertOrUpdate = insertOrUpdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
