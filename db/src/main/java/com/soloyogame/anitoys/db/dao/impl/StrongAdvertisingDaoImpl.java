package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.db.bean.StrongAdvertising;
import com.soloyogame.anitoys.db.dao.StrongAdvertisingDao;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

@Repository("strongAdvertisingDaoManage")
public class StrongAdvertisingDaoImpl implements StrongAdvertisingDao {

    @Resource
    private BaseDao dao;
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public int insert(StrongAdvertising e) {
    	redisCacheProvider.put(ManageContainer.CACHE_STRONGADVERTISSING_LIST,null);
        return dao.insert("manage.strongAdvertising.insert", e);
    }

    /**
     * 批量删除用户
     *
     * @param ids
     */
    public int deletes(String[] ids) {
        StrongAdvertising e = new StrongAdvertising();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int delete(StrongAdvertising e) {
    	redisCacheProvider.put(ManageContainer.CACHE_STRONGADVERTISSING_LIST,null);
        return dao.delete("manage.strongAdvertising.delete", e);
    }

    public int update(StrongAdvertising e) {
    	redisCacheProvider.put(ManageContainer.CACHE_STRONGADVERTISSING_LIST,null);
        return dao.update("manage.strongAdvertising.update", e);
    }

    public StrongAdvertising selectOne(StrongAdvertising e) {
        return (StrongAdvertising) dao.selectOne("manage.strongAdvertising.selectOne", e);
    }

    public PagerModel selectPageList(StrongAdvertising e) {
        return dao.selectPageList("manage.strongAdvertising.selectPageList",
                "manage.strongAdvertising.selectPageCount", e);
    }

    public List<StrongAdvertising> selectList(StrongAdvertising e) {
        return dao.selectList("manage.strongAdvertising.selectList", e);
    }

    public int deleteById(int id) {
        return 0;
    }

    public StrongAdvertising selectById(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    public void deletess(String[] ids) {
        StrongAdvertising e = new StrongAdvertising();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }

    }

}
