package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 抵扣券（新老项目字段一致）
 *
 * @author shaojian
 */
public class DeductibleVoucher extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;
    //private String id;
    private String deductibleSn;//抵扣码
    private String deductibleName;//抵扣券名称
    private int deductibleType;//抵扣券类型
    private String deductibleValue;//抵扣券值
    private String deductibleRule;//抵扣规则
    private String receiveTime;//发放时间
    private String endReceiveTime;//结束领取时间
    private String startTime;//有效期开始日期
    private String endTime;//有效期结束日期
    private int isUse;//是否可用
    private int deductibleCount;//发行数量
    private int receiveCount;//领取数量
    private int useCount;//使用数量
    private String addTime;//添加时间
    private String editTime;//修改时间

    public void clear() {
        super.clear();
        //id = null;
        deductibleSn = null;
        deductibleName = null;
        deductibleType = 0;
        deductibleValue = null;
        deductibleRule = null;
        receiveTime = null;
        endReceiveTime = null;
        startTime = null;
        endTime = null;
        isUse = 0;
        deductibleCount = 0;
        receiveCount = 0;
        useCount = 0;
        addTime = null;
        editTime = null;
    }

    public String getDeductibleSn() {
        return deductibleSn;
    }

    public void setDeductibleSn(String deductibleSn) {
        this.deductibleSn = deductibleSn;
    }

    public String getDeductibleName() {
        return deductibleName;
    }

    public void setDeductibleName(String deductibleName) {
        this.deductibleName = deductibleName;
    }

    public int getDeductibleType() {
        return deductibleType;
    }

    public void setDeductibleType(int deductibleType) {
        this.deductibleType = deductibleType;
    }

    public String getDeductibleValue() {
        return deductibleValue;
    }

    public void setDeductibleValue(String deductibleValue) {
        this.deductibleValue = deductibleValue;
    }

    public String getDeductibleRule() {
        return deductibleRule;
    }

    public void setDeductibleRule(String deductibleRule) {
        this.deductibleRule = deductibleRule;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getEndReceiveTime() {
        return endReceiveTime;
    }

    public void setEndReceiveTime(String endReceiveTime) {
        this.endReceiveTime = endReceiveTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getIsUse() {
        return isUse;
    }

    public void setIsUse(int isUse) {
        this.isUse = isUse;
    }

    public int getDeductibleCount() {
        return deductibleCount;
    }

    public void setDeductibleCount(int deductibleCount) {
        this.deductibleCount = deductibleCount;
    }

    public int getReceiveCount() {
        return receiveCount;
    }

    public void setReceiveCount(int receiveCount) {
        this.receiveCount = receiveCount;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getEditTime() {
        return editTime;
    }

    public void setEditTime(String editTime) {
        this.editTime = editTime;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
