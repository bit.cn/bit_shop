package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.PopularityAdvert;

public interface PopularityAdvertDao extends DaoManager<PopularityAdvert> {

    public void deletess(String[] ids);

    //front selectOne
    public PopularityAdvert frontselectOne(PopularityAdvert e);
}
