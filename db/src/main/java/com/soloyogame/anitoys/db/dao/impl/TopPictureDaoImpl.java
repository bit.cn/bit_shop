package com.soloyogame.anitoys.db.dao.impl;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.db.dao.TopPictureDao;

import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.List;

@Repository("topPictureDaoManage")
public class TopPictureDaoImpl implements TopPictureDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public int insert(TopPicture e) {
        return dao.insert("manage.topPicture.insert", e);
    }

    public int delete(TopPicture e) {
        return dao.delete("manage.topPicture.delete", e);
    }

    public int update(TopPicture e) {
        return dao.update("manage.topPicture.update", e);
    }

    public TopPicture selectOne(TopPicture e) {
        return (TopPicture) dao.selectOne("manage.topPicture.selectOne", e);
    }

    public PagerModel selectPageList(TopPicture e) {
        return dao.selectPageList("manage.topPicture.selectPageList",
                "manage.topPicture.selectPageCount", e);
    }

    public List<TopPicture> selectList(TopPicture e) {
        return dao.selectList("manage.topPicture.selectList", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.topPicture.deleteById", id);
    }

    public TopPicture selectById(String id) {
        return (TopPicture) dao.selectOne("manage.topPicture.selectById", id);
    }

    /**
	 * business 前端商家的大首页图片
	 * @param topPicture
	 * @return
	 */
    public TopPicture selectFrontOne(TopPicture e) {
        return (TopPicture) dao.selectOne("front.topPicture.selectOne", e);
    }

}
