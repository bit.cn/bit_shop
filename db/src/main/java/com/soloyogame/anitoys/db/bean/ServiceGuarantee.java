package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.QueryModel;

/**
 * 售后保障（新老项目字段一致）
 *
 * @author Administrator
 *
 */
public class ServiceGuarantee extends QueryModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;//主键
    private String serviceGuaranteeUrl;//售后保障链接
    private String businessId;//商铺id

    public void clear() {
        super.clear();
        id = null;
        serviceGuaranteeUrl = null;
        businessId = null;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceGuaranteeUrl() {
        return serviceGuaranteeUrl;
    }

    public void setServiceGuaranteeUrl(String serviceGuaranteeUrl) {
        this.serviceGuaranteeUrl = serviceGuaranteeUrl;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

}
