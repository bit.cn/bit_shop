package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;

/**
 * 用户抵扣信息
 *
 * @author shaojian
 */
public class AccountDeductibleVoucher extends com.soloyogame.anitoys.db.bean.AccountDeductibleVoucher implements Serializable {

    private static final long serialVersionUID = 1L;
}
