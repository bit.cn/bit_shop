package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;
import java.util.Map;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 用户
 *
 * @author shaojian
 */
public class User extends PagerModel implements Serializable {

    private String id;               //主键ID
    private String username;         //帐号
    private String password;         //密码
    private String createtime;       //创建时间
    private String updatetime;       //最后修改时间
    private String createAccount;    //创建人
    private String updateAccount;    //最后修改人
    private String status;           //状态（y:启用,n:禁用；默认y）
    private String rid;              //角色ID
    private String email;            //邮箱
    private String nickname;         //昵称
    private String newpassword;      //新密码
    private String newpassword2;     //确认新密码
    private String businessId;       //商家ID
    private String role_dbPrivilege; //用户的数据库操作权限
    
    
    
    private Map<String, String> dbPrivilegeMap;    // 用户数据库权限
    public static final String user_status_y = "y";// 启用
    public static final String user_status_n = "n";// 禁用

    public void clear() {
        this.id = null;
        this.status = null;
        this.createtime = null;
        this.updatetime = null;
        this.createAccount = null;
        this.updateAccount = null;
        this.rid = null;
        this.username = null;
        this.password = null;
        newpassword = null;
        newpassword2 = null;
        nickname = null;
        email = null;
        role_dbPrivilege = null;

        if (dbPrivilegeMap != null) {
            dbPrivilegeMap.clear();
            dbPrivilegeMap = null;
        }
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getNewpassword2() {
        return newpassword2;
    }

    public void setNewpassword2(String newpassword2) {
        this.newpassword2 = newpassword2;
    }

    public Map<String, String> getDbPrivilegeMap() {
        return dbPrivilegeMap;
    }

    public void setDbPrivilegeMap(Map<String, String> dbPrivilegeMap) {
        this.dbPrivilegeMap = dbPrivilegeMap;
    }

    public String getRole_dbPrivilege() {
        return role_dbPrivilege;
    }

    public void setRole_dbPrivilege(String role_dbPrivilege) {
        this.role_dbPrivilege = role_dbPrivilege;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getCreateAccount() {
        return createAccount;
    }

    public void setCreateAccount(String createAccount) {
        this.createAccount = createAccount;
    }

    public String getUpdateAccount() {
        return updateAccount;
    }

    public void setUpdateAccount(String updateAccount) {
        this.updateAccount = updateAccount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
