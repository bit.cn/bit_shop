/**
 * 
 */
package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;
import java.util.Date;

import com.soloyogame.anitoys.db.QueryModel;

/**
 * @author kacey
 *
 */
public class DeductibleVoucherSn extends QueryModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7441051363987022454L;
	private String id;
	private String deductibleSn;
	private int couponType;
	private int receiveStatus;//线上领取还是线下领取（1：领取，2：未领取）
	private String isOnline;//线上领取还是线下领取（1：线上领取，2：线下领取）
	private Date receiveTime;
	private String deductibleSnNum;//抵扣券编号:账号生成规则，
	//商家编号+YYYYMMMDDHH+优惠券个数（长度与最大数长度一致，不足前面以0补充）
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDeductibleSn() {
		return deductibleSn;
	}
	public void setDeductibleSn(String deductibleSn) {
		this.deductibleSn = deductibleSn;
	}
	public int getCouponType() {
		return couponType;
	}
	public void setCouponType(int couponType) {
		this.couponType = couponType;
	}
	public int getReceiveStatus() {
		return receiveStatus;
	}
	public void setReceiveStatus(int receiveStatus) {
		this.receiveStatus = receiveStatus;
	}
	public String getIsOnline() {
		return isOnline;
	}
	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}
	public Date getReceiveTime() {
		return receiveTime;
	}
	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}
	public String getDeductibleSnNum() {
		return deductibleSnNum;
	}
	public void setDeductibleSnNum(String deductibleSnNum) {
		this.deductibleSnNum = deductibleSnNum;
	}
	
	

}
