package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.QueryModel;

/**
 * 工作簿（新老项目字段一致）
 *
 * @author yuanming
 */
public class OrderWork extends QueryModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;					//订单id
    private String orderCode;           //订单编号
    private String productId;			//商品janCode
    private String productName;			//商品名称
    private String productPrice;		//商品单价
    private String number;				//购买数量
    private String account;				//用户id
    private String shipAddress;   		//收货地址
    private String phone;				//联系方式
    private String shipName;      		//收货人姓名
    private String carry;             	//运送方式
    private String expressName;         //快递名称
    private String expressNo;  		  	//快递单号
    private String downOrderTime;		//下单时间
    private String deliverTime;         //发货时间
    private String bespeakTime;			//预约时间
    private String replenishmentTime;	//补款时间
    private String finishreplenishmentTime;//完成补款时间
    private String finishPayTime;          //完成支付时间
    private String isCustomer;			//是否售后
    private String orderStatus;			//订单状态
    private String orderSumPrice;		//订单总额
    private String feeSumPrice;			//运费总额
    private String buyerRank;			//买家使用积分
    private String buyerCoupon;			//买家优惠券抵扣
    private String buyerCouponPlat;		//买家平台抵扣券抵扣
    private String buyerAmount;			//买家站内余额使用
    private String buyerPayMoney;		//买家现金支付金额
    private String ptotal; 				//商品总金额/单品总额
    private String startDate;			//开始时间
    private String endDate;				//结束时间
    private String code;                //商品的规格code
    private String status;              //订单状态
    private String amount;              //订单总额
    private String fee;                 //运费
    private String amountExchangeScore; //买家积分使用
    private String coupons;             //买家使用的优惠券抵扣的金额（需要测试数据的准确性）
    private String deductible;          //买家平台抵扣券抵扣的金额
    private String amountCount;         //买家站内余额使用
    private String payAccount;          //买家现金支付金额
    
    
    public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getAmountExchangeScore() {
		return amountExchangeScore;
	}

	public void setAmountExchangeScore(String amountExchangeScore) {
		this.amountExchangeScore = amountExchangeScore;
	}

	public String getCoupons() {
		return coupons;
	}

	public void setCoupons(String coupons) {
		this.coupons = coupons;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getAmountCount() {
		return amountCount;
	}

	public void setAmountCount(String amountCount) {
		this.amountCount = amountCount;
	}

	public String getPayAccount() {
		return payAccount;
	}

	public void setPayAccount(String payAccount) {
		this.payAccount = payAccount;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFinishPayTime() {
		return finishPayTime;
	}

	public void setFinishPayTime(String finishPayTime) {
		this.finishPayTime = finishPayTime;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDeliverTime() {
		return deliverTime;
	}

	public void setDeliverTime(String deliverTime) {
		this.deliverTime = deliverTime;
	}

	public String getExpressName() {
		return expressName;
	}

	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getCarry() {
        return carry;
    }

    public void setCarry(String carry) {
        this.carry = carry;
    }

    public String getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo;
    }

    public String getDownOrderTime() {
        return downOrderTime;
    }

    public void setDownOrderTime(String downOrderTime) {
        this.downOrderTime = downOrderTime;
    }

    public String getBespeakTime() {
        return bespeakTime;
    }

    public void setBespeakTime(String bespeakTime) {
        this.bespeakTime = bespeakTime;
    }

    public String getReplenishmentTime() {
        return replenishmentTime;
    }

    public void setReplenishmentTime(String replenishmentTime) {
        this.replenishmentTime = replenishmentTime;
    }

    public String getFinishreplenishmentTime() {
        return finishreplenishmentTime;
    }

    public void setFinishreplenishmentTime(String finishreplenishmentTime) {
        this.finishreplenishmentTime = finishreplenishmentTime;
    }

    public String getIsCustomer() {
        return isCustomer;
    }

    public void setIsCustomer(String isCustomer) {
        this.isCustomer = isCustomer;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderSumPrice() {
        return orderSumPrice;
    }

    public void setOrderSumPrice(String orderSumPrice) {
        this.orderSumPrice = orderSumPrice;
    }

    public String getFeeSumPrice() {
        return feeSumPrice;
    }

    public void setFeeSumPrice(String feeSumPrice) {
        this.feeSumPrice = feeSumPrice;
    }

    public String getBuyerRank() {
        return buyerRank;
    }

    public void setBuyerRank(String buyerRank) {
        this.buyerRank = buyerRank;
    }

    public String getBuyerCoupon() {
        return buyerCoupon;
    }

    public void setBuyerCoupon(String buyerCoupon) {
        this.buyerCoupon = buyerCoupon;
    }

    public String getBuyerCouponPlat() {
        return buyerCouponPlat;
    }

    public void setBuyerCouponPlat(String buyerCouponPlat) {
        this.buyerCouponPlat = buyerCouponPlat;
    }

    public String getBuyerAmount() {
        return buyerAmount;
    }

    public void setBuyerAmount(String buyerAmount) {
        this.buyerAmount = buyerAmount;
    }

    public String getBuyerPayMoney() {
        return buyerPayMoney;
    }

    public void setBuyerPayMoney(String buyerPayMoney) {
        this.buyerPayMoney = buyerPayMoney;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPtotal() {
        return ptotal;
    }

    public void setPtotal(String ptotal) {
        this.ptotal = ptotal;
    }
}
