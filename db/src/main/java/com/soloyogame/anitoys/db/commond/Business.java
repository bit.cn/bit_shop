package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;
import java.util.List;

public class Business extends com.soloyogame.anitoys.db.bean.Business implements Serializable {

    private static final long serialVersionUID = 1L;

    public void clear() {
        super.clear();
    }

    /*
	 * 查询条件
     */
    private List<BusinessDetails> businessList;//所属商家的订单列表

    private String startDate;//订单开始时间
    private String endDate;//订单结束时间

    private String startDates;//订单开始时间
    private String endDates;//订单结束时间

    public List<BusinessDetails> getBusinessList() {
        return businessList;
    }

    public void setBusinessList(List<BusinessDetails> businessList) {
        this.businessList = businessList;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDates() {
        return startDates;
    }

    public void setStartDates(String startDates) {
        this.startDates = startDates;
    }

    public String getEndDates() {
        return endDates;
    }

    public void setEndDates(String endDates) {
        this.endDates = endDates;
    }
}
