package com.soloyogame.anitoys.db.dao.impl;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.Business;
import com.soloyogame.anitoys.db.dao.BusinessDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.List;

@Repository
public class BusinessDaoImpl implements BusinessDao {

    @Resource
    private BaseDao dao;
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Business e) {
        return dao.selectPageList("manage.business.selectPageList",
                "manage.business.selectPageCount", e);
    }

    public List selectList(Business e) {
        return dao.selectList("manage.business.selectList", e);
    }

    public Business selectOne(Business e) {
        return (Business) dao.selectOne("manage.business.selectOne", e);
    }

    public int delete(Business e) {
    	redisCacheProvider.put(ManageContainer.CACHE_BUSINESSLIST, null);
        return dao.delete("manage.business.delete", e);
    }

    public int update(Business e) {
    	redisCacheProvider.put(ManageContainer.CACHE_BUSINESSLIST, null);
        return dao.update("manage.business.update", e);
    }

    public int deletes(String[] ids) {
        Business e = new Business();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Business e) {
    	redisCacheProvider.put(ManageContainer.CACHE_BUSINESSLIST, null);
        return dao.insert("manage.business.insert", e);
    }

    public int deleteById(int id) {
    	redisCacheProvider.put(ManageContainer.CACHE_BUSINESSLIST, null);
        return dao.delete("manage.business.deleteById", id);
    }

    public Business selectById(String businessID) {
        return (Business) dao.selectOne("manage.business.selectById", businessID);
    }

    @Override
    public PagerModel selectBusinessDetailsList(Business e) {

        return dao.selectPageList("manage.business.selectDetailsList",
                "manage.business.selectDetailsCount", e);
    }
}
