package com.soloyogame.anitoys.db.bean;

import java.util.Date;

//新老项目字段一致
/**
 * 短信
 * @author 邵健
 */
public class Letter {

    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 邵健待定
     */
    private String code;
    /**
     * 内容
     */
    private String content;
    /**
     * 创建时间
     */
    private Date createTime;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
