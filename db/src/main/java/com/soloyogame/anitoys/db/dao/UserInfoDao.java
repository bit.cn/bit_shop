package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.UserInfo;

/**
 * 用户信息Dao
 *
 * @author xukezhen
 *
 */
public interface UserInfoDao extends DaoManager<UserInfo> {

    /**
     * 新增商家联动生成商家用户
     *
     * @param e
     * @return
     */
    public int insertByBusiness(UserInfo e);
}
