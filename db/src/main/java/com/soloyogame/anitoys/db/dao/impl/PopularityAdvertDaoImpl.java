package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.PopularityAdvert;
import com.soloyogame.anitoys.db.dao.PopularityAdvertDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

@Repository("PopularityAdvertDaoManage")
public class PopularityAdvertDaoImpl implements PopularityAdvertDao {

    @Resource
    private BaseDao dao;
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public int insert(PopularityAdvert e) {
    	redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_ADVERT,null);
        return dao.insert("manage.popularityAdverPlat.insert", e);
    }

    /**
     * 批量删除用户
     *
     * @param ids
     */
    public int deletes(String[] ids) {
        PopularityAdvert e = new PopularityAdvert();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    @Override
    public int delete(PopularityAdvert e) {
    	redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_ADVERT,null);
        return dao.delete("manage.popularityAdverPlat.delete", e);
    }

    @Override
    public int update(PopularityAdvert e) {
    	redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_ADVERT,null);
        return dao.update("manage.popularityAdverPlat.update", e);
    }

    @Override
    public PopularityAdvert selectOne(PopularityAdvert e) {
        return (PopularityAdvert) dao.selectOne("manage.popularityAdverPlat.selectOne", e);
    }

    @Override
    public PagerModel selectPageList(PopularityAdvert e) {
        return dao.selectPageList("manage.popularityAdverPlat.selectPageList",
                "manage.popularityAdverPlat.selectPageCount", e);
    }

    @Override
    public List<PopularityAdvert> selectList(PopularityAdvert e) {
        return dao.selectList("manage.popularityAdverPlat.selectList", e);
    }

    @Override
    public int deleteById(int id) {
    	redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_ADVERT,null);
        return dao.delete("manage.popularityAdverPlat.deleteById", id);
    }

    @Override
    public PopularityAdvert selectById(String id) {
        return null;
    }

    @Override
    public void deletess(String[] ids) {
        PopularityAdvert e = new PopularityAdvert();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }

    }

    //前段selectOne
    @Override
    public PopularityAdvert frontselectOne(PopularityAdvert e) {
        return (PopularityAdvert) dao.selectOne("front.popularityAdver.selectOne", e);
    }

}
