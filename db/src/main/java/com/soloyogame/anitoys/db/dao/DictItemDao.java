package com.soloyogame.anitoys.db.dao;

import java.util.List;

import com.soloyogame.anitoys.db.bean.AppDictItem;

public interface DictItemDao {

    /**
     * 根据父类code查询字典项
     */
    public List<AppDictItem> selectDictItemList(String code);

    /**
     * 根据id查询字典项
     */
    public AppDictItem selectDictItemById(String id);
}
