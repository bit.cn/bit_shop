package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.db.commond.FavoriteBusiness;
import com.soloyogame.anitoys.db.commond.FavoriteBusinessResult;
import com.soloyogame.anitoys.db.dao.FavoriteBusinessDao;

@Repository("favoriteBusinessDaoFront")
public class FavoriteBusinessDaoImpl implements FavoriteBusinessDao {

    @Resource
    private BaseDao dao;

    public int insert(FavoriteBusiness e) {

        return dao.insert("front.favoriteBusiness.insert", e);
    }

    public int delete(FavoriteBusiness e) {
        return dao.delete("front.favoriteBusiness.delete", e);
    }

    public int update(FavoriteBusiness e) {

        return dao.update("front.favoriteBusiness.update", e);
    }

    public FavoriteBusiness selectOne(FavoriteBusiness e) {

        return (FavoriteBusiness) dao.selectOne("front.favorite.selectOne", e);
    }

    @Override
    public PagerModel selectPageList(FavoriteBusiness favoriteBusiness) {
        return null;
    }

    @Override
    public List<FavoriteBusiness> selectList(FavoriteBusiness favoriteBusiness) {
        return null;
    }

    @Override
    public int deleteById(int id) {
        return 0;
    }

    @Override
    public FavoriteBusiness selectById(String id) {
        return null;
    }

    @Override
    public int selectCount(FavoriteBusiness favorite) {
        return 0;
    }

    //查询用户所有收藏的店铺
    public List<FavoriteBusinessResult> selectFavoriteBusiness(
            FavoriteBusiness favoritebusi) {

        return dao.selectList("front.favoriteBusiness.selectUserFavBusi", favoritebusi);
    }

    //查询用户收藏的店铺ID,不关联店铺内商品信息
    public List<FavoriteBusinessResult> selectFavoriteBusinessId(
            FavoriteBusiness favoritebusi) {
        return dao.selectList("front.favoriteBusiness.selectUserFavBusiness", favoritebusi);
    }

    @Override
    public FavoriteBusinessResult selectBusinessInfo(FavoriteBusinessResult favoriteBusinessResult) {

        return (FavoriteBusinessResult) dao.selectOne("front.favoriteBusiness.selectBusinessInfo", favoriteBusinessResult);
    }

    public List<FavoriteBusinessResult> selectFavoriteBusinessByAccountBusiID(
            FavoriteBusiness favBusi) {
        return dao.selectList("front.favoriteBusiness.selectFavoriteBusinessByAccountBusiID", favBusi);
    }

}
