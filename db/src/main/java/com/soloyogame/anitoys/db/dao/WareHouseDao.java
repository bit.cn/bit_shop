package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.bean.WareHouse;

public interface WareHouseDao extends DaoManager<WareHouse> {

}
