package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.IndexImg;
import com.soloyogame.anitoys.db.dao.PlatIndexImgDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

/**
 * 图片信息DAO接口实现
 *
 * @author shaojian
 */
@Repository("indexImgDaoPlat")
public class PlatIndexImgDaoImpl implements PlatIndexImgDao {

    @Resource
    private BaseDao dao;
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(IndexImg e) {
        return dao.selectPageList("manage.indexImgPlat.selectPageList",
                "manage.indexImg.selectPageCount", e);
    }

    public List selectList(IndexImg e) {
        return dao.selectList("manage.indexImgPlat.selectList", e);
    }

    public IndexImg selectOne(IndexImg e) {
        return (IndexImg) dao.selectOne("manage.indexImgPlat.selectOne", e);
    }

    public int delete(IndexImg e) {
    	redisCacheProvider.put(ManageContainer.CACHE_INDEX_IMAGES,null);
        return dao.delete("manage.indexImgPlat.delete", e);
    }

    public int update(IndexImg e) {
    	redisCacheProvider.put(ManageContainer.CACHE_INDEX_IMAGES,null);
        return dao.update("manage.indexImgPlat.update", e);
    }

    /**
     * 批量删除用户
     *
     * @param ids
     */
    public int deletes(String[] ids) {
        IndexImg e = new IndexImg();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(IndexImg e) {
    	redisCacheProvider.put(ManageContainer.CACHE_INDEX_IMAGES,null);
        return dao.insert("manage.indexImgPlat.insert", e);
    }

    /**
     * @param bInfo
     */
    public List<IndexImg> getLoseList(IndexImg bInfo) {
        return dao.selectList("manage.indexImgPlat.getLoseList", bInfo);
    }

    @Override
    public int deleteById(int id) {
    	redisCacheProvider.put(ManageContainer.CACHE_INDEX_IMAGES,null);
        return dao.delete("manage.indexImgPlat.deleteById", id);
    }

    public IndexImg selectById(String id) {
        return (IndexImg) dao.selectOne(id);
    }

    /**
     * 得到平台的轮播图
     */
    public List<IndexImg> getPlatImgsShowToIndex(int i) {
        return dao.selectList("front.indexImgPlat.getPlatImgsShowToIndex", i);
    }

    @Override
    public List<IndexImg> getImgsShowToIndex(int i) {
        return dao.selectList("front.indexImgPlat.getImgsShowToIndex", i);
    }
}
