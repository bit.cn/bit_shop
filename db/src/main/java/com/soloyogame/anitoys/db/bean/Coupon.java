package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;
import java.util.Date;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * （老项目比新项目的字段多） 优惠券
 *
 * @author shaojian
 */
public class Coupon extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String couponSn;    	//优惠券号码
    private String businessId;  	//商家编号
    private String couponName;  	//优惠券名称
    private int couponType;     	//优惠券类型(1.抵价券,2.折扣券)
    private String couponValue; 	//优惠券优惠值
    private int enableStatus;   	//启用状态
    private int auditStatus;    	//优惠券审核状态(0.待审核 1.通过 2.拒绝)
    private int receiveStatus;  	//发放状态(1.已发放,0.未发放)
    private String receiveTime; 	//优惠券发放时间
    private String endReceiveTime;  //优惠券结束领取时间
    private String startTime;       //优惠券开始使用时间
    private String endTime;         //优惠券结束使用时间
    //private int isuse;            //优惠券是否可以用
    private int couponCount;        //优惠券发行数量
    private int receiveCount;       //优惠券领取数量
    private int useCount;           //优惠券使用数量
    private String addTime;         //增加时间
    private String editTime;        //编辑时间
    private String receiveId;       //派发者
    private String userId;
    private String couponId;
    private String status;
    private Date useTime;
    private String orderId;

    private String businessName;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public void clear() {
        super.clear();
        id = null;
        couponSn = null;
        businessId = null;
        couponName = null;
        couponType = 0;
        couponValue = null;
        enableStatus = 0;
        auditStatus = 0;
        receiveStatus = 0;
        receiveTime = null;
        endReceiveTime = null;
        startTime = null;
        endTime = null;
        //isuse=0;
        couponCount = 0;
        receiveCount = 0;
        useCount = 0;
        addTime = null;
        editTime = null;
        receiveId = null;
        businessName = null;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getUseTime() {
        return useTime;
    }

    public void setUseTime(Date useTime) {
        this.useTime = useTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCouponSn() {
        return couponSn;
    }

    public void setCouponSn(String couponSn) {
        this.couponSn = couponSn;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public int getCouponType() {
        return couponType;
    }

    public void setCouponType(int couponType) {
        this.couponType = couponType;
    }

    public String getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(String couponValue) {
        this.couponValue = couponValue;
    }

    public int getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(int enableStatus) {
        this.enableStatus = enableStatus;
    }

    public int getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    public int getReceiveStatus() {
        return receiveStatus;
    }

    public void setReceiveStatus(int receiveStatus) {
        this.receiveStatus = receiveStatus;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getEndReceiveTime() {
        return endReceiveTime;
    }

    public void setEndReceiveTime(String endReceiveTime) {
        this.endReceiveTime = endReceiveTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /*public int getIsuse() {
		return isuse;
	}
	public void setIsuse(int isuse) {
		this.isuse = isuse;
	}*/
    public int getCouponCount() {
        return couponCount;
    }

    public void setCouponCount(int couponCount) {
        this.couponCount = couponCount;
    }

    public int getReceiveCount() {
        return receiveCount;
    }

    public void setReceiveCount(int receiveCount) {
        this.receiveCount = receiveCount;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getEditTime() {
        return editTime;
    }

    public void setEditTime(String editTime) {
        this.editTime = editTime;
    }

    public String getReceiveId() {
        return receiveId;
    }

    public void setReceiveId(String receiveId) {
        this.receiveId = receiveId;
    }

}
