package com.soloyogame.anitoys.db.dao;

import java.util.List;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.Help;

/**
 * @author 索罗游
 */
public interface HelpDao extends DaoManager<Help> {

    /**
     * @param e
     * @return
     */
    List<Help> selecIndexHelp(Help e);

    /**
     * @param news
     */
    void sync(Help help);

    void updateDownOrUp(Help help);

    int selectCount(Help help);
    
    /**
     * 前端查询帮助支持列表
     * @param e
     * @return
     */
    public List<Help> selectFrontList(Help e);

}
