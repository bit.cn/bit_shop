package com.soloyogame.anitoys.db.commond;

import com.soloyogame.anitoys.db.page.ClearBean;

/**
 * 订单简单报表信息
 *
 * @author jqsl2012@163.com
 */
public class OrderSimpleReport implements ClearBean {

    private int orderWaitPayCount;	//待付款
    private int orderCancelCount;	//取消订单
    private int orderCompleteCount; //待确认收货
    private int orderPassCount;		//待补款

    public int getOrderWaitPayCount() {
        return orderWaitPayCount;
    }

    public void setOrderWaitPayCount(int orderWaitPayCount) {
        this.orderWaitPayCount = orderWaitPayCount;
    }

    public int getOrderCancelCount() {
        return orderCancelCount;
    }

    public void setOrderCancelCount(int orderCancelCount) {
        this.orderCancelCount = orderCancelCount;
    }

    public int getOrderCompleteCount() {
        return orderCompleteCount;
    }

    public void setOrderCompleteCount(int orderCompleteCount) {
        this.orderCompleteCount = orderCompleteCount;
    }

    public int getOrderPassCount() {
        return orderPassCount;
    }

    public void setOrderPassCount(int orderPassCount) {
        this.orderPassCount = orderPassCount;
    }

    @Override
    public String toString() {
        return "OrderSimpleReport [orderWaitPayCount=" + orderWaitPayCount
                + ", orderCancelCount=" + orderCancelCount
                + ", orderPassCount=" + orderPassCount
                + ", orderCompleteCount=" + orderCompleteCount + "]";
    }

    public void clear() {
        this.orderWaitPayCount = 0;
        this.orderCancelCount = 0;
        this.orderCompleteCount = 0;
        this.orderPassCount = 0;
    }

}
