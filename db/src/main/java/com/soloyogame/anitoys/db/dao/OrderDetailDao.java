package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.Orderdetail;
import com.soloyogame.anitoys.db.commond.ReportInfo;

import java.util.List;

public interface OrderDetailDao extends DaoManager<Orderdetail> {

    /**
     * 订单明细评价状态修改
     */
    public int updateOrderDetailIsCommon(String id);

    public List<ReportInfo> reportProductSales(Orderdetail orderdetail);
}
