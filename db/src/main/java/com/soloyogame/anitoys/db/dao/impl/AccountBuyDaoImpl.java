package com.soloyogame.anitoys.db.dao.impl;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.AccountBuy;
import com.soloyogame.anitoys.db.dao.AccountBuyDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 购买数量DAO
 *
 * @author 索罗游
 */
@Repository("accountBuyDaoFront")
public class AccountBuyDaoImpl implements AccountBuyDao {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BaseDao dao;

    /*
	 * 查询我的优惠券
	 * 
     */
    @Override
    public PagerModel selectPageList(AccountBuy e) {
        return dao.selectPageList("front.accountBuy.selectPageList", "front.coupon.selectPageCount", e);
    }

    @Override
    public int insert(AccountBuy e) {
        return dao.insert("front.accountBuy.insert", e);
    }

    @Override
    public int delete(AccountBuy e) {
        return 0;
    }

    @Override
    public int update(AccountBuy e) {
        return dao.update("front.accountBuy.update", e);
    }

    @Override
    public AccountBuy selectOne(AccountBuy e) {
        return (AccountBuy) dao.selectOne("front.accountBuy.selectOne", e);
    }

    @Override
    public List<AccountBuy> selectList(AccountBuy e) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int deleteById(int id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public AccountBuy selectById(String id) {
        // TODO Auto-generated method stub
        return null;
    }

}
