package com.soloyogame.anitoys.db.dao.impl;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.MyOrderRepairs;
import com.soloyogame.anitoys.db.commond.OrderRepairs;
import com.soloyogame.anitoys.db.dao.OrderRepairsDao;
import com.soloyogame.anitoys.db.page.PagerModel;

import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.List;

/**
 * 售后订单
 * @author jiangyongzhi
 */
@Repository("orderRepairsDaoFront")
public class OrderRepairsDaoImpl implements OrderRepairsDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(OrderRepairs e) {
        return dao.selectPageList("manage.orderRepairs.selectPageList",
                "manage.orderRepairs.selectPageCount", e);
    }

    @Override
    public PagerModel selectFrontPageList(OrderRepairs e) {
        return dao.selectPageList("front.orderrepairs.selectPageList",
                "front.orderrepairs.selectPageCount", e);
    }

    public int update(OrderRepairs e) {
        return dao.update("manage.orderRepairs.update", e);
    }

    public int insert(OrderRepairs e) {
        return dao.insert("manage.orderRepairs.insert", e);
    }

    public MyOrderRepairs selectRepairSimpleReport(OrderRepairs e) {
        return (MyOrderRepairs) dao.selectOne("manage.orderrepairs.selectRepairSimpleReport", e);
    }

    public int delete(OrderRepairs e) {
        return dao.delete("manage.orderRepairs.delete", e);
    }

    public int deleteById(int id) {
        // TODO Auto-generated method stub
        return 0;
    }

    public OrderRepairs selectById(String id) {
        return (OrderRepairs) dao.selectOne("manage.orderRepairs.selectById", id);
    }

    public List<OrderRepairs> selectList(OrderRepairs e) {
        // TODO Auto-generated method stub
        return null;
    }

    public OrderRepairs selectOne(OrderRepairs e) {
        return (OrderRepairs) dao.selectOne("manage.orderRepairs.selectOne", e);
    }

    public MyOrderRepairs selectOneOrder(OrderRepairs e) {
        return (MyOrderRepairs) dao.selectOne("manage.orderrepairs.selectOrderInfoOne", e);
    }

    @Override
    public List<MyOrderRepairs> selectOrderInfoList(OrderRepairs e) {
        return dao.selectList("front.orderrepairs.selectOrderInfoList", e);
    }

    @Override
    public List<OrderRepairs> selectByUserId(OrderRepairs e) {
        return dao.selectList("front.orderrepairs.selectByUserId", e);
    }

    @Override
    public int updateStatus(String id) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    /**
     * 前端个人中心录入售后单信息
     */
	public int insertFront(OrderRepairs e) {
		return dao.insert("front.orderrepairs.insert", e);
	}

}
