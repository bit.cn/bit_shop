package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.db.commond.PopularityProductPlat;
import com.soloyogame.anitoys.db.dao.PopularityProductPlatDao;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository("popularityProductPlatDaoManage")
public class PopularityProductPlatDaoImpl implements PopularityProductPlatDao {

    @Resource
    private BaseDao dao;
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(PopularityProductPlat e) {
        return dao.selectPageList("manage.popularityProductPlat.selectPageList",
                "manage.popularityProductPlat.selectPageCount", e);
    }

    public List selectList(PopularityProductPlat e) {
        return dao.selectList("manage.popularityProductPlat.selectList", e);
    }

    public PopularityProductPlat selectOne(PopularityProductPlat e) {
        return (PopularityProductPlat) dao.selectOne("manage.popularityProductPlat.selectOne", e);
    }

    public int delete(PopularityProductPlat e) {
    	redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_PRODUCT_PLAT_LIST,null);
        return dao.delete("manage.popularityProductPlat.delete", e);
    }

    public int update(PopularityProductPlat e) {
    	redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_PRODUCT_PLAT_LIST,null);
        return dao.update("manage.popularityProductPlat.update", e);
    }

    public int deletes(String[] ids) {
        PopularityProductPlat e = new PopularityProductPlat();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(PopularityProductPlat e) {
    	redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_PRODUCT_PLAT_LIST,null);
        return dao.insert("manage.popularityProductPlat.insert", e);
    }

    public int deleteById(int id) {
    	redisCacheProvider.put(ManageContainer.CACHE_POPULARITY_PRODUCT_PLAT_LIST,null);
        return dao.delete("manage.popularityProductPlat.deleteById", id);
    }

    public PopularityProductPlat selectById(String id) {
        return (PopularityProductPlat) dao.selectOne("manage.popularityProductPlat.selectById", id);
    }
    
    /**
     * 大首页查询人气商品列表
     * @param e
     * @return
     */
    public List<PopularityProductPlat> selectFrontList(PopularityProductPlat e)
    {
		return dao.selectList("front.popularityProductPlat.selectList", e);
	}
}
