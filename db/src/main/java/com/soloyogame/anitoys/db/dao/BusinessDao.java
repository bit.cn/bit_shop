package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.Business;
import com.soloyogame.anitoys.db.page.PagerModel;

public interface BusinessDao extends DaoManager<Business> {

    public PagerModel selectBusinessDetailsList(Business e);
}
