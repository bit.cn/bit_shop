package com.soloyogame.anitoys.db.bean;

import com.soloyogame.anitoys.db.QueryModel;

import java.io.Serializable;

/**
 * 商家的实体类（新的实体类比老的字段端）
 *
 * @author shaojian
 */
public class Business extends QueryModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;//商家ID
    private String businessName;//商家名称
    private String businessLicense;//商家营业执照编号
    private String businessOrcc;//商家组织机构编号
    private String businesstTrc;//商家税务登记证编号
    private String businessApName;//商企业法人名称
    private String businessApNo;//商家法人身份证编号
    private String businessPhone;//商家电话
    private String businessAddress;//企业工作地址
    private String businessMobile;//商家联系人手机号码
    private String businessContact;//企业联系人
    private int regionID;//商家所在区域编号
    private String regionName;//商家所在区域名称
    private int status;//商家状态
    private String closeReason;//商家关闭原因
    private String addTime;//店铺入住时间
    private String userId;//用户ID
    private String picture;//商铺图片
    private String service1;//企业联系人
    private String service2;//企业联系人
    private String service3;//企业联系人
    private String service4;//企业联系人
    private int sortOrder;//商家显示排序
    private String theme;//商家模板主题
    private String maxPicture;//大图片地址
    private String bannerPicture;//banner图片地址

    private String theFilePath;
    private String customerAddress;

    private String username;  //账号
    private String password;  //密码
    private String userInfoId;//商家ID
    private String type;      //查询类型

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBusinesstTrc() {
        return businesstTrc;
    }

    public void setBusinesstTrc(String businesstTrc) {
        this.businesstTrc = businesstTrc;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getService1() {
        return service1;
    }

    public void setService1(String service1) {
        this.service1 = service1;
    }

    public String getService2() {
        return service2;
    }

    public void setService2(String service2) {
        this.service2 = service2;
    }

    public String getService3() {
        return service3;
    }

    public void setService3(String service3) {
        this.service3 = service3;
    }

    public String getService4() {
        return service4;
    }

    public void setService4(String service4) {
        this.service4 = service4;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getTheFilePath() {
        return theFilePath;
    }

    public void setTheFilePath(String theFilePath) {
        this.theFilePath = theFilePath;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(String userInfoId) {
        this.userInfoId = userInfoId;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getBannerPicture() {
        return bannerPicture;
    }

    public void setBannerPicture(String bannerPicture) {
        this.bannerPicture = bannerPicture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }

    public String getBusinessOrcc() {
        return businessOrcc;
    }

    public void setBusinessOrcc(String businessOrcc) {
        this.businessOrcc = businessOrcc;
    }

    public String getBusinessApName() {
        return businessApName;
    }

    public void setBusinessApName(String businessApName) {
        this.businessApName = businessApName;
    }

    public String getBusinessApNo() {
        return businessApNo;
    }

    public void setBusinessApNo(String businessApNo) {
        this.businessApNo = businessApNo;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessMobile() {
        return businessMobile;
    }

    public void setBusinessMobile(String businessMobile) {
        this.businessMobile = businessMobile;
    }

    public String getBusinessContact() {
        return businessContact;
    }

    public void setBusinessContact(String businessContact) {
        this.businessContact = businessContact;
    }

    public int getRegionID() {
        return regionID;
    }

    public void setRegionID(int regionID) {
        this.regionID = regionID;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getMaxPicture() {
        return maxPicture;
    }

    public void setMaxPicture(String maxPicture) {
        this.maxPicture = maxPicture;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

}
