package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;
import java.util.Date;
import com.soloyogame.anitoys.db.QueryModel;

/**
 * 订单列表（新老项目一致）
 * @author yuanming
 */
public class OrderManager extends QueryModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String account;           //买家会员帐号
    private String nickName;          //买家会员真实名
    private String productId;         //商品janCode
    private String buyerMessage;      //买家留言
    private String shipName;          //收货人姓名
    private String shipAddress;       //收货人详细地址
    private String carry;             //运送方式
    private String tel;				  //联系电话
    private String phone;			  //手机号
    private String remark;			  //订单备注
    private String expressNo;  		  //物流单号
    private String expressCompanyName;//物流公司名称
    private String expressName;       //快递名称
    private String deliverTime;		  //发货时间
    private String wareHouseRemark;	  //仓库备注
    private String startDate;		  //开始时间
    private String endDate;			  //结束时间

    public String getExpressName() {
		return expressName;
	}

	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo;
    }

    public String getExpressCompanyName() {
        return expressCompanyName;
    }

    public void setExpressCompanyName(String expressCompanyName) {
        this.expressCompanyName = expressCompanyName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDeliverTime() {
        return deliverTime;
    }

    public void setDeliverTime(String deliverTime) {
        this.deliverTime = deliverTime;
    }

    public String getWareHouseRemark() {
        return wareHouseRemark;
    }

    public void setWareHouseRemark(String wareHouseRemark) {
        this.wareHouseRemark = wareHouseRemark;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getBuyerMessage() {
        return buyerMessage;
    }

    public void setBuyerMessage(String buyerMessage) {
        this.buyerMessage = buyerMessage;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getCarry() {
        return carry;
    }

    public void setCarry(String carry) {
        this.carry = carry;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

}
