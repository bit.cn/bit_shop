package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.db.bean.WareHouse;
import com.soloyogame.anitoys.db.dao.WareHouseDao;

@Repository("wareHouseDaoManage")
public class WareHouseDaoImpl implements WareHouseDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    @Override
    public int insert(WareHouse e) {
        return dao.insert("manage.wareHouse.insert", e);
    }

    @Override
    public int delete(WareHouse e) {
        return dao.delete("manage.wareHouse.delete", e);
    }

    public int deletes(String[] ids) {
        WareHouse e = new WareHouse();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    @Override
    public int update(WareHouse e) {
        return dao.update("manage.wareHouse.update", e);
    }

    @Override
    public WareHouse selectOne(WareHouse e) {
        return (WareHouse) dao.selectOne("manage.wareHouse.selectOne", e);
    }

    @Override
    public PagerModel selectPageList(WareHouse e) {
        return dao.selectPageList("manage.wareHouse.selectPageList",
                "manage.wareHouse.selectPageCount", e);
    }

    @Override
    public List<WareHouse> selectList(WareHouse e) {
        return dao.selectList("manage.wareHouse.selectList", e);
    }

    @Override
    public int deleteById(int id) {
        return dao.delete("manage.wareHouse.deleteById", id);
    }

    @Override
    public WareHouse selectById(String id) {
        return (WareHouse) dao.selectOne("manage.wareHouse.deleteById", id);
    }

}
