package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.OrderWork;
import com.soloyogame.anitoys.db.page.PagerModel;

public interface OrderWorkDao extends DaoManager<OrderWork> {

}
