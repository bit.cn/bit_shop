package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 用户抵扣券信息
 *
 * @author xukezhen
 */
public class AccountDeductibleVoucher extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String deductibleId;       //抵扣券的ID
    private String deductibleSn;       //抵扣券编号
    private String userId;             //用户ID
    private int status;                //抵扣券的状态
    private String expireTime;         //抵扣券的过期时间
    private String addTime;
    private String useTime;
    private String orderId;            //抵扣券的绑定订单号

    private int deductibleCount;
    private String deductibleName;  	//抵扣券名称（老项目字段)
    private int deductibleType;     	//抵扣券类型（老项目字段)
    private String deductibleValue; 	//抵扣券优惠值（老项目字段)
    private String deductibleRule;  	//抵扣券规则（老项目字段)

    public void clear() {
        super.clear();
        deductibleId = null;
        deductibleSn = null;
        userId = null;
        expireTime = null;
        addTime = null;
        useTime = null;
        orderId = null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(String deductibleId) {
        this.deductibleId = deductibleId;
    }

    public String getDeductibleSn() {
        return deductibleSn;
    }

    public void setDeductibleSn(String deductibleSn) {
        this.deductibleSn = deductibleSn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getDeductibleCount() {
        return deductibleCount;
    }

    public void setDeductibleCount(int deductibleCount) {
        this.deductibleCount = deductibleCount;
    }

    public String getDeductibleName() {
        return deductibleName;
    }

    public void setDeductibleName(String deductibleName) {
        this.deductibleName = deductibleName;
    }

    public int getDeductibleType() {
        return deductibleType;
    }

    public void setDeductibleType(int deductibleType) {
        this.deductibleType = deductibleType;
    }

    public String getDeductibleValue() {
        return deductibleValue;
    }

    public void setDeductibleValue(String deductibleValue) {
        this.deductibleValue = deductibleValue;
    }

    public String getDeductibleRule() {
        return deductibleRule;
    }

    public void setDeductibleRule(String deductibleRule) {
        this.deductibleRule = deductibleRule;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return "AccountDeductibleVoucher [id=" + id + ", deductibleId="
                + deductibleId + ", deductibleSn=" + deductibleSn + ", userId="
                + userId + ", status=" + status + ", expireTime=" + expireTime
                + ", addTime=" + addTime + ", useTime=" + useTime
                + ", orderId=" + orderId + ", deductibleCount="
                + deductibleCount + "]";
    }
}
