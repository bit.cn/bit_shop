package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.ServiceGuarantee;

public interface ServiceGuaranteeDao extends DaoManager<ServiceGuarantee> {

}
