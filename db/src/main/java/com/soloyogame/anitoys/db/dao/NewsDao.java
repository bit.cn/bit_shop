package com.soloyogame.anitoys.db.dao;

import java.util.List;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * @author huangf
 * @param <T>
 */
public interface NewsDao extends DaoManager<News> {

    /**
     * @param e
     * @return
     */
    List<News> selecIndexNews(News e);

    /**
     * @param news
     */
    void sync(News news);

    void updateDownOrUp(News news);

    int selectCount(News news);

    /**
     * @return
     */
    List<String> selectAllMd5();

    /**
     * @param e
     */
    void updateInBlackList(String e);

    List<News> selectNoticeList(News news);

    List<News> selectPlatNoticeList(News news);

    News selectSimpleOne(News news);

    /**
     * 加载平台的新闻列表
     *
     * @return
     */
    public List<News> selectPlatList(News news);

    /**
     * 查询平台文章
     *
     * @param news
     * @return
     */
    public News selectPlatNewsById(String id);

    /**
     * 平台的文章分页查询
     *
     * @param e
     * @return
     */
    public PagerModel selectPlatNewsPageList(News news);
    
    /**
	 * 商家首页加载商家的新闻列表
	 */
	public List<News> selectFrontList(News e);

}
