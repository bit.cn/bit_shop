package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

public class OrderManager extends com.soloyogame.anitoys.db.bean.OrderManager implements Serializable {

    private static final long serialVersionUID = 1L;

    private String productId;
    private String productName;    //商品名称
    private String firstType;      //商品种类
    private String quantity;       //商品购买数量
    private String code;       	   //暂时舍弃
    private String orderCode;  	   //订单编号
    private String productJanCode; //商品的jancode
    private String specCode;       //商品的规格code

    public void clear() {
        super.clear();
    }
    
    
    public String getSpecCode() {
		return specCode;
	}


	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}


	public String getProductJanCode() {
		return productJanCode;
	}


	public void setProductJanCode(String productJanCode) {
		this.productJanCode = productJanCode;
	}


	public String getOrderCode() {
		return orderCode;
	}


	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}



	public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getFirstType() {
        return firstType;
    }

    public void setFirstType(String firstType) {
        this.firstType = firstType;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String quantity) {
        this.productId = productId;
    }

}
