package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.Orderdetail;
import com.soloyogame.anitoys.db.commond.Orderpay;
import com.soloyogame.anitoys.db.commond.ReportInfo;
import java.util.List;

public interface OrderpayDao extends DaoManager<Orderpay> {

    public List<ReportInfo> reportProductSales(Orderdetail orderdetail);

    /**
     * 查询订单支付记录是否已创建 （true已创建 ，false未创建）
     *
     * @param orderId
     * @return
     */
    public boolean isOrderpayCreate(String orderId);
    
    /**
     * 前端更新支付信息
     */
    public int updateFront(Orderpay e) ;
    
    /**
     * 前段插入支付信息
     * @param e
     * @return
     */
    public int insertFront(Orderpay e);
}
