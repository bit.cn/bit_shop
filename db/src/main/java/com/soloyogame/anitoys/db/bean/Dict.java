package com.soloyogame.anitoys.db.bean;

import java.util.ArrayList;
import java.util.List;
import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * （新老项目字段一致） 新版字典 与DictItem配合使用,由数据库来管理 注意：原DicModel不再升级,由dic-config.xml来配置
 * 由dicApi来调用
 *
 * @author shaojian
 */
public class Dict extends PagerModel {

    private String dic_id;     //字典ID
    private String dic_code;   //字典code
    private String dic_name;   //字典name
    private List<DictItem> items = new ArrayList<DictItem>();

    /**
     * 获取所有字典项,含各子项
     *
     * @return
     */
    public List<DictItem> getItems() {
        return items;
    }

    public void setItems(List<DictItem> items) {
        this.items = items;
    }

    public void addItem(DictItem item) {
        this.items.add(item);
    }

    public void removeItem(DictItem item) {
        this.items.remove(item);
    }

    public List<DictItem> getChildItemList() {
        List<DictItem> children = new ArrayList();
        for (DictItem item : this.getItems()) {
            if (item.getPid().equals("0")) {
                children.add(item);
            }
        }
        return children;
    }

    public DictItem gettChildItemByValue(String value) {
        DictItem dictItem = null;
        for (DictItem item : this.getChildItemList()) {
            if (item.getItem_value().equals(value)) {
                dictItem = item;
                break;
            }
        }
        return dictItem;
    }

    public DictItem getChildItemByCode(String code) {
        DictItem dictItem = null;
        for (DictItem item : this.getChildItemList()) {
            if (item.getItem_code().equals(code)) {
                dictItem = item;
                break;
            }
        }
        return dictItem;
    }

    public DictItem gettItemByValue(String value) {
        DictItem dictItem = null;
        for (DictItem item : this.getItems()) {
            if (item.getItem_value().equals(value)) {
                dictItem = item;
                break;
            }
        }
        return dictItem;
    }

    public DictItem gettItemByCode(String code) {
        DictItem dictItem = null;
        for (DictItem item : this.getItems()) {
            if (item.getItem_code().equals(code)) {
                dictItem = item;
                break;
            }
        }
        return dictItem;
    }

    public DictItem gettItemById(String itemid) {
        DictItem dictItem = null;
        for (DictItem item : this.getItems()) {
            if (item.getId().equals(itemid)) {
                dictItem = item;
                break;
            }
        }
        return dictItem;
    }

    public String getDic_id() {
        return dic_id;
    }

    public void setDic_id(String dic_id) {
        this.dic_id = dic_id;
    }

    public String getDic_code() {
        return dic_code;
    }

    public void setDic_code(String dic_code) {
        this.dic_code = dic_code;
    }

    public String getDic_name() {
        return dic_name;
    }

    public void setDic_name(String dic_name) {
        this.dic_name = dic_name;
    }

}
