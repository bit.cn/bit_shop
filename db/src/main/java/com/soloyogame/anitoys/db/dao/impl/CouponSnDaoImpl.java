package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.CouponSn;
import com.soloyogame.anitoys.db.dao.CouponSnDao;
import com.soloyogame.anitoys.db.page.PagerModel;


/**
 * 优惠券编号管理
 * @author 索罗游
 */
@Repository("couponsnDaoManage")
public class CouponSnDaoImpl implements CouponSnDao {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BaseDao dao;

    public PagerModel selectPageList(CouponSn e) {
        return dao.selectPageList("manage.couponsn.selectPageList",
                "manage.couponsn.selectPageCount", e);
    }

    public List selectList(CouponSn e) {
        return dao.selectList("manage.couponsn.selectList", e);
    }

    public CouponSn selectOne(CouponSn e) {
        return (CouponSn) dao.selectOne("manage.couponsn.selectOne", e);
    }

    public int delete(CouponSn e) {
        return dao.delete("manage.couponsn.delete", e);
    }

    public int update(CouponSn e) {
        return dao.update("manage.couponsn.update", e);
    }

    public int deletes(String[] ids) {
    	CouponSn e = new CouponSn();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(CouponSn e) {
        return dao.insert("manage.couponsn.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.couponsn.deleteById", id);
    }

    @Override
    public CouponSn selectById(String id) {
        return (CouponSn) dao.selectOne("manage.couponsn.selectById", id);
    }

    public int updateBySn(Map<String, Object> map) {
        return dao.update("manage.couponsn.updateBySn", map);
    }

    /*
	 * 查询可领取的优惠券(程版)
	 *
     */
    public PagerModel selectCouponByReceive(CouponSn e) {
        return dao.selectPageList("front.couponsn.selectCouponByReceive", "front.coupon.selectCouponCountByReceive", e);
    }

    /**
     * 领取优惠券(程版)
     */
    public int updateStatus(CouponSn e) {
        return dao.update("front.couponsn.updateStatus", e);
    }

    /**
     * 查找一条有效的优惠券
     * @param e
     * @return
     */
    @Override
    public CouponSn findEffectiveCouponSnCode(CouponSn e) {
        return (CouponSn) dao.selectOne("front.couponSn.findEffectiveCouponSnCode", e);
    }
}
