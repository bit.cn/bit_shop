package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;
import java.util.Map;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.AccountPointsLog;
import com.soloyogame.anitoys.db.commond.Coupon;
import com.soloyogame.anitoys.db.commond.Order;
import com.soloyogame.anitoys.db.commond.OrderSimpleReport;
import com.soloyogame.anitoys.db.commond.OrdersReport;
import com.soloyogame.anitoys.db.commond.ReportInfo;
import com.soloyogame.anitoys.db.dao.OrderDao;
import com.soloyogame.anitoys.db.page.PagerModel;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class OrderDaoImpl implements OrderDao {

    @Resource
    private BaseDao dao;

    public PagerModel selectPageList(Order e) {
        return dao.selectPageList("manage.order.selectPageList",
                "manage.order.selectPageCount", e);
    }

    public List selectList(Order e) {
        return dao.selectList("manage.order.selectList", e);
    }

    public Order selectOne(Order e) {
        return (Order) dao.selectOne("manage.order.selectOne", e);
    }

    public int delete(Order e) {
        return dao.delete("manage.order.delete", e);
    }

    public int update(Order e) {
        return dao.update("manage.order.update", e);
    }

    public int deletes(String[] ids) {
        Order e = new Order();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Order e) {
        return dao.insert("manage.order.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.order.deleteById", id);
    }

    public Order selectById(String id) {
        return (Order) dao.selectOne("manage.order.selectById", id);
    }

    public List<ReportInfo> selectOrderSales(Order order) {
        return dao.selectList("manage.order.selectOrderSales", order);
    }

    public List<Order> selectCancelList(Order order) {
        return dao.selectList("manage.order.selectCancelList", order);
    }

    public List<ReportInfo> selectProductSales(Order order) {
        return dao.selectList("manage.order.selectProductSales", order);
    }

    public void updatePayMonery(Order e) {
        dao.update("manage.order.updatePayMonery", e);
    }

    public OrdersReport loadOrdersReport() {
        return (OrdersReport) dao.selectOne("manage.order.loadOrdersReport");
    }

    public void updateExpress(Order order) {
        dao.update("manage.order.updateExpress", order);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Order> selectOrderInfo(Order order) {
        return dao.selectList("front.order.selectOrderInfo", order);
    }

    @Override
    public OrderSimpleReport selectOrdersSimpleReport(String account) {
        return (OrderSimpleReport) dao.selectOne("front.order.selectOrdersSimpleReport", account);
    }

    /**
     * 更新商品库存 wuxiongxiong
     *
     * @param ps,商品ID、购买数量
     * @return
     */
    @Override
    public int updateProductStock(String orderId) {
        int i = 0;
        dao.update("front.order.updateStock", orderId);
        dao.update("front.order.updateSpec", orderId);
        return i;
    }

    /**
     * 根据订单号查询所有子订单
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Order> selectSonOrderList(Order or) {

        return dao.selectList("front.order.selectSonOrderList", or);
    }

    /**
     * 更改用户优惠券信息
     */
    @Override
    public int updateAccountCoupon(Coupon coupon) {
        return dao.update("front.order.updateAccountCoupon", coupon);
    }

    @Override
    public int addProductStock(String orderId) {
        int i = 0;
        dao.update("front.order.addProductStock", orderId);
        dao.update("front.order.addSpec", orderId);
        return i;
    }

    @Override
    public int updateOrderByReceipt(Order or) {
        return dao.update("front.order.updateOrderByReceipt", or);
    }

    public List selectOrderPoints(AccountPointsLog e) {
        return dao.selectList("front.order.selectOrderPoints", e);
    }

    @Override
    public int updateOrderIsSaleorderById(String id) {
        return dao.update("front.order.updateOrderIsSaleorderById", id);
    }

    /**
     * 根据accountId查询消费记录订单
     */
    @Override
    public PagerModel selectExpenseRecordsList(Order order) {

        return dao.selectPageList("front.order.selectExpenseRecordsCount", "front.order.selectExpenseRecordsCount", order);
    }
    
    /**
     * cart 结算页确认支付创建订单
     */
    public int insertFront(Order e) {
		return dao.insert("front.order.insert", e);
	}
    
    /**
     * cart 查询订单
     * @param e
     * @return
     */
    public Order selectFrontOne(Order e) {
		return (Order) dao.selectOne("front.order.selectOne", e);
	}
    
    /**
     * 个人中心中查询订单列表
     * @param e
     * @return
     */
    public List selectFrontList(Order e) {
		return dao.selectList("front.order.selectList", e);
	}
    
    /**
     * 个人中心查询分页订单列表
     */
    public PagerModel selectFrontPageList(Order e) {
		return dao.selectPageList("front.order.selectPageList",
				"front.order.selectPageCount", e);
	}
    
    /**
     * 前端下单修改订单状态
     */
    public int updateFront(Order e) {
		return dao.update("front.order.update", e);
	}
    
    /**
     * 前段根据订单ID得到订单堆箱
     * @param id
     * @return
     */
    public Order selectFrontById(String id) {
		return (Order) dao.selectOne("front.order.selectById", id);
	}

    /**
   	 * 保存支付宝支付日志
   	 * @param params
     * @return 
   	 */
	public int saveAlipayLog(Map<String, String> params) 
	{
		 SqlSession session = dao.openSession();
	        try 
	        {
	            return session.insert("front.alipaylog.insert", params);
	        }
	        catch (RuntimeException e) 
	        {
	            throw e;
	        }
	}
}
