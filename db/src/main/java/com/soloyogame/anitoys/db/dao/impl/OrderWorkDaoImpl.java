package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.db.commond.OrderWork;
import com.soloyogame.anitoys.db.dao.OrderWorkDao;

@Repository("orderWorkDaoManage")
public class OrderWorkDaoImpl implements OrderWorkDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    @Override
    public int insert(OrderWork e) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int delete(OrderWork e) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int update(OrderWork e) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public OrderWork selectOne(OrderWork e) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PagerModel selectPageList(OrderWork e) {
        return dao.selectPageList("manage.orderWork.selectPageList", "manage.orderWork.selectPageCount", e);
    }

    @Override
    public List<OrderWork> selectList(OrderWork e) {
        return dao.selectList("manage.orderWork.selectList", e);
    }

    @Override
    public int deleteById(int id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public OrderWork selectById(String id) {
        // TODO Auto-generated method stub
        return null;
    }

}
