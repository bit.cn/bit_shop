package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.TopPicture;

/**
 * 平台首页图片DAO接口
 * @author 索罗游
 */
public interface PlatTopPictureDao extends DaoManager<TopPicture> 
{
}
