package com.soloyogame.anitoys.db.dao;

import java.util.List;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.MyOrderRepairs;
import com.soloyogame.anitoys.db.commond.OrderRepairs;
import com.soloyogame.anitoys.db.page.PagerModel;

public interface OrderRepairsDao extends DaoManager<OrderRepairs> {

    /**
     * 用户确认售后
     */
    public int updateStatus(String id);

    /**
     * 计算总数
     *
     * @param e
     * @return
     */
    public MyOrderRepairs selectRepairSimpleReport(OrderRepairs e);

    /**
     * 查询售后订单详情
     *
     * @param e
     * @return
     */
    public MyOrderRepairs selectOneOrder(OrderRepairs e);

    /**
     * 查询售后订单详情
     *
     * @param e
     * @return
     */
    public List<MyOrderRepairs> selectOrderInfoList(OrderRepairs e);

    /**
     * 根据UserID查询售后订单
     *
     * @param e
     * @return
     */
    public List<OrderRepairs> selectByUserId(OrderRepairs e);

    /**
     * 分页查询
     *
     * @param e
     * @return
     */
    public PagerModel selectFrontPageList(OrderRepairs e);
    
    /**
     * 前端个人中心录入售后单信息
     */
	public int insertFront(OrderRepairs e);

}
