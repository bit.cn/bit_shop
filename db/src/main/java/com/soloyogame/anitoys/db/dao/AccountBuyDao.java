package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.AccountBuy;

/**
 * 购买数量
 * @author 索罗游
 */
public interface AccountBuyDao extends DaoManager<AccountBuy>{

}
