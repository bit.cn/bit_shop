package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;
import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 店铺仓库bean
 *
 * @author earl.k.wang
 */
public class WareHouse extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 仓库编号
     */
    private String id;
    /**
     * 仓库名称
     */
    private String wareHouseName;
    /**
     * 店铺编号
     */
    private String businessId;

    /**
     * 获取仓库编号
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * 设置仓库编号
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取仓库名称
     *
     * @return
     */
    public String getWareHouseName() {
        return wareHouseName;
    }

    /**
     * 设置仓库名称
     *
     * @param wareHouseName
     */
    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    /**
     * 获取商家编号
     *
     * @return
     */
    public String getBusinessId() {
        return businessId;
    }

    /**
     * 设置商家编号
     *
     * @param businessId
     */
    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

}
