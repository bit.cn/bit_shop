package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;

public class BusinessDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;      	//订单号
    private String parentId;	//父订单号
    private String account; 	//购买人
    private double ptotal;  	//购买总金额
    private String orderCode;	//订单号
    private String accountName; //用户姓名

    
    public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public double getPtotal() {
        return ptotal;
    }

    public void setPtotal(double ptotal) {
        this.ptotal = ptotal;
    }

}
