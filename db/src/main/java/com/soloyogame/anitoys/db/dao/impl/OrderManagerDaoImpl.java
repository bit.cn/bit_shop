package com.soloyogame.anitoys.db.dao.impl;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.dao.OrderManagerDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import org.springframework.stereotype.Repository;
import com.soloyogame.anitoys.db.commond.OrderManager;
import javax.annotation.Resource;
import java.util.List;

@Repository("orderManagerDaoManage")
public class OrderManagerDaoImpl implements OrderManagerDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(OrderManager e) {

        return dao.selectPageList("manage.orderManage.selectPageList", "manage.orderManage.selectPageCount", e);
    }

    public List<OrderManager> selectList(OrderManager e) {
        return dao.selectList("manage.orderManage.selectList", e);
    }

    public int deleteById(int id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int insert(com.soloyogame.anitoys.db.commond.OrderManager e) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int delete(com.soloyogame.anitoys.db.commond.OrderManager e) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int update(com.soloyogame.anitoys.db.commond.OrderManager e) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public com.soloyogame.anitoys.db.commond.OrderManager selectOne(
            com.soloyogame.anitoys.db.commond.OrderManager e) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public com.soloyogame.anitoys.db.commond.OrderManager selectById(String id) {
        // TODO Auto-generated method stub
        return null;
    }

}
