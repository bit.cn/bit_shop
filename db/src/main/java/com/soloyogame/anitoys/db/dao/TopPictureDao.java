package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.TopPicture;

/**
 * 商铺首页图片DAO接口
 *
 * @author Liam
 */
public interface TopPictureDao extends DaoManager<TopPicture> 
{
    
	/**
	 * business 前端商家的大首页图片
	 * @param topPicture
	 * @return
	 */
    public TopPicture selectFrontOne(TopPicture topPicture);
}
