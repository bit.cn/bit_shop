package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;
import java.util.Date;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 首页图片广告信息表（新老项目字段一致）
 *
 * @author shaojian
 */
public class IndexImg extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;
    //private String id;       //主键ID
    private String title;    //标题
    private String picture;  //图片的路径
    private int order1;      //排序
    private String desc1;    //图片描述
    private String link;     //图片的链接地址
    private int type;        //图片类型(1：商城首页的轮播图2：APP首页的轮播图3:APP商铺统一轮播图4:APP首页推荐图)
    private String memo;     //备注
    private Date createTime; //创建时间
    private Date updateTime; //更新时间
    private int status;      //状态(1-上线；2-下线)
    /**
     * 商家编号
     */
    private String businessId;

    @Override
    public void clear() {
        //this.id = null;
        this.title = null;
        this.picture = null;
        this.order1 = 0;
        desc1 = null;
        link = null;
        status = 0;
        memo = null;
        type = 0;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDesc1() {
        return desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    /*public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
     */
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getOrder1() {
        return order1;
    }

    public void setOrder1(int order1) {
        this.order1 = order1;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

}
