package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 字典类
 *
 * @author Administrator
 *
 */
public class AppDictItem implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 字典id
     */
    private String id;
    /**
     * 字典父类id
     */
    private String pid;
    /**
     * 字典值
     */
    private String itemName;
    /**
     * 字典子类
     */
    private List<AppDictItem> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public List<AppDictItem> getChildren() {
        return children;
    }

    public void setChildren(List<AppDictItem> children) {
        this.children = children;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

}
