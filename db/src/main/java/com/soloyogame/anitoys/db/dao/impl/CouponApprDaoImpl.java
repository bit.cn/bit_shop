package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.CouponAppr;
import com.soloyogame.anitoys.db.dao.CouponApprDao;
import com.soloyogame.anitoys.db.page.PagerModel;

import javax.annotation.Resource;

/**
 * 优惠券DAO实现类
 *
 * @author xukezhen
 */
@Repository("CouponApprDaoManage")
public class CouponApprDaoImpl implements CouponApprDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(CouponAppr e) {
        return dao.selectPageList("manage.couponAppr.selectPageList",
                "manage.couponAppr.selectPageCount", e);
    }

    public List selectList(CouponAppr e) {
        return dao.selectList("manage.couponAppr.selectList", e);
    }

    public CouponAppr selectOne(CouponAppr e) {
        return (CouponAppr) dao.selectOne("manage.couponAppr.selectOne", e);
    }

    public int delete(CouponAppr e) {
        return dao.delete("manage.couponAppr.delete", e);
    }

    public int update(CouponAppr e) {
        return dao.update("manage.couponAppr.update", e);
    }

    public int deletes(String[] ids) {
        CouponAppr e = new CouponAppr();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(CouponAppr e) {
        return dao.insert("manage.couponAppr.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.couponAppr.deleteById", id);
    }

    @Override
    public CouponAppr selectById(String id) {
        return (CouponAppr) dao.selectOne("manage.couponAppr.selectById", id);
    }

    public int updateBySn(Map<String, Object> map) {
        return dao.update("manage.couponAppr.updateBySn", map);
    }
}
