package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 大首页图片实体类（新老项目字段一致）
 *
 * @author 索罗游
 */
public class TopPicture extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 编号
     */
    private String id;
    /**
     * 图片地址
     */
    private String topPicture;
    /**
     * 商家编号
     */
    private String businessId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopPicture() {
        return topPicture;
    }

    public void setTopPicture(String topPicture) {
        this.topPicture = topPicture;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

}
