package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;

public class AccountFinance extends com.soloyogame.anitoys.db.bean.AccountFinance implements Serializable {

	private static final long serialVersionUID = 1L;
	private String userId;   //用户id
	private String amount;   //账户余额
	private int points;      //账户积分点
	private int rank;        //我的积分
	private int cutRank;     //减少的积分
	private double cutAmount;//减少的余额
	
	public int getCutRank() {
		return cutRank;
	}
	public void setCutRank(int cutRank) {
		this.cutRank = cutRank;
	}
	public double getCutAmount() {
		return cutAmount;
	}
	public void setCutAmount(double cutAmount) {
		this.cutAmount = cutAmount;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
