package com.soloyogame.anitoys.db;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.util.exception.PrivilegeException;

/**
 * 封装mybatis最基本的数据库操作
 *
 * @author shaojian
 */
@Repository
public class BaseDao extends SqlSessionDaoSupport {

    protected static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BaseDao.class);

    private static final boolean SELECT_PRIVILEGE = false;

    /**
     * 打开session，mybatis中的session能进行数据库基本的操作
     *
     * @return SqlSession
     */
    public SqlSession openSession() {
        SqlSession session = getSqlSession();
        return session;
    }

    /**
     * 查询一条记录
     *
     * @param statement SQL语句
     * @return 如果没有返回null,成功返回数据对象
     */
    public Object selectOne(String statement) {
        SqlSession session = openSession();
        return session.selectOne(statement);
    }

    /**
     * 查询一条记录
     *
     * @param statement SQL语句
     * @param parameter 参数
     * @return 如果没有返回null,成功返回数据对象
     */
    public Object selectOne(String statement, Object parameter) {
        SqlSession session = openSession();
        return session.selectOne(statement, parameter);
    }

    /**
     * 分页查询
     *
     * @param selectList SQL语句
     * @param selectCount 返回数值数量
     * @param param 参数
     * @return 如果没有返回null,成功返回数据对象
     */
    public PagerModel selectPageList(String selectList, String selectCount, Object param) {
        SqlSession session = openSession();
        List list = session.selectList(selectList, param);
        PagerModel pm = new PagerModel();
        pm.setList(list);
        Object oneC = session.selectOne(selectCount, param);
        if (oneC != null) {
            pm.setTotal(Integer.parseInt(oneC.toString()));
        } else {
            pm.setTotal(0);
        }
        return pm;
    }

    /**
     * 查询多条记录
     *
     * @param statement SQL语句
     * @return 成功返回对象列表,失败返回null
     */
    public List selectList(String statement) {
        SqlSession session = openSession();
        return session.selectList(statement);
    }

    /**
     * 查询多条记录
     *
     * @param statement SQL语句
     * @param parameter 参数
     * @return 成功返回对象列表,失败返回null
     */
    public List selectList(String statement, Object parameter) {
        SqlSession session = openSession();
        return session.selectList(statement, parameter);
    }

    /**
     * 查询总数
     *
     * @param statement SQL语句
     * @param parameter 参数
     * @return 成功返回对象列表,失败返回null
     */
    public int getCount(String statement, Object parameter) {
        SqlSession session = openSession();
        return (Integer) session.selectOne(statement, parameter);
    }

    /**
     * 插入一条记录
     *
     * @param statement sql语句
     * @return
     */
    public int insert(String statement) {
        if (SELECT_PRIVILEGE) {
            throw new PrivilegeException("只具备查询的权限！");
        }
        SqlSession session = openSession();
        try {
            return session.insert(statement);
        } catch (RuntimeException e) {
            throw e;
        }

    }

    /**
     * 插入一条记录，成功则返回插入的ID；失败则抛出异常
     *
     * @param statement sql语句
     * @param parameter 参数
     * @return
     */
    public int insert(String statement, Object parameter) {
        if (SELECT_PRIVILEGE) {
            throw new PrivilegeException("只具备查询的权限！");
        }
        SqlSession session = openSession();
        int row = session.insert(statement, parameter);
        if (row == 1) {
            return Integer.valueOf(((PagerModel) parameter).getId());
        }
        throw new RuntimeException();
    }

    /**
     * 更新一条记录
     *
     * @param statement sql语句
     * @return 返回影响的行数
     */
    public int update(String statement) {
        if (SELECT_PRIVILEGE) {
            throw new PrivilegeException("只具备查询的权限！");
        }
        SqlSession session = openSession();
        return session.update(statement);
    }

    /**
     * 更新一条记录
     *
     * @param statement sql语句
     * @param parameter 参数
     * @return 返回影响的行数
     */
    public int update(String statement, Object parameter) {
        if (SELECT_PRIVILEGE) {
            throw new PrivilegeException("只具备查询的权限！");
        }
        SqlSession session = openSession();
        int row = session.update(statement, parameter);
        if (row == 1) {
            if (parameter instanceof PagerModel) {
                String obj = ((PagerModel) parameter).getId();
                if (obj == null) {
                    return 0;
                }
                return Integer.valueOf(obj);
            }
        }
        return 1;
    }

    /**
     * 删除一条记录
     *
     * @param statement
     * @return
     */
    public int delete(String statement) {
        if (SELECT_PRIVILEGE) {
            throw new PrivilegeException("只具备查询的权限！");
        }
        SqlSession session = openSession();
        return session.delete(statement);
    }

    /**
     * 删除一条记录
     *
     * @param statement
     * @param parameter
     * @return
     */
    public int delete(String statement, Object parameter) {
        if (SELECT_PRIVILEGE) {
            throw new PrivilegeException("只具备查询的权限！");
        }
        SqlSession session = openSession();
        return session.delete(statement, parameter);
    }

}
