package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.bean.StrongAdvertising;

public interface StrongAdvertisingDao extends DaoManager<StrongAdvertising> {

    public void deletess(String[] ids);
}
