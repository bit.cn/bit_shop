package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.db.commond.ServiceGuarantee;
import com.soloyogame.anitoys.db.dao.ServiceGuaranteeDao;

@Repository
public class ServiceGuaranteeDaoImpl implements ServiceGuaranteeDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    @Override
    public int insert(ServiceGuarantee e) {
        return dao.insert("front.serviceGuarantee.insert", e);
    }

    @Override
    public int delete(ServiceGuarantee e) {
        return dao.delete("front.serviceGuarantee.delete", e);
    }

    public int deletes(String[] ids) {
        ServiceGuarantee e = new ServiceGuarantee();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    @Override
    public int update(ServiceGuarantee e) {
        return dao.update("front.serviceGuarantee.update", e);
    }

    @Override
    public ServiceGuarantee selectOne(ServiceGuarantee e) {
        return (ServiceGuarantee) dao.selectOne("front.serviceGuarantee.selectOne", e);
    }

    @Override
    public PagerModel selectPageList(ServiceGuarantee e) {
        return dao.selectPageList("front.serviceGuarantee.selectPageList",
                "manage.serviceGuarantee.selectPageCount", e);
    }

    @Override
    public List<ServiceGuarantee> selectList(ServiceGuarantee e) {
        return dao.selectList("front.serviceGuarantee.selectList", e);
    }

    @Override
    public int deleteById(int id) {
        return dao.delete("front.serviceGuarantee.deleteById", id);
    }

    @Override
    public ServiceGuarantee selectById(String id) {
        return (ServiceGuarantee) dao.selectOne("front.serviceGuarantee.deleteById", id);
    }

}
