package com.soloyogame.anitoys.db.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.commond.AccountPointsLog;
import com.soloyogame.anitoys.db.dao.PointsManageDao;
import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 会员账户积分日志
 *
 * @author jiangyongzhi
 */
@Repository
public class PointsManageDaoImpl implements PointsManageDao {

    @Resource
    private BaseDao dao;

    @Override
    public int delete(AccountPointsLog e) {
        return 0;
    }

    @Override
    public int deleteById(int id) {
        return 0;
    }

    @Override
    public int insert(AccountPointsLog e) {
        return dao.insert("manage.pointsManage.insert", e);
    }

    @Override
    public PagerModel selectPageList(AccountPointsLog e) {
        return dao.selectPageList("manage.pointsManage.selectPageList", "manage.pointsManage.selectPageCount", e);
    }

    @Override
    public AccountPointsLog selectById(String id) {
        return null;
    }

    @Override
    public List<AccountPointsLog> selectList(AccountPointsLog e) {
        return null;
    }

    @Override
    public AccountPointsLog selectOne(AccountPointsLog e) {
        return null;
    }

    @Override
    public int update(AccountPointsLog e) {
        return 0;
    }
}
