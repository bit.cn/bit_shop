package com.soloyogame.anitoys.db.commond;

import java.io.Serializable;
import com.soloyogame.anitoys.db.QueryModel;

/**
 * 保存客户的购买数量的实体类
 * @author 索罗游
 */
public class AccountBuy extends QueryModel implements Serializable {
    private static final long serialVersionUID = 1L;
    private String accountId;                   //用户ID
    private int productId;                      //商品ID
    private int buyNum;                         //购买数量

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(int buyNum) {
        this.buyNum = buyNum;
    }
}
