package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;

import com.soloyogame.anitoys.db.QueryModel;

/**
 * 广告位的实体类 (新老项目字段一致)
 *
 * @author shaojian
 */
public class BusinessAdvert extends QueryModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    private String title;// 广告标题
    private String advertLink;// 广告链接
    private String advertImage;// 广告图片
    private int advertPosition;// 广告位(1,2,3,4);
    private String businessId;// 店铺id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAdvertLink() {
        return advertLink;
    }

    public void setAdvertLink(String advertLink) {
        this.advertLink = advertLink;
    }

    public String getAdvertImage() {
        return advertImage;
    }

    public void setAdvertImage(String advertImage) {
        this.advertImage = advertImage;
    }

    public int getAdvertPosition() {
        return advertPosition;
    }

    public void setAdvertPosition(int advertPosition) {
        this.advertPosition = advertPosition;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}
