/**
 * 
 */
package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.DeductibleVoucherSn;

/**
 * @author kacey
 *
 */
public interface DeductibleVoucherSnDao extends DaoManager<DeductibleVoucherSn>{

    public DeductibleVoucherSn findEffectiveDeductibleSnCode(DeductibleVoucherSn e);
}
