package com.soloyogame.anitoys.db.bean;

import java.io.Serializable;
import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 用户优惠券
 *
 * @author shaojian
 */
public class AccountCoupon extends PagerModel implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 编号
     */
    private String id;
    
    /**
     * 优惠券ID
     */
    private String couponId;
    
    /**
     * 优惠券号码
     */
    private String couponSn;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 状态(0.待领取1.已领取2.已使用3.已过期)
     */
    private int status;
    /**
     * 领取时间
     */
    private String addTime;
    /**
     * 使用时间
     */
    private String useTime;
    /**
     * 使用订单id
     */
    private String orderId;
    /**
     * 过期日期
     */
    private String expireTime;
    /**
     * 优惠券名称
     */
    private String couponName;
    /**
     * 优惠券类型(1.抵价券,2.折扣券)
     */
    private int couponType;
    /**
     * 优惠券优惠值
     */
    private String couponValue;
    /**
     * 优惠规则券
     */
    private String couponRule;
    /**
     * 商铺id
     */
    private String businessId;
    /**
     * 商铺名
     */
    private String businessName;
    /**
     * 有效期开始日期
     */
    private String startTime;
    /**
     * 有效期结束日期
     */
    private String endTime;

    public void clear() {
        super.clear();
        //id=null;
        couponSn = null;
        userId = null;
        status = 0;
        addTime = null;
        useTime = null;
        orderId = null;
        expireTime = null;

        couponName = null;
        couponValue = null;
        couponType = 0;
        couponRule = null;
        businessId = null;
        businessName = null;
        startTime = null;
        endTime = null;
    }

    
    public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCouponSn() {
        return couponSn;
    }

    public void setCouponSn(String couponSn) {
        this.couponSn = couponSn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public int getCouponType() {
        return couponType;
    }

    public void setCouponType(int couponType) {
        this.couponType = couponType;
    }

    public String getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(String couponValue) {
        this.couponValue = couponValue;
    }

    public String getCouponRule() {
        return couponRule;
    }

    public void setCouponRule(String couponRule) {
        this.couponRule = couponRule;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "AccountCoupon [id=" + id + ", couponSn=" + couponSn
                + ", userId=" + userId + ", status=" + status + ", addTime="
                + addTime + ", useTime=" + useTime + ", orderId=" + orderId
                + ", expireTime=" + expireTime + ", couponName=" + couponName
                + ", couponType=" + couponType + ", couponValue=" + couponValue
                + ", couponRule=" + couponRule + ", businessId=" + businessId
                + ", businessName=" + businessName + ", startTime=" + startTime
                + ", endTime=" + endTime + "]";
    }

}
