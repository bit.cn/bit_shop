package com.soloyogame.anitoys.db.dao;

import java.util.List;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.FavoriteBusiness;
import com.soloyogame.anitoys.db.commond.FavoriteBusinessResult;

public interface FavoriteBusinessDao extends DaoManager<FavoriteBusiness> {

    int selectCount(FavoriteBusiness favorite);

    //查询用户所有收藏的店铺
    List<FavoriteBusinessResult> selectFavoriteBusiness(FavoriteBusiness favoritebusi);

    //查询用户收藏的店铺ID,不关联店铺内商品信息
    List<FavoriteBusinessResult> selectFavoriteBusinessId(FavoriteBusiness favoritebusi);

    //查询店铺即将收藏的店铺信息
    FavoriteBusinessResult selectBusinessInfo(FavoriteBusinessResult favoriteBusinessResult);

    //根据用户ID和店铺ID，查询是否已收藏过该店铺
    List<FavoriteBusinessResult> selectFavoriteBusinessByAccountBusiID(FavoriteBusiness favBusi);
}
