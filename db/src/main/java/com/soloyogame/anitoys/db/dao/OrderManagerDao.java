package com.soloyogame.anitoys.db.dao;

import com.soloyogame.anitoys.db.DaoManager;
import com.soloyogame.anitoys.db.commond.OrderManager;

public interface OrderManagerDao extends DaoManager<OrderManager> {

}
