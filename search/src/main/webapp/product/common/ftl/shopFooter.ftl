<#macro shopFooter title="" checkLogin=true>
<div class="footer">
  <div class="footer_top">
    <div class="ftmain">
      <div class="ftleft">
        <ul>
        <#list systemManager().newsCatalogs as item>
	          <li>
	            <h3>${item.name!""}</h3>
	            <#if item.news??>
					<#list item.news as item>
		            	<a href="${systemSetting().doc}/help/${item.code}.html" target="_blank">
		                     ${item.title!""}
		                </a>
	                </#list>
				</#if>
	            </li>
            </#list>
        </ul>
      </div>            
      <div class="ftphone">
        <div class="ftp"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="footer_bottom">@2014 阿尼托 上海索罗游信息技术有限公司 anitoys.com 版权所有 沪ICP备11047967号-18</div>
</div>
</#macro>
