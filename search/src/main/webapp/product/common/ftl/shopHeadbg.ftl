<!--搜索栏-->
<#macro shopHeadbg>

<div class="headbg">
  <div class="header">
    <div class="head_img"><img src="${systemSetting().staticSource}/shopindex/layout/img/topimg.jpg" width="207" height="145"></div>
    <div class="h_logo"><a class="fp-logo fp-iconfont" href="${systemSetting().www}" title="anitoys"><img src="${systemSetting().staticSource}/shopindex/layout/img/logo.gif"></a></div>
    <!-- search查询输入框 -->
    <form class="form-inline" role="form" name="searchForm" id="searchForm" method="post" action="${systemSetting().search}/search.html">
    <div class="h_input">
      <div class="h_itop">
        <input type="text" name="key"  value="${key!""}" placeholder="请输入商品关键字" accesskey="s" autocomplete="off" x-webkit-speech="" x-webkit-grammar="builtin:translate" id="mq" role="combobox" aria-haspopup="true" class="combobox-input" onfocus="if(this.value=='ANI大搜查') {this.value='';}this.style.color='#333';" onblur="if(this.value=='') {this.value='ANI大搜查';this.style.color='#999999';}" value="ANI大搜查"/  maxlength="24">
        
		    <button style="height:40px;border:none;width:100px;text-align:center;line-height:40px;
        	background:#ff6600;color:#FFF;font-size: 14px;font-weight:bold;cursor: pointer;
		    position: absolute;" type="submit" onclick="search();">搜整站</button>
      </div>
      <div class="h_iul"> <span>热门作品：</span>
      	<#list systemManager().hotqueryList as item>
      		<li><a href="${item.url}">${item.key1!""}</a></li>
		</#list>
      </div>
    </div>
    </form>
    <div class="clear"></div>
  </div>
  <script type="javascript">
//搜索商品
function search(){
alert(1);
	var _key = $.trim($("#mq").val());
	if(_key==''){
		return false;
	}
	$("#searchForm").submit();
}
</script>
</#macro>
