<!--导航栏-->
<#macro shopChannel checkLogin=true>
  <div class="menu" style="margin-top: -20px;">
    <div class="menucont">
      <div class="menuc_left">所有周边[共有周边${productCount!""}个]</div>
      <div class="menuc_main">
        <ul>
        </ul>
        </div>
      <!--购物车-->
      <div class="menuc_right">
        <div class="gwcnum"><i>${productList?size}</i></div>
        <div class="mr_icon">&nbsp;</div>
        <div class="mr_link"><a href="${systemSetting().card}/cart/cart.html">我的购物车</a></div>
        <div class="gwctk">
          <ul>
          <#if productList?? && productList?size gt 0>
          <#list productList as item>
            <li>
              <span class="gwcimg">
             	<a href="${systemSetting().card}/cart/cart.html" target="_blank">
              	<img src="${systemSetting().imageRootPath}/${item.picture}" width="52" height="52"></a>
              	</span><span class="gwctext">
              	<a href="${systemSetting().card}/cart/cart.html">${item.name!""}</a>
              <p>有特典</p>
              </span><span class="gwcright"><strong>¥ ${item.nowPrice}</strong><a href="#">删除</a></span>
              <div class="clear"></div>
            </li>
            </#list>
            </#if>
          </ul>
          <div class="gwcbtn"><span>共${productList?size}件商品 <b>共计¥${cartInfo.amount!""}</b></span><strong><a href="${systemSetting().card}/cart/cart.html" target="_blank">查看购物车</a></strong></div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </#macro>