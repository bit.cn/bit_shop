<#import "/ftl/htmlBase.ftl" as htmlBase>
<#if (e.businessId?? && e.businessId!="")>
<#import "/ftl/top1.ftl" as shopTop>
<#else>
<#import "/ftl/top.ftl" as shopTop>
</#if>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/bootstrap.min.css" type="text/css">
	<@htmlBase.htmlBase>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/main/index.css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/base.css" type="text/css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/css.css" />
	<link rel="stylesheet" href="${systemSetting().staticSource}/resource/validator-0.7.0/jquery.validator.css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<style type="text/css">
				img {
				border: 0px;
			}
			
			.thumbnail_css {
				border-color: #f40;
			}
			
			.attr_css {
				font-size: 100%;
				float: left;
			}
			
			.left_product {
				font-size: 12px;
				max-height: 35px;
				overflow: hidden;
				text-overflow: ellipsis;
				-o-text-overflow: ellipsis;
			}
			
			.left_title {
				display: block;
				/* width: 280px; */
				overflow: hidden;
				white-space: nowrap;
				-o-text-overflow: ellipsis;
				text-overflow: ellipsis;
			}
			
			img.err-product {
				background: url(${systemSetting().defaultProductImg}) no-repeat 50% 50%;
				.lazy {
					display: none;
				}
				.centerImageCss {
					width: 289px;
					height: 190px;
				}
				.title {
					display: block;
					width: 280px;
					overflow: hidden;
					/*注意不要写在最后了*/
					white-space: nowrap;
					-o-text-overflow: ellipsis;
					text-overflow: ellipsis;
				}
				body {
					padding-top: 0px;
					padding-bottom: 0px;
					font-size: 12px;
					/*    	font-family: 微软雅黑, Verdana, sans-serif, 宋体; */
				}
				.hotSearch {
				cursor: pointer;
			}
		</style>
<title>产品列表</title>
</head>
<body>
<!--商城主页top页-->
<@shopTop.shopTop/>
<div id="wrap">
	<div class="container">
		<div class="row">
			<div class="col-xs-3">
				<#include "/catalog_superMenu.ftl">
				</br>
				<#include "/product/productlist_left_picScroll.ftl">
			</div>
			<div class="col-xs-9">
				<!-- 导航栏 -->
				<div class="row">
					<#if e.mainCatalogName??>
						<div style="border: 0px solid;text-align: left;">
							<div>
								<ol class="breadcrumb" style="margin-bottom: 0px;">
								  <li class="active">
								  	<a href="${basepath}/catalog/${mainCatalogCode}.html">
								  		${e.mainCatalogName!""}
									</a>
								  </li>
								  <#if e.childrenCatalogName??>
									  <li class="active"><a href="#">${e.childrenCatalogName!""}</a></li>
								  </#if>
								</ol>
							</div>
						</div>
					</#if>
				</div>
				
				<!-- 条件搜索栏 -->
				<div class="row" style="margin: 10px 0px;">
					<div class="col-xs-12">
						<#assign map={"0":"全部商品","1":"现货商品","2":"预售商品"}/>
						<#if catalogChildren??>
							<div>
								<span style="margin:5px;font-weight: bold;">类型：</span>
								<#list map?keys as key1>
									<span class="label ${(code??&&key==code)?string("label-success","")}" style="margin-right:5px;font-size:100%;">
										<#if businessId??>
											<a href="${basepath}/search.html?key=${key!""}&businessId=${businessId!""}&productType=${key1}">${map[key1]}</a>
										<#else>
											<a href="${basepath}/search.html?key=${key!""}&businessId=${businessId!""}&productType=${key1}">${map[key1]}</a>
										</#if>
									</span>
								</#list>
								<br/>
								<span style="margin:5px;font-weight: bold;">分类</span>
								<#list catalogChildren as item>
									<span class="label ${(catalogCode?? && item.code==catalogCode)?string("label-success","label-info2")}" style="margin:5px;font-size:100%;">
										<a href="${basepath}/catalog/${item.code!""}.html">${item.name!""}</a>
									</span>
								</#list>
							</div>
						</#if>
						<#if attrs??>
							<div class="panel panel-default" style="margin:10px 0px;">
					              <div class="panel-body" style="font-weight: normal;text-align: center;">
										<div style="padding-left: 0px;">
											<#list attrs as item>
												<div class="row" style="margin-bottom: 5px;margin-left:-45px;">
													<div class="col-xs-2" style="text-align: right;">
														${item.name!""}：
													</div>
													<div class="col-xs-10" style="text-align: 
                                                 left;height:30px;width:650px;margin-left:0px;">
														<#if item.attrList??>
														<#list item.attrList as item>
															<#if e.attrID??&&item.id==e.attrID&&item_index <4 >
																
																
																<span style="line-height:10px;" class="label label-success attr_css">
																
															
														
																		
																		<a href="${basepath}/catalog/attr/${item.id!""}.html?orderBy=${item.orderBy!0}">${item.name!""}&nbsp;&nbsp;</a>
															
																</span>
												                      
															<#elseif item_index <8 >
														
																<span class="label label-info2 attr_css">
															
																		<a href="${basepath}/catalog/attr/${item.id!""}.html?orderBy=${item.orderBy!0}">${item.name!""}&nbsp;&nbsp;</a>
																
																</span>
															
															</#if>
														
														</#list>
														</#if>
														  <br>

													</div>
													
													<div>
													<a href="#" style="margin-left:-40px;" id="moreType" ">更多  </a>
													</div>
													
												</div>
													<div id="newType" style="display:none;width:20px;height:20px;margin-left:115px;">
													<#if item.attrList??>
														<#list item.attrList as item>
														<#if (item_index >=8) >
																<span class="label label-info2 attr_css">
																		<a href="${basepath}/catalog/attr/${item.id!""}.html?orderBy=${item.orderBy!0}">${item.name!""}&nbsp;&nbsp;</a>
																</span>
															</#if>
															</#list>
															</#if>
													</div>
                                        												<p style="color:gray;line-height:5px;">-&nbsp;-&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp;-&nbsp;-&nbsp;-&nbsp;-&nbsp;-&nbsp;-&nbsp;-&nbsp;-&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -

&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp;  -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; 

-&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; 

-&nbsp; -&nbsp; -&nbsp; -&nbsp; -&nbsp; </p> 
											</#list>
											
						              </div>
					              </div>
							</div>
						</#if>
					</div>
				</div>
		
				<!-- 排序栏 
				<#if productList1?? && productList1?size gt 0>
					<div class="row" style="margin: 0px;">
						<div class="col-xs-12">
			 
						</div>
					</div>
					<div ><hr style="clear: both;"></div>
				</#if>
				<div class="row">
					<!-- 商品展示 -->
					<div >
						<#if productList1?? && productList1?size gt 0>
						<#list productList1 as item>
						<div class="col-xs-3" style="padding: 5px;text-align: center;">
							<div class="thumbnail" style="width: 100%; display: block;">
								<div style="height: 200px;border: 0px solid;text-align: center;">
									<a href="${systemSetting().item}/product/${item.id!""}.html" target="_blank">
										<img class="lazy" style="border: 0px;display: block;margin: auto;max-height: 100%;
										max-width: 100%;"  
										border="0" src="${systemSetting().imageRootPath}${item.picture!""}"
										data-original="${systemSetting().imageRootPath}${item.picture!""}">
									</a>
									<#if item.productType??>
										<#if item.productType==2>
											<div style="position:absolute;top:0;left:0;">
												<img src="${systemSetting().staticSource}/static/frontend/v1/images/jshop/ys.png" class="carousel-inner img-responsive img-rounded">
											</div>
										</#if>
									</#if>
								</div>
								<div style="height: 40px;text-align: center;">
									<div class="col-xs-12 left_product">
										<div class="row">
											<a style="cursor: pointer;margin: auto;" 
											href="${systemSetting().item}/product/${item.id!""}.html" target="_blank"
											title="${item.name!""}">
												${item.name!""}
											</a>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<b style="font-weight: bold;color: #cc0000;">
											￥${item.nowPrice!""}
										</b>
									</div>
								</div>
							</div>
						</div>
						</#list>
						</div>
						<#else>
                                                                                    抱歉，没有找到<font color='#f40'>${key!""}</font>相关的宝贝!
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="font-size: 14px;font-weight: normal;">
                                        <div class="panel panel-default">
                                            <div class="panel-body" style="font-size: 16px;font-weight: normal;text-align: center;">
                                                <div class="panel-body" style="font-size: 16px;font-weight: normal;">
                                                    <span class="glyphicon glyphicon-ok"></span>
                                                    <span class="text-success">您可以尝试换一个关键词或者换一个分类。</span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
						</#if>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-xs-12" style="border: 0px solid;text-align: right;">
						<#if productList1??>
							<#include "/pager.ftl"/>
						</#if>
					</div>
				</div>
		
			</div>
		</div>
	</div>
	</div>
	<!-- 商城主页footer页 -->
	<@shopFooter.shopFooter/>
	<script src="${systemSetting().staticSource}/static/frontend/v1/js/hover.js"></script>
	<script src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery.SuperSlide.js"></script>
	<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/new.js"></script>
	<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
	<script type="text/javascript">
		$(function() {
			//商品鼠标移动效果
			$("div[class=thumbnail]").hover(function() {
				$(this).addClass("thumbnail_css");
			}, function() {
				$(this).removeClass("thumbnail_css");
			});
			$(".col-xs-3 img").addClass("carousel-inner img-responsive img-rounded");
		});
		//搜索商品
		function search(){
			var _key = $.trim($("#mq").val());
			if(_key==''){
				return false;
			}
		$("#searchForm").submit();
		}		
		
		function domore(){ 
		$("#newType").show();   
		}
		$(function(){
		$("#moreType").on("click",function(){
		$("#newType").toggle();//显示隐藏切换
		})
		})
		
		//搜索商品(整站搜索)
        function search() {
            var _key = $.trim($("#mq").val());
            if (_key == '') {
                return false;
            }
            $("#businessId").remove();
            $("#searchForm").attr("action","${systemSetting().search}/product/search.html")
            $("#searchForm").submit();
        }
        
        //搜索商品(本站搜索)
        function searchbusiness() {
            var _key = $.trim($("#mq").val());
            if (_key == '') {
                return false;
            }
            $("#searchForm").submit();
        }
        
        jQuery(".picScroll-top").slide({
				titCell: ".hd ul",
				mainCell: ".bd ul",
				autoPage: true,
				effect: "top",
				autoPlay: true,
				scroll: 2,
				vis: 5
			});
			jQuery(".slideTxtBox").slide();
	</script>
</@htmlBase.htmlBase>
</body>
</html>