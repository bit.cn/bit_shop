<#import "/resource/common_html_front.ftl" as html/>
<#import "/indexMenu.ftl" as menu/>
<@html.htmlBase>
<#if e.title??>
<title>${e.title!""}</title>
<#else>
<title>${e.name!""}</title>
</#if>

<#if e.description??>
<meta name="description" content="${e.description!""}" />
<#else>
<meta name="description" content="${e.name!""}" />
</#if>

<#if e.keywords??>
<meta name="keywords"  content="${e.keywords!""}" />
<#else>
<meta name="keywords"  content="${e.name!""}" />
</#if>

<link rel="shortcut icon" href="${systemSetting().shortcuticon}">
<style type="text/css">
.topCss {
	height: 28px;
	line-height: 28px;
	background-color: #f8f8f8;
	border-bottom: 1px solid #E6E6E6;
	padding-left: 9px;
	font-size: 14px;
	font-weight: bold;
	position: relative;
	margin-top: 0px;
}
.left_product{
	font-size: 12px;display: inline-block;overflow: hidden;text-overflow: ellipsis;-o-text-overflow: ellipsis;white-space: nowrap;max-width: 150px;
}
img.err-product {
background: url(${systemSetting().defaultProductImg}) no-repeat 50% 50%;
}

.nowPrice{
	color: #F00;
	font-family: "寰蒋闆呴粦";
	font-size: 20px;
}

.spec li{
	float: left;
	position: relative;
	margin: 0 4px 4px 0;
	line-height: 20px;
	vertical-align: middle;
	padding: 1px;
	border: 1px solid #ccc;
	cursor: pointer;
}

.specSelectCss{
	border: 2px solid #ff0000;
	color:red;
}

.specNotAllowed{
	color: #CDCDCD;
	cursor: not-allowed;
}

.lazy {
  display: none;
}
</style>
<script>
function defaultProductImg(){
	if(1==1){
		return;
	}
	var img=event.srcElement; 
	img.src="${systemSetting().defaultProductImg}";
	img.onerror=null; //鎺у埗涓嶈涓�洿璺冲姩 
}
</script>
	<div id="wrap">
		<@menu.menu selectMenu=topMenu/>
		<div class="container">
			<!-- 浜у搧璇︾粏淇℃伅-->
			<div class="row">
				<!-- 鐑棬鍟嗗搧鍒楄〃 -->
				<div class="col-xs-3" style="border: 1px solid #c0c0c0; text-align: left;">
					<div class="row" >
						<h4 class="topCss">鐣呴攢鍟嗗搧</h4>
					</div>
					<#if hotProductList??>
					<#list hotProductList as item>
						<div class="row">
							<div class="col-xs-3">
								
								<a style="width: 50px;height: 50px;" href="${systemSetting().item}/product/${item.id!""}.html" target="_blank" title="${item.name!""}">
									<img class="lazy" style="border: 0px;display: block;margin: auto;width: 50px;height: 50px;" 
									src="${systemSetting().defaultProductImg}"
									data-original="${systemSetting().imageRootPath}${item.picture!""}" />
								</a>
							</div>
							<div class="col-xs-9">
								<h4>
									<div class="left_product">
										<a title="${item.name!""}" href="${systemSetting().item}/product/${item.id!""}.html" target="_blank">
											${item.name!""}
										</a>
									</div>
								</h4>
								<div class="row">
									<div class="col-xs-6">
										<b style="font-weight: bold;color: #cc0000;">
											锟�{item.nowPrice!""}
										</b>
									</div>
									<div class="col-xs-6">
										<b style="text-decoration: line-through;font-weight: normal;font-size: 11px;color: #a5a5a5;">
											锟�{item.price!""}
										</b>
									</div>
								</div>
							</div>
						</div>
						<br>
					</#list>
					</#if>
					<!-- 鐗逛环鍟嗗搧 -->
					<div class="row">
						<h4 class="topCss">鐗逛环鍟嗗搧</h4>
					</div>
					<#list saleProducts as item>
						<div class="row">
							<div class="col-xs-3">
								<a href="${systemSetting().item}/product/${item.id!""}.html" target="_blank" title="${item.name!""}">
									
									<img class="lazy" style="border: 0px;display: block;margin: auto;width: 50px;height: 50px;"
                                         src="${systemSetting().defaultProductImg}"
                                         data-original="${systemSetting().imageRootPath}${item.picture!""}" />
								</a>
							</div>
							<div class="col-xs-9">
								<h4>
									<div class="left_product">
										<a title="${item.name!""}" href="${systemSetting().item}/product/${item.id!""}.html" target="_blank">
											${item.name!""}
										</a>
									</div>
								</h4>
								<div class="row">
									<div class="col-xs-6">
										<b style="font-weight: bold;color: #cc0000;">
											锟�{item.nowPrice!""}
										</b>
									</div>
									<div class="col-xs-6">
										<b style="text-decoration: line-through;font-weight: normal;font-size: 11px;color: #a5a5a5;">
											锟�{item.price!""}
										</b>
									</div>
								</div>
							</div>
						</div>
						<br>
					</#list>
					<#include "/history_productList.ftl"/>
				</div>
				
				<!-- 鍟嗗搧鍥剧墖鍒楄〃鍜岃喘涔版寜閽�-->
				<div class="col-xs-9" style="border: 0px solid red; text-align: left;">
					<!-- 瀵艰埅 -->
					<div class="row" style="margin-top: 0px;">
						<div class="col-xs-12">
							<ol class="breadcrumb" style="margin-bottom: 0px;">
							  <li>${e.mainCatalogName!""}</li>
							  <#if e.childrenCatalogName??>
								  <li class="active"><a href="${systemSetting().www}/catalog/${e.childrenCatalogCode!""}.html">${e.childrenCatalogName!""}</a></li>
							  </#if>
							</ol>
						</div>
					</div>
					
					<!-- 濡傛灉鍟嗗搧鏈夎禒鍝侊紝鍒欐樉绀哄埌姝ゅ -->
					<div class="row" style="margin-top: 10px;">
						<div class="col-xs-12">
							<#if e.gift??>
								<ul class="list-group" >
									<li class="list-group-item">
										鍟嗗搧璧犲搧锛�{e.gift.giftName!""}
										<button class="btn btn-link btn-xs" onclick="showGiftDetail()">銆愯鎯呫�</button>
										
										<div style="display: none;" id="giftDetailDiv">
											<div class="row" style="margin-top: 10px;">
												<div class="col-xs-6">
													<img class="lazy" style="border: 0px;display: block;margin: auto;max-width: 100%;" 
													src="${systemSetting().imageRootPath}${e.gift.picture!""}" />
												</div>
												<div class="col-xs-6">
													璧犲搧鍚嶇О锛�{e.gift.giftName!""}<br>
													甯傚満浠凤細${e.gift.giftPrice!""}<br>
													璧犲搧鏁伴噺鏈夐檺锛岃禒瀹屽嵆姝紒
												</div>
											</div>
										</div>
									</li>
								</ul>
							</#if>
						</div>
					</div>
					
					<div class="row" style="margin-top: 10px;">
						<div class="col-xs-6" id="productMainDiv">
							<#include "product_center_piclist_slide2.ftl"/>
							<!--
							<#--<ul id="myGallery">-->
								<#--<li style="max-width: 408px; height: 200px;"><img src="${e.maxPicture!""}" />-->
								<#--<#list e.imageList as item>-->
									<#--<li style="max-width: 408px; height: 200px;"><img src="${item!""}" />-->
								<#--</#ilist>-->
							<#--</ul>-->
							 -->
						</div>
						
						<!-- 浜у搧璇︾粏淇℃伅 -->
						<div class="col-xs-6">
							<div style="line-height: 20px;">
								<!-- 娲诲姩鍒ゆ柇锛屾樉绀篐TML -->
								<#if e.activityID??>
									<#if !e.expire>
										<ul class="list-group">
											<li class="list-group-item">
												<#if e.activityType??&&e.activityType=="c">
													<s:if test="e.discountType.equals(\"d\")">
														<span class="badge pull-right" style="background-color:red;">
															${e.discountFormat!""}鎶�/span>
														<span class="badge pull-left">鎶樻墸浠�															<b style="font-weight: bold;">
																锟�{e.finalPrice!""}
															</b>
														</span>
													</s:if>
													<s:elseif test="e.discountType.equals(\"r\")">
														<span class="label label-danger">淇冮攢浠�															<b style="font-weight: bold;">
																锟�{e.finalPrice!""}
															</b>
														</span>
													</s:elseif>
													<s:elseif test="e.discountType.equals(\"s\")">
														<span class="label label-success">鍙屽�绉垎</span>
													</s:elseif>
												<#elseif e.activityType??&&e.activityType=="j">
													<span class="badge pull-left" style="background-color:red;">鍏戞崲绉垎:
														<b style="font-weight: bold;">
															${e.exchangeScore!""}
														</b>
													</span>
												<#elseif e.activityType??&&e.activityType=="t">
													<span class="badge pull-left" style="background-color:red;">鍥㈣喘浠�
														<b style="font-weight: bold;">
															${e.tuanPrice!""}
														</b>
													</span>
												</#if>
												
							
												<br>
												<!-- 娲诲姩缁撴潫鏃堕棿鏄剧ず -->
												璺濈娲诲姩缁撴潫杩樺墿锛�div style="display: inline;" timer="activityEndDateTime" activityEndDateTime="${e.activityEndDateTime!""}"></div>
												<#if e.maxSellCount!=0>
													<br>
													<div>鏈�璐拱锛�{e.maxSellCount!""}浠�/div>
												</#if>
												<div>浼氬憳鑼冨洿锛�{e.accountRange!""}</div>
											</li>
										</ul>
									<#else>
										<ul class="list-group">
											<li class="list-group-item">
												娲诲姩宸茬粡缁撴潫锛�											</li>
										</ul>
									</#if>
								</#if>
								<!-- 娲诲姩鍒ゆ柇锛屾樉绀篐TML END-->
								
								<div class="row">
									<div class="col-xs-12">
										<div style="font-weight: bold;font-size: 18px;">${e.name!""}</div>
										甯傚満浠凤細<b style="text-decoration: line-through;font-weight: normal;font-size: 11px;color: #a5a5a5;">
											锟�{e.price!""}
										</b><br>
										鐜颁环锛�										<#if e.activityID?? && !e.expire && e.discountType!="s">
											<b class="nowPrice" style="text-decoration: line-through;font-weight: bold;">
										<#else>
											<b class="nowPrice" style="font-weight: bold;">
										</#if>
											锟�span id="nowPrice">${e.nowPrice!""}</span>
										</b><br>
										
										<#if e.score gt 0>
											璧犻�锛�{e.score!""}涓Н鍒嗙偣<br>
										</#if>
										閿�噺锛氬凡鍞�{e.sellcount!""}浠�										
									</div>
								</div>
								
								
								
								<#if e.specJsonString??>
									<!-- 鍟嗗搧瑙勬牸 -->
									<input type="hidden" name="specJsonString" id="specJsonString"/>
									<div style="border:0px solid red;" class="spec" id="specDiv">
										<dl>
											<dt style="float: left;">灏哄锛�/dt>
											<dd>
												<ul style="list-style: none;" id="specSize">
													<#list e.specSize as item>
														<li>${item!""}</li>
													</#list>
												</ul>
											</dd>
										</dl>
										<br>
										<dl>
											<dt style="float: left;">棰滆壊锛�/dt>
											<dd>
												<ul style="list-style: none;" id="specColor">
													<#list e.specColor as item>
														<li>${item!""}</li>
													</#list>
												</ul>
											</dd>
										</dl>
									</div>
								</#if>
								
								<form action="${basepath}/product/buyNow.html" namespace="/" method="post" theme="simple">
									<div class="row" style="margin-bottom: 10px;">
										<div class="col-xs-12">
											
											<br>璐拱鏁伴噺锛�											<span onclick="subFunc()" style="cursor: pointer;"><img src="${basepath}/resource/images/minimize.png" style="vertical-align: middle;"/></span>
											<input value="1" size="4" maxlength="4" name="inputBuyNum" id="inputBuyNum" style="text-align: center;"/>
											<!-- <a id="addProductToCartErrorTips" href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="璐拱鐨勫晢鍝佽秴鍑哄簱瀛樻暟锛�></a> -->
											<span onclick="addFunc(this,false)" style="cursor: pointer;"><img src="${basepath}/resource/images/maximize.png" style="vertical-align: middle;"/></span>
											
											
											(搴撳瓨锛�span id="stock_span_id">${e.stock!""}</span>
												${e.unit!""})<br>
											
											<!-- 瓒呭嚭搴撳瓨鎻愮ず璇�-> 
											<div id="exceedDivError" class="alert alert-danger fade in" style="display: none;margin-bottom: 0px;">
		<!-- 										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">脳</button> -->
												<h4 id="exceedSpanError"></h4>
											</div>
											
											<input type="hidden" name="id" value="${e.id!""}"/>
											<input type="hidden" name="addCart" value="1"/>
									
										</div>
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<#if e.status==2 && e.stock gt 0>
											
											
												<a name="stockErrorTips" productid="${id!""}" href="#" data-toggle="tooltip" title="" data-placement="top" ></a>
												<button type="button" data-toggle="show" data-placement="top" id="addToCartBtn" onclick="addToCart()" value="鍔犲叆璐墿杞� disabled="disabled" class="btn btn-primary btn-sm">
													<span class="glyphicon glyphicon-shopping-cart"></span>鍔犲叆璐墿杞�												</button>
											<#else>
												<button type="button" id="addToCartBtn" onclick="addToCart()" value="鍔犲叆璐墿杞� class="btn btn-primary btn-sm" disabled="disabled">
													<span class="glyphicon glyphicon-shopping-cart"></span>鍔犲叆璐墿杞�												</button>
											</#if>
											
											<button id="addToFavoriteBtn" type="button" onclick="addToFavorite()" class="btn btn-primary btn-sm" disabled="disabled" 
												data-container="body" data-toggle="popover" data-placement="right" data-content="">
												<span class="glyphicon glyphicon-flag"></span>鏀惰棌
											</button>
										</div>
									</div>
								</form>
								<br>
								${qqHelpHtml!""}
								<hr style="margin-top: 5px;margin-bottom: 5px;">
								
								
								<#if e.stock lte 0>
									<strong><font style="font-size: 14px;" class="text-danger">鎶辨瓑锛岃鍟嗗搧宸插敭鍗栧畬浜嗭紒</font></strong><br>
								<#elseif e.status==3>
									<strong><font style="font-size: 14px;" class="text-danger">鎶辨瓑锛岃鍟嗗搧宸蹭笅鏋讹紒</font></strong><br>
								</#if>
								<#if currentUser()?? && e.showEmailNotifyProductInput>
									<div class="row" style="margin-top: 10px;" id="emailNotifyProduct_input">
									  <div class="col-lg-12">
									    <div class="input-group">
									      <input type="text" class="form-control" placeholder="鍒拌揣閫氱煡鐨勯偖绠卞湴鍧� id="receiveEmail" maxlength="45" size="45">
									      <span class="input-group-btn">
									        <button class="btn btn-success" type="button" onclick="emailNotifyProduct(this)"><span class="glyphicon glyphicon-ok"></span>&nbsp;鍒拌揣閫氱煡</button>
									      </span>
									    </div>
									  </div>
									</div>
									<div class="row" style="margin-top: 10px;">
										<div class="col-lg-12 text-success" id="emailNotifyProductDiv"></div>
									</div>
								</#if>
											
							</div>
						</div>
					</div>
					
					<br>
					
					<div class="row">
						<div class="col-xs-12">
							<#include "/product_tab_slide.ftl"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><b>鎻愮ず淇℃伅:</b></h4>
      </div>
      <div class="modal-body" style="color: #7ABD54;font:normal 24px">
        <h3><span class="glyphicon glyphicon-ok"></span>&nbsp;鍟嗗搧宸叉垚鍔熷姞鍏ヨ喘鐗╄溅锛�/h3>
      </div>
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" onclick="javascript:$('#myModal').modal('hide');">缁х画璐墿</button>
        	<button class="btn btn-primary" data-dismiss="modal" value="鍘昏喘鐗╄溅缁撶畻" onclick="toCart();">
        		<span class="glyphicon glyphicon-usd"></span>鍘昏喘鐗╄溅缁撶畻
        	</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



	<input type="hidden" value="${e.name!""}" id="productName">
	<input type="hidden" value="${e.id!""}" id="productID">
	<input type="hidden" value="${e.nowPrice!""}" id="nowPriceHidden">
	<input type="hidden" value="${e.stock!""}" id="stockHidden">
	<input type="hidden" id="specIdHidden">

<script src="${basepath}/resource/js/product.js"></script>
<script src="${basepath}/resource/js/front.js"></script>
<script src="${basepath}/resource/js/superSlide/jquery.SuperSlide.js"></script>
<script src="${basepath}/resource/js/jquery.imagezoom/js/jquery.imagezoom.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".jqzoom").imagezoom();
	
	$("#thumblist li a").click(function(){
		$(this).parents("li").addClass("tb-selected").siblings().removeClass("tb-selected");
		$(".jqzoom").attr('src',$(this).find("img").attr("mid"));
		$(".jqzoom").attr('rel',$(this).find("img").attr("big"));
	});
});
</script>
<script>
$(function() {
	$("#addToCartBtn").removeAttr("disabled");
	$("#addToFavoriteBtn").removeAttr("disabled");
	
	jQuery(".slideTxtBox").slide();
	var ww = $("#productMainDiv").width();
	console.log("aww="+ww);
	$("#mainBox00").css("width",ww+"px");
	$("#mainBox00").find("img[name=box_img]").css("max-width",ww+"px");
	
	var specJsonStringVal = $("#specJsonString").val();
	
	//濡傛灉瑙勬牸瀛樺湪
	if(specJsonStringVal && specJsonStringVal.length>0){
		console.log("specJsonStringVal = " + specJsonStringVal);
		var specJsonStringObject = eval('('+specJsonStringVal+')');
		
		for(var i=0;i<specJsonStringObject.length;i++){
			console.log("specJsonStringObject = " + specJsonStringObject[i].specColor);
		}

		//瑙勬牸琚偣鍑伙紝鍒欐爣璁伴�涓拰涓嶉�涓�		$("#specDiv li").click(function(){
			console.log("瑙勬牸琚偣鍑汇�" + $(this).hasClass("specSelectCss"));
			if($(this).hasClass("specNotAllowed")){
				console.log("鐢变簬瑙勬牸琚鐢ㄤ簡锛岀洿鎺ヨ繑鍥炪�");
				return;
			}
			
			$(this).parent().find("li").not(this).each(function(){
				$(this).removeClass("specSelectCss");
				$(this).attr("disabled","disabled");
			});
			if($(this).is(".specSelectCss")){
				console.log("removeClass specSelectCss");
				$(this).removeClass("specSelectCss");
				
				//濡傛灉褰撳墠鐐瑰嚮鐨勬槸灏哄锛屽垯閲婃斁鎵�湁鐨勯鑹茬殑绂佺敤鐘舵�锛涘鏋滅偣鍑荤殑鏄鑹诧紝鍒欓噴鏀炬墍鏈夌殑灏哄绂佺敤鐘舵�
				if($(this).parent().attr("id")=="specSize"){
					console.log("褰撳墠鐐瑰嚮鐨勬槸灏哄銆�);
					//閲婃斁鎵�湁棰滆壊鐨勯紶鏍囩鐢ㄧ姸鎬�					$("#specColor li").each(function(){
						$(this).removeClass("specNotAllowed");
					});
				}else if($(this).parent().attr("id")=="specColor"){
					console.log("褰撳墠鐐瑰嚮鐨勬槸棰滆壊銆�);
					//閲婃斁鎵�湁棰滆壊鐨勯紶鏍囩鐢ㄧ姸鎬�					$("#specSize li").each(function(){
						$(this).removeClass("specNotAllowed");
					});
				}else{
					console.log("褰撳墠鐐瑰嚮鐨勪笢涓滀笉鏄庣‘銆�);
				}
			}else{
				console.log("addClass specSelectCss");
				$(this).addClass("specSelectCss");
			}
			
			//$("#specSize")
			
			var parentID = $(this).parent().attr("id");
			console.log("parentID = " + parentID);
			
			if($("#specSize li").hasClass("specSelectCss") && $("#specColor li").hasClass("specSelectCss")){
				console.log("閮介�涓簡銆�);
				
				console.log("閫変腑鐨勬枃鏈細"+$("#specSize .specSelectCss").html());
				//鎵惧嚭瀵瑰簲鐨勮鏍�				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					if(specItem.specSize==$("#specSize .specSelectCss").html() 
							&& specItem.specColor==$("#specColor .specSelectCss").html()){
						console.log("鎵惧埌浜嗚鏍煎璞°�");
						//鏀瑰彉鍟嗗搧鐨勪环鏍煎拰搴撳瓨鏁�						$("#nowPrice").text(specItem.specPrice);
						$("#stock_span_id").text(specItem.specStock);
						$("#specIdHidden").val(specItem.id);
						console.log("閫変腑鐨勮鏍糏D="+$("#specIdHidden").val());
						break;
					}
				}
				//specNotAllowed
			}else if($("#specSize li").hasClass("specSelectCss")){
				resetProductInfo();
				//灏哄琚�涓簡涓�釜锛屽垯灏嗕簬璇ュ昂瀵镐笉鍖归厤鐨勯鑹茬鐢ㄦ帀銆�				console.log("灏哄琚�涓簡涓�釜锛屽垯灏嗕簬璇ュ昂瀵镐笉鍖归厤鐨勯鑹茬鐢ㄦ帀銆�);
				//鎵惧嚭瀵瑰簲鐨勮鏍�				var colorArr = [];//涓庨�涓殑瑙勬牸鐩稿尮閰嶇殑棰滆壊闆嗗悎
				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					if(specItem.specSize==$("#specSize .specSelectCss").html()){
						colorArr.push(specItem.specColor);
					}
				}
				
				//閲婃斁鎵�湁棰滆壊鐨勯紶鏍囩鐢ㄧ姸鎬�				$("#specColor li").each(function(){
					$(this).removeClass("specNotAllowed");
				});
				
				//鎵惧嚭浜庨�鎷╃殑灏哄涓嶅尮閰嶇殑棰滆壊锛屽皢鍏剁鐢ㄦ帀銆�				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					var hanFind = false;
					for(var j=0;j<colorArr.length;j++){
						if(specItem.specColor==colorArr[j]){
							hanFind = true;
							break;
						}
					}
					
					if(!hanFind){
						console.log("绂佹帀鐨勯鑹叉湁锛�+specItem.specColor);
						
						$("#specColor li").each(function(){
							console.log("text="+$(this).text());
							if($(this).text()==specItem.specColor){
								console.log("鎵惧埌浜嗐�");
								$(this).addClass("specNotAllowed");
								return false;
							}
						});
					}
				}
				
			}else if($("#specColor li").hasClass("specSelectCss")){
				resetProductInfo();
				//棰滆壊琚�涓簡涓�釜锛屽垯灏嗕簬璇ラ鑹蹭笉鍖归厤鐨勫昂瀵哥鐢ㄦ帀銆�				console.log("棰滆壊琚�涓簡涓�釜锛屽垯灏嗕簬璇ラ鑹蹭笉鍖归厤鐨勫昂瀵哥鐢ㄦ帀銆�);
				
				//鎵惧嚭瀵瑰簲鐨勮鏍�				var sizeArr = [];//涓庨�涓殑瑙勬牸鐩稿尮閰嶇殑棰滆壊闆嗗悎
				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					if(specItem.specColor==$("#specColor .specSelectCss").html()){
						sizeArr.push(specItem.specSize);
					}
				}
				
				//閲婃斁鎵�湁棰滆壊鐨勯紶鏍囩鐢ㄧ姸鎬�				$("#specSize li").each(function(){
					$(this).removeClass("specNotAllowed");
				});
				
				//鎵惧嚭浜庨�鎷╃殑灏哄涓嶅尮閰嶇殑棰滆壊锛屽皢鍏剁鐢ㄦ帀銆�				for(var i=0;i<specJsonStringObject.length;i++){
					var specItem = specJsonStringObject[i];
					var hanFind = false;
					for(var j=0;j<sizeArr.length;j++){
						if(specItem.specSize==sizeArr[j]){
							hanFind = true;
							break;
						}
					}
					
					if(!hanFind){
						console.log("绂佹帀鐨勫昂瀵告湁锛�+specItem.specSize);
						
						$("#specSize li").each(function(){
							console.log("text="+$(this).text());
							if($(this).text()==specItem.specSize){
								console.log("鎵惧埌浜嗐�");
								$(this).addClass("specNotAllowed");
								return false;
							}
						});
					}
				}
				
			}else{
				console.log("閮芥病閫変腑銆�);
				resetProductInfo();
			}
			
		});
	}
	
});

//閲嶇疆鍟嗗搧淇℃伅
function resetProductInfo(){
	console.log("resetProductInfo..."+$("#nowPriceHidden").val());
	//璁剧疆鍊间负鍟嗗搧鍘熶环鏍�	$("#nowPrice").text($("#nowPriceHidden").val());
	$("#stock_span_id").text($("#stockHidden").val());
	$("#specIdHidden").val("");
}

//鍘昏喘鐗╄溅缁撶畻
function toCart(){
	window.location.href = "${systemSetting().www}/cart/cart.html";
}
var options={
		animation:true,
		trigger:'hover', //瑙﹀彂tooltip鐨勪簨浠�		show: 500, hide: 100
	};
//娣诲姞鍟嗗搧鏀惰棌
function addToFavorite(){
	var _url = "${systemSetting().item}/product/addToFavorite.html?productID="+$("#productID").val()+"&radom="+Math.random();
	console.log("_url="+_url);
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  success: function(data){
		  console.log("addToFavorite.data="+data);
		  var _result = "鍟嗗搧宸叉垚鍔熸坊鍔犲埌鏀惰棌澶癸紒";
		  if(data=="0"){
			  _result = "鍟嗗搧宸叉垚鍔熸坊鍔犲埌鏀惰棌澶癸紒";
		  }else if(data=='1'){
			  _result = "宸叉坊鍔狅紝鏃犻渶閲嶅娣诲姞锛�;
		  }else if(data=='-1'){//鎻愮ず鐢ㄦ埛瑕佸厛鐧婚檰
			  _result = "浣跨敤姝ゅ姛鑳介渶瑕佸厛鐧婚檰锛�;
		  }
		  
		  $('#addToFavoriteBtn').attr("data-content",_result).popover("toggle");
	  },
	  dataType: "text",
	  error:function(er){
		  console.log("addToFavorite.er="+er);
	  }
	});
}
//鍒拌揣閫氱煡
function emailNotifyProduct(obj){
	var _receiveEmail = $("#receiveEmail").val();
	if($.trim(_receiveEmail).length==0){
		$("#receiveEmail").focus();
		return;
	}
	
	var _url = "${basepath}/product/insertEmailNotifyProductService.html?receiveEmail="+_receiveEmail+"&productID="+$("#productID").val()+"&productName="+$("#productName").val();
	console.log("_url="+_url);
	$(obj).attr({"disabled":"disabled"});
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  success: function(data){
		  console.log("emailNotifyProduct.data="+data);
		  var _result = "鍒拌揣閫氱煡娣诲姞鎴愬姛锛�;
		  if(data=="0"){
		  }else if(data=='-1'){//鎻愮ず鐢ㄦ埛瑕佸厛鐧婚檰
			  _result = "浣跨敤姝ゅ姛鑳介渶瑕佸厛鐧婚檰锛�;
		  }
		  $("#emailNotifyProduct_input").hide();
		  $("#emailNotifyProductDiv").html(_result);
		  console.log(_result);
	  },
	  dataType: "text",
	  error:function(er){
		  console.log("emailNotifyProduct.er="+er);
		  $("#emailNotifyProductDiv").html("娣诲姞鍒拌揣閫氱煡澶辫触锛岃鑱旂郴绔欑偣绠＄悊鍛橈紒");
	  }
	});
}

//鏄剧ず绀煎搧璇︽儏
function showGiftDetail(){
	if($("#giftDetailDiv").is(':hidden')){
		$("#giftDetailDiv").slideDown(1000);		
	}else{
		$("#giftDetailDiv").slideUp(1000);
	}
}

</script>

<!-- baidu fenxiang -->
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"slide":{"type":"slide","bdImg":"0","bdPos":"right","bdTop":"100"}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
<!-- Baidu Button END -->

</@html.htmlBase>