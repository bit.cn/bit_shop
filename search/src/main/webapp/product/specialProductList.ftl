<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top.ftl" as shopTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/bootstrap.min.css" type="text/css">
	<@htmlBase.htmlBase>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/main/index.css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/base.css" type="text/css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/css.css" />
	<link rel="stylesheet" href="${systemSetting().staticSource}/resource/validator-0.7.0/jquery.validator.css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<style type="text/css">
				img {
				border: 0px;
			}
			
			.thumbnail_css {
				border-color: #f40;
			}
			
			.attr_css {
				font-size: 100%;
				float: left;
			}
			
			.left_product {
				font-size: 12px;
				max-height: 35px;
				overflow: hidden;
				text-overflow: ellipsis;
				-o-text-overflow: ellipsis;
			}
			
			.left_title {
				display: block;
				/* width: 280px; */
				overflow: hidden;
				white-space: nowrap;
				-o-text-overflow: ellipsis;
				text-overflow: ellipsis;
			}
			
			img.err-product {
				background: url(${systemSetting().defaultProductImg}) no-repeat 50% 50%;
				.lazy {
					display: none;
				}
				.centerImageCss {
					width: 289px;
					height: 190px;
				}
				.title {
					display: block;
					width: 280px;
					overflow: hidden;
					/*注意不要写在最后了*/
					white-space: nowrap;
					-o-text-overflow: ellipsis;
					text-overflow: ellipsis;
				}
				body {
					padding-top: 0px;
					padding-bottom: 0px;
					font-size: 12px;
					/*    	font-family: 微软雅黑, Verdana, sans-serif, 宋体; */
				}
				.hotSearch {
				cursor: pointer;
			}
		</style>
	<title>产品列表</title>
</head>
<body>
<!--商城主页top页-->
<@shopTop.shopTop/>
<div id="wrap">
	<div class="container">
		<div class="row">
			<div class="col-xs-3">
					<#include "/catalog_superMenu.ftl"/>
					</br>
					<#include "/product/productlist_left_picScroll.ftl"/>
			</div>
			
			<div class="col-xs-9">
				<!-- 导航栏 -->
				<div class="row">
				<#if (e.businessId!="")>
					<#assign map={"new":"最新登场","hot":"人气商品","spot":"现货商品","replenishment":"补款商品"}/>
				<#else>
					<#assign map={"hot":"人气商品","new":"最新登场","presale":"预售商品","spot":"现货商品"}/>
				</#if>
					<div>
						<span style="margin:5px;font-weight: bold;">类型：</span>
						<#list map?keys as key>
							<span class="label ${(code??&&key==code)?string("label-success","")}" style="margin-right:5px;font-size:100%;">
								<#if businessId??>
									<a href="${basepath}/special/${key}.html?businessId=${businessId}">${map[key]}</a>
								<#else>
									<a href="${basepath}/special/${key}.html">${map[key]}</a>
								</#if>
							</span>
						</#list>
					</div>
					<hr>
				</div>

				<#if productList1??>
				<div class="row">
					<#list productList1 as item>
						<div class="col-xs-3" style="padding: 5px;text-align: center;">
							<div class="thumbnail" style="width: 100%; display: block;">
								<div style="height: 150px;border: 0px solid;">
									<a href="${systemSetting().item}/product/${item.id!""}.html" target="_blank">
										
										<img class="lazy" style="border: 0px;display: block;margin: auto;max-height: 100%;max-width: 100;"  
										border="0" src="${systemSetting().imageRootPath!""}${item.picture!""}"
										data-original="${systemSetting().imageRootPath!""}${item.picture!""}">
										
									</a>
								</div>
								<div style="height: 40px;">
									<div class="col-xs-12 left_product">
										<div class="row">
											<a style="cursor: pointer;" href="${systemSetting().item}/product/${item.id!""}.html" target="_blank"
											title="${item.name!""}">
												${item.name!""}
											</a>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<b style="font-weight: bold;color: #cc0000;">
											￥${item.nowPrice!""}
										</b>
									</div>
									<div class="col-xs-12">
										<b style="text-decoration: line-through;font-weight: normal;font-size: 11px;color: #a5a5a5;">
											￥${item.price!""}
										</b>
									</div>
								</div>
								<div class="row" style="display: none;">
									<div class="col-xs-12">
										<#if item.isnew?? && item.isnew=="1">
											<div>1天前上架</div>
										<#else>
											<div>活动已结束!</div>
										</#if>
										<#if item.sale?? && item.sale=="1">
											<div>还剩余12小时结束</div>
										<#else>
											<div>活动已结束!</div>
										</#if>
									</div>
								</div>
							</div>
						</div>
					</#list>
				</div>
				<br style="clear: both;">
				<div style="text-align: right;">
					<#include "/pager.ftl"/>
				</div>
				<#else>
                <div class="row">
					没有找到<font color='red'>${key!""}</font>相关的宝贝!
                </div>
				</#if>
		</div>
		</div>
	</div>
	</div>
<script type="text/javascript">
$(function() {
	//商品鼠标移动效果
	$("div[class=thumbnail]").hover(function() {
		$(this).addClass("thumbnail_css");
	}, function() {
		$(this).removeClass("thumbnail_css");
	});
	$(".col-xs-3 img").addClass("carousel-inner img-responsive img-rounded");
});
</script>
<!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/hover.js"></script>
<script type="text/javascript">
		//搜索商品
		function search(){
			var _key = $.trim($("#mq").val());
			if(_key==''){
				return false;
			}
		$("#searchForm").submit();
		}		
		
		function domore(){ 
		$("#newType").show();   
		}
		$(function(){
		$("#moreType").on("click",function(){
		$("#newType").toggle();//显示隐藏切换
		})
		})
	</script>
</@htmlBase.htmlBase>
</body>
</html>