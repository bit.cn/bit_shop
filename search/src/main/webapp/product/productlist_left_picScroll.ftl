<!-- 产品列表左边产品图片滚动 -->
<style type="text/css">
						/* 本例子css */
						
						.picScroll-top {
							overflow: hidden;
							position: relative;
							border: 1px solid #ccc;
							min-height: 300px;
							width: 270px;
						}
						/* 		.picScroll-top *{margin:0; padding:0; list-style:none;} */
						
						.picScroll-top .hd {
							overflow: hidden;
							height: 30px;
							background: #f4f4f4;
							padding: 0 10px;
						}
						
						.picScroll-top .hd .prev,
						.picScroll-top .hd .next {
							display: block;
							width: 9px;
							height: 5px;
							float: right;
							margin-right: 5px;
							margin-top: 10px;
							overflow: hidden;
							cursor: pointer;
							background: url("${systemSetting().staticSource}/static/frontend/v1/images/jshop/images/icoUp.gif") no-repeat;
						}
						
						.picScroll-top .hd .next {
							background: url("${systemSetting().staticSource}/static/frontend/v1/images/jshop/images/icoDown.gif") no-repeat;
						}
						
						.picScroll-top .hd ul {
							float: right;
							overflow: hidden;
							zoom: 1;
							margin-top: 10px;
							zoom: 1;
						}
						
						.picScroll-top .hd ul li {
							float: left;
							width: 9px;
							height: 9px;
							overflow: hidden;
							margin-right: 5px;
							text-indent: -999px;
							cursor: pointer;
							background: url("${systemSetting().staticSource}/static/frontend/v1/images/jshop/images/icoCircle.gif") 0 -9px no-repeat;
						}
						
						.picScroll-top .hd ul li.on {
							background-position: 0 0;
						}
						
						.picScroll-top .bd {
							padding: 10px;
						}
						
						.picScroll-top .bd ul {
							overflow: hidden;
							zoom: 1;
						}
						
						.picScroll-top .bd ul li {
							text-align: center;
							zoom: 1;
						}
						
						.picScroll-top .bd ul li .pic {
							text-align: center;
							width: 100%;
						}
						
						.picScroll-top .bd ul li .pic img {
							max-width: 100%;
							max-height: 200px;
							display: block;
							padding: 0px;
							border: 0px solid #ccc;
						}
						
						.picScroll-top .bd ul li .pic a:hover img {
							border-color: #999;
						}
						
						.picScroll-top .bd ul li .title {
							line-height: 24px;
							text-align: left;
						}
						.left_title a {display: block; height: 25px; line-height: 25px;}
							.left_title a i{display: inline-block; float: right; margin-right:5px; width: 40px; height: 20px; line-height: 20px; color: #fff;background: #f60; margin-top: 2px;}
					</style>
		<div class="picScroll-top">
			<div class="hd"><b>热门推荐</b>
				<a class="next"></a>
				<ul></ul>
				<a class="prev"></a>
				<span class="pageState"></span>
			</div>
			<div class="bd">
				<ul class="picList">
				<#if businessRecommends??>
				 <#list businessRecommends as item>
							<li class="row col-xs-12">
								<div class="pic"><a href="${systemSetting().item}/product/${item.productId}" target="_blank">
									<img border="0" style="margin: auto;" src="${systemSetting().imageRootPath}/${item.picture!""}" /></a>
								</div>
								<div class="left_title" style="text-align: center;">
									<a href="http://www.SuperSlide2.com" target="_blank" style="margin: auto;text-align: center;" title="${item.name!""}">
									${item.name!""}
									</a>
								</div>
								<div class="left_title" style="text-align: center;">
									<b style="font-weight: bold;color: #cc0000;">
										${item.nowPrice!"0.00"}
									</b>
								</div>
							</li>
						</#list>
					</#if>
				</ul>
			</div>
		</div>

