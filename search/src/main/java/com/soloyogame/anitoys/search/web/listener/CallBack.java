package com.soloyogame.anitoys.search.web.listener;

public interface CallBack {
	String callback() throws Exception;
}
