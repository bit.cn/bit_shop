package com.soloyogame.anitoys.search.web;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.search.web.util.LoginUserHolder;
import com.soloyogame.anitoys.search.web.util.RequestHolder;
import com.soloyogame.anitoys.service.AccountService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;

/**
 * @author shaojian
 * @param <E>
 */
@Controller
public abstract class FrontBaseController<E extends PagerModel> 
{
    protected Logger logger = LoggerFactory.getLogger(getClass());
    public abstract Services<E> getService();
    protected static final String page_toLogin = "/account/login.html";
    protected static final String page_toLoginRedirect = "redirect:/account/login.html";


    @Autowired
	private RedisCacheProvider redisCacheProvider;
    @Autowired
	private AccountService accountService;

    protected Account getLoginAccount()
    {
        return LoginUserHolder.getLoginAccount();
    }

    /**
     * 查询分页信息列表
     */
    protected <X extends PagerModel> PagerModel selectPageList(Services<X> service, X model) throws Exception 
    {
        int offset = 0;//分页偏移量
        if (RequestHolder.getRequest().getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(RequestHolder.getRequest().getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        model.setOffset(offset);
        PagerModel pager = service.selectPageList(model);
        if (pager == null) 
        {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)
                / pager.getPageSize());
        return pager;
    }
    
    

}
