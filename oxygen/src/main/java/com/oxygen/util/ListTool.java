package com.oxygen.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * list工具类
 * @author jason
 *
 */
public class ListTool {
	/**
	 * @see   对List<Map>进行排序
	 * @param orderKey
	 * @return
	 */
	public static List orderMapList(List maplist,String orderKey){
		MapComparator Comparator=new MapComparator();
		Comparator.setKey(orderKey);
		Collections.sort(maplist,Comparator);
		return maplist;

    }
	/**
	 * @see   对List<Map>进行倒序排序
	 * @param orderKey
	 * @return
	 */
	public static List orderMapListDesc(List maplist,String orderKey){
		MapComparator Comparator=new MapComparator();
		Comparator.setKey(orderKey);
		Collections.sort(maplist,Comparator);
		Collections.reverse(maplist);
		return maplist;

    }
	/**
	 * @see   对List<Object>进行排序,要求Object为pojo对象
	 * @param orderKey
	 * @return
	 */
	public static List orderObjList(List list,String orderKey){
		ObjComparator Comparator=new ObjComparator();
		Comparator.setKey(orderKey);
		Collections.sort(list,Comparator);
		return list;

    }
	/**
	 * @see   对List<Object>进行倒序排序,要求Object为pojo对象
	 * @param orderKey
	 * @return
	 */
	public static List orderObjListDesc(List list,String orderKey){
		ObjComparator Comparator=new ObjComparator();
		Comparator.setKey(orderKey);
		Collections.sort(list,Comparator);
		Collections.reverse(list);
		return list;

    }

	public static List arrayToList(Object[] array){
		List list=new ArrayList();
		if(array!=null)
		for(int i=0;i<array.length;i++){
			list.add(array[i]);
		}
		return list;

    }
	public static Object[] listToArray(List list){
		Object[] objs=list.toArray();
		
		return objs;

    }

	public static boolean isInList(Object obj,List list){
		boolean result=false;
		for(Object object:list){
			if(obj.equals(object)){
				result=true;
				break;
			}
		}
		return result;
	}
	public static boolean isInList(Object obj,Object[] list){
		boolean result=false;
		for(Object object:list){
			if(obj.equals(object)){
				result=true;
				break;
			}
		}
		return result;
	}
	/**
	 * 将list 顺序打乱
	 * @param list
	 */
	public static void randomList(List list){
		Collections.shuffle(list);
	}
	
	/**
	 * 判断列表是否为null或长度为0
	 * @param list
	 * @return
	 */
	public static boolean isEmpty(List list){
		boolean result=false;
		if(list==null){
			result=true;
		}
		else if(list.isEmpty()){
			result=true;
		}
		return result;
	}
	
	public static void clear(List list){
		if(list!=null){
			list.clear();
		}
	}
	
	/**
	 * 判断list不为空
	 * @param list
	 * @return
	 */
	public static boolean isNotNull(List list){
		boolean flag = false;
		if(list != null && list.size() > 0){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * 判断list为空
	 * @param list
	 * @return
	 */
	public static boolean isNull(List list){
		boolean flag = false;
		if(list == null || list.size() == 0){
			flag = true;
		}
		return flag;
	}
}
