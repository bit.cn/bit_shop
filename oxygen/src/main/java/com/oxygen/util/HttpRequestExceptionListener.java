package com.oxygen.util;

import java.util.Map;

public interface HttpRequestExceptionListener {
	void exceptionData(Map<String, String> data);
}
