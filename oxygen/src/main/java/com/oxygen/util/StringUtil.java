package com.oxygen.util;

import org.springframework.util.StringUtils;

/**
 * 字符串处理工具类<br/>
 * 
 * @author liko
 * 注：<br/>
 * 	1.继承 org.springframework.util.StringUtils
 */
public class StringUtil extends StringUtils {

	public static String nullToEmpty(String str) {
		if (str == null || "null".equalsIgnoreCase(str)) {
			str = "";
		}
		return str;
	}
	

}
