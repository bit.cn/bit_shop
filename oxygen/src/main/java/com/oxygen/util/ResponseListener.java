package com.oxygen.util;

import java.util.Map;

public interface ResponseListener {
	void responseData(Map<String, String> data);
}
