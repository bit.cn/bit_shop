package com.oxygen.util;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oxygen.dto.IdNameDto;

/**
 * 实现json字符串与java对象之间的互相转换等功能
 * 
 * @author daniel
 * 
 */
public class JsonUtil {
	public static void main(String[] args) {
		IdNameDto o = new IdNameDto();
		o.setId(1);
		o.setName("1");
		IdNameDto o1 = new IdNameDto();
		o1.setId(1);
		o1.setName("1");
		Map<String, IdNameDto> map = new HashMap<String, IdNameDto>();
		map.put("A", o);
		map.put("B", o1);
		String json = JsonUtil.objectToJson(map);
		System.out.println(json);
		// o=(IdNameDto)JsonUtil.getJsonToObject(json, HashMap.class, "A");
		System.out.println(o);
	}

	/**
	 * 将json字符串转换成java对象（支持泛型）
	 * 
	 * @param json
	 *            json字符串
	 * @param type
	 *            对象类型
	 * @return
	 */

	public static <T> T jsonToObject(String json, Type type) {
		T obj = null;
		try {
			obj = new Gson().fromJson(json, type);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}

	/**
	 * 将java对象转换成json字符串
	 * 
	 * @param obj
	 *            对象
	 * @return
	 */
	public static String objectToJson(Object obj) {
		return objectToJson(obj, null);
	}

	/**
	 * 将java对象转换成json字符串（支持泛型）
	 * 
	 * @param obj
	 *            对象
	 * @param type
	 *            对象类型
	 * @return
	 */
	public static String objectToJson(Object obj, Type type) {
		return objectToJson(obj, type, null);
	}

	public static String objectToJson(Object obj, Type type, String dateFormat) {
		String json = null;
		try {
			GsonBuilder gb = new GsonBuilder().serializeNulls();
			if (dateFormat != null && !"".equals(dateFormat)) {
				gb.setDateFormat(dateFormat);
			}
			if (type != null) {
				json = gb.create().toJson(obj, type);
			} else {
				json = gb.create().toJson(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}

}
