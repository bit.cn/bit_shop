package com.oxygen.util;

import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;
import com.oxygen.dto.IpInfo;

public class IpUtil {
	private static final String SINA_URL = "http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=";
	private static final String TAOBAO_URL = "http://ip.taobao.com/service/getIpInfo.php?ip=";

	public static IpInfo getIpOfBelongingTo(String ip) {
		String json = getIpOfBelongingToJson(ip);
		Type type = new TypeToken<IpInfo>() {
		}.getType();
		IpInfo info = JsonUtil.jsonToObject(json, type);
		return info;
	}

	public static String getIpOfBelongingToJson(String ip) {
		HttpClientUtil HttpClientUtil = new HttpClientUtil();
		String json = HttpClientUtil.doHttpGet(TAOBAO_URL + ip, null);
		return json;
	}

	public static void main(String[] args) throws Exception {

		IpInfo retJson = IpUtil.getIpOfBelongingTo("119.62.64.170");
		System.out.print(retJson);
	}
}
