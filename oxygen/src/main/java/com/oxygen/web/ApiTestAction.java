package com.oxygen.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.oxygen.constants.Constant;
import com.oxygen.dto.ApiParamDto;
import com.oxygen.enums.ApiMsgEnum;
import com.oxygen.util.HttpClientUtil;
import com.oxygen.util.JsonUtil;
import com.oxygen.util.SimpleTokenUtil;

@Controller
@RequestMapping(value = "/apitest")
public class ApiTestAction {
	@RequestMapping(value = { "", "/" })
	public String apitest(HttpServletRequest req, ModelMap modelMap) {
		modelMap.addAttribute("functionCodeCatalogMap", SpringBeanProxy.getFunctionCodeCatalogMap());
		req.setAttribute("apiMsgMap", ApiMsgEnum.getAll());
		return "/apitest";
	}

	/**
	 * 根据类目获取API列表
	 * 
	 * @param catalog
	 * @param rsp
	 * @author Daniel
	 */
	@RequestMapping(value = "/getFunctionListByCatalog")
	public void getFunctionListByCatalog(@RequestParam String catalog, HttpServletResponse rsp) {
		Map<String, String> paramMap = SpringBeanProxy.getFunctionListByCatalog(catalog);
		String json = JsonUtil.objectToJson(paramMap);
		WebHelper.outputJson(json, rsp);
	}

	/**
	 * 根据API CODE获取参数列表
	 * 
	 * @param functionCode
	 * @param rsp
	 * @author Daniel
	 */
	@RequestMapping(value = "/getParamsByFunctionCode")
	public void getParamsByFunctionCode(@RequestParam String functionCode, HttpServletResponse rsp) {
		Map<String, ApiParamDto> paramMap = SpringBeanProxy.getParamsByFunctionCode(functionCode);
		String json = JsonUtil.objectToJson(paramMap);
		WebHelper.outputJson(json, rsp);
	}

	@RequestMapping(value = "/doApiTest")
	public String doApiTest(HttpServletRequest req, HttpServletResponse rsp) {
		String apiKey = req.getParameter("apiKey");
		String apiSecret = req.getParameter("apiSecret");
		String functioncode = req.getParameter("functionCode");
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		String[] paramNameArr = req.getParameterValues("paramName");
		String[] paramValueArr = req.getParameterValues("paramValue");
		if (paramNameArr != null && paramNameArr.length > 0) {
			int n = 0;
			for (String paramName : paramNameArr) {
				if (paramName != null && !"".equals(paramName.trim())) {
					paramsMap.put(paramName, paramValueArr[n]);
				}
				n++;
			}
		}
		paramsMap.put(Constant.API_KEY, apiKey);
		String token = SimpleTokenUtil.buildToken(paramsMap, functioncode, apiSecret);
		paramsMap.put(Constant.ACCESS_TOKEN, token);

		Map<String, Object> functionCodeMap = new HashMap<String, Object>();
		functionCodeMap.put(functioncode, paramsMap);
		// paramsMap.put("functionCode", functioncode);
		// String key = makeNginxKey(paramsMap);

		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put(Constant.DATA, JsonUtil.objectToJson(functionCodeMap, Map.class));
		HttpClientUtil clientUtil = new HttpClientUtil();
		String defaultApiServer = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/";
		if (req.getServerPort() == 80) {
			defaultApiServer = req.getScheme() + "://" + req.getServerName() + req.getContextPath() + "/";
		}
		String retJson = clientUtil.doHttpPost(defaultApiServer, dataMap);
		return WebHelper.outputJson(retJson, rsp);
	}

	@RequestMapping(value = "/doApiToken")
	public String doApiToken(HttpServletRequest req, HttpServletResponse rsp) {
		String apiKey = req.getParameter("apiKey");
		String apiSecret = req.getParameter("apiSecret");
		String functioncode = req.getParameter("functionCode");
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		String[] paramNameArr = req.getParameterValues("paramName");
		String[] paramValueArr = req.getParameterValues("paramValue");
		if (paramNameArr != null && paramNameArr.length > 0) {
			int n = 0;
			for (String paramName : paramNameArr) {
				if (paramName != null && !"".equals(paramName.trim())) {
					paramsMap.put(paramName, paramValueArr[n]);
				}
				n++;
			}
		}
		paramsMap.put(Constant.API_KEY, apiKey);
		String token = SimpleTokenUtil.buildToken(paramsMap, functioncode, apiSecret);
		paramsMap.put("token", token);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put(functioncode, paramsMap);
		return WebHelper.outputJson(JsonUtil.objectToJson(data), rsp);
	}
}
