package com.oxygen.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.util.WebUtils;

import com.oxygen.util.JsonUtil;

public class WebHelper {
	protected static final String MSG = "msg";
	protected static final String IS_SUCCESS = "isSuccess";
	private static final Logger loger = Logger.getLogger(WebHelper.class);

	public static void sendRedirect(HttpServletResponse response, String url) throws IOException {
		response.sendRedirect(response.encodeRedirectURL(url));
	}

	public static String getCurrRequestUrl(HttpServletRequest request) throws UnsupportedEncodingException {
		StringBuffer currRequestUrl = request.getRequestURL();
		Enumeration<String> parameterNames = request.getParameterNames();
		boolean flag = true;
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			String value = request.getParameter(paramName);
			if (flag) {
				// currRequestUrl.append("?" + paramName + "=" + value);
				currRequestUrl.append("?" + paramName + "=" + URLEncoder.encode(value, "UTF-8"));
				flag = false;
			} else {
				// currRequestUrl.append("&" + paramName + "=" + value);
				currRequestUrl.append("&" + paramName + "=" + URLEncoder.encode(value, "UTF-8"));
				flag = false;
			}
		}
		return currRequestUrl.toString();
	}

	/**
	 * 移除cookie
	 * 
	 * @param request
	 * @param response
	 * @param cookieName
	 */
	public static void removeCookie(HttpServletRequest request, HttpServletResponse response, String cookieName, String domain) {
		Cookie cookie = WebUtils.getCookie(request, cookieName);
		if (cookie != null) {
			cookie.setMaxAge(0);
			if (domain != null && !"".equals(domain.trim())) {
				cookie.setDomain(domain);
			}
			cookie.setPath("/");
			response.addCookie(cookie);
		}
	}

	/**
	 * 获取当前操作人IP
	 * 
	 * @param request
	 * @return
	 * @author Daniel
	 */
	public static String getRequestIp(HttpServletRequest request) {
		// return request.getRemoteAddr();
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 是否ajax请求
	 * 
	 * @param request
	 * @return
	 * @author Daniel
	 */
	public static boolean isAjaxRequest(HttpServletRequest request) {
		if ((request.getHeader("accept").indexOf("application/json") > -1) || (request.getHeader("X-Requested-With") != null && request.getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1)) {
			return true;
		}
		return false;
	}

	/**
	 * 输出JSON
	 * 
	 * @param json
	 * @param response
	 * @return
	 * @author daniel
	 */
	public static String outputJson(String json, HttpServletResponse response) {
		return output(json, response, "text/html;charset=UTF-8");
	}

	public static String outputText(String text, HttpServletResponse response) {
		return output(text, response, "text/html;charset=UTF-8");
	}

	public static String outputXml(String xml, HttpServletResponse response) {
		return output(xml, response, "text/xml;charset=UTF-8");
	}

	public static String output(String str, HttpServletResponse response, String contentType) {
		response.reset();
		response.setCharacterEncoding("UTF-8");
		response.setContentType(contentType);
		try {
			PrintWriter out = response.getWriter();
			out.print(str);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void outputExcel(File file, HttpServletResponse rsp, String excelFileName) {
		try {
			rsp.setContentType("application/vnd.ms-excel");
			rsp.setCharacterEncoding("UTF-8");
			rsp.addHeader("Content-disposition", "attachment;filename=\"" + new String((excelFileName + "_" + file.getName()).getBytes("UTF-8"), "ISO8859_1") + "\"");
			ServletOutputStream os = rsp.getOutputStream();
			os.write(FileUtils.readFileToByteArray(file));
			os.flush();
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 输出消息
	 * 
	 * @param isSuccess
	 * @param msg
	 * @param response
	 * @return
	 * @author daniel
	 */
	public static String outputMsg(boolean isSuccess, String msg, HttpServletResponse response) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		retMap.put(IS_SUCCESS, isSuccess);
		retMap.put(MSG, msg);
		return outputMsg(retMap, response);
	}

	/**
	 * 输出消息
	 * 
	 * @param retMap
	 * @param response
	 * @return
	 * @author daniel
	 */
	public static String outputMsg(Map<String, Object> retMap, HttpServletResponse response) {
		String json = JsonUtil.objectToJson(retMap);
		return outputJson(json, response);
	}

	/**
	 * 适用于jsonp方式调用返回
	 * 
	 * @param jsonpCallback
	 * @param isSuccess
	 * @param msg
	 * @param response
	 * @author daniel
	 */
	public static String outputForJsonp(String jsonpCallback, boolean isSuccess, String msg, HttpServletResponse response) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		retMap.put(IS_SUCCESS, isSuccess);
		retMap.put(MSG, msg);
		return outputForJsonp(jsonpCallback, retMap, response);
	}

	public static String outputForJsonp(String jsonpCallback, Map<String, Object> retMap, HttpServletResponse response) {
		String json = JsonUtil.objectToJson(retMap);
		return outputForJsonp(jsonpCallback, json, response);
	}

	public static String outputForJsonp(String jsonpCallback, String json, HttpServletResponse response) {
		return outputJson(jsonpCallback + "(" + json + ")", response);
	}

	public static Map<String, Object> buildParamMapFromRequest(HttpServletRequest request) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Enumeration<String> e = request.getParameterNames();
		while (e.hasMoreElements()) {
			String name = e.nextElement();
			String value = request.getParameter(name);
			paramMap.put(name, value);
		}
		return paramMap;
	}

	public static void setRequestAttributesFromRequestParam(HttpServletRequest request) {
		Enumeration<String> e = request.getParameterNames();
		while (e.hasMoreElements()) {
			String name = e.nextElement();
			String value = request.getParameter(name);
			request.setAttribute(name, value);
		}
	}

	@SuppressWarnings("rawtypes")
	public static String extractRequestHeader(HttpServletRequest req, String headerName) {
		Enumeration aHeader = req.getHeaders(headerName);
		if (aHeader != null) {
			while (aHeader.hasMoreElements()) {
				Object obj = aHeader.nextElement();
				if (!StringUtils.isEmpty(obj)) {
					return obj.toString();
				}
			}
		}
		return null;
	}

	public static String getRequestUrl(HttpServletRequest req) {
		String rquestUrl = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getRequestURI();
		if (req.getServerPort() == 80) {
			rquestUrl = req.getScheme() + "://" + req.getServerName() + req.getRequestURI();
		}
		return rquestUrl;
	}

	public static String getHttpBody(HttpServletRequest req) {
		try {
			InputStream inputStream = req.getInputStream();
			BufferedReader buff = new BufferedReader(new InputStreamReader(inputStream));
			String inputLine = null;
			StringBuilder buf = new StringBuilder();
			while ((inputLine = buff.readLine()) != null) {
				buf.append(inputLine);
			}
			buff.close();
			return buf.toString();
		} catch (IOException e) {
			loger.error("getHttpBody", e);
		}
		return null;
	}
}
