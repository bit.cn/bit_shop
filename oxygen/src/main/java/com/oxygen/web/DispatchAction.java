package com.oxygen.web;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.oxygen.annotations.ApiMethod;
import com.oxygen.annotations.ApiParam;
import com.oxygen.constants.Constant;
import com.oxygen.dto.ApiFinalResponse;
import com.oxygen.dto.ApiKeyDto;
import com.oxygen.dto.ApiRequest;
import com.oxygen.dto.ApiResponse;
import com.oxygen.enums.ApiMethodEnum;
import com.oxygen.enums.ApiMsgEnum;
import com.oxygen.util.JsonUtil;
import com.oxygen.util.SimpleTokenUtil;
import com.oxygen.util.StringUtil;

/**
 * API分发处理器<br>
 * 
 * 思路：<br>
 * 
 * 1.首先定义一个注解，如FunctionCode，value就是约定的user.login这种；<br>
 * 2.将这个注解应用于相应业务的service以及service方法上; <br>
 * 3.当请求进入时，拦截请求，封装请求参数至ApiRequest，并分发到相应的service上；<br>
 * 4.业务处理完成后，由总控制的地方，统一输出响应数据，如ApiResponse。<br>
 * 
 * 调用service之前还可以加一些验证beforeInvokeService等等，安全方面的考虑，比如token<br>
 * 
 * 调用service之后也可以做一些日志记录afterInvokeService<br>
 * 
 * @author clj
 */
@SuppressWarnings("rawtypes")
public abstract class DispatchAction {
	private final Log loger = LogFactory.getLog(DispatchAction.class);

	@Value("${api.version}")
	private String apiVersion;

	private static final Map<String, ApiKeyDto> apiKeyMap = new HashMap<String, ApiKeyDto>();
	private static final Map<Long, ApiRequest> currentReqeust = new HashMap<Long, ApiRequest>();
	private static final String defaultApiKey = "android_client";

	static {
		apiKeyMap.put("android_client", new ApiKeyDto("android_client", "5b2e1c483b4cf67c87399e1de4554cf9", "android_client"));
		apiKeyMap.put("weshare_web", new ApiKeyDto("weshare_web", "c111801f8af3dad7f209f22a045175ee", "weshare_web"));
		apiKeyMap.put("weshare_backend", new ApiKeyDto("backend", "6c1e1c12d70fc08c823d90e791a46e0c", "backend"));
		apiKeyMap.put("ios_client", new ApiKeyDto("ios_client", "8cb800c5eb187992fe1e3c1b900b24ec", "ios_client"));
		apiKeyMap.put("task", new ApiKeyDto("task", "91eefcbe8e4f8b876658d724e76fc25e", "task"));
		apiKeyMap.put("order", new ApiKeyDto("order", "91eefcbe8e4f8b876658d724e76fc25e", "order"));
		apiKeyMap.put("user", new ApiKeyDto("user", "8cb800c5eb187992fe1e3c1b900b24ew", "user"));
		apiKeyMap.put("trusteeship", new ApiKeyDto("trusteeship", "91eefcbe8e4f8b527658d724e76fc25e", "trusteeship"));
		apiKeyMap.put("activity", new ApiKeyDto("activity", "8cb800c5eb187992fe7e3c1b900byuhj", "activity"));
		apiKeyMap.put("m_client", new ApiKeyDto("m_client", "5b2e6c483b8cf67c87399e1de4554cf9", "m_client"));
		apiKeyMap.put("product", new ApiKeyDto("product", "5b2e6c483b8cf67c87399e1de4554cf9", "product"));

	}

	/**
	 * 请求控制分发
	 * 
	 * @param req
	 * @param rsp
	 * @return
	 */
	protected final String doDispatch(ApiRequest apiReq) {
		String retJson = null;
		String functionCode = null;
		try {
			ApiResponse apiRsp = this.checkParam(apiReq);
			if (apiRsp != null) {
				return this.buildApiFinalResponse(apiReq, apiRsp);
			}
			functionCode = apiReq.getFunctionCode();
			// String apiVersion=apiReq.getApiVersion();
			// 此处可以把调用接口的IP,时间记录下来
			this.beforeInvokeService(apiReq);
			Object bean = SpringBeanProxy.getBeanByFunctionCode(functionCode);
			Method method = SpringBeanProxy.getMethodByFunctionCode(functionCode);
			try {
				Object rspObj = method.invoke(bean, new Object[] { apiReq });
				if (rspObj instanceof String) {
					retJson = (String) rspObj;
					this.afterInvokeService(apiReq, retJson);
				}
			} catch (Exception e) {
				retJson = this.buildApiFinalResponse(apiReq, new ApiResponse(ApiMsgEnum.INVOKE_ERROR));
				loger.error("logId:" + SuperDispatcherServlet.getLogIdByThreadId(Thread.currentThread().getId()) + ",method.invoke exception in " + functionCode, e);
			}
		} catch (Exception e) {
			loger.error("logId:" + SuperDispatcherServlet.getLogIdByThreadId(Thread.currentThread().getId()) + ",apiDispatche exception for " + functionCode, e);
		}
		return retJson;
	}

	@SuppressWarnings("unchecked")
	protected String buildApiFinalResponse(ApiRequest apiReq, ApiResponse apiRsp) {
		// 统一输出响应JSON
		ApiFinalResponse finalRsp = new ApiFinalResponse();
		finalRsp.setVersion(apiVersion);
		finalRsp.setFunctionCode(apiReq.getFunctionCode());
		finalRsp.setIsSuccess(apiRsp.getReturnEnum().getIsSuccess());
		finalRsp.setCode(apiRsp.getReturnEnum().getCode());
		finalRsp.setMsg(apiRsp.getReturnEnum().getMsg());
		finalRsp.setCount(apiRsp.getCount());
		finalRsp.setResults(apiRsp.getResults());
		return JsonUtil.objectToJson(finalRsp);
	}

	/**
	 * 获取请求参数
	 * 
	 * @param req
	 * @param apiReq
	 */
	protected List<ApiRequest> buildApiRequest(String data, HttpServletRequest req) {
		List<ApiRequest> apiReqList = new ArrayList<ApiRequest>();
		JsonElement dataJsonEle = JsonUtil.jsonToObject(data, JsonElement.class);
		if (dataJsonEle != null && dataJsonEle.isJsonObject()) {
			JsonObject functionCodeJsonObj = dataJsonEle.getAsJsonObject();
			for (Entry<String, JsonElement> functionCodeEntry : functionCodeJsonObj.entrySet()) {
				ApiRequest apiReq = new ApiRequest();
				apiReq.setApiVersion(this.apiVersion);
				String functionCode = functionCodeEntry.getKey();
				apiReq.setFunctionCode(functionCode);
				ApiMethodEnum aEnum = ApiMethodEnum.getApiMethodEnum(functionCode);
				if (aEnum != null) {
					apiReq.setContentTypeEnum(aEnum.getContentType());
				}
				JsonElement paramJsonEle = functionCodeEntry.getValue();
				if (paramJsonEle != null && paramJsonEle.isJsonObject()) {
					JsonObject paramJsonObj = paramJsonEle.getAsJsonObject();
					for (Entry<String, JsonElement> paramEntry : paramJsonObj.entrySet()) {
						String paramName = paramEntry.getKey();
						JsonElement paramValueEle = paramEntry.getValue();
						String paramValue = null;
						if (paramValueEle != null && !paramValueEle.isJsonNull() && paramValueEle.isJsonPrimitive()) {
							paramValue = paramValueEle.getAsString();
						}
						if (Constant.API_KEY.equals(paramName)) {
							apiReq.setApiKey(paramValue);
						} else if (Constant.ACCESS_TOKEN.equals(paramName)) {
							apiReq.setAccessToken(paramValue);
						} else if (Constant.JUST_NEED_RESULT.equals(paramName)) {
							apiReq.setJustNeedResult(paramValueEle.getAsBoolean());
						} else if (Constant.OLD_VERSION.equals(paramName)) {
							apiReq.setOldVersion(paramValueEle.getAsBoolean());
						} else if (Constant.REQUEST_IP.equals(paramName)) {
							apiReq.setRequestIp(paramValue);
						} else if (Constant.USER_AGENT.equals(paramName)) {
							apiReq.setUserAgent(paramValue);
						} else if (Constant.DEVICE_NO.equals(paramName)) {
							apiReq.setDeviceNo(paramValue);
						} else if (Constant.MODEL_NAME.equals(paramName)) {
							apiReq.setModelName(paramValue);
						} else if (Constant.PAGE.equals(paramName) && !StringUtil.isEmpty(paramValue)) {
							apiReq.put(paramName, Integer.valueOf(paramValue));
						} else if (Constant.PAGE_SIZE.equals(paramName) && !StringUtil.isEmpty(paramValue)) {
							apiReq.put(paramName, Integer.valueOf(paramValue));
						} else {
							if (paramValueEle == null || paramValueEle.isJsonNull() || paramValueEle.isJsonPrimitive()) {
								apiReq.put(paramName, paramValue);
							} else if (paramValueEle.isJsonObject()) {
								apiReq.put(paramName, paramValueEle.getAsJsonObject());
							} else if (paramValueEle.isJsonArray()) {
								apiReq.put(paramName, paramValueEle.getAsJsonArray());
							}
						}
					}
				}

				if (StringUtils.isEmpty(apiReq.getApiKey())) {
					apiReq.setApiKey(defaultApiKey);
				} else {
					ApiKeyDto keyDto = apiKeyMap.get(apiReq.getApiKey());
					if (keyDto != null) {
						apiReq.setApiSource(keyDto.getApiSource());
						apiReq.put(Constant.API_KEY, apiReq.getApiKey());
					}
				}
				if (StringUtils.isEmpty(apiReq.getRequestIp())) {
					apiReq.setRequestIp(WebHelper.getRequestIp(req));
				}
				if (StringUtils.isEmpty(apiReq.getUserAgent())) {
					String agent = req.getHeader("User-Agent");
					apiReq.setUserAgent(agent);
				}
				apiReqList.add(apiReq);
			}
		}
		return apiReqList;
	}

	/**
	 * 组装一个json项
	 * 
	 * @param itmeName
	 * @param json
	 * @return
	 */
	protected String buildJsonItem(String itmeName, String json) {
		StringBuffer buf = new StringBuffer();
		buf.append("\"" + itmeName + "\"").append(":").append(json);
		return buf.toString();

	}

	/**
	 * 验证参数
	 * 
	 * @param req
	 * @return
	 * @author clj
	 */

	protected ApiResponse checkParam(ApiRequest apiReq) {
		String apiKey = apiReq.getApiKey();
		if (StringUtils.isEmpty(apiKey)) {
			loger.info("checkParam apiKey is null");
			return new ApiResponse(ApiMsgEnum.ForbiddenException);
		}
		String functionCode = apiReq.getFunctionCode();
		if (StringUtils.isEmpty(functionCode)) {
			loger.info("checkParam functionCode is null");
			return new ApiResponse(ApiMsgEnum.ForbiddenException);
		}
		ApiMethod apiMethod = SpringBeanProxy.getApiMethodByFunctionCode(functionCode);
		if (apiMethod == null) {
			loger.info("checkParam not found apiMethod in ApiMethod ");
			return new ApiResponse(ApiMsgEnum.ForbiddenException);
		}
		String inputToken = apiReq.getAccessToken();
		if (StringUtils.isEmpty(inputToken)) {
			loger.info("checkParam inputToken is null ");
			return new ApiResponse(ApiMsgEnum.ForbiddenException);
		}
		if (!Constant.DEFAULT_ACCESS_TOKEN.equals(inputToken)) {
			ApiKeyDto keyDto = apiKeyMap.get(apiReq.getApiKey());
			if (keyDto == null) {
				loger.info("checkParam not found apiKey in ApiKeyDto ");
				return new ApiResponse(ApiMsgEnum.ForbiddenException);
			}
			String rightToken = SimpleTokenUtil.buildToken(apiReq, functionCode, keyDto.getApiSecret());
			if (!inputToken.equals(rightToken)) {
				return new ApiResponse(ApiMsgEnum.TOKEN_ERROR);
			}
		}
		if (apiMethod != null && apiMethod.needLogin()) {
			if (StringUtils.isEmpty(apiReq.get(Constant.AUTH_TOKEN))) {
				return new ApiResponse(ApiMsgEnum.UserUnloginException);
			}
		}
		ApiParam[] apiParams = apiMethod.apiParams();
		if (apiParams != null) {
			for (ApiParam parm : apiParams) {
				if (!parm.isNull()) {
					if (StringUtil.isEmpty(apiReq.get(parm.name()))) {
						return new ApiResponse(ApiMsgEnum.MissParameterException);
					}
				}
			}
		}
		return null;
	}

	/**
	 * 调用service之前
	 * 
	 * @param apiReq
	 * @author clj
	 */
	protected void beforeInvokeService(ApiRequest apiReq) {
		currentReqeust.put(Thread.currentThread().getId(), apiReq);
	}

	/**
	 * 调用service之后
	 * 
	 * @param apiReq
	 * @param rspObj
	 * @author clj
	 */
	protected void afterInvokeService(ApiRequest apiReq, String resp) {
		currentReqeust.remove(Thread.currentThread().getId());
	}

	/**
	 * 获取当前线程的AipRequest
	 * 
	 * @return
	 */
	public static ApiRequest getCurrentReqeust() {
		return currentReqeust.get(Thread.currentThread().getId());
	}
}
