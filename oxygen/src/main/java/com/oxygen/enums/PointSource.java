package com.oxygen.enums;


import java.util.LinkedHashMap;
import java.util.Map;


/**
 * 0-REGISTER 注册, 1-INVITE 邀请好友, 2-BINDMOBILE绑手机, 3-SUBSCRIBE关注, 4-ORDER订单, 5-SUBMISSION游记投稿, 
 * 6-CHECKIN签到, 7-SHARE分享, 8-QUESTION问卷, 9-转盘送积分, 10-RICHINFO完善個人信息 11-扣减积分加体力
 * @author yzz
 */
public enum PointSource {

	REGISTER("REGISTER", "0"),
	INVITE("INVITE", "1"),
	BINDMOBILE("BINDMOBILE", "2"),
	SUBSCRIBE("SUBSCRIBE", "3"),
	ORDER("ORDER", "4"),
	SUBMISSION("SUBMISSION", "5"),
	CHECKIN("CHECKIN", "6"),
	SHARE("SHARE", "7"),
	QUESTION("QUESTION", "8"),
	TURNTABLE("TURNTABLE", "9"),
	RICHINFO("RICHINFO", "10"),
	POINT_MINUS("POINT_MINUS", "11")
	;
	
	PointSource(String code, String value) {
		this.code = code;
		this.value = value;
	}

	String code;
	String value;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static Map<String, Object> getMap() {
		Map<String, Object> retMap = new LinkedHashMap<String, Object>();
		PointSource[] enumArr = PointSource.values();
		for (PointSource aEnum : enumArr) {
			retMap.put(aEnum.code, aEnum.getValue());
		}
		return retMap;
	}
}
