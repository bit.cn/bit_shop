package com.oxygen.enums;

public enum ContentTypeEnum {
	/**
	 * permission
	 */
	PERMISSION(1),

	/**
	 * product
	 */
	PRODUCT(2),

	/**
	 * user
	 */
	USER(3),

	/**
	 * content type
	 */
	CONTENTTYPE(4),

	/**
	 * session
	 */
	SESSION(5),

	/**
	 * site
	 */
	SITE(6),

	/**
	 * migration history
	 */
	MIGRATIONHISTORY(7),

	/**
	 * log entry
	 */
	LOGENTRY(8),

	/**
	 * tag
	 */
	TAG(9),

	/**
	 * tagged item
	 */
	TAGGEDITEM(10),

	/**
	 * source
	 */
	SOURCE(11),

	/**
	 * thumbnail
	 */
	THUMBNAIL(12),

	/**
	 * revision
	 */
	REVISION(13),

	/**
	 * version
	 */
	VERSION(14),

	/**
	 * Author
	 */
	AUTHOR(15),

	/**
	 * Package
	 */
	PACKAGE(16),

	/**
	 * Package Version
	 */
	PACKAGEVERSION(17),

	/**
	 * package version screenshot
	 */
	PACKAGEVERSIONSCREENSHOT(18),

	/**
	 * category
	 */
	CATEGORY(19),

	/**
	 * topic
	 */
	TOPIC(20),

	/**
	 * topical item
	 */
	TOPICALITEM(21),

	/**
	 * tips word
	 */
	TIPSWORD(22),

	/**
	 * place
	 */
	PLACE(23),

	/**
	 * advertisement
	 */
	ADVERTISEMENT(24),

	/**
	 * advertisement_ places
	 */
	ADVERTISEMENT_PLACES(25),

	/**
	 * userena registration
	 */
	USERENASIGNUP(26),

	/**
	 * token
	 */
	TOKEN(27),

	/**
	 * profile
	 */
	PROFILE(28),

	/**
	 * player
	 */
	PLAYER(29),

	/**
	 * user object permission
	 */
	USEROBJECTPERMISSION(30),

	/**
	 * group object permission
	 */
	GROUPOBJECTPERMISSION(31),

	/**
	 * package-category relationship
	 */
	PACKAGE_CATEGORIES(32),

	/**
	 * xtd comment
	 */
	XTDCOMMENT(33),

	/**
	 * comment
	 */
	COMMENT(34),

	/**
	 * comment flag
	 */
	COMMENTFLAG(35),

	/**
	 * Client Package Version
	 */
	CLIENTPACKAGEVERSION(36),

	// /**
	// * user
	// */
	// USER(37),

	/**
	 * dependency
	 */
	DEPENDENCY(38),

	/**
	 * Setting
	 */
	SETTING(39),

	/**
	 * Site permission
	 */
	SITEPERMISSION(40),

	/**
	 * Page
	 */
	PAGE(41),

	/**
	 * Rich text page
	 */
	RICHTEXTPAGE(42),

	/**
	 * Link
	 */
	LINK(43),

	/**
	 * Blog post
	 */
	BLOGPOST(44),

	/**
	 * Blog Category
	 */
	BLOGCATEGORY(45),

	/**
	 * Comment
	 */
	THREADEDCOMMENT(46),

	/**
	 * Keyword
	 */
	KEYWORD(47),

	/**
	 * assigned keyword
	 */
	ASSIGNEDKEYWORD(48),

	/**
	 * Rating
	 */
	RATING(49),

	/**
	 * Form
	 */
	FORM(50),

	/**
	 * Field
	 */
	FIELD(51),

	/**
	 * Form entry
	 */
	FORMENTRY(52),

	/**
	 * Form field entry
	 */
	FIELDENTRY(53),

	/**
	 * Gallery
	 */
	GALLERY(54),

	/**
	 * Image
	 */
	GALLERYIMAGE(55),

	/**
	 * redirect
	 */
	REDIRECT(56),

	/**
	 * 产品-应用-版本激活启动
	 */
	SUMACTIVATEDEVICEPRODUCTPACKAGEVERSIONRESULT(57),

	/**
	 * 应用版本
	 */
	PACKAGEDIM(58),

	/**
	 * 应用下载
	 */
	SUMDOWNLOADPRODUCTRESULT(59),

	/**
	 * 榜单
	 */
	PACKAGERANKING(60),

	/**
	 * 榜单类型
	 */
	PACKAGERANKINGTYPE(61),

	/**
	 * 榜单应用
	 */
	PACKAGERANKINGITEM(62),

	/**
	 * 反馈类型
	 */
	FEEDBACKTYPE(63),

	/**
	 * 封面
	 */
	LOADINGCOVER(64),

	/**
	 * ios app data
	 */
	IOSAPPDATA(65),

	/**
	 * ios package version
	 */
	IOSPACKAGEVERSION(66),

	/**
	 * resource
	 */
	RESOURCE(67),

	/**
	 * 反馈
	 */
	FEEDBACK(68),

	/**
	 * ios buy info
	 */
	IOSBUYINFO(69),

	/**
	 * ios package
	 */
	IOSPACKAGE(70),

	/**
	 * 礼包
	 */
	GIFTBAG(71),

	/**
	 * 说明
	 */
	NOTE(72),

	/**
	 * 推荐
	 */
	RECOMMEND(73),

	/**
	 * 视频
	 */
	VIDEO(74),

	/**
	 * 活动
	 */
	ACTIVITY(75),

	/**
	 * 公告
	 */
	BULLETIN(76),

	/**
	 * 抽奖
	 */
	LOTTERY(77),

	/**
	 * 奖品
	 */
	LOTTERYPRIZE(78),

	/**
	 * 中奖名单
	 */
	LOTTERYWINNING(79),

	/**
	 * 公会
	 */
	GUILD(80),
	/**
	 * 公会数据
	 */
	GUILD_DATA(81),
	/**
	 * 公会礼包
	 */
	GUILD_GIFTBAG(82),
	/**
	 * 公会成员
	 */
	GUILD_MEMBER(83),
	/**
	 * 公会消息
	 * 
	 */
	GUILD_MESSAGE(84),
	/**
	 * 公会勋章
	 * 
	 */
	GUILD_MEDAL(85),

	/**
	 * 勋章
	 * 
	 */
	MEDAL(86),

	/**
	 * 定时任务
	 */
	TASKTIMING(87),

	/**
	 * 用户好友
	 */
	USER_FRIEND(88),

	/**
	 * 用户统计
	 */
	USER_REPORT(89),
	/**
	 * 包下载统计
	 */
	PACKAGE_REPORT(90),

	/**
	 * 公会统计
	 */
	GUILD_REPORT(91),
	/**
	 * 友情链接
	 */
	FRIENDLINK(100),
	/**
	 * 社区版块
	 */
	FORUM_AREA(101),
	/**
	 * 社区话题
	 */
	FORUM_TOPIC(102),

	/**
	 * 开发者用户
	 */
	DEVELOPER_USER(103),

	/**
	 * 开发者结算信息
	 */
	DEVELOPER_USER_SETTLEMENT(104),

	/**
	 * 开发者资质
	 */
	DEVELOPER_USER_QUALIFICATION(105),

	/**
	 * 开发者SDK接口信息
	 */
	DEVELOPER_USER_INTEFACE(106),

	/**
	 * 开发者商品类型
	 */
	DEVELOPER_PRODUCT_CATEGORY(107),

	/**
	 * 开发者游戏
	 */
	DEVELOPER_GAME(108),

	/**
	 * 开发者游戏商品
	 */
	DEVELOPER_GAME_PRODUCT(109),
	/**
	 * 订单
	 */
	ORDERS(110),
	/**
	 * 折扣券
	 */
	DISCOUNT_COUPON(111),

	/**
	 * 支付折扣
	 */
	PAY_DISCOUNT(112),
	/**
	 * 用户活跃
	 */
	USER_ACTIVE(113),
	
	/**
	 * 小米推送
	 */
	MIPUSH(114);;

	Integer id;

	ContentTypeEnum(Integer _id) {
		this.id = _id;
	}

	public Integer getId() {
		return id;
	}

	public static ContentTypeEnum getContentTypeEnum(Integer id) {
		ContentTypeEnum[] enumArr = ContentTypeEnum.values();
		for (ContentTypeEnum aEnum : enumArr) {
			if (aEnum.getId().intValue() == id.intValue()) {
				return aEnum;
			}
		}
		return null;
	}

	public static String getContentType(Integer id) {
		ContentTypeEnum aEnum = getContentTypeEnum(id);
		if (aEnum != null) {
			return aEnum.name().toLowerCase();
		}
		return null;
	}
}
