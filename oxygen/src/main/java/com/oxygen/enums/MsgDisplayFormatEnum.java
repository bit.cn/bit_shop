package com.oxygen.enums;

/**
 * 
 * 消息显示格式
 * 
 * @author Daniel
 */
public enum MsgDisplayFormatEnum {
	/**
	 * 标题+内容
	 */
	title_content,
	/**
	 * 标题+内容+详情链接
	 */
	title_content_link,
	/**
	 * 标题+图片+详情链接
	 */
	title_image_link,
	/**
	 * 标题+内容+图片
	 */
	title_content_image,
	/**
	 * 标题+内容+图片+详情链接
	 */
	title_content_image_link, ;
}
