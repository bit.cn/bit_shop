package com.oxygen.enums;

import java.util.LinkedHashMap;
import java.util.Map;

public enum ClientChannelEnum {
	android_client("android_client", "虫虫助手"), android_guild("android_guild", "虫虫公会"), android_payment_sdk("android_payment_sdk", "支付SDK");

	private String channelCode;
	private String description;

	ClientChannelEnum(String channelCode, String description) {
		this.channelCode = channelCode;
		this.description = description;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public String getDescription() {
		return description;
	}

	public static ClientChannelEnum[] getClientChannels() {
		return ClientChannelEnum.values();
	}

	public static Map<String, String> getClientChannelMap() {
		Map<String, String> retMap = new LinkedHashMap<String, String>();
		for (ClientChannelEnum aEnum : ClientChannelEnum.values()) {
			retMap.put(aEnum.getChannelCode(), aEnum.getDescription());
		}
		return retMap;
	}
}
