package com.oxygen.enums;

/**
 * 开关
 * 
 * @author Daniel
 */
public enum SwitchEnum {
	on, off;
}
