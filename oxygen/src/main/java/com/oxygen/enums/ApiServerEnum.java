package com.oxygen.enums;

import java.util.LinkedHashMap;
import java.util.Map;

public enum ApiServerEnum {
	/**
	 * 用户体系api
	 */
	user("用户体系api"),
	/**
	 * 产品体系
	 */
	product("产品体系"),
	/**
	 * 内容体系api
	 */
	content("内容体系api"),
	/**
	 * 活动体系api
	 */
	activity("活动体系api"),
	/**
	 * 公用服务api
	 */
	commonservice("公用服务api"),
	/**
	 * 报表服务api
	 */
	report("报表服务api"),

	/**
	 * 支付体系api
	 */
	payment("支付体系api"),

	/**
	 * 订单体系api
	 */
	order("订单体系api"),
	/**
	 * 任务体系api
	 */
	task("任务api"),
	/**
	 * 支付体系api
	 */
	devloper("开发者体系api");
	private String descript;

	ApiServerEnum(String _descript) {
		this.descript = _descript;
	}

	public String getDescript() {
		return descript;
	}

	public static Map<String, String> getApiServerEnumMap() {
		Map<String, String> retMap = new LinkedHashMap<String, String>();
		ApiServerEnum[] enumArr = ApiServerEnum.values();
		for (ApiServerEnum aEnum : enumArr) {
			retMap.put(aEnum.name(), aEnum.getDescript());
		}
		return retMap;
	}

	public static ApiServerEnum getApiServerEnum(String name) {
		try {
			return ApiServerEnum.valueOf(name);
		} catch (Exception e) {
			return null;
		}
	}
}
