package com.oxygen.enums;

public enum MemcachedEnum {
	/**
	 * 缓存数据是否启用
	 */
	memcache_open_key("MEMCACHE_OPEN_KEY"), 
	
	/***************************************数据缓存key值统一管理*************************************/
	memcache_user_vercode("MEMCACHE_USER_VERCODE_"),   //用户发送验证码key
	;

	String code;

	MemcachedEnum(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

}
