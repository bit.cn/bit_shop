package com.oxygen.enums;

public enum ActionTypeEnum {
	/**
	 * 做任务
	 */
	task("做任务", null),
	/**
	 * 安装游戏送金币
	 */
	installGame("安装游戏送金币", null),
	/**
	 * 评论
	 */
	comment("评论", null),
	/**
	 * 分享
	 */
	share("分享", null),
	/**
	 * 签到
	 */
	sign("签到", null),
	/**
	 * 刮刮卡
	 */
	scratchCard("刮刮卡", null),
	/**
	 * 大转盘抽奖
	 */
	lottery("大转盘抽奖", null),

	/**
	 * 注册
	 */
	register("注册", null),
	/**
	 * 交新朋友
	 */
	addFriend("交新朋友", "将任意陌生人添加为好友"),

	/**
	 * 签到(发言) 自动签到
	 */
	autoSign("签到(发言) 自动签到", null),
	/**
	 * 我爱发帖
	 */
	createTopic("我爱发言", "在社区任意版块发布新话题"),
	/**
	 * 我爱点赞
	 */
	likeTopic("我爱点赞", "打开任意话题进行点赞"),
	/**
	 * 回帖
	 */
	replyTopic("我爱回复", "打开任意话题进行回复"),
	/**
	 * 联络感情/发言
	 */
	sendMsg("联络感情", "向任意好友或公会发送消息"),
	/**
	 * 申请招募
	 */
	applyRecruit("申请招募", null),

	/**
	 * 领取礼包
	 */
	userTakeGiftbag("领取礼包", null),

	/**
	 * 创建订单
	 */
	createOrder("创建订单", null),
	/**
	 * 充值虫币
	 */
	recharge("充值虫币", null),
	/**
	 * 充值赠送
	 */
	rebate("充值赠送", null);
	private String title;
	private String description;

	ActionTypeEnum(String title, String description) {
		this.title = title;
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public static ActionTypeEnum[] getActionTypeEnum() {
		return ActionTypeEnum.values();
	}
}
