package com.oxygen.enums;

import java.util.LinkedHashMap;
import java.util.Map;

public enum ApiMsgEnum {
	// 这一部分是系统级错误代码（从0000开始到0999）////////////////
	/**
	 * 操作成功
	 */
	SUCCESS(Boolean.TRUE, "0000", "成功"),
	/**
	 * 操作失败
	 */
	FAIL(Boolean.FALSE, "0001", "失败"),
	/**
	 * 服务器内部出现异常
	 */
	INTERNAL_ERROR(Boolean.FALSE, "9999", "服务器内部出现异常:{0}"),
	/**
	 * 签名错误
	 */
	TOKEN_ERROR(Boolean.FALSE, "0002", "签名错误"),
	/**
	 * 调用错误
	 */
	INVOKE_ERROR(Boolean.FALSE, "0003", "调用错误"),
	/**
	 * 未知错误
	 */
	UNKNOW_ERROR(Boolean.FALSE, "0004", "未知错误"),
	/**
	 * 只支持post请求
	 */
	POST_ONLY(Boolean.FALSE, "0005", "只支持post请求"),
	/**
	 * 缺少参数
	 */
	MissParameterException(Boolean.FALSE, "0006", "缺少参数"),
	/**
	 * 数据格式错误
	 */
	DataFormatException(Boolean.FALSE, "0007", "数据格式错误"),
	/**
	 * 请求被禁止/提交数据有误
	 */
	ForbiddenException(Boolean.FALSE, "0008", "提交数据有误"),

	/**
	 * 请求错误
	 */
	BadRequestException(Boolean.FALSE, "0009", "请求错误"),
	/**
	 * 未找到
	 */
	NotFoundException(Boolean.FALSE, "0010", "未找到"),
	/**
	 * 无返回内容
	 */
	NoContentException(Boolean.FALSE, "0011", "无返回内容"),
	/**
	 * 创建成功
	 */
	CreatedSuccess(Boolean.TRUE, "0020", "创建成功"),
	/**
	 * 已存在
	 */
	Existed(Boolean.TRUE, "0021", "已存在"),
	/**
	 * 不存在
	 */
	DosentExistsException(Boolean.FALSE, "0022", "不存在"),
	/**
	 * 平台错误
	 */
	PlatformException(Boolean.FALSE, "0023", "平台错误"),
	/**
	 * 请求敏感词
	 */
	StopwordException(Boolean.FALSE, "0025", "内容中含有敏感词"),

	/**
	 * 文件上传错误
	 */
	FileUploadException(Boolean.FALSE, "0100", "文件上传错误"),
	/**
	 * 文件大小错误
	 */
	FileSizeException(Boolean.FALSE, "0101", "文件大小错误"),
	/**
	 * 文件对象错误
	 */
	FileItemException(Boolean.FALSE, "0102", "文件对象错误"),

	// 这一部分是用户体系错误代码（从1000开始到1999）////////////////
	/**
	 * 用户名不能为空
	 */
	UserNameIsNullException(Boolean.FALSE, "1000", "用户名不能为空"),
	/**
	 * 密码不能为空
	 */
	UserPasswordIsNullException(Boolean.FALSE, "1001", "密码不能为空"),
	/**
	 * 登录失败次数过多
	 */
	ExcessiveAttemptsException(Boolean.FALSE, "1002", "登录失败次数过多"),
	/**
	 * 用户名或密码错误
	 */
	UserNameOrPasswordException(Boolean.FALSE, "1003", "用户名或密码错误"),
	/**
	 * 用户未激活
	 */
	UserInactiveException(Boolean.FALSE, "1004", "账号未激活"),
	/**
	 * 用户被锁定
	 */
	UserLockedException(Boolean.FALSE, "1005", "账号被锁定"),
	/**
	 * 用户名已经被注册
	 */
	UserNameExistedException(Boolean.FALSE, "1007", "用户名已被使用"),
	/**
	 * 邮箱已经被注册
	 */
	EmailExistedException(Boolean.FALSE, "1008", "邮箱已被使用"),
	/**
	 * 手机号码已经被注册
	 */
	PhoneExistedException(Boolean.FALSE, "1009", "手机号码已被使用"),
	/**
	 * 用户未登录
	 */
	UserUnloginException(Boolean.FALSE, "1010", "用户未登录"),
	/**
	 * 当前密码错误
	 */
	UserPasswordException(Boolean.FALSE, "1011", "当前密码错误"),
	/**
	 * 用户类型错误
	 */
	UserTypeException(Boolean.FALSE, "1012", "用户类型错误"),
	/**
	 * 新密码与确认密码不一致
	 */
	PasswordMismatchException(Boolean.FALSE, "1013", "新密码与确认密码不一致"),
	/**
	 * 验证码错误
	 */
	ValidateCodeNull(Boolean.FALSE, "1105", "验证码不能为空"),
	/**
	 * 验证码错误
	 */
	ValidateCodeException(Boolean.FALSE, "1014", "验证码错误"),
	/**
	 * 验证码发送失败
	 */
	ValidateCodeSendException(Boolean.FALSE, "1025", "验证码发送失败"),
	/**
	 * 验证码已失效
	 */
	ValidateCodeInvalid(Boolean.FALSE, "1026", "验证码已失效"),
	/**
	 * 登录密码至少6位最多16位
	 */
	UserPasswordInvlidException(Boolean.FALSE, "1015", "登录密码至少6位最多16位"),
	/**
	 * 当前手机号不正确
	 */
	UserCurrentPhoneException(Boolean.FALSE, "1016", "当前手机号不正确"),
	/**
	 * 获取接口调用凭证错误
	 */
	GetAccessTokenException(Boolean.FALSE, "1017", "获取接口调用凭证错误"),
	/**
	 * 获取用户信息错误
	 */
	GetUserInfoException(Boolean.FALSE, "1018", "获取用户信息错误"),
	/**
	 * 手机号码不存在
	 */
	PhoneDosentExistsException(Boolean.FALSE, "1019", "手机号码不存在"),
	/**
	 * 邮箱格式不对
	 */
	EmailFormatException(Boolean.FALSE, "1020", "邮箱格式错误"),
	/**
	 * QQ格式不对
	 */
	QQFormatException(Boolean.FALSE, "1021", "QQ格式错误"),
	/**
	 * 当前邮箱不正确
	 */
	UserCurrentEmailException(Boolean.FALSE, "1022", "当前邮箱不正确"),
	/**
	 * 用户不存在
	 */
	UserNotExistedException(Boolean.FALSE, "1023", "用户不存在"),
	/**
	 * 邮箱不存在
	 */
	EmailDosentExistsException(Boolean.FALSE, "1024", "邮箱不存在"),

	/**
	 * 已经是好友了
	 */
	FriendExistedException(Boolean.FALSE, "1100", "已经是好友了"),
	/**
	 * 不能加自己为好友
	 */
	FriendCannotAddSelfException(Boolean.FALSE, "1101", "不能加自己为好友"),
	/**
	 * 添加成功
	 */
	FriendAddSuccess(Boolean.TRUE, "1102", "添加成功"),
	/**
	 * 好友已申请，等待验证
	 */
	FriendAddSuccessWaitValidate(Boolean.TRUE, "1103", "好友已申请，等待验证"),
	/**
	 * 不能重复申请
	 */
	FriendAppliedAdd(Boolean.FALSE, "1104", "不能重复申请"),

	/**
	 * 金币不足
	 */
	UserCoinNotEnough(Boolean.FALSE, "1200", "金币不足"),

	/**
	 * 订单已经支付"
	 */
	OrderPaidException(Boolean.FALSE, "6010", "订单已经支付"),
	/**
	 * 订单不存在"
	 */
	OrderNotExistException(Boolean.FALSE, "6011", "该订单不存在"),

	/**
	 * 支付记录不存在
	 */
	PaymentNotExistException(Boolean.FALSE, "6014", "支付记录不存在"),

	/**
	 * 已支付
	 */
	PaymentPaidException(Boolean.FALSE, "6015", "已支付"),

	/**
	 * 充值已到账
	 */
	PaymentToAccountedException(Boolean.FALSE, "6016", "充值已到账"),
	/**
	 * 充值返虫币关闭
	 */
	PaymentRechargeRabateException(Boolean.FALSE, "6017", "充值返虫币关闭"),

	/**
	 * 目前暂无代金券
	 */
	NoCouponsException(Boolean.FALSE, "6018", "目前暂无代金券"),

	/**
	 * 订单未支付
	 */
	OrderNopayException(Boolean.FALSE, "6022", "订单未支付"),

	/**
	 * 卡面值小于订单金额
	 */
	CardMoneyLessThanOrderPrice(Boolean.FALSE, "6030", "面值小于订单金额"),

	/**
	 * 微信支付金额小于0.1元
	 */
	WechatpayAmountLessThanMinValue(Boolean.FALSE, "6031", "微信支付金额不能小于0.1元"),

	/**
	 * 代金券不在有效使用期内
	 */
	CouponIsExpiredException(Boolean.FALSE, "7006", "代金券不在有效使用期内"),

	/**
	 * 该平台无法使用此代金券
	 */
	CouponIsWrongPlateFormException(Boolean.FALSE, "7007", "该平台无法使用此代金券"),

	/**
	 * 无此代金券信息
	 */
	CouponIsNoWhereFormException(Boolean.FALSE, "7008", "无此代金券信息"),

	/**
	 * 无效的兑换码
	 */
	CouponCodeIsValid(Boolean.FALSE, "7009", "无效的兑换码"),

	/**
	 * 自定义金额太小
	 */
	CustomAmountMoreThan0(Boolean.FALSE, "7010", "自定义金额太小"),

	/**
	 * 自定义金额不能为空
	 */
	CustomAmountIsNOTNULL(Boolean.FALSE, "7011", "自定义金额不能为空"),
	/**
	 * 积分不足
	 */
	IntegralShortage(Boolean.FALSE, "7012", "积分不足"),

	/**
	 * 产品下线
	 */
	ProudctOffLine(Boolean.FALSE, "7013", "产品下线"),
	/**
	 * 必须购买一份以上
	 */
	OrderQuantityMoreThan0(Boolean.FALSE, "7014", "必须购买一份以上"),
	/**
	 * 产品所需积分已经变化
	 */
	ProductIntegralChange(Boolean.FALSE, "7015", "产品所需积分已经变化"),

	/**
	 * 产品所需金额已经变化
	 */
	ProductMoneyChange(Boolean.FALSE, "7016", "产品价格已经变化"),

	/**
	 * 此产品不支持该兑换方式
	 */
	ProductNotPayWay(Boolean.FALSE, "7017", "此产品不支持该兑换方式"),
	/**
	 * 订单已经取消不能再支付
	 */
	orderCancel(Boolean.FALSE, "7018", "该订单已经取消不能再支付"),

	/**
	 * 该订单已经超过支付等待时间不能支付
	 */
	orderThanMorePaymentWaitDateTime(Boolean.FALSE, "7019", "该订单已经超过支付等待时间不能支付"),
	/**
	 * 您已确定该订单使用{0}张优惠券，不可更改
	 */
	orderCouponNotExchange(Boolean.FALSE, "7020", "您已确定该订单使用{0}张优惠券，不可更改"),

	/**
	 * 您没有足够的优惠券
	 */
	CouponNotEnoughException(Boolean.FALSE, "7021", "您没有足够的优惠券"),
	/**
	 * 抱歉，该游玩日期不可选，请重新选择
	 */
	VisitTimeDisabled(Boolean.FALSE, "7022", "抱歉，该游玩日期不可选，请重新选择"),

	/**
	 * 已支付成功，请查看订单状态"
	 */

	OrderPaidToDetail(Boolean.FALSE, "7023", "已支付成功，请查看订单状态"),
	/**
	 * 创建订单失败，请重新下订单
	 */
	CreateOrderFail(Boolean.FALSE, "7024", "创建订单失败，请重新下订单"),
	/**
	 * 订单支付失败，请重新再试
	 */
	PayOrderFail(Boolean.FALSE, "7025", "订单支付失败，请重新再试"),
	/**
	 * 产品卖疯了!只有{0}个货\n请重新选择产品数量
	 */
	StockInsufficie(Boolean.FALSE, "7026", "产品卖疯了!只有{0}个货\n请重新选择产品数量"),
	/**
	 * 产品兑换完了,请兑换其他产品
	 */

	StockInsufficieZeor(Boolean.FALSE, "7027", "产品兑换完了,请兑换其他产品"),

	/**
	 * 该栋座下已有房源生成合同
	 */
	HouseHasContract(Boolean.FALSE, "6028", "该栋座下已有房源生成合同"),
	/**
	 * 该房型在使用中
	 */
	RoomTypeInUse(Boolean.FALSE, "6029", "该房型在使用中"),
	/**
	 * 该房源已生成合同
	 */
	ThisHouseHasContract(Boolean.FALSE, "6030", "该房源已生成合同"),

	/**
	 * 度假屋下订单提示
	 */
	HotailEstateStockInsufficieZeor(Boolean.FALSE, "7028", "预定时间超限"),

	HotailEstateOrderSwitchoffStatus(Boolean.FALSE, "7029", "很抱歉，暂不可预订度假屋"),

	HotailEstateOrderNoMoreThanNum(Boolean.FALSE, "7030", "很抱歉，当日预订超限"),

	HotailEstateOneOrderNoMoreThanNum(Boolean.FALSE, "7031", "单个订单预定套数超限"),

	HotailEstateOneOrderEveryDayNoMoreThanNum(Boolean.FALSE, "7032", "很抱歉，当日预订同一度假屋超限"),

	HotailEstateBookingMoreTime(Boolean.FALSE, "7033", "预定时间超限"),

	/**
	 * 积分商城下订单提示语
	 */
	PointShopOrderSwitchoffStatus(Boolean.FALSE, "7034", "很抱歉，暂不可兑换商品"),

	PointShopOrderStockInsufficieZeor(Boolean.FALSE, "7035", "兑换时间超限"),

	PointShopOrderNoMoreThanNum(Boolean.FALSE, "7036", "很抱歉，当日兑换超限"),

	PointShopOrderOneOrderNoMoreThanNum(Boolean.FALSE, "7037", "单个订单兑换数超限"),

	PointShopMoreThanSkuNum(Boolean.FALSE, "7038", "很抱歉，暂不可兑换商品"),

	ProductPlatformMoneyChange(Boolean.FALSE, "7039", "产品换游币发生变化"),

	orderBooking(Boolean.FALSE, "7040", "该订单在预定中不能取消"),

	OrderPayChange(Boolean.FALSE, "7041", "当前支付方式已发生改变，请重新选择"),

	ProductPlatformAndMoneyChange(Boolean.FALSE, "7042", "产品金额和换游币发生变化"),

	HotailEstateStock(Boolean.FALSE, "7043", "当前日期已无房源，请选择其他日期或者其他度假屋"),

	HotailEstateNot(Boolean.FALSE, "7044", "当前日期已过期，请重新选择日期"),

	HotailEstateNoTime(Boolean.FALSE, "7044", "已过最晚预订时间，请重新下订单"),

	ShopStockNotLess(Boolean.FALSE, "7045", "当前库存不足，请选择其他产品"),
	
	TrackObjectAlert(Boolean.FALSE, "7046", "目前版本不支持优惠券查看和使用，请升级到最新版本！"),

	// 换游币相关操作的状态码
	/**
	 * 系统管理员每天发放的换游币超出上限
	 */
	ExchangeCurrencySendOverstep(Boolean.FALSE, "8001", "系统管理员今天发放的换游币已经超出上限"),
	/**
	 * 客户每天充值的换游币超出上限
	 */
	ExchangeCurrencyRechargeOverstep(Boolean.FALSE, "8002", "客户今天充值的换游币超出上限"),
	/**
	 * 充值的换游币已经过期
	 */
	ExchangeCurrencyRechargeExpired(Boolean.FALSE, "8008", "充值的换游币已经过期"),

	/**
	 * 订单支付校验换游币不足
	 */
	ExchangeCurrencyNotEnough(Boolean.FALSE, "8003", "您的换游币不足，是否选择现金支付？"),

	/**
	 * 订单支付换游币校验成功
	 */
	ExchangeCurrencyEnough(Boolean.FALSE, "8004", "换游币校验成功"),

	/**
	 * 订单支付换游币扣款成功
	 */
	ExchangeCurrencyPaySuccess(Boolean.FALSE, "8005", "换游币扣款成功"),

	/**
	 * 订单支付换游币扣款失败
	 */
	ExchangeCurrencyPayFalse(Boolean.FALSE, "8006", "换游币扣款失败"),

	/**
	 * 查询换游币概要信息如果不存在换游币信息
	 */
	ExchangeCurrencyNone(Boolean.FALSE, "8007", "暂时您还没有换游币信息"),

	// 物业系统相关操作状态码
	/**
	 * 用户已经激活
	 * */
	TrusteeshipAlreadyActivated(Boolean.FALSE, "8008", "该托管号已经激活"),

	/**----------------activity:02000--02999 ---------start--------------*/

	ActivityYourselfIsDrawing(Boolean.FALSE, "02000", "您已在抽奖中请等待抽奖结果!"),
	ActivityInvalid(Boolean.FALSE, "02001", "活动无效"), 
	ActivityNotSettingGift(Boolean.FALSE, "02002", "未设置抽奖奖品"), 
	ActivityLoadGiftStockError(Boolean.FALSE, "02003", "加载奖品库存数据异常"),
	ActivityClosed(Boolean.FALSE, "02004", "活动已关闭"),
	ActivityNoGoing(Boolean.FALSE, "02005", "活动不在进行中"), 
	ActivityReady(Boolean.FALSE, "02006", "活动尚未开始，请耐心等待。"), 
	ActivityCompleting(Boolean.FALSE, "02007","活动设置完善中"), 
	ActivityAddPowerFailing(Boolean.FALSE, "02008", "加体力失败"), 
	ActivityAddPowerOver(Boolean.FALSE, "02009", "今日换体力次数超限，明日再来试试哦！"), 
	ActivitylackMinusPower(Boolean.FALSE, "02010","中奖前需要先要有抽奖减一次体能的记录"), 
	CustomerIsExistException(Boolean.FALSE, "02011", "用户Id不存在"),
	ActivityPowerIsNotEnough(Boolean.FALSE, "02100", "体力不足"), 
	ActivityNotCanUpdate(Boolean.FALSE, "02101", "上线状态的活动不允许编辑"),
	/**
	 * 积分不足
	 */
	ActivityIntegralShortage(Boolean.FALSE, "02102", "您的积分不足，不能兑换体力"),
	
	//招募提示消息
	ActivityRecruitVerCodeError(Boolean.FALSE, "02201", "您输入的验证码有误，请输入正确的验证码，谢谢"),
	ActivityRecruitDuplicateAdd(Boolean.FALSE, "02202", "您的手机号已提交报名，我们会尽快跟你联系，请耐心等待，谢谢"),
	ActivityRecruitSysError(Boolean.FALSE, "02203", "系统开小差啦，重新提交一次吧"),

	//id管理系统
	ActivityIdManageDuplicateAdd(Boolean.FALSE, "02300", "ID编号重复，请重新生成后重试!"),
	
	//用户留言
	ActivityAddMessageIsToMuch(Boolean.FALSE, "02310", "您的留言过于频繁，休息一会再来哦!"),

	// ----------------activity:02000--02999 ---------end--------------
	/**
	 * 手机号或托管用户号不正确
	 * */
	MoblieOrEntrustNumNotExist(Boolean.FALSE, "8009", "手机号或托管用户号不正确");
	public Boolean isSuccess;
	public String code;
	public String msg;

	ApiMsgEnum(Boolean isSuccess, String code, String msg) {
		this.isSuccess = isSuccess;
		this.code = code;
		this.msg = msg;
	}

	public Boolean getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public static Map<String, String> getAll() {
		Map<String, String> retMap = new LinkedHashMap<String, String>();
		ApiMsgEnum[] enumArr = ApiMsgEnum.values();
		for (ApiMsgEnum aEnum : enumArr) {
			retMap.put(aEnum.getCode(), aEnum.getMsg());
		}
		return retMap;
	}

	public static ApiMsgEnum getByCode(String code) {
		ApiMsgEnum[] enumArr = ApiMsgEnum.values();
		for (ApiMsgEnum aEnum : enumArr) {
			if (aEnum.getCode().equals(code)) {
				return aEnum;
			}
		}
		return null;
	}
}
