package com.oxygen.enums;

import java.util.LinkedHashMap;
import java.util.Map;

import com.oxygen.interfaces.Api;

public enum ApiMethodEnum implements Api {

	/**
	 * 获取用户信息
	 */
	USER_GETPROFILE("user.getProfile", "获取用户信息", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("authorization_token", "用户token");
			paramMap.put("user_id", "用户ID");
			return paramMap;
		}
	},
	/**
	 * 获取简要信息
	 */
	USER_GETSIMPLEPROFILE("user.getSimpleProfile", "获取简要信息", ApiServerEnum.user, ContentTypeEnum.USER, true, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("authorization_token", "用户token");
			paramMap.put("user_id", "用户ID");
			return paramMap;
		}
	},
	/**
	 * 修改基本信息
	 */
	USER_UPDATEPROFILE("user.updateProfile", "修改基本信息", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("authorization_token", "用户token");
			paramMap.put("icon", "头像");
			paramMap.put("email", "邮箱");
			paramMap.put("email_code", "邮箱验证码");
			paramMap.put("sex", "性别");
			paramMap.put("birthday", "生日");
			paramMap.put("phone", "手机");
			paramMap.put("code", "手机验证码");
			paramMap.put("cover", "背景图");
			paramMap.put("nick_name", "昵称");
			paramMap.put("signature", "签名");
			return paramMap;
		}
	},
	/**
	 * 新增定时任务
	 */
	BACKEND_TASKTIMING_ADD("backend.TaskTiming.add", "新增定时任务", ApiServerEnum.task, ContentTypeEnum.TASKTIMING, false, true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("executeTime", "任务执行时间");
			paramMap.put("interfaceName", "执行接口");
			paramMap.put("parameters", "参数");
			paramMap.put("status", "任务状态");
			paramMap.put("taskType", "任务类型");
			paramMap.put("mainTaskId", "主任务ID");
			paramMap.put("mutexFlag", "任务互斥标志");
			paramMap.put("afterMinutes", "任务延时分钟");
			return paramMap;
		}
	},

	/**
	 * 根据参数删除定时任务
	 */
	BACKEND_TASKTIMING_DELETEBYPARAMS("backend.TaskTiming.deleteByParams", "根据参数删除定时任务", ApiServerEnum.task, ContentTypeEnum.TASKTIMING, false, true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("idList", "ID");
			return paramMap;
		}
	},

	/**
	 * 手工取消订单
	 */
	PAYMENT_ORDERCANCEL("payment.orderCancel", "手工取消订单", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transaction_no", "交易流水号");
			return paramMap;
		}
	},

	/**
	 * 订单自动取消
	 */
	PAYMENT_ORDERAUTOCANCEL("payment.orderAutoCancel", "订单自动取消", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transaction_no", "交易流水号");
			return paramMap;
		}
	},

	/**
	 * 支付渠道
	 */
	PAYMENT_PAYMENTCHANNELS("payment.paymentChannels", "支付渠道", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("isRecharge", "是否为充值业务");
			return paramMap;
		}
	},

	/**
	 * 创建门票订单
	 */
	PAYMENT_CREATEORDERFORTICKET("payment.createOrderForTicket", "创建门票订单", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("productId", "产品Id");
			paramMap.put("quantity", "购买份数");
			paramMap.put("exchange", "兑换({integral:{value:多少积分},coupon:{value:面值,quantity:多少张},money:{value:RMB},type:{value:(1:纯积分,2:积分+RMB)}})");
			paramMap.put("visitTime", "游玩日期(yyyy-MM-dd)");
			paramMap.put("contactName", "取票人姓名");
			paramMap.put("contactPhone", "取票人电话");
			paramMap.put("cardType", "取票人证类型(identity, driving)");
			paramMap.put("cardId", "取票人身份证");
			paramMap.put("customerMessage", "用户备注");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},

	/**
	 * 创建线路订单
	 */
	PAYMENT_CREATEORDERFORROUTE("payment.createOrderForRoute", "创建线路订单", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("productId", "产品Id");
			paramMap.put("quantity", "购买份数");
			paramMap.put("exchange", "兑换({integral:{value:多少积分},coupon:{value:面值,quantity:多少张},money:{value:RMB},type:{value:(1:纯积分,2:积分+RMB)}})");
			paramMap.put("contacts", "联系人([{contactType:联系人类型(contact,visitor),contactName:姓名,contactPhone:电话,cardType:证件类,cardId:证件号码}])");
			paramMap.put("customerMessage", "用户备注");
			paramMap.put("origin", "出发地");
			paramMap.put("checkingInTime", "出发时间(USERy-MM-dd)");
			paramMap.put("checkOutTime", "结束日期(USERy-MM-dd)");
			paramMap.put("timeSchedulePordId", "档期");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},

	/**
	 * 创建体验产品订单
	 */
	PAYMENT_CREATEORDERFOREXPERIENCED("payment.createOrderForExperienced", "创建体验产品订单", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("productId", "产品Id");
			paramMap.put("quantity", "购买份数");
			paramMap.put("contacts", "联系人([{contactType:联系人类型(contact,visitor,checkin),contactName:姓名,contactPhone:电话,cardType:证件类,cardId:证件号码}])");
			paramMap.put("exchange", "兑换({integral:{value:多少积分},coupon:{value:面值,quantity:多少张},money:{value:RMB},type:{value:(1:纯积分,2:积分+RMB)}})");
			paramMap.put("customerMessage", "用户备注");
			paramMap.put("checkingInTime", "入住日期(USERy-MM-dd)");
			paramMap.put("checkOutTime", "退房日期(USERy-MM-dd)");
			paramMap.put("timeSchedulePordId", "档期");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},

	/**
	 * 创建超值礼品订单
	 */
	PAYMENT_CREATEORDERFORGIFT("payment.createOrderForGift", "创建超值礼品订单", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("productId", "产品Id");
			paramMap.put("quantity", "购买份数");
			paramMap.put("exchange", "兑换({integral:{value:多少积分},coupon:{value:面值,quantity:多少张},money:{value:RMB},type:{value:(1:纯积分,2:积分+RMB)}})");
			paramMap.put("contactName", "收货人姓名");
			paramMap.put("contactPhone", "收货人电话");
			paramMap.put("contactAddress", "收货人地址");
			paramMap.put("customerMessage", "用户备注");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},

	/**
	 * 选择支付渠道
	 */
	PAYMENT_CHOOSEPAYMENTCHANNEL("payment.choosePaymentChannel", "选择支付渠道", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transaction_no", "交易流水号");
			paramMap.put("paymentChannelCode", "支付渠道代码");
			paramMap.put("appType", "应用类型(APP,WAP,WEB)");
			paramMap.put("exchange", "兑换({integral:{value:多少积分},coupon:{value:面值,quantity:多少张}})");
			return paramMap;
		}
	},

	/**
	 * 支付回调
	 */
	PAYMENT_PAYMENTCALLBACK("payment.paymentCallback", "支付回调", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transaction_no", "交易流水号");
			paramMap.put("status", "交易状态");
			paramMap.put("paymentChannelCode", "支付渠道CODE");
			paramMap.put("paymentId", "支付ID");
			paramMap.put("paymentDesc", "支付描述");
			return paramMap;
		}
	},

	/**
	 * 支付结果查询
	 */
	PAYMENT_QUERYBYPAYMENTID("payment.queryByPaymentId", "支付结果查询", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("paymentId", "支付ID");
			return paramMap;
		}
	},

	/**
	 * 选择支付渠道
	 */
	PAYMENT_V2_CHOOSEPAYMENTCHANNEL("payment.v2.choosePaymentChannel", "选择支付渠道", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transaction_no", "交易流水号");
			paramMap.put("paymentChannelCode", "支付渠道代码");
			paramMap.put("appType", "应用类型(APP,WAP,WEB)");
			paramMap.put("exchange", "兑换({integral:{value:多少积分},coupon:{value:面值,quantity:多少张}})");
			return paramMap;
		}
	},

	/**
	 * 订单列表
	 */
	ORDERS_ORDERLIST("orders.orderList", "订单列表", ApiServerEnum.order, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("orderType", "订单类型");
			paramMap.put("authorization_token", "用户token");
			paramMap.put("create_datetime_start", "交易时间开始");
			paramMap.put("create_datetime_end", "交易时间结束");
			paramMap.put("status", "订单状态");
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			return paramMap;
		}
	},

	/**
	 * 订单详情
	 */
	ORDERS_ORDERDETAIL("orders.orderDetail", "订单详情", ApiServerEnum.order, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transactionNo", "交易号");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},

	/**
	 * 订单取消原因
	 */
	ORDERS_CANCELREASON("orders.cancelReason", "订单取消原因", ApiServerEnum.order, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transactionNo", "交易号");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},

	/**
	 * 创建度假屋订单
	 */
	PAYMENT_CREATEORDERFORCOTTAGES("payment.createOrderForCottages", "创建度假屋订单", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("productId", "产品Id");
			paramMap.put("quantity", "购买份数");
			paramMap.put("exchange", "兑换({tourCoin:{value:换游币},money:{value:RMB},type:{value:(1:纯换游币,2:纯RMB 3:换游币+RMB)}})");
			paramMap.put("contacts", "联系人([{contactType:联系人类型(contact,visitor,checkin),contactName:姓名,contactPhone:电话,cardType:证件类,cardId:证件号码}])");
			paramMap.put("customerMessage", "用户备注");
			paramMap.put("checkingInTime", "入住日期(USERy-MM-dd)");
			paramMap.put("checkOutTime", "退房日期(USERy-MM-dd)");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},
	/**
	 * 查询订单常量
	 */
	COMMONSETTING_SELECTCOMMONSETTINGBYKEY("commonSetting.selectCommonSettingByKey", "查询订单常量", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("key", "常量键");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},
	/**
	 * 手动取消订单
	 */
	PAYMENT_USERCANCELORDER("payment.userCancelOrder", "手动取消订单", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transaction_no", "交易流水号");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},
	/**
	 * 订单列表V2.2
	 */
	ORDERS_V2_ORDERLIST("orders.v2.orderList", "订单列表V2.2", ApiServerEnum.order, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("orderType", "订单类型<br>(GIFT, TICKET, ROUTELINE, EXPERIENCED,HOTAILROOMKIND 组合类型 POINTSHOP={GIFT, TICKET, ROUTELINE, EXPERIENCED })");
			paramMap.put("create_datetime_start", "交易时间开始(USERy-MM-dd)");
			paramMap.put("create_datetime_end", "交易时间结束(USERy-MM-dd)");
			paramMap.put("status", "订单状态");
			paramMap.put("authorization_token", "用户token");
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			return paramMap;
		}
	},

	// 换游币暴露接口
	/**
	 * 根据用户ID得到用户的换游币日志信息
	 */
	EXCURRENCY_GETCUSTOMEREXCHANGECURRENCYLOG("excurrency.getCustomerExchangeCurrencyLog", "根据用户ID得到用户的换游币日志信息", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			paramMap.put("authorization_token", "用户请求认证");
			return paramMap;
		}
	},

	/**
	 * 根据用户ID得到用户的换游币信息
	 */
	EXCURRENCY_GETCUSTOMEREXCHANGECURRENCY("excurrency.getCustomerExchangeCurrency", "根据用户ID得到用户的换游币信息", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("authorization_token", "用户请求认证");
			return paramMap;
		}
	},

	/**
	 * 发送验证码
	 */
	USER_SENDCODE("user.sendCode", "发送验证码", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("mobilePhone", "手机号");
			return paramMap;
		}
	},

	/**
	 * 订单快递信息
	 */
	ORDERS_ORDEREXPRESSINFO("orders.orderExpressInfo", "订单快递信息", ApiServerEnum.order, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transactionNo", "交易号");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},

	/**
	 * 注册校验手机号的唯一性
	 */
	USER_CHECKMOBILE("user.checkMobile", "注册校验手机号的唯一性", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("mobilePhone", "手机号");
			paramMap.put("userType", "用户类型");
			return paramMap;
		}
	},

	/**
	 * 后台手机号验证登陆
	 */
	USER_BACKENDLOGIN("user.backendLogin", "后台手机号验证登陆", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("mobile", "手机号");
			paramMap.put("verCode", "验证码");
			return paramMap;
		}
	},

	/**
	 * 获取支付状态
	 */
	PAYMENT_GETPAYMENTSTATUS("payment.getPaymentStatus", "获取支付状态", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transaction_no", "订单号");
			paramMap.put("authorization_token", "用户token");
			return paramMap;
		}
	},

	/**
	 * 物业登陆
	 */
	USER_PROPERTY_PROPERTYLOGIN("user.trusteeship.propertyLogin", "物业登陆", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("mobilePhone", "物业账号");
			paramMap.put("password", "密码");
			return paramMap;
		}
	},

	/**
	 * 业主用户注册
	 */
	USER_PROPERTY_OWNERREGISTER("user.trusteeship.ownerRegister", "业主用户注册", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("mobilePhone", "电话号码");
			paramMap.put("password", "密码");
			paramMap.put("entrustNum", "托管号");
			return paramMap;
		}
	},

	/**
	 * 业主登陆(手机号验证码登录)
	 */
	USER_PROPERTY_OWNERLOGINBYCODE("user.trusteeship.ownerLoginByCode", "业主登陆(手机号验证码登录)", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("mobilePhone", "手机号");
			paramMap.put("vercode", "验证码");
			return paramMap;
		}
	},

	/**
	 * 业主登陆(手机号密码登录)
	 */
	USER_TRUSTEESHIP_OWNERLOGIN("user.trusteeship.ownerLogin", "业主登陆(手机号密码登录)", ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("mobilePhone", "手机号");
			paramMap.put("password", "密码");
			return paramMap;
		}
	},

	/**
	 * 添加留言
	 */
	ACTIVITY_MESSAGEBOARD_INSERT("activity.messageBoard.insert", "添加留言", ApiServerEnum.activity, ContentTypeEnum.ACTIVITY, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("messageRecordJson", "留言信息json串");
			paramMap.put("authorization_token", "authorization_token");
			return paramMap;
		}
	},
	/**
	 * app活动频道页展示所有活动类别
	 */
	APP_HOT_ACTIVITY_INDEX("app.hotActivityType.hotActivityIndex", "app活动频道页展示所有活动类别", ApiServerEnum.activity, ContentTypeEnum.ACTIVITY, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			return paramMap;
		}
	},

	/**
	 * app活动频道页依据类别ID获取活动
	 */
	APP_GET_HOT_ACTIVITY_BY_TYPEID("app.hotActivityType.getHotActivityByTypeId", "app活动频道页依据类别ID获取活动", ApiServerEnum.activity, ContentTypeEnum.ACTIVITY, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("typeId", "活动类别ID");
			return paramMap;
		}
	},

	/**
	 * APP端文章频道页获取内容
	 */
	APP_GET_STORY_LIST_BY_TYPENAME("app.story.getStoryList", "APP端文章频道页获取内容", ApiServerEnum.activity, ContentTypeEnum.ACTIVITY, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			return paramMap;
		}
	},

	/**
	 * 获取七牛上传图片的token
	 */
	BACKEND_COMMON_GETCDNTOKEN("backend.common.getCdnToken", "获取七牛上传图片的token", ApiServerEnum.commonservice, ContentTypeEnum.GALLERYIMAGE, false, true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("bucketName", "bucketName");
			return paramMap;
		}
	},

	/************************************************** 产品product *******************************************/
	/**
	 * 分页查询商品列表
	 */
	PRODUCT_GETPRODUCTPAGELIST("product.getProductPageList", "分页查询商品列表", ApiServerEnum.product, ContentTypeEnum.PRODUCT, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			return paramMap;
		}
	},
	;

	private String code;
	private String msg;
	private ApiServerEnum apiServer;
	private ContentTypeEnum contentType;
	private boolean privateApi;
	private boolean backendApi;
	private boolean webApi;

	/**
	 * @param code
	 *            编码
	 * @param msg
	 *            描述
	 * @param apiServer
	 *            所属服务器
	 * @param contentType
	 *            所属业务
	 * @param privateApi
	 *            是否内部私有API
	 * @param backendApi
	 *            是否后台API
	 * @param webApi
	 *            是否WEB API
	 * @author Daniel
	 */
	ApiMethodEnum(String code, String msg, ApiServerEnum apiServer, ContentTypeEnum contentType, boolean privateApi, boolean backendApi, boolean webApi) {
		this.code = code;
		this.msg = msg;
		this.apiServer = apiServer;
		this.contentType = contentType;
		this.privateApi = privateApi;
		this.backendApi = backendApi;
		this.webApi = webApi;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public ApiServerEnum getApiServer() {
		return apiServer;
	}

	public ContentTypeEnum getContentType() {
		return contentType;
	}

	public boolean isPrivateApi() {
		return privateApi;
	}

	public boolean isBackendApi() {
		return backendApi;
	}

	public boolean isWebApi() {
		return webApi;
	}

	/**
	 * 获取所有API方法枚举
	 * 
	 * @return
	 * @author Daniel
	 */
	public static Map<String, ApiMethodEnum> getApiMethodEnumMap() {
		Map<String, ApiMethodEnum> retMap = new LinkedHashMap<String, ApiMethodEnum>();
		ApiMethodEnum[] enumArr = ApiMethodEnum.values();
		for (ApiMethodEnum aEnum : enumArr) {
			retMap.put(aEnum.getCode(), aEnum);
		}
		return retMap;
	}

	/**
	 * 根据服务器获取API方法（供对外的API测试工具使用）
	 * 
	 * @param apiServer
	 * @return
	 * @author Daniel
	 */
	public static Map<String, String> getApiMethodMapByServer(String apiServer) {
		ApiServerEnum sEnum = ApiServerEnum.getApiServerEnum(apiServer);
		Map<String, String> retMap = new LinkedHashMap<String, String>();
		if (sEnum != null) {
			ApiMethodEnum[] enumArr = ApiMethodEnum.values();
			for (ApiMethodEnum aEnum : enumArr) {
				if (sEnum.name().equals(aEnum.getApiServer().name()) && !aEnum.isPrivateApi() && !aEnum.isBackendApi() && !aEnum.isWebApi()) {
					retMap.put(aEnum.getCode(), aEnum.getMsg());
				}
			}
		}
		return retMap;
	}

	/**
	 * 根据API方法编号获取参数列表（供API测试工具使用）
	 * 
	 * @param methodCode
	 * @return
	 * @author Daniel
	 */
	public static Map<String, String> getApiParamMapByMethod(String methodCode) {
		ApiMethodEnum mEnum = ApiMethodEnum.getApiMethodEnum(methodCode);
		if (mEnum != null) {
			return mEnum.getApiParams();
		}
		return null;
	}

	public static ApiMethodEnum getApiMethodEnum(String methodCode) {
		return getApiMethodEnumMap().get(methodCode);
	}

	/**
	 * 根据API方法编号获取所属服务器（供API总控制中心分发使用）
	 * 
	 * @param methodCode
	 * @return
	 * @author Daniel
	 */
	public static ApiServerEnum getApiServerEnum(String methodCode) {
		ApiMethodEnum aEnum = getApiMethodEnumMap().get(methodCode);
		if (aEnum != null) {
			return aEnum.getApiServer();
		}
		return null;
	}

}
