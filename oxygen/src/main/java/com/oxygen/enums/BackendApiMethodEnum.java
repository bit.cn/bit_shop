package com.oxygen.enums;

import java.util.LinkedHashMap;
import java.util.Map;

import com.oxygen.interfaces.Api;

public enum BackendApiMethodEnum implements Api {

	/**
	 * 订单列表
	 */
	BACKEND_ORDERLIST("backend.orderList", "订单列表", ApiServerEnum.order,
			ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("order_type", "订单类型");
			paramMap.put("settlementStatus", "订单处理状态");
			paramMap.put("userId", "用户ID");
			paramMap.put("userName", "用户名");
			paramMap.put("transactionNo", "交易号");
			paramMap.put("productId", "商品ID");
			paramMap.put("create_datetime_start", "订单创建时间开始");
			paramMap.put("create_datetime_end", "订单创建时间结束");
			paramMap.put("status", "订单状态");
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			return paramMap;
		}
	},

	/**
	 * 订单详情
	 */
	BACKEND_ORDERDETAIL("backend.orderDetail", "订单详情", ApiServerEnum.order,
			ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transactionNo", "交易号");
			return paramMap;
		}
	},

	/**
	 * 修改订单状态
	 */
	BACKEND_UPDATEORDERSTATUS("backend.updateOrderStatus", "修改订单状态",
			ApiServerEnum.order, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transactionNo", "交易号");
			paramMap.put("status", "状态");
			return paramMap;
		}
	},

	/**
	 * 修改订单
	 */
	BACKEND_UPDATEORDER("backend.updateOrder", "修改订单", ApiServerEnum.order,
			ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transactionNo", "交易号");
			paramMap.put(
					"contacts",
					"联系人([{contactType:联系人类型(contact,visitor,take_ticket),contactName:姓名,contactPhone:电话,cardType:证件类,cardId:证件号码}])");
			paramMap.put("remark", "订单备注");
			paramMap.put("visitTime", "游玩日期");
			paramMap.put("checkingInTime", "入住时间");
			paramMap.put("checkOutTime", "离店时间");
			paramMap.put("expressCompany", "快递公司");
			paramMap.put("expressNO", "快递单号");
			paramMap.put("shipmentsTime", "发货时间");
			paramMap.put("status", "订单状态");
			paramMap.put("timeSchedulePordId", "档期ID");

			return paramMap;
		}
	},

	/**
	 * 根据类型查询订单常量列表
	 */
	BACKEND_COMMONSETTING_SELECTCOMMONSETTINGBYTYPE(
			"backend.commonSetting.selectCommonSettingByType", "根据类型查询订单常量列表",
			ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("type", "常量类型");
			return paramMap;
		}
	},

	/**
	 * 查询订单常量
	 */
	BACKEND_COMMONSETTING_SELECTCOMMONSETTINGBYKEY(
			"backend.commonSetting.selectCommonSettingByKey", "查询订单常量",
			ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("key", "常量键");
			return paramMap;
		}
	},

	/**
	 * 批量修改订单常量
	 */
	BACKEND_COMMONSETTING_BATHUPDATECOMMONSETTING(
			"backend.commonSetting.bathUpdateCommonSetting", "批量修改订单常量",
			ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("commonSettingList",
					"常量信息值([{id:常量Id,value:常量值,desc:常量描述}])");
			return paramMap;
		}
	},

	/**
	 * 修改订单常量
	 */
	BACKEND_COMMONSETTING_UPDATECOMMONSETTING(
			"backend.commonSetting.updateCommonSetting", "修改订单常量",
			ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "常量Id");
			paramMap.put("value", "常量值");
			paramMap.put("desc", "常量描述");
			return paramMap;
		}
	},

	/**
	 * 新增订单常量
	 */
	BACKEND_COMMONSETTING_INSERTORDERCOMMONSETTING(
			"backend.commonSetting.insertOrderCommonSetting", "新增订单常量",
			ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("settingType", "常量类型");
			paramMap.put("key", "常量键");
			paramMap.put("value", "常量值");
			paramMap.put("desc", "常量描述");
			return paramMap;
		}
	},
	/**
	 * 扣除积分和优惠券
	 */
	PAYMENT_OPERATIONPRODPOINTANDCOUPON("payment.operationProdPointAndCoupon",
			"扣除积分和优惠券", ApiServerEnum.payment, ContentTypeEnum.ORDERS, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transaction_no", "交易流水号");
			return paramMap;
		}
	},

	/**
	 * 扣除积分换游币
	 */
	PAYMENT_OPERATIONTOUR("payment.operationTour", "扣除积分换游币",
			ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("transaction_no", "交易流水号");
			return paramMap;
		}
	},

	// 换游币相关接口暴露
	/**
	 * 订单支付扣除换游币
	 */
	BACKEND_EXCURRENCY_PAYORDERDEDUCTIONEXCHANGECURRENCY(
			"backend.excurrency.payorderDeductionExchangeCurrency",
			"订单支付扣除换游币", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("customerId", "用户ID");
			paramMap.put("orderId", "订单ID");
			paramMap.put("orderType", "订单类型");
			paramMap.put("payMoney", "订单金额");
			return paramMap;
		}
	},

	/**
	 * 查询客户的换游币概要信息
	 */
	BACKEND_EXCURRENCY_GETCUSTOMEREXCHANGECURRENCYDETAIL(
			"backend.excurrency.getCustomerExchangeCurrencyDetail",
			"查询客户的换游币概要信息", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("customerId", "用户ID");
			return paramMap;
		}
	},

	/**
	 * 取消订单返还换游币
	 */
	BACKEND_EXCURRENCY_CANCELORDERRETURNEXCHANGECURRENCY(
			"backend.excurrency.cancelorderReturnExchangeCurrency",
			"取消订单返还换游币", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("customerId", "用户ID");
			paramMap.put("orderId", "订单ID");
			paramMap.put("payMoney", "订单金额");
			paramMap.put("checkingInTime", "入住时间");
			return paramMap;
		}
	},

	/**
	 * 修改过期换游币的状态
	 */
	BACKEND_EXCURRENCY_CHANGEEXPIREDEXCHANGECURRENCYSTATUS(
			"backend.excurrency.changeExpiredExchangeCurrencyStatus",
			"修改过期换游币的状态", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			return paramMap;
		}
	},

	/**
	 * 换游币充值
	 */
	BACKEND_EXCURRENCY_INSERTEXCHANGECURRENCY(
			"backend.excurrency.insertExchangeCurrency", "换游币充值",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("customerId", "用户ID");
			paramMap.put("mobilePhone", "用户手机号");
			paramMap.put("currencyResource", "换游币来源");
			paramMap.put("currencyTitle", "换游币标题");
			paramMap.put("currencyAmount", "换游币充值金额");
			paramMap.put("status", "换游币状态");
			paramMap.put("startTime", "有效期起始时间");
			paramMap.put("endTime", "有效期结束时间");
			paramMap.put("userId", "用户ID");
			return paramMap;
		}
	},

	/**
	 * 校验换游币
	 */
	BACKEND_EXCURRENCY_CHECKEXCHANGECURRENCY(
			"backend.excurrency.checkExchangeCurrency", "校验换游币",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("customerId", "用户ID");
			paramMap.put("payMoney", "订单金额");
			paramMap.put("checkingInTime", "入住时间");
			paramMap.put("checkOutTime", "退房时间");
			paramMap.put("orderId", "订单ID");
			paramMap.put("productId", "商品ID");
			paramMap.put("productType", "商品类型");
			paramMap.put("quantity", "商品数量");
			paramMap.put("createDatetime", "下单时间");
			paramMap.put("orderItems", "订单详情列表");
			return paramMap;
		}
	},

	/**
	 * 查询换游币日志信息
	 */
	BACKEND_EXCURRENCY_GETEXCHANGECURRENCYLOG(
			"backend.excurrency.getExchangeCurrencyLog", "查询换游币日志信息",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("customerId", "用户ID");
			paramMap.put("logType", "日志类型");
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			return paramMap;
		}
	},
	/**
	 * 门票定时任务
	 */
	BACKEND_ORDERTICKETTASK("backend.orderTicketTask", "门票定时任务",
			ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			return paramMap;
		}
	},

	/**
	 * 度假屋体验定时任务
	 */
	BACKEND_ORDEREXPERINGCEDTASK("backend.orderExperingcedTask", "度假屋体验定时任务",
			ApiServerEnum.payment, ContentTypeEnum.ORDERS, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			return paramMap;
		}
	},

	/**
	 * 物业公司用户注册
	 */
	USER_PROPERTY_PROPERTYREGISTER("backend.user.property.propertyRegister",
			"物业公司用户注册", ApiServerEnum.user, ContentTypeEnum.USER, false, false,
			false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("mobilePhone", "电话号码");
			paramMap.put("password", "密码");
			paramMap.put("isSuperuser", "是否是超级管理员");
			paramMap.put("propertyId", "物业公司ID");
			paramMap.put("belongCompany", "所属物业公司");
			paramMap.put("userType", "用户类型");
			paramMap.put("propertyNum", "物业编号");
			return paramMap;
		}
	},

	/**
	 * 修改物业帐号
	 */
	BACKEND_TRUSTEESHIP_UPDATEPROPERTY("backend.trusteeship.updateProperty",
			"修改物业帐号", ApiServerEnum.user, ContentTypeEnum.USER, false, false,
			false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("propertyNum", "物业编号");
			paramMap.put("propertyId", "物业ID");
			paramMap.put("belongCompany", "所属物业公司");
			paramMap.put("userId", "用户ID");
			paramMap.put("propertyMobile", "物业帐号");
			paramMap.put("propertyPassword", "物业密码");
			return paramMap;
		}
	},
	/**
	 * 新增物业子帐号
	 */
	BACKEND_TRUSTEESHIP_INSERTSUBPROPERTY(
			"backend.trusteeship.insertSubProperty", "新增物业子帐号",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("propertyNum", "物业编号");
			paramMap.put("propertyId", "物业ID");
			paramMap.put("userName", "用户名");
			paramMap.put("propertyMobile", "物业帐号");
			paramMap.put("propertyPassword", "物业密码");
			return paramMap;
		}
	},

	/**
	 * 删除物业子帐号
	 */
	BACKEND_DELETESUBPROPERTY("backend.deleteSubProperty", "删除物业子帐号",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("userId", "用户ID");
			return paramMap;
		}
	},

	/**
	 * 修改物业子帐号
	 */
	BACKEND_TRUSTEESHIP_UPDATESUBPROPERTY(
			"backend.trusteeship.updateSubProperty", "修改物业子帐号",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("propertyNum", "物业编号");
			paramMap.put("propertyId", "物业ID");
			paramMap.put("userName", "用户名");
			paramMap.put("userId", "用户ID");
			paramMap.put("propertyMobile", "物业帐号");
			paramMap.put("propertyPassword", "物业密码");
			return paramMap;
		}
	},

	/**
	 * 获取子管理员列表
	 */
	BACKEND_GETSUBPROPERTY("backend.getSubProperty", "获取子管理员列表",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("propertyNum", "物业编号");
			paramMap.put("propertyId", "物业ID");
			return paramMap;
		}
	},

	/**
	 * 根据物业ID获取管理员
	 */
	BACKEND_USER_PROPERTY_GETPROPERTY("backend.user.property.getProperty",
			"根据物业ID获取管理员", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("propertyNum", "物业编号");
			paramMap.put("propertyId", "物业ID");
			return paramMap;
		}
	},
	/***** 用户后台管理 ******************************************/
	/**
	 * 用户登录
	 */
	USER_LOGIN("user.login", "用户登录", ApiServerEnum.user, ContentTypeEnum.USER,
			false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("username", "用户名");
			paramMap.put("password", "密码");
			return paramMap;
		}
	},

	/**
	 * 用户注册
	 */
	USER_REGISTER("user.register", "用户注册", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("mobilePhone", "电话号码");
			paramMap.put("username", "用户名");
			paramMap.put("password", "密码");
			paramMap.put("realName", "真实姓名");
			paramMap.put("nickName", "昵称");
			paramMap.put("email", "电子邮件");
			paramMap.put("registSource", "来源");
			paramMap.put("isSuperuser", "是否是超级管理员");
			paramMap.put("userType", "用户类型");
			paramMap.put("userTypeName", "用户类型名称");
			paramMap.put("roleIds", "角色");
			paramMap.put("orgIds", "组织");
			return paramMap;
		}
	},

	/**
	 * 查询角色列表
	 */
	USER_GETROLES("user.getRoles", "查询角色列表", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			return paramMap;
		}
	},

	/**
	 * 新增用户组织关系
	 */
	USER_OPERATIONUSERRELATEORG("user.operationUserRelateOrg", "新增用户组织关系",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("orgId", "组织ID");
			paramMap.put("userId", "用户ID");
			return paramMap;
		}
	},

	/**
	 * 修改用户角色关系
	 */
	USER_UPDATEUSERRELATEROLE("user.updateUserRelateRole", "修改用户角色关系",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "用户角色关系Id");
			paramMap.put("roleId", "角色ID");
			paramMap.put("userId", "用户ID");
			return paramMap;
		}
	},

	/**
	 * 修改功能菜单
	 */
	USER_UPDATEFUNCTIONRELATEMENU("user.updateFunctionRelateMenu", "修改功能菜单",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "功能菜单Id");
			paramMap.put("menuId", "菜单ID");
			paramMap.put("funId", "功能Id");
			return paramMap;
		}
	},

	/**
	 * 删除组织架构
	 */
	USER_DELETEORGANIZATIONAL("user.deleteOrganizational", "删除组织架构",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "组织Id");
			return paramMap;
		}
	},

	/**
	 * 新增功能菜单关系
	 */
	USER_OPERATIONFUNCTIONRELATEMENU("user.operationFunctionRelateMenu",
			"新增功能菜单关系", ApiServerEnum.user, ContentTypeEnum.USER, false, false,
			false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("menuId", "菜单ID");
			paramMap.put("funId", "功能Id");
			return paramMap;
		}
	},

	/**
	 * 批量新增功能菜单关系
	 */
	USER_BATCHADDFUNCTIONRELATEMENU("user.batchAddFunctionRelateMenu",
			"批量新增功能菜单关系", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("menuId", "菜单ID");
			paramMap.put("funIds", "功能Id数组集合");
			return paramMap;
		}
	},

	/**
	 * 新增角色功能权限关系
	 */
	USER_OPERATIONPERMISSIONRELATEROLE("user.operationPermissionRelateRole",
			"新增角色功能权限关系", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("roleId", "角色ID");
			paramMap.put("funId", "功能Id");
			paramMap.put("menuId", "菜单Id");
			return paramMap;
		}
	},

	/**
	 * 批量新增菜单角色关联功能信息
	 */
	USER_BATCHSAVEPERMISSIONRELATEROLE("user.batchSavePermissionRelateRole",
			"批量新增菜单角色关联功能信息", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("roleId", "角色ID");
			paramMap.put("funIds", "功能Ids");
			paramMap.put("menuId", "菜单Id");
			return paramMap;
		}
	},

	/**
	 * 新增组织角色关系
	 */
	USER_OPERATIONORGRELATEROLE("user.operationOrgRelateRole", "新增组织角色关系",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("roleId", "角色ID");
			paramMap.put("orgId", "组织Id");
			return paramMap;
		}
	},

	/**
	 * 修改用户组织关系
	 */
	USER_UPDATEUSERRELATEORG("user.updateUserRelateOrg", "修改用户组织关系",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "用户组织关系Id");
			paramMap.put("roleId", "角色ID");
			paramMap.put("userId", "用户ID");
			return paramMap;
		}
	},

	/**
	 * 修改用户角色关系
	 */
	USER_UPDATEORGRELATEROLE("user.updateOrgRelateRole", "修改用户角色关系",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "用户角色关系Id");
			paramMap.put("roleId", "角色ID");
			paramMap.put("orgId", "组织Id");
			return paramMap;
		}
	},

	/**
	 * 修改组织架构
	 */
	USER_UPDATEORGANIZATIONAL("user.updateOrganizational", "修改组织架构",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "组织Id");
			paramMap.put("name", "组织名");
			paramMap.put("code", "组织码");
			paramMap.put("parentId", "父节点Id");
			paramMap.put("description", "描述");
			return paramMap;
		}
	},

	/**
	 * 新增组织架构
	 */
	USER_OPERATIONORGANIZATIONAL("user.operationOrganizational", "新增组织架构",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("name", "组织名");
			paramMap.put("code", "组织码");
			paramMap.put("parentId", "父节点Id");
			paramMap.put("description", "描述");
			return paramMap;
		}
	},

	/**
	 * 新增权限
	 */
	USER_OPERATIONPERMISSIONFUNCTION("user.operationPermissionFunction",
			"新增权限", ApiServerEnum.user, ContentTypeEnum.USER, false, false,
			false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("name", "权限名称");
			paramMap.put("code", "权限编号");
			paramMap.put("onclick", "操作事件");
			return paramMap;
		}
	},

	/**
	 * 修改权限
	 */
	USER_UPDATEPERMISSIONFUNCTION("user.updatePermissionFunction", "修改权限",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "权限Id");
			paramMap.put("name", "权限名称");
			paramMap.put("code", "权限编号");
			paramMap.put("onclick", "操作事件");
			return paramMap;
		}
	},

	/**
	 * 修改角色功能权限关系
	 */
	USER_UPDATEPERMISSIONRELATEROLE("user.updatePermissionRelateRole",
			"修改角色功能权限关系", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "角色功能权限关系Id");
			paramMap.put("roleId", "角色ID");
			paramMap.put("funId", "功能Id");
			paramMap.put("menuId", "菜单Id");
			return paramMap;
		}
	},

	/**
	 * 新增用户角色关系
	 */
	USER_OPERATIONUSERRELATEROLE("user.operationUserRelateRole", "新增用户角色关系",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("roleId", "角色ID");
			paramMap.put("userId", "用户ID");
			return paramMap;
		}
	},

	/**
	 * 查询组织列表
	 */
	USER_GETORGANIZATIONAL("user.getOrganizational", "查询组织列表",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			return paramMap;
		}
	},

	/**
	 * 根据组织ID得到组织的详情
	 */
	USER_GETORGBYID("user.getOrgById", "根据组织ID得到组织的详情", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "菜单ID");
			return paramMap;
		}
	},

	/**
	 * 删除角色功能权限关系
	 */
	USER_DELETEPERMISSIONRELATEROLE("user.deletePermissionRelateRole",
			"删除角色功能权限关系", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "角色功能权限关系Id");
			return paramMap;
		}
	},

	/**
	 * 删除用户角色关系
	 */
	USER_DELETEORGRELATEROLE("user.deleteOrgRelateRole", "删除用户角色关系",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "用户角色关系Id");
			return paramMap;
		}
	},

	/**
	 * 删除权限
	 */
	USER_DELETEPERMISSIONFUNCTION("user.deletePermissionFunction", "删除权限",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "权限Id");
			return paramMap;
		}
	},

	/**
	 * 删除用户角色关系
	 */
	USER_DELETEUSERRELATEROLE("user.deleteUserRelateRole", "删除用户角色关系",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "用户角色关系Id");
			return paramMap;
		}
	},

	/**
	 * 删除功能菜单
	 */
	USER_DELETEFUNCTIONRELATEMENU("user.deleteFunctionRelateMenu", "删除功能菜单",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "功能菜单Id");
			return paramMap;
		}
	},

	/**
	 * 删除用户组织关系
	 */
	USER_DELETEUSERRELATEORG("user.deleteUserRelateOrg", "删除用户组织关系",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "用户组织关系Id");
			return paramMap;
		}
	},

	/**
	 * 新增角色
	 */
	USER_OPERATIONROLES("user.operationRoles", "新增角色", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("name", "角色名称");
			paramMap.put("code", "角色编码");
			paramMap.put("description", "描述");
			paramMap.put("menuIds", "菜单ID集合");
			paramMap.put("funIds", "功能ID集合");
			return paramMap;
		}
	},

	/**
	 * 查询菜单列表
	 */
	USER_GETMENU("user.getMenu", "查询菜单列表", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			return paramMap;
		}
	},

	/**
	 * 根据菜单ID得到菜单的详情
	 */
	USER_GETMENUBYID("user.getMenuById", "根据菜单ID得到菜单的详情", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "菜单ID");
			return paramMap;
		}
	},
	/**
	 * 修改菜单
	 */
	USER_UPDATEMENU("user.updateMenu", "修改菜单", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "菜单Id");
			paramMap.put("name", "菜单名称");
			paramMap.put("code", "菜单编码");
			paramMap.put("parentId", "菜单父节点Id");
			paramMap.put("description", "描述");
			return paramMap;
		}
	},

	/**
	 * 查询权限功能列表
	 */
	USER_GETFUNCTION("user.getFunction", "查询权限功能列表", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			return paramMap;
		}
	},

	/**
	 * 新增菜单
	 */
	USER_OPERATIONMENU("user.operationMenu", "新增菜单", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("name", "菜单名称");
			paramMap.put("code", "菜单编码");
			paramMap.put("parentId", "菜单父节点Id");
			paramMap.put("description", "描述");
			paramMap.put("funIds", "功能Id数组集合");
			return paramMap;
		}
	},

	/**
	 * 修改角色
	 */
	USER_UPDATEROLES("user.updateRoles", "修改角色", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "角色Id");
			paramMap.put("name", "角色名称");
			paramMap.put("code", "角色编码");
			paramMap.put("description", "描述");
			paramMap.put("menuIds", "菜单ID集合");
			paramMap.put("funIds", "功能ID集合");
			return paramMap;
		}
	},

	/**
	 * 删除角色
	 */
	USER_DELETEROLES("user.deleteRoles", "删除角色", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "角色Id");
			return paramMap;
		}
	},

	/**
	 * 删除菜单
	 */
	USER_DELETEMENU("user.deleteMenu", "删除菜单", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "菜单Id");
			return paramMap;
		}
	},

	/**
	 * 查询用户列表
	 */
	USER_QUERYUSERLIST("user.queryUserList", "查询用户列表", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("username", "用户名");
			paramMap.put("realName", "真实姓名");
			paramMap.put("nickName", "昵称");
			paramMap.put("email", "电子邮件");
			paramMap.put("userType", "用户类型");
			paramMap.put("mobilePhone", "电话号码");
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			return paramMap;
		}
	},
	/**
	 * 查询用户详情
	 */
	USER_QUERYUSERBYID("user.queryUserById", "查询用户详情", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "用户ID");
			return paramMap;
		}
	},

	/**
	 * 用户修改
	 */
	USER_UPDATEUSERS("user.updateUsers", "用户修改", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "用户ID");
			paramMap.put("username", "用户名");
			paramMap.put("password", "密码");
			paramMap.put("realName", "真实姓名");
			paramMap.put("nickName", "昵称");
			paramMap.put("email", "电子邮件");
			paramMap.put("isSuperuser", "是否是超级管理员");
			paramMap.put("userType", "用户类型");
			paramMap.put("userTypeName", "用户类型名称");
			paramMap.put("roleIds", "角色");
			paramMap.put("orgIds", "组织");
			return paramMap;
		}
	},

	/**
	 * 查询角色详情
	 */
	USER_QUERYROLESBYID("user.queryRolesById", "查询角色详情", ApiServerEnum.user,
			ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "角色ID");
			return paramMap;
		}
	},

	/**
	 * 查询权限详情
	 */
	USER_QUERYFUNCTIONBYID("user.queryFunctionById", "查询权限详情",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "权限ID");
			return paramMap;
		}
	},

	/**
	 * 根据父节点查询菜单列表
	 */
	USER_GETMENUBYPARENID("user.getMenuByParenId", "根据父节点查询菜单列表",
			ApiServerEnum.user, ContentTypeEnum.USER, false, false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("parentId", "菜单父节点Id");
			return paramMap;
		}
	},

	/**
	 * 根据菜单ID得到功能列表
	 */
	USER_GETFUNCTIONLISTBYMENUID("user.getFunctionListByMenuId",
			"根据菜单ID得到功能列表", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("menuId", "菜单Id");
			return paramMap;
		}
	},

	/**
	 * 根据角色查询菜单的关联功能列表
	 */
	USER_GETPERMISSIONRELATEROLELIST("user.getPermissionRelateRoleList",
			"根据角色查询菜单的关联功能列表", ApiServerEnum.user, ContentTypeEnum.USER, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("roleId", "角色ID");
			paramMap.put("menuId", "菜单Id");
			return paramMap;
		}
	},

	/**
	 * 删除接口客户端
	 */
	BACKEND_COMMON_DELETEINTERFACECLIENTS(
			"backend.common.deleteInterfaceClients", "删除接口客户端",
			ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "接口ID");
			return paramMap;
		}
	},

	/**
	 * 新增接口客户端
	 */
	BACKEND_COMMON_ADDINTERFACECLIENTS("backend.common.addInterfaceClients",
			"新增接口客户端", ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION,
			false, true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("clientName", "调用方名称");
			paramMap.put("clientKey", "客户端key");
			paramMap.put("description", "描述");
			paramMap.put("effectiveTime", "生效时间");
			paramMap.put("expirationTime", "过期时间");
			return paramMap;
		}
	},

	/**
	 * 更新接口客户端
	 */
	BACKEND_COMMON_UPDATEINTERFACECLIENTS(
			"backend.common.updateInterfaceClients", "更新接口客户端",
			ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "接口ID");
			paramMap.put("clientName", "调用方名称");
			paramMap.put("clientKey", "客户端key");
			paramMap.put("description", "描述");
			paramMap.put("effectiveTime", "生效时间");
			paramMap.put("expirationTime", "过期时间");
			return paramMap;
		}
	},

	/**
	 * 得到接口客户端列表
	 */
	BACKEND_COMMON_GETINTERFACECLIENTSLIST(
			"backend.common.getInterfaceClientsList", "得到接口客户端列表",
			ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			return paramMap;
		}
	},

	/**
	 * 新增接口
	 */
	BACKEND_COMMON_ADDINTERFACE("backend.common.addInterface", "新增接口",
			ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("name", "接口名称");
			paramMap.put("code", "接口代码");
			paramMap.put("summary", "接口描述");
			paramMap.put("createUser", "接口创建者");
			paramMap.put("updateUser", "接口更新者");
			return paramMap;
		}
	},

	/**
	 * 更新接口
	 */
	BACKEND_COMMON_UPDATEINTERFACE("backend.common.updateInterface", "更新接口",
			ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "接口ID");
			paramMap.put("name", "接口名称");
			paramMap.put("code", "接口代码");
			paramMap.put("summary", "接口描述");
			paramMap.put("createUser", "接口创建者");
			paramMap.put("updateUser", "接口更新者");
			return paramMap;
		}
	},

	/**
	 * 删除接口
	 */
	BACKEND_COMMON_DELETEINTERFACE("backend.common.deleteInterface", "删除接口",
			ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "接口ID");
			return paramMap;
		}
	},

	/**
	 * 得到接口列表
	 */
	BACKEND_COMMON_GETINTERFACELIST("backend.common.getInterfaceList",
			"得到接口列表", ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION,
			false, true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("page", "页码");
			paramMap.put("page_size", "每页多少条");
			return paramMap;
		}
	},

	/**
	 * 得到接口客户端详情
	 */
	BACKEND_COMMON_GETINTERFACECLIENTSINFO(
			"backend.common.getInterfaceClientsInfo", "得到接口客户端详情",
			ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "客户端ID");
			return paramMap;
		}
	},

	/**
	 * 更新客户端接口关系
	 */
	BACKEND_COMMON_UPDATECLIENTRELATEINTERFACE(
			"backend.common.updateClientRelateInterface", "更新客户端接口关系",
			ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "客户端Id");
			paramMap.put("clientName", "客户端名称");
			paramMap.put("clientKey", "客户端Key");
			paramMap.put("clientSecret", "客戶端secret");
			paramMap.put("description", "客户端描述");
			paramMap.put("interfaces", "接口ID,");
			return paramMap;
		}
	},

	/**
	 * 得到接口详情
	 */
	BACKEND_COMMON_GETINTERFACEINFO("backend.common.getInterfaceInfo",
			"得到接口详情", ApiServerEnum.commonservice, ContentTypeEnum.PERMISSION,
			false, true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("id", "接口Id");
			return paramMap;
		}
	},

	/**
	 * 周期性扫描更新活动状态
	 */
	ACTIVITY_UPDATEACTIVITYSTATUS("activity.updateActivityStatus",
			"周期性扫描更新活动状态", ApiServerEnum.activity, ContentTypeEnum.ACTIVITY, false,
			false, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			return paramMap;
		}
	},

	/**
	 * 推送消息给android应用的所有用户
	 */
	BACKEND_MIPUSH_SENDANDROIDTOALL("backend.mipush.sendAndroidToAll",
			"推送消息给android应用的所有用户", ApiServerEnum.commonservice, ContentTypeEnum.MIPUSH, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("app", "接收消息的APP名称(必填):weshare,jyg");
			paramMap.put("title", "消息标题(必填)");
			paramMap.put("description", "消息摘要(必填)");
			paramMap.put("extra", "JSON格式的自定义键值对;注：至多可以设置10个key-value键值对");
			paramMap.put("passThrough", "消息是否通过透传的方式送给app，1表示透传消息，0表示通知栏消息(默认)");
			paramMap.put("data", "数据参数");
			return paramMap;
		}
	},
	/**
	 * 推送消息给android应用的单个用户
	 */
	BACKEND_MIPUSH_SENDANDROIDTOUSER("backend.mipush.sendAndroidToUser",
			"推送消息给android应用的单个用户", ApiServerEnum.commonservice, ContentTypeEnum.MIPUSH, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("app", "接收消息的APP名称(必填):weshare,jyg");
			paramMap.put("title", "消息标题(必填)");
			paramMap.put("description", "消息摘要(必填)");
			paramMap.put("userAccount", "用户账号(必填)");
			paramMap.put("extra", "JSON格式的自定义键值对;注：至多可以设置10个key-value键值对");
			paramMap.put("passThrough", "消息是否通过透传的方式送给app，1表示透传消息，0表示通知栏消息(默认)");
			paramMap.put("data", "数据参数");
			return paramMap;
		}
	},
	/**
	 * 推送消息给android应用的多个用户
	 */
	BACKEND_MIPUSH_SENDANDROIDTOUSERS("backend.mipush.sendAndroidToUsers",
			"推送消息给android应用的多个用户", ApiServerEnum.commonservice, ContentTypeEnum.MIPUSH, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("app", "接收消息的APP名称(必填):weshare,jyg");
			paramMap.put("title", "消息标题(必填)");
			paramMap.put("description", "消息摘要(必填)");
			paramMap.put("userAccountList", "List(String)格式的多个用户账号,元素的个数不能超过1000个(必填)");
			paramMap.put("extra", "JSON格式的自定义键值对;注：至多可以设置10个key-value键值对");
			paramMap.put("passThrough", "消息是否通过透传的方式送给app，1表示透传消息，0表示通知栏消息(默认)");
			paramMap.put("data", "数据参数");
			return paramMap;
		}
	},
	/**
	 * 推送消息给IOS应用的所有用户
	 */
	BACKEND_MIPUSH_SENDIOSTOALL("backend.mipush.sendIosToAll",
			"推送消息给IOS应用的所有用户", ApiServerEnum.commonservice, ContentTypeEnum.MIPUSH, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("app", "接收消息的APP名称(必填):weshare,jyg");
			paramMap.put("description", "消息摘要(必填)");
			paramMap.put("extra", "JSON格式的自定义键值对;注：至多可以设置10个key-value键值对");
			paramMap.put("badge", "通知数字角标");
			paramMap.put("soundURL", "自定义消息铃声url");
			return paramMap;
		}
	},
	/**
	 * 推送消息给IOS应用的单个用户
	 */
	BACKEND_MIPUSH_SENDIOSTOUSER("backend.mipush.sendIosToUser",
			"推送消息给IOS应用的单个用户", ApiServerEnum.commonservice, ContentTypeEnum.MIPUSH, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("app", "接收消息的APP名称(必填):weshare,jyg");
			paramMap.put("description", "消息摘要(必填)");
			paramMap.put("userAccount", "用户账号(必填)");
			paramMap.put("extra", "JSON格式的自定义键值对;注：至多可以设置10个key-value键值对");
			paramMap.put("badge", "通知数字角标");
			paramMap.put("soundURL", "自定义消息铃声url");
			return paramMap;
		}
	},
	/**
	 * 推送消息给IOS应用的多个用户
	 */
	BACKEND_MIPUSH_SENDIOSTOUSERS("backend.mipush.sendIosToUsers",
			"推送消息给IOS应用的多个用户", ApiServerEnum.commonservice, ContentTypeEnum.MIPUSH, false,
			true, false) {
		@Override
		public Map<String, String> getApiParams() {
			Map<String, String> paramMap = new LinkedHashMap<String, String>();
			paramMap.put("app", "接收消息的APP名称(必填):weshare,jyg");
			paramMap.put("description", "消息摘要(必填)");
			paramMap.put("userAccountList", "List(String)格式的多个用户账号,元素的个数不能超过1000个(必填)");
			paramMap.put("extra", "JSON格式的自定义键值对;注：至多可以设置10个key-value键值对");
			paramMap.put("badge", "通知数字角标");
			paramMap.put("soundURL", "自定义消息铃声url");
			return paramMap;
		}
	},
	
	;
	private String code;
	private String msg;
	private ApiServerEnum apiServer;
	private ContentTypeEnum contentType;
	private boolean privateApi;
	private boolean backendApi;
	private boolean webApi;

	/**
	 * 
	 * @param code
	 *            编码
	 * @param msg
	 *            描述
	 * @param apiServer
	 *            所属服务器
	 * @param contentType
	 *            所属业务
	 * @param privateApi
	 *            是否内部私有API
	 * @param backendApi
	 *            是否后台API
	 * @param webApi
	 *            是否WEB API
	 * @author Daniel
	 */
	BackendApiMethodEnum(String code, String msg, ApiServerEnum apiServer,
			ContentTypeEnum contentType, boolean privateApi,
			boolean backendApi, boolean webApi) {
		this.code = code;
		this.msg = msg;
		this.apiServer = apiServer;
		this.contentType = contentType;
		this.privateApi = privateApi;
		this.backendApi = backendApi;
		this.webApi = webApi;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public ApiServerEnum getApiServer() {
		return apiServer;
	}

	public ContentTypeEnum getContentType() {
		return contentType;
	}

	public boolean isPrivateApi() {
		return privateApi;
	}

	public boolean isBackendApi() {
		return backendApi;
	}

	public boolean isWebApi() {
		return webApi;
	}

	/**
	 * 获取所有API方法枚举
	 * 
	 * @return
	 * @author Daniel
	 */
	public static Map<String, BackendApiMethodEnum> getApiMethodEnumMap() {
		Map<String, BackendApiMethodEnum> retMap = new LinkedHashMap<String, BackendApiMethodEnum>();
		BackendApiMethodEnum[] enumArr = BackendApiMethodEnum.values();
		for (BackendApiMethodEnum aEnum : enumArr) {
			retMap.put(aEnum.getCode(), aEnum);
		}
		return retMap;
	}

	/**
	 * 根据服务器获取API方法（供对外的API测试工具使用）
	 * 
	 * @param apiServer
	 * @return
	 * @author Daniel
	 */
	public static Map<String, String> getApiMethodMapByServer(String apiServer) {
		ApiServerEnum sEnum = ApiServerEnum.getApiServerEnum(apiServer);
		Map<String, String> retMap = new LinkedHashMap<String, String>();
		if (sEnum != null) {
			BackendApiMethodEnum[] enumArr = BackendApiMethodEnum.values();
			for (BackendApiMethodEnum aEnum : enumArr) {
				if (sEnum.name().equals(aEnum.getApiServer().name())
						&& !aEnum.isPrivateApi() && !aEnum.isBackendApi()
						&& !aEnum.isWebApi()) {
					retMap.put(aEnum.getCode(), aEnum.getMsg());
				}
			}
		}
		return retMap;
	}

	/**
	 * 根据API方法编号获取参数列表（供API测试工具使用）
	 * 
	 * @param methodCode
	 * @return
	 * @author Daniel
	 */
	public static Map<String, String> getApiParamMapByMethod(String methodCode) {
		BackendApiMethodEnum mEnum = BackendApiMethodEnum
				.getApiMethodEnum(methodCode);
		if (mEnum != null) {
			return mEnum.getApiParams();
		}
		return null;
	}

	public static BackendApiMethodEnum getApiMethodEnum(String methodCode) {
		return getApiMethodEnumMap().get(methodCode);
	}

	/**
	 * 根据API方法编号获取所属服务器（供API总控制中心分发使用）
	 * 
	 * @param methodCode
	 * @return
	 * @author Daniel
	 */
	public static ApiServerEnum getApiServerEnum(String methodCode) {
		BackendApiMethodEnum aEnum = getApiMethodEnumMap().get(methodCode);
		if (aEnum != null) {
			return aEnum.getApiServer();
		}
		return null;
	}

}
