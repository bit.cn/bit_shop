package com.oxygen.enums;

public enum BindPlatformEnum {
	/**
	 * BBS
	 */
	BBS(1),
	/**
	 * WEIXIN
	 */
	WEIXIN(2),
	/**
	 * QQ
	 */
	QQ(3),
	/**
	 * WEIBO
	 */
	WEIBO(4), ;

	int id;

	BindPlatformEnum(int _id) {
		this.id = _id;
	}

	public int getId() {
		return id;
	}

	public static BindPlatformEnum getBindPlatformEnum(int id) {
		BindPlatformEnum[] enumArr = BindPlatformEnum.values();
		for (BindPlatformEnum aEnum : enumArr) {
			if (aEnum.getId() == id) {
				return aEnum;
			}
		}
		return null;
	}
}
