package com.oxygen.enums;

/**
 * 消息业务类型
 * 
 * @author Daniel
 */
public enum MsgBizTypeEnum {
	/**
	 * 创建公会审核通过
	 */
	guildAudited(MsgDisplayFormatEnum.title_content_link, "guild", "系统通知", "您创建的《{0}》公会审核通过。"),
	/**
	 * 创建公会审核不通过
	 */
	guildRejected(MsgDisplayFormatEnum.title_content, null, "系统通知", "您创建的《{0}》公会审核未通过。"),
	/**
	 * 公会被封
	 */
	guildLocked(MsgDisplayFormatEnum.title_content_link, "guild_serviceterm", "系统通知", "您所在的《{0}》公会被封。"),
	/**
	 * 公会解封
	 */
	guildUnlock(MsgDisplayFormatEnum.title_content_link, "guild", "系统通知", "您所在的《{0}》公会已解封。"),
	/**
	 * 公会解散
	 */
	guildDismissed(MsgDisplayFormatEnum.title_content, null, "公会通知", "您所在的《{0}》公会已解散。"),
	/**
	 * 公会招募审核通过
	 */
	guildRecruitAudited(MsgDisplayFormatEnum.title_content_link, "guild_member", "系统通知", "您向虫虫平台申请的招募已审核通过，系统已发布了招募消息，快去看看公会新成员。"),
	/**
	 * 入会申请
	 */
	applyJoinGuild(MsgDisplayFormatEnum.title_content_link, "guild_member_apply", "新的成员", "{0}请求加入《{1}》公会。附加信息：{2}"),
	/**
	 * 加入公会审核通过
	 */
	joinGuild(MsgDisplayFormatEnum.title_content_link, "guild", "公会通知", "您已成功加入《{0}》公会。"),
	/**
	 * 邀请入会
	 */
	inviteJoinGuild(MsgDisplayFormatEnum.title_content_link, "my_guild_invite", "好友通知", "{0}邀请你加入《{1}》公会。"),
	/**
	 * 同意入会邀请
	 */
	agreeJoinGuildInvite(MsgDisplayFormatEnum.title_content_link, "guild", "好友通知", "{0}同意加入《{1}》公会。"),
	/**
	 * 拒绝入会邀请
	 */
	rejectJoinGuildInvite(MsgDisplayFormatEnum.title_content_link, "guild", "好友通知", "{0}拒绝加入《{1}》公会。"),
	/**
	 * 退会申请
	 */
	applyExitGuild(MsgDisplayFormatEnum.title_content_link, "guild_exit_apply", "退会审核", "{0}请求退出《{1}》公会。"),
	/**
	 * 退会审核通过
	 */
	exitGuild(MsgDisplayFormatEnum.title_content, null, "公会通知", "您已退出《{0}》公会。"),
	/**
	 * 踢出公会
	 */
	memberDeleted(MsgDisplayFormatEnum.title_content, null, "公会通知", "您被会长踢出《{0}》公会。"),
	/**
	 * 公会成员添加头衔
	 */
	memberAddTitle(MsgDisplayFormatEnum.title_content, null, "公会通知", "您已成为《{0}》公会{1}。"),
	/**
	 * 用户申请礼包
	 */
	userApplyGiftbag(MsgDisplayFormatEnum.title_content_link, "guild_giftbag_apply", "公会通知", "{0}向您申请《{1}》礼包，请尽快审核。"),
	/**
	 * 拒绝用户申请礼包
	 */
	userApplyGiftbagRejected(MsgDisplayFormatEnum.title_content, null, "公会通知", "您在《{0}》申请的《{1}》未通过，原因：{2}"),
	/**
	 * 用户领取礼包审核通过
	 */
	userTakeGiftbag(MsgDisplayFormatEnum.title_content_link, "my_giftbag", "公会通知", "您领取的《{0}》礼包可以使用啦。"),
	/**
	 * 公会申请礼包审核通过
	 */
	guildApplyGiftbag(MsgDisplayFormatEnum.title_content_link, "guild", "系统通知", "您向虫虫平台申请的《{0}》礼包审核通过啦。"),
	/**
	 * 公会申请礼包审核不通过
	 */
	guildApplyGiftbagRejected(MsgDisplayFormatEnum.title_content, null, "系统通知", "您向虫虫平台申请的《{0}》礼包审核未通过。"),
	/**
	 * 加好友申请
	 */
	applyAddFriend(MsgDisplayFormatEnum.title_content_link, "my_new_friend", "新的好友", "{0}请求添加您为好友。附加信息：{1}"),
	/**
	 * 加好友成功
	 */
	addFriend(MsgDisplayFormatEnum.title_content_link, "user", "好友通知", "你和{0}已成为好友，现在可以开始聊天了。"),

	/**
	 * 游戏详情
	 */
	packageDetail(null, "package", "{0}", "{0}"),
	/**
	 * 公会详情
	 */
	guildDetail(null, "guild", "{0}", "{0}"),
	/**
	 * 网页链接
	 */
	link(null, null, "{0}", "{0}"),

	;

	public static void main(String[] args) {
		MsgBizTypeEnum[] enumArr = MsgBizTypeEnum.values();
		for (MsgBizTypeEnum aEnum : enumArr) {
			System.out.println(aEnum.getDisplayFormat().name() + "---" + aEnum.getDetailPageName());
		}
	}

	private MsgDisplayFormatEnum displayFormat;
	private String detailPageName;
	private String titleTemplate;
	private String contentTemplate;

	private MsgBizTypeEnum(MsgDisplayFormatEnum displayFormat, String detailPageName, String titleTemplate, String contentTemplate) {
		this.displayFormat = displayFormat;
		this.detailPageName = detailPageName;
		this.titleTemplate = titleTemplate;
		this.contentTemplate = contentTemplate;
	}

	public MsgDisplayFormatEnum getDisplayFormat() {
		return displayFormat;
	}

	public String getDetailPageName() {
		return detailPageName;
	}

	public String getTitleTemplate() {
		return titleTemplate;
	}

	public String getContentTemplate() {
		return contentTemplate;
	}

}
