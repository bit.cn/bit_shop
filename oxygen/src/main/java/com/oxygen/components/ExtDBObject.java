package com.oxygen.components;

import com.mongodb.BasicDBObject;

public class ExtDBObject extends BasicDBObject {
	private static final long serialVersionUID = -2015724872149965023L;

	public ExtDBObject() {
	}

	public ExtDBObject(String key, Object value) {
		super(key, value);
	}

	/**
	 * 查询关键字
	 */
	private String key;
	private Object value;
	/**
	 * 排序属性
	 */
	private String orderName;
	private int sort;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

}
