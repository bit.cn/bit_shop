package com.oxygen.components;

import org.apache.commons.dbcp.BasicDataSource;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


@SuppressWarnings("restriction")
public class MyBasicDataSource extends BasicDataSource {
	@Override
	public void setPassword(String password) {
		try {
			super.setPassword(new String(decryptBASE64(password)));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void setUsername(String username) {
		try {
			super.setUsername(new String(decryptBASE64(username)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * BASE64解密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public byte[] decryptBASE64(String key) throws Exception {
		return (new BASE64Decoder()).decodeBuffer(key);
	}

	/**
	 * BASE64加密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public String encryptBASE64(byte[] key) throws Exception {
		return (new BASE64Encoder()).encodeBuffer(key);
	}

	public static void main(String[] args) {
		MyBasicDataSource source = new MyBasicDataSource();
		int i = 999999999;
		try {
			String password = "postgres";
			String username = "";
			String url = "jdbc:oracle:thin:@192.168.2.141:1515:apptest";
			String newPassword = source.encryptBASE64(password.getBytes());
			String newUserName = source.encryptBASE64(username.getBytes());
			String newurl = source.encryptBASE64(url.getBytes());
			System.out.println("加密：username:" + newUserName + "--------------password:" + newPassword);

			System.out.println("解密：username:" + new String(source.decryptBASE64(newUserName)) + "-----------password:" + new String(source.decryptBASE64(newPassword)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
