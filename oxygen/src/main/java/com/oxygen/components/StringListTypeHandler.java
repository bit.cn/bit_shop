package com.oxygen.components;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

public class StringListTypeHandler implements TypeHandler<List<String>> {

	@Override
	public List<String> getResult(ResultSet rs, String columnName) throws SQLException {
		String columnValue = rs.getString(columnName);
		return this.getStringArray(columnValue);
	}

	public List<String> getResult(ResultSet rs, int columnIndex) throws SQLException {
		String columnValue = rs.getString(columnIndex);
		return this.getStringArray(columnValue);
	}

	@Override
	public List<String> getResult(CallableStatement cs, int columnIndex) throws SQLException {
		String columnValue = cs.getString(columnIndex);
		return this.getStringArray(columnValue);
	}

	@Override
	public void setParameter(PreparedStatement ps, int i, List<String> parameter, JdbcType jdbcType) throws SQLException {
		if (parameter == null)
			ps.setNull(i, Types.VARCHAR);
		else {
			StringBuffer result = new StringBuffer();
			for (String value : parameter) {
				result.append(value).append(" ");
			}
			result.deleteCharAt(result.length() - 1);
			ps.setString(i, result.toString());
		}
	}

	private List<String> getStringArray(String columnValue) {
		if (columnValue == null) {
			return null;
		}
		return Arrays.asList(columnValue.split(" "));
	}
}
