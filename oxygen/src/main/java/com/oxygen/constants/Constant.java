package com.oxygen.constants;

public class Constant {

	public static final String API_KEY = "apiKey";
	public static final String ACCESS_TOKEN = "token";

	public static final String NEED_SIGN_SYSPARAM = "needSignSysParam";
	public static final String FUNCTION_CODE = "functionCode";
	public static final String REQUEST_IP = "requestIp";
	public static final String USER_AGENT = "userAgent";
	public static final String DEVICE_NO = "deviceNo";
	public static final String MODEL_NAME = "modelName";
	public static final String JUST_NEED_RESULT = "justNeedResult";
	public static final String OLD_VERSION = "oldVersion";
	public static final String SITE_ID = "site_id";
	public static final String DATA = "data";
	public static final String PAGE = "page";
	public static final String PAGE_SIZE = "page_size";
	public static final String PAGING_URL = "paging_url";
	public static final int DEFAULT_PAGE_SIZE = 10;
	public static final String AUTH_TOKEN = "authorization_token";
	public static final String DEFAULT_ACCESS_TOKEN = "innerToken";
	public static final int DEFAULT_PERCENT = 100;
	public static final Double MIN_UNIT = 0.01;

	public static final String IDFA = "idfa";
	public static final String SYS_VER = "sysVer";
	public static final String CERT_ID = "certId";
	public static final String CLIENT_CHANNEL_CODE = "clientChannelCode";
	public static final String CLIENT_BUNDLE_ID = "clientBundleId";
	public static final String CLIENT_VER = "clientVer";
	public static final String TRACK_OBJECT = "trackObject";
	public static final String  VERSION_CODE="versionCode";

	
	//后台管理的登录用户信息session key
	public static final String BACKEND_SESSION_USER_INFO = "backend_session_user_info";
}
