package com.oxygen.dto;

import java.math.BigDecimal;

public class CouponSummary {
  
    /**优惠券面值数*/
    private int couponTotal;
    /**优惠券面值*/
    private BigDecimal couponPrice;
    public int getCouponTotal() {
        return couponTotal;
    }
    public void setCouponTotal(int couponTotal) {
        this.couponTotal = couponTotal;
    }
    public BigDecimal getCouponPrice() {
        return couponPrice;
    }
    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }
    

}
