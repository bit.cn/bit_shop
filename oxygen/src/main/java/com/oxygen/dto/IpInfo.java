package com.oxygen.dto;

public class IpInfo {
	private Integer code;
	private IpData data;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public IpData getData() {
		return data;
	}

	public void setData(IpData data) {
		this.data = data;
	}
}
