package com.oxygen.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 搜索结果
 * 
 * @author daniel
 * 
 */
public class SearchResponse<E> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5670818113208443192L;

	/**
	 * 排序
	 */
	private Map<String, String> orderBy;

	/**
	 * 关键字
	 */
	private String q;

	/**
	 * 搜索条件
	 */
	private String criteria;

	private String criteriaFilter;

	/**
	 * 搜索耗时
	 */
	private int qtime;

	/**
	 * 头部信息
	 */
	private String headerInfo;

	/**
	 * 分页信息
	 */
	private Pagination<E> pagination;

	/**
	 * 文档集合
	 */
	private List<E> docs;

	/**
	 * 高亮信息
	 */
	private Map<String, Map<String, List<String>>> highLight;

	/**
	 * 分组信息
	 */
	private Map<String, Map<String, Long>> facetMap;

	public String getCriteriaFilter() {
		return criteriaFilter;
	}

	public void setCriteriaFilter(String criteriaFilter) {
		this.criteriaFilter = criteriaFilter;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public String getHeaderInfo() {
		headerInfo = "";
		if (this.getPagination() != null) {
			String pagingStr = "";
			long start = (this.getPagination().getPage() - 1) * this.getPagination().getRows();
			long stop = 0;
			if (this.getPagination().getTotalPages() == 1) {
				stop = this.getPagination().getNumFound();
			} else {
				if (this.getPagination().getPage() == this.getPagination().getTotalPages()) {
					stop = this.getPagination().getNumFound();
				} else {
					stop = this.getPagination().getPage() * this.getPagination().getRows();
				}
			}
			if (stop < (start + 1)) {
				pagingStr = " ";
			} else if ((start + 1) == stop) {
				pagingStr = "当前显示第  <b>" + stop + "</b>  条记录。";
			} else {
				pagingStr = "当前显示第  <b>" + (start + 1) + "-" + stop + "</b>  条记录。";
			}
			double elapsedSeconds = this.getQtime() / 1000.00;
			String keyword = this.getQ();
			if (keyword == null || "".equals(keyword)) {
				keyword = "*";
			}
			headerInfo = " 搜索:  <b><span id='qDiv'>" + keyword + "</span></b>  , 耗时:  <b>" + String.valueOf(elapsedSeconds) + "</b>  秒, 共找到:  <b>" + this.getPagination().getNumFound() + "</b>  条记录, "
					+ pagingStr;
		}

		return headerInfo;
	}

	// public void setHeaderInfo(String headerInfo) {
	// this.headerInfo = headerInfo;
	// }

	public Pagination<E> getPagination() {
		return pagination;
	}

	public void setPagination(Pagination<E> pb) {
		this.pagination = pb;
	}

	public List<E> getDocs() {
		return docs;
	}

	public void setDocs(List<E> docs) {
		this.docs = docs;
	}

	public void setHighLight(Map<String, Map<String, List<String>>> hl) {
		this.highLight = hl;
	}

	public Map<String, Map<String, Long>> getFacetMap() {
		return facetMap;
	}

	public void setFacetMap(Map<String, Map<String, Long>> facetMap) {
		this.facetMap = facetMap;
	}

	@Override
	public String toString() {
		return "SearchResult [criteria=" + criteria + ", QTime=" + qtime + ", headerInfo=" + headerInfo + ", docs=" + docs + ", pb=" + pagination + "]";
	}

	public Map<String, Map<String, List<String>>> getHighLight() {
		return highLight;
	}

	public void setHeaderInfo(String headerInfo) {
		this.headerInfo = headerInfo;
	}

	public Map<String, String> getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Map<String, String> order) {
		this.orderBy = order;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}

	public int getQtime() {
		return qtime;
	}

	public void setQtime(int qtime) {
		this.qtime = qtime;
	}

}
