package com.oxygen.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.oxygen.enums.Gender;
import com.oxygen.enums.MemberType;

public class CustomerInitResponse {

	/** 微信ID */
	private String openid;

	/** 真实姓名 */
	private String name;

	/** 微信头像 */
	private String headImgUrl;

	/** 昵称 */
	private String nickname;

	/** 会员类型 */
	private MemberType memberType = MemberType.NORMAL;

	/** 性别 */
	private Gender gender;

	/** 生日 */
	private Date birthday;
	
	/** 省区地址 */
	private String fullname;

	/** 详细地址 */
	private String customeraddress;

	/** 积分值 */
	private int point;

	/** 换游币数额 */
	private BigDecimal tourCoin;

	/** 现金 */
	private BigDecimal cash;

	/** 优惠券张数 */
	private int couponCount;

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		if (nickname != null) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < nickname.length(); i++) {
				char ch = nickname.charAt(i);
				if (!Character.isHighSurrogate(ch) && !Character.isLowSurrogate(ch)) {
					sb.append(ch);
				}
			}
			nickname = sb.toString();
		}
		this.nickname = nickname;
	}

	public MemberType getMemberType() {
		return memberType;
	}

	public void setMemberType(MemberType memberType) {
		this.memberType = memberType;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCustomeraddress() {
		return customeraddress;
	}

	public void setCustomeraddress(String customeraddress) {
		this.customeraddress = customeraddress;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}


	public BigDecimal getTourCoin() {
		return tourCoin;
	}

	public void setTourCoin(BigDecimal tourCoin) {
		this.tourCoin = tourCoin;
	}

	public BigDecimal getCash() {
		return cash;
	}

	public void setCash(BigDecimal cash) {
		this.cash = cash;
	}

	public int getCouponCount() {
		return couponCount;
	}

	public void setCouponCount(int couponCount) {
		this.couponCount = couponCount;
	}

}
