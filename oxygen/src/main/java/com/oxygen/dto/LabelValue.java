package com.oxygen.dto;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class LabelValue implements Serializable {

	private static final long serialVersionUID = 8963181708993150970L;

	private String value;

	private String label;

	private Boolean selected;

	public LabelValue() {

	}

	public LabelValue(String label, String value) {
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (StringUtils.isNotBlank(label) || StringUtils.isNotBlank(value)) {
			sb.append(value).append(" = ").append(StringUtils.isNotBlank(label) ? label : value);
		}
		return sb.toString();
	}

}
