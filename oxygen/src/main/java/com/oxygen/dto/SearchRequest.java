package com.oxygen.dto;

import java.io.Serializable;
import java.util.Map;

/**
 * 搜索请求
 * 
 * @author daniel
 */
public class SearchRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2244512550997869502L;

	private int p = 1;

	private int pageSize = 10;

	private String order;

	private String q;

	private String criteria;

	private String criteriaFilter;

	private String[] fields;
	private String[] highlightFields;
	private String[] facetFields;

	private Map<String, String> orderBy;

	public String getCriteriaFilter() {
		return criteriaFilter;
	}

	public void setCriteriaFilter(String criteriaFilter) {
		this.criteriaFilter = criteriaFilter;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	public String[] getHighlightFields() {
		return highlightFields;
	}

	public void setHighlightFields(String[] highlightFields) {
		this.highlightFields = highlightFields;
	}

	public String[] getFacetFields() {
		return facetFields;
	}

	public void setFacetFields(String[] facetFields) {
		this.facetFields = facetFields;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public int getP() {
		return p;
	}

	public void setP(int p) {
		this.p = p;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}

	public Map<String, String> getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Map<String, String> orderBy) {
		this.orderBy = orderBy;
	}

}
