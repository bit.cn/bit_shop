package com.oxygen.dto;

import java.io.Serializable;
import java.util.List;

public class UserDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4640109872129173497L;
	/** 客户ID */
	String customerId;

	/** 微信ID */
	private String openid;

	/** 真实姓名 */
	private String name;

	/** 微信头像 */
	private String headImgUrl;

	/** 昵称 */
	private String nickname;

	/** 会员类型 */
	private String memberType;

	/** 性别 */
	private String gender;

	/** 生日 */
	private String birthday;

	/** 省区地址 */
	private String fullname;

	/** 详细地址 */
	private String customeraddress;

	/** 积分值 */
	private double point;

	/** 换游币数额 */
	private double tourCoin;

	/** 现金 */
	private double cash;

	/** 优惠券张数 */
	private int couponCount;

	/** 邀请码 */
	private String invitationCode;

	/** 客户电话号码 */
	private String mobile;
	
    private List<CouponSummary> couponSummary;//优惠券明细


	public List<CouponSummary> getCouponSummary() {
		return couponSummary;
	}

	public void setCouponSummary(List<CouponSummary> couponSummary) {
		this.couponSummary = couponSummary;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCustomeraddress() {
		return customeraddress;
	}

	public void setCustomeraddress(String customeraddress) {
		this.customeraddress = customeraddress;
	}

	public double getPoint() {
		return point;
	}

	public void setPoint(double point) {
		this.point = point;
	}

	public double getTourCoin() {
		return tourCoin;
	}

	public void setTourCoin(double tourCoin) {
		this.tourCoin = tourCoin;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}

	public int getCouponCount() {
		return couponCount;
	}

	public void setCouponCount(int couponCount) {
		this.couponCount = couponCount;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
