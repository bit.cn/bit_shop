package com.oxygen.interfaces;

import java.util.Map;

public interface Api {
	Map<String, String> getApiParams();
}
