<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="运费管理">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<SCRIPT type="text/javascript">
function deleteSelect(id) {
		try{
			if(confirm("确定删除当前记录?")){
				$.blockUI({ message: "系统处理中，请等待...",css: { 
			        border: 'none', 
			        padding: '15px', 
			        backgroundColor: '#000', 
			        '-webkit-border-radius': '10px', 
			        '-moz-border-radius': '10px', 
			        opacity: .5, 
			        color: '#fff' 
			    }});
				var _url = "deleteById?id="+id;
				$.ajax({
				  type: 'POST',
				  url: _url,
				  data: {},
				  async:false,
				  success: function(data){
					  console.log("ajax.data="+data);
					  if(data){					  
						var _form = $("#form");
						_form.attr("action","selectList");
						_form.submit();
						  alert("删除成功！");
					  }
					  jQuery.unblockUI();
				  },
				  dataType: "text",
				  error:function(){
					  	jQuery.unblockUI();
						alert("加载失败，请联系管理员。");
				  }
				});
			}
		}catch(e){
			console.log("eee="+e);
		}
		return false;
	}
	//编辑
	function editSelect(id){
        var _url = "toEdit?id="+id;
        var _form = $("#form");
		_form.attr("action",_url);
		_form.submit();
	}
	
	//设置物流信息
	function setSelect(id){
        var _url = "${basepath}/manage/businessShippingDetail/selectList?businessShippingId="+id;
        var _form = $("#form");
		_form.attr("action",_url);
		_form.submit();
	}
	
	//设置默认快递
	function setDefaultShipping(){
		console.log("submitIDs...");
		//alert($("input[name=ids]:checked").val());
		if ($("input:checked").size() == 0) {
			alert("请先选择要操作的内容！");
			return false;
		}
		if ($("input:checked").size()>1) {
			alert("只可选择一个默认快递！");
			return false;
		}
		        var id=$("input[name=ids]:checked").val();
		        var businessId=$("#businessId")[0].value; ;
				var _url = "setDefaultShipping?id="+id;
				var forurl = "selectList?businessId="+businessId;
				$.ajax({
				  type: 'POST',
				  url: _url,
				  data: {},
				  async:false,
				  dataType: "text",
				  success: function(data){
					  console.log("ajax.data="+data);
					  if(null!=data&&""!=data.trim()){					  
						  alert("设置成功！");
						  //alert(data);
						window.location.href =forurl;
					  }
					  jQuery.unblockUI();
				  },
				  error:function(data){
					  	jQuery.unblockUI();
					  	//alert(data);
						alert("加载失败，请联系管理员。");
				  }
				});
	}
	
</SCRIPT>
	<form action="${systemSetting().center}/manage/businessShipping" method="post" theme="simple" id="form" name="form">
		<input type="hidden" value="${e.id!""}" id="id"/>
		<table class="table table-bordered">
			<tr>
				<td>快递名称</td>
				<td>
					<select name="shippingId" id="shippingId">
						<option>--请选择快递公司--</option>
						 <#list expressList as item>
								<option pid="0" <#if e.shippingId?? && item.id==e.shippingId>selected="selected" </#if> value="${item.id!""}"><font color='red'>${item.name!""}</font></option>
						 </#list>	
					</select>
				</td>
				<#--<td>省份名称</td>-->
					<#--<td>-->
						<#--<select name="businessId" id="businessId">-->
							<#--<option>--请选择省--</option>-->
							 <#--<#list areaList as item>-->
								<#--<option pid="0" <#if e.businessId?? && item.id==e.businessId>selected="selected" </#if> value="${item.id!""}"><font color='red'>${item.name!""}</font></option>-->
							<#--</#list>-->
						<#--</select>-->
					<#--</td>-->
			</tr>
			<tr>
				<td colspan="16">

					<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
						<i class="icon-search icon-white"></i> 查询
					</button>
						
					<a href="toAdd" class="btn btn-success">
						<i class="icon-plus-sign icon-white"></i> 添加
					</a>
						
					<button method="deletes" class="btn btn-danger" onclick="return submitIDs(this,'确定删除选择的记录?');">
						<i class="icon-remove-sign icon-white"></i> 删除
					</button>
					
					<a href="#" class="btn btn-danger" onclick="setDefaultShipping();">
						<i class="icon-remove-sign icon-white"></i> 设置默认快递
					</a>
						
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
                        <#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>

		<table class="table table-bordered table-hover">
			<tr style="background-color: #dff0d8">
				<th width="20px"><input type="checkbox" id="firstCheckbox" /></th>
				<th width="20px" style="display:none;"><input type="text" /></th>
				<th width="120px" style="display:none;">ID</th>
				<th width="60px;">快递名称</th>
				<th width="60px;">状态</th>
				<th width="60px;">是否默认快递</th>
				<th width="60px;">操作</th>
			</tr>

            <#list pager.list as item>
				<tr>
					<td><input type="checkbox" name="ids" value="${item.id!""}" /></td>
					<td style="display:none;"><input type="text" id="businessId" value="${item.businessId!""}" /></td>
			        <td style="display:none;" >${item.id!""}</td>
					<td>${item.shippingName!""}</td>
					<td>
						<#if item.enable??&&item.enable==1>
								可用
						<#elseif item.enable??&&item.enable==0>
								 不可用
						</#if>
					</td>
					<td>
						<#if item.isDefault??&&item.isDefault==1>
						     	默认快递
						<#elseif item.isDefault??&&item.isDefault==0>
								非默认快递
						</#if>
					</td>
					<td>
						<button class="btn btn-warning" onclick="return editSelect('${item.id}');">
                    		<i class="icon-edit icon-white"></i> 编辑
                		</button>
                		<button class="btn btn-warning" onclick="return setSelect('${item.id}');">
                    		<i class="icon-edit icon-white"></i>设置物流运费
                		</button>
                   		 <button method="deletes" class="btn btn-danger" onclick="return deleteSelect('${item.id}');">
                       		 <i class="icon-remove-sign icon-white"></i> 删除
                    	</button>
					</td>
				</tr>
            </#list>

			<tr>
				<td colspan="17" style="text-align: center;font-size: 12px;">
                    <#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
		
		<div class="alert alert-info" style="text-align: left;font-size: 14px;margin: 2px 0px;">
		</div>

	</form>
</@page.pageBase>