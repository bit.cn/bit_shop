<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="运费管理">
<style>
#insertOrUpdateMsg{
border: 0px solid #aaa;margin: 0px;position: fixed;top: 0;width: 100%;
background-color: #d1d1d1;display: none;height: 30px;z-index: 9999;font-size: 18px;color: red;
}
.btnCCC{
	background-image: url("../img/glyphicons-halflings-white.png");
	background-position: -288px 0;
}
</style>
	<form action="${basepath}/manage/businessShippingDetail" namespace="/manage" theme="simple" name="form" id="form" method="post">
		<table class="table table-bordered">
				<tr>
					<td colspan="2" style="text-align: center;">
					   <#if e.id??>
	                        <button method="update" class="btn btn-success">
	                            <i class="icon-ok icon-white"></i> 保存
	                        </button>
						<#else>
	                        <button method="insert" class="btn btn-success">
	                            <i class="icon-ok icon-white"></i> 新增
	                        </button>
						</#if>
					</td>
				</tr>
				<tr style="background-color: #dff0d8">
					<td colspan="2" style="background-color: #dff0d8;text-align: center;">
						<strong>物流运费管理 </strong>
					</td>
				</tr>
				<tr style="display: none;">
					<td>id</td>
					<td>
						<input type="hidden" value="${e.id!""}" name="id" label="id" />
						<input type="hidden" value="${e.businessShippingId!""}" name="businessShippingId" label="businessShippingId" />
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">快递公司</td>
					<td>
						<#if businessShipping??>
							${businessShipping.shippingName!""}
						</#if> 
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">省</td>
					<td style="text-align: left;">
					   <input type="hidden"  value="${e.provinceName!""}" name="provinceName" id="provinceName" />
						<select onchange="provinceChange(this)" name="provinceId" id="provinceId">
							<option>--请选择省--</option>
							 <#list areaList as item>
								<option pid="0" <#if e.provinceId?? && item.code==e.provinceId>selected="selected" </#if> value="${item.code!""}"><font color='red'>${item.name!""}</font></option>
							</#list>
						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">首重</td>
					<td style="text-align: left;">
						<input type="text"  value="${e.firstHeavy!""}" name="firstHeavy" id="firstHeavy" data-rule="首重:required;integer[+];firstHeavy;length[1~5];"<br>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">首重价格</td>
					<td style="text-align: left;">
						<input type="text"  value="${e.fristPrice!""}" name="fristPrice" id="fristPrice" data-rule="首重价格:required;integer[+0];fristPrice;length[1~45];"/><br>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">续重规格</td>
					<td style="text-align: left;">
						<input type="text"  value="${e.stepHeavy!""}" name="stepHeavy" id="stepHeavy" data-rule="续重规格:required;integer[+];stepHeavy;length[1~45];"/><br>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">续重单价</td>
					<td style="text-align: left;">
						<input type="text"  value="${e.stepPrice!""}" name="stepPrice" id="stepPrice" data-rule="续重单价:required;integer[+0];stepHeavy;length[1~45];"/><br>
					</td>
				</tr>
		</table>
	</form>
<script type="text/javascript">
	function provinceChange(who){
		$("#provinceName").val($(who).find("option:selected").text());
	}
	function shippingChange(who){
		$("#shippingName").val($(who).find("option:selected").text());
	}
</script>
</@page.pageBase>