<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="运费管理">
<style>
#insertOrUpdateMsg{
border: 0px solid #aaa;margin: 0px;position: fixed;top: 0;width: 100%;
background-color: #d1d1d1;display: none;height: 30px;z-index: 9999;font-size: 18px;color: red;
}
.btnCCC{
	background-image: url("../img/glyphicons-halflings-white.png");
	background-position: -288px 0;
}
</style>
	<form action="${systemSetting().center}/manage/businessShipping" namespace="/manage" theme="simple" name="form" id="form" method="post">
		<table class="table table-bordered">
			<tr>
				<td colspan="2" style="text-align: center;">
				   <#if e.id??>
                        <button method="update" class="btn btn-success">
                            <i class="icon-ok icon-white"></i> 保存
                        </button>
					<#else>
					
                        <button method="insert" class="btn btn-success" <#if expressList?size=0> disabled="disabled" </#if>>
                            <i class="icon-ok icon-white"></i> 新增
                        </button>
					</#if>
				</td>
			</tr>
			<tr style="background-color: #dff0d8">
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>物流运费管理 </strong>
				</td>
			</tr>
			<tr style="display: none;">
				<td>id</td>
				<td><input type="hidden" value="${e.id!""}" name="id" label="id" /></td>
			</tr>
				<tr>
					<td style="text-align: right;">快递公司</td>
					<td>
					 <input type="hidden"  value="${e.shippingName!""}" name="shippingName" id="shippingName" />
						<select onchange="shippingChange(this)" name="shippingId" id="shippingId">
							<option>--请选择快递公司--</option>
							 <#list expressList as item>
								<option pid="0" <#if e.shippingId?? && item.id==e.shippingId>selected="selected" </#if> value="${item.id!""}"><font color='red'>${item.name!""}</font></option>
							</#list>
						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">是否默认快递</td>
					<td style="text-align: left;">
						<input type="radio" disabled="disabled" <#if e.isDefault??&&e.isDefault==1>checked=checked</#if>  value="1" name="isDefault" id="enable" />是
						<input type="radio" disabled="disabled" <#if e.isDefault??&&e.isDefault==0>checked=checked</#if> value="0" name="isDefault" id="enable" />否
					    <span style="color:red;font-size:12px;">[设置默认快递请到列表页！而且只能有一个默认快递。]
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">快递顺序</td>
					<td style="text-align: left;">
						<input type="text"  value="${e.sortOrder!""}" name="sortOrder" id="sortOrder" data-rule="快递顺序:required;sortOrder;length[1~45];"/><br>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">是否可用</td>
					<td style="text-align: left;">
						<input type="radio" <#if e.enable??&&e.enable==1>checked=checked</#if> value="1" name="enable" id="enable" />是
						<input type="radio" <#if e.enable??&&e.enable==0>checked=checked</#if> value="0" name="enable" id="enable" />否<br>
					</td>
				</tr>
		</table>
	</form>
<script type="text/javascript">
	function provinceChange(who){
		$("#provinceName").val($(who).find("option:selected").text());
	}
	function shippingChange(who){
		$("#shippingName").val($(who).find("option:selected").text());
	}
</script>
</@page.pageBase>