<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="售后保障">
<form action="${systemSetting().center}/manage/serviceGuarantee" id="form" name="form" namespace="/manage" theme="simple" enctype="multipart/form-data" method="post">

	<div style="text-align: center;">
	           <input type="hidden" value="${serviceGuarantee.id!""}" id="id" name="id"/>
	           <input type="hidden" value="${serviceGuarantee.businessId!""}" id="businessId" name="businessId"/>
                <button method="update" class="btn btn-success">
                    <i class="icon-ok icon-white"></i> 保存
                </button>
 	</div>

		<div id="tabs-9">
				<textarea data-rule="售后保障;required;serviceGuaranteeUrl;" id="serviceGuaranteeUrl" name="serviceGuaranteeUrl" style="width:100%;height:500px;visibility:hidden;">${serviceGuarantee.serviceGuaranteeUrl!""}</textarea>
		</div>
  </form>
  <script>
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="serviceGuaranteeUrl"]', {
			allowFileManager : true
		});
		K('input[name=getHtml]').click(function(e) {
			alert(editor.html());
		});
		K('input[name=isEmpty]').click(function(e) {
			alert(editor.isEmpty());
		});
		K('input[name=getText]').click(function(e) {
			alert(editor.text());
		});
		K('input[name=selectedHtml]').click(function(e) {
			alert(editor.selectedHtml());
		});
		K('input[name=setHtml]').click(function(e) {
			editor.html('<h3>Hello KindEditor</h3>');
		});
		K('input[name=setText]').click(function(e) {
			editor.text('<h3>Hello KindEditor</h3>');
		});
		K('input[name=insertHtml]').click(function(e) {
			editor.insertHtml('<strong>插入HTML</strong>');
		});
		K('input[name=appendHtml]').click(function(e) {
			editor.appendHtml('<strong>添加HTML</strong>');
		});
		K('input[name=clear]').click(function(e) {
			editor.html('');
		});
	});
	
	function addTrFunc(){
		var cc = $("#firstTr").clone();
		$("#firstTr").after(cc);
		
		cc.find("a").show();
	}
	
	function removeThis(t){
		$(t).parent().parent().remove();
		return false;
	}
</script>

</@page.pageBase>