<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="订单管理">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<script type="text/javascript">
	$(function() {
		$("#treegrid").treegrid({"treeColumn":1});
		
		function c1(f) {
			$(":checkbox").each(function() {
				$(this).attr("checked", f);
			});
		}
		$("#firstCheckbox").click(function() {
			if ($(this).attr("checked")) {
				c1(true);
			} else {
				c1(false);
			}
		});
	});
	function deleteSelect() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定删除选择的记录?");
	}
	function updateInBlackList() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定将选择的记录拉入新闻黑名单吗?");
	}
	
	//导出Excel
	function exportExcel(){
		var _url = "${systemSetting().center}/manage/order/exportExcel";
		$("#htmlTable").val($("#treegrid").prop("outerHTML"));
		var _form = $("form");
		_form.attr("action",_url);
		_form.submit();
	}
</script>
	<form action="${systemSetting().center}/manage/order" method="post" theme="simple">
		<table class="table table-bordered">
			<tr>
				<td style="width: 8%;">订单编号</td>
				<td><input type="text" value="${e.id!""}" name="id" class="search-query input-small"/></td>
				<td style="width: 8%;">支付状态</td>
				<td>
					<#assign map = {'':'','n':'未支付','y':'完全支付'}>
                    <select id="paystatus" name="paystatus" class="search-query input-medium">
						<#list map?keys as key>
                            <option value="${key}" <#if e.paystatus?? && e.paystatus==key>selected="selected" </#if>>${map[key]}</option>
						</#list>
                    </select>
					</td>
				<td style="width: 8%;">时间范围</td>
				<td><input id="d4311" class="Wdate search-query input-small" type="text" name="startDate"
					value="${e.startDate!""}"
					onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'d4312\')||\'2100-10-01 01:00:00\'}'})"/>
					~ 
					<input id="d4312" class="Wdate search-query input-small" type="text" name="endDate"
					value="${e.endDate!""}"
					onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2100-10-01 24:00:00'})"/>
				</td>
			</tr>
			<tr>
				<td style="width: 8%;">订单状态</td>
				<td colspan="1">
					<#assign map = {'':'','init':'未审核','pass':'已审核','send':'已发货','sign':'已签收','cancel':'已取消','file':'已归档'}>
                    <select id="status" name="status" class="search-query input-medium">
						<#list map?keys as key>
                            <option value="${key}" <#if e.status?? && e.status==key>selected="selected" </#if>>${map[key]}</option>
						</#list>
                    </select></td>
                <td style="width: 8%;">用户账号</td>
				<td><input type="text"  value="${e.account!""}" name="account" class="search-query input-small"/></td>
                <td style="width: 8%;">订单金额</td>
				<td><input type="text" value="${e.amount!""}" name="amount" class="search-query input-small"/>
					~ 
					<input type="text" value="${e.amount!""}" name="amount" class="search-query input-small"/>
				</td>
			</tr>
			<tr>
				<td colspan="14">
						<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
							<i class="icon-search icon-white"></i> 查询
						</button>
						<button type="button" method="exportExcel" class="btn btn-primary" onclick="exportExcel(this)">
							<i class="icon-search icon-white"></i> 导出
						</button>
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
						<#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>
		<input id="htmlTable" name="htmlTable" type="hidden" value="" />
		<table id="treegrid" title="订单目录" class="table tree table-bordered" style="min-width:800px;min-height:250px">
			<tr style="background-color: #dff0d8">
				<th width="20" data-options="field:'id'" nowrap="nowrap"><input type="checkbox" id="firstCheckbox"/></th>
				<th width="100" data-options="field:'id'" nowrap="nowrap">订单号</th>
				<th data-options="field:'amount'" nowrap="nowrap">订单总金额</th>
				<th data-options="field:'ptotal'" nowrap="nowrap">商品总金额</th>
				<th data-options="field:'fee'" nowrap="nowrap">配送费</th>
				<th data-options="field:'createdate'" nowrap="nowrap">数量</th>
				<th data-options="field:'account'" nowrap="nowrap">会员</th>
                <!--<th>折扣</th> -->
				<th data-options="field:'createdate'" nowrap="nowrap">创建日期</th>
				<th data-options="field:'status'" nowrap="nowrap">订单状态</th>
				<th data-options="field:'paystatus'" nowrap="nowrap">支付状态</th>
				<th data-options="field:'orderType'" nowrap="nowrap">订单类型</th>
				<th width="100px">操作</th>
			</tr>
			<#list pager.list as item>
				<tr class="treegrid-${item.id} ${(item.parentId=="0")?string("","treegrid-parent-"+item.parentId)}">
					<td><input type="checkbox" name="ids"
						value="${item.id!""}" /></td>
					<td>
						${item.id!""}
						<#if item.lowStocks?? && item.lowStocks=="y"><font color="red">【缺货】</font></#if>
					</td>
					<td>${item.amount!""}
						<#if item.updateAmount?? && item.updateAmount=="y"><font color="red">【修】</font></#if>
					</td>
					<td>${item.ptotal!""}</td>
					<td>${item.fee!""}</td>
					<td align="center">${item.quantity!""}</td>
					<td><a target="_blank" href="${systemSetting().center}/manage/account/show?account=${item.account!""}">${item.account!""}</a></td>
<#--<%-- 					<td>${item.rebate!""}</td> --%>-->
					<td>${item.createdate!""}</td>
					<td>${item.statusStr!""}
						<#if item.status?? && item.status=="cancel">
							<img src="${systemSetting().staticSource}/static/frontend/v1/images/action_delete.gif">
						<#elseif  item.status?? && item.status=="file">
							<img src="${systemSetting().staticSource}/static/frontend/v1/images/action_check.gif">
						<#elseif  item.status?? && item.status=="init">
							<img src="${systemSetting().staticSource}/static/frontend/v1/images/action_add.gif">
						</#if>
					</td>
					<td>
						${item.paystatusStr!""}
						<#if item.paystatus?? && item.paystatus=="y">
							<img src="${systemSetting().staticSource}/static/frontend/v1/images/action_check.gif">
						<#elseif  item.paystatus?? && item.paystatus=="n">
							<img src="${systemSetting().staticSource}/static/frontend/v1/images/action_add.gif">
						</#if>
					</td>
					<td>
						${item.orderType!""}
					</td>
					<td><a target="_blank" href="toEdit?id=${item.id}">编辑</a>|
					<a target="_blank" href="toPrint?id=${item.id}">打印</a>
					</td>
				</tr>
			</#list>
			<tr>
				<td colspan="55" style="text-align: center;">
					<#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
		
		<div class="alert alert-info" style="text-align: left;font-size: 14px;margin: 2px 0px;">
			图标含义：<BR>
			<img alt="新增" src="${systemSetting().staticSource}/static/frontend/v1/images/action_add.gif">：未审核、未支付
			<img alt="已上架" src="${systemSetting().staticSource}/static/frontend/v1/images/action_check.gif">：已归档
			<img alt="已下架" src="${systemSetting().staticSource}/static/frontend/v1/images/action_delete.gif">：已取消
		</div>

	</form>
	
	
	<link rel="stylesheet" type="text/css" href="${systemSetting().staticSource}/jquery-treegrid/css/jquery.treegrid.css">
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.js"></script>
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.bootstrap3.js"></script>
</@page.pageBase>