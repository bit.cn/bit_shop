<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="商品品牌">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<SCRIPT type="text/javascript">
function deleteSelect(id) {
		try{
			if(confirm("确定删除当前记录?")){
				$.blockUI({ message: "系统处理中，请等待...",css: { 
			        border: 'none', 
			        padding: '15px', 
			        backgroundColor: '#000', 
			        '-webkit-border-radius': '10px', 
			        '-moz-border-radius': '10px', 
			        opacity: .5, 
			        color: '#fff' 
			    }});
				var _url = "deleteById?id="+id;
				$.ajax({
				  type: 'POST',
				  url: _url,
				  data: {},
				  async:false,
				  success: function(data){
					  console.log("ajax.data="+data);
					  if(data){					  
						var _form = $("#form");
						_form.attr("action","selectList");
						_form.submit();
						  alert("删除成功！");
					  }
					  jQuery.unblockUI();
				  },
				  dataType: "text",
				  error:function(){
					  	jQuery.unblockUI();
						alert("加载失败，请联系管理员。");
				  }
				});
			}
		}catch(e){
			console.log("eee="+e);
		}
		return false;
	}
	//编辑
	function editSelect(id){
        var _url = "toEdit?id="+id;
        var _form = $("#form");
		_form.attr("action",_url);
		_form.submit();
	}
</SCRIPT>
	<form action="${basepath}/manage/brand" method="post" theme="simple" id="form" name="form">
		<input type="hidden" value="${e.id!""}" id="id"/>
		<table class="table table-bordered">
			<tr>
				<td>品牌名称</td>
				<td><input type="text" value="${e.brandName!""}" class="input-medium search-query" name="brandName"/></td>
			</tr>
			<tr>
				<td colspan="16">

					<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
						<i class="icon-search icon-white"></i> 查询
					</button>
						
					<a href="toAdd" class="btn btn-success">
						<i class="icon-plus-sign icon-white"></i> 添加
					</a>
						
					<button method="deletes" class="btn btn-danger" onclick="return submitIDs(this,'确定删除选择的记录?');">
						<i class="icon-remove-sign icon-white"></i> 删除
					</button>
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
                        <#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>

		<table class="table table-bordered table-hover">
			<tr style="background-color: #dff0d8">
				<th width="20px"><input type="checkbox" id="firstCheckbox" /></th>
				<th width="120px">ID</th>
				<th width="60px;">品牌名称</th>
				<th width="60px;" hidden>品牌类型</th>
				<th width="60px;">操作</th>
			</tr>

            <#list pager.list as item>
				<tr>
					<td><input type="checkbox" name="ids" value="${item.id!""}" /></td>
					<td >${item.id!""}</td>
					<td>${item.brandName!""}</td>
					<td hidden>${item.brandType!""}</td>
					<td>
						<button class="btn btn-warning" onclick="return editSelect('${item.id}');">
                    		<i class="icon-edit icon-white"></i> 编辑
                		</button>
                   		 <button method="deletes" class="btn btn-danger" onclick="return deleteSelect('${item.id}');">
                       		 <i class="icon-remove-sign icon-white"></i> 删除
                    	</button>
					</td>
				</tr>
            </#list>

			<tr>
				<td colspan="17" style="text-align: center;font-size: 12px;">
                    <#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
		
		<div class="alert alert-info" style="text-align: left;font-size: 14px;margin: 2px 0px;">
		</div>

	</form>
</@page.pageBase>