<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="批量导入物流单号">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<script type="text/javascript">

	//导出Excel
	function importExcel(){
	var _form = $("form");
    var oInput = document.getElementById('importExcler').value;
   if(oInput==""){
          alert("请选择要上传的文件");
           return;
    }
		_form.submit();
	}

</script>
	<form action="${systemSetting().business}/manage/importExcle/importExcels" ENCTYPE="multipart/form-data" method="post" theme="simple" target="hidden_frame">
		<table class="table table-bordered">
			<tr>
				<td colspan="14">

					  <p><input type="file" name="importExcler" value="" id="importExcler" enctype="multipart/form-data"/></p>
					
						<button type="button" class="btn btn-primary" onclick="importExcel(); ">
							<i class="icon-search icon-white"></i> 导入
						</button>

				</td>
			</tr>
		</table>
        <iframe name='hidden_frame' id="hidden_frame" style='display:none'＞</iframe>

	</form>

	<link rel="stylesheet" type="text/css" href="${systemSetting().staticSource}/jquery-treegrid/css/jquery.treegrid.css">
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.js"></script>
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.bootstrap3.js"></script>
</@page.pageBase>