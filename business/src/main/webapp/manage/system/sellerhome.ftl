<#import "/manage/tpl/sellerPageBase.ftl" as sellerPage>
<#import "/manage/tpl/sellermenu.ftl" as menu />
<#import "/manage/tpl/sellerFoot.ftl" as foot/>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${systemSetting().staticSource}/common.seller/css/orderManager.css" rel="stylesheet" type="text/css">
<title>订单管理</title>
</head>
<body>
	<!--顶部导航-->
	<@sellerPage.sellerPageBase currentMenu="卖家中心">
	<!--左边菜单-->
	<div class="head_left">
		<p class="head_my">
			卖家中心
		</p>
		 <@menu.menu menus=userMenus topMenu=topMenu currentMenu=currentMenu/>
	</div>
</div>
	</div>
		<div class="div_style_od_5">
	    <ul class="ul_style_od_1">
		<li class="li_style_od_1">售后待处理中心</li>
		<li class="li_style_od_2">订单编号 <input type="text" class="input_style_od_1"/></li>
		<li class="li_style_od_3">订单金额 <input type="text" class="input_style_od_2"  /> &nbsp; -- <input type="text" class="input_style_od_2" /> </li>
		<li class="li_style_od_2">买家姓名 <input type="text" class="input_style_od_1" /></li>
		<li class="li_style_od_3">订单状态 <input type="text" class="input_style_od_3" /></li>
		<li class="li_style_od_2">商品名称 <input type="text" class="input_style_od_1" /></li>
		<li class="li_style_od_4">	<input type="submit" class="btn-style-01" value="搜索"/></li>
		</ul>
		<ul>
		<div>
		<ul class="ul_style_od_2">
		<li class="li_style_od_5"><p class="p_style_od_1">近三个月的订单</p></li>
		<li class="li_style_od_6">代付款</li>
		<li class="li_style_od_6">预款代付</li>
		<li class="li_style_od_6">尾款代付</li>
		<li class="li_style_od_6">代发货</li>
		<li class="li_style_od_6">已出库</li>
		<li class="li_style_od_6">已发货</li>
		<li class="li_style_od_6">已完成</li>
		<li class="li_style_od_6">已退单</li>
		<li class="li_style_od_6">取消的订单</li>
		<ul class="ul_style_od_4">
		<li class="li_style_od_7">三个月前的订单</li>
		</ul>
		</ul>
		
		<ul class="ul_style_od_3">
		<li class="li_style_od_8">订单信息</li>
		<li class="li_style_od_9">成交单价</li>
		<li class="li_style_od_10">数量</li>
		<li class="li_style_od_11">下单时间</li>
		<li class="li_style_od_12">付款时间</li>
		<li class="li_style_od_13">订单状态</li>
		<li class="li_style_od_14">操作</li>
			</ul>
	      
		</div>
	
	  </div>
	
</div>
   <div class="div_style_od_6">
			 <ul class="ul_style_od_7">
			 <li class="li_style_od_15">订单编号 : 8888888</li>
			 <li class="li_style_od_16">买家 : 小南瓜</li>
			 </ul>
			 <ul>
			 <li class="li_style_od_17"><img src="${systemSetting().manageHttp}/resource/common.seller/image/girl.png" /></li>
			 <li class="li_style_od_18">这个是等待定金支付的预定商品<br /><p class="p_style_od_2">尺寸:红色底座</p><p class="p_style_od_2">颜色分类 : 原色(银白)</p><p class="p_style_od_2">款(优质ver2)</p></li>
			 <li class="li_style_od_19">预售中</li>
			 <li class="li_style_od_20">100.00</li>
			 <li class="li_style_od_21">X 1</li>
			 <li class="li_style_od_22">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_23">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_24">等待付款</li>
			 <li class="li_style_od_25"><input type="submit" class="btn-style-02" value="查看订单"/><br /><input type="submit" class="btn-style-02" value="收到定金"/><br /><input type="submit" class="btn-style-02" value="取消订单"/></li>
			 </ul>
			 </div>
			 <div class="div_style_od_7">
			 <ul>
			 <li class="li_style_od_26">定金金额 : 100.00</li>
			 <li class="li_style_od_27">已支付 : 100.00</li>
			 <li class="li_style_od_28">待支付 : 440.00</li>
			 <li class="li_style_od_29">订单总价 : <span class="span_style_od_3">￥ 540.00</span></li>
			 </ul>
			 </div>
			 
			    <div class="div_style_od_6">
			 <ul class="ul_style_od_7">
			 <li class="li_style_od_15">订单编号 : 8888888</li>
			 <li class="li_style_od_16">买家 : 小南瓜</li>
			 </ul>
			 <ul>
			 <li class="li_style_od_17"><img src="${systemSetting().manageHttp}/resource/common.seller/image/girl.png" /></li>
			 <li class="li_style_od_18">这个是等待定金支付的预定商品<br /><p class="p_style_od_2">尺寸:红色底座</p><p class="p_style_od_2">颜色分类 : 原色(银白)</p><p class="p_style_od_2">款(优质ver2)</p></li>
			 <li class="li_style_od_19">预售中</li>
			 <li class="li_style_od_20">100.00</li>
			 <li class="li_style_od_21">X 1</li>
			 <li class="li_style_od_22">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_23">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_24">等待付款</li>
			 <li class="li_style_od_25"><input type="submit" class="btn-style-02" value="查看订单"/><br /><input type="submit" class="btn-style-02" value="收到定金"/><br /><input type="submit" class="btn-style-02" value="取消订单"/></li>
			 </ul>
			 </div>
			 <div class="div_style_od_7">
			 <ul>
			 <li class="li_style_od_26">定金金额 : 100.00</li>
			 <li class="li_style_od_27">已支付 : 100.00</li>
			 <li class="li_style_od_28">待支付 : 440.00</li>
			 <li class="li_style_od_29">订单总价 : <span class="span_style_od_3">￥ 540.00</span></li>
			 </ul>
			 </div>
			 
			    <div class="div_style_od_6">
			 <ul class="ul_style_od_7">
			 <li class="li_style_od_15">订单编号 : 8888888</li>
			 <li class="li_style_od_16">买家 : 小南瓜</li>
			 </ul>
			 <ul>
			 <li class="li_style_od_17"><img src="${systemSetting().manageHttp}/resource/common.seller/image/girl.png" /></li>
			 <li class="li_style_od_18">这个是等待定金支付的预定商品<br /><p class="p_style_od_2">尺寸:红色底座</p><p class="p_style_od_2">颜色分类 : 原色(银白)</p><p class="p_style_od_2">款(优质ver2)</p></li>
			 <li class="li_style_od_19">预售中</li>
			 <li class="li_style_od_20">100.00</li>
			 <li class="li_style_od_21">X 1</li>
			 <li class="li_style_od_22">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_23">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_24">等待付款</li>
			 <li class="li_style_od_25"><input type="submit" class="btn-style-02" value="查看订单"/><br /><input type="submit" class="btn-style-02" value="收到定金"/><br /><input type="submit" class="btn-style-02" value="取消订单"/></li>
			 </ul>
			 </div>
			 <div class="div_style_od_7">
			 <ul>
			 <li class="li_style_od_26">定金金额 : 100.00</li>
			 <li class="li_style_od_27">已支付 : 100.00</li>
			 <li class="li_style_od_28">待支付 : 440.00</li>
			 <li class="li_style_od_29">订单总价 : <span class="span_style_od_3">￥ 540.00</span></li>
			 </ul>
			 </div>
			 
			    <div class="div_style_od_6">
			 <ul class="ul_style_od_7">
			 <li class="li_style_od_15">订单编号 : 8888888</li>
			 <li class="li_style_od_16">买家 : 小南瓜</li>
			 </ul>
			 <ul>
			 <li class="li_style_od_17"><img src="${systemSetting().manageHttp}/resource/common.seller/image/girl.png" /></li>
			 <li class="li_style_od_18">这个是等待定金支付的预定商品<br /><p class="p_style_od_2">尺寸:红色底座</p><p class="p_style_od_2">颜色分类 : 原色(银白)</p><p class="p_style_od_2">款(优质ver2)</p></li>
			 <li class="li_style_od_19">预售中</li>
			 <li class="li_style_od_20">100.00</li>
			 <li class="li_style_od_21">X 1</li>
			 <li class="li_style_od_22">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_23">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_24">等待付款</li>
			 <li class="li_style_od_25"><input type="submit" class="btn-style-02" value="查看订单"/><br /><input type="submit" class="btn-style-02" value="收到定金"/><br /><input type="submit" class="btn-style-02" value="取消订单"/></li>
			 </ul>
			 </div>
			 <div class="div_style_od_7">
			 <ul>
			 <li class="li_style_od_26">定金金额 : 100.00</li>
			 <li class="li_style_od_27">已支付 : 100.00</li>
			 <li class="li_style_od_28">待支付 : 440.00</li>
			 <li class="li_style_od_29">订单总价 : <span class="span_style_od_3">￥ 540.00</span></li>
			 </ul>
			 </div>
			    <div class="div_style_od_6">
			 <ul class="ul_style_od_7">
			 <li class="li_style_od_15">订单编号 : 8888888</li>
			 <li class="li_style_od_16">买家 : 小南瓜</li>
			 </ul>
			 <ul>
			 <li class="li_style_od_17"><img src="${systemSetting().manageHttp}/resource/common.seller/image/girl.png" /></li>
			 <li class="li_style_od_18">这个是等待定金支付的预定商品<br /><p class="p_style_od_2">尺寸:红色底座</p><p class="p_style_od_2">颜色分类 : 原色(银白)</p><p class="p_style_od_2">款(优质ver2)</p></li>
			 <li class="li_style_od_19">预售中</li>
			 <li class="li_style_od_20">100.00</li>
			 <li class="li_style_od_21">X 1</li>
			 <li class="li_style_od_22">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_23">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_24">等待付款</li>
			 <li class="li_style_od_25"><input type="submit" class="btn-style-02" value="查看订单"/><br /><input type="submit" class="btn-style-02" value="收到定金"/><br /><input type="submit" class="btn-style-02" value="取消订单"/></li>
			 
			 </ul>
			 </div>
			 	    <div class="div_style_od_8">
			 <ul>
			 <li class="li_style_od_17"><img src="${systemSetting().manageHttp}/resource/common.seller/image/girl.png" /></li>
			 <li class="li_style_od_18">这个是等待定金支付的预定商品<br /><p class="p_style_od_2">尺寸:红色底座</p><p class="p_style_od_2">颜色分类 : 原色(银白)</p><p class="p_style_od_2">款(优质ver2)</p></li>
			 <li class="li_style_od_19">预售中</li>
			 <li class="li_style_od_20">100.00</li>
			 <li class="li_style_od_21">X 1</li>
			 <li class="li_style_od_22">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_23">2015.10.26<br />11:23:00</li>
			 <li class="li_style_od_24">等待付款</li>
			 <li class="li_style_od_25"><input type="submit" class="btn-style-02" value="查看订单"/><br /><input type="submit" class="btn-style-02" value="收到定金"/><br /><input type="submit" class="btn-style-02" value="取消订单"/></li>
			 
			 </ul>
			 </div>
			 <div class="div_style_od_7">
			 <ul>
			 <li class="li_style_od_26">定金金额 : 100.00</li>
			 <li class="li_style_od_27">已支付 : 100.00</li>
			 <li class="li_style_od_28">待支付 : 440.00</li>
			 <li class="li_style_od_29">订单总价 : <span class="span_style_od_3">￥ 540.00</span></li>
			 </ul>
			 
			 </div>
			 <div>
			 	  <ul class="ul_style_odd_2">
	    <li class="li_style_odd_1">
	      <input name="submit" type="submit" class="btn-style-odd-02" value="1"/>
	    </li>
	    <li class="li_style_odd_1"><input type="submit" class="btn-style-odd-04" value="2"/></li>
	  <li class="li_style_odd_1"><input type="submit" class="btn-style-odd-04" value="3"/></li>
	  	  <li class="li_style_odd_1"><input type="submit" class="btn-style-odd-04" value="…"/></li>
	  <li class="li_style_odd_1"><input type="submit" class="btn-style-odd-05" value=""/></li>
	  <li class="li_style_odd_1" ><input type="submit" class="btn-style-odd-04" value="显示更多"/></li>
	  </ul>
	</div>
	<!--商城卖家底部-->
	<div class="div_style_od_9">
	 <@foot.sellerFoot/>
    </div>
  <div class="footer_bottom">@2014 阿尼托 上海索罗游信息技术有限公司 anitoys.com 版权所有 沪ICP备11047967号-18</div>
  </div>
</body>
</html>
</@sellerPage.sellerPageBase>
