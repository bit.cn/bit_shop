<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="发送优惠券">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${basepath}/manage/coupon/css/style.css" rel="stylesheet" type="text/css">
<link href="${basepath}/manage/coupon/css/omt.css" rel="stylesheet" type="text/css">
<link href="${basepath}/manage/coupon/css/yxjUser.css" rel="stylesheet" type="text/css">
<link href="${basepath}/manage/coupon/css/send.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
       //得到会员的列表
	   function getAccountList(who){
	   	var accountId = $(who).val();
	   	$.getJSON("${systemSetting().center}/manage/coupon/getAccountList", {accountId: accountId}, function(data){
	      $("#customerList").html("");
	      $("#customerList").parent().show()
		  $.each(data, function(i,item){
			$("#customerList").append("<li ondblclick='getAccount(this)'>"+item.id+"</li>");
		  });
		});
	   }
	   
	   //选中会员
	   function getAccount(who){
	   	$("#accountId").val($(who).text());
	   	 $("#customerList").html("");
	   	 $("#customerList").parent().hide();
	   }
	 
	 


function fun(){
	 
            var userId = $("#accountId").val();
          
        $.ajax({
             type:'post',    
              dataType:'text',
            url:"${systemSetting().center}/manage/coupon/getUserInfo?userId="+userId,
            data:userId,
            success:function()
                {   
                    alert();
                },
    
          error:function(data)
               {
                  // alert("结果"+data);
                   return false;
               }
            });


	   }
	   function sendAccountCoupon(){
	   if(fun()==false){
	  
	   };
	   	$("#form").submit();
	   }
</script>
<form action="${systemSetting().center}/manage/coupon/sendCoupon" namespace="/manage" theme="simple" name="form" id="form" method="post">
<div>
	<p class="p_style_5">发送优惠券给用户</p>
	<table class="table_style_5">
	<tr>
	<td class="td_style_1">
		用户ID: &nbsp;&nbsp;&nbsp;<input id="accountId" data-rule="用户ID:required;" name="userId" class="input_t2" type="text" onblur="fun()" onkeydown="getAccountList(this)"/>
		<div style="margin-left:55px;width:180px;height:80px;background-color:rgb(243, 243, 243);overflow: auto;display:none;">
			<ul id="customerList">
			</ul>
		</div>
	</td>
	</tr>
	<tr>
	<td class="td_style_2" hidden>优惠券ID: <input name="couponId" type="text" value="${e.id!""}" /></td>
	</tr>
	<tr>
	<td class="td_style_2" hidden>优惠券编码: <input name="couponSn" type="text" value="${e.couponSn!""}" /></td>
	</tr>
	</table>
	</div>
	<p class="p_style_2"><input type="submit" class="btn-style-01" value="完成" onClick="sendAccountCoupon()"/></p>
</form>
</@page.pageBase>