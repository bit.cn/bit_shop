<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="优惠券管理">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
	<form action="${systemSetting().center}/manage/coupon" method="post" theme="simple" id="form" name="form">
		<input type="hidden" value="${e.id!""}" id="id"/>
		<table class="table table-bordered">
			<tr>
				<td>优惠券名称</td>
				<td><input type="text" value="${e.couponName!""}" class="input-medium search-query" name="couponName"/></td>
			</tr>
			<tr>
				<td colspan="16">
					<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
						<i class="icon-search icon-white"></i> 查询
					</button>
						
					<a href="toAdd" class="btn btn-success">
						<i class="icon-plus-sign icon-white"></i> 添加
					</a>
						
					<button method="deletes" class="btn btn-danger" onclick="return submitIDs(this,'确定删除选择的记录?');">
						<i class="icon-remove-sign icon-white"></i> 删除
					</button>
						
					<!--<button method="updateStatusN" class="btn btn-warning" onclick="return submitIDs(this,'执行该操作后,选择的记录将不会出现在门户上。确定要执行?');">
						<i class="icon-arrow-down icon-white"></i> 给买家分配优惠券
					</button>-->
						
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
                        <#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>

		<table class="table table-bordered table-hover">
			<tr style="background-color: #dff0d8">
				<th width="20px"><input type="checkbox" id="firstCheckbox" /></th>
				<th width="30px">优惠券ID</th>
				<th width="60px">优惠券名称</th>
				<th width="20px">种类</th>
				<th width="40px">面值</th>
				<th width="110px">使用数/总数</th>
				<th width="50px">启用状态</th>
				<th width="50px">派发状态</th>
				<th width="50px">审核状态</th>
				<th width="130px">有效期</th>
				<th width="50px;">操作</th>
			</tr>

            <#list pager.list as item>
				<tr>
					<td><input type="checkbox" name="ids" value="${item.id!""}" /></td>
					<td>${item.id!""}</td>
					<td>${item.couponName!""}</td>
					<td nowrap="nowrap">
						<#if item.couponType?? && item.couponType==1>抵价券</#if>
						<#if item.couponType?? && item.couponType==2>折价券</#if>
					</td>
					<td nowrap="nowrap">${item.couponValue!""}</td>
					<td>${item.useCount!""} / ${item.couponCount!""}</td>
					<td>
						<#if item.enableStatus?? && item.enableStatus==1>已启用</#if>
						<#if item.enableStatus?? && item.enableStatus==0>未启用</#if>
					</td>
					<td>
						<#if item.receiveStatus?? && item.receiveStatus==1>已派发</#if>
						<#if item.receiveStatus?? && item.receiveStatus==0>未派发</#if>
					
					</td>
					<td>
						<#if item.auditStatus?? && item.auditStatus==0>待审核</#if>
						<#if item.auditStatus?? && item.auditStatus==1>通过</#if>
						<#if item.auditStatus?? && item.auditStatus==2>未通过</#if>
					</td>
					<td>${item.startTime!""}----${item.endTime!""}</td>
					<td>
						<a href="toEdit?id=${item.id!""}">编辑</a>
						<!--<#if item.auditStatus?? && item.auditStatus==1>
						<a href="tosendCoupon?id=${item.id!""}">|分配</a>|
						</#if>
						<a href="toEdit?id=${item.id!""}">导出序列号</a>-->
					</td>
				</tr>
            </#list>

			<tr>
				<td colspan="17" style="text-align: center;font-size: 12px;">
                    <#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
	</form>
	

<SCRIPT type="text/javascript">
$(function(){
	selectDefaultCatalog();
});
function selectDefaultCatalog(){
	var _catalogID = $("#catalogID").val();
	if(_catalogID!='' && _catalogID>0){
		$("#catalogSelect").attr("value",_catalogID);
	}
}
</SCRIPT>
</@page.pageBase>