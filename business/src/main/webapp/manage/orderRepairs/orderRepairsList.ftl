<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="售后中心">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<script type="text/javascript">
	$(function() {
		$("#treegrid").treegrid({"treeColumn":1});
		
		function c1(f) {
			$(":checkbox").each(function() {
				$(this).attr("checked", f);
			});
		}
		$("#firstCheckbox").click(function() {
			if ($(this).attr("checked")) {
				c1(true);
			} else {
				c1(false);
			}
		});
	});
	function deleteSelect() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定删除选择的记录?");
	}
	function updateInBlackList() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定将选择的记录拉入新闻黑名单吗?");
	}
</script>
	<form action="${systemSetting().center}/manage/orderRepairs" method="post" theme="simple">
		<table class="table table-bordered">
			<tr>
			</tr>
			<tr>
				<td colspan="16">
					<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
						<i class="icon-search icon-white"></i> 查询
					</button>
						
					<a href="toAdd" class="btn btn-success">
						<i class="icon-plus-sign icon-white"></i> 添加
					</a>
						
					<button method="deletes" class="btn btn-danger" onclick="return submitIDs(this,'确定删除选择的记录?');">
						<i class="icon-remove-sign icon-white"></i> 删除
					</button>
						
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
                        <#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>
				
		<table id="treegrid" title="订单目录" class="table tree table-bordered" style="min-width:800px;min-height:250px">
			<tr style="background-color: #dff0d8">
				<th width="20" data-options="field:'id'" nowrap="nowrap"><input type="checkbox" id="firstCheckbox"/></th>
				<th width="100" data-options="field:'id'" nowrap="nowrap">订单编号</th>
				<th data-options="field:'amount'" nowrap="nowrap">用户编号</th>
				<th data-options="field:'amount'" nowrap="nowrap">客服类型</th>
				<th data-options="field:'ptotal'" nowrap="nowrap">物品数量</th>
				<th data-options="field:'fee'" nowrap="nowrap">问题描述</th>
				<th data-options="field:'createdate'" nowrap="nowrap">问题图片</th>
				<th data-options="field:'account'" nowrap="nowrap">售后状态</th>
				<th data-options="field:'createdate'" nowrap="nowrap">反馈结果</th>
				<th data-options="field:'status'" nowrap="nowrap">售后类型</th>
				<th data-options="field:'paystatus'" nowrap="nowrap">售后处理结果</th>
				<th width="100px">操作</th>
			</tr>
			<#list pager.list as item>
				<tr class="">
					<td>
						<input type="checkbox" name="ids" value="${item.id!""}" />
					</td>
					<td>
						${item.orderId!""}
					</td>
					<td>
						${item.userId!""}
					</td>
					<td>
						${item.repairType!""}
					</td>
					<td>
						${item.repairCount!""}
					</td>
					<td>
						${item.repairDescription!""}
					</td>
					<td>
						${item.repairImg!""}
					</td>
					<td>
						${item.status!""}
					</td>
					<td>
						${item.feedback!""}
					</td>
					<td>
						${item.salesType!""}
					</td>
					<td>
						${item.processingResults!""}
					</td>
					<td>	
						<a target="_blank" href="toEdit?id=${item.id}">编辑</a>
					</td>
				</tr>
			</#list>
			<tr>
				<td colspan="55" style="text-align: center;">
					<#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
	</form>
	
	
	<link rel="stylesheet" type="text/css" href="${systemSetting().staticSource}/jquery-treegrid/css/jquery.treegrid.css">
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.js"></script>
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.bootstrap3.js"></script>
</@page.pageBase>