<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="售后中心">
<style>
#insertOrUpdateMsg{
border: 0px solid #aaa;margin: 0px;position: fixed;top: 0;width: 100%;
background-color: #d1d1d1;display: none;height: 30px;z-index: 9999;font-size: 18px;color: red;
}
.btnCCC{
	background-image: url("../img/glyphicons-halflings-white.png");
	background-position: -288px 0;
}
</style>
<script>
</script>
	<form action="${systemSetting().center}/manage/orderRepairs/examineOrderRepairs" namespace="/manage" theme="simple" name="form" id="form" method="post">
		<input type="hidden" value="${e.id!""}" name="id" label="id" />
		<input type="hidden" value="${e.processingResults!""}" id="processingResults" name="processingResults" label="processingResults" />
		<table class="table table-bordered">
			<tr style="background-color: #dff0d8">
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>售后审核 </strong>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">售后类型</td>
				<td>
					<#assign map = {'1':'换货','2':'退款'}>
						<#list map?keys as key>
                            <#if e.repairType?? && e.repairType?string==key>${map[key]}</#if>
						</#list>
				</td>	
			</tr>
			<tr>
				<td style="text-align: right;">申请人信息</td>
				<td>
						${e.userInfo!""}
				</td>	
			</tr>
			<tr>
				<td style="text-align: right;">申请退单的理由</td>
				<td>
					${e.repairDescription!""}
				</td>	
			</tr>
			<tr>
				<td style="text-align: right;">问题截图</td>
				<td>
				<img src="${systemSetting().imageRootPath}/${e.repairImg!""}" border="0">
					
				</td>	
			</tr>
			<tr>
				<td style="text-align: right;">反馈结果</td>
				<td>
					<textarea name="feedback" cols="20" rows="5">${e.feedback!""}</textarea>
				</td>	
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;">
                        <button type="button"  method="examineOrderRepairs" class="btn btn-success" onclick="examineOrderRepairs('1')">
                            <i class="icon-ok icon-white"></i> 审核通过
                        </button>
                        <button type="button"  method="examineOrderRepairs" class="btn btn-success" onclick="examineOrderRepairs('0')">
                            <i class="icon-ok icon-white"></i> 拒绝
                        </button>
				</td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
		function examineOrderRepairs(processingResults){
			$("#processingResults").val(processingResults);
			$("form[theme=simple]").submit();
		}
	</script>
</@page.pageBase>