<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="栏目管理">
<div style="text-align: center; border: 0px solid #999;margin: auto;">
	<div style="text-align: center; border: 0px solid #999;
		margin: auto;">
		<form action="${systemSetting().center}/manage/dict" theme="simple" method="post">
				<table class="table table-bordered table-condensed">
					<tr>
						<td colspan="4">
						<table class="table table-bordered" >
							<tr class="warning" style="text-align: left;">
								<td colspan="2">
									<div>
										<font color="red">在父菜单下的所有子菜单全部勾选的情况下，是否级联删除父菜单：<input type="checkbox" id="deleteParent"></font><br>
										提示：点击菜单项，此处则能编辑该菜单项或增加顶级菜单或子菜单项。<br> 
										<input type="button" id="deleteMenus" value="删除选择的菜单" class="btn btn-danger"/>
										<font color="red">(默认只删除叶子菜单)</font>
										[<a id="expandOrCollapseAllBtn" href="#" title="展开/折叠全部资源" onclick="return false;">展开/折叠</a>]
										<span onclick="addChannel()"><input type="button" id="addChannel" value="+顶级栏目" class="btn btn-danger"/></span>
									</div>
								</td>
							</tr>
							<tr>
								<td style="width:20%;">
									<div>
										<div id="loadImg" style="text-align: center;">
											<img alt="菜单加载中......" src="${systemSetting().staticSource}/static/frontend/v1/images/loader.gif">资源加载中...
										</div>			
										<ul id="channelTree" style="display: none;" class="ztree"></ul>
									</div>
								</td>
								<td>
									<iframe src="" width="100%" id="iframeMenuEdit" height="800">
										点击菜单项，此处则能编辑该菜单项或增加顶级菜单或子菜单项。
									</iframe>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
		</form>
	</div>
</div>
<script>
$(function()
{
	var setting = 
	{
			check: {
				enable: true,
				title: "channelName",
                name:"channelName",
				dblClickExpand: false
			},callback: {
				onClick: function(e,treeId, treeNode) {
	                var zTree = $.fn.zTree.getZTreeObj("channelTree");
	                zTree.expandNode(treeNode);
	            },
				onMouseDown: function (event, treeId, treeNode) {
					if(treeNode!=null)
					{
						 var url = "toAddOrUpdate?id="+treeNode.id;
		                if(true){
		                    $("#iframeMenuEdit").attr("src",url);
		                    return;
		                }
		                $("#dfsfsf").val(treeNode.id);
		                document.form1.action = url;
		                document.form1.submit();
					}
	            }
			},
	        data:{
	            key:{
	                url:"_url"
	            }
	        }
	};
	
loadMenusTree();
//加载菜单树
function loadMenusTree(){
	$.ajax({
		    url:"${systemSetting().center}/manage/channel/getChannelTree?parentId=0",
					type:"post",
					dataType:"text",
					success:function(data, textStatus){
						var zNodes = eval('('+data+')');
						if(zNodes.length==0)
						{
							$("#loadImg").hide();
							$("#channelTree").html("<li id=\"channelTree_2\" class=\"level0\" tabindex=\"0\" hidefocus=\"true\" treenode=\"\">"+
							"<a id=\"channelTree_2_a\" class=\"level0\" treenode_a=\"\"  target=\"rightFrame\" style=\"\" title=\"栏目管理\">"+
							"<span id=\"channelTree_2_ico\" title=\"\" treenode_ico=\"\" class=\"button ico_close\" style=\"\"></span>"+
							"<span id=\"channelTree_2_span\">栏目</span>"+
							"</a></li>");
							$("#channelTree").show();
						}
						else
						{
							setting.data.key.title="channelName";
							setting.data.key.name="channelName";
							$.fn.zTree.init($("#channelTree"), setting, zNodes);
							$("#loadImg").hide();
							$("#channelTree").show();
						}
					},
					error:function(){
						alert("error");
					}
				});
			}
			
			
			//删除栏目
			$("#deleteMenus").click(function(){
				
				if(!confirm("确定删除选择的菜单项?")){
					return false;
				}
// 				alert("deleteMenus...");
				var ids = "";
				var treeObj = $.fn.zTree.getZTreeObj("channelTree");
				var nodes = treeObj.getCheckedNodes(true);
				if(nodes.length==0){
					return false;
				}
				for(var i=0;i<nodes.length;i++){
// 					alert(nodes[i].id);
					ids+=nodes[i].id+",";
				}
				
				$.ajax({
					url:"${systemSetting().center}/manage/channel/delete",
					type:"post",
					data:{ids:ids,deleteParent:$("#deleteParent").attr("checked")?"1":"-1"},
					dataType:"text",
					success:function(data){
// 						var zNodes = eval('('+data+')');
// 						$.fn.zTree.init($("#channelTree"), setting, zNodes);
						if(data==1){
							loadMenusTree();
						}else{
							alert("删除菜单失败！");
						}
					},
					error:function(){
						alert("删除菜单失败！");
					}
				});
			});
			
			
			
			
			//点击菜单项
			var expandAllFlg = true;
			function expandNode(e) {
				var zTree = $.fn.zTree.getZTreeObj("channelTree"),
				type = e.data.type,
				nodes = zTree.getSelectedNodes();

				if (type == "expandAll") {
					zTree.expandAll(true);
				} else if (type == "collapseAll") {
					zTree.expandAll(false);
				} else if (type == "expandOrCollapse") {
					zTree.expandAll(expandAllFlg);
					expandAllFlg = !expandAllFlg;
				} else {
					if (type.indexOf("All")<0 && nodes.length == 0) {
						alert("请先选择一个父节点");
					}
					var callbackFlag = $("#callbackTrigger").attr("checked");
					for (var i=0, l=nodes.length; i<l; i++) {
						zTree.setting.view.fontCss = {};
						if (type == "expand") {
							zTree.expandNode(nodes[i], true, null, null, callbackFlag);
						} else if (type == "collapse") {
							zTree.expandNode(nodes[i], false, null, null, callbackFlag);
						} else if (type == "toggle") {
							zTree.expandNode(nodes[i], null, null, null, callbackFlag);
						} else if (type == "expandSon") {
							zTree.expandNode(nodes[i], true, true, null, callbackFlag);
						} else if (type == "collapseSon") {
							zTree.expandNode(nodes[i], false, true, null, callbackFlag);
						}
					}
				}
			}
			$("#expandOrCollapseAllBtn").bind("click", {type:"expandOrCollapse"}, expandNode);
		});
		
		//添加字典项
		function addChannel(){
			 var url = "toAddOrUpdate";
		     $("#iframeMenuEdit").attr("src",url);
		}
</script>
</@page.pageBase>