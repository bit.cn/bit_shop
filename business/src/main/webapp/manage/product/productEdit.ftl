<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="商品管理">
<form action="${systemSetting().center}/manage/product" id="form" name="form" namespace="/manage" theme="simple" enctype="multipart/form-data" method="post">

    <span id="pifeSpan" class="input-group-addon" style="display:none">${systemSetting().imageRootPath}</span>
    <input type="hidden" value="${e.id!""}" id="productID"/>
    <input type="hidden" value="${e.catalogID!""}" id="catalogID"/>

    <div style="text-align: center;">
        <div id="updateMsg"><font color='red'>${updateMsg!""}</font></div>
        <#if e.id??>
            商品ID：<span class="badge badge-success">${e.id!""}</span>
            <#if e.activityID??>
                活动ID：<span class="badge badge-success">${e.activityID!""}</span>
            </#if>
            <button method="update" class="btn btn-success">
                <i class="icon-ok icon-white"></i> 保存
            </button>

            <#if e.status??&&e.status!=2>
                <button method="updateUpProduct?id=${e.id!""}" class="btn btn-success" onclick="return confirm(\"确定上架商品吗?\");">
                <i class="icon-ok icon-white"></i>上架
                </button>
            <#else>
                <button method="updateDownProduct?id=${e.id!""}" class="btn btn-success" onclick="return confirm(\"确定上架商品吗?\");">
                <i class="icon-ok icon-white"></i>下架
                </button>
            </#if>

            <a class="btn btn-info" target="_blank" href="${systemSetting().item}/product/${e.id!""}.html">
                <i class="icon-eye-open icon-white"></i> 查看</a>
            <#--<a target="_blank" href="${systemSetting().item}/freemarker/create?method=staticProductByID&id=${e.id!""}"-->
               <#--class="btn btn-warning">-->
                <#--<i class="icon-refresh icon-white"></i> 静态化</a>-->
        <#else>
            <button onclick="validate()" method="insert" class="btn btn-success" id="addNewProduct">
                <i class="icon-ok icon-white"></i> 新增
            </button>
        </#if>
    </div>

    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">基本信息</a></li>
            <li><a href="#tabs-2">商品图片</a></li>
            <li><a href="#tabs-3">商品属性</a></li>
            <li><a href="#tabs-4">商品参数</a></li>
            <li><a href="#tabs-5">商品规格</a></li>
            <li><a href="#tabs-6">antioys通用信息</a></li>
            <li><a href="#tabs-7">商品物流信息</a></li>
            <li><a href="#tabs-9">商品描述</a></li>
        </ul>
        <!--基本信息-->
        <div id="tabs-1">
            <input type="hidden" value="${e.id!""}" name="id" label="id" id="id"/>
            <div class="row form-horizontal" -role="form">

                <div class="form-group">
                    <label class="col-md-2 control-label">商品名称</label>
                    <div class="col-md-10">
                        <input type="text" value="${e.name!""}" name="name" 
                        	data-rule="商品名称;required;name;length[0~150];"
                               size="100" maxlength="200" style="width: 80%;"
                               id="name"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-4 control-label">anitoys全局分类</label>
                    <div class="col-md-8">
                        <select onchange="catalogChange(this)" name="catalogID" id="catalogSelect" data-rule="全局分类;required;">
                            <option></option>
                            <#list catalogs as item>
                                <option pid="0" value="${item.id!""}"><font color='red'>${item.name!""}</font></option>
                                <#if item.children??>
                                    <#list item.children as item>
                                        <option value="${item.id!""}">&nbsp;&nbsp;&nbsp;&nbsp;${item.name!""}</option>
                                    </#list>
                                </#if>
                            </#list>
                        </select>(请选择子类别)
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-4 control-label">所属品牌</label>
                    <div class="col-md-8">
                        <select name="brandId" id="brandSelect" data-rule="所属品牌;required;">
                            <option></option>
                            <#if brandList??>
                                <#list brandList as item>
                                    <option value="${item.id!""}"
                                            <#if e.brandId?? && e.brandId==item.id>selected="selected" </#if>><font color='red'>${item.brandName!""}</font></option>
                                </#list>
                            </#if>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">简介</label>
                    <div class="col-md-10">
							<textarea name="introduce" class="form-control" rows="3" id="introduce"
                                      data-rule="商品简介;required;introduce;length[4~500];">${e.introduce!""}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">主图</label>
                    <div class="col-md-10">
                        <input type="button" name="filemanager" value="浏览图片" class="btn btn-success"/>
                        <input type="text" value="${e.maxPicture!""}" name="maxPicture" type="text"
                               id="maxPicture" ccc="imagesInput" style="width: 600px;"
                               data-rule="大图;required;maxPicture;"/>
                        水印文字<input type="text" id="maxPictureWatermark" name="maxPictureWatermark"
                                   value="${e.maxPictureWatermark!""}"/>
                        <#if e.picture??>
                            <a target="_blank" href="${systemSetting().imageRootPath}${e.maxPictureWatermarkView!""}">
                                <img style="max-width: 50px;max-height: 50px;" alt=""
                                     src="${systemSetting().imageRootPath}${e.maxPictureWatermarkView!""}">(尺寸：260px x 341px)
                            </a>
                        </#if>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">小图</label>
                    <div class="col-md-10">
                        <input type="button" name="filemanager" value="浏览图片" class="btn btn-success"/>
                        <input type="text" value="${e.picture!""}" name="picture" type="text" id="picture"
                               ccc="imagesInput" style="width: 600px;"
                               data-rule="小图;required;picture;"/>
                        水印文字<input type="text" id="pictureWatermark" name="pictureWatermark"
                                   value="${e.pictureWatermark!""}"/>
                        <#if e.picture??>
                            <a target="_blank" href="${systemSetting().imageRootPath}${e.pictureWatermarkView!""}">
                                <img style="max-width: 50px;max-height: 50px;" alt=""
                                     src="${systemSetting().imageRootPath}${e.pictureWatermarkView!""}">(尺寸：169px x
                                104px)
                            </a>
                        </#if>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4 control-label">是否新品</label>
                    <div class="col-md-8">
                        <#assign map = {'n':'否','y':'是'}>
                        <select id="isnew" name="isnew" class="input-medium">
                            <#list map?keys as key>
                                <option value="${key}"
                                        <#if e.isnew?? && e.isnew==key>selected="selected" </#if>>${map[key]}</option>
                            </#list>
                        </select>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-8">
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-4 control-label">送积分</label>
                    <div class="col-md-8">
                        <input type="text" value="${e.score!""}" name="score" type="text" id="score" maxlength="20"
                               data-rule="积分;required;integer[+0];score;"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-4 control-label">适用年龄</label>
                    <div class="col-md-8">
                        <input type="text" value="${e.applicableAge!""}" name="applicableAge" type="text" id="applicableAge"
                               maxlength="20" data-rule="适用年龄;required;integer[+];applicableAge;"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-4 control-label">区域</label>
                    <div class="col-md-8">
                        <input type="text" value="${e.area!""}" name="area" type="text" id="area" maxlength="20"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-4 control-label">系列</label>
                    <div class="col-md-8">
                        <input type="text" value="${e.series!""}" name="series" type="text" id="series" maxlength="20"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-4 control-label">型号</label>
                    <div class="col-md-8">
                        <input type="text" value="${e.model!""}" name="model" type="text" id="model" maxlength="20"/>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">发售日期</label>
                    <div class="col-md-10">
                        <input id="saleatDate" class="Wdate search-query input-small" type="text" name="saleatDate"
                               style="height:40px;"
                               value="${e.saleatDate!""}" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                               data-rule="发售日期:required;"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">页面标题</label>
                    <div class="col-md-10">
                        <input type="text" value="${e.title!""}" name="title" type="text" class="form-control"
                               data-rule="商品标题;required;title;length[0~100];"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">设置关键字</label>
                    <div class="col-md-10">
                        <input type="text" value="${e.keywords!""}" name="keywords" type="text" class="form-control"
                        data-rule="设置关键字;required;keywords;length[0~100];"/>
                    </div>
                </div>
                <#if e.id??>
                    <div class="form-group">
                        <label class="col-md-2 control-label">其他信息</label>
                        <div class="col-md-10">
                            录入人：<a style="text-decoration: underline;" target="_blank"
                                   href="${basepath}/manage/user/show?account=${e.createAccount!""}">${e.createAccount!""}</a>
                            录入时间：${e.createtime!""}<br>
                            最后修改人：<a style="text-decoration: underline;" target="_blank"
                                     href="${basepath}/manage/user/show?account=${e.updateAccount!""}">${e.updateAccount!""}</a>
                            最后修改时间：${e.updatetime!""}
                        </div>
                    </div>
                </#if>
            </div> <!--end form-->
        </div>

        <!--商品图片-->
        <div id="tabs-2">
            <div>
                <h4>
                    <div class="alert alert-info">图片列表</div>
                </h4>
                <table class="table table-bordered" id="productimages">
                    <tr>
                        <td colspan="11">
                            <!--<input  onclick="addTrFunc();" value="添加" class="btn btn-warning" type="button"/>-->
                            <button method="deleteImageByImgPaths" class="btn btn-success"
                                    onclick="return deleteImageByImgPaths();">
                                <i class="icon-ok icon-white"></i>删除
                            </button>
                        </td>
                    </tr>
                    <tr style="background-color: #dff0d8">
                        <th width="20"><input type="checkbox" id="firstCheckbox"/></th>
                        <th>图片地址</th>
                    </tr>
                    <#if e.imagesList??>
                        <#list e.imagesList as item>
                            <tr>
                                <td><input type="checkbox" name="imagePaths"
                                           value="${item!""}"/></td>
                                <td>
                                    <a href="${systemSetting().imageRootPath}${item!""}" target="_blank">
                                        <img style="max-width: 100px;max-height: 100px;" alt=""
                                             data-rule="图片地址;required;"
                                             src="${systemSetting().imageRootPath}${item!""}">
                                    </a>
                                </td>
                            </tr>
                        </#list>
                    </#if>
                </table>
            </div>
            <br>
            <table class="table table-bordered">
                <tr style="background-color: #dff0d8">
                    <th>文件</th>
                </tr>
                <tr id="firstTr">
                    <td>
                        <#list [1..10] as item>
                            <div>
                                <input type="button" name="multiimage" value="浏览图片" class="btn btn-warning"/>
                                <input type="text" ccc="imagesInput" name="images" style="width: 80%;"/>
                            </div>
                        </#list>
                    </td>
                </tr>
            </table>
            <table class="table table-bordered">
                <tr style="background-color: #dff0d8">
                    <th>商品视频</th>
                </tr>
                <tr id="firstTr">
                    <td>
                        <div>
                            <input type="button" name="videomanager" value="浏览视频" class="btn btn-warning"/>
                            <input type="text" ccc="videoInput" name="videoUrl" style="width: 80%;"/>
                            <embed id="player" src="${systemSetting().imageRootPath}${e.videoUrl!""}" width="550"
                                   height="400" quality="high"/>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <!-- 商品属性 -->
        <div id="tabs-3">
            <table class="table table-bordered">
                <#if e.attrList?? && e.attrList?size gt 0>
                    <#list e.attrList as attr>
                        <tr>
                            <td nowrap="nowrap" style="text-align: right;">${attr.name!""}</td>
                            <td>
                                <select id="attrSelectIds_${attr_index}" name="attrSelectIds" data-rule="属性:required;">
                                    <option value="">--请选择--</option>
                                    <#list attr.attrList as item>
                                        <#>
                                        <option value="${item.id!""}"
                                                <#if attr.selectedID==item.id?eval>selected="selected" </#if>>${item.name!""}</option>
                                    </#list>
                                </select>
                            </td>
                        </tr>
                    </#list>
                </#if>
            </table>
        </div>

        <!-- 商品参数 -->
        <div id="tabs-4">
            <table class="table">
                <#if e.parametersList?? && e.parametersList?size gt 0 >
                    <#list e.parametersList as param>
                        <tr>
                            <th style="display: none;"><input type="hidden" value="${param.id!""}" name="parameterIds"/>
                            </th>
                            <th style="text-align: right;">${param.name!""}</th>
                            <th><input type="text" value="${param.parameterValue!""}" name="parameterNames"
                                       data-rule="参数值:required;"/></th>
                        </tr>
                    </#list>
                </#if>
            </table>
        </div>
        <!-- 商品规格 -->
        <div id="tabs-5">
            <div class="row">
                <table class="table">
                    <tr>
                        <th style="display: none;">id</th>
                        <th>规格一</th>
                        <th>规格二</th>
                        <th>规格库存数</th>
                        <th>价格</th>
                        <th>JANCODE</th>
                        <th>是否显示</th>
                        <th><span onClick="addSpec(this)"><input type="button" name="addspec" value="+"
                                                                 class="btn btn-warning"/></span></th>
                    </tr>
                    <#if e.specList?? && e.specList?size gt 0>
                        <#list e.specList as item>
                            <tr>
                                <td style="display: none;">
                                    <input type="hidden" value="${item_index}" name="i"/>
                                    <input type="hidden" value="${item.id!""}" name="specList[${item_index}].id"/>
                                </td>
                                <td>
                                    <input type="text" value="${e.specList[item_index].specSize!""}"
                                           name="specList[${item_index}].specSize" class="search-query input-small"
                                           data-rule="规格一:required;"/>
                                </td>
                                <td>
                                    <input type="text" value="${e.specList[item_index].specColor!""}"
                                           name="specList[${item_index}].specColor" class="search-query input-small"
                                           data-rule="规格二:required;"/>
                                </td>
                                <td>
                                    <input type="text" value="${e.specList[item_index].specStock!""}"
                                           name="specList[${item_index}].specStock" class="form-control n-valid"
                                           data-rule="库存数:required;integer[+];"/></td>
                                <td>
                                    <input type="text" value="${e.specList[item_index].specPrice!""}"
                                           name="specList[${item_index}].specPrice" class="form-control n-valid"
                                           data-rule="价格:required;double[+];"/></td>
                                <td>
                                    <input type="text" value="${e.specList[item_index].specCode!""}"
                                           name="specList[${item_index}].specCode" class="form-control n-valid"
                                           data-rule="JANCODE:required;"/></td>
                                <td>
                                    <#assign map = {'n':'不显示','y':'显示'}>
                                    <select id="e_spec_specStatus" name="specList[${item_index}].specStatus"
                                            class="search-query input-medium">
                                        <#list map?keys as key>
                                            <option value="${key}"
                                                    <#if item.specStatus?? && item.specStatus==key>selected="selected" </#if>>${map[key]}</option>
                                        </#list>
                                    </select>
                                </td>
                                <td>
                                    <a href="#" onclick="deleteSpec('${item.id!""}',this)" class="btn btn-primary"
                                       style="color:white;">__</a>
                                </td>
                            </tr>
                        </#list>
                    <#else>
                        <#list [1] as item>
                            <tr>
                                <td style="display: none;"><input type="hidden" value="${item_index}" name="i"/><input
                                        type="hidden" value="${e.id!""}" name="specList[${item_index}].id"/></td>
                                <td><input type="text" name="specList[${item_index}].specColor"
                                           class="search-query form-control n-valid" data-rule="规格一:required;"/></td>
                                <td><input type="text" name="specList[${item_index}].specSize"
                                           class="search-query form-control n-valid" data-rule="规格二:required;"/></td>
                                <td><input type="text" name="specList[${item_index}].specStock"
                                           class="search-query form-control n-valid" data-rule="库存数:required;integer[+];"/>
                                </td>
                                <td><input type="text" name="specList[${item_index}].specPrice"
                                           class="search-query form-control n-valid" data-rule="价格:length[0~7];required;double[+];"/>
                                </td>
                                <td><input type="text" name="specList[${item_index}].specCode"
                                           class="search-query form-control n-valid"
                                           data-rule="JANCODE:required;"/></td>
                                <td>
                                    <#assign map = {'n':'不显示','y':'显示'}>
                                    <select id="e_spec_specStatus" name="specList[${item_index}].specStatus"
                                            class="search-query input-medium">
                                        <#list map?keys as key>
                                            <option value="${key}">${map[key]}</option>
                                        </#list>
                                    </select>
                                </td>
                                <td>
                                    <a href="#" onclick="deleteSpec('',this)" class="btn btn-primary"
                                       style="color:white;">__</a>
                                </td>
                            </tr>
                        </#list>
                    </#if>
                </table>
            </div>
        </div>

        <!--antioys通用商品贩售信息-->
        <div id="tabs-6">
            <div class="row form-horizontal" -role="form">
                <#assign map = {'1':'现货','2':'预售'}>
                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">商品类型</label>
                        <div class="col-md-10">
                            <#if e.id??>
                                <input type="hidden" value="${e.productType!""}" id="productType" name="productType"/>
                                <select id="productType" name="productType" class="search-query input-medium"
                                        onchange="changeProductType(this)" disabled>
                                    <#list map?keys as key>
                                        <option value="${key}"
                                                <#if e.productType?? && e.productType?string("number")==key>selected="selected" </#if>>
                                        ${map[key]}
                                        </option>
                                    </#list>
                                </select>
                            <#else>
                                <select id="productType" name="productType" class="search-query input-medium"
                                        onchange="changeProductType(this)">
                                    <#list map?keys as key>
                                        <option value="${key}"
                                                <#if e.productType?? && e.productType?string("number")==key>selected="selected" </#if>>
                                        ${map[key]}
                                        </option>
                                    </#list>
                                </select>
                            </#if>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">限购数量</label>
                        <div class="col-md-10">
                            <input type="text" value="${e.productRestrictions!""}" name="productRestrictions" size="44"
                                   maxlength="44" style="width: 80%;"id="productRestrictions"
                                   data-rule="限购数量:required;integer[+0];"/>(0:表示不限购)
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">上架</label>
                        <div class="col-md-10">
                            <input type="checkbox" value="2" <#if e.status==2>checked="checked" </#if> name="status"
                                   size="44" maxlength="44" id="status"/>打钩表示允许销售，否则不允许销售
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">预售/购买 开始时间</label>
                        <div class="col-md-10">
                            <input id="sellTime" class="Wdate search-query input-small" type="text" name="sellTime"
                                   style="height:40px;"
                                   value="${e.sellTime!""}" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                                   data-rule="预售/购买 开始时间:required;"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">预售/购买 结束时间</label>
                        <div class="col-md-10">
                            <input id="sellEndTime" class="Wdate search-query input-small" type="text"
                                   name="sellEndTime" style="height:40px;"
                                   value="${e.sellEndTime!""}" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                                   data-rule="预售/购买 结束时间:required;"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">补款开始时间</label>
                        <div class="col-md-10">
                            <input id="replenishmentTime" class="Wdate search-query input-small" type="text"
                                   name="replenishmentTime" style="height:40px;"
                                   value="${e.replenishmentTime!""}"
                                   onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">补款结束时间</label>
                        <div class="col-md-10">
                            <input id="replenishmentEndTime" class="Wdate search-query input-small" type="text"
                                   name="replenishmentEndTime" style="height:40px;"
                                   value="${e.replenishmentEndTime!""}"
                                   onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">延迟补款时间</label>
                        <div class="col-md-10">
                            <input id="replenishmentdeplaytime" class="Wdate search-query input-small" type="text"
                                   name="replenishmentdeplaytime" style="height:40px;"
                                   value="${e.replenishmentdeplaytime!""}"
                                   onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">

                    <div class="form-group">
                        <label class="col-md-2 control-label">现价</label>
                        <div class="col-md-10">
                            <input type="text" value="${e.nowPrice!""}" name="nowPrice" id="nowPrice"
                                   data-rule="现价:length[0~7];required;double[+];"/>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">预售价格</label>
                        <div class="col-md-10">
                            <input type="text" value="${e.depositPrice!""}" name="depositPrice" size="10" maxlength="10"
                                   id="depositPrice"/>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="form-group">
                        <label class="col-md-2 control-label">JANCODE</label>
                        <div class="col-md-10">
                            <input type="text" value="${e.code!""}" name="code" size="44"
                                   maxlength="44" style="width: 80%;" id="code" data-rule="JANCODE:required;"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--商品物流信息-->
        <div id="tabs-7">
            <div class="row form-horizontal" -role="form">
                <div class="form-group">
                    <label class="col-md-2 control-label">物流重量（KG）</label>
                    <div class="col-md-10">
                        <input type="text" value="${e.productWeight!""}" name="productWeight"
                               data-rule="物流重量;required;name;length[0~44];integer[+0]" size="44" maxlength="44" style="width: 30%;"
                               id="productWeight"/>
                    </div>
                </div>
            </div>
        </div>
        <!--商品描述-->
        <div id="tabs-9">
            <textarea data-rule="商品描述;required;productHTML;" id="productHTML" name="productHTML"
                      style="width:100%;height:500px;visibility:hidden;">${e.productHTML!""}</textarea>
        </div>

    </div>
</form>
<script type="text/javascript" src="${systemSetting().staticSource}/static/plugins/kindeditor-4.1.7/plugins/jwplayer/jwplayer.js"></script>
<script type='text/javascript'>
    //非视频，不加载播放器
    if (document.getElementById('player') != null) {
        jwplayer('player').onReady(function () {
        });
        jwplayer('player').onPlay(function () {
        });
        //jwplayer('player').play(); //自动播放？
    }

    $(function () {
        $("#tabs").tabs({
            //event: "mouseover"
        });
        //alert($("#insertOrUpdateMsg").html());
//	if($("#insertOrUpdateMsg").html()!='' && $("#insertOrUpdateMsg").html().trim().length>0){
//		$("#insertOrUpdateMsg").slideDown(1000).delay(1500).slideUp(1000);
//	}

        selectDefaultCatalog();

        $("#removePife").click(function () {
            clearRootImagePath();
        });
    });

    //删除图片主路径
    function clearRootImagePath(picInput) {
        var _pifeSpan = $("#pifeSpan").text();
        var _imgVal = picInput.val();
        console.log("1===>_imgVal = " + _imgVal);
        //if(_imgVal && _imgVal.length>0 && _imgVal.indexOf(_pifeSpan)==0){
        //picInput.val(_imgVal.substring(_pifeSpan.length));
        console.log("2===>" + _imgVal.indexOf("/attached/"));
        picInput.val(_imgVal.substring(_imgVal.indexOf("/attached/")));

        //}
    }

    //批量删除商品图像
    function deleteImageByImgPaths() {
        if ($("input:checked").size() == 0) {
            alert("请选择要删除的图片！");
            return false;
        }
        return confirm("确定删除选择的图片吗?");
    }

    function selectDefaultCatalog() {
        var _catalogID = $("#catalogID").val();
        if (_catalogID != '' && _catalogID > 0) {
            //$("#catalogSelect").attr("value",_catalogID);
            $("#catalogSelect").val(_catalogID);
        }
    }

    function validate() {
        var _pid = $('#catalogSelect').find("option:selected").attr("pid");
        if (_pid == 0) {
            alert("不能选择大类!");
            selectDefaultCatalog();
            return false;
        }
    }

    function catalogChange(obj) {
        var _pid = $(obj).find("option:selected").attr("pid");
        if (_pid == 0) {
            alert("不能选择大类!");
            selectDefaultCatalog();
            return false;
        }
        var _productID = $("#productID").val();

        if (confirm("修改商品类别会清空该商品的属性和参数，确认要这样做吗？")) {
            $.blockUI({
                message: "正在切换商品目录，请稍候...", css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });

            //alert($(obj).val());
            if (_productID == '') {
                //alert(3);
                document.form.action = "toAdd?chanageCatalog=true&catalog=" + $(obj).val();
            } else {
                document.form.action = "updateProductCatalog?id=" + _productID + "&chanageCatalog=true&catalog=" + $(obj).val();
            }
            document.form.submit();
        } else {
            selectDefaultCatalog();
        }
    }
</script>

<script>
    var editor;
    KindEditor.ready(function (K) {
        editor = K.create('textarea[name="productHTML"]', {
            allowFileManager: true
        });
        K('input[name=getHtml]').click(function (e) {
            alert(editor.html());
        });
        K('input[name=isEmpty]').click(function (e) {
            alert(editor.isEmpty());
        });
        K('input[name=getText]').click(function (e) {
            alert(editor.text());
        });
        K('input[name=selectedHtml]').click(function (e) {
            alert(editor.selectedHtml());
        });
        K('input[name=setHtml]').click(function (e) {
            editor.html('<h3>Hello KindEditor</h3>');
        });
        K('input[name=setText]').click(function (e) {
            editor.text('<h3>Hello KindEditor</h3>');
        });
        K('input[name=insertHtml]').click(function (e) {
            editor.insertHtml('<strong>插入HTML</strong>');
        });
        K('input[name=appendHtml]').click(function (e) {
            editor.appendHtml('<strong>添加HTML</strong>');
        });
        K('input[name=clear]').click(function (e) {
            editor.html('');
        });
    });

    function addTrFunc() {
        var cc = $("#firstTr").clone();
        $("#firstTr").after(cc);

        cc.find("a").show();
    }

    function removeThis(t) {
        $(t).parent().parent().remove();
        return false;
    }
</script>

<script>
    KindEditor.ready(function (K) {

        var editor = K.editor({
            fileManagerJson: '${systemSetting().staticSource}/kindeditor-4.1.7/jsp/file_manager_json.jsp',
            allowFileManager: true
        });

        //批量上传图片
        K('input[name=multiimage]').click(function () {
            var imagesInputObj = $(this).parent().children("input[ccc=imagesInput]");
            editor.loadPlugin('multiimage', function () {
                editor.plugin.multiImageDialog({
                    clickFn: function (urlList) {
                        K.each(urlList, function (i, data) {
                            var imageurl = data.url.replace("${systemSetting().imageRootPath}", "");
                            $("#productimages").append
                            ('<tr><td><input type="checkbox" name="imagePaths" value="' + data.url + '"></td><td><a href="' + data.url + '" target="_blank"><img style="max-width: 100px;max-height: 100px;" alt="" src="' + data.url + '"><input type="hidden" ccc="imagesInput" name="images" style="width: auto; display: inline-block;" class="form-control" value="' + imageurl + '"></a></td></tr>');
                        });
                        editor.hideDialog();
                        clearRootImagePath(imagesInputObj);//$("#picture"));
                    }
                });
            });
        });

        //上传图片
        K('input[name=filemanager]').click(function () {
            var imagesInputObj = $(this).parent().children("input[ccc=imagesInput]");
            editor.loadPlugin('image', function () {
                editor.plugin.imageDialog({
                    viewType: 'VIEW',
                    dirName: 'image',
                    clickFn: function (url, title) {
                        imagesInputObj.val(url);
                        editor.hideDialog();
                        clearRootImagePath(imagesInputObj);//$("#picture"));
                    }
                });
            });
        });

        //上传视频
        K('input[name=videomanager]').click(function () {
            var videoInputObj = $(this).parent().children("input[ccc=videoInput]");
            editor.loadPlugin('filemanager', function () {
                editor.plugin.filemanagerDialog({
                    viewType: 'LIST',
                    dirName: 'media',
                    clickFn: function (url, title) {
                        videoInputObj.val(url);
                        $("#player").attr("src", url);
                        editor.hideDialog();
                        clearRootImagePath(videoInputObj);//$("#picture"));
                    }
                });
            });
        });

    });
</script>

<link rel="stylesheet" href="${systemSetting().staticSource}/static/plugins/uploadify/uploadify/uploadify.css" type="text/css">
<script type="text/javascript" src="${systemSetting().staticSource}/static/plugins/uploadify/uploadify/jquery.uploadify.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        //预售商品
        if ($("#productType").val() == "2") {
            $("#replenishmentTime").parent().parent().show();
            $("#replenishmentEndTime").parent().parent().show();
            $("#replenishmentdeplaytime").parent().parent().show();
            $("#depositPrice").parent().parent().show();
        }
        //现货产品
        else if ($("#productType").val() == "1") {
            $("#replenishmentTime").parent().parent().hide();
            $("#replenishmentEndTime").parent().parent().hide();
            $("#replenishmentdeplaytime").parent().parent().hide();
            $("#depositPrice").parent().parent().hide();
        }

        ajaxLoadImgList();
        var url = '${basepath}/uploadify.do?id=' + $("#id").val();
        //alert(url);
        $("#uploadify").uploadify({
            //'auto'           : false,
            'swf': '${systemSetting().staticSource}/uploadify/uploadify.swf',
            'uploader': url,//后台处理的请求
            'queueID': 'fileQueue',//与下面的id对应
            //'queueSizeLimit' :100,
            //'fileTypeDesc'   : 'rar文件或zip文件',
            //'fileTypeExts' 	 : '*.jpg;*.jpg', //控制可上传文件的扩展名，启用本项时需同时声明fileDesc
            //'fileTypeExts'   : '*.rar;*.zip',    //控制可上传文件的扩展名，启用本项时需同时声明fileDesc


            //'fileTypeDesc' : '图片文件' ,                  //出现在上传对话框中的文件类型描述
            //'fileTypeExts' : '*.jpg;*.bmp;*.png;*.gif', //控制可上传文件的扩展名，启用本项时需同时声明filedesc

            'multi': true,
            'buttonText': '上传',

            onUploadSuccess: function (file, data, response) {
                //alert("上传成功,data="+data+",file="+file+",response="+response);
                ajaxLoadImgList();
            },
            onUploadError: function (file, errorCode, errorMsg) {
                alert("上传失败,data=" + data + ",file=" + file + ",response=" + response);
            }
        });
    });

    //ajax加载内容图片列表
    function ajaxLoadImgList() {
        if ($("#id").val() == '') {
            $("#fileListDiv").html("");
            return;
        }

        $("#fileListDiv").html("");
        var _url = "ajaxLoadImgList?id=" + $("#id").val();
        $.ajax({
            type: 'POST',
            url: _url,
            data: {},
            success: function (data) {
                var _tableHtml = "<table class='table table-bordered' style='border:0px solid red;'>";
                _tableHtml += "<tr style='background-color: #dff0'>";
                _tableHtml += "<td>图片地址</td><td>设为默认图片</td><td>操作</td>";
                _tableHtml += "</tr>";
                $.each(data, function (i, row) {
                    _tableHtml += "<tr>";
                    var str = "<a target='_blank' href='" + row + "'>" + row + "</a>";
                    _tableHtml += "<td>" + str + "</td><td><input type='radio' onclick='setProductImageToDefault(\"" + row + "\")' name='abcdef123'/></td><td><input type='button' Class='btn btn-danger' value='删除' onclick='deleteImageByProductID(\"" + row + "\")'/></td>";
                    _tableHtml += "</tr>";
                    //$("#fileListDiv").append("<a target='_blank' href='"+row+"'>"+row+"</a><br>");
                });
                _tableHtml += "</table>";
                $("#fileListDiv").append(_tableHtml);
            },
            dataType: "json",
            error: function () {
                alert("加载图片列表失败！");
            }
        });
    }

    //产品图片设置为默认图片
    function setProductImageToDefault(imageUrl) {
        var _url = "setProductImageToDefault?id=" + $("#id").val() + "&imageUrl=" + imageUrl;
        $.ajax({
            type: 'POST',
            url: _url,
            data: {},
            success: function (data) {
                //alert("设置成功!");
                $("#showMessage").append("设置成功！").fadeTo(2000, 1, function () {
                    //alert("Animation Done.");
                    $("#showMessage").html("").hide();
                });
            },
            dataType: "text",
            error: function () {
                alert("设置失败！");
            }
        });
    }

    //产品图片设置为默认图片
    function deleteImageByProductID(imageUrl) {
        if (!confirm("确定删除选择的记录?")) {
            return;
        }
        var _url = "deleteImageByProductID?id=" + $("#id").val() + "&imageUrl=" + imageUrl;
        $.ajax({
            type: 'POST',
            url: _url,
            data: {},
            success: function (data) {
                ajaxLoadImgList();
                //$("#showMessage").append("删除成功！").fadeTo(2000, 1, function(){
                //   $("#showMessage").html("").hide();
                //});

            },
            dataType: "text",
            error: function () {
                alert("删除失败！");
            }
        });
    }

    //添加规格行
    function addSpec(who) {
        var specsize = $("input[name=i]").size();
        $(who).parent().parent().parent().append("<tr>" +
                "<td style=\"display: none;\"><input type=\"hidden\" value=\"" + specsize + "\" name=\"i\"/><input type=\"hidden\" value=\"\" name=\"specList[" + specsize + "].id\"/></td>" +
                "<td><input type=\"text\"  value=\"\" name=\"specList[" + specsize + "].specSize\"  class=\"search-query input-small form-control\" data-rule=\"规格一:required;\"/></td>" +
                "<td><input type=\"text\"  value=\"\" name=\"specList[" + specsize + "].specColor\"  class=\"search-query input-small form-control\" data-rule=\"规格二:required;\"/></td>" +
                "<td><input type=\"text\"  value=\"\" name=\"specList[" + specsize + "].specStock\"  class=\"form-control n-valid\" data-rule=\"库存:required;integer[+];\"/></td>" +
                "<td><input type=\"text\"  value=\"\" name=\"specList[" + specsize + "].specPrice\"  class=\"form-control n-valid\" data-rule=\"价格:required;integer[+];\"/></td>" +
                "<td>" +
                "<input type=\"text\"  value=\"\" name=\"specList[" + specsize + "].specCode\"  class=\"form-control n-valid\" data-rule=\"JANCODE:required;\"/></td>" +
                "<td>" +
                "<select id=\"e_spec_specStatus\" name=\"specList[" + specsize + "].specStatus\" class=\"search-query input-medium form-control\" style=\"width: auto; display: inline-block;\">" +
                "<option value=\"n\">不显示</option>" +
                "<option value=\"y\" selected=\"selected\">显示</option>" +
                "</select>" +
                "</td>" +
                "<td>" +
                "<a href=\"#\" onclick=\"deleteSpec('',this)\" class=\"btn btn-primary\" style=\"color:white;\">__</a>" +
                "</td>" +
                "</tr>");
    }
    //删除规格
    function deleteSpec(id, who) {
        if (id != null && id != "") {
            var _url = "${basepath}/manage/product/deleteSpec";
            $.ajax({
                type: 'POST',
                url: _url,
                data: {id: id},
                dataType: "text",
                success: function (data) {
                    alert("删除成功！");
                    location.reload();
                },
                error: function () {
                    alert("删除失败！");
                }
            });
        }
        else {
            who.parentNode.parentNode.remove();
        }
    }

    //添加商品属性
    function addAttr(who) {
        $(who).next().append("<div class=\"form-group col-md-6\"><label class=\"col-md-4 control-label\">属性名称</label><div class=\"col-md-8\"><input type=\"hidden\" value=\"00\" name=\"productAttrIds\" id=\"id\" /><input type=\"text\" value=\"\" name=\"productAttrNames\"  data-rule=\"属性名称;required;productAttrNames;length[0~44];\" size=\"44\" maxlength=\"44\" class=\"form-control n-valid\" id=\"productAttrNames\" /></div></div>" +
                "<div class=\"form-group col-md-6\"><label class=\"col-md-4 control-label\">属性值</label>" +
                "<div class=\"col-md-8\"><input type=\"text\" value=\"\" name=\"productAttrValues\"  data-rule=\"属性名称;required;attributeValue;length[0~44];\" size=\"44\" maxlength=\"44\" class=\"form-control n-valid\" id=\"productAttrValues\" /></div></div><a href=\"#\" onclick=\"deleteAttr('',this)\" class=\"btn btn-primary\" style=\"color:white;margin-top: 20px;\">__</a>");
    }

    //删除商品自定义属性
    function deleteAttr(id, who) {
        if (id != null && id != "") {
            var _url = "${basepath}/manage/attribute/deleteById";
            $.ajax({
                type: 'POST',
                url: _url,
                data: {id: id},
                dataType: "text",
                success: function (data) {
                    alert("删除成功！");
                    location.reload();
                },
                error: function () {
                    alert("删除失败！");
                }
            });
        }
        else {
            $(who).prev().remove();
            $(who).prev().remove();
            $(who).remove();
        }
    }

    //控制产品的类型属性
    function changeProductType(who) {
        //预售商品
        if ($(who).val() == "2") {
            $("#replenishmentTime").parent().parent().show();
            $("#replenishmentEndTime").parent().parent().show();
            $("#replenishmentdeplaytime").parent().parent().show();
            $("#depositPrice").parent().parent().show();
        }
        //现货产品
        else if ($(who).val() == "1") {
            $("#replenishmentTime").parent().parent().hide();
            $("#replenishmentEndTime").parent().parent().hide();
            $("#replenishmentdeplaytime").parent().parent().hide();
            $("#depositPrice").parent().parent().hide();
        }
    }
</script>
</@page.pageBase>