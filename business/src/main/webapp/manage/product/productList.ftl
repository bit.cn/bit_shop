<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="商品管理">
<style type="text/css">
.product-name {
	display: inline-block;
	width: 250px;
	overflow: hidden; /*注意不要写在最后了*/
	white-space: nowrap;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
}
</style>
	<form action="${systemSetting().center}/manage/product" namespace="/manage" method="post" theme="simple">
		<input type="hidden" value="${e.catalogID!""}" id="catalogID"/>
		<table class="table table-bordered table-condensed">
			<tr>
				<td style="text-align: right;">商品编号</td>
				<td style="text-align: left;">
					<input type="text"  value="${e.id!""}" name="id"  class="search-query input-small" id="id" />
				</td>
				<td style="text-align: right;">状态</td>
				<td style="text-align: left;">
                    <#assign map = {"0":'',"1":'新增',"2":'已上架',"3":'已下架'}>
                    <select id="status" name="status" class="input-medium">
                        <#list map?keys as key>
                            <option value="${key}" <#if e.status?? && e.status==key?eval>selected="selected" </#if>>${map[key]}</option>
                        </#list>
				</td>
				<td style="text-align: right;">
					商品分类
				</td>
				<td>
					<select onchange="catalogChange(this)" name="catalogID" id="catalogSelect" class="input-medium">
						<option></option>
                        <#list catalogs as item>
							<option pid="0" value="${item.id!""}"><font color='red'>${item.name!""}</font></option>
                            <#if item.children??>
                                <#list item.children as item>
								    <option value="${item.id!""}">&nbsp;&nbsp;&nbsp;&nbsp;${item.name!""}</option>
                                </#list>
                            </#if>
                        </#list>
					</select>
				</td>
				<td style="text-align: right;">贩售状态</td>
				<td style="text-align: left;">
					<#assign map = {"0":'','1':'现货','2':'预售'}>
                    <select id="productType" name="productType" class="search-query input-medium">
                         <#list map?keys as key>
                          <option value="${key}" <#if e.productType?? && e.productType?string("number")==key>selected="selected" </#if> >
                          ${map[key]}
                         </option>
                       </#list>                     	               
                   </select>
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">商品名称</td>
				<td style="text-align: left;" >
					<input type="text"  value="${e.name!""}" name="name"  class="input-small" id="name" />
				</td>
				<td style="text-align: right;">销售日期</td>
				<td style="text-align: left;" colspan="3">
					<input id="d4311" class="Wdate search-query input-small" type="text" name="startDate"
					value="${e.startDate!""}" style="height:30px;"
					onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01\'}'})"/>
					~ 
					<input id="d4312" class="Wdate search-query input-small" type="text" name="endDate"
					value="${e.endDate!""}" style="height:30px;"
					onFocus="WdatePicker({minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01'})"/>
				</td>
				<td style="text-align: right;">JANCODE</td>
				<td style="text-align: left;">
					<input type="text"  value="${e.code!""}" name="code"  class="search-query input-small" id="code" />
				</td>
			</tr>
			<tr>
				<td colspan="20">
                    <#if checkPrivilege("product/selectList")>
						<button method="selectList" class="btn btn-primary" onclick="selectList(this)">
							<i class="icon-search icon-white"></i> 查询
						</button>
                    </#if>

                     <#if checkPrivilege("product/toAdd")>
						<a href="toAdd" class="btn btn-success">
							<i class="icon-plus-sign icon-white"></i> 添加
						</a>
                     </#if>

                    <#if checkPrivilege("product/deletes")>
						<button method="deletes" class="btn btn-danger" onclick="return submitIDs(this,'确定删除选择的记录?');">
							<i class="icon-remove-sign icon-white"></i> 删除
						</button>
                    </#if>

                    <#if checkPrivilege("product/updateUp")>
<!-- 							<i class="icon-arrow-up icon-white"></i> 上架 -->
						<button method="updateUp" class="btn btn-warning" onclick="return submitIDs(this,'确定上架选择的记录?');">
							<i class="icon-arrow-up icon-white"></i> 上架
						</button>
                    </#if>

                    <#if checkPrivilege("product/updateDown")>
						<button method="updateDown" class="btn btn-warning" onclick="return submitIDs(this,'确定下架选择的记录?');">
							<i class="icon-arrow-down icon-white"></i> 下架
						</button>
                    </#if>
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
						<#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>
		<table class="table table-bordered table-condensed table-hover">
			<tr style="background-color: #dff0d8">
				<th width="20"><input type="checkbox" id="firstCheckbox" /></th>
				<th nowrap="nowrap">商品编号</th>
				<th>JANCODE</th>
				<th>商品名</th>
				<th>预定价</th>
				<th>全款价格</th>
				<th>上架时间</th>
				<th>下架时间</th>
				<th>浏览次数</th>
				<th>库存数</th>
				<th>预定数</th>
				<th>销量</th>
				<th>商品类别</th>
				<th>商品状态</th>
				<th>是否上架</th>
				<th width="10%">操作</th>
			</tr>
            <#list pager.list as item>
				<tr>
					<td><input type="checkbox" name="ids"
						value="${item.id!""}" /></td>
					<td nowrap="nowrap">&nbsp;${item.id!""}</td>
					<td>&nbsp;${item.code!""}</td>
					<td >
						<a class="product-name" title="${item.name}" href="toEdit?id=${item.id}">
						<#if item.name??>
								<#if item.name?length gte 50>
									${item.name?substring(0,50)}...
								<#else>
									${item.name!""}
								</#if>
						</#if>
						</a>
					</td>
					<td>&nbsp;${item.depositPrice!""}</td>
					<td>&nbsp;${item.nowPrice!""}</td>
					<td>&nbsp;${item.groundingTime!""}</td>
					<td>&nbsp;${item.offshelfTime!""}</td>
					<td>&nbsp;${item.hit!""}</td>
					<td>&nbsp;
							${item.stock!""}
					</td>
					<td>&nbsp;${item.sellcount!""}</td>
					<td>&nbsp;${item.sellcount!""}</td>
					<td>&nbsp;
					<#if item.productType==1>
					现货
					<#elseif item.productType==2>
					预售
					<#elseif item.productType==3>
					补款
					</#if>
					
					</td>
					
					<td>
					<#if item.productType?? && item.productType==1>
					  <#if item.status??&&item.status==1>
					       即将上架
					  <#elseif item.status??&&item.status==2>
					     <#if item.stock lt 1>
					     无货
					     <#else>
					     贩售中
					     </#if>
					  <#elseif item.status??&&item.status==3>
					  已下架
					  </#if>
					<#else>
					     <#if item.status??&&item.status==1>
					         即将上架
					     <#elseif item.status??&&item.status==2>
					      <#if item.sellEndTime?? && item.sellEndTime?date("yyyy-MM-dd HH:mm:ss") gt now?date("yyyy-MM-dd HH:mm:ss")>
					                     预售中					        					               					         
					        <#elseif item.sellEndTime?? && item.sellEndTime?date("yyyy-MM-dd HH:mm:ss") lt now?date("yyyy-MM-dd HH:mm:ss") && item.replenishmentTime?? && item.replenishmentTime?date("yyyy-MM-dd HH:mm:ss") gt now?date("yyyy-MM-dd HH:mm:ss")>
					                    预售结束
					        <#elseif item.sellEndTime?? && item.sellEndTime?date("yyyy-MM-dd HH:mm:ss") lt now?date("yyyy-MM-dd HH:mm:ss") && !item.replenishmentTime??>
					                    预售结束
					        <#elseif item.replenishmentTime?? && item.replenishmentTime?date("yyyy-MM-dd HH:mm:ss") lt now?date("yyyy-MM-dd HH:mm:ss") && item.replenishmentEndTime?date("yyyy-MM-dd HH:mm:ss") gt now?date("yyyy-MM-dd HH:mm:ss")>
					                    补款中
					        <#elseif item.replenishmentTime?? && item.replenishmentTime?date("yyyy-MM-dd HH:mm:ss") lt now?date("yyyy-MM-dd HH:mm:ss") && item.replenishmentEndTime?date("yyyy-MM-dd HH:mm:ss") lt now?date("yyyy-MM-dd HH:mm:ss")>
					                    补款结束
					        </#if>
					     <#elseif item.status??&&item.status==3>
					     已下架   
					     </#if>
					</#if>
					</td>
					
					<td>&nbsp;
						<#if item.status??&&item.status==1>
							<img alt="新增" src="${systemSetting().staticSource}/static/frontend/v1/images/action_add.gif">
						<#elseif item.status??&&item.status==2>
							<img alt="已上架" src="${systemSetting().staticSource}/static/frontend/v1/images/action_check.gif">
						<#elseif item.status??&&item.status==3>
							<img alt="已下架" src="${systemSetting().staticSource}/static/frontend/v1/images/action_delete.gif">
						</#if>
					</td>
					<td>
						<a href="toEdit?id=${item.id}">编辑</a>|
						<a target="_blank" href="${systemSetting().item}/product/${item.id!""}.html">预览</a>
							<#if item.productType?? && item.productType==2>
						     	<#if item.status??&&item.status==2>
						        <#if item.replenishmentTime?? && item.replenishmentTime?date("yyyy-MM-dd HH:mm:ss") lt now?date("yyyy-MM-dd HH:mm:ss") && item.replenishmentEndTime?date("yyyy-MM-dd HH:mm:ss") gt now?date("yyyy-MM-dd HH:mm:ss")>
						           <a target="_blank" href="${systemSetting().business}/manage/product/sendReplenishmentEmail?id=${item.id!""}">补款中发送邮件</a>
						        <#elseif item.replenishmentTime?? && item.replenishmentTime?date("yyyy-MM-dd HH:mm:ss") lt now?date("yyyy-MM-dd HH:mm:ss") && item.replenishmentEndTime?date("yyyy-MM-dd HH:mm:ss") lt now?date("yyyy-MM-dd HH:mm:ss")>
						           <a target="_blank" href="${systemSetting().business}/manage/product/sendReplenishmentEmail?id=${item.id!""}">补款结束发送邮件</a>
						   		</#if>
						   	</#if>
						</#if>
					</td>
				</tr>
            </#list>

			<tr>
				<td colspan="70" style="text-align: center;">
                    <#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
		
		<div class="alert alert-info" style="text-align: left;font-size: 14px;margin: 2px 0px;">
			图标含义：<BR>
			<img alt="新增" src="${systemSetting().staticSource}/static/frontend/v1/images/action_add.gif">：新增商品
			<img alt="已上架" src="${systemSetting().staticSource}/static/frontend/v1/images/action_check.gif">：商品已上架
			<img alt="已下架" src="${systemSetting().staticSource}/static/frontend/v1/images/action_delete.gif">：商品已下架
		</div>

	</form>
	
<script type="text/javascript">
	$(function() {
		selectDefaultCatalog();
	});
	function selectDefaultCatalog(){
		var _catalogID = $("#catalogID").val();
		if(_catalogID!='' && _catalogID>0){
			$("#catalogSelect").attr("value",_catalogID);
		}
	}
</script>
</@page.pageBase>