package com.soloyogame.anitoys.business.web.controller.manage.order;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.soloyogame.anitoys.business.web.controller.BaseController;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.Order;
import com.soloyogame.anitoys.db.commond.OrderWork;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.OrderService;
import com.soloyogame.anitoys.service.OrderWorkService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/manage/importExcle/")
public class ImportOrder extends BaseController<OrderWork> 
{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(OrderWorkAction.class);
    private static final long serialVersionUID = 1L;
    
    @Autowired
    private OrderWorkService orderWorkService;
    @Autowired
    private OrderService orderService;
    private File importExcle;
    private static final String page_toList = "/manage/importLogistics/orderList";

    @Override
    public OrderWorkService getService() {
        return orderWorkService;
    }

    public void setOrderWorkService(OrderWorkService orderWorkService) {
        this.orderWorkService = orderWorkService;
    }
    
    private ImportOrder() {
        super.page_toList = page_toList;
    }

    public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	@Override
    protected void selectListAfter(PagerModel pager) {
        pager.setPagerUrl("selectList");
        super.selectListAfter(pager);

    }

    /**
     * 导入物流单号
     *
     * @param request
     * @param response
     * @param e
     * @throws IOException
     */
    @RequestMapping("importExcels")
    @ResponseBody
    public void importExcels(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("e") OrderWork e) throws IOException {
        try {
//            MultipartHttpServletRequest mulRequest = (MultipartHttpServletRequest) request;
//            MultipartFile file = mulRequest.getFile("importExcler");
//            System.out.println(file.getName());
////            java.io.ByteArrayInputStream cannot be cast to java.io.FileInputStream<br><br> Please contact support
//            InputStream fis = file.getInputStream();
//            Workbook wb = create(fis);
//
//            Sheet sheet = wb.getSheetAt(0);
//            //第一行为表头,所以从j=1开始
//            for (int j = 1; j < sheet.getLastRowNum() + 1; j++) {
//                Row row = sheet.getRow(j);
//                Order newOrder = getSingleOrderFromExcel(row);
//                newOrder.setStatus(Order.order_status_send);
//                newOrder.setPaystatus(Order.order_paystatus_y);
//                orderService.updateExpress(newOrder);
//            }
//            response.getWriter().write("<script>alert(' Import Success!')</script>");

        } catch (Exception exception) {
            exception.printStackTrace();
            response.getWriter().write("<script>alert(' " + exception.getMessage() + "\r\n Please contact support team!!')</script>");
            response.flushBuffer();
        }
    }


//    public static Workbook create(InputStream in) throws IOException, InvalidFormatException {
//        if (!in.markSupported()) {
//            in = new PushbackInputStream(in, 8);
//        }
//        if (POIFSFileSystem.hasPOIFSHeader(in)) {
//            return new HSSFWorkbook(in);
//        }
//        if (POIXMLDocument.hasOOXMLHeader(in)) {
//            return new XSSFWorkbook(OPCPackage.open(in));
//        }
//        throw new IllegalArgumentException("Not support the file format!");
//    }

    private Order getSingleOrderFromExcel(Row row) throws Exception {
        Order order = new Order();
        for (int i = 0; i < row.getLastCellNum(); i++) {
            Cell cell = row.getCell(i);
            switch (cell.getCellType()) {
                case HSSFCell.CELL_TYPE_NUMERIC:
                    String name = String.valueOf(Math.round(cell.getNumericCellValue()));
                    if (i == 0) {
                        order.setId(name);
                        continue;
                    }
                    if (i == 1) {
                        order.setExpressCompanyName(name);
                        continue;
                    }
                    if (i == 2) {
                        order.setExpressNo(name);
                        continue;
                    }
                    break;
                case HSSFCell.CELL_TYPE_STRING:
                    if (i == 0) {
                        order.setId(cell.getStringCellValue());
                        continue;
                    }
                    if (i == 1) {
                        order.setExpressCompanyName(cell.getStringCellValue());
                        continue;
                    }
                    if (i == 2) {
                        order.setExpressNo(cell.getStringCellValue());
                        continue;
                    }

                    break;

                default:
                    throw new Exception("Unsupport Data! Please verify the data type in you excel!");
                    // write some  response to response
            }
        }
        return order;
    }

    public File getImportExcle() {
        return importExcle;
    }

    public void setImportExcle(File importExcle) {
        this.importExcle = importExcle;
    }


}
