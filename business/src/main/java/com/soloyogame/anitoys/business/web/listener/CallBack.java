package com.soloyogame.anitoys.business.web.listener;

public interface CallBack {
	String callback() throws Exception;
}
