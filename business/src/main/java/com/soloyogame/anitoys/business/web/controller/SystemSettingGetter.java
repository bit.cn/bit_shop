package com.soloyogame.anitoys.business.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.db.commond.SystemSetting;
import com.soloyogame.anitoys.service.SystemSettingService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

/**
 * 获取系统参数的配置
 * @author 索罗游
 * 系统设置全局变量
 */
public class SystemSettingGetter implements TemplateMethodModelEx 
{
	 @Autowired
	    private SystemSettingService systemSettingService;
	    @Autowired
	    private RedisCacheProvider redisCacheProvider;

	    @Override
	    public Object exec(List arguments) throws TemplateModelException 
	    {
	    	SystemSetting systemSetting =JSON.parseObject(redisCacheProvider.get("systemSetting").toString(), SystemSetting.class);
	    	if(systemSetting==null)
	    	{
	    		systemSetting = systemSettingService.selectOne(new SystemSetting());
	    	}
	        return systemSetting;
	    }
}
