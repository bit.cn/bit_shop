package com.soloyogame.anitoys.business.web.controller.manage.order;

import com.soloyogame.anitoys.business.web.controller.BaseController;
import com.soloyogame.anitoys.business.web.controller.util.CSVUtils;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.AccountPointsLog;
import com.soloyogame.anitoys.db.commond.Order;
import com.soloyogame.anitoys.db.commond.OrderWork;
import com.soloyogame.anitoys.db.commond.Orderpay;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.OrderService;
import com.soloyogame.anitoys.service.OrderWorkService;
import com.soloyogame.anitoys.service.OrderpayService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;


@Controller
@RequestMapping("/manage/orderWork/")
public class OrderWorkAction extends BaseController<OrderWork> {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(OrderWorkAction.class);
    private static final long serialVersionUID = 1L;
    @Autowired
    private OrderWorkService orderWorkService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderpayService orderpayService;

    private static final String page_toList = "/manage/orderWork/orderList";

    private OrderWorkAction() {
        super.page_toList = page_toList;
    }

    @Override
    protected void selectListAfter(PagerModel pager) {
        pager.setPagerUrl("selectList");
        super.selectListAfter(pager);
    }


    /**
     * 根据订单的创建日期导出cvs
     *
     * @param request
     * @param response
     * @param e
     * @throws IOException
     */
    @RequestMapping("exportExcels")
    @ResponseBody
    public void exportExcels(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("e") OrderWork e) throws IOException {
        String name = "工作簿.csv";
        String fileName = java.net.URLEncoder.encode(name, "UTF-8");
        List<OrderWork> dataList = orderWorkService.selectList(e);
        String[] head = {"订单号", "商品janCode", "商品名", "商品单价", "购买数量",
                "购买人ID", "收货地址", "联系方式", "收货人姓名", "快递方式",
                "快递单号", "下单时间", "预定时间", "补款时间", "完成付款时间",
                "是否售后", "订单状态", "订单总额", "运费金额", "买家积分使用",
                "买家优惠券抵扣", "买家平台抵扣券的抵扣", "买家站内余额使用",
                "买家现金支付金额", "单品总额", "母订单号",
                "买家第一次积分使用", "买家第一次平台抵扣券的抵扣", "买家第一次站内余额使用", "买家第一次现金支付金额",
                "买家第二次积分使用", "买家第二次平台抵扣券的抵扣", "买家第二次站内余额使用", "买家第二次现金支付金额"};

        byte[] csvBytes = CSVUtils.createCSVBytes(head, dataList, request);
        response.setContentType("application/vnd.ms-csv");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + "");

        OutputStream outputStream = response.getOutputStream();
        outputStream.write(csvBytes);
        outputStream.flush();
        outputStream.close();
    }

    private void getOtherInfo(List<OrderWork> datas) {
        for (OrderWork data : datas) {
            Order parenOrder = orderService.selectById(data.getParentId());
            data.setParentId(parenOrder.getOrderCode());

            //根据订单号查询支付记录集合
            //订单支付日志
            Orderpay op = new Orderpay();
            op.setOrderid(data.getId());
            List<Orderpay> payList = orderpayService.selectList(op);
            if (payList.size() == 1) {
                if ("amtway".equalsIgnoreCase(payList.get(0).getPaymethod())) {
                    //站内余额
                    data.setFirstAmountCount(String.valueOf(payList.get(0).getPayamount()));
                } else if ("baoway".equalsIgnoreCase(payList.get(0).getPaymethod())) {
                    //现金支付
                    data.setFirstPayAccount(String.valueOf(payList.get(0).getPayamount()));
                }
            } else {
                if (payList.get(0).getPayTime().before(payList.get(1).getPayTime())) {
                    if ("amtway".equalsIgnoreCase(payList.get(0).getPaymethod())) {
                        //站内余额
                        data.setFirstAmountCount(String.valueOf(payList.get(0).getPayamount()));
                    } else if ("baoway".equalsIgnoreCase(payList.get(0).getPaymethod())) {
                        //现金支付
                        data.setFirstPayAccount(String.valueOf(payList.get(0).getPayamount()));
                    }
                    if ("amtway".equalsIgnoreCase(payList.get(1).getPaymethod())) {
                        //站内余额
                        data.setSecondAmountCount(String.valueOf(payList.get(1).getPayamount()));
                    } else if ("baoway".equalsIgnoreCase(payList.get(1).getPaymethod())) {
                        //现金支付
                        data.setSecondPayAcount(String.valueOf(payList.get(1).getPayamount()));
                    }
                } else {
                    if ("amtway".equalsIgnoreCase(payList.get(0).getPaymethod())) {
                        //站内余额
                        data.setSecondAmountCount(String.valueOf(payList.get(0).getPayamount()));
                    } else if ("baoway".equalsIgnoreCase(payList.get(0).getPaymethod())) {
                        //现金支付
                        data.setSecondPayAcount(String.valueOf(payList.get(0).getPayamount()));
                    }
                    if ("amtway".equalsIgnoreCase(payList.get(0).getPaymethod())) {
                        //站内余额
                        data.setFirstAmountCount(String.valueOf(payList.get(0).getPayamount()));
                    } else if ("baoway".equalsIgnoreCase(payList.get(0).getPaymethod())) {
                        //现金支付
                        data.setFirstPayAccount(String.valueOf(payList.get(0).getPayamount()));
                    }
                }
            }

            //根据订单号查询积分使用日志
            //查询使用积分
            AccountPointsLog pointsLog = new AccountPointsLog();
            pointsLog.setOrderId(parenOrder.getId());
            List<AccountPointsLog> pointsLogList = orderService.selectOrderPoints(pointsLog);
            if (pointsLogList.size() == 1) {
                data.setFirstExchangeScore(String.valueOf(pointsLogList.get(0).getRankCount()));
            } else if (pointsLogList.size() == 2) {
                data.setFirstExchangeScore(String.valueOf(pointsLogList.get(1).getRankCount()));
                data.setSecondExchangeScore(String.valueOf(pointsLogList.get(0).getRankCount()));
            }

            //查询平台抵扣券的抵扣
//            //根据订单号查询余额使用日志
//            //根据订单ID得到订单消费余额的日志
//            AccountRankLog accountRankLog = new AccountRankLog();
//            accountRankLog.setUserId(data.getAccount());
//            accountRankLog.setOrderId(data.getId());
//            AccountFinanceService accountFinanceService = (AccountFinanceService) app.getBean("accountFinanceServiceImpl");
//            List<AccountRankLog> accountRankLogList = accountFinanceService.selectRankLogPageList(accountRankLog).getList();
        }
    }

    @Override
    public Services<OrderWork> getService() {
        return orderWorkService;
    }
}
