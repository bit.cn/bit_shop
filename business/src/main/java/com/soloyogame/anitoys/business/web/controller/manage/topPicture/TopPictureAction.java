package com.soloyogame.anitoys.business.web.controller.manage.topPicture;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.soloyogame.anitoys.business.web.controller.BaseController;
import com.soloyogame.anitoys.business.web.controller.util.LoginUserHolder;
import com.soloyogame.anitoys.db.bean.User;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.TopPictureService;


/**
 * 商铺首页的顶部图片Action
 *
 * @author Liam
 */
@Controller
@RequestMapping("/manage/topPicture/")
public class TopPictureAction extends BaseController<TopPicture> 
{
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(TopPictureAction.class);
    private static final String page_toList = "/manage/topPicture/topPictureList";
    private static final String page_toEdit = "/manage/topPicture/topPictureEdit";
    private static final String page_toAdd = "/manage/topPicture/topPictureEdit";

    private TopPictureAction() {
        super.page_toList = page_toList;
        super.page_toAdd = page_toAdd;
        super.page_toEdit = page_toEdit;
    }

    @Autowired
    private TopPictureService topPictureService;

    @Autowired
    public TopPictureService getService() {
        return topPictureService;
    }

    public void TopPictureService(TopPictureService topPictureService) {
        this.topPictureService = topPictureService;
    }

    @Override
    public void insertAfter(TopPicture e) {
        e.clear();
    }
    
    /**
     * 公共的分页方法
     * @return
     * @throws Exception
     */
    @RequestMapping("selectList")
    public String selectList(HttpServletRequest request, @ModelAttribute("e") TopPicture e) throws Exception 
    {
        /**
         * 由于prepare方法不具备一致性，加此代码解决init=y查询的时候条件不被清除干净的BUG
         */
        User user = LoginUserHolder.getLoginUser();
        this.initPageSelect();
        e.setBusinessId(user.getBusinessId());
        setParamWhenInitQuery(e);

        int offset = 0;//分页偏移量
        if (request.getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(request.getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        e.setOffset(offset);
        PagerModel pager = getService().selectPageList(e);
        if (pager == null) 
        {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)/ pager.getPageSize());

        selectListAfter(pager);
        request.setAttribute("pager", pager);
        return page_toList;
    }


    @Override
    @RequestMapping(value = "insert", method = RequestMethod.POST)
    public String insert(HttpServletRequest request, @ModelAttribute("e") TopPicture e, RedirectAttributes flushAttrs) throws Exception {
        User u = LoginUserHolder.getLoginUser();
        e.setBusinessId(u.getBusinessId());
        getService().insert(e);
        insertAfter(e);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
}
