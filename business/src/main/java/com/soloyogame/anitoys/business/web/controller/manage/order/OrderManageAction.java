package com.soloyogame.anitoys.business.web.controller.manage.order;


import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.soloyogame.anitoys.business.web.controller.BaseController;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.Order;
import com.soloyogame.anitoys.db.commond.OrderManager;
import com.soloyogame.anitoys.db.commond.OrderWork;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.OrderManagerService;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 订单管理
 * @author shaojian
 */
@Controller
@RequestMapping("/manage/orderManage/")
public class OrderManageAction extends BaseController<OrderManager> 
{
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(OrderAction.class);
	private static final long serialVersionUID = 1L;
	@Autowired
	private OrderManagerService orderManagerService;

	private static final String page_toList = "/manage/orderManage/orderList";
	private OrderManageAction() 
	{
		super.page_toList = page_toList;
	}
	
	@Override
	protected void selectListAfter(PagerModel pager) 
	{
		pager.setPagerUrl("selectList");
		super.selectListAfter(pager);
	}
	
	public OrderManagerService getOrderManagerService() 
	{
		return orderManagerService;
	}

	public void setOrderManagerService(OrderManagerService orderManagerService) 
	{
		this.orderManagerService = orderManagerService;
	}
	
	@Override
	public Services<OrderManager> getService() 
	{
		return orderManagerService;
	}

  /**
  * 根据订单的创建日期导出cvs
  * @param request
  * @param response
  * @param e
  * @throws IOException
  */
 @RequestMapping("exportExcels")
 public @ResponseBody void exportExcels(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("e") OrderManager e) throws IOException
 {
	 String name="现货出单.csv";
	 String fileName = java.net.URLEncoder.encode(name, "UTF-8");
	 List<OrderManager> dataList=orderManagerService.selectList(e);
	 HSSFWorkbook wb = this.excelPort(dataList);
	 response.setContentType("application/vnd.ms-excel");
	 response.setHeader("Content-disposition", "attachment;filename="+fileName+""); 
	 OutputStream ouputStream = response.getOutputStream();
	 wb.write(ouputStream);        
	 ouputStream.flush();
	 ouputStream.close(); 
} 
 
 /**
  * 现货出单导出方法
  * @param list
  */
 public static HSSFWorkbook excelPorts(List<OrderWork> list) 
 {
     String[] excelHeader = {"订单号", "商品janCode", "商品名", "商品单价", "购买数量", "购买人ID", "收货地址", "联系方式", "收货人姓名", "快递方式", "快递单号", "下单时间", "预定时间", "补款时间", "完成付款时间", "是否售后", "订单状态", "订单总额", "运费金额", "买家积分使用", "买家优惠券抵扣", "买家平台抵扣券的抵扣", "买家站内余额使用", "买家现金支付金额", "单品总额"};
     HSSFWorkbook wb = new HSSFWorkbook();
     HSSFSheet sheet = wb.createSheet("订单总表");
     HSSFRow row0 = sheet.createRow((int) 0);

     HSSFRow row = sheet.createRow((int) 1);
     HSSFCellStyle style = wb.createCellStyle();
     style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
     
     for (short i = 0; i < excelHeader.length; i++) 
     {
         HSSFCell cell = row.createCell(i);
         cell.setCellValue(excelHeader[i]);
         cell.setCellStyle(style);
         sheet.setColumnWidth(i, 3500);
     }
     for (int i = 0; i < list.size(); i++) 
     {
         row = sheet.createRow(i + 2);
         OrderWork orderWork = list.get(i);
         row.createCell(0).setCellValue(orderWork.getOrderCode());
         row.createCell(1).setCellValue(orderWork.getProductId());
         row.createCell(2).setCellValue(orderWork.getProductName());
         row.createCell(3).setCellValue(orderWork.getProductPrice());
         row.createCell(4).setCellValue(orderWork.getNumber());
         row.createCell(5).setCellValue(orderWork.getAccount());
         row.createCell(6).setCellValue(orderWork.getShipAddress());
         row.createCell(7).setCellValue(orderWork.getPhone());
         row.createCell(8).setCellValue(orderWork.getShipName());
         row.createCell(9).setCellValue(orderWork.getExpressName());
         row.createCell(10).setCellValue(orderWork.getExpressNo());
         row.createCell(11).setCellValue(orderWork.getDownOrderTime());
         row.createCell(12).setCellValue(orderWork.getBespeakTime());
         row.createCell(13).setCellValue(orderWork.getReplenishmentTime());
         row.createCell(14).setCellValue(orderWork.getFinishreplenishmentTime());
         row.createCell(15).setCellValue(("1".equals(orderWork.getIsCustomer())) ? "是" : "否");
         if (!StringUtils.isBlank(orderWork.getOrderStatus())) {
             if (Order.order_status_init.equals(orderWork.getOrderStatus())) {
                 row.createCell(16).setCellValue("待付款");
             } else if (Order.order_status_pass.equals(orderWork.getOrderStatus())) {
                 row.createCell(16).setCellValue("待补款");
             } else if (Order.order_status_stay.equals(orderWork.getOrderStatus())) {
                 row.createCell(16).setCellValue("待发货");
             } else if (Order.order_status_send.equals(orderWork.getOrderStatus())) {
                 row.createCell(16).setCellValue("待收货");
             } else if (Order.order_status_sign.equals(orderWork.getOrderStatus())) {
                 row.createCell(16).setCellValue("已完成");
             } else if (Order.order_status_none.equals(orderWork.getOrderStatus())) {
                 row.createCell(16).setCellValue("已取消");
             } else if (Order.order_status_cancel.equals(orderWork.getOrderStatus())) {
                 row.createCell(16).setCellValue("已取消");
             } else if (Order.order_status_file.equals(orderWork.getOrderStatus())) {
                 row.createCell(16).setCellValue("已归档");
             } else {
                 row.createCell(16).setCellValue("");
             }
         } else {
             row.createCell(16).setCellValue("");
         }
         row.createCell(17).setCellValue(orderWork.getOrderSumPrice());
         row.createCell(18).setCellValue(orderWork.getFeeSumPrice());
         row.createCell(19).setCellValue(orderWork.getBuyerRank());
         row.createCell(20).setCellValue(orderWork.getBuyerCoupon());
         row.createCell(21).setCellValue(orderWork.getBuyerCouponPlat());
         row.createCell(22).setCellValue(orderWork.getBuyerAmount());
         row.createCell(23).setCellValue(orderWork.getBuyerPayMoney());
         row.createCell(24).setCellValue(orderWork.getPtotal());
     }
     return wb;
 }

 /**
  * 现货出单导出方法
  * @param list
  */
 public static HSSFWorkbook excelPort(List<OrderManager> list) 
 {
     String[] excelHeader = {"janCode", "订单编号", "买家会员名", "买家留言", "收货人姓名", "收货地址", "联系电话", "联系手机", "宝贝标题", "宝贝种类 ", "物流单号", "物流公司", "发货时间", "仓库备注", "订单备注", "宝贝总数量", "购买数量"};
     HSSFWorkbook wb = new HSSFWorkbook();
     excelPortSheet1(list, wb);
     HSSFSheet sheet = wb.createSheet("总单");
     HSSFRow row = sheet.createRow((int) 0);
     HSSFCellStyle style = wb.createCellStyle();
     style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
     for (short i = 0; i < excelHeader.length; i++) 
     {
         HSSFCell cell = row.createCell(i);
         cell.setCellValue(excelHeader[i]);
         cell.setCellStyle(style);
         sheet.setColumnWidth(i, 3000);
     }
     for (int i = 0; i < list.size(); i++) 
     {
         row = sheet.createRow(i + 1);
         OrderManager orderManager = list.get(i);
         row.createCell(0).setCellValue(orderManager.getCode());
         row.createCell(1).setCellValue(orderManager.getOrderCode());
         row.createCell(2).setCellValue(orderManager.getAccount());
         row.createCell(3).setCellValue(orderManager.getBuyerMessage());
         row.createCell(4).setCellValue(orderManager.getShipName());
         row.createCell(5).setCellValue(orderManager.getShipAddress());
//	    	 row.createCell(6).setCellValue(orderManager.getCarry()); 
         row.createCell(6).setCellValue(orderManager.getTel());
         row.createCell(7).setCellValue(orderManager.getPhone());
         row.createCell(8).setCellValue(orderManager.getProductName());
         row.createCell(9).setCellValue(orderManager.getFirstType());
         row.createCell(10).setCellValue(orderManager.getExpressNo());
         row.createCell(11).setCellValue(orderManager.getExpressName());
         row.createCell(12).setCellValue(orderManager.getDeliverTime());
         row.createCell(13).setCellValue(orderManager.getWareHouseRemark());
         row.createCell(14).setCellValue(orderManager.getRemark());
         row.createCell(15).setCellValue(orderManager.getQuantity());
         row.createCell(16).setCellValue(orderManager.getQuantity());
     }
     return wb;
 }	
 
 public static void excelPortSheet1(List<OrderManager> list, HSSFWorkbook wb) 
 {
     String[] excelHeader = {"订单编号", "janCode", "总计", "买家会员名", "买家留言", "收货人姓名", "收货地址", "运送方式", "联系电话", "联系手机", "订单备注", "物流单号", "物流公司", "发货时间", "仓库备注"};
     HSSFSheet sheet = wb.createSheet("合并后订单");
     HSSFRow row = sheet.createRow((int) 0);
     HSSFCellStyle style = wb.createCellStyle();
     style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
     for (short i = 0; i < excelHeader.length; i++) 
     {
         HSSFCell cell = row.createCell(i);
         cell.setCellValue(excelHeader[i]);
         cell.setCellStyle(style);
// 	 sheet.autoSizeColumn(i);
         sheet.setColumnWidth(i, 3000);
     }
     for (int i = 0; i < list.size(); i++) 
     {
         row = sheet.createRow(i + 1);
         OrderManager orderManager = list.get(i);
         row.createCell(0).setCellValue(orderManager.getOrderCode());
         row.createCell(1).setCellValue(orderManager.getCode());
         row.createCell(2).setCellValue(orderManager.getQuantity());
         row.createCell(3).setCellValue(orderManager.getAccount());
         row.createCell(4).setCellValue(orderManager.getBuyerMessage());
         row.createCell(5).setCellValue(orderManager.getShipName());
         row.createCell(6).setCellValue(orderManager.getShipAddress());
         row.createCell(7).setCellValue(orderManager.getExpressName());
         row.createCell(8).setCellValue(orderManager.getTel());
         row.createCell(9).setCellValue(orderManager.getPhone());
         row.createCell(10).setCellValue(orderManager.getRemark());
         row.createCell(11).setCellValue(orderManager.getExpressNo());
         row.createCell(12).setCellValue(orderManager.getExpressCompanyName());
         row.createCell(13).setCellValue(orderManager.getDeliverTime());
         row.createCell(14).setCellValue(orderManager.getWareHouseRemark());
     }
 }

}




    
