package com.soloyogame.anitoys.business.web.controller.manage.businessAdvert;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.soloyogame.anitoys.business.web.controller.BaseController;
import com.soloyogame.anitoys.business.web.controller.util.LoginUserHolder;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.bean.User;
import com.soloyogame.anitoys.db.commond.BusinessAdvert;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.BusinessAdvertService;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 索罗游
 */
@Controller
@RequestMapping("/manage/businessAdvert/")
public class BusinessAdvertAction extends BaseController<BusinessAdvert> 
{
    private static final long serialVersionUID = 1L;
    private static final String page_toList = "/manage/businessAdvert/businessAdvertList";
    private static final String page_toEdit = "/manage/businessAdvert/businessAdvertEdit";
    private static final String page_toAdd = "/manage/businessAdvert/businessAdvertEdit";

    private BusinessAdvertAction() {
        super.page_toList = page_toList;
        super.page_toAdd = page_toAdd;
        super.page_toEdit = page_toEdit;
    }

    @Autowired
    private BusinessAdvertService businessAdvertService;

    @Override
    public Services<BusinessAdvert> getService() {
        return businessAdvertService;
    }

    @Override
    public void insertAfter(BusinessAdvert e) {
        e.clear();
    }

    @Override
    protected void selectListAfter(PagerModel pager) {
        pager.setPagerUrl("selectList");
    }


    /**
     * app平台推荐图的更新数据的方法，子类可以通过重写此方法实现个性化的需求。
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "updateApps", method = RequestMethod.POST)
    public String updateApps(HttpServletRequest request, @ModelAttribute("e") BusinessAdvert e, RedirectAttributes flushAttrs) throws Exception 
    {
        User u = LoginUserHolder.getLoginUser();
        e.setBusinessId(u.getBusinessId());
        getService().update(e);
        insertAfter(e);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList?&businessId=" + u.getBusinessId();
    }

    /**
     * app轮播图的插入数据方法，子类可以通过重写此方法实现个性化的需求。
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "insertApp", method = RequestMethod.POST)
    public String insertApp(HttpServletRequest request, @ModelAttribute("e") BusinessAdvert e, RedirectAttributes flushAttrs) throws Exception 
    {
        User u = LoginUserHolder.getLoginUser();
        e.setBusinessId(u.getBusinessId());
        BusinessAdvert businessAd = getService().selectOne(e);
        if (businessAd == null) 
        {
            getService().insert(e);
            insertAfter(e);
            addMessage(flushAttrs, "操作成功！");
            return "redirect:selectList?businessId=" + u.getBusinessId();
        }
        else
        {
        	e.setId(businessAd.getId());
        	getService().update(e);
        }
        addMessage(flushAttrs, "同一个广告位不能重复添加!");
        return "redirect:selectList?businessId=" + u.getBusinessId();
    }

    /**
     * app轮播图的批量删除数据的方法，子类可以通过重写此方法实现个性化的需求。
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "deleteApp", method = RequestMethod.POST)
    public String deleteApp(HttpServletRequest request, String[] ids, @ModelAttribute("e") BusinessAdvert e, RedirectAttributes flushAttrs) throws Exception 
    {
//		User user = (User) getSession().getAttribute(Global.USER_INFO);
//		if(user==null){
//			throw new NullPointerException();
//		}
//		if(user.getDbPrivilegeMap()!=null && user.getDbPrivilegeMap().size()>0){
//			if(user.getDbPrivilegeMap().get(Container.db_privilege_delete)==null){
//				throw new PrivilegeException(Container.db_privilege_delete_error);
//			}
//		}
        User u = LoginUserHolder.getLoginUser();
        businessAdvertService.deletess(ids);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList?businessId=" + u.getBusinessId();
    }

    /**
     * 公共的分页方法
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectList")
    public String selectList(HttpServletRequest request, @ModelAttribute("e") BusinessAdvert e) throws Exception 
    {
        User u = LoginUserHolder.getLoginUser();
        e.setBusinessId(u.getBusinessId());
        /**
         * 由于prepare方法不具备一致性，加此代码解决init=y查询的时候条件不被清除干净的BUG
         */
        this.initPageSelect();

        setParamWhenInitQuery(e);

        int offset = 0;//分页偏移量
        if (request.getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(request.getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        e.setOffset(offset);
        PagerModel pager = getService().selectPageList(e);
        if (pager == null) 
        {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1) / pager.getPageSize());

        selectListAfter(pager);
        request.setAttribute("pager", pager);
        return page_toList;
    }

    /**
     * 同步缓存
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("syncCache")
    public String syncCache(HttpServletRequest request, BusinessAdvert img) throws Exception 
    {
//		SystemSingle.getInstance().sync(Container.imgList);
        return super.selectList(request, img);
    }
}
