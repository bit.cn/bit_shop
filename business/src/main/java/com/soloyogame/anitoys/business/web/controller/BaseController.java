package com.soloyogame.anitoys.business.web.controller;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.soloyogame.anitoys.business.web.controller.util.RequestHolder;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.page.PagerModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 基础的拦截器
 * @author shaojian
 * @param <E>
 */
public abstract class BaseController<E extends PagerModel> 
{

	protected Logger logger = LoggerFactory.getLogger(getClass());
    protected String page_toList = null;
    protected String page_toEdit = null;
    protected String page_toAdd = null;
    public abstract Services<E> getService();



    /**
     * 后台左边导航菜单的初始化查询方法
     */
    protected void initPageSelect() 
    {
        logger.error("initPageSelect..init=n!");
    }

    /**
     * 初始化查询的时候，会清除所有的查询参数(所以在e中的)，但可以设置不在e中的参数，然后在此方法中进行e.setXxx(参数)的方式进行保留。
     */
    protected void setParamWhenInitQuery(E e) 
    {
        //BaseAction 的子类如有初始化页面的时候进行相关查询 ，则可以实现此方法。
    }

    /**
     * 公共的分页方法
     * @return
     * @throws Exception
     */
    @RequestMapping("selectList")
    public String selectList(HttpServletRequest request, @ModelAttribute("e") E e) throws Exception 
    {
        /**
         * 由于prepare方法不具备一致性，加此代码解决init=y查询的时候条件不被清除干净的BUG
         */
        this.initPageSelect();

        setParamWhenInitQuery(e);

        int offset = 0;//分页偏移量
        if (request.getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(request.getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        e.setOffset(offset);
        PagerModel pager = getService().selectPageList(e);
        if (pager == null) 
        {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)/ pager.getPageSize());

        selectListAfter(pager);
        request.setAttribute("pager", pager);
        return page_toList;
    }

    @RequestMapping("toEdit")
    public String toEdit(@ModelAttribute("e") E e, ModelMap model) throws Exception 
    {
        e = getService().selectOne(e);
        model.addAttribute("e", e);
        return page_toEdit;
    }

    @RequestMapping("toAdd")
    public String toAdd(@ModelAttribute("e") E e, ModelMap model) throws Exception 
    {
        e.clear();
        return page_toAdd;
    }

    /**
     * 子类必须要实现的方法当分页查询后.
     * 解决了用户先点击新增按钮转到新增页面,然后点击返回按钮返回后,再点分页控件出错的BUG.
     * 原因是分页查询后未将pageUrl重新设置为正确的URL所致
     */
    protected void selectListAfter(PagerModel pager) 
    {
        pager.setPagerUrl("selectList");
    }

    /**
     * 返回到查询页面
     * @return
     * @throws Exception
     */
    @RequestMapping("back")
    public String back(@ModelAttribute("e") E e, ModelMap model) throws Exception 
    {
        return selectList(RequestHolder.getRequest(), e);
    }
    
    /**
     * 根据主键ID删除对象
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "deleteById", method = RequestMethod.POST)
    public String deleteById(HttpServletRequest request, String id, RedirectAttributes flushAttrs) throws Exception 
    {
    	getService().deleteById(Integer.valueOf(id).intValue());
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
    
    /**
     * 公共的批量删除数据的方法，子类可以通过重写此方法实现个性化的需求。
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "deletes", method = RequestMethod.POST)
    public String deletes(HttpServletRequest request, String[] ids, @ModelAttribute("e") E e, RedirectAttributes flushAttrs) throws Exception 
    {
        getService().deletes(ids);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }

    /**
     * 公共的更新数据的方法，子类可以通过重写此方法实现个性化的需求。
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String update(HttpServletRequest request, @ModelAttribute("e") E e, RedirectAttributes flushAttrs) throws Exception 
    {
        getService().update(e);
        insertAfter(e);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
    
   

   
    /**
     * insert之后，selectList之前执行的动作，一般需要清除添加的E，否则查询会按照E的条件进行查询.
     * 部分情况下需要保留某些字段，可以选择不清除
     * @param e
     */
    protected void insertAfter(E e)
    {
    }

    /**
     * 公共的插入数据方法，子类可以通过重写此方法实现个性化的需求。
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "insert",method = RequestMethod.POST)
    public String insert(HttpServletRequest request, @ModelAttribute("e") E e, RedirectAttributes flushAttrs) throws Exception 
    {
        getService().insert(e);
        insertAfter(e);
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }
    
    

    @RequestMapping("loadData")
    @ResponseBody
    public PagerModel loadData(HttpServletRequest request, E e)
    {
        int offset = 0;
        int pageSize = 10;
        if (request.getParameter("start") != null) 
        {
            offset = Integer
                    .parseInt(request.getParameter("start"));
        }
        if (request.getParameter("length") != null) 
        {
            pageSize = Integer.parseInt(request.getParameter("length"));
        }
        if (offset < 0)
            offset = 0;
        if(pageSize < 0)
        {
            pageSize = 10;
        }
        e.setOffset(offset);
        e.setPageSize(pageSize);
        PagerModel pager = getService().selectPageList(e);
        pager.setRecordsTotal(pager.getTotal());
        pager.setRecordsFiltered(pager.getTotal());
        return pager;
    }
    
   
    
    public void writeStream(HttpServletResponse response,String str,String encode) 
	{
		DataOutputStream dataOut = null;
		OutputStream respOS = null;
		try 
		{
			respOS = response.getOutputStream();
			dataOut = new DataOutputStream(respOS);
			dataOut.write(str.getBytes(encode));
		} 
		catch (IOException e) 
		{
		} 
		finally 
		{
			try 
			{
				if(dataOut != null)
				{
					dataOut.flush();
					dataOut.close();
				}
				if(respOS != null) 
				{
					respOS.flush();
					respOS.close();
				}
			} 
			catch (IOException e) 
			{
			}
		}
	}
    
    /**
     * 导出excel
     * @param htmlTable
     * @throws Exception
     */
    @RequestMapping("exportExcel")
    public void  exportExcel(HttpServletResponse response,String htmlTable) throws Exception 
    {
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-disposition","attachment; filename=export_data.xls");
		this.writeStream(response,htmlTable, "UTF-16");
	}
    
    protected void addMessage(ModelMap modelMap, String message) 
    {
        modelMap.addAttribute("message", message);
    }
    protected void addWarning(ModelMap modelMap, String warning) 
    {
        modelMap.addAttribute("warning", warning);
    }
    protected void addError(ModelMap modelMap, String warning) 
    {
        modelMap.addAttribute("errorMsg", warning);
    }
    protected void addMessage(RedirectAttributes flushAttrs, String message) 
    {
        flushAttrs.addFlashAttribute("message", message);
    }
    protected void addWarning(RedirectAttributes flushAttrs, String warning) 
    {
        flushAttrs.addFlashAttribute("warning", warning);
    }
    protected void addError(RedirectAttributes flushAttrs, String warning) 
    {
        flushAttrs.addFlashAttribute("errorMsg", warning);
    }
}
