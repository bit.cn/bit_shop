package com.soloyogame.anitoys.business.web.controller.util;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.soloyogame.anitoys.db.bean.User;
import com.soloyogame.anitoys.db.commond.Account;
import com.soloyogame.anitoys.util.constants.ManageContainer;
import com.soloyogame.anitoys.util.front.FrontContainer;

/**
 * Created by shaojian on 15-11-30.
 */
public class LoginUserHolder 
{
	public static User getLoginUser()
    {
        HttpSession session = RequestHolder.getSession();
        return session == null ? null : (User)session.getAttribute(ManageContainer.manage_session_user_info);
    }
    
    public static Account getLoginAccount()
    {
        HttpSession session = RequestHolder.getSession();
        Account account = (Account)session.getAttribute(FrontContainer.USER_INFO);
        if(account != null && StringUtils.isBlank(account.getId())) {
        	account.setId(account.getId());
        	session.setAttribute(FrontContainer.USER_INFO, account);
        } 
        return session == null ? null : account;
    }
}
