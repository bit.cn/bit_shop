package com.soloyogame.anitoys.business.web.listener;

import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

public class XContextLoaderListener extends ContextLoaderListener
{
	public void contextInitialized(ServletContextEvent event)
	{
		System.setProperty("user.timezone", "Asia/Shanghai");//设置系统时区
		ServletContext c=event.getServletContext();
		super.contextInitialized(event);
	}

}
