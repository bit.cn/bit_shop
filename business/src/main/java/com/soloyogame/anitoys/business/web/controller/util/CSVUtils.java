package com.soloyogame.anitoys.business.web.controller.util;

import com.soloyogame.anitoys.db.commond.OrderWork;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.List;

/**
 * CSV文件导出工具类
 *
 * @author shaojian
 * @reviewer
 */
public class CSVUtils {
    public static byte[] createCSVBytes(String[] head, List<OrderWork> dataList, HttpServletRequest request) {
        StringBuffer csvString = new StringBuffer();
        for (String h : head) {
            csvString.append("\"" + h + "\",");
        }
        csvString.append("\r\n");
        for (OrderWork data : dataList) {
            csvString.append("\"" + data.getOrderCode() + "\",");
            csvString.append("\"" + data.getCode() + "\",");
            csvString.append("\"" + data.getProductName() + "\",");
            csvString.append("\"" + data.getProductPrice() + "\",");
            csvString.append("\"" + data.getNumber() + "\",");
            csvString.append("\"" + data.getAccount() + "\",");
            csvString.append("\"" + data.getShipAddress() + "\",");
            csvString.append("\"" + data.getPhone() + "\",");
            csvString.append("\"" + data.getShipName() + "\",");
            csvString.append("\"" + data.getExpressName() + "\",");
            csvString.append("\"" + data.getExpressNo() + "\",");
            csvString.append("\"" + data.getDownOrderTime() + "\",");
            csvString.append("\"" + data.getBespeakTime() + "\",");
            csvString.append("\"" + data.getReplenishmentTime() + "\",");
            csvString.append("\"" + data.getFinishreplenishmentTime() + "\",");
            csvString.append("\"" + ("1".equals(data.getIsCustomer()) ? "是" : "否") + "\",");
            if (!StringUtils.isBlank(data.getStatus())) {
                if (com.soloyogame.anitoys.db.bean.Order.order_status_init.equals(data.getStatus())) {
                    csvString.append("\"" + "待付款" + "\",");
                } else if (com.soloyogame.anitoys.db.bean.Order.order_status_pass.equals(data.getStatus())) {
                    csvString.append("\"" + "待补款" + "\",");
                } else if (com.soloyogame.anitoys.db.bean.Order.order_status_stay.equals(data.getStatus())) {
                    csvString.append("\"" + "待发货" + "\",");
                } else if (com.soloyogame.anitoys.db.bean.Order.order_status_send.equals(data.getStatus())) {
                    csvString.append("\"" + "待收货" + "\",");
                } else if (com.soloyogame.anitoys.db.bean.Order.order_status_sign.equals(data.getStatus())) {
                    csvString.append("\"" + "已完成" + "\",");
                } else if (com.soloyogame.anitoys.db.bean.Order.order_status_none.equals(data.getStatus())) {
                    csvString.append("\"" + "已取消" + "\",");
                } else if (com.soloyogame.anitoys.db.bean.Order.order_status_cancel.equals(data.getStatus())) {
                    csvString.append("\"" + "已取消" + "\",");
                } else if (com.soloyogame.anitoys.db.bean.Order.order_status_file.equals(data.getStatus())) {
                    csvString.append("\"" + "已归档" + "\",");
                } else {
                    csvString.append("\"" + "\",");
                }
            } else {
                csvString.append("\"" + "\",");
            }
            csvString.append("\"" + data.getAmount() + "\",");
            csvString.append("\"" + data.getFee() + "\",");
            csvString.append("\"" + data.getAmountExchangeScore() + "\",");
            csvString.append("\"" + data.getBuyerCoupon() + "\",");
            csvString.append("\"" + data.getBuyerCouponPlat() + "\",");
            csvString.append("\"" + data.getBuyerAmount() + "\",");
            csvString.append("\"" + data.getBuyerPayMoney() + "\",");
            csvString.append("\"" + data.getPtotal() + "\",");
            csvString.append("\"" + data.getParentId() + "\",");
            csvString.append("\"" + data.getFirstExchangeScore() + "\",");
            csvString.append("\"" + data.getSecondExchangeScore() + "\",");
            csvString.append("\"" + data.getFirstDeductible() + "\",");
            csvString.append("\"" + data.getSecondDeductible() + "\",");
            csvString.append("\"" + data.getFirstPayAccount() + "\",");
            csvString.append("\"" + data.getSecondPayAcount() + "\",");
            csvString.append("\r\n");
        }
        return csvString.toString().getBytes();
    }

    /**
     * CSV文件生成方法
     *
     * @param head
     * @param dataList
     * @param outPutPath
     * @param filename
     * @return
     */
    public static File createCSVFile(List<Object> head, List<List<Object>> dataList, String outPutPath, String filename) {
        File csvFile = null;
        BufferedWriter csvWtriter = null;
        try {
            csvFile = new File(outPutPath + File.separator + filename + ".csv");
            File parent = csvFile.getParentFile();
            if (parent != null && !parent.exists()) {
                parent.mkdirs();
            }
            csvFile.createNewFile();

            // GB2312使正确读取分隔符","
            csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                    csvFile), "GB2312"), 1024);
            // 写入文件头部
            writeRow(head, csvWtriter);

            // 写入文件内容
            for (List<Object> row : dataList) {
                writeRow(row, csvWtriter);
            }
            csvWtriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                csvWtriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return csvFile;
    }

    /**
     * 写一行数据方法
     *
     * @param row
     * @param csvWriter
     * @throws IOException
     */
    private static void writeRow(List<Object> row, BufferedWriter csvWriter) throws IOException {
        // 写入文件头部
        for (Object data : row) {
            StringBuffer sb = new StringBuffer();
            String rowStr = sb.append("\"").append(data).append("\",").toString();
            csvWriter.write(rowStr);
        }
        csvWriter.newLine();
    }
}