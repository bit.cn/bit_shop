package com.soloyogame.anitoys.business.web.controller.manage.orderRepairs;


import java.util.Date;







import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.soloyogame.anitoys.business.web.controller.BaseController;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.OrderRepairs;
import com.soloyogame.anitoys.service.OrderRepairsService;
import com.soloyogame.anitoys.service.OrderService;

/**
 * 售后控制类
 * @author shaojian
 */
@Controller
@RequestMapping("/manage/orderRepairs/")
public class OrderRepairsAction extends BaseController<OrderRepairs>
{
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(OrderRepairsAction.class);
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private OrderRepairsService orderRepairsService;
	@Autowired
	private OrderService orderService;
	
	private static final String page_toList = "/manage/orderRepairs/orderRepairsList";
	private static final String page_toAdd = "/manage/orderRepairs/orderRepairsEdit";
	private static final String page_toEdit = "/manage/orderRepairs/orderRepairsEdit";
	
	private OrderRepairsAction() 
	{
		super.page_toList = page_toList;
		super.page_toAdd = page_toAdd;
		super.page_toEdit = page_toEdit;
	}

	@Override
	public Services<OrderRepairs> getService() 
	{
		return orderRepairsService;
	}
	
	/**
	 * 审批售后的申请
	 */
	@RequestMapping("examineOrderRepairs")
	public String examineOrderRepairs(@ModelAttribute("e") OrderRepairs e,ModelMap model)
	{
		e.setProcessingTime(new Date());
		e.setStatus(3);
		getService().update(e);
		e = getService().selectOne(e);
		model.addAttribute("e", e);
		return page_toEdit;
	}
}
