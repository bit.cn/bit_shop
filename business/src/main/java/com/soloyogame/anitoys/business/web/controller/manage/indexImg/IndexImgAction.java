package com.soloyogame.anitoys.business.web.controller.manage.indexImg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.soloyogame.anitoys.business.web.controller.BaseController;
import com.soloyogame.anitoys.business.web.controller.util.LoginUserHolder;
import com.soloyogame.anitoys.business.web.controller.util.RequestHolder;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.bean.User;
import com.soloyogame.anitoys.db.commond.IndexImg;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.IndexImgService;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;

/**
 * 滚动图片
 *
 * @author 索罗游
 */
@Controller
@RequestMapping("/manage/indexImg/")
public class IndexImgAction extends BaseController<IndexImg> {
    private static final long serialVersionUID = 1L;
    private static final String page_toList = "/manage/indexImg/indexImgList";
    private static final String page_toEdit = "/manage/indexImg/indexImgEdit";
    private static final String page_toAdd = "/manage/indexImg/indexImgEdit";

    private IndexImgAction() {
        super.page_toList = page_toList;
        super.page_toAdd = page_toAdd;
        super.page_toEdit = page_toEdit;
    }

    @Autowired
    private IndexImgService imgService;

    @Override
    public Services<IndexImg> getService() {
        return imgService;
    }

    @Override
    public void insertAfter(IndexImg e) {
        e.clear();
    }

    @Override
    protected void selectListAfter(PagerModel pager) {
        pager.setPagerUrl("selectList");
    }

    /**
     * 公共的分页方法
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectList")
    public String selectList(HttpServletRequest request, @ModelAttribute("e") IndexImg e) throws Exception {
        User user = LoginUserHolder.getLoginUser();
        this.initPageSelect();
        e.setBusinessId(user.getBusinessId());
        setParamWhenInitQuery(e);
        int offset = 0;//分页偏移量
        if (request.getParameter("pager.offset") != null) {
            offset = Integer.parseInt(request.getParameter("pager.offset"));
        }
        if (offset < 0)
            offset = 0;
        e.setOffset(offset);
        PagerModel pager = getService().selectPageList(e);
        if (pager == null) {
            pager = new PagerModel();
        }
        // 计算总页数
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1) / pager.getPageSize());

        selectListAfter(pager);
        request.setAttribute("pager", pager);
        return page_toList;
    }


    //上传文件
    @Deprecated
    private void uploadImage(MultipartFile image, IndexImg e) throws IOException {
        if (image == null) {
            return;
        }
        String imageName = String.valueOf(System.currentTimeMillis()) + ".jpg";
        String realpath = RequestHolder.getSession().getServletContext().getRealPath("/indexImg/");
        logger.info("realpath: " + realpath);
        if (image != null) {
            File savefile = new File(new File(realpath), imageName);
            if (!savefile.getParentFile().exists()) {
                savefile.getParentFile().mkdirs();
            }
            image.transferTo(savefile);
        }
        String url = "/indexImg/" + imageName;
        e.setPicture(url);
        image = null;
    }

    /**
     * 同步缓存
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("syncCache")
    public String syncCache(HttpServletRequest request, IndexImg img) throws Exception {
        return super.selectList(request, img);
    }

    /**
     * 公共的插入数据方法，子类可以通过重写此方法实现个性化的需求。
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "insert", method = RequestMethod.POST)
    public String insert(HttpServletRequest request, @ModelAttribute("e") IndexImg e, RedirectAttributes flushAttrs) throws Exception {
        User user = LoginUserHolder.getLoginUser();
        e.setBusinessId(user.getBusinessId());
        IndexImg uniqueIndexImg = new IndexImg();
        uniqueIndexImg.setBusinessId(user.getBusinessId());
        uniqueIndexImg.setOrder1(e.getOrder1());

        try {
            IndexImg indexImg = getService().selectOne(uniqueIndexImg);
            if (indexImg == null) {
                getService().insert(e);
                insertAfter(e);
                addMessage(flushAttrs, "操作成功！");
            }else{
                addMessage(flushAttrs, "不能对同一个广告位重复添加数据! ");
            }
        } catch (Exception e1) {
            addMessage(flushAttrs, "不能对同一个广告位重复添加数据! ");
        }
        return "redirect:selectList";
    }
}
