package com.soloyogame.anitoys.business.web.controller.manage.serviceGuarantee;

import com.alibaba.fastjson.JSON;
import com.soloyogame.anitoys.business.web.controller.BaseController;
import com.soloyogame.anitoys.business.web.controller.util.LoginUserHolder;
import com.soloyogame.anitoys.db.bean.User;
import com.soloyogame.anitoys.db.commond.ServiceGuarantee;
import com.soloyogame.anitoys.service.ServiceGuaranteeService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import com.soloyogame.anitoys.util.constants.ManageContainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 商铺售后保障
 *
 * @author shaojian
 */
@Controller
@RequestMapping("/manage/serviceGuarantee/")
public class ServiceGuaranteeAction extends BaseController<ServiceGuarantee> {
    private static final long serialVersionUID = 1L;
    @Autowired
    private ServiceGuaranteeService serviceGuaranteeService;
    private static final String page_toList = "/manage/serviceGuarantee/serviceGuaranteeEdit";
    @Autowired
    private RedisCacheProvider redisCacheProvider;

    public ServiceGuaranteeService getService() {
        return serviceGuaranteeService;
    }

    public void setServiceGuaranteeService(
            ServiceGuaranteeService serviceGuaranteeService) {
        this.serviceGuaranteeService = serviceGuaranteeService;
    }

    //重写分页查询商铺
    @Override
    @RequestMapping("selectList")
    public String selectList(HttpServletRequest request, @ModelAttribute("e") ServiceGuarantee e) throws Exception {
        try {
            User u = LoginUserHolder.getLoginUser();
            e.setBusinessId(u.getBusinessId());
            ServiceGuarantee serviceGuarantee = serviceGuaranteeService.selectOne(e);
            if (serviceGuarantee == null) {
                serviceGuarantee = new ServiceGuarantee();
                serviceGuarantee.setBusinessId(u.getBusinessId());
                serviceGuaranteeService.insert(serviceGuarantee);
            }
            request.setAttribute("serviceGuarantee", serviceGuarantee);
            return page_toList;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    @Override
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String update(HttpServletRequest request, @ModelAttribute("e") ServiceGuarantee e, RedirectAttributes flushAttrs) throws Exception {
        getService().update(e);
        insertAfter(e);
        User u = LoginUserHolder.getLoginUser();
        e.setBusinessId(u.getBusinessId());
        saveToRedis(e.getBusinessId() + ManageContainer.PRODUCT_SERVICEGUARANTEE, JSON.toJSONString(e));
        addMessage(flushAttrs, "操作成功！");
        return "redirect:selectList";
    }

    private void saveToRedis(final String redisKey, final String value) {
        new Runnable() {
            @Override
            public void run() {
                redisCacheProvider.put(redisKey, value);
            }
        }.run();

    }

}
