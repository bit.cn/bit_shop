package com.soloyogame.anitoys.service;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.Business;
import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 商家的服务接口
 * @author 索罗游
 */
public interface BusinessService extends Services<Business>
{
	public PagerModel selectBusinessDetailsList(Business e);
}
