package com.soloyogame.anitoys.service;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.bean.StrongAdvertising;

public interface StrongAdvertisingService extends Services<StrongAdvertising> 
{
    public void deletess(String[] ids);
    /**
     * 插入时广告位置是否有效
     * 广告位置不能重复
     * @param advertPosition
     * @return
     */
    public boolean orderIsValid(int advertPosition);
    /**
     * 更新时广告位置是否有效
     * 广告位置不能重复
     * @param advertPosition
     * @return
     */
    public boolean updateOrderIsValid(StrongAdvertising advertPosition);
}
