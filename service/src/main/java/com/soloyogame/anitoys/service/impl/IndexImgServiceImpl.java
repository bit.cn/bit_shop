package com.soloyogame.anitoys.service.impl;

import java.util.List;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.IndexImg;
import com.soloyogame.anitoys.db.dao.IndexImgDao;
import com.soloyogame.anitoys.service.IndexImgService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author shaojian
 */
@Service("indexImgService")
public class IndexImgServiceImpl extends ServersManager<IndexImg, IndexImgDao> implements IndexImgService 
{

    @Resource
    @Override
    public void setDao(IndexImgDao indexImgDao) 
    {
        this.dao = indexImgDao;
    }

	@Override
	public List<IndexImg> getImgsShowToIndex(int i) 
	{
		return dao.getImgsShowToIndex(i);
	}
	
	public List<IndexImg> getPlatImgsShowToIndex(int i) 
	{
		return dao.getPlatImgsShowToIndex(i);
	}
	
}
