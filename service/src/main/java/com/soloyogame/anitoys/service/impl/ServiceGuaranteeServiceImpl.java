package com.soloyogame.anitoys.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;





import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.ServiceGuarantee;
import com.soloyogame.anitoys.db.dao.ServiceGuaranteeDao;
import com.soloyogame.anitoys.service.ServiceGuaranteeService;

@Service("serviceGuaranteeServiceManage")
public class ServiceGuaranteeServiceImpl extends ServersManager<ServiceGuarantee,ServiceGuaranteeDao>  implements ServiceGuaranteeService{

    @Resource
    @Override
    public void setDao(ServiceGuaranteeDao serviceGuaranteeDao) {
        this.dao = serviceGuaranteeDao;
    }
}
