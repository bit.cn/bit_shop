package com.soloyogame.anitoys.service;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.UserInfo;

/**
 * 用户信息service
 * @author xukezhen
 *
 */
public interface UserInfoService extends Services<UserInfo> {
	/**
	 * 新增商家联动生成商家用户
	 * @param e
	 * @return
	 */
	public int insertByBusiness(UserInfo e);
}
