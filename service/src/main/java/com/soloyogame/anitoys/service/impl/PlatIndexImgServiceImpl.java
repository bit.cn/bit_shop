package com.soloyogame.anitoys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.IndexImg;
import com.soloyogame.anitoys.db.dao.IndexImgDao;
import com.soloyogame.anitoys.db.dao.PlatIndexImgDao;
import com.soloyogame.anitoys.service.PlatIndexImgService;


/**
 * @author shaojian
 */
@Service("platindexImgService")
public class PlatIndexImgServiceImpl extends ServersManager<IndexImg, PlatIndexImgDao> implements PlatIndexImgService 
{

    @Resource
    @Override
    public void setDao(PlatIndexImgDao indexImgDao) 
    {
        this.dao = indexImgDao;
    }

	@Override
	public List<IndexImg> getImgsShowToIndex(int i) 
	{
		return dao.getImgsShowToIndex(i);
	}
	
	public List<IndexImg> getPlatImgsShowToIndex(int i) 
	{
		return dao.getPlatImgsShowToIndex(i);
	}
	
}
