package com.soloyogame.anitoys.service;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.bean.PrivilegePlat;
import com.soloyogame.anitoys.db.bean.RolePlat;
import com.soloyogame.anitoys.db.page.PagerModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author shapjian 商家角色业务逻辑实现类
 */
@Service
public class RolePlatService implements Services<RolePlat> 
{
    @Resource
	private BaseDao dao;
    @Resource
	private PrivilegePlatService privilegePlatService;

	public void setDao(BaseDao dao) 
	{
		this.dao = dao;
	}

	public PrivilegePlatService getPrivilegeService() {
		return privilegePlatService;
	}

	public void setPrivilegeService(PrivilegePlatService privilegePlatService) {
		this.privilegePlatService = privilegePlatService;
	}

	public PagerModel selectPageList(RolePlat role) 
	{
		return dao.selectPageList("rolePlat.selectPageList",
				"rolePlat.selectPageCount", role);
	}

	public List selectList(RolePlat role) 
	{
		return dao.selectList("rolePlat.selectList", role);
	}

	public RolePlat selectOne(RolePlat role) 
	{
		return (RolePlat) dao.selectOne("rolePlat.selectOne", role);
	}

	public int insert(RolePlat role) 
	{
		return dao.insert("rolePlat.insert", role);
	}

	/**
	 * 删除指定角色以及该角色下的所有权限
	 * 
	 * @param role
	 */
	public int delete(RolePlat role) 
	{
		// 删除角色
		dao.delete("rolePlat.delete", role);
		// 删除角色对应的权限
        privilegePlatService.deleteByRole(role);
		return 0;
	}

	public int update(RolePlat role) 
	{
		return dao.update("rolePlat.update", role);
	}

	/**
	 * 编辑角色
	 * 
	 * @param role
	 * @throws Exception
	 */
	public void editRole(RolePlat role, String insertOrUpdate) throws Exception {
		int insertRole = 0;
		PrivilegePlat privilege = new PrivilegePlat();
		if (insertOrUpdate.equals("1")) {
			// 新增角色
			insertRole = insert(role);
		} else {
			// 修改角色
			insertRole = update(role);
			// 删除角色的所有权限
			privilege.setRid(String.valueOf(insertRole));
            privilegePlatService.delete(privilege);
		}

		// 赋予权限
		if (role.getPrivileges() == null
				|| role.getPrivileges().trim().equals(""))
			return;

		String[] pArr = role.getPrivileges().split(",");
		for (int i = 0; i < pArr.length; i++) {
			privilege.clear();

			privilege.setMid(pArr[i]);
			privilege.setRid(String.valueOf(insertRole));
            privilegePlatService.insert(privilege);
		}
	}

	/**
	 * 批量删除角色
	 * 
	 * @param ids
	 */
	public int deletes(String[] ids) 
	{
		RolePlat role = new RolePlat();
		for (int i = 0; i < ids.length; i++) 
		{
			role.setId(ids[i]);
			delete(role);
			role.clear();
		}
		return 0;
	}

	@Override
	public RolePlat selectById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteById(int id) 
	{
		RolePlat role = new RolePlat();
		role.setId(String.valueOf(id));
		delete(role);
		role.clear();
		return 0;
	}

}
