package com.soloyogame.anitoys.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.dao.HelpDao;

import javax.annotation.Resource;

import com.soloyogame.anitoys.db.commond.Help;
import com.soloyogame.anitoys.service.HelpService;

/**
 * @author 索罗游
 */
@Service("helpServiceManage")
public class HelpServiceImpl extends ServersManager<Help, HelpDao> implements HelpService 
{
	@Resource
    public void setDao(HelpDao helpDao) {
        this.dao = helpDao;
    }
	
	/**
	 * @param e
	 */
	public List<Help> selecIndexHelp(Help e) {
		return dao.selecIndexHelp(e);
	}

	@Override
	public void updateStatus(String[] ids, String status) {
		if(ids==null || ids.length==0){
			return;
		}
		
		for(int i=0;i<ids.length;i++){
			Help help = new Help();
			help.setId(ids[i]);
			help.setStatus(status);
			dao.sync(help);
		}
//		throw new NullPointerException();
	}

	@Override
	public void updateDownOrUp(Help help) {
		dao.updateDownOrUp(help);
	}

	@Override
	public int selectCount(Help help) {
		return dao.selectCount(help);
	}
	
	/**
     * 前端查询帮助支持列表
     * @param e
     * @return
     */
	@Override
    public List<Help> selectFrontList(Help e) {
		return dao.selectFrontList(e);
	}
}
