package com.soloyogame.anitoys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.OrderWork;
import com.soloyogame.anitoys.db.dao.OrderWorkDao;
import com.soloyogame.anitoys.service.OrderWorkService;
@Service("orderWorkServiceManage")
public class OrderWorkServiceImpl extends ServersManager<OrderWork,OrderWorkDao> implements OrderWorkService {
	 @Resource
		@Override
			public void setDao(OrderWorkDao orderWorkDao) {
		        this.dao = orderWorkDao;
			}
}
