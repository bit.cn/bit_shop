package com.soloyogame.anitoys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.FavoriteBusiness;
import com.soloyogame.anitoys.db.commond.FavoriteBusinessResult;
import com.soloyogame.anitoys.db.dao.FavoriteBusinessDao;
import com.soloyogame.anitoys.service.FavoriteBusinessService;

import org.springframework.stereotype.Service;

@Service("favoriteBusinessServiceFront")
public class FavoriteBusinessServiceImpl extends ServersManager<FavoriteBusiness, FavoriteBusinessDao> implements FavoriteBusinessService {

	@Resource
	@Override
	public void setDao(FavoriteBusinessDao favoriteBusinessDao) {
		 this.dao = favoriteBusinessDao;
		
	}

	@Override
	public int selectCount(FavoriteBusiness favorite) {
		return dao.selectCount(favorite);
	}

	//查询用户收藏的店铺
	@Override
	public List<FavoriteBusinessResult> searchFavoriteBusiness(
			FavoriteBusiness favBusi) {
		return dao.selectFavoriteBusiness(favBusi) ;
	}

	//查询用户收藏的店铺ID,不关联店铺内商品信息
	@Override
	public List<FavoriteBusinessResult> searchFavoriteBusinessID(
			FavoriteBusiness favBusi) {
		return dao.selectFavoriteBusinessId(favBusi) ;
	}

	@Override
	public FavoriteBusinessResult searchBusinessInfo(FavoriteBusinessResult favoriteBusinessResult) {
		return dao.selectBusinessInfo(favoriteBusinessResult);
	}

	//根据用户ID和店铺ID，查询是否已收藏过该店铺
	@Override
	public List<FavoriteBusinessResult> searchFavoriteBusinessByAccountBusiID(
			FavoriteBusiness favBusi) {
		
		 return dao.selectFavoriteBusinessByAccountBusiID(favBusi) ;
	}

}
