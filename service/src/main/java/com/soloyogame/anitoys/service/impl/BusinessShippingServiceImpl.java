package com.soloyogame.anitoys.service.impl;


import java.util.List;
import java.util.Map;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.BusinessShipping;
import com.soloyogame.anitoys.db.dao.BusinessShippingDao;
import com.soloyogame.anitoys.service.BusinessShippingService;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 物流运费管理服务接口
 * @author shaojian
 */
@Service
public class BusinessShippingServiceImpl extends ServersManager<BusinessShipping, BusinessShippingDao> implements BusinessShippingService 
{

    @Resource
    @Override
    public void setDao(BusinessShippingDao businessShippingDao) 
    {
        this.dao = businessShippingDao;
    }

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public int deletes(String[] ids) 
	{
		if (ids == null || ids.length == 0) 
		{
			throw new NullPointerException("id不能全为空！");
		}
		
		for (int i = 0; i < ids.length; i++) 
		{
			if(StringUtils.isBlank(ids[i]))
			{
				throw new NullPointerException("id不能为空！");
			}
			dao.deleteById(Integer.parseInt(ids[i]));
		}
		return 0;
	}

	/**
	 * 查询卖家支持的快递省份 
	 */
	public List<BusinessShipping> selectPrivence(Map<String, Object> map)
	{
		return dao.selectPrivence(map);
	}
	/**
	 * 设置默认快递,先将商铺快递改为不默认状态id为businessId
	 */
	public int updateDefault(String id){
		return dao.updateDefault(id);
	}
	/**
	 * 根据商铺id查询商铺快递
	 * @param map
	 * @return
	 */
	public List<BusinessShipping> selBusinessShipping(String businessId){
		return dao.selBusinessShipping(businessId);
	}
}
