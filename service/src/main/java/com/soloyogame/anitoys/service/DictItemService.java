package com.soloyogame.anitoys.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.bean.DictItem;
import com.soloyogame.anitoys.db.page.PagerModel;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * 数据字典项业务逻辑实现类
 * @author shaojian
 */
@Service
public class DictItemService implements Services<DictItem> 
{
    @Resource
	private BaseDao dao;

	public void setDao(BaseDao dao) 
	{
		this.dao = dao;
	}

	/**
	 * 查询所有的数据字典
	 * @param param
	 * @return
	 */
	public List<DictItem> selectList(Map<String, String> param) 
	{
		if (param == null)
			return dao.selectList("dictItem.selectList");
		return dao.selectList("dictItem.selectList", param);
	}

	/**
	 * 新增字典项
	 */
	public int insert(DictItem e) 
	{
		return dao.insert("dictItem.insert",e);
	}

	/** 
	 * 删除字典项对象
	 */
	public int delete(DictItem e) 
	{
		return dao.delete("dictItem.delete", e);
	}

	@Override
	public int deletes(String[] ids) {
		return 0;
	}

	/**
	 * 更新字典项
	 */
	@Override
	public int update(DictItem e) 
	{
		return dao.update("dictItem.update", e);
	}

	@Override
	public DictItem selectOne(DictItem e) 
	{
		return (DictItem) dao.selectOne("dictItem.selectOne", e);
	}

	@Override
	public DictItem selectById(String id) {
		return null;
	}

	@Override
	public PagerModel selectPageList(DictItem e) 
	{
		return dao.selectPageList("dict.selectPageList","dict.selectPageCount", e);
	}
	
	/**
	 * 根据条件查询数量
	 * @param user
	 * @return
	 */
	public int selectCount(DictItem dictItem) 
	{
		if(dictItem==null)
		{
			throw new NullPointerException();
		}
		return (Integer) dao.selectOne("dictItem.selectCount",dictItem);
	}
	
	@Override
	public List<DictItem> selectList(DictItem e) {
		return null;
	}
	
	
	/**
	 * 加载数据字典根据
	 */
	
	public List<DictItem> getDictItems(String dic_id) {
		Map<String, String> param = new HashMap<String, String>();
		param.put("dic_id", dic_id);               //字典父ID
		List<DictItem> dictItems = dao.selectList("dictItem.selectDictItems", param);
		return dictItems;
	}
	
	/**
	 * 加载数据字典根节点
	 * @param dic_id
	 * @param url
	 * @return
	 */
	public List<DictItem> loadDictItems(String dic_id) 
	{
		Map<String, String> param = new HashMap<String, String>();
		param.put("dic_id", dic_id);               //字典父ID
		List<DictItem> dictItems = dao.selectList("dictItem.selectDictItems", param);
		// 创建菜单集合
		List<DictItem> root = new ArrayList<DictItem>();
		// 循环添加菜单到菜单集合
		for(int i=0;i<dictItems.size();i++)
		{
			DictItem dictItem = dictItems.get(i);
			List<DictItem> children = new ArrayList<DictItem>();
			if(dictItem.getPid().equals("0"))
			{
				for (DictItem dictItem1 : dictItems) 
				{
					if(dictItem1.getPid().equals(dictItem.getId()))
					{
						children.add(dictItem1);
					}
				}
				dictItem.setChildren(children);
				root.add(dictItem);
			}
		}
		return root;
	}

	/**
	 * 根据id查询dict_item
	 * @param id
	 * @return
	 */
	public DictItem selectDictItemById(String id)
	{
		return (DictItem) dao.selectOne("dictItem.selectOne",id);
	}

	@Override
	public int deleteById(int id) {
		return 0;
	}
	
	/**
	 * 根据菜单ID，删除菜单树勾选的节点。某个非叶子节点即便它下面的所有的叶子节点都被勾选也不会被本次删除操作删除掉，
	 * 这样做是为了避免只想删除某个非叶子节点下面的所有子节点
	 * @param ids
	 * @param deleteParent
	 * 是否级联删除父菜单,在父菜单下的所有子菜单全部勾选的情况下,1:级联删除,-1不级联
	 */
	public void deletesDictItemNode(String ids, String deleteParent) 
	{
		String[] idArr = ids.split(",");
		//System.out.println(Arrays.toString(idArr));
		// 按照从小到大排序
		Arrays.sort(idArr, new Comparator<String>() 
		{
			public int compare(String o1, String o2) 
			{
				int a1 = Integer.parseInt(o1);
				int a2 = Integer.parseInt(o2);
				if (a1 > a2) 
				{
					return 1;
				} 
				else if (a1 < a2) 
				{
					return -1;
				}
				return 0;
			}
		});
		DictItem dictItem = new DictItem();
		if (deleteParent.equals("-1")) 
		{
			// 从菜单ID最小的开始删起，避免先把ID大的删除了，倒置ID小的成为了叶子节点而被删除掉
			for (int i = 0; i < idArr.length; i++) 
			{
				/*
				 * 1、菜单下没有子菜单，直接删除 2、菜单下有子菜单，检查所有的子菜单是否全部已经勾选 A)全部勾选，则可以删除。
				 * B)没有全部勾选，则不能删除。
				 */
				dictItem.clear();
				dictItem.setPid(idArr[i]);
				if (this.getCount(dictItem) == 0) 
				{
					// 指定节点下没有子菜单，删除指定的菜单(叶子)
					dictItem.clear();
					dictItem.setId(idArr[i]);
					this.delete(dictItem);
				}
			}
		} 
		else if (deleteParent.equals("1")) 
		{
			for (int i = idArr.length - 1; i >= 0; i--) 
			{
				/*
				 * 1、菜单下没有子菜单，直接删除 2、菜单下有子菜单，检查所有的子菜单是否全部已经勾选 A)全部勾选，则可以删除。
				 * B)没有全部勾选，则不能删除。
				 */
				dictItem.clear();
				dictItem.setPid(idArr[i]);
				if (this.getCount(dictItem) == 0) 
				{
					// 指定节点下没有子菜单，删除指定的菜单(叶子)
					dictItem.clear();
					dictItem.setId(idArr[i]);
					this.delete(dictItem);
				} 
				else 
				{
					dictItem.clear();
					dictItem.setPid(idArr[i]);
					// 查询指定菜单下的全部子菜单
					List<DictItem> dictItemList = this.selectList(dictItem);
//					System.out.println("find menus:" + menus);
					if (dictItemList != null && dictItemList.size() > 0) 
					{
						if (checkAllContains(idArr, dictItemList)) 
						{
							this.delete(dictItem);
						}
					}
				}
			}
		} 
		else 
		{
			throw new NullPointerException("deleteParent:" + deleteParent);
		}
	}

	public int getCount(DictItem dictItem) 
	{
		return Integer.valueOf(dao.selectOne("dictItem.getCount", dictItem).toString());
	}
	
	/**
	 * 检查指定的菜单列表是否全部存在于另一个列表中
	 * @param idArr
	 * 待删除的菜单列表
	 * @param list
	 * 被检查的菜单列表
	 * @return 全部存在返回true，否则返回false
	 */
	private boolean checkAllContains(String[] idArr, List<DictItem> list) 
	{
		int n = list.size();
		for (int i = 0; i < list.size(); i++) 
		{
			for (int j = 0; j < idArr.length; j++) 
			{
				if (list.get(i).getId().equals(idArr[j])) 
				{
					n--;
					break;
				}
			}
		}
		// System.out.println("=========="+Arrays.toString(idArr)+",list:"+list+",n:"+n);
		return n == 0 ? true : false;
	}
}
