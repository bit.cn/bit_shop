package com.soloyogame.anitoys.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.DeductibleVoucherSn;
import com.soloyogame.anitoys.db.dao.DeductibleVoucherSnDao;
import com.soloyogame.anitoys.service.DeductibleVoucherSnService;

@Service
public class DeductibleVoucherSnServiceImpl extends
		ServersManager<DeductibleVoucherSn, DeductibleVoucherSnDao> implements
		DeductibleVoucherSnService {
	
	    @Resource
	    @Override
	    public void setDao(DeductibleVoucherSnDao deductibleVoucherSnDao) 
	    {
	        this.dao = deductibleVoucherSnDao;
	    }

	@Override
	public int insert(DeductibleVoucherSn e) {
		return dao.insert(e);
	}


	@Override
	public DeductibleVoucherSn findEffectiveDeductibleSnCode(DeductibleVoucherSn deductibleVoucherSn) {
		return this.dao.findEffectiveDeductibleSnCode(deductibleVoucherSn);
	}

	@Override
	public int updateDeductibleVoucherSnReceiveStatus(DeductibleVoucherSn deductibleVoucherSn) {
		return this.dao.update(deductibleVoucherSn);
	}
}
