package com.soloyogame.anitoys.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.bean.Dict;
import com.soloyogame.anitoys.db.bean.DictItem;
import com.soloyogame.anitoys.db.page.PagerModel;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * 数据字典业务逻辑实现类
 * @author shaojian
 */
@Service
public class DictService implements Services<Dict> 
{
    @Resource
	private BaseDao dao;

	public void setDao(BaseDao dao) 
	{
		this.dao = dao;
	}

	/**
	 * 查询所有的数据字典
	 * @param param
	 * @return
	 */
	public List selectList(Map<String, String> param) 
	{
		if (param == null)
			return dao.selectList("dict.selectList");
		return dao.selectList("dict.selectList", param);
	}

	/**
	 * 新增字典
	 */
	public int insert(Dict e) 
	{
		return dao.insert("dict.insert", e);
	}

	
    /**
     * 删除字典对象
     */
	public int delete(Dict e) 
	{
		return dao.delete("dict.delete", e);
	}

	@Override
	public int deletes(String[] ids) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Dict e) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Dict selectOne(Dict e) 
	{
		return (Dict) dao.selectOne("dict.selectOne", e);
	}

	@Override
	public Dict selectById(String id) {
		return null;
	}

	@Override
	public PagerModel selectPageList(Dict e) 
	{
		return dao.selectPageList("dict.selectPageList","dict.selectPageCount", e);
	}
	
	/**
	 * 根据条件查询数量
	 * @param user
	 * @return
	 */
	public int selectCount(Dict dict) 
	{
		if(dict==null)
		{
			throw new NullPointerException();
		}
		return (Integer) dao.selectOne("dict.selectCount",dict);
	}
	
	@Override
	public List<Dict> selectList(Dict e) {
		return null;
	}
	
	
	/**
	 * 加载数据字典根节点
	 * @param dic_id
	 * @param url
	 * @return
	 */
	public List<DictItem> loadDictItems(String dic_id) 
	{
		Map<String, String> param = new HashMap<String, String>();
		param.put("dic_id", dic_id);               //字典父ID
		List<DictItem> dictItems = dao.selectList("dictItem.selectDictItems", param);
		// 创建菜单集合
		List<DictItem> root = new ArrayList<DictItem>();
		// 循环添加菜单到菜单集合
		for(int i=0;i<dictItems.size();i++)
		{
			DictItem dictItem = dictItems.get(i);
			List<DictItem> children = new ArrayList<DictItem>();
			if(dictItem.getPid().equals("0"))
			{
				for (DictItem dictItem1 : dictItems) 
				{
					List<DictItem> children1 = new ArrayList<DictItem>();
					if(dictItem1.getPid().equals(dictItem.getId()))
					{
						for (DictItem dictItem2 : dictItems) 
						{
							List<DictItem> children2 = new ArrayList<DictItem>();
							if(dictItem2.getPid().equals(dictItem1.getId()))
							{
								for (DictItem dictItem3 : dictItems) 
								{
									if(dictItem3.getPid().equals(dictItem2.getId()))
									{
										children2.add(dictItem3);
									}
									dictItem2.setChildren(children2);
								}
								children1.add(dictItem2);
							}
							dictItem1.setChildren(children1);
						}
						children.add(dictItem1);
					}
				}
				dictItem.setChildren(children);
				root.add(dictItem);
			}
		}
		return root;
	}

	/**
	 * 根据id查询dict_item
	 * @param id
	 * @return
	 */
	public DictItem selectDictItemById(String id)
	{
		
		return (DictItem) dao.selectOne("dictItem.selectOne",id);
	}

	/**
	 * 删除对象
	 * @param id
	 * @return
	 */
	public int deleteById(int id) 
	{
		return dao.delete("dict.deleteById", id);
	}
}
