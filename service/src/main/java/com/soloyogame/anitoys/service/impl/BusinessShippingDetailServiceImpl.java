package com.soloyogame.anitoys.service.impl;

import java.util.List;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.BusinessShippingDetail;
import com.soloyogame.anitoys.db.dao.BusinessShippingDetailDao;
import com.soloyogame.anitoys.service.BusinessShippingDetailService;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 物流运费管理服务接口
 * @author shaojian
 */
@Service("businessShippingDetailServiceManage")
public class BusinessShippingDetailServiceImpl extends ServersManager<BusinessShippingDetail, BusinessShippingDetailDao> implements BusinessShippingDetailService 
{

    @Resource
    @Override
    public void setDao(BusinessShippingDetailDao businessShippingDetailDao) 
    {
        this.dao = businessShippingDetailDao;
    }

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public int deletes(String[] ids) 
	{
		if (ids == null || ids.length == 0) 
		{
			throw new NullPointerException("id不能全为空！");
		}
		
		for (int i = 0; i < ids.length; i++) 
		{
			if(StringUtils.isBlank(ids[i]))
			{
				throw new NullPointerException("id不能为空！");
			}
			dao.deleteById(Integer.parseInt(ids[i]));
		}
		return 0;
	}
}
