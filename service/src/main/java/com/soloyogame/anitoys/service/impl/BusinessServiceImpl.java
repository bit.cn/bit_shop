package com.soloyogame.anitoys.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.Business;
import com.soloyogame.anitoys.db.dao.BusinessDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.BusinessService;

@Service
public class BusinessServiceImpl extends ServersManager<Business, BusinessDao> implements BusinessService {
	@Resource
	@Override
	public void setDao(BusinessDao businessDao) {
		this.dao=businessDao;
		
	}
	@Override
	public PagerModel selectBusinessDetailsList(Business e){
		return dao.selectBusinessDetailsList(e);
	}
}
