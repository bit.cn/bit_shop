package com.soloyogame.anitoys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.BusinessAdvert;
import com.soloyogame.anitoys.db.dao.BusinessAdvertDao;
import com.soloyogame.anitoys.service.BusinessAdvertService;
@Service
public class BusinessAdvertServiceImpl extends ServersManager<BusinessAdvert,BusinessAdvertDao> implements BusinessAdvertService {
	@Resource
	@Override
	public void setDao(BusinessAdvertDao businessAdvertDao) {
		this.dao=businessAdvertDao;
		
	}

	public void deletess(String[] ids) {
		
		dao.deletess(ids);
	}

}
