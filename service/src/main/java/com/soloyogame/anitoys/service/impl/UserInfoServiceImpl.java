package com.soloyogame.anitoys.service.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.UserInfo;
import com.soloyogame.anitoys.db.dao.UserInfoDao;
import com.soloyogame.anitoys.service.UserInfoService;



@Service("userInfoServiceManage")
public class UserInfoServiceImpl extends ServersManager<UserInfo, UserInfoDao> implements UserInfoService {
	@Resource
	@Override
	public void setDao(UserInfoDao userInfoDao) {
		this.dao=userInfoDao;
		
	}
	@Override
	public int insertByBusiness(UserInfo e){
		return dao.insertByBusiness(e);
	}
}
