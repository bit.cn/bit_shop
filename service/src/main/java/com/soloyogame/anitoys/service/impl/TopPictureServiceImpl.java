package com.soloyogame.anitoys.service.impl;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.db.dao.TopPictureDao;
import com.soloyogame.anitoys.service.TopPictureService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 商铺首页顶部图片管理服务接口实现
 * @author Liam
 */
@Service("topPictureServiceManage")
public class TopPictureServiceImpl  extends ServersManager<TopPicture, TopPictureDao> implements TopPictureService
{

	@Resource
    @Override
    public void setDao(TopPictureDao topPictureDao) 
    {
        this.dao = topPictureDao;
    }
	
	@Override
	public TopPicture selectFrontOne(TopPicture topPicture) 
	{
		return dao.selectFrontOne(topPicture);
	}

}
