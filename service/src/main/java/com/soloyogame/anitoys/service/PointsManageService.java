package com.soloyogame.anitoys.service;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.AccountPointsLog;

/**
 * 会员账户积分日志
 * @author jiangyongzhi
 */
public interface PointsManageService extends Services<AccountPointsLog> {

}
