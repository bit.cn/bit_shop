package com.soloyogame.anitoys.service;

import java.util.List;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.FavoriteBusiness;
import com.soloyogame.anitoys.db.commond.FavoriteBusinessResult;

public interface FavoriteBusinessService  extends Services<FavoriteBusiness> {
	int selectCount(FavoriteBusiness favorite);
	//查询用户收藏的店铺
	List<FavoriteBusinessResult> searchFavoriteBusiness(FavoriteBusiness favBusi) ;
	
	//查询用户收藏的店铺ID,不关联店铺内商品信息
	List<FavoriteBusinessResult> searchFavoriteBusinessID(FavoriteBusiness favBusi) ;
	
	//根据用户ID和店铺ID，查询是否已收藏过该店铺
	List<FavoriteBusinessResult> searchFavoriteBusinessByAccountBusiID(FavoriteBusiness favBusi) ;
	
	//查询店铺即将收藏的店铺信息
	FavoriteBusinessResult searchBusinessInfo(FavoriteBusinessResult favoriteBusinessResult) ;
}
