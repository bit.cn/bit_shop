package com.soloyogame.anitoys.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.AccountPointsLog;
import com.soloyogame.anitoys.db.dao.PointsManageDao;
import com.soloyogame.anitoys.service.PointsManageService;

/**
 * 会员账户积分日志
 * @author jiangyongzhi
 */
@Service
public class PointsManageServiceImpl  extends ServersManager<AccountPointsLog, PointsManageDao> implements
		PointsManageService{
	
	 	@Resource
	    @Override
	    public void setDao(PointsManageDao pointsManageDao) {
	        this.dao = pointsManageDao;
	    }

}
