package com.soloyogame.anitoys.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.OrderManager;
import com.soloyogame.anitoys.db.dao.OrderManagerDao;
import com.soloyogame.anitoys.service.OrderManagerService;

@Service("orderManagerServiceManage")
public class OrderManagerServiceImpl extends ServersManager<OrderManager,OrderManagerDao> implements OrderManagerService {
	
    @Resource
	@Override
		public void setDao(OrderManagerDao orderManagerDao) {
	        this.dao = orderManagerDao;
		}
}
