package com.soloyogame.anitoys.service;

import java.util.List;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.IndexImg;


/**
 * 图片信息接口
 * @author shaojian
 */
public interface PlatIndexImgService extends Services<IndexImg> 
{

	/**
	 * 加载图片显示到门户
	 * @param i
	 */
	List<IndexImg> getImgsShowToIndex(int i);
	
	/**
	 * 根据图片类型得到APP轮播图
	 * @param type
	 * @return
	 */
	/*List<IndexImg> getImgsByType(int type);*/

}
