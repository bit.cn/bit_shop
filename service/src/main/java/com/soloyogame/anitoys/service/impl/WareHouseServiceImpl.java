package com.soloyogame.anitoys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.bean.WareHouse;
import com.soloyogame.anitoys.db.dao.WareHouseDao;
import com.soloyogame.anitoys.service.WareHouseService;
@Service("wareHouseServiceManage")
public class WareHouseServiceImpl extends ServersManager<WareHouse,WareHouseDao>  implements WareHouseService {

	 @Override
	    @Resource
		public void setDao(WareHouseDao wareHouseDao) {
	        this.dao = wareHouseDao;
		}
}
