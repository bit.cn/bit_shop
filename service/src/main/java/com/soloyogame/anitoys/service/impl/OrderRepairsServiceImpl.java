package com.soloyogame.anitoys.service.impl;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.MyOrderRepairs;
import com.soloyogame.anitoys.db.commond.OrderRepairs;
import com.soloyogame.anitoys.db.dao.OrderRepairsDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.OrderRepairsService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * 售后订单
 * @author jiangyongzhi
 */
@Service
public class OrderRepairsServiceImpl  extends ServersManager<OrderRepairs, OrderRepairsDao> implements
		OrderRepairsService {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(OrderRepairsServiceImpl.class);

	@Autowired
	@Override
	public void setDao(OrderRepairsDao dao) {
		this.dao = dao;
	}
	
	@Override
	public MyOrderRepairs selectRepairSimpleReport(OrderRepairs e) {
		return dao.selectRepairSimpleReport(e);
	}
	
	@Override
	public MyOrderRepairs selectOneOrder(OrderRepairs e) {
		return dao.selectOneOrder(e);
	}
	
	@Override
	public List<MyOrderRepairs> selectOrderInfoList(OrderRepairs e) {
		return dao.selectOrderInfoList(e);
	}

    @Override
	public List<OrderRepairs> selectByUserId(OrderRepairs e) {
		return dao.selectByUserId(e);
	}

	@Override
	public PagerModel selectFrontPageList(OrderRepairs e) {
		return dao.selectFrontPageList(e);
	}
	
	 /**
     * 前端个人中心录入售后单信息
     */
	public int insertFront(OrderRepairs e){
		return dao.insertFront(e);
	}
}
