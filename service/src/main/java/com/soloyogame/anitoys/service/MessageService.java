package com.soloyogame.anitoys.service;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.Message;
import com.soloyogame.anitoys.db.page.PagerModel;


public interface MessageService extends Services<Message>{
	/**
	 * 分页查
	 */
	public PagerModel selectPageList(Message e);
	
	 /**
     * 个人中心前端查询消息列表
     */
	public PagerModel selectFrontPageList(Message e);
}
