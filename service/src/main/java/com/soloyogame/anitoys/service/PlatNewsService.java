package com.soloyogame.anitoys.service;

import java.util.List;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.page.PagerModel;

/**
 * 平台文章接口
 * @author 索罗游
 *
 */
public interface PlatNewsService extends Services<News> {
	public List<News> selecIndexNews(News e);

	/**
	 * @param ids
	 * @param status 2:审核通过,4:审核未通过
	 */
	public void updateStatus(String[] ids, String status);
	
	/**
	 * 更新指定的文章 显示/不显示
	 * @param news
	 */
	public void updateDownOrUp(News news);

	public int selectCount(News news);
	
	//www大首页的news接口
	/**
	 * 加载news表的所有的记录的md5值
	 * @return
	 */
	public List<String> selectAllMd5();

	/**
	 * @param ids
	 */
	public void updateInBlackList(String[] ids);

	/**
	 * @param ids
	 * @param status 2:审核通过,4:审核未通过
	 */
	public void sync(String[] ids, int status);

	/**
	 * 查询通知，门户显示
	 */
	public List<News> selectNoticeList(News news);
	
	/**
     * 加载平台的最新情报列表
     * @param news
     * @return
     */
	public List<News> selectPlatNoticeList(News news);
	
	/**
	 * 加载平台的新闻列表
	 * @return
	 */
	public List<News> selectPlatList(News news);

	/**
	 * 查询文章--不包含文章内容
	 * @param news
	 * @return
	 */
	public News selectSimpleOne(News news);
	
	/**
	 * 查询平台文章
	 * @param news
	 * @return
	 */
	public News selectPlatNewsById(String id);
	
	/**
	 * 平台的文章分页查询
	 * @param e
	 * @return
	 */
	public PagerModel selectPlatNewsPageList(News news);
}
