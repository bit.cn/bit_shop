package com.soloyogame.anitoys.service.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.Message;
import com.soloyogame.anitoys.db.dao.MessageDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.MessageService;

@Service("messageServiceFrot")
public class MessageServiceImpl extends ServersManager<Message, MessageDao> implements MessageService{

	@Resource
	@Override
	public void setDao(MessageDao dao) {
		this.dao = dao;
	}

	@Override
	public PagerModel selectPageList(Message e) {
		return dao.selectPageList(e);
	}

	@Override
	public PagerModel selectFrontPageList(Message e) {
		return dao.selectFrontPageList(e);
	}
	
	
}
