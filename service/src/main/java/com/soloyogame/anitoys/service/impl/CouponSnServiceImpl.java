package com.soloyogame.anitoys.service.impl;

import javax.annotation.Resource;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.CouponSn;
import com.soloyogame.anitoys.db.dao.CouponSnDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.CouponSnService;

@Service
public class CouponSnServiceImpl extends ServersManager<CouponSn, CouponSnDao> implements CouponSnService 
{	
	
	private static final org.slf4j.Logger logger = LoggerFactory
			.getLogger(CouponSnServiceImpl.class);

	
	@Autowired
	private CouponSnDao couponsnDao;
	
	public PagerModel selectPageList(CouponSn couponsn) {
		return couponsnDao.selectPageList(couponsn);	 
	}


	@Resource(name = "couponsnDaoManage")
    @Override
    public void setDao(CouponSnDao couponsnDao) 
    {
        this.dao = couponsnDao;
    }

	@Override
	public CouponSn findEffectiveCouponSnCodeByName(CouponSn couponSn) {
		return couponsnDao.findEffectiveCouponSnCode(couponSn);
	}

	@Override
	public int updateCouponSnReceiveStatus(CouponSn couponSn) {
		return couponsnDao.update(couponSn);
	}
}
