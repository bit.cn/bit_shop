package com.soloyogame.anitoys.service;

import java.util.List;

import com.soloyogame.anitoys.db.commond.BusinessShipping;

/**
 * 
 * 订单-商品寄送方式及运费
 * @author jfh
 *
 */
public interface OrderBusinessShippingService {
	/**
	 * 查询商铺的快递信息列表
	 * @param BusinessId
	 * @return
	 */
    List<BusinessShipping> queryOrderBusinessShipping (String BusinessId);
   
    
}
