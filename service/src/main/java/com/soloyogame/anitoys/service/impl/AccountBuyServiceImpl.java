package com.soloyogame.anitoys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.AccountBuy;
import com.soloyogame.anitoys.db.dao.AccountBuyDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.AccountBuyService;

/**
 * 客户的购买数量的接口实现
 * 
 * @author 索罗游
 */
@Service("accountBuyServiceImpl")
public class AccountBuyServiceImpl extends ServersManager<AccountBuy, AccountBuyDao> implements AccountBuyService {
	private static final org.slf4j.Logger logger = LoggerFactory
			.getLogger(AccountBuyServiceImpl.class);

	@Resource
	@Override
	public void setDao(AccountBuyDao accountBuyDao) {
		this.dao = accountBuyDao;
	}

	/**
	 * 分页查询列表
	 */
	@Override
	public PagerModel selectPageList(AccountBuy coupon) {
		return dao.selectPageList(coupon);
	}

	@Override
	public int insert(AccountBuy e) {
		return dao.insert(e);
	}
	
	@Override
	public AccountBuy selectOne(AccountBuy e) {
		return dao.selectOne(e);
	}

	
}
