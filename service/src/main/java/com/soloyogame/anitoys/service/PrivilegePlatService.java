package com.soloyogame.anitoys.service;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.bean.PrivilegePlat;
import com.soloyogame.anitoys.db.bean.RolePlat;
import com.soloyogame.anitoys.db.page.PagerModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * 权限业务逻辑实现类
 * 
 * @author huangf
 * 
 */
@Service
public class PrivilegePlatService implements Services<PrivilegePlat> {
    @Resource
	private BaseDao dao;

	public void setDao(BaseDao dao) {
		this.dao = dao;
	}

	public List<PrivilegePlat> selectList(PrivilegePlat privilege) {
		if (privilege == null)
			return dao.selectList("privilegePlat.selectList");
		return dao.selectList("privilegePlat.selectList", privilege);
	}

	public PrivilegePlat selectOne(PrivilegePlat privilege) {
		return (PrivilegePlat) dao.selectOne("privilegePlat.selectOne", privilege);
	}

	public int insert(PrivilegePlat privilege) {
		return dao.insert("privilegePlat.insert", privilege);
	}

	public int delete(PrivilegePlat privilege) {
		return dao.delete("privilegePlat.delete", privilege);
	}

	public int update(PrivilegePlat privilege) {
		return dao.update("privilegePlat.update", privilege);
	}

	/**
	 * 根绝角色删除权限
	 * 
	 * @param role
	 */
	public void deleteByRole(RolePlat role) {
		PrivilegePlat privilege = new PrivilegePlat();
		privilege.setRid(role.getId());
		delete(privilege);
	}

	public PagerModel selectPageList(PrivilegePlat e) {
		// TODO Auto-generated method stub
		return null;
	}

	public int deletes(String[] ids) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public PrivilegePlat selectById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteById(int id) {
		// TODO Auto-generated method stub
		return 0;
	}
}
