package com.soloyogame.anitoys.service.impl;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.AccountFinance;
import com.soloyogame.anitoys.db.commond.AccountRankLog;
import com.soloyogame.anitoys.db.dao.AccountFinanceDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.AccountFinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("accountFinanceService")
public class AccountFinanceServiceImpl extends
		ServersManager<AccountFinance, AccountFinanceDao> implements
		AccountFinanceService {

	@Override
	public AccountFinance queryAccountFinanceByUserId(String userId) {
		return dao.selectbyUserId(userId);
	}

	@Override
	public int cutAccountRank(String userId, int value) {
		// 减少积分不能大于当前账户最大积分
		AccountFinance aFinance = getAccountFinance(userId);
		if (aFinance != null) {
			if (value != 0) {
				if (aFinance.getRank() >= value) {
					aFinance.setRank(aFinance.getRank()-value);
					return dao.updateRankByUserId(aFinance);
				}
			}
		}
		return 0;
	}

	public AccountFinance getAccountFinance(String userId) {
		AccountFinance aFinance = queryAccountFinanceByUserId(userId);
		if (aFinance != null) {
			return aFinance;
		}
		return null;
	}

	@Override
	public int cutAccountAmount(String userId, String value) {
		AccountFinance aFinance = getAccountFinance(userId);
		if (aFinance != null) {
			if (aFinance.getAmount() != null) {
				aFinance.setAmount(value);
//				double money = Double.parseDouble(value);
//				double amount = Double.parseDouble(aFinance.getAmount());
//				aFinance.setAmount((amount - money)+"");
				return dao.updateAmountByUserId(aFinance);
			}
		}
		return 0;
	}
	
	@Autowired
	@Override
	public void setDao(AccountFinanceDao dao) {
		this.dao = dao;
	}

	 /**
		 * 根据用户id查询积分余额
		 */
		public AccountFinance selAccountFinanceByUserId(String userId){
			return dao.selAccountFinanceByUserId(userId);
		}
		/**
		 * 修改用户积分值(type:1.减 2.加  rank:变动值  id:用户id)
		 */
		public int updateAccountScore(Map<String,Object> map){
			return dao.updateAccountScore(map);
		}
		
		public int insertRankLog(AccountRankLog e) {
			return dao.insertRankLog(e);
		}
		
		public int updateAccountAmount(Map<String, Object> map) {
			return dao.updateAccountAmount(map);
		}
		
		public PagerModel selectRankLogPageList(AccountRankLog e) {
			return dao.selectRankLogPageList(e);
		}

}
