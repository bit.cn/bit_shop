package com.soloyogame.anitoys.service;

import java.util.List;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.Help;

/**
 * 大首页帮助支持的service服务
 * @author 索罗游
 */
public interface HelpService extends Services<Help> 
{
	public List<Help> selecIndexHelp(Help e);

	/**
	 * @param ids
	 * @param status 2:审核通过,4:审核未通过
	 */
	public void updateStatus(String[] ids, String status);
	
	/**
	 * 更新指定的文章 显示/不显示
	 * @param news
	 */
	public void updateDownOrUp(Help help);

	public int selectCount(Help help);
	
	/**
     * 前端查询帮助支持列表
     * @param e
     * @return
     */
    public List<Help> selectFrontList(Help e);
}
