package com.soloyogame.anitoys.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.TopPicture;
import com.soloyogame.anitoys.db.dao.PlatTopPictureDao;
import com.soloyogame.anitoys.service.PlatTopPictureService;

/**
 * 平台顶部图片管理服务接口实现
 * @author 索罗游
 */
@Service("platTopPictureService")
public class PlatTopPictureServiceImpl  extends ServersManager<TopPicture, PlatTopPictureDao> implements PlatTopPictureService
{

	@Resource
    @Override
    public void setDao(PlatTopPictureDao topPictureDao) 
    {
        this.dao = topPictureDao;
    }
}
