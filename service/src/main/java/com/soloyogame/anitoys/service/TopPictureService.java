package com.soloyogame.anitoys.service;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.TopPicture;

/**
 * 商家顶部图片接口
 * @author 索罗游
 */
public interface TopPictureService extends Services<TopPicture> 
{
	/**
	 * business 前端商家的大首页图片
	 * @param topPicture
	 * @return
	 */
	public TopPicture selectFrontOne(TopPicture topPicture);

}
