package com.soloyogame.anitoys.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.db.commond.BusinessShipping;
import com.soloyogame.anitoys.db.commond.BusinessShippingDetail;
import com.soloyogame.anitoys.db.dao.BusinessShippingDao;
import com.soloyogame.anitoys.db.dao.BusinessShippingDetailDao;
import com.soloyogame.anitoys.service.OrderBusinessShippingService;

/**
 * 订单-商品寄送方式及运费
 * 
 * @author jfh
 * 
 */
@Service
public class OrderBusinessShippingServiceImpl implements
		OrderBusinessShippingService {

	@Resource(name = "businessShippingDetailDao")
	BusinessShippingDetailDao businessShippingDetailDao;

	@Resource(name = "businessShippingDao")
	BusinessShippingDao businessShippingDao;

	public void init(){
		queryOrderBusinessShipping("2");
	}
	
	@Override
	public List<BusinessShipping> queryOrderBusinessShipping(String businessId) {
		//组装快递方式查询条件
		BusinessShipping bsCond=new BusinessShipping();
		bsCond.setBusinessId(businessId);
		List bsfList= businessShippingDao.selectList(bsCond);
		List<BusinessShipping> bsList=new ArrayList<BusinessShipping> ();
		for(int i=0;i<bsfList.size();i++){
			BusinessShipping bs=(BusinessShipping)bsfList.get(i);
			//组装快递费用查询条件
			BusinessShippingDetail bsdCond=new BusinessShippingDetail();
			bsdCond.setBusinessShippingId(bs.getId());
			List bsdfList=businessShippingDetailDao.selectList(bsdCond);
			List<BusinessShippingDetail> bsdList=new ArrayList<BusinessShippingDetail> ();
			for(int j=0;j<bsdfList.size();j++){
				BusinessShippingDetail bsd=(BusinessShippingDetail)bsdfList.get(j);
				bsdList.add(bsd);
			}
			bs.setBsdList(bsdList);
			bsList.add(bs);
		}
		return bsList;
	}

}
