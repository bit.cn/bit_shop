package com.soloyogame.anitoys.service;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.DeductibleVoucherSn;

public interface DeductibleVoucherSnService extends
		Services<DeductibleVoucherSn> {

	/**
	 * 查找一条有效的平台优惠券
	 * @param couponSn
	 * @return
	 */
	public DeductibleVoucherSn findEffectiveDeductibleSnCode(DeductibleVoucherSn couponSn);


	/**
	 * 更新平台优惠券信息
	 * @param couponSn
	 * @return
	 */
	public  int updateDeductibleVoucherSnReceiveStatus(DeductibleVoucherSn couponSn);
}
