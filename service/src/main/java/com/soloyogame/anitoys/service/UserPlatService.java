package com.soloyogame.anitoys.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.db.BaseDao;
import com.soloyogame.anitoys.db.bean.User;
import com.soloyogame.anitoys.db.page.PagerModel;

import javax.annotation.Resource;


/**
 * 用户业务逻辑实现类
 * @author shaojian
 */
@Service
public class UserPlatService implements UserInteface 
{
    @Resource
	private BaseDao dao;

	public void setDao(BaseDao dao) 
	{
		this.dao = dao;
	}

	public User login(User user) 
	{
		return (User) dao.selectOne("userPlat.selectOne", user);
	}

	public List selectList(User user) 
	{
		if (user == null)
			return dao.selectList("userPlat.selectList");
		return dao.selectList("userPlat.selectList", user);
	}

	public int insert(User user) 
	{
		return dao.insert("userPlat.insert", user);
	}

	/**
	 * 批量删除用户
	 * 
	 * @param ids
	 */
	public int deletes(String[] ids) 
	{
		User user = new User();
		for (int i = 0; i < ids.length; i++) 
		{
			user.setId(ids[i]);
			delete(user);
		}
		return 0;
	}

	public int delete(User e) 
	{
		return dao.delete("userPlat.delete", e);
	}

	public int update(User e) 
	{
		return dao.update("userPlat.update", e);
	}

	public PagerModel selectPageList(User e) 
	{
		return dao.selectPageList("userPlat.selectPageList",
				"userPlat.selectPageCount", e);
	}

	@Override
	public User selectOne(User e) 
	{
		return (User) dao.selectOne("userPlat.selectOne", e);
	}

	@Override
	public User selectById(String id) 
	{
		User user = new User();
		user.setId(id);
		return selectOne(user);
	}
	
	/**
	 * 根据条件查询数量
	 * @param user
	 * @return
	 */
	public int selectCount(User user) 
	{
		if(user==null)
		{
			throw new NullPointerException();
		}
		
		return (Integer) dao.selectOne("userPlat.selectCount",user);
	}

	public User selectOneByCondition(User user) 
	{
		if(user==null)
		{
			throw new NullPointerException();
		}
		return (User) dao.selectOne("userPlat.selectOneByCondition", user);
	}

	@Override
	public int deleteById(int id) 
	{
		return 0;
	}

}
