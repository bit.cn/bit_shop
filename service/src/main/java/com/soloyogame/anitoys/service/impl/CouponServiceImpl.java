package com.soloyogame.anitoys.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.soloyogame.anitoys.db.commond.CouponSn;
import com.soloyogame.anitoys.db.commond.DeductibleVoucherSn;
import com.soloyogame.anitoys.db.dao.CouponSnDao;
import com.soloyogame.anitoys.db.dao.DeductibleVoucherSnDao;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.Coupon;
import com.soloyogame.anitoys.db.dao.CouponDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.CouponService;

@Service
public class CouponServiceImpl extends ServersManager<Coupon, CouponDao> implements CouponService 
{	
	
	private static final org.slf4j.Logger logger = LoggerFactory
			.getLogger(CouponServiceImpl.class);

	
	@Autowired
	private CouponDao couponDao;

	@Autowired
	private CouponSnDao couponSnDao;

	@Autowired
	private DeductibleVoucherSnDao deductibleVoucherSnDao;

	/*
	 *查询我的优惠券 
	 *@author kuangy 20151209
	 */
	@Override
	public PagerModel selectPageList(Coupon coupon) {
		return couponDao.selectPageList(coupon);	 
	}


	@Resource(name = "couponDaoManage")
    @Override
    public void setDao(CouponDao CouponDao) 
    {
        this.dao = CouponDao;
    }


	/*
	 * 查询可领取的优惠券
	 * @author kuangy 20151209
	 *
	 */
	@Override
	public PagerModel selectReceiveList(Coupon coupon) {
		return couponDao.couponReceiveList(coupon);
	}


	/*
	 * 领取优惠券
	 * @author kuangy 20151212
	 */
	@Override
	public int insertCoupon(Coupon coupon) {
		int insertNo = 0;

		try{
			//领取平台折扣券
			if(coupon.getCouponType()==3){
				DeductibleVoucherSn e = new DeductibleVoucherSn();
				e.setDeductibleSn(coupon.getCouponSn());
				// 查找一条可用的平台优惠券
				DeductibleVoucherSn deductibleVoucherSn =  deductibleVoucherSnDao.findEffectiveDeductibleSnCode(e);
				if(deductibleVoucherSn!=null) {
					// 设置为已领取
					deductibleVoucherSn.setReceiveStatus(1);
					deductibleVoucherSn.setIsOnline("1");
					deductibleVoucherSn.setReceiveTime(new Date());
					deductibleVoucherSnDao.update(deductibleVoucherSn);
					insertNo = this.couponDao.insertVoucher(coupon);
				}

			}else{//领取商家优惠券

				CouponSn e = new CouponSn();
				e.setCouponSn(coupon.getCouponSn());
				// 查找一条可用的平台优惠券
				CouponSn couponSn =  couponSnDao.findEffectiveCouponSnCode(e);
				if(couponSn!=null) {
					// 设置为已领取
					couponSn.setReceiveStatus(1);
					couponSn.setIsOnline("1");
					couponSn.setReceiveTime(new Date());
					couponSnDao.update(couponSn);
					insertNo = this.couponDao.insertCoupon(coupon);
				}

			}


		}catch (Exception e) {
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return insertNo;
	}


	@Override
	public PagerModel selectCouponByReceive(Coupon e) {
		return couponDao.selectCouponByReceive(e);
	}


	@Override
	public int updateStatus(Coupon e) {
		return couponDao.updateStatus(e);
	}

	/**
	 * 查询我的优惠券 
	 */
	public PagerModel selectFrontPageList(Coupon coupon) {
		return couponDao.selectFrontPageList(coupon);
	}
}
