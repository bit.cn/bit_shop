package com.soloyogame.anitoys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.bean.StrongAdvertising;
import com.soloyogame.anitoys.db.dao.StrongAdvertisingDao;
import com.soloyogame.anitoys.service.StrongAdvertisingService;

@Service("strongAdvertisingServiceManage")
public class StrongAdvertisingServiceImpl extends ServersManager<StrongAdvertising,StrongAdvertisingDao> implements StrongAdvertisingService 
{
	@Resource
	@Override
	public void setDao(StrongAdvertisingDao strongAdvertisingDao) 
	{
		this.dao=strongAdvertisingDao;
		
	}

	@Override
	public void deletess(String[] ids) {
		
		dao.deletess(ids);
	}

	@Override
	public boolean orderIsValid(int advertPosition) {
		StrongAdvertising e = new StrongAdvertising();
		e.setAdvertPosition(advertPosition);
		List<StrongAdvertising> list = dao.selectList(e);
		if(list != null && list.size() >0){
			return false;
		}
		e.clear();
		List<StrongAdvertising> allList = dao.selectList(e);
		if(allList == null){
			return true;
		}
		else if( allList.size() < 5 ){
			return true;
		}
		return false;
		
	}

	@Override
	public boolean updateOrderIsValid(StrongAdvertising e) {
		List<StrongAdvertising> list = dao.selectList(e);
		if(list != null && list.size() == 1){
			if(e.getId().equals(list.get(0).getId())){
				return true;
			}
			return false;
		}
		return true;
	}
}
