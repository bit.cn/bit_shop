package com.soloyogame.anitoys.service;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.PopularityAdvert;

public interface PopularityAdvertService extends Services<PopularityAdvert> 
{
    public void deletess(String[] ids);
    
    //front selectOne
    public PopularityAdvert frontselectOne(PopularityAdvert e) ;
}
