package com.soloyogame.anitoys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.News;
import com.soloyogame.anitoys.db.dao.NewsDao;
import com.soloyogame.anitoys.db.dao.PlatNewsDao;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.service.PlatNewsService;


/**
 * 平台文章
 * @author shaojian
 */
@Service
public class PlatNewsServiceImpl extends ServersManager<News, PlatNewsDao> implements PlatNewsService 
{
    @Autowired
    @Override
    public void setDao(PlatNewsDao newsDao) 
    {
        this.dao = newsDao;
    }
	/**
	 * @param e
	 */
	public List<News> selecIndexNews(News e) {
		return dao.selecIndexNews(e);
	}

	@Override
	public List<String> selectAllMd5() 
	{
		return dao.selectAllMd5();
	}

	@Override
	public void updateInBlackList(String[] ids) 
	{
		// TODO Auto-generated method stub
//		if(ids==null || ids.length==0){
//			return;
//		}
//		
//		for(int i=0;i<ids.length;i++){
//			String e = ids[i];
//			newsDao.updateInBlackList(e);
//		}
	}

	@Override
	public void sync(String[] ids, int status) 
	{
//		if(ids==null || ids.length==0){
//			return;
//		}
//		
//		for(int i=0;i<ids.length;i++){
//			News news = new News();
//			news.setId(ids[i]);
////			news.setStatus(status);
//			newsDao.sync(news);
//		}
	}

	@Override
	public List<News> selectNoticeList(News news) 
	{
		return dao.selectNoticeList(news);
	}
	
	/**
     * 加载平台的最新情报列表
     * @param news
     * @return
     */
	@Override
	public List<News> selectPlatNoticeList(News news) 
	{
		return dao.selectPlatNoticeList(news);
	}

	@Override
	public News selectSimpleOne(News news) {
		return dao.selectSimpleOne(news);
	}
	
	@Override
	public List<News> selectPlatList(News news) 
	{
		return dao.selectPlatList(news);
	}
	
	/**
	 * 查询平台文章
	 * @param news
	 * @return
	 */
	public News selectPlatNewsById(String id) 
	{
		return dao.selectPlatNewsById(id);
	}
	
	/**
	 * 平台的文章分页查询
	 * @param e
	 * @return
	 */
	public PagerModel selectPlatNewsPageList(News news) 
	{
		return dao.selectPlatNewsPageList(news);
	}
	
	@Override
	public void updateStatus(String[] ids, String status) {
		if(ids==null || ids.length==0){
			return;
		}
		
		for(int i=0;i<ids.length;i++){
			News news = new News();
			news.setId(ids[i]);
			news.setStatus(status);
			dao.sync(news);
		}
//		throw new NullPointerException();
	}

	@Override
	public void updateDownOrUp(News news) {
		dao.updateDownOrUp(news);
	}

	@Override
	public int selectCount(News news) {
		return dao.selectCount(news);
	}

}
