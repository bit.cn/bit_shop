package com.soloyogame.anitoys.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.PopularityAdvert;
import com.soloyogame.anitoys.db.dao.PopularityAdvertDao;
import com.soloyogame.anitoys.service.PopularityAdvertService;

@Service("PopularityAdvertServiceManage")
public class PopularityAdvertServiceImpl extends ServersManager<PopularityAdvert,PopularityAdvertDao> implements PopularityAdvertService 
{
	@Resource
	@Override
	public void setDao(PopularityAdvertDao PopularityAdvertDao) 
	{
		this.dao=PopularityAdvertDao;
		
	}

	@Override
	public void deletess(String[] ids) {
		
		dao.deletess(ids);
	}

	@Override
	public PopularityAdvert frontselectOne(PopularityAdvert e) {
		return dao.frontselectOne(e);
	}
	
	

}
