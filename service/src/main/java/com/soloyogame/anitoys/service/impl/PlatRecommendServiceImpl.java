package com.soloyogame.anitoys.service.impl;

import com.soloyogame.anitoys.core.ServersManager;
import com.soloyogame.anitoys.db.commond.PlatBusinessRecommend;
import com.soloyogame.anitoys.db.dao.PlatBusinessRecommendDao;
import com.soloyogame.anitoys.service.PlatRecommendService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service("platRecommendServiceManage")
public class PlatRecommendServiceImpl extends ServersManager<PlatBusinessRecommend, PlatBusinessRecommendDao> implements PlatRecommendService {
	@Resource
    @Override
    public void setDao(PlatBusinessRecommendDao platRecommenDao) {
        this.dao = platRecommenDao;
    }

}
