package com.soloyogame.anitoys.service;
import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.TopPicture;

/**
 * 大首页顶部图片接口
 * @author 索罗游
 */
public interface PlatTopPictureService extends Services<TopPicture> 
{
}
