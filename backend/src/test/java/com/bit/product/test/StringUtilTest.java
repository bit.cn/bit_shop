package com.bit.product.test;

import junit.framework.Assert;

import com.backend.mybatis.generator.util.StringUtils;


/**
 * @author jason
 */
public class StringUtilTest {

    public void testDbStringToCamelStyle() {
        String result = StringUtils.dbStringToCamelStyle("person_address");
        Assert.assertEquals("PersonAddress", result);
    }

    public void testDbStringToCamelStyle_case2() {
        String result = StringUtils.dbStringToCamelStyle("person_address_name");
        Assert.assertEquals("PersonAddressName", result);
    }

    public void testDbStringToCamelStyle_case3() {
        String result = StringUtils.dbStringToCamelStyle("person_DB_name");
        Assert.assertEquals("PersonDBName", result);
    }

    public void testDbStringToCamelStyle_case4() {
        String result = StringUtils.dbStringToCamelStyle("person_jobs_");
        Assert.assertEquals("PersonJobs", result);
    }

    public void testDbStringToCamelStyle_case5() {
        String result = StringUtils.dbStringToCamelStyle("a");
        Assert.assertEquals("A", result);
    }

}
