package com.bit.product.test;

import java.util.List;

import junit.framework.Assert;

import com.backend.mybatis.generator.model.DatabaseConfig;
import com.backend.mybatis.generator.model.GeneratorConfig;
import com.backend.mybatis.generator.util.ConfigHelper;

/**
 * @author jason
 *
 */
public class ConfigHelperTest {

    public void testFindConnectorLibPath_Oracle() {
        String path = ConfigHelper.findConnectorLibPath("Oracle");
        Assert.assertTrue(path.contains("ojdbc"));
    }

    public void testFindConnectorLibPath_Mysql() {
        String path = ConfigHelper.findConnectorLibPath("MySQL");
        Assert.assertTrue(path.contains("mysql-connector"));
    }

    public void testFindConnectorLibPath_PostgreSQL() {
        String path = ConfigHelper.findConnectorLibPath("PostgreSQL");
        Assert.assertTrue(path.contains("postgresql"));
    }
    
    public static void main(String[] args){
    	try {
			List<DatabaseConfig> databaseConfigList = ConfigHelper.loadDatabaseConfig();
			for (DatabaseConfig databaseConfig : databaseConfigList) {
				System.out.println(databaseConfig.getName());
			}
			
    		GeneratorConfig generatorConfig = ConfigHelper.loadGeneratorConfig("product");
    		System.out.println(generatorConfig.getMappingXMLPackage());
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
