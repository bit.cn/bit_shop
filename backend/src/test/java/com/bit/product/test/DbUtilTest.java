package com.bit.product.test;

import java.util.List;

import com.backend.mybatis.generator.model.DatabaseConfig;
import com.backend.mybatis.generator.util.ConfigHelper;
import com.backend.mybatis.generator.util.DbUtil;


/**
 * @author jason
 */
public class DbUtilTest {

    public static void main(String[] args){
    	 DatabaseConfig selectedConfig = new DatabaseConfig();
    	 selectedConfig.setDbType("MySQL");
    	 selectedConfig.setEncoding("utf8");
    	 selectedConfig.setHost("localhost");
    	 selectedConfig.setName("bit_product");
    	 selectedConfig.setUsername("root");
    	 selectedConfig.setPassword("root");
    	 selectedConfig.setPort("3306");
    	 selectedConfig.setSchema("bit_product");
    	 try {
			List<String> list = DbUtil.getTableNames(selectedConfig);
			for (String string : list) {
				//System.out.println(string);
			}
			
			List<String> lists = DbUtil.getColumnCommentByTableName(ConfigHelper.loadDatabaseConfigByName("localhost"), "product");
			for (String string : lists) {
				System.out.println(string);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

}
