package com.backend.mybatis.generator.util;

import org.springframework.stereotype.Component;

@Component
public class TemplateUtil implements ITemplateUtil {

	public String fill(TemplateContext context, String tmdir, String tmFile) {
		tmdir=this.getClass().getResource("/").getPath()+tmdir;
		return TemplateTool.fill(context, tmdir, tmFile);
	}

	public String fill(TemplateContext context, String tmdir, String tmFile,
			String inputEncode, String ouputEncode) {
		tmdir=this.getClass().getResource("/").getPath()+tmdir;
		return TemplateTool.fill(context, tmdir, tmFile, inputEncode, ouputEncode);
	}

	public String fill(TemplateContext context, String templateString) {
		return TemplateTool.fill(context, templateString);
	}

}
