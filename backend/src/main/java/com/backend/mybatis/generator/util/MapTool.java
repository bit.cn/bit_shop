package com.backend.mybatis.generator.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * map工具类
 * 
 * @author amo CreateDate:Feb 7, 2012
 */
public class MapTool {
	/**
	 * 获取map中的字符串值 null返回空字符串
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	public static String getStringFromMap(Map map, String key) {
		return map.get(key) == null ? "" : map.get(key).toString();
	}

	/**
	 * 获取map中的日期值 null返回null
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	public static Date getDateFromMap(Map map, String key) {
		return map.get(key) == null ? null : (Date) map.get(key);
	}

	/**
	 * 获取map中的Integer值
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	public static Integer getIntegerFromMap(Map map, String key) {
		return map.get(key) == null ? 0 : Integer.parseInt(map
				.get(key).toString());
	}

	/**
	 * 获取map中的Double值
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	public static Double getDoubleFromMap(Map map, String key) {
		return map.get(key) == null ? 0 : Double.parseDouble(map
				.get(key).toString());
	}
	/**
	 * 获取map中的Long值
	 * @param map
	 * @param key
	 * @return
	 */
	public static Long getLongFromMap(Map map, String key) {
		return map.get(key) == null ? 0 : Long.parseLong(map.get(key)
				.toString());
	}
	
	/**
	 * 获取map中的BigDecimal值
	 * @param map
	 * @param key
	 * @return
	 */
	public static BigDecimal getBigDecimal(Map map, String key) {
		return map.get(key) == null ? new BigDecimal(0) : new BigDecimal(map.get(key).toString());
	}
}
