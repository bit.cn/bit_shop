package com.backend.mybatis.generator.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.oxygen.util.StringTool;

/***************************************直接读取模版文件*************************		
try {
	Properties p = new Properties();
	//设置模板目录
	p.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_PATH,tmdir);
	VelocityEngine ve = new VelocityEngine();
	ve.init(p);
	Template t = ve.getTemplate(tmFile);
	StringWriter writer = new StringWriter();

	t.merge(context, writer);
	result = writer.toString();
	writer.close();
} catch (Exception e) {
	e.printStackTrace();
}
****************************************************************************************/
public class TemplateTool {
	
	
	public static Map TemplateMap=new HashMap();//缓存模版文件
	
	/**
	 * 填充模板文件
	 * @param context
	 * @param tmdir
	 * @param tmFile
	 * @return
	 */
	public static String fill(VelocityContext context,String tmdir, String tmFile) {
			String result="";
			Object templateString=TemplateMap.get(tmdir+"/"+tmFile);
			if(templateString!=null){
				result=TemplateTool.fill(context, templateString.toString());
				
			}
			else{
				String vmFilePath=tmdir+"/"+tmFile;
				String vmString=FileTool.readFile(vmFilePath);
				TemplateMap.put(tmdir+"/"+tmFile,vmString);
				result=TemplateTool.fill(context, vmString);
			}

		return result;
	}
	
	/**
	 * 填充模板文件,开发模式
	 * @param context
	 * @param tmdir
	 * @param tmFile
	 * @return
	 */
	public static String fillDev(VelocityContext context,String tmdir, String tmFile) {
		String result = "";
		String vmFilePath=tmdir+"/"+tmFile;
		String vmString=FileTool.readFile(vmFilePath);
		result=TemplateTool.fill(context, vmString);
		return result;
	}
	/**
	 * 填充模板文件
	 * @param context
	 * @param tmdir
	 * @param tmFile
	 * @return
	 */
	public static String fill(VelocityContext context,String tmdir, String tmFile,String inputEncode,String ouputEncode) {
		return TemplateTool.fill(context, tmdir, tmFile);
	}
	
	/**
	 * 填充字符串
	 * @param context
	 * @param templateString
	 * @return
	 */
	public static String fill(VelocityContext context,String templateString) {
		String result = "";
		StringWriter writer = null;
		StringReader reader=null;
		try {

			VelocityEngine ve = new VelocityEngine();
			String p=FileTool.getRealPath("/velocity.properties");
			ve.init(p);  
			writer = new StringWriter();
			 reader = new StringReader(templateString);
			context.put("StringTool", new StringTool());
            ve.evaluate(context, writer, "temp", reader);
			result = writer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(writer!=null){
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(reader!=null){
				reader.close();
			}
		}
		return result;
	}
	/**
	 * 执行模版中的脚本并将VelocityContext返回
	 * 将模版的执行结果放在VelocityContext.resultText中
	 * @param context
	 * @param templateString
	 * @return
	 */
	public static VelocityContext eval(VelocityContext context,String templateString) {
		String result = "";
		StringWriter writer = null;
		StringReader reader=null;
		try {

			VelocityEngine ve = new VelocityEngine();
			String p=FileTool.getRealPath("/velocity.properties");
			ve.init(p);
			writer = new StringWriter();
			reader = new StringReader(templateString);
			context.put("StringTool", new StringTool());
            ve.evaluate(context, writer, "temp", reader);
			result = writer.toString();
			context.put("resultText", result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(writer!=null){
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(reader!=null){
				reader.close();
			}
		}
		return context;
	}
	
	
	public static String test(int i){
		return "array:"+i;
	}
	
	
	public static void main(String[] args)

	throws Exception

	{

//		ArrayList list = new ArrayList();
//		Map map = new HashMap();
//		map.put("name", "Cow");
//		map.put("price", "100.00");
//		list.add(map);
//		map = new HashMap();
//		map.put("name", "Eagle");
//		map.put("price", "59.99");
//		list.add(map);
//		map = new HashMap();
//		map.put("name", "Shark");
//		map.put("price", "3.99");
//		list.add(map);
//		
//		SqlModel sqlModel = new SqlModel();
//		sqlModel.setSql("select * from mactest");
//
//		VelocityContext context = new VelocityContext();
//		context.put("TemplateTool", new TemplateTool());
//		context.put("petList", list);
//		context.put("num", 10);
//		context.put("test", "1");
//		context.put("sqlModel", sqlModel);
//		context.put("chineseTest", "汉字");
//	    String tmdir=TemplateTool.class.getResource("/template").getPath();
//		String result=TemplateTool.fill(context, tmdir, "demo.xml","UTF-8","UTF-8");
//		//System.out.println(result);
//		
//		String result2=TemplateTool.fill(context,"测试：${test}");
//		//System.out.println(result2);

	}
	

}
