package com.backend.mybatis.generator.util;

import org.apache.velocity.VelocityContext;

import com.oxygen.util.StringTool;

public class TemplateContext extends VelocityContext{
public TemplateContext(){
	this.put("StringTool",new  StringTool());
	this.put("TemplateArgs", new TemplateArgs());

}
}
