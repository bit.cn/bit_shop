package com.backend.mybatis.generator.util;

/**
 * 模版工具
 * @author jason
 */
public interface ITemplateUtil {
	
	/**填充模版**/
	public  String fill(TemplateContext context,String tmdir, String tmFile);
	public  String fill(TemplateContext context,String tmdir, String tmFile,String inputEncode,String ouputEncode);
	public  String fill(TemplateContext context,String templateString);

}
