package com.backend.mybatis.generator.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TemplateArgs {
	public List<Object> args=new ArrayList();
	public void add(Object arg){
		args.add(arg);
	}
	
	public void addLike(String arg){
		args.add("%"+arg+"%");
	}
	
//	public void addLeftLike(String arg){
//		args.add(arg+"%");
//	}
//	
//	public void addRightLike(String arg){
//		args.add("%"+arg);
//	}
	
	
	public void addDate(String dateString){
		Date d=null;
		try {
			d = DateTool.getDate(dateString);
		} catch (Exception e) {
			LogTool.error(DateTool.class, e);
		}
		args.add(d);
	}
	public void addDate(Date date){
		args.add(date);
	}
	public void addFromDate(String dateString){
		Date d=null;
		try {
			d = DateTool.getDateTime(dateString+" 00:00:00");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		args.add(d);
	}
	public void addFromDate(Date date){
		String dateString=DateTool.getFormatDate("yyyy-MM-dd", date);
		Date d=null;
		try {
			d = DateTool.getDateTime(dateString+" 00:00:00");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		args.add(d);
	}
	public void addToDate(String dateString){
		Date d=null;
		try {
			d = DateTool.getDateTime(dateString+" 23:59:59");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		args.add(d);
	}
	public void addToDate(Date date){
		String dateString=DateTool.getFormatDate("yyyy-MM-dd", date);
		Date d=null;
		try {
			d = DateTool.getDateTime(dateString+" 23:59:59");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		args.add(d);
	}
	public Object execute(String beanName,String method,Object ... objs){
		Object bean=SpringTool.getSpringBean(beanName.replaceFirst("Api", "Manager"));
		Class[] clazzs=new Class[objs.length];
		for(int i=0;i<clazzs.length;i++){
			if(objs[i]!=null){
			clazzs[i]=objs[i].getClass();
			}
		}
		Object result=ReflectTool.invoke(bean, method, clazzs, objs);
		return result;
	}
	
	

}
