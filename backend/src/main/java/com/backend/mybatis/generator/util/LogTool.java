package com.backend.mybatis.generator.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import org.apache.log4j.Logger;

public class LogTool {

	
	public static void debug(Class classname,String logStr) {
		Logger log;
		log= Logger.getLogger(classname.getName());
		log.debug(logStr);

	}
	public static void inf(Class classname,String logStr) {
		Logger log;
		log= Logger.getLogger(classname.getName());
		log.info(logStr);

	}
	public static void debug(Class classname,Exception e) {
		e.printStackTrace();
		LogTool.error(classname, e);

	}
	public static void error(Class classname,Exception e) {
		Logger log;
		log= Logger.getLogger(classname.getName());
		log.error("Exception message.", e);//打印异常堆栈
		//log.error(e);//打印最上层

	}
	public static void error(Class classname,Exception e,String msg) {
		Logger log;
		log= Logger.getLogger(classname.getName());
		log.error("Exception message."+msg, e);//打印异常堆栈

	}
	public static void warn(Class classname,String warn) {
		Logger log;
		log= Logger.getLogger(classname.getName());
		log.warn(warn);

	}
	public static String getExceptionStr(Exception e) {
	        StringWriter stringWriter= new StringWriter();
	        PrintWriter writer= new PrintWriter(stringWriter);
	        e.printStackTrace(writer);
	        StringBuffer buffer= stringWriter.getBuffer();
	        return buffer.toString();
	 }
	 

	 


}
