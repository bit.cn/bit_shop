package com.backend.mybatis.generator.model;

/**
 * 数据源配置信息
 * @author jason
 */
public class DatabaseConfig {

    private String dbType;      //数据库类型
  
    private String name;        //数据源配置名称

    private String host;        //数据源主机名或IP地址

    private String port;        //数据源端口
 
    private String schema;      //数据库名称

    private String username;    //数据源用户名

    private String password;    //密码

    private String encoding;    //数据源编码方式

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    @Override
    public String toString() {
        return name;
    }

}
