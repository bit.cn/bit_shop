package com.backend.plat.controller.servicemodule;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.backend.dto.ProductDto;
import com.backend.plat.sa.controller.BaseAction;
import com.google.gson.reflect.TypeToken;
import com.oxygen.constants.Constant;
import com.oxygen.dto.ApiFinalResponse;
import com.oxygen.enums.ApiMethodEnum;
import com.oxygen.util.JsonUtil;
import com.oxygen.web.WebHelper;

/**
 * 产品管理action
 * @author jason
 */
@Controller
@RequestMapping(value = "/product")
public class ProductAction extends BaseAction {

	/**
	 * 商品列表
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/list")
	public String login(HttpServletRequest req) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put(Constant.PAGE, req.getParameter(Constant.PAGE));
		appendMap.put(Constant.PAGE_SIZE, req.getParameter(Constant.PAGE_SIZE));
		String json = this.callApi(ApiMethodEnum.PRODUCT_GETPRODUCTPAGELIST, appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<List<ProductDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<ProductDto>>> retMap = JsonUtil.jsonToObject(json, type);

		if (retMap != null) {
			retMap.get(ApiMethodEnum.PRODUCT_GETPRODUCTPAGELIST.getCode()).getResults();
			req.setAttribute("apiRsp", retMap.get(ApiMethodEnum.PRODUCT_GETPRODUCTPAGELIST.getCode()));
		}
		req.setAttribute("page_size", appendMap.get(Constant.PAGE_SIZE));
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/product/list";
	}
	
	/**
	 * 新增商品
	 * @param req
	 * @param rsp
	 * @return
	 */
	@RequestMapping(value = "/add")
	public String add(HttpServletRequest req, HttpServletResponse rsp) {
		return "/product/add";
	}
}
