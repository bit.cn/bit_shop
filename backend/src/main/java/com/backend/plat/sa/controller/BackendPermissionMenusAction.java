package com.backend.plat.sa.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.backend.dto.PermissionFunctionDto;
import com.backend.dto.PermissionMenusDto;
import com.backend.dto.TreeDto;
import com.google.gson.reflect.TypeToken;
import com.oxygen.constants.Constant;
import com.oxygen.dto.ApiFinalResponse;
import com.oxygen.enums.BackendApiMethodEnum;
import com.oxygen.util.JsonUtil;
import com.oxygen.web.WebHelper;

/**
 * 后台权限菜单管理
 * 
 * @author jason
 */
@Controller
@RequestMapping(value = "/menu")
public class BackendPermissionMenusAction extends BaseAction {

	private static final String page_toList = "/menu/list";
	private static final String page_edit = "/menu/edit";

	@RequestMapping(value = "/list")
	public String list(HttpServletRequest req, HttpServletResponse rsp) {
		return page_toList;
	}

	/**
	 * ajax得到顶级菜单列表
	 */
	@ResponseBody
	@RequestMapping(value = "/getTopMenuList")
	public void getTopMenuList(HttpServletRequest req, HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		String json = this.callApi(BackendApiMethodEnum.USER_GETMENU,
				appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<List<PermissionMenusDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<PermissionMenusDto>>> retMap = JsonUtil
				.jsonToObject(json, type);
		List<TreeDto> treeDtoList = new ArrayList<TreeDto>();
		if (retMap != null) {
			List<PermissionMenusDto> permissionMenusList = retMap.get(
					BackendApiMethodEnum.USER_GETMENU.getCode()).getResults();

			for (PermissionMenusDto permissionMenusDto : permissionMenusList) {
				TreeDto treeDto = new TreeDto();
				treeDto.setId(permissionMenusDto.getId().toString());
				if (permissionMenusDto.getParentId() == 0) {
					treeDto.setParent("#");
				} else {
					treeDto.setParent(permissionMenusDto.getParentId()
							.toString());
				}
				treeDto.setText(permissionMenusDto.getName());
				treeDtoList.add(treeDto);
			}
		}
		String str = JSON.toJSONString(treeDtoList);
		WebHelper.outputJson(str, rsp);
	}

	/**
	 * 根据菜单ID得到菜单的详情
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadMenu")
	public String loadMenu(HttpServletRequest req, HttpServletResponse rsp) {
		// 得到菜单详情
		Map<String, Object> appendMap = new HashMap<String, Object>();
		String json = this.callApi(BackendApiMethodEnum.USER_GETMENUBYID,
				appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<PermissionMenusDto>>>() {
		}.getType();
		Map<String, ApiFinalResponse<PermissionMenusDto>> retMap = JsonUtil
				.jsonToObject(json, type);
		PermissionMenusDto permissionMenus = retMap.get(
				BackendApiMethodEnum.USER_GETMENUBYID.getCode()).getResults();
		// 得到功能列表
		Map<String, Object> appendMap1 = new HashMap<String, Object>();
		appendMap1.put(Constant.PAGE, req.getParameter(Constant.PAGE));
		appendMap1.put(Constant.PAGE_SIZE, 1000);
		String json1 = this.callApi(BackendApiMethodEnum.USER_GETFUNCTION,
				appendMap1, req);
		Type type1 = new TypeToken<Map<String, ApiFinalResponse<List<PermissionFunctionDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<PermissionFunctionDto>>> retMap1 = JsonUtil
				.jsonToObject(json1, type1);
		List<PermissionFunctionDto> permissionFunctionList = retMap1.get(
				BackendApiMethodEnum.USER_GETFUNCTION.getCode()).getResults();
		req.setAttribute("permissionFunctionList", permissionFunctionList);
		req.setAttribute("permissionMenus", permissionMenus);
		return page_edit;
	}

	/**
	 * 进入页面添加子菜单
	 * 
	 * @return
	 */
	@RequestMapping(value = "/addChildMenu")
	public String addChildMenu(HttpServletRequest req, HttpServletResponse rsp) {
		String parentId = req.getParameter("parentId");
		// 得到功能列表
		Map<String, Object> appendMap = new HashMap<String, Object>();
		String json = this.callApi(BackendApiMethodEnum.USER_GETFUNCTION,
				appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<List<PermissionFunctionDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<PermissionFunctionDto>>> retMap = JsonUtil
				.jsonToObject(json, type);
		List<PermissionFunctionDto> permissionFunctionList = retMap.get(
				BackendApiMethodEnum.USER_GETFUNCTION.getCode()).getResults();
		req.setAttribute("parentId", parentId);
		req.setAttribute("permissionFunctionList", permissionFunctionList);
		return page_edit;
	}

	/**
	 * 保存菜单
	 * 
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public void save(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			// 保存菜单基础信息
			Map<String, Object> appendMap = new HashMap<String, Object>();
			String funIds = StringUtils.join(req.getParameterValues("funIds"),
					",");
			appendMap.put("funIds", funIds);
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.USER_OPERATIONMENU, appendMap, req),
					rsp);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 更新菜单
	 * 
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public void update(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			// 修改菜单基础信息
			Map<String, Object> appendMap = new HashMap<String, Object>();
			String funIds = StringUtils.join(req.getParameterValues("funIds"),
					",");
			appendMap.put("funIds", funIds);
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.USER_UPDATEMENU, appendMap, req), rsp);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***
	 * 删除菜单
	 * 
	 * @return
	 */
	@RequestMapping(value = "/deleteMenu")
	public void delete(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			Map<String, Object> appendMap = new HashMap<String, Object>();
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.USER_DELETEMENU, appendMap, req), rsp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
