package com.backend.plat.sa.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.backend.dto.OrganizationalDto;
import com.backend.dto.TreeDto;
import com.google.gson.reflect.TypeToken;
import com.oxygen.constants.Constant;
import com.oxygen.dto.ApiFinalResponse;
import com.oxygen.enums.BackendApiMethodEnum;
import com.oxygen.util.JsonUtil;
import com.oxygen.web.WebHelper;

/**
 * 前台组织管理
 * 
 * @author jason
 */
@Controller
@RequestMapping(value = "/org")
public class BackendOrgAction extends BaseAction {

	private static final String page_toList = "/organizational/list";
	private static final String page_edit = "/organizational/edit";

	/**
	 * ajax得到顶级组织列表
	 */
	@ResponseBody
	@RequestMapping(value = "/getTopOrgList")
	public void getTopOrgList(HttpServletRequest req, HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put(Constant.PAGE, req.getParameter(Constant.PAGE));
		appendMap.put(Constant.PAGE_SIZE, req.getParameter(Constant.PAGE_SIZE));
		String json = this.callApi(BackendApiMethodEnum.USER_GETORGANIZATIONAL,
				appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<List<OrganizationalDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<OrganizationalDto>>> retMap = JsonUtil
				.jsonToObject(json, type);
		List<TreeDto> treeDtoList = new ArrayList<TreeDto>();
		if (retMap != null) {
			List<OrganizationalDto> orgList = retMap.get(
					BackendApiMethodEnum.USER_GETORGANIZATIONAL.getCode())
					.getResults();
			for (OrganizationalDto org : orgList) {
				TreeDto treeDto = new TreeDto();
				treeDto.setId(org.getId().toString());
				if (org.getParentId() == 0) {
					treeDto.setParent("#");
				} else {
					treeDto.setParent(org.getParentId().toString());
				}
				treeDto.setText(org.getName());
				treeDtoList.add(treeDto);
			}
		}
		String str = JSON.toJSONString(treeDtoList);
		WebHelper.outputJson(str, rsp);
	}

	/**
	 * 根据组织ID得到组织详情
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadOrg")
	public String loadOrg(HttpServletRequest req, HttpServletResponse rsp) {
		// 得到组织详情
		Map<String, Object> appendMap = new HashMap<String, Object>();
		String json = this.callApi(BackendApiMethodEnum.USER_GETORGBYID,
				appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<OrganizationalDto>>>() {
		}.getType();
		Map<String, ApiFinalResponse<OrganizationalDto>> retMap = JsonUtil
				.jsonToObject(json, type);
		OrganizationalDto org = retMap.get(
				BackendApiMethodEnum.USER_GETORGBYID.getCode()).getResults();
		req.setAttribute("org", org);
		return page_edit;
	}

	/**
	 * 进入页面添加子机构组织
	 * 
	 * @return
	 */
	@RequestMapping(value = "/addChildOrg")
	public String addChildOrg(HttpServletRequest req, HttpServletResponse rsp) {
		String parentId = req.getParameter("parentId");
		req.setAttribute("parentId", parentId);
		return page_edit;
	}

	@RequestMapping(value = "/search")
	public String search(HttpServletRequest req, HttpServletResponse rsp) {
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/organizational/search";
	}

	@RequestMapping(value = "/list")
	public String list(HttpServletRequest req, HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put(Constant.PAGE, req.getParameter(Constant.PAGE));
		appendMap.put(Constant.PAGE_SIZE, req.getParameter(Constant.PAGE_SIZE));
		String json = this.callApi(BackendApiMethodEnum.USER_GETORGANIZATIONAL,
				appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<List<OrganizationalDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<OrganizationalDto>>> retMap = JsonUtil
				.jsonToObject(json, type);

		if (retMap != null) {
			retMap.get(BackendApiMethodEnum.USER_GETORGANIZATIONAL.getCode())
					.getResults();
			req.setAttribute("apiRsp", retMap
					.get(BackendApiMethodEnum.USER_GETORGANIZATIONAL.getCode()));

		}
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/organizational/list";
	}

	@RequestMapping(value = "/edit/{id}")
	public String edit(@PathVariable String id, HttpServletRequest req,
			HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put("id", id);
		String json = this.callApi(BackendApiMethodEnum.USER_QUERYUSERBYID,
				appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<OrganizationalDto>>>() {
		}.getType();
		Map<String, ApiFinalResponse<OrganizationalDto>> retMap = JsonUtil
				.jsonToObject(json, type);

		if (retMap != null
				&& retMap
						.get(BackendApiMethodEnum.USER_QUERYUSERBYID.getCode()) != null) {
			req.setAttribute(
					"user",
					retMap.get(
							BackendApiMethodEnum.USER_QUERYUSERBYID.getCode())
							.getResults());
		}
		return "/organizational/edit";
	}

	@RequestMapping(value = "/saveOrg", method = RequestMethod.POST)
	public void save(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			Map<String, Object> appendMap = new HashMap<String, Object>();
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.USER_OPERATIONORGANIZATIONAL,
					appendMap, req), rsp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 更新组织
	 * 
	 * @return
	 */
	@RequestMapping(value = "/updateOrg", method = RequestMethod.POST)
	public void updateOrg(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			// 修改菜单基础信息
			Map<String, Object> appendMap = new HashMap<String, Object>();
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.USER_UPDATEORGANIZATIONAL, appendMap,
					req), rsp);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***
	 * 删除组织
	 * 
	 * @return
	 */
	@RequestMapping(value = "/deleteOrg")
	public void deleteOrg(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			Map<String, Object> appendMap = new HashMap<String, Object>();
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.USER_DELETEORGANIZATIONAL, appendMap,
					req), rsp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
