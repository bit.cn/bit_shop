package com.backend.plat.sa.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.backend.mybatis.generator.model.GeneratorConfig;
import com.backend.mybatis.generator.util.ConfigHelper;
import com.backend.mybatis.generator.util.DbUtil;
import com.backend.mybatis.generator.util.FileTool;
import com.backend.mybatis.generator.util.ITemplateUtil;
import com.backend.mybatis.generator.util.TemplateContext;
import com.backend.mybatis.generator.util.XmlTool;
import com.backend.plat.sa.api.CodeMakeApi;
import com.backend.plat.sa.model.CodeMaker;
import com.oxygen.util.StringTool;

@Component
public class CodeMakeManager implements CodeMakeApi {
	
	@Autowired
	ITemplateUtil templateUtil;

	/**
	 * 利用mybatis generator 生成代码Model,dao,mapper
	 * @param codeMaker
	 * @return
	 * @throws Exception 
	 */
	public String makeMybatisGeneratorCode(CodeMaker codeMaker) throws Exception {
		TemplateContext context=new TemplateContext();
		codeMaker.setAuthor("jason");
		context.put("codeMaker", codeMaker);
		context.put("CodeMakeApi", 	this);
		String code=templateUtil.fill(context, "/template/codeMaker/","generatorConfig-template.vm");
		String workSpaceDir="D:/myBatisGenerator/";
		String folder=workSpaceDir.replaceAll("\\.", "/");
		FileTool.createFolder(folder);
		XmlTool.writeXmlToFlie(XmlTool.bulidXmlDoc(code), workSpaceDir+"generatorConfig.xml", "UTF-8");
		File configFile = new File(workSpaceDir+"generatorConfig.xml");
        List<String> warnings = new ArrayList<String>();
        boolean overwrite = true;
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(configFile);
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);
		return code;
	}

	/**
	 * 生成Controller代码
	 * @param codeMaker
	 * @return
	 */
	public String makeControllerCode(CodeMaker codeMaker) {
		TemplateContext context=new TemplateContext();
		context.put("codeMaker", codeMaker);
		context.put("CodeMakeApi", 	this);
		String code=templateUtil.fill(context, "/template/codeMaker/","action-template.vm");
		String workSpaceDir="D:/myBatisGenerator/";
		String folder=workSpaceDir+"/"+codeMaker.getSrcName()+"/"+codeMaker.getPackageName().replaceAll("\\.", "/")+"/action";
		FileTool.createFolder(folder);
		String filepath=folder+"/"+codeMaker.getModelCode()+"Action.java";
		FileTool.saveStringToFile(code, filepath, "UTF-8");
		return code;
	}


	public String makeApiCode(CodeMaker codeMaker) {
		TemplateContext context=new TemplateContext();
		context.put("CodeMaker", codeMaker);
		context.put("CodeMakeApi", 	this);
		String code=templateUtil.fill(context, "/template/codeMaker/","api-template.vm");
		String workSpaceDir="D:/myBatisGenerator/";
		String folder=workSpaceDir+"/"+codeMaker.getSrcName()+"/"+codeMaker.getPackageName().replaceAll("\\.", "/")+"/api";
		FileTool.createFolder(folder);
		String filepath=folder+"/"+codeMaker.getModelCode()+"Api.java";
		FileTool.saveStringToFile(code, filepath, "UTF-8");
		return code;
	}


	public String makeConfigCode(CodeMaker codeMaker) {
		TemplateContext context=new TemplateContext();
		context.put("codeMaker", codeMaker);
		context.put("CodeMakeApi", 	this);
		String code=templateUtil.fill(context, "/template/codeMaker/","action-config-template.vm");
		String workSpaceDir="D:/myBatisGenerator/";
		String folder=workSpaceDir+"/config/action/";
		FileTool.createFolder(folder);
		String filepath=folder+"/"+"action-config-"+codeMaker.getModelCode()+".xml";
		FileTool.saveStringToFile(code, filepath, "UTF-8");
		return code;
	}

	public String makeManagerCode(CodeMaker codeMaker) {
		TemplateContext context=new TemplateContext();
		context.put("codeMaker", codeMaker);
		context.put("CodeMakeApi", 	this);
		String code=templateUtil.fill(context, "/template/codeMaker/","manager-template.vm");
		String workSpaceDir="D:/myBatisGenerator/";
		String folder=workSpaceDir+"/"+codeMaker.getSrcName()+"/"+codeMaker.getPackageName().replaceAll("\\.", "/")+"/manager";
		FileTool.createFolder(folder);
		String filepath=folder+"/"+codeMaker.getModelCode()+"Manager.java";
		FileTool.saveStringToFile(code, filepath, "UTF-8");
		return code;
	}
	
	public String up1(String word){
		return StringTool.up1stLetter(word);
	}
	
	public String lower1(String word){
		return StringTool.lower1stLetter(word);
	}



	public String makeListViewCode(CodeMaker codeMaker) {
		TemplateContext context=new TemplateContext();
		context.put("CodeMaker", codeMaker);
		context.put("CodeMakeApi", 	this);
		String code=templateUtil.fill(context, "/template/codeMaker/","listView-template.vm");
		String workSpaceDir="D:/myBatisGenerator/";
		String folder=workSpaceDir+"/WebRoot/view/"+codeMaker.getAppCode()+"/jsp/";
		FileTool.createFolder(folder);
		String filepath=folder+"/"+codeMaker.getLowerModelCode()+"ListView.jsp";
		FileTool.saveStringToFile(code, filepath, "UTF-8");
		return code;
	}


	public String makeEditViewCode(CodeMaker codeMaker) {
//		TemplateContext context=new TemplateContext();
//		DBTable dBTable=codeMakeDao.getDBTable(codeMaker);
//		List<DBTableField> fieldList=dBTable.getFieldList();
//		int fieldNum=fieldList.size();
//		fieldList=this.formatFileds(fieldList);//格式化为驼峰式
//		context.put("fields", 	fieldList);
//		context.put("fieldNum", fieldNum);
//		context.put("CodeMaker", codeMaker);
//		context.put("CodeMakeApi", 	this);
//		String code=templateUtil.fill(context, "/template/codeMaker/","editView-template.vm");
//		String workSpaceDir="D:/myBatisGenerator/";
//		String folder=workSpaceDir+"/WebRoot/view/"+codeMaker.getAppCode()+"/jsp/";
//		FileTool.createFolder(folder);
//		String filepath=folder+"/"+codeMaker.getLowerModelCode()+"EditView.jsp";
//		FileTool.saveStringToFile(code, filepath, "UTF-8");
		return null;
	}

	@Override
	public String makeServiceCode(CodeMaker codeMaker) {
		TemplateContext context=new TemplateContext();
		context.put("codeMaker", codeMaker);
		context.put("CodeMakeApi", 	this);
		String code=templateUtil.fill(context, "/template/codeMaker/","service-template.vm");
		String workSpaceDir="D:/myBatisGenerator/";
		String folder=workSpaceDir+"/"+codeMaker.getGeneratorConfig().getModelPackageTargetFolder()+"/"+"com.bit.product.service".replaceAll("\\.", "/");
		FileTool.createFolder(folder);
		String filepath=folder+"/"+codeMaker.getGeneratorConfig().getDomainObjectName()+"Service.java";
		FileTool.saveStringToFile(code, filepath, "UTF-8");
		return code;
	}

	@Override
	public String makeServiceImplCode(CodeMaker codeMaker) {
		try {
			TemplateContext context=new TemplateContext();
			context.put("codeMaker", codeMaker);
			context.put("CodeMakeApi", 	this);
			String code=templateUtil.fill(context, "/template/codeMaker/","serviceimpl-template.vm");
			String workSpaceDir="D:/myBatisGenerator/";
			String folder=workSpaceDir+"/"+codeMaker.getGeneratorConfig().getModelPackageTargetFolder()+"/"+"com.bit.product.service.impl".replaceAll("\\.", "/");
			FileTool.createFolder(folder);
			String filepath=folder+"/"+codeMaker.getGeneratorConfig().getDomainObjectName()+"ServiceImpl.java";
			FileTool.saveStringToFile(code, filepath, "UTF-8");
			return code;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
