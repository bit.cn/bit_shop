package com.backend.plat.sa.controller;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.backend.dto.PermissionFunctionDto;
import com.google.gson.reflect.TypeToken;
import com.oxygen.constants.Constant;
import com.oxygen.dto.ApiFinalResponse;
import com.oxygen.enums.BackendApiMethodEnum;
import com.oxygen.util.JsonUtil;
import com.oxygen.web.WebHelper;

/**
 * 前台用户管理
 * 
 * @author jason
 */
@Controller
@RequestMapping(value = "/function")
public class BackendFunctionAction extends BaseAction {

	private static final String page_edit = "/function/edit";

	/**
	 * 进入新增页面
	 * 
	 * @param req
	 * @param rsp
	 */
	@RequestMapping(value = "/add")
	public String add(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			// 得到功能列表
			Map<String, Object> appendMap1 = new HashMap<String, Object>();
			String json1 = this.callApi(BackendApiMethodEnum.USER_GETFUNCTION,
					appendMap1, req);
			Type type1 = new TypeToken<Map<String, ApiFinalResponse<List<PermissionFunctionDto>>>>() {
			}.getType();
			Map<String, ApiFinalResponse<List<PermissionFunctionDto>>> retMap1 = JsonUtil
					.jsonToObject(json1, type1);
			req.setAttribute("apiRsp", retMap1
					.get(BackendApiMethodEnum.USER_GETFUNCTION.getCode()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return page_edit;
	}

	@RequestMapping(value = "/search")
	public String search(HttpServletRequest req, HttpServletResponse rsp) {
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/function/search";
	}

	@RequestMapping(value = "/list")
	public String list(HttpServletRequest req, HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put(Constant.PAGE, req.getParameter(Constant.PAGE));
		appendMap.put(Constant.PAGE_SIZE, req.getParameter(Constant.PAGE_SIZE));
		String json = this.callApi(BackendApiMethodEnum.USER_GETFUNCTION,
				appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<List<PermissionFunctionDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<PermissionFunctionDto>>> retMap = JsonUtil
				.jsonToObject(json, type);

		if (retMap != null) {
			retMap.get(BackendApiMethodEnum.USER_GETFUNCTION.getCode())
					.getResults();
			req.setAttribute("apiRsp",
					retMap.get(BackendApiMethodEnum.USER_GETFUNCTION.getCode()));

		}
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/function/list";
	}

	@RequestMapping(value = "/edit/{id}")
	public String edit(@PathVariable String id, HttpServletRequest req,
			HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put("id", id);
		String json = this.callApi(BackendApiMethodEnum.USER_QUERYFUNCTIONBYID,
				appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<PermissionFunctionDto>>>() {
		}.getType();
		Map<String, ApiFinalResponse<PermissionFunctionDto>> retMap = JsonUtil
				.jsonToObject(json, type);

		if (retMap != null
				&& retMap.get(BackendApiMethodEnum.USER_QUERYFUNCTIONBYID
						.getCode()) != null) {
			req.setAttribute(
					"function",
					retMap.get(
							BackendApiMethodEnum.USER_QUERYFUNCTIONBYID
									.getCode()).getResults());
		}
		return "/function/edit";
	}

	/**
	 * 保存权限功能
	 * @param req
	 * @param rsp
	 */
	@RequestMapping(value = "/save")
	public void save(HttpServletRequest req, HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		WebHelper.outputJson(this.callApi(
				BackendApiMethodEnum.USER_OPERATIONPERMISSIONFUNCTION,
				appendMap, req), rsp);
	}

	/**
	 * 更新权限功能
	 * @param req
	 * @param rsp
	 */
	@RequestMapping(value = "/update")
	public void update(HttpServletRequest req, HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		WebHelper.outputJson(this.callApi(
				BackendApiMethodEnum.USER_UPDATEPERMISSIONFUNCTION, appendMap,
				req), rsp);
	}
	
	/***
	 * 删除权限功能
	 * 
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void delete(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			Map<String, Object> appendMap = new HashMap<String, Object>();
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.USER_DELETEPERMISSIONFUNCTION, appendMap, req), rsp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
