package com.backend.plat.sa.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.mybatis.generator.model.DatabaseConfig;
import com.backend.mybatis.generator.model.DbType;
import com.backend.mybatis.generator.model.GeneratorConfig;
import com.backend.mybatis.generator.util.ConfigHelper;
import com.backend.mybatis.generator.util.DbUtil;
import com.backend.mybatis.generator.util.TemplateContext;
import com.backend.plat.sa.api.CodeMakeApi;
import com.backend.plat.sa.model.CodeMaker;
import com.backend.plat.sa.service.CodeMakeService;
import com.oxygen.annotations.ApiService;

@Service
@ApiService(descript = "用户账号")
public class CodeMakeServiceImpl extends BaseServiceImpl implements CodeMakeService {
	
	private static final Logger logger = Logger.getLogger(CodeMakeServiceImpl.class);

	@Autowired
	private CodeMakeApi codeMakeApi;
	
	@Override
	public String makeMybatisGeneratorCode(CodeMaker codeMaker) {
		try {
			return codeMakeApi.makeMybatisGeneratorCode(codeMaker);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String makeControllerCode(CodeMaker codeMaker) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String makeApiCode(CodeMaker codeMaker) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String makeManagerCode(CodeMaker codeMaker) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String makeConfigCode(CodeMaker codeMaker) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String makeListViewCode(CodeMaker codeMaker) {
		try {
			return codeMakeApi.makeListViewCode(codeMaker);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String makeEditViewCode(CodeMaker codeMaker) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String makeServiceCode(CodeMaker codeMaker) {
		try {
			return codeMakeApi.makeServiceCode(codeMaker);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public String makeServiceImplCode(CodeMaker codeMaker) {
		try {
			return codeMakeApi.makeServiceImplCode(codeMaker);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}