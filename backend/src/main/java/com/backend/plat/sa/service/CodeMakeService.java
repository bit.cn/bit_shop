package com.backend.plat.sa.service;

import com.backend.mybatis.generator.model.DatabaseConfig;
import com.backend.mybatis.generator.model.GeneratorConfig;
import com.backend.plat.sa.model.CodeMaker;

/**
 * 代码生成Service
 * @author jason
 *
 */
public interface CodeMakeService {
	
	/**
	 * 利用mybatis generator 生成代码Model,dao,mapper
	 * @param generatorConfig
	 * @param databaseConfig
	 * @return
	 */
	public String makeMybatisGeneratorCode(CodeMaker codeMaker);
	
	/**
	 * 生成Controller代码
	 * @param codeMaker
	 * @return
	 */
	public String makeControllerCode(CodeMaker codeMaker);
	
	/**
	 * 生成api代码
	 * @param codeMaker
	 * @return
	 */
	public String makeApiCode(CodeMaker codeMaker);
	
	/**
	 * 生成service服务接口代码
	 * @param codeMaker
	 * @return
	 */
	public String makeServiceCode(CodeMaker codeMaker);
	
	/**
	 * 生成服务接口实现代码
	 * @param codeMaker
	 * @return
	 */
	public String makeServiceImplCode(CodeMaker codeMaker);
	
	/**
	 * 生成Manager代码
	 * @param codeMaker
	 * @return
	 */
	public String makeManagerCode(CodeMaker codeMaker);
	
	/**
	 * 生成Action配置文件代码
	 * @param codeMaker
	 * @return
	 */
	public String makeConfigCode(CodeMaker codeMaker);
	
	/**
	 * 生成列表页面代码
	 * @param codeMaker
	 * @return
	 */
	public String makeListViewCode(CodeMaker codeMaker);
	
	/**
	 * 生成编辑页面代码
	 * @param codeMaker
	 * @return
	 */
	public String makeEditViewCode(CodeMaker codeMaker);

}
