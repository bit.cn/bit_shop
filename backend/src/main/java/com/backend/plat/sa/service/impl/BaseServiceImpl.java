package com.backend.plat.sa.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;

import com.oxygen.service.impl.AbstractSolrService;
import com.oxygen.web.SuperDispatcherServlet;

public abstract class BaseServiceImpl extends AbstractSolrService {

	@Value("${weixin.accessToken}")
	protected String weixinAccessToken;
	@Value("${weixin.getUserInfo}")
	protected String weixinGetUserInfo;

	@Value("${dounaba.user.api.server}")
	protected String dounabaUserApiServer;

	@Value("${dounaba.commonservice.api.server}")
	protected String dounabaCommonserviceApiServer;

	@Value("${dounaba.activity.api.server}")
	protected String dounabaActivityApiServer;

	@Value("${dounaba.report.api.server}")
	protected String dounabaReportApiServer;

	@Value("${dounaba.content.api.server}")
	protected String dounabaContentApiServer;

	@Value("${dounaba.payment.api.server}")
	protected String dounabaPaymentApiServer;

	@Value("${dounaba.log.api.server}")
	protected String dounabaLogApiServer;

	@Value("${dounaba.commondata.api.server}")
	protected String dounabaCommonDataApiServer;

	@Value("${dounaba.api.key}")
	protected String apiKey;

	@Value("${qqzone.open.appId}")
	protected String qqzoneOpenAppId;
	@Value("${qqzone.open.appKey}")
	protected String qqzoneOpenAppKey;
	@Value("${qqzone.open.host}")
	protected String qqzoneOpenHost;

	protected static final Map<String, String> head = new HashMap<String, String>();

	public BaseServiceImpl() {
		int port = SuperDispatcherServlet.getlocalPort();
		head.put("referer", "dounaba_user/" + port);
	}

	/**
	 * 调用公用API
	 * 
	 * @param apiKey
	 * @param functionCode
	 * @param inMap
	 * @return
	 * @author Daniel
	 */
	protected String callCommonserviceApi(String functionCode, Map<String, Object> inMap) {
		return this.callApi(dounabaCommonserviceApiServer, apiKey, functionCode, inMap, head);
	}

	/**
	 * 调用活动API
	 * 
	 * @param apiKey
	 * @param functionCode
	 * @param inMap
	 * @return
	 * @author Daniel
	 */
	protected String callActivityApi(String functionCode, Map<String, Object> inMap) {
		return this.callApi(dounabaActivityApiServer, apiKey, functionCode, inMap, head);
	}

	/**
	 * 调用报表API
	 * 
	 * @param apiKey
	 * @param functionCode
	 * @param inMap
	 * @return
	 * @author Daniel
	 */
	protected String callReportApi(String functionCode, Map<String, Object> inMap) {
		return this.callApi(dounabaReportApiServer, apiKey, functionCode, inMap, head);
	}

	/**
	 * 调用内容API
	 * 
	 * @param apiKey
	 * @param functionCode
	 * @param inMap
	 * 
	 * @return
	 * @author Daniel
	 */
	protected String callContentApi(String functionCode, Map<String, Object> inMap) {
		return this.callApi(dounabaContentApiServer, apiKey, functionCode, inMap, head);
	}

	protected String callPaymentApi(String functionCode, Map<String, Object> inMap) {
		return this.callApi(dounabaPaymentApiServer, apiKey, functionCode, inMap, head);
	}

	protected String callLogApi(String functionCode, Map<String, Object> inMap) {
		return this.callApi(dounabaLogApiServer, apiKey, functionCode, inMap, head);
	}

	protected String callCommonDataApi(String functionCode, Map<String, Object> inMap) {
		return this.callApi(dounabaCommonDataApiServer, apiKey, functionCode, inMap, head);
	}
}
