package com.backend.plat.sa.controller;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.backend.dto.InterfaceDto;
import com.google.gson.reflect.TypeToken;
import com.oxygen.constants.Constant;
import com.oxygen.dto.ApiFinalResponse;
import com.oxygen.enums.ApiMethodEnum;
import com.oxygen.enums.BackendApiMethodEnum;
import com.oxygen.util.JsonUtil;
import com.oxygen.web.WebHelper;

/**
 * 前台用户管理
 * @author jason
 */
@Controller
@RequestMapping(value = "/interfaces")
public class BackendInterfacesAction extends BaseAction {
	
	/**
	 * 初始化接口
	 * @return
	 */
	@RequestMapping(value = "/initInteface")
	public String initInteface(HttpServletRequest req, HttpServletResponse rsp){
		Map<String, ApiMethodEnum> retMap = ApiMethodEnum.getApiMethodEnumMap();
		for (String key : retMap.keySet()) { 
			ApiMethodEnum value = retMap.get(key);  
			Map<String, Object> appendMap = new HashMap<String, Object>();
			appendMap.put("name", value.getMsg());
			appendMap.put("code", value.getCode());
			appendMap.put("summary", value.getMsg());	
			this.callApi(
					BackendApiMethodEnum.BACKEND_COMMON_ADDINTERFACE, appendMap, req);
		}  
		Map<String, BackendApiMethodEnum> backRetMap = BackendApiMethodEnum.getApiMethodEnumMap();
		for (String key : backRetMap.keySet()) { 
			BackendApiMethodEnum value = backRetMap.get(key);  
			Map<String, Object> appendMap = new HashMap<String, Object>();
			appendMap.put("name", value.getMsg());
			appendMap.put("code", value.getCode());
			appendMap.put("summary", value.getMsg());	
			this.callApi(
					BackendApiMethodEnum.BACKEND_COMMON_ADDINTERFACE, appendMap, req);
		}  
		return "redirect:list";
	}
	
	@RequestMapping(value = "/search")
	public String search(HttpServletRequest req, HttpServletResponse rsp) {
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/interfaces/search";
	}
	@RequestMapping(value = "/list")
	public String list(HttpServletRequest req, HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put(Constant.PAGE, req.getParameter(Constant.PAGE));
		appendMap.put(Constant.PAGE_SIZE, req.getParameter(Constant.PAGE_SIZE));
		String json = this.callApi(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACELIST, appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<List<InterfaceDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<InterfaceDto>>> retMap = JsonUtil.jsonToObject(json, type);
		
		if (retMap != null) {
			retMap.get(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACELIST.getCode()).getResults();
			req.setAttribute("apiRsp", retMap.get(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACELIST.getCode()));
		}
		req.setAttribute("page_size", appendMap.get(Constant.PAGE_SIZE));
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/interfaces/list";
	}

	@RequestMapping(value = "/edit/{id}")
	public String edit(@PathVariable String id, HttpServletRequest req, HttpServletResponse rsp) {
		//得到客户端信息
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put("id", id);
		String json = this.callApi(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACEINFO, appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<InterfaceDto>>>() {
		}.getType();
		Map<String, ApiFinalResponse<InterfaceDto>> retMap = JsonUtil.jsonToObject(json, type);

		if (retMap != null && retMap.get(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACEINFO.getCode()) != null) {
			req.setAttribute("interfaces", retMap.get(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACEINFO.getCode()).getResults());
		}
		
	
		return "/interfaces/edit";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public void save(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			Map<String, Object> appendMap = new HashMap<String, Object>();
			appendMap.put("id", req.getParameter("id"));
			appendMap.put("name", req.getParameter("name"));
			appendMap.put("code", req.getParameter("code"));
			appendMap.put("summary", req.getParameter("summary"));		
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.BACKEND_COMMON_UPDATEINTERFACE, appendMap, req),
					rsp);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/add")
	public String add(HttpServletRequest req, HttpServletResponse rsp) {
		//新增页面
		return "/interfaces/add";
	}
	
	@RequestMapping(value = "/addSave", method = RequestMethod.POST)
	@ResponseBody
	public void addSave(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			Map<String, Object> appendMap = new HashMap<String, Object>();
			appendMap.put("name", req.getParameter("name"));
			appendMap.put("code", req.getParameter("code"));
			appendMap.put("summary", req.getParameter("summary"));		
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.BACKEND_COMMON_ADDINTERFACE, appendMap, req),
					rsp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
