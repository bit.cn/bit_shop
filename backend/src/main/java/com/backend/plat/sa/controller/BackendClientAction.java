package com.backend.plat.sa.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.backend.dto.ClientsRelateInterfaceDto;
import com.backend.dto.InterfaceClientsDto;
import com.backend.dto.InterfaceClientsInfoDto;
import com.backend.dto.InterfaceDto;
import com.backend.dto.UserDto;
import com.backend.dto.UserRelateOrgDto;
import com.google.gson.reflect.TypeToken;
import com.oxygen.constants.Constant;
import com.oxygen.dto.ApiFinalResponse;
import com.oxygen.enums.BackendApiMethodEnum;
import com.oxygen.util.JsonUtil;
import com.oxygen.web.WebHelper;

/**
 * 前台用户管理
 * @author jason
 */
@Controller
@RequestMapping(value = "/client")
public class BackendClientAction extends BaseAction {
	
	@RequestMapping(value = "/search")
	public String search(HttpServletRequest req, HttpServletResponse rsp) {
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/client/search";
	}
	@RequestMapping(value = "/list")
	public String list(HttpServletRequest req, HttpServletResponse rsp) {
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put(Constant.PAGE, req.getParameter(Constant.PAGE));
		appendMap.put(Constant.PAGE_SIZE, req.getParameter(Constant.PAGE_SIZE));
		String json = this.callApi(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACECLIENTSLIST, appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<List<InterfaceClientsDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<InterfaceClientsDto>>> retMap = JsonUtil.jsonToObject(json, type);
		
		if (retMap != null) {
			retMap.get(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACECLIENTSLIST.getCode()).getResults();
			req.setAttribute("apiRsp", retMap.get(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACECLIENTSLIST.getCode()));
		}
		req.setAttribute("page_size", appendMap.get(Constant.PAGE_SIZE));
		WebHelper.setRequestAttributesFromRequestParam(req);
		return "/client/list";
	}

	@RequestMapping(value = "/edit/{id}")
	public String edit(@PathVariable String id, HttpServletRequest req, HttpServletResponse rsp) {
		//得到客户端信息
		Map<String, Object> appendMap = new HashMap<String, Object>();
		appendMap.put("id", id);
		String json = this.callApi(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACECLIENTSINFO, appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<InterfaceClientsInfoDto>>>() {
		}.getType();
		Map<String, ApiFinalResponse<InterfaceClientsInfoDto>> retMap = JsonUtil.jsonToObject(json, type);
		InterfaceClientsInfoDto interfaceClientsInfoDto = retMap.get(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACECLIENTSINFO.getCode()).getResults();
		
		InterfaceClientsDto interfaceClientsDto=interfaceClientsInfoDto.getInterfaceClientsDto();
		
		List<ClientsRelateInterfaceDto> clientsRelateInterfaceDtoList = interfaceClientsInfoDto.getClientsRelateInterfaceDto();
		req.setAttribute("client", interfaceClientsDto);
		req.setAttribute("clientIntefaceList", clientsRelateInterfaceDtoList);
		
		//得到接口列表
		Map<String, Object> appendMap1 = new HashMap<String, Object>();
		appendMap1.put(Constant.PAGE, req.getParameter(Constant.PAGE));
		appendMap1.put(Constant.PAGE_SIZE, req.getParameter(Constant.PAGE_SIZE));
		String json1 = this.callApi(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACELIST, appendMap1, req);
		Type type1 = new TypeToken<Map<String, ApiFinalResponse<List<InterfaceDto>>>>() {
		}.getType();
		Map<String, ApiFinalResponse<List<InterfaceDto>>> retMap1 = JsonUtil.jsonToObject(json1, type1);
		if (retMap1 != null) {
			List<InterfaceDto> interfaceList = retMap1.get(BackendApiMethodEnum.BACKEND_COMMON_GETINTERFACELIST.getCode()).getResults();
			req.setAttribute("interfaceList", interfaceList);
		}
		return "/client/edit";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public void save(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			Map<String, Object> appendMap = new HashMap<String, Object>();
			String interfaces = StringUtils.join(req.getParameterValues("list-checkbox"), ",");
			appendMap.put("interfaces", interfaces);
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.BACKEND_COMMON_UPDATECLIENTRELATEINTERFACE, appendMap, req),
					rsp);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/add")
	public String add(HttpServletRequest req, HttpServletResponse rsp) {
		//新增页面
		return "/client/add";
	}
	
	@RequestMapping(value = "/addSave", method = RequestMethod.POST)
	@ResponseBody
	public void addSave(HttpServletRequest req, HttpServletResponse rsp) {
		try {
			Map<String, Object> appendMap = new HashMap<String, Object>();
			appendMap.put("clientName", req.getParameter("clientName"));
			appendMap.put("clientKey", req.getParameter("clientKey"));
			appendMap.put("description", req.getParameter("description"));	
			appendMap.put("effectiveTime", req.getParameter("effectiveTime"));	
			appendMap.put("expirationTime", req.getParameter("expirationTime"));
			WebHelper.outputJson(this.callApi(
					BackendApiMethodEnum.BACKEND_COMMON_ADDINTERFACECLIENTS, appendMap, req),
					rsp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
