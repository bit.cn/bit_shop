package com.backend.plat.sa.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSON;
import com.backend.dto.TreeDto;
import com.backend.mybatis.generator.model.DatabaseConfig;
import com.backend.mybatis.generator.model.DbType;
import com.backend.mybatis.generator.model.GeneratorConfig;
import com.backend.mybatis.generator.util.ConfigHelper;
import com.backend.mybatis.generator.util.DbUtil;
import com.backend.plat.sa.model.CodeMaker;
import com.backend.plat.sa.service.CodeMakeService;
import com.oxygen.web.WebHelper;

/**
 * 代码生成功能Controller
 * 
 * @author jason
 */
@Controller
@RequestMapping(value = "/codeGenerator")
public class CodeGeneratorAction extends BaseAction {

	@Autowired
	private CodeMakeService codeMakeService;

	/**
	 * 代码生成器首页
	 * 
	 * @param req
	 * @param rsp
	 * @return
	 */
	@RequestMapping(value = "/index")
	public String index(HttpServletRequest req, HttpServletResponse rsp) {
		return "/codeGenerator/index";
	}

	/**
	 * 得到数据源列表
	 * 
	 * @param req
	 * @param rsp
	 * @return
	 */
	@RequestMapping(value = "/getDatabaseList")
	public void getDatabaseList(HttpServletRequest req, HttpServletResponse rsp) {
		List<TreeDto> treeDtoList = new ArrayList<TreeDto>();
		List<DatabaseConfig> databaseConfigList = null;
		try {
			databaseConfigList = ConfigHelper.loadDatabaseConfig();
			// 一级数据源列表
			for (DatabaseConfig databaseConfig : databaseConfigList) {
				TreeDto databasetreeDto = new TreeDto();
				databasetreeDto.setId(databaseConfig.getName());
				databasetreeDto.setParent("#");
				databasetreeDto.setText(databaseConfig.getName());
				// databasetreeDto.setIcon("../jstree/default/computer.png");
				treeDtoList.add(databasetreeDto);
				// 二级数据源中的表列表
				List<String> tableList = DbUtil.getTableNames(databaseConfig);
				for (String s : tableList) {
					TreeDto tabletreeDto = new TreeDto();
					tabletreeDto.setId(s);
					tabletreeDto.setParent(databasetreeDto.getId());
					tabletreeDto.setText(s);
					// tabletreeDto.setIcon("../jstree/default/table.png");
					treeDtoList.add(tabletreeDto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String str = JSON.toJSONString(treeDtoList);
		WebHelper.outputJson(str, rsp);
	}

	/**
	 * 生成源码 action
	 * 
	 * @param req
	 * @param rsp
	 * @param e
	 * @param model
	 */
	@RequestMapping(value = "/generateSource",method = RequestMethod.POST)
	public void generateSource(HttpServletRequest req, HttpServletResponse rsp,
			@ModelAttribute("generatorConfig") GeneratorConfig generatorConfig,
			@ModelAttribute("databaseConfig") DatabaseConfig databaseConfig,
			@ModelAttribute("codeMaker") CodeMaker codeMaker,
			ModelMap model) {
		List<DatabaseConfig> databaseConfigList;
		try {
			databaseConfig = ConfigHelper.loadDatabaseConfigByName("localhost");
			generatorConfig.setConnectorJarPath(ConfigHelper.findConnectorLibPath(databaseConfig.getDbType()).replaceFirst("/", ""));
			generatorConfig.setDriverClass(DbType.valueOf(databaseConfig.getDbType()).getDriverClass());
			codeMaker.setDatabaseConfig(databaseConfig);
			codeMaker.setGeneratorConfig(generatorConfig);
			List<String> fields = DbUtil.getColumnCommentByTableName(databaseConfig, generatorConfig.getTableName());
			codeMaker.setFields(fields);
			codeMaker.setPackageName("com.bit.product");
			codeMaker.setModelName(generatorConfig.getDomainObjectName());
			codeMaker.setTableName(generatorConfig.getTableName());
			// mybatis generator 生成源码DAO Model Mapper
			codeMakeService.makeMybatisGeneratorCode(codeMaker);
			//生成service层代码
			codeMakeService.makeServiceCode(codeMaker);
			codeMakeService.makeServiceImplCode(codeMaker);
			//生成列表页面展示代码
			codeMakeService.makeListViewCode(codeMaker);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 查询数据源
	 * @param req
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/loadDatabase")
	public String loadDatabase(HttpServletRequest req,ModelMap model){
		String id = req.getParameter("id");                //对应的表名
		String parentId = req.getParameter("parentId");    //数据源名称
		try {
			//生成Model Dao mapperXml
			DatabaseConfig databaseConfig = ConfigHelper.loadDatabaseConfigByName(parentId);
			GeneratorConfig generatorConfig = ConfigHelper.loadGeneratorConfig(id);
			model.put("generatorConfig", generatorConfig);
			model.put("databaseConfig", databaseConfig);
			//生成service api TODO
			//生成action      TODO
			//生成页面                       TODO
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/codeGenerator/editPanel";
	}
	
	/**
	 * 查询数据源
	 * @param req
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/edit/{id}")
	public String edit(HttpServletRequest req,ModelMap model){
		Map<String, Object> appendMap = new HashMap<String, Object>();
		return "/codeGenerator/edit";
	}
}
