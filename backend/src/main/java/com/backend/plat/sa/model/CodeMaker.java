package com.backend.plat.sa.model;

import java.lang.reflect.Field;
import java.util.List;

import com.backend.mybatis.generator.model.DatabaseConfig;
import com.backend.mybatis.generator.model.GeneratorConfig;
import com.oxygen.util.StringTool;

/**
 * 代码生成模版
 * @author jason
 */
public class CodeMaker {

	public String author;      //作者
	public String tableName;   //数据库对应表名
	public String pkfield;     //主键字段名,字段名
	public String pkCode;      //主键代码,属性名
	public String appCode;     //应用代码 如:sa
	public String modelCode;   //模型代码
	public String modelName;   //模型名称(汉字)
	public String srcName;     //源文件名
	public String packageName; //生成包名
	public String templatePath;//代码模版
	public DatabaseConfig databaseConfig;  //数据源信息
	public GeneratorConfig generatorConfig;//mybatis generator config 配置文件
	public List<String> fields;     //对象的属性集合
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getSrcName() {
		return srcName;
	}
	public void setSrcName(String srcName) {
		this.srcName = srcName;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getTemplatePath() {
		return templatePath;
	}
	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}
	public String getPkfield() {
		return pkfield;
	}
	public void setPkfield(String pkfield) {
		this.pkfield = pkfield;
	}
	public String getModelCode() {
		return modelCode;
	}
	public String getLowerModelCode() {
		String lowerCode=StringTool.lower1stLetter(modelCode);
		return lowerCode;
	}
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getPkCode() {
		return pkCode;
	}
	public void setPkCode(String pkCode) {
		this.pkCode = pkCode;
	}
	public String getAppCode() {
		return appCode;
	}
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	public DatabaseConfig getDatabaseConfig() {
		return databaseConfig;
	}
	public void setDatabaseConfig(DatabaseConfig databaseConfig) {
		this.databaseConfig = databaseConfig;
	}
	public GeneratorConfig getGeneratorConfig() {
		return generatorConfig;
	}
	public void setGeneratorConfig(GeneratorConfig generatorConfig) {
		this.generatorConfig = generatorConfig;
	}
	public List<String> getFields() {
		return fields;
	}
	public void setFields(List<String> fields) {
		this.fields = fields;
	}
}
