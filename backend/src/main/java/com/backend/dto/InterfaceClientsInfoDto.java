package com.backend.dto;

import java.util.List;

public class InterfaceClientsInfoDto {
	
	/**
	 * 客户端详情
	 */
	private InterfaceClientsDto interfaceClientsDto;
    
	/**
	 * 客户端关联的接口
	 */
	private List<ClientsRelateInterfaceDto> clientsRelateInterfaceDto;

	public InterfaceClientsDto getInterfaceClientsDto() {
		return interfaceClientsDto;
	}

	public void setInterfaceClientsDto(InterfaceClientsDto interfaceClientsDto) {
		this.interfaceClientsDto = interfaceClientsDto;
	}

	public List<ClientsRelateInterfaceDto> getClientsRelateInterfaceDto() {
		return clientsRelateInterfaceDto;
	}

	public void setClientsRelateInterfaceDto(
			List<ClientsRelateInterfaceDto> clientsRelateInterfaceDto) {
		this.clientsRelateInterfaceDto = clientsRelateInterfaceDto;
	}

	
	
	
	
	
}
