package com.backend.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class ProductDto implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 货品ID
     */
    private Integer goodsId;

    /**
     * 商品编码
     */
    private String productCode;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品简介
     */
    private String introduce;

    /**
     * 点击量
     */
    private Integer hit;

    /**
     * 商品列表小图片地址
     */
    private String picture;

    /**
     * 大图片地址
     */
    private String maxPicture;

    /**
     * 原价
     */
    private BigDecimal price;

    /**
     *  促销价 当没有规格的时候商品的价格
     */
    private BigDecimal nowPrice;

    /**
     * 销量
     */
    private Integer sellCount;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 商品状态(新增，已上架，已下架)
     */
    private String status;

    /**
     * 商品限购数量
     */
    private Integer productRestrictions;

    /**
     * 分类ID
     */
    private Integer catalogId;

    /**
     * 商品品牌ID
     */
    private Integer brandId;

    /**
     * 商家ID
     */
    private Integer businessId;

    /**
     * 上架时间
     */
    private Date shelvesTime;

    /**
     * 下架时间
     */
    private Date shelfTime;

    /**
     * 商品购买开始时间或者预售开始时间
     */
    private Date sellTime;

    /**
     * 商品购买结束时间或预售结束时间
     */
    private Date sellEndTime;

    /**
     * 商品补款开始时间
     */
    private Date replenishmentTime;

    /**
     * 商品补款结束时间
     */
    private Date replenishmentEndTime;

    /**
     * 预售价格
     */
    private BigDecimal depositPrice;

    /**
     * 尾款价格
     */
    private BigDecimal balancePrice;

    /**
     * 搜索关键字
     */
    private String searchKey;

    /**
     * 商品发售日期
     */
    private Date saleatDate;

    /**
     * 商品称重
     */
    private BigDecimal productWeight;

    /**
     * 商品单位
     */
    private String unit;

    /**
     * 商品类型
     */
    private String productType;

    /**
     * 商品发货仓库ID
     */
    private Integer expressId;

    /**
     * 商品售后服务ID
     */
    private Integer serviceId;

    /**
     * 商品库存总量
     */
    private Integer productTotal;

    /**
     * 创建者ID
     */
    private String createId;

    /**
     * 更新者ID
     */
    private String updateId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getMaxPicture() {
        return maxPicture;
    }

    public void setMaxPicture(String maxPicture) {
        this.maxPicture = maxPicture;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getNowPrice() {
        return nowPrice;
    }

    public void setNowPrice(BigDecimal nowPrice) {
        this.nowPrice = nowPrice;
    }

    public Integer getSellCount() {
        return sellCount;
    }

    public void setSellCount(Integer sellCount) {
        this.sellCount = sellCount;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getProductRestrictions() {
        return productRestrictions;
    }

    public void setProductRestrictions(Integer productRestrictions) {
        this.productRestrictions = productRestrictions;
    }

    public Integer getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Integer catalogId) {
        this.catalogId = catalogId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public Date getShelvesTime() {
        return shelvesTime;
    }

    public void setShelvesTime(Date shelvesTime) {
        this.shelvesTime = shelvesTime;
    }

    public Date getShelfTime() {
        return shelfTime;
    }

    public void setShelfTime(Date shelfTime) {
        this.shelfTime = shelfTime;
    }

    public Date getSellTime() {
        return sellTime;
    }

    public void setSellTime(Date sellTime) {
        this.sellTime = sellTime;
    }

    public Date getSellEndTime() {
        return sellEndTime;
    }

    public void setSellEndTime(Date sellEndTime) {
        this.sellEndTime = sellEndTime;
    }

    public Date getReplenishmentTime() {
        return replenishmentTime;
    }

    public void setReplenishmentTime(Date replenishmentTime) {
        this.replenishmentTime = replenishmentTime;
    }

    public Date getReplenishmentEndTime() {
        return replenishmentEndTime;
    }

    public void setReplenishmentEndTime(Date replenishmentEndTime) {
        this.replenishmentEndTime = replenishmentEndTime;
    }

    public BigDecimal getDepositPrice() {
        return depositPrice;
    }

    public void setDepositPrice(BigDecimal depositPrice) {
        this.depositPrice = depositPrice;
    }

    public BigDecimal getBalancePrice() {
        return balancePrice;
    }

    public void setBalancePrice(BigDecimal balancePrice) {
        this.balancePrice = balancePrice;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public Date getSaleatDate() {
        return saleatDate;
    }

    public void setSaleatDate(Date saleatDate) {
        this.saleatDate = saleatDate;
    }

    public BigDecimal getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(BigDecimal productWeight) {
        this.productWeight = productWeight;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Integer getExpressId() {
        return expressId;
    }

    public void setExpressId(Integer expressId) {
        this.expressId = expressId;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(Integer productTotal) {
        this.productTotal = productTotal;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId;
    }

    public String getUpdateId() {
        return updateId;
    }

    public void setUpdateId(String updateId) {
        this.updateId = updateId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}