package com.backend.dto;

public class ClientsRelateInterfaceDto {
	
    private Integer idw;

    private Integer interfaceId;

    private Integer clientId;
    
    private String clientName;
    
    private String interfaceName;


    public Integer getIdw() {
        return idw;
    }

    public void setIdw(Integer idw) {
        this.idw = idw;
    }

    public Integer getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(Integer interfaceId) {
        this.interfaceId = interfaceId;
    }

 

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
    
    
    
}
