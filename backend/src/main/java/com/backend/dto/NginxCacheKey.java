package com.backend.dto;

import java.util.Date;

public class NginxCacheKey {
	private Integer id;

	private String name;

	private String cacheType;

	private String cacheKey;

	private Date createTime;
	private String cacheSite;
	private String nginxServer;

	private String status;
	private String content;
	private String codes;
	private String typeValue;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCacheType() {
		return cacheType;
	}

	public void setCacheType(String cacheType) {
		this.cacheType = cacheType;
	}

	public String getCacheKey() {
		return cacheKey;
	}

	public void setCacheKey(String cacheKey) {
		this.cacheKey = cacheKey;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCacheSite() {
		return cacheSite;
	}

	public void setCacheSite(String cacheSite) {
		this.cacheSite = cacheSite;
	}

	public String getNginxServer() {
		return nginxServer;
	}

	public void setNginxServer(String nginxServer) {
		this.nginxServer = nginxServer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCodes() {
		return codes;
	}

	public void setCodes(String codes) {
		this.codes = codes;
	}

	public String getTypeValue() {
		return typeValue;
	}

	public void setTypeValue(String typeValue) {
		this.typeValue = typeValue;
	}
}