package com.backend.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 通用的用户业务实体类
 * @author jason
 */
public class UserDto implements Serializable{
	 private String id;          //主键ID

	    private String username;    //用户名

	    private String password;    //密码

	    private String realName;    //真实姓名

	    private String nickName;    //昵称

	    private String email;       //邮箱

	    private String mobilePhone; //电话

	    private String registSource;//用户注册来源

	    private String status;      //用户状态

	    private String registTime;  //注册时间

	    private String isSuperuser; //是否是超级用户

	    private String userType;    //用户类型
	    
	    private String userTypeName;//用户类型名称
	    
	    private List<UserRelateRoleDto> roleList;     	//用户角色列表
	    
	    private List<UserRelateOrgDto> orgList;      	//用户组织列表
	    
	    private String checkedOrgId;                  	//选中的用户组织ID
	    
	    private List<PermissionRelateRoleDto> menuList; //角色的菜单集合
	    
	    private List<PermissionRelateRoleDto> funList;  //功能集合

	    public String getId() {
	        return id;
	    }

	    public void setId(String id) {
	        this.id = id;
	    }

	    public String getUsername() {
	        return username;
	    }

	    public void setUsername(String username) {
	        this.username = username;
	    }

	    public String getPassword() {
	        return password;
	    }

	    public void setPassword(String password) {
	        this.password = password;
	    }

	    public String getRealName() {
	        return realName;
	    }

	    public void setRealName(String realName) {
	        this.realName = realName;
	    }

	    public String getNickName() {
	        return nickName;
	    }

	    public void setNickName(String nickName) {
	        this.nickName = nickName;
	    }

	    public String getEmail() {
	        return email;
	    }

	    public void setEmail(String email) {
	        this.email = email;
	    }

	    public String getMobilePhone() {
	        return mobilePhone;
	    }

	    public void setMobilePhone(String mobilePhone) {
	        this.mobilePhone = mobilePhone;
	    }

	    public String getRegistSource() {
	        return registSource;
	    }

	    public void setRegistSource(String registSource) {
	        this.registSource = registSource;
	    }

	    public String getStatus() {
	        return status;
	    }

	    public void setStatus(String status) {
	        this.status = status;
	    }

	    public String getIsSuperuser() {
	        return isSuperuser;
	    }

	    public void setIsSuperuser(String isSuperuser) {
	        this.isSuperuser = isSuperuser;
	    }

	    public String getUserType() {
	        return userType;
	    }

	    public void setUserType(String userType) {
	        this.userType = userType;
	    }

		public String getUserTypeName() {
			return userTypeName;
		}

		public void setUserTypeName(String userTypeName) {
			this.userTypeName = userTypeName;
		}

		public String getRegistTime() {
			return registTime;
		}

		public void setRegistTime(String registTime) {
			this.registTime = registTime;
		}

		public List<UserRelateRoleDto> getRoleList() {
			return roleList;
		}

		public void setRoleList(List<UserRelateRoleDto> roleList) {
			this.roleList = roleList;
		}

		public List<UserRelateOrgDto> getOrgList() {
			return orgList;
		}

		public void setOrgList(List<UserRelateOrgDto> orgList) {
			this.orgList = orgList;
		}

		public String getCheckedOrgId() {
			return checkedOrgId;
		}

		public void setCheckedOrgId(String checkedOrgId) {
			this.checkedOrgId = checkedOrgId;
		}

		public List<PermissionRelateRoleDto> getMenuList() {
			return menuList;
		}

		public void setMenuList(List<PermissionRelateRoleDto> menuList) {
			this.menuList = menuList;
		}

		public List<PermissionRelateRoleDto> getFunList() {
			return funList;
		}

		public void setFunList(List<PermissionRelateRoleDto> funList) {
			this.funList = funList;
		}

	}
 