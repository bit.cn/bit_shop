package com.backend.dto;

import java.util.List;

/**
 * 功能菜单关联实体类
 * @author jason
 */
public class FunctionRelateMenuDto {
    private Integer id;     //主键ID

    private Integer menuId; //菜单ID

    private Integer funId;  //功能ID
    
    private String funName; //功能名称
    
    private List<FunctionRelateMenuDto> functionRelateMenuList; //菜单关联的功能集合

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getFunId() {
        return funId;
    }

    public void setFunId(Integer funId) {
        this.funId = funId;
    }
    
	public List<FunctionRelateMenuDto> getFunctionRelateMenuList() {
		return functionRelateMenuList;
	}

	public void setFunctionRelateMenuList(
			List<FunctionRelateMenuDto> functionRelateMenuList) {
		this.functionRelateMenuList = functionRelateMenuList;
	}
	
	public String getFunName() {
		return funName;
	}

	public void setFunName(String funName) {
		this.funName = funName;
	}
}