package com.backend.dto;

/**
 * 用户隶属组织
 * @author jason
 */
public class UserRelateOrg {
    private Integer id;      //主键ID

    private String userId;   //用户ID

    private Integer orgId;   //组织ID

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }
}