package com.backend.dto;

import java.util.Date;

/**
 * 权限功能包装实体类
 * @author jason
 */
public class PermissionFunctionDto {
    private Integer id;        //主键ID

    private String name;       //权限功能名称

    private String code;       //权限功能code
    
    private String onclick;    //操作事件

    private String createTime; //创建时间

    private String updateTime; //更新时间

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getOnclick() {
		return onclick;
	}

	public void setOnclick(String onclick) {
		this.onclick = onclick;
	}

}
