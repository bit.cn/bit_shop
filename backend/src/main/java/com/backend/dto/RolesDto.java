package com.backend.dto;

import java.util.List;

/**
 * 角色封装实体类
 * @author jason
 */
public class RolesDto {
	private Integer id;        //主键ID

	private String name;       //角色名称

	private String code;

	private Integer parentId;

	private String createTime;

	private String updateTime;

	private String description;
	
	private List<PermissionRelateRoleDto> menuList;  //角色的菜单集合
	
	private List<PermissionRelateRoleDto> funList;   //功能集合
	
	private String checkedMenuId;                     //选中的用户菜单ID

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<PermissionRelateRoleDto> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<PermissionRelateRoleDto> menuList) {
		this.menuList = menuList;
	}

	public List<PermissionRelateRoleDto> getFunList() {
		return funList;
	}

	public void setFunList(List<PermissionRelateRoleDto> funList) {
		this.funList = funList;
	}

	public String getCheckedMenuId() {
		return checkedMenuId;
	}

	public void setCheckedMenuId(String checkedMenuId) {
		this.checkedMenuId = checkedMenuId;
	}

}
