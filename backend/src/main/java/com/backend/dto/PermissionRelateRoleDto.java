package com.backend.dto;

import java.util.List;

/**
 * 角色与菜单关联实体类
 * @author jason
 */
public class PermissionRelateRoleDto {
	 	private Integer id;

	    private Integer roleId;

	    private Integer funId;    //功能ID
	    
	    private String funName;   //功能名称
	    
	    private String funCode;   //功能操作的CODE

	    private Integer menuId;
	    
	    private String name;      //菜单名称

		private String code;      //菜单code
		
		private String menuUrl;   //菜单链接地址

		private Integer parentId; //父节点ID
		
		private List<FunctionRelateMenuDto> functionRelateMenuList; //菜单关联的功能集合

	    public Integer getId() {
	        return id;
	    }

	    public void setId(Integer id) {
	        this.id = id;
	    }

	    public Integer getRoleId() {
	        return roleId;
	    }

	    public void setRoleId(Integer roleId) {
	        this.roleId = roleId;
	    }

	    public Integer getFunId() {
	        return funId;
	    }

	    public void setFunId(Integer funId) {
	        this.funId = funId;
	    }

	    public Integer getMenuId() {
	        return menuId;
	    }

	    public void setMenuId(Integer menuId) {
	        this.menuId = menuId;
	    }

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMenuUrl() {
			return menuUrl;
		}

		public void setMenuUrl(String menuUrl) {
			this.menuUrl = menuUrl;
		}

		public Integer getParentId() {
			return parentId;
		}

		public void setParentId(Integer parentId) {
			this.parentId = parentId;
		}

		public List<FunctionRelateMenuDto> getFunctionRelateMenuList() {
			return functionRelateMenuList;
		}

		public void setFunctionRelateMenuList(
				List<FunctionRelateMenuDto> functionRelateMenuList) {
			this.functionRelateMenuList = functionRelateMenuList;
		}

		public String getFunName() {
			return funName;
		}

		public void setFunName(String funName) {
			this.funName = funName;
		}

		public String getFunCode() {
			return funCode;
		}

		public void setFunCode(String funCode) {
			this.funCode = funCode;
		}
	}