package com.backend.tag;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import com.backend.dto.PermissionRelateRoleDto;
import com.backend.dto.UserDto;
import com.oxygen.constants.Constant;
import com.oxygen.util.StringUtil;

/**
 * 权限超链接
 * 
 * @author jason
 * 
 */
public class A extends TagSupport {

	private static final long serialVersionUID = 4677074422159732225L;

	private static final Logger log = Logger.getLogger(Button.class);

	private HttpServletRequest request;
	private String style;
	private String name;
	private String id;
	private String onClick;
	private String href;
	private String funcationCode;
	private HttpSession session;

	public int doEndTag() throws JspException {
		return super.doEndTag();
	}

	public int doStartTag() throws JspException {
		try {
			StringBuffer buf = new StringBuffer();
			UserDto user = (UserDto) this.session.getAttribute(Constant.BACKEND_SESSION_USER_INFO);
			if (user != null) {
				List<PermissionRelateRoleDto> menuList = user.getMenuList();
				for (PermissionRelateRoleDto permissionRelateRoleDto : menuList) {
					if(permissionRelateRoleDto.getCode().equals(funcationCode)){
						buf.append("<a class='" + style + "' id='" + id).append("'");
						if (!StringUtil.isEmpty(this.onClick)) {
							buf.append(" href='javascript:void(0)'");
							buf.append(" onClick='" + onClick + "';>");
						} else {
							buf.append(" href='" + href + "'>");
						}
						buf.append(name + "</a>");
					} else {
						buf.append("<a class='" + style + "' id='" + id).append("'");
						if (!StringUtil.isEmpty(this.onClick)) {
							buf.append(" href='javascript:void(0)'");
							buf.append(" onClick='" + onClick + "';>");
						} else {
							buf.append(" href='" + href + "'>");
						}
						buf.append(name + "</a>");
					}
				}
			} else {
				buf.append("<a class='" + style + "' id='" + id).append("'");
				if (!StringUtil.isEmpty(this.onClick)) {
					buf.append(" href='javascript:void(0)'");
					buf.append(" onClick='" + onClick + "';>");
				} else {
					buf.append(" href='" + href + "'>");
				}
				buf.append(name + "</a>");
				// buf.append(name);
			}
			pageContext.getOut().write(buf.toString());
		} catch (Exception e) {
			log.error("生成按钮异常", e);
		}
		return EVAL_PAGE;
	}

	@Override
	public void setPageContext(PageContext context) {
		super.setPageContext(context);
		request = (HttpServletRequest) pageContext.getRequest();
		session = request.getSession();
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setOnClick(String onClick) {
		this.onClick = onClick;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public void setFuncationCode(String funcationCode) {
		this.funcationCode = funcationCode;
	}

}
