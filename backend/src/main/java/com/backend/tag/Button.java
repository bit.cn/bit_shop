package com.backend.tag;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import com.backend.dto.FunctionRelateMenuDto;
import com.backend.dto.PermissionRelateRoleDto;
import com.backend.dto.UserDto;
import com.oxygen.constants.Constant;
import com.oxygen.util.StringUtil;

/**
 * 权限按钮
 * @author jason
 */
public class Button extends TagSupport {
	private static final long serialVersionUID = 4677074422159732225L;
	private static final Logger log = Logger.getLogger(Button.class);

	private HttpServletRequest request;
	private String style;
	private String name;
	private String buttonId;
	private String menuId;
	private String onClick;
	private String type;
	private String funcationCode;
	private HttpSession session;

	public void setType(String type) {
		this.type = type;
	}

	public int doEndTag() throws JspException {
		return super.doEndTag();
	}

	public int doStartTag() throws JspException {

		try {
			StringBuffer buf = new StringBuffer();
			UserDto user = (UserDto) this.session.getAttribute(Constant.BACKEND_SESSION_USER_INFO);
			if (user != null) {
				List<PermissionRelateRoleDto> menuList = user.getMenuList();
				for (PermissionRelateRoleDto permissionRelateRoleDto : menuList) {
					if(menuId.equals(permissionRelateRoleDto.getMenuId().toString())){
						List<FunctionRelateMenuDto> functionRelateMenuList = permissionRelateRoleDto.getFunctionRelateMenuList();
						for (FunctionRelateMenuDto functionRelateMenuDto : functionRelateMenuList) {
							System.out.println(functionRelateMenuDto.getFunId().toString());
								if(functionRelateMenuDto.getFunId().toString().equals(funcationCode)){
									buf.append("<input type='" + type + "'  class='" + style + "' id='" + funcationCode + "' value='"+functionRelateMenuDto.getFunName()+"'");
									if (!StringUtil.isEmpty(this.onClick)) {
										buf.append(" onclick=" + onClick + "");
									}
									buf.append(">");
							}
						}
					}
				}
			} else {
				buf.append("<input  type='" + type + "' class='" + style + "' id='" + buttonId + "' value='" + name + "' onclick='" + onClick + "';\">");
			}
			pageContext.getOut().write(buf.toString());
		} catch (Exception e) {
			log.error("生成按钮异常", e);
		}
		return EVAL_PAGE;
	}

	@Override
	public void setPageContext(PageContext context) {
		super.setPageContext(context);
		request = (HttpServletRequest) pageContext.getRequest();
		session = request.getSession();
	}

	public void setButtonId(String buttonId) {
		this.buttonId = buttonId;
	}

	public void setOnClick(String onClick) {
		this.onClick = onClick;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public void setFuncationCode(String funcationCode) {
		this.funcationCode = funcationCode;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
}
