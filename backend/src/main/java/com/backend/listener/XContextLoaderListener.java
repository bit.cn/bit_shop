package com.backend.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;

import com.backend.mybatis.generator.util.SpringTool;

public class XContextLoaderListener extends ContextLoaderListener  {
	public void contextInitialized(ServletContextEvent event) {
		System.setProperty("user.timezone", "Asia/Shanghai");//设置系统时区
		ServletContext c=event.getServletContext();
		SpringTool.setServletContext(c);
		super.contextInitialized(event);
	}

}
