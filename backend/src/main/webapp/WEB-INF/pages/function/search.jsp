<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>权限搜索</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>
<body>
<div class="main user-search">
	<div class="main-top">    
            <h3>权限搜索</h3>              
    </div><!--main-top-->

<form id="searchForm" action="${ctx}/function/list" method="post">
<!--用户搜索-->
<div class="panel panel-info">
  <div class="panel-heading">请先根据条件搜索权限，然后选择相应的操作。</div>
  <table class="table">
  		<tbody>
                <tr>
                    <th width="15%" class="active">名称</th>
                    <td width="85%"><input type="text" class="form-control" name="user_names" value="${name}"/><span class="text-muted">多个名称用半角<code>,</code>隔开</span></td>
                </tr>
                <tr>
                    <th class="active">编号</th>
                    <td><input type="text" class="form-control" name="user_ids" value="${code}"/><span class="text-muted">多个CODE用半角<code>,</code>隔开</span></td>
                </tr>
        </tbody>
  </table>
</div><!--用户搜索 结束-->
<div class="submit">
      <button type="submit" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-search"></span> 搜索</button>
      <button type="button" class="btn btn-primary" onclick="location='${ctx}/function/list'">显示全部</button>
</div>
</form>
</div><!--main end-->
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript" src="${ctx}/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
</script>
</body>
</html>
