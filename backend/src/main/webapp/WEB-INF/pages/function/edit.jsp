<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>编辑权限</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
</head>
<body>
	<div class="main user-editor">
		<div class="main-top">
			<h3>编辑权限</h3>
		</div>
		<!--main-top-->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation"><a onclick="javascript:returnToList();"
				href="javascript:;"><span class="glyphicon glyphicon-arrow-left"></span>
					返回列表</a></li>
			<li role="presentation" class="active"><a href="#"></a></li>
		</ul>
		<div class="panel-group" id="accordion" role="tablist"
			aria-multiselectable="true">
			<!--基本信息-->
			<div class="panel panel-info">
				<div class="panel-heading" data-toggle="collapse"
					data-parent="#accordion" href="#collapseAPK" aria-expanded="true"
					aria-controls="collapseThree">
					<h4 class="panel-title">基本信息</h4>
				</div>
				<div id="collapseAPK" class="panel-collapse collapse in"
					role="tabpanel">
					<table class="table">
						<form id="editForm" role="form" action="${ctx}/function/save"
							method="post">
							<tbody>
								<tr>
									<th class="active" width="15%">权限名称</th>
									<c:if test="${not empty function}">
										<input type="hidden" class="form-control" name="id"
											value="${function.id}" />
									</c:if>
									<td width="85%"><input type="text" name="name"
										class="form-control" value="${function.name}" /></td>
								</tr>
								<tr>
									<th class="active">权限编号</th>
									<td><input type="text" class="form-control" name="code"
										value="${function.code}" /></td>
								</tr>
								<tr>
									<th class="active">操作事件</th>
									<td><input type="text" class="form-control" name="onclick"
										value="${function.onclick}" /></td>
								</tr>
							</tbody>
						</form>
					</table>
				</div>
			</div>
		</div>
		<div class="submit">
			<c:choose>
				<c:when test="${empty function}">
					<button type="button" id="formButton"
						onclick="javascript:saveFunction();"
						class="btn btn-success loading-btn" data-loading-text="Loading..."
						autocomplete="off">
						<span class="glyphicon glyphicon-ok"></span> 确认提交
					</button>
				</c:when>
				<c:otherwise>
					<button type="button" id="formButton"
						onclick="javascript:updateFunction();"
						class="btn btn-success loading-btn" data-loading-text="Loading..."
						autocomplete="off">
						<span class="glyphicon glyphicon-ok"></span> 确认提交
					</button>
				</c:otherwise>
			</c:choose>
			<button type="button" class="btn btn-default"
				onclick="javascript:returnToList();">返回列表</button>
		</div>

	</div>
	<!--main end-->
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script type="text/javascript">
		function returnToList() {
			parent.closeThenAddTab("用户管理", "${ctx}/function/list");
		}

		//保存功能信息
		function saveFunction() {
			$("#editForm").ajaxForm({
				success : function(event, param) {
					var apiRsp = getApiJson(param.data);
					if (apiRsp) {
						if (apiRsp.isSuccess) {
							alert('修改成功!');
							parent.location.reload();
							parent.close();
						} else {
							alert(apiRsp.msg);
						}
					} else {
						alert("返回异常");
					}
				}
			});
		}

		//更新功能信息
		function updateFunction() {
			$("#editForm").attr("action", "${ctx}/function/update");
			$("#editForm").ajaxForm({
				success : function(event, param) {
					var apiRsp = getApiJson(param.data);
					if (apiRsp) {
						if (apiRsp.isSuccess) {
							alert('修改成功!');
							parent.location.reload();
							parent.close();
						} else {
							alert(apiRsp.msg);
						}
					} else {
						alert("返回异常");
					}
				}
			});
		}
	</script>
</body>
</html>