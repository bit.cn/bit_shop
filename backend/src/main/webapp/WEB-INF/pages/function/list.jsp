<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>权限管理</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
</head>
<body>
	<div class="main user-management">
		<div class="main-top">
			<h3>
				权限管理 <small>共搜索到<span>${apiRsp.count}</span>名符合条件的用户
				</small>
			</h3>
			<a
				href="${ctx}/function/search?user_names=${user_names}&user_ids=${user_ids}&user_phones=${user_phones}&user_emails=${user_emails}&user_level_start=${user_level_start}&user_level_end=${user_level_end}&signup_date_start=${signup_date_start}&signup_date_end=${signup_date_end}&login_date_start=${login_date_start}&login_date_end=${login_date_end}&user_sinupIps=${user_sinupIps}&user_loginIps=${user_loginIps}"
				class="btn btn-default pull-right"><span
				class="glyphicon glyphicon-search"></span> 重新搜索</a>
		</div>
		<!--main-top-->
		<!--用户列表-->
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="pin">
					<form class="form-inline" role="form">
						<div class="form-group">
							<select id="batchOperateType" class="form-control">
								<option value="">批量操作</option>
								<option value="disable">禁用</option>
								<!-- <option>打入小黑屋</option> -->
								<option value="enable">恢复正常</option>
								<option value="delete">删除</option>
							</select>
						</div>
						<div class="btn-group">
							<button id="batchOperateBtn" type="button"
								class="btn btn-default">确认</button>
						</div>
						<button id="addOperateBtn" type="button" onclick="addFun()"
							class="btn btn-default">新增</button>
					</form>
				</div>
			</div>
			<ol class="breadcrumb">
				<li><a
					href="${ctx}/function/search?user_names=${user_names}&user_ids=${user_ids}&user_phones=${user_phones}&user_emails=${user_emails}&user_level_start=${user_level_start}&user_level_end=${user_level_end}&signup_date_start=${signup_date_start}&signup_date_end=${signup_date_end}&login_date_start=${login_date_start}&login_date_end=${login_date_end}&user_sinupIps=${user_sinupIps}&user_loginIps=${user_loginIps}">用户搜索</a></li>
				<li>权限管理</li>
			</ol>
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="th-checkbox"><input type="checkbox" id="check-btn"
							class="tag" title="" data-original-title="全选/反选"></th>
						<th>ID</th>
						<th>名称</th>
						<th>编号</th>
						<th>操作</th>
					</tr>
				</thead>
				<c:if
					test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
					<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}" />
				</c:if>
				<tbody id="moreUserListDiv" nextUrl="${nextUrl}">
					<c:forEach items="${apiRsp.results}" var="item">
						<tr>
							<td><input type="checkbox" name="list-checkbox"
								value="${item.id}"></td>
							<td>${item.id}</td>
							<td>${item.name}</td>
							<td>${item.code}</td>
							<td><span class="row-actions"> <a
									href="javascript:editFun('${item.id}','${item.name}');"><span
										class="glyphicon glyphicon-eye-open"></span>编辑</a> <a
									href="javascript:deleteFun('${item.id}','${item.name}');">
										<span class="glyphicon glyphicon-eye-open"></span>删除
								</a>
							</span></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!--用户列表结束-->
		<c:if test="${not empty nextUrl}">
			<div class="load_more">
				<button id="loadingDiv" style="display: none" type="button"
					class="btn btn-default btn-block loading-btn">Loading...</button>
				<button id="moreDiv" style="display:" type="button"
					class="btn btn-default btn-block loading-btn"
					data-loading-text="Loading..." autocomplete="off">加载更多</button>
			</div>
		</c:if>

		<div class="main-bottom">
			<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
				<jsp:param name="paginationObjectName" value="apiRsp" />
				<jsp:param name="pageNoName" value="" />
				<jsp:param name="requestUrl" value="${pagingUrl}" />
				<jsp:param name="refreshDiv" value="" />
			</jsp:include>
		</div>
		<!--main-bottom-->
	</div>
	<!--main end-->
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script type="text/javascript">
/*表格排序*/
(function($){$.fn.stupidtable=function(sortFns){return this.each(function(){var $table=$(this);sortFns=sortFns||{};sortFns=$.extend({},$.fn.stupidtable.default_sort_fns,sortFns);$table.on("click.stupidtable","thead th",function(){var $this=$(this);var th_index=0;var dir=$.fn.stupidtable.dir;$this.parents("tr").find("th").slice(0,$this.index()).each(function(){var cols=$(this).attr("colspan")||1;th_index+=parseInt(cols,10)});var sort_dir=$this.data("sort-default")||dir.ASC;if($this.data("sort-dir"))sort_dir=$this.data("sort-dir")===dir.ASC?dir.DESC:dir.ASC;var type=$this.data("sort")||null;if(type===null){return}$table.trigger("beforetablesort",{column:th_index,direction:sort_dir});$table.css("display");setTimeout(function(){var column=[];var sortMethod=sortFns[type];var trs=$table.children("tbody").children("tr");trs.each(function(index,tr){var $e=$(tr).children().eq(th_index);var sort_val=$e.data("sort-value");var order_by=typeof(sort_val)!=="undefined"?sort_val:$e.text();column.push([order_by,tr])});column.sort(function(a,b){return sortMethod(a[0],b[0])});if(sort_dir!=dir.ASC)column.reverse();trs=$.map(column,function(kv){return kv[1]});$table.children("tbody").append(trs);$table.find("th").data("sort-dir",null).removeClass("sorting-desc sorting-asc");$this.data("sort-dir",sort_dir).addClass("sorting-"+sort_dir);$table.trigger("aftertablesort",{column:th_index,direction:sort_dir});$table.css("display")},10)})})};$.fn.stupidtable.dir={ASC:"asc",DESC:"desc"};$.fn.stupidtable.default_sort_fns={"int":function(a,b){return parseInt(a,10)-parseInt(b,10)},"float":function(a,b){return parseFloat(a)-parseFloat(b)},"string":function(a,b){return a.localeCompare(b)},"string-ins":function(a,b){a=a.toLocaleLowerCase();b=b.toLocaleLowerCase();return a.localeCompare(b)}}})(jQuery);$(function(){var table=$("table").stupidtable();table.on("aftertablesort",function(event,data){var th=$(this).find("th");th.find(".arrow").remove();var dir=$.fn.stupidtable.dir;var arrow=data.direction===dir.ASC?"&uArr;":"&dArr;";th.eq(data.column).append('<span class="arrow">'+arrow+'</span>')})});	
/*表格排序 结束*/
function batchOperate(batchOperateType,idArr){
	var operateTitle="";
	var url="";
	if(batchOperateType=="delete"){
		operateTitle="批量删除";
		url="${ctx}/function/delete";
	}else if(batchOperateType=="disable"){
		operateTitle="批量禁用";
		url="${ctx}/function/disable";
	}else if(batchOperateType=="enable"){
		operateTitle="批量启用";
		url="${ctx}/function/enable";
	}
	ajaxSubmit(url, {ids:idArr.join(",")}, reload, operateTitle+"成功", "确认"+operateTitle+"？");
}
function resetPassword(id){
	var successCallback=function(event,param){
		var apiRsp=getApiJson(param.data);
		alert("重置成功，新密码为："+apiRsp.results);
	};
	ajaxSubmit("${ctx}/function/resetPassword?id="+id, {}, successCallback, null, "确认重置密码？");
}

function bindEvents(){
	$(".pin").pin();
	/*全选 取消全选*/	
	var check_btn = document.getElementById("check-btn");
	var check_name = document.getElementsByName("list-checkbox");
	check_btn.onclick = function(){
		for(var i=1; i<=check_name.length; i+=1){
			if(check_name[i-1].checked){
				check_name[i-1].checked = false;
			}else{
				check_name[i-1].checked = true;
			}
		}
	};
}
$(function(){
	$("#moreDiv").loadingMore({
		dataDivId:'moreUserListDiv',
		successCallback:function(){
			bindEvents();
		}
	});	
	$("#batchOperateBtn").click(function(){
		var batchOperateType=$("#batchOperateType").val();
		var check_name = document.getElementsByName("list-checkbox");
		var idArr=new Array();
		for(var i=0;i<check_name.length;i++){
			if(check_name[i].checked){
				idArr.push(check_name[i].value);
			}
		}
		if(batchOperateType!="" && idArr.length>0){
			batchOperate(batchOperateType,idArr);
		}
	});
});

//新增功能
function addFun(){
	parent.addTab("新增功能","${ctx}/function/add/");
}

//编辑功能
function editFun(id,username){
	parent.addTab("编辑功能("+username+")","${ctx}/function/edit/"+id);
}

//删除功能
function deleteFun(id, username){
	ajaxSubmit("${ctx}/function/delete", {id : id}, reload, "操作成功!","确认删除？");
}
</script>
</body>
</html>
