<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>编辑接口</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
<link rel="stylesheet" href="${ctx}/css/bootstrap-multiselect.css"
	type="text/css">
<link rel="stylesheet" href="${ctx}/jstree/default/style.min.css">
</head>
<body>

	<div class="main user-editor">

		<div class="main-top">
			<h3>编辑接口</h3>
		</div>
		<!--main-top-->

		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation"><a onclick="javascript:returnToList();"
				href="javascript:;"><span class="glyphicon glyphicon-arrow-left"></span>
					返回列表</a></li>
			<li role="presentation" class="active"><a href="#">${interfaces.name}</a></li>
		</ul>
		<form id="editForm" role="form" action="${ctx}/interfaces/save"
			method="post">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

				<!--基本信息-->
				<div class="panel panel-info">

					<div class="panel-heading" data-toggle="collapse"
						data-parent="#accordion" href="#collapseAPK" aria-expanded="true"
						aria-controls="collapseThree">
						<h4 class="panel-title">基本信息</h4>
					</div>
					<div id="collapseAPK" class="panel-collapse collapse in"
						role="tabpanel">
						<table class="table">
							<tbody>
								<tr>
									<th class="active" width="15%">接口名</th>
									<td width="85%"><input type="text" name="name"
										class="form-control" value="${interfaces.name}" disabled />
										<input type="hidden"
										id="id" name="id" class="form-control" value="${interfaces.id}">
										</td>
								</tr>
								<tr>
									<th class="active">接口Code</th>
									<td><input type="text" class="form-control"
										name="code" value="${interfaces.code}"
										disabled="disabled" /></td>
								</tr>
								
								<tr>
									<th class="active">接口描述</th>
									<td><input type="text" class="form-control" name="summary"
										value="${interfaces.summary}" /></td>
								</tr>
							
							</tbody>
						</table>
					</div>
				</div>
				<!--基本信息 结束-->
			</div>
			<div class="submit">
				<button type="button" id="formButton"
					class="btn btn-success loading-btn" data-loading-text="Loading..."
					autocomplete="off">
					<span class="glyphicon glyphicon-ok"></span> 确认提交
				</button>
				<button type="button" id="formButton"
					class="btn btn-success loading-btn" data-loading-text="Loading..."
					autocomplete="off" onclick="javascript:returnToList();">
					<span class="glyphicon glyphicon-ok"></span>返回列表
				</button>
			</div>
		</form>
	</div>
	<!--main end-->
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
	<script src="${ctx}/jstree/jstree.js" type="text/javascript"></script>
	<script src="${ctx}/js/x_jstree.js" type="text/javascript"></script>
	<script type="text/javascript">
		function returnToList() {
			parent.closeThenAddTab("接口管理", "${ctx}/interfaces/list");
		}
		$(function() {

			$("#formButton").click(function() {
				$.ajax({
					cache : true,
					type : "POST",
					url : $("#editForm").attr("action"),
					data : $("#editForm").serialize(),// 你的formid
					async : false,
					error : function(request) {
						alert("返回异常");
					},
					success : function(data) {
						alert('修改成功!');
						parent.location.reload();
						parent.close();
					}
				});
			});
			
		});
	</script>
</body>
</html>