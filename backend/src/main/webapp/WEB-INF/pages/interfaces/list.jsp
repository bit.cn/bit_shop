<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>接口管理</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>
<body>
<div class="main user-management">
    	<div class="main-top">
           <h3>接口管理 <small>共搜索到<span>${apiRsp.count}</span>名符合条件的接口</small></h3>  
            <form id="searchForm" action="${ctx}/user/list" method="post" class="form-inline pull-right search-box" role="form">
            	<input type="hidden" id="page_size" name="page_size" value="${page_size}">
                <a href="${ctx}/user/list?page_size=${page_size}" class="btn btn-primary">显示全部</a>
            </form>
    </div><!--main-top-->
<!--接口列表-->
<div class="panel panel-info">
  <div class="panel-heading">
        <div class="pin">
                 <form class="form-inline" role="form">
                    <div class="form-group">
                          <select id="batchOperateType" class="form-control">
                                    <option value="">批量操作</option>
                                    <option value="disable">禁用</option>
                                    <!-- <option>打入小黑屋</option> -->
                                    <option value="enable">恢复正常</option>
                                    <option value="delete">删除</option>
                          </select>
                    </div>
                    <div class="btn-group">
                      <button id="batchOperateBtn" type="button" class="btn btn-default">确认</button>
                    </div>
                     <div class="btn-group">
                       <button id="addOperateBtn" type="button" class="btn btn-default">新增</button>
                     </div>
                      <div class="btn-group">
                       <button id="initOperateBtn" type="button" class="btn btn-default" onclick="initInteface()">初始化接口</button>
                     </div>
                  </form>
        </div>   
  </div>
    <ol class="breadcrumb">
      <li><a href="${ctx}/user/search?username=${username}">接口搜索</a></li>
      <li>接口管理</li>
    </ol>
          <table class="table table-hover">
            <thead>
              <tr>
                <th class="th-checkbox"><input type="checkbox" id="check-btn" class="tag" title="" data-original-title="全选/反选"></th>
                <th>ID</th>
                <th>接口名称</th>
                <th>接口CODE</th>
                <th>接口分类</th>
                <th>创建人</th>
                <th>修改人</th>
                <th>操作</th>
              </tr>
            </thead>
	     	  <c:set var="pagingUrl" value="${ctx}/interfaces/list?searchValue=${searchValue}&page_size=${page_size}"/>
			 <c:set var="nextUrl" value=""/>
			 <c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
			 <c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
			 </c:if>
			<tbody id="moreUserListDiv" nextUrl="${nextUrl}">
            	<c:forEach items="${apiRsp.results}" var="item">
              	<tr>
                      <td><input type="checkbox" name="list-checkbox" value="${item.id}"></td>
                      <td>${item.id}</td>
                      <td>${item.name}</td>
                      <td>${item.code}</td>
                      <td>${item.summary}</td>
                      <td>${item.createUser}</td>
                      <td>${item.createTime}</td> 
                      <td>
                          <span class="row-actions">
                          	<a href="javascript:editClientInteface('${item.id}','${item.name}');"><span class="glyphicon glyphicon-eye-open"></span>编辑</a>
                          </span>
                      </td>       
                </tr>
                </c:forEach>
            </tbody>
          </table>

</div><!--接口列表结束-->
    <c:if test="${not empty nextUrl}">
	    <div class="load_more">
	    	<button id="loadingDiv" style="display:none" type="button" class="btn btn-default btn-block loading-btn">Loading...</button>
	    	<button id="moreDiv" style="display:" type="button" class="btn btn-default btn-block loading-btn" data-loading-text="Loading..." autocomplete="off">加载更多</button>
	    </div>
    </c:if>
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="apiRsp" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${pagingUrl}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div><!--main-bottom-->
</div><!--main end-->

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
/*表格排序*/
(function($){$.fn.stupidtable=function(sortFns){return this.each(function(){var $table=$(this);sortFns=sortFns||{};sortFns=$.extend({},$.fn.stupidtable.default_sort_fns,sortFns);$table.on("click.stupidtable","thead th",function(){var $this=$(this);var th_index=0;var dir=$.fn.stupidtable.dir;$this.parents("tr").find("th").slice(0,$this.index()).each(function(){var cols=$(this).attr("colspan")||1;th_index+=parseInt(cols,10)});var sort_dir=$this.data("sort-default")||dir.ASC;if($this.data("sort-dir"))sort_dir=$this.data("sort-dir")===dir.ASC?dir.DESC:dir.ASC;var type=$this.data("sort")||null;if(type===null){return}$table.trigger("beforetablesort",{column:th_index,direction:sort_dir});$table.css("display");setTimeout(function(){var column=[];var sortMethod=sortFns[type];var trs=$table.children("tbody").children("tr");trs.each(function(index,tr){var $e=$(tr).children().eq(th_index);var sort_val=$e.data("sort-value");var order_by=typeof(sort_val)!=="undefined"?sort_val:$e.text();column.push([order_by,tr])});column.sort(function(a,b){return sortMethod(a[0],b[0])});if(sort_dir!=dir.ASC)column.reverse();trs=$.map(column,function(kv){return kv[1]});$table.children("tbody").append(trs);$table.find("th").data("sort-dir",null).removeClass("sorting-desc sorting-asc");$this.data("sort-dir",sort_dir).addClass("sorting-"+sort_dir);$table.trigger("aftertablesort",{column:th_index,direction:sort_dir});$table.css("display")},10)})})};$.fn.stupidtable.dir={ASC:"asc",DESC:"desc"};$.fn.stupidtable.default_sort_fns={"int":function(a,b){return parseInt(a,10)-parseInt(b,10)},"float":function(a,b){return parseFloat(a)-parseFloat(b)},"string":function(a,b){return a.localeCompare(b)},"string-ins":function(a,b){a=a.toLocaleLowerCase();b=b.toLocaleLowerCase();return a.localeCompare(b)}}})(jQuery);$(function(){var table=$("table").stupidtable();table.on("aftertablesort",function(event,data){var th=$(this).find("th");th.find(".arrow").remove();var dir=$.fn.stupidtable.dir;var arrow=data.direction===dir.ASC?"&uArr;":"&dArr;";th.eq(data.column).append('<span class="arrow">'+arrow+'</span>')})});	
/*表格排序 结束*/
function batchOperate(batchOperateType,idArr){
	var operateTitle="";
	var url="";
	if(batchOperateType=="delete"){
		operateTitle="批量删除";
		url="${ctx}/user/delete";
	}else if(batchOperateType=="disable"){
		operateTitle="批量禁用";
		url="${ctx}/user/disable";
	}else if(batchOperateType=="enable"){
		operateTitle="批量启用";
		url="${ctx}/user/enable";
	}
	ajaxSubmit(url, {ids:idArr.join(",")}, reload, operateTitle+"成功", "确认"+operateTitle+"？");
}



function editClientInteface(id,username){
	parent.addTab("编辑接口("+username+")","${ctx}/interfaces/edit/"+id);
}

$("#addOperateBtn").click(function(){
	parent.addTab("新增接口","${ctx}/interfaces/add");

});


$(function(){
	$("#moreDiv").loadingMore({
		dataDivId:'moreUserListDiv',
		successCallback:function(){
			bindEvents();
		}
	});	
	$("#batchOperateBtn").click(function(){
		var batchOperateType=$("#batchOperateType").val();
		var check_name = document.getElementsByName("list-checkbox");
		var idArr=new Array();
		for(var i=0;i<check_name.length;i++){
			if(check_name[i].checked){
				idArr.push(check_name[i].value);
			}
		}
		if(batchOperateType!="" && idArr.length>0){
			batchOperate(batchOperateType,idArr);
		}
	});
});

//初始化接口
function initInteface(){
	location.href="${ctx}/interfaces/initInteface";
}
</script>
</body>
</html>
