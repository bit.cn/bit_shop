<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>接口搜索</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>
<body>
<div class="main user-search">
	<div class="main-top">    
            <h3>接口搜索</h3>              
    </div><!--main-top-->

<form id="searchForm" action="${ctx}/user/list" method="post">
<!--接口搜索-->
<div class="panel panel-info">
  <div class="panel-heading">请先根据条件搜索接口，然后选择相应的操作。</div>
  <table class="table">
  		<tbody>
                <tr>
                    <th width="15%" class="active">接口名 </th>
                    <td width="85%"><input type="text" class="form-control" name="user_names" value="${user_names}"/><span class="text-muted">多个接口名用半角<code>,</code>隔开</span></td>
                </tr>
                <tr>
                    <th class="active">接口ID</th>
                    <td><input type="text" class="form-control" name="user_ids" value="${user_ids}"/><span class="text-muted">多个接口ID用半角<code>,</code>隔开</span></td>
                </tr>
                <tr>
                    <th class="active">手机</th>
                    <td><input type="text" class="form-control" name="user_phones" value="${user_phones}"/><span class="text-muted">多个手机用半角<code>,</code>隔开</span></td>
                </tr>
                <tr>
                    <th class="active">邮箱</th>
                    <td><input type="text" class="form-control" name="user_emails" value="${user_emails}"/><span class="text-muted">多个邮箱用半角<code>,</code>隔开</span></td>
                </tr>
        </tbody>
  </table>
</div><!--接口搜索 结束-->
<div class="submit">
      <button type="submit" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-search"></span> 搜索</button>
      <button type="button" class="btn btn-primary" onclick="location='${ctx}/user/list'">显示全部</button>
</div>
</form>
</div><!--main end-->
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript" src="${ctx}/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
</script>
</body>
</html>
