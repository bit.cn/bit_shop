<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>用户管理</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
</head>
<body>
	<div class="main user-management">
		<div class="main-top">
			<h3>
				用户管理 <small>共搜索到<span>${apiRsp.count}</span>名符合条件的用户
				</small>
			</h3>
			<form id="searchForm" action="${ctx}/user/list" method="post"
				class="form-inline pull-right search-box" role="form">
				<input type="hidden" id="page_size" name="page_size"
					value="${page_size}"> <a
					href="${ctx}/user/list?page_size=${page_size}"
					class="btn btn-primary">显示全部</a>
			</form>
		</div>
		<!--main-top-->
		<!--用户列表-->
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="pin">
					<form class="form-inline" role="form">
						<div class="form-group">
							<select id="batchOperateType" class="form-control">
								<option value="">批量操作</option>
								<option value="disable">禁用</option>
								<!-- <option>打入小黑屋</option> -->
								<option value="enable">恢复正常</option>
								<option value="delete">删除</option>
							</select>
						</div>
						<div class="btn-group">
							<button id="batchOperateBtn" type="button"
								class="btn btn-default">确认</button>
						</div>
						<div class="btn-group">
							<button id="addOperateBtn" type="button" onclick="addUser()"
								class="btn btn-default">新增</button>
						</div>
					</form>
				</div>
			</div>
			<ol class="breadcrumb">
				<li><a href="${ctx}/user/search?username=${username}">用户搜索</a></li>
				<li>用户管理</li>
			</ol>
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="th-checkbox"><input type="checkbox" id="check-btn"
							class="tag" title="" data-original-title="全选/反选"></th>
						<th>ID</th>
						<th>状态</th>
						<th>用户名</th>
						<th>真实姓名</th>
						<th>昵称</th>
						<th>手机</th>
						<th>邮箱</th>
						<th>操作</th>
					</tr>
				</thead>
				<c:set var="pagingUrl"
					value="${ctx}/user/list?searchValue=${searchValue}&page_size=${page_size}" />
				<c:set var="nextUrl" value="" />
				<c:if
					test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
					<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}" />
				</c:if>
				<tbody id="moreUserListDiv" nextUrl="${nextUrl}">
					<c:forEach items="${apiRsp.results}" var="item">
						<tr>
							<td><input type="checkbox" name="list-checkbox"
								value="${item.id}"></td>
							<td>${item.id}</td>
							<td>
								<c:choose>
									<c:when test="${item.status eq 'activity'}">
										<span class="glyphicon glyphicon-ok-sign text-success"
											title="启用"></span>
									</c:when>
									<c:otherwise>
										<span class="glyphicon glyphicon-remove-sign text-danger"
											title="禁用"></span>
									</c:otherwise>
								</c:choose>
							</td>
							<td>${item.username}</td>
							<td>${item.realName}</td>
							<td>${item.nickName}</td>
							<td>${item.mobilePhone}</td>
							<td>${item.email}</td>
							<td>
									<c:forEach items="${sessionScope.backend_session_user_info.funList}" var="fun">
									 	<c:if test="${fun.menuId==menuId}">
									 	 	<x:button buttonId="addOperateBtn" type="button" onClick="${fun.funCode}('${item.id}','${item.username}')" name="${fun.funName}" style="" funcationCode="${fun.funId}" menuId="${fun.menuId}"></x:button>
									  	</c:if>
									</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</div>
		<!--用户列表结束-->
		<c:if test="${not empty nextUrl}">
			<div class="load_more">
				<button id="loadingDiv" style="display: none" type="button"
					class="btn btn-default btn-block loading-btn">Loading...</button>
				<button id="moreDiv" style="display:" type="button"
					class="btn btn-default btn-block loading-btn"
					data-loading-text="Loading..." autocomplete="off">加载更多</button>
			</div>
		</c:if>
		<div class="main-bottom">
			<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
				<jsp:param name="paginationObjectName" value="apiRsp" />
				<jsp:param name="pageNoName" value="" />
				<jsp:param name="requestUrl" value="${pagingUrl}" />
				<jsp:param name="refreshDiv" value="" />
			</jsp:include>
		</div>
		<!--main-bottom-->
	</div>
	<!--main end-->

	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script type="text/javascript">
function batchOperate(batchOperateType,idArr){
	var operateTitle="";
	var url="";
	if(batchOperateType=="delete"){
		operateTitle="批量删除";
		url="${ctx}/user/delete";
	}else if(batchOperateType=="disable"){
		operateTitle="批量禁用";
		url="${ctx}/user/disable";
	}else if(batchOperateType=="enable"){
		operateTitle="批量启用";
		url="${ctx}/user/enable";
	}
	ajaxSubmit(url, {ids:idArr.join(",")}, reload, operateTitle+"成功", "确认"+operateTitle+"？");
}
function resetPassword(id){
	var successCallback=function(event,param){
		var apiRsp=getApiJson(param.data);
		alert("重置成功，新密码为："+apiRsp.results);
	};
	ajaxSubmit("${ctx}/user/resetPassword?id="+id, {}, successCallback, null, "确认重置密码？");
}

function bindEvents(){
	$(".pin").pin();
	/*全选 取消全选*/	
	var check_btn = document.getElementById("check-btn");
	var check_name = document.getElementsByName("list-checkbox");
	check_btn.onclick = function(){
		for(var i=1; i<=check_name.length; i+=1){
			if(check_name[i-1].checked){
				check_name[i-1].checked = false;
			}else{
				check_name[i-1].checked = true;
			}
		}
	};
}
$(function(){
	$("#moreDiv").loadingMore({
		dataDivId:'moreUserListDiv',
		successCallback:function(){
			bindEvents();
		}
	});	
	$("#batchOperateBtn").click(function(){
		var batchOperateType=$("#batchOperateType").val();
		var check_name = document.getElementsByName("list-checkbox");
		var idArr=new Array();
		for(var i=0;i<check_name.length;i++){
			if(check_name[i].checked){
				idArr.push(check_name[i].value);
			}
		}
		if(batchOperateType!="" && idArr.length>0){
			batchOperate(batchOperateType,idArr);
		}
	});
});

//新增用户
function addUser(){
	location.href="${ctx}/user/add";
}

//编辑用户
function edit(id,username){
	parent.addTab("编辑用户("+username+")","${ctx}/user/edit/"+id);
}
</script>
</body>
</html>
