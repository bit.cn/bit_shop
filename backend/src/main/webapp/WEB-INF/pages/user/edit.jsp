<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>编辑用户</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
<link rel="stylesheet" href="${ctx}/css/bootstrap-multiselect.css"
	type="text/css">
<link rel="stylesheet" href="${ctx}/jstree/default/style.min.css">
</head>
<body>
	<div class="main user-editor">
		<div class="main-top">
			<h3>编辑用户</h3>
		</div>
		<!--main-top-->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation"><a onclick="javascript:returnToList();"
				href="javascript:;"><span class="glyphicon glyphicon-arrow-left"></span>
					返回列表</a></li>
			<li role="presentation" class="active"><a href="#">${user.username}</a></li>
		</ul>
		<form id="editForm" role="form" action="${ctx}/user/save"
			method="post">
			<div class="panel-group" id="accordion" role="tablist"
				aria-multiselectable="true">

				<!--基本信息-->
				<div class="panel panel-info">

					<div class="panel-heading" data-toggle="collapse"
						data-parent="#accordion" href="#collapseAPK" aria-expanded="true"
						aria-controls="collapseThree">
						<h4 class="panel-title">基本信息</h4>
					</div>
					<div id="collapseAPK" class="panel-collapse collapse in"
						role="tabpanel">
						<table class="table">
							<tbody>
								<tr>
									<th class="active" width="15%">用户名</th>
									<input type="hidden" name="id" class="form-control"
										placeholder="用户名" required autofocus value="${user.id}">
									<td width="85%"><input type="text" name="username"
										class="form-control" value="${user.username}" /> <input
										type="hidden" id="checkedOrgId" name="checkedOrgId"
										class="form-control" value="${user.checkedOrgId}"></td>
								</tr>
								<tr>
									<th class="active">电话号码</th>
									<td><input type="text" class="form-control"
										name="mobilePhone" value="${user.mobilePhone}" /></td>
								</tr>
								<tr>
									<th class="active">状态</th>
									<td><label class="radio-inline"> <input
											type="radio" name="inlineRadioOptions" id="inlineRadio1"
											value="activity"
											<c:if test="${user.status eq 'activity'}">checked="checked"</c:if>>
											<span class="glyphicon glyphicon-ok-sign text-success"
											title="正常"></span> 正常
									</label> <label class="radio-inline"> <input type="radio"
											name="inlineRadioOptions" id="inlineRadio2" value="freeze"
											<c:if test="${user.status eq 'freeze'}">checked="checked"</c:if>>
											<span class="glyphicon glyphicon-ban-circle text-muted"
											title="禁用"></span> 禁用
									</label></td>
								</tr>

								<tr>
									<th class="active">真是姓名</th>
									<td><input type="text" class="form-control"
										name="realName" value="${user.realName}" /></td>
								</tr>
								<tr>
									<th class="active">昵称</th>
									<td><input type="text" class="form-control"
										name="nickName" value="${user.nickName}" /></td>
								</tr>
								<tr>
									<th class="active">邮箱</th>
									<td><input type="text" class="form-control" name="email"
										value="${user.email}" /></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!--基本信息 结束-->
				<!--权限设置-->
				<div class="panel panel-info">
					<div class="panel-heading" data-toggle="collapse"
						data-parent="#accordion" href="#collapseTwo" aria-expanded="true"
						aria-controls="collapseTwo">
						<h4 class="panel-title">权限设置</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse in"
						role="tabpanel">
						<table class="table">
							<tbody>
								<tr>
									<th class="active">机构：</th>
									<td>
										<div class="tree" id="tree"></div> <input type="hidden"
										id="ids" name="ids" class="form-control" value="">
									</td>
								</tr>
								<tr>
									<th class="active">角色</th>
									<td><select id="example-dropUp" multiple="multiple"
										name="roleIds">
											<c:forEach items="${rolesList}" var="roles">
												<option value="${roles.id}"
													<c:forEach items="${user.roleList}" var="role">
												<c:choose>
												<c:when test="${role.roleId==roles.id}">
													selected="selected"
												</c:when>
												</c:choose>
											</c:forEach>>${roles.name}</option>
											</c:forEach>
									</select></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="submit">
				<c:choose>
					<c:when test="${empty user}">
						<button type="button" id="formButton"
							onclick="javascript:saveUser();"
							class="btn btn-success loading-btn"
							data-loading-text="Loading..." autocomplete="off">
							<span class="glyphicon glyphicon-ok"></span> 确认提交
						</button>
					</c:when>
					<c:otherwise>
						<button type="button" id="formButton"
							onclick="javascript:updateUser();"
							class="btn btn-success loading-btn"
							data-loading-text="Loading..." autocomplete="off">
							<span class="glyphicon glyphicon-ok"></span> 确认提交
						</button>
					</c:otherwise>
				</c:choose>
				<button type="button" id="formButton"
					class="btn btn-success loading-btn" data-loading-text="Loading..."
					autocomplete="off" onclick="javascript:returnToList();">
					<span class="glyphicon glyphicon-ok"></span>返回列表
				</button>
			</div>
		</form>
	</div>
	<!--main end-->
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
	<script src="${ctx}/jstree/jstree.js" type="text/javascript"></script>
	<script src="${ctx}/js/x_jstree.js" type="text/javascript"></script>
	<script type="text/javascript" src="${ctx}/js/bootstrap-multiselect.js"
		type="text/javascript"></script>
	<script type="text/javascript">
		function returnToList() {
			parent.closeThenAddTab("用户管理", "${ctx}/user/list");
		}
		$(function() {
			var checkedOrgId = $("#checkedOrgId").val();
			var checkId = checkedOrgId.split(",");
			bindJsTree("tree", "${ctx}/org/getTopOrgList", true, checkId,
					"${ctx}/org/loadOrg");

			$('#example-dropUp').multiselect({
				enableFiltering : true,
				includeSelectAllOption : true,
				maxHeight : 400,
				dropUp : true,
				nonSelectedText : '--请选择功能--',
				selectAllText : '全选'
			});

			//得到后台传过来选中的树节点数组
			function showCheckboxTreeDefault() {
				var checkedOrgId = $("#checkedOrgId").val();
				var checkId = checkedOrgId.split(";");
				alert(checkId);
				$("#tree").find("li").each(function() {
					if (checkId == 'all') {
						$("#tree").jstree("check_node", jQuery(this));
					} else if (checkId instanceof Array) {
						for ( var i = 0; i < checkId.length; i++) {
							if (jQuery(this).attr("id") == checkId[i]) {
								$("#tree").jstree("check_node", jQuery(this));
							}
						}
					}
				});
			}
		});
		//保存用户信息	
		function saveUser() {
			$.ajax({
				cache : true,
				type : "POST",
				url : $("#editForm").attr("action"),
				data : $("#editForm").serialize(),// 你的formid
				async : false,
				error : function(request) {
					alert("返回异常");
				},
				success : function(data) {
					alert('新增成功!');
					parent.location.reload();
					parent.close();
				}
			});
		}

		//更新用户信息
		function updateUser() {
			$("#editForm").attr("action", "${ctx}/user/update");
			$.ajax({
				cache : true,
				type : "POST",
				url : $("#editForm").attr("action"),
				data : $("#editForm").serialize(),// 你的formid
				async : false,
				error : function(request) {
					alert("返回异常");
				},
				success : function(data) {
					alert('新增成功!');
					parent.location.reload();
					parent.close();
				}
			});
		}
	</script>
</body>
</html>