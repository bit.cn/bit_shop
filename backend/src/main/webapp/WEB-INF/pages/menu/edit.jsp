<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>编辑菜单信息</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
<link rel="stylesheet" href="${ctx}/css/bootstrap-multiselect.css"
	type="text/css">
</head>
<body>
	<div class="main user-editor">
		<div class="panel-group" id="accordion" role="tablist"
			aria-multiselectable="true">
			<!--基本信息-->
			<div class="panel panel-info">
				<div class="panel-heading" data-toggle="collapse"
					data-parent="#accordion" href="#collapseAPK" aria-expanded="true"
					aria-controls="collapseThree">
					<h4 class="panel-title">菜单信息</h4>
				</div>
				<div id="collapseAPK" class="panel-collapse collapse in"
					role="tabpanel">
					<form name="editForm" id="editForm" method="post"
						action="${ctx}/menu/save">
						<c:choose>
							<c:when test="${empty permissionMenus}">
								<c:choose>
									<c:when test="${empty parentId}">
										<input type="hidden" class="form-control" name="parentId"
											value="0" />
									</c:when>
									<c:otherwise>
										<input type="hidden" class="form-control" name="parentId"
											value="${parentId}" />
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<input type="hidden" class="form-control" name="parentId"
											value="${permissionMenus.parentId}" />
							</c:otherwise>
						</c:choose>
						<c:if test="${not empty permissionMenus}">
							<input type="hidden" class="form-control" name="id"
								value="${permissionMenus.id}" />
						</c:if>
						<table class="table">
							<tbody>
								<tr>
									<th class="active">菜单名称</th>
									<td><input type="text" class="form-control" name="name"
										value="${permissionMenus.name}" /></td>
								</tr>
								<tr>
									<th class="active">菜单编码</th>
									<td><input type="text" class="form-control" name="code"
										value="${permissionMenus.code}" /></td>
								</tr>
								<tr>
									<th class="active">菜单URL</th>
									<td><input type="text" class="form-control" name="menuUrl"
										value="${permissionMenus.menuUrl}" /></td>
								</tr>
								<tr>
									<th class="active">包含功能</th>
									<td><select id="example-dropUp" multiple="multiple"
										name="funIds">
											<c:forEach items="${permissionFunctionList}"
												var="permissionFunction">
												<option value="${permissionFunction.id}"
													<c:forEach items="${permissionMenus.functionRelateMenuDtoList}" var="function">
												<c:choose>
												<c:when test="${function.funId==permissionFunction.id}">
													selected="selected"
												</c:when>
												</c:choose>
											</c:forEach>>${permissionFunction.name}</option>
											</c:forEach>
									</select></td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
		<div class="submit">
			<c:choose>
				<c:when test="${empty permissionMenus}">
					<button type="button" id="formButton"
						onclick="javascript:saveMenu();"
						class="btn btn-success loading-btn" data-loading-text="Loading..."
						autocomplete="off">
						<span class="glyphicon glyphicon-ok"></span> 确认提交
					</button>
				</c:when>
				<c:otherwise>
					<button type="button" id="formButton"
						onclick="javascript:updateMenu();"
						class="btn btn-success loading-btn" data-loading-text="Loading..."
						autocomplete="off">
						<span class="glyphicon glyphicon-ok"></span> 确认提交
					</button>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<!--main end-->
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script type="text/javascript" src="${ctx}/js/bootstrap-multiselect.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example-dropUp').multiselect({
				enableFiltering : true,
				includeSelectAllOption : true,
				maxHeight : 200,
				dropUp : true,
				nonSelectedText : '--请选择功能--',
				selectAllText : '全选'
			});
		});

		//保存菜单信息
		function saveMenu() {
			$("#editForm").ajaxForm({
				success : function(event, param) {
					var apiRsp = getApiJson(param.data);
					if (apiRsp) {
						if (apiRsp.isSuccess) {
							alert('修改成功!');
							parent.location.reload();
							parent.close();
						} else {
							alert(apiRsp.msg);
						}
					} else {
						alert("返回异常");
					}
				}
			});
		}
		
		//更新菜单信息
		function updateMenu() {
			$("#editForm").attr("action", "${ctx}/menu/update");
			$("#editForm").ajaxForm({
				success : function(event, param) {
					var apiRsp = getApiJson(param.data);
					if (apiRsp) {
						if (apiRsp.isSuccess) {
							alert('修改成功!');
							parent.location.reload();
							parent.close();
						} else {
							alert(apiRsp.msg);
						}
					} else {
						alert("返回异常");
					}
				}
			});
		}
	</script>
</body>
</html>