<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>查看用户</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>
<body>

<div class="main user-editor">

	<div class="main-top">    
            <h3>查看用户</h3> 
    </div><!--main-top-->
    
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation"><a onclick="javascript:returnToList();" href="javascript:;"><span class="glyphicon glyphicon-arrow-left"></span> 返回列表</a></li>
      <li role="presentation" class="active"><a href="#">${user.username}</a></li>
    </ul>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<!--基本信息-->  
  <div class="panel panel-info">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseAPK" aria-expanded="true" aria-controls="collapseThree">
      <h4 class="panel-title">基本信息</h4>
    </div>
    <div id="collapseAPK" class="panel-collapse collapse in" role="tabpanel">
      <table class="table">
        <tbody>
          <tr>
            <th class="active" width="15%">用户名</th>
            <td width="85%"><input type="text" class="form-control" value="${user.username}" disabled/></td>
          </tr>
          <tr>
          	<th class="active">状态</th>
            <td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" disabled="disabled" value="activity" <c:if test="${user.status eq 'activity'}">checked="checked"</c:if>> <span class="glyphicon glyphicon-ok-sign text-success" title="正常"></span> 正常
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions" id="inlineRadio2" disabled="disabled" value="freeze" <c:if test="${user.status eq 'freeze'}">checked="checked"</c:if>> <span class="glyphicon glyphicon-ban-circle text-muted" title="禁用"></span> 禁用
				</label>
            </td>
          </tr>
          <tr>
          	<th class="active">真是姓名</th>
            <td>
				<input type="text" class="form-control" value="${user.email}" disabled="disabled"/>
            </td>
          </tr>
          <tr>
          	<th class="active">昵称</th>
            <td><input type="text" class="form-control" value="${user.email}" disabled="disabled"/></td>
          </tr>
          <tr>
          	<th class="active">电话号码</th>
            <td><input type="text" class="form-control" value="${user.email}" disabled="disabled"/></td>
          </tr>
          <tr>
          	<th class="active">邮箱</th>
            <td><input type="text" class="form-control" value="${user.email}" disabled="disabled"/></td>
          </tr>
        </tbody>
      </table>
      
      
    </div>

  </div>



<!--其他信息-->  
  <div class="panel panel-info">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseTwo">
      <h4 class="panel-title">其他信息</h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel">
      <table class="table">
        <tbody>
          <tr>
            <th class="active" width="15%">注册日期</th><fmt:formatDate value="${user.signup_date}" pattern="yyyy-MM-dd HH:mm:ss" var="signup_date"/>
            <td width="85%"><input type="text" class="form-control" value="${signup_date}" disabled="disabled"/><!-- <span class="text-muted">请勿随意修改注册日期</span> --></td>                        
          </tr>
          <tr>
            <th class="active">注册IP</th>
            <td><input type="text" class="form-control" value="${user.signup_ip}" disabled="disabled"/></td>
          </tr>
          <tr>
            <th class="active">最后登录</th><fmt:formatDate value="${user.last_login}" pattern="yyyy-MM-dd HH:mm:ss" var="last_login"/>
            <td><input type="text" class="form-control" value="${last_login}" disabled="disabled"/><!-- <span class="text-muted">请勿随意修改登录日期</span> --></td>                        
          </tr>
          <tr>
            <th class="active">登录IP</th>
            <td><input type="text" class="form-control" value="${user.last_login_ip}" disabled="disabled"/></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
<!--其他信息 结束--> 
</div>
<div class="submit">
      <button type="button" class="btn btn-default" onclick="javascript:returnToList();">返回列表</button>
</div>

</div><!--main end-->
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
</script>
</body>
</html>