<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>后台用户控制中心</title>
<%@ include file="/WEB-INF/pages/common/css.jsp" %>
</head>
<body class="easyui-layout">
    <!--head 头部  -->
    <%@ include file="/WEB-INF/pages/common/head.jsp" %>
    <!--左侧菜单  -->
    <%@ include file="/WEB-INF/pages/common/leftMenu.jsp" %>
    <div id="mainPanle" region="center" class="right">
        <div id="tabs" class="easyui-tabs man-tab" fit="true" border="false">
            <div title="首页" id="home">
				<!--菜单快捷键  -->
				<div class="quick-menu">
				</div>
				 <div id="mainPanle_frame">
				 	请稍等，正在加载数据...
				</div>
            </div>
        </div>
    </div><!--mainPanle-->
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>
<script type="text/javascript">
function createCookie(name,value,days){if(days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString()}else var expires="";document.cookie=name+"="+value+expires+"; path=/"}function readCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(nameEQ)==0)return c.substring(nameEQ.length,c.length)}return null}function eraseCookie(name){createCookie(name,"",-1)}
function switchStylestyle(styleName){
	$('link[rel*=style][title]').each(function(i) 
	{
		this.disabled = true;
		if (this.getAttribute('title') == styleName) this.disabled = false;
	});
	createCookie('style', styleName, 365);
}
function addTab(title,href,update){
	var options = {
		title : title, 
		content: createFrame(href),
		closable : true,
		cache : false,
		closed: true,  //如果不加这个条件，当选中已经存在的tabs时不同于上一次下载的url，会报错
		width: $('#mainPanle').width() - 10,
		height: $('#mainPanle').height() - 26
	};
	
	var tt = $('#tabs');  
    if (tt.tabs('exists', title)){//如果tab已经存在,则选中并刷新该tab          
        tt.tabs('select', title);  
	    if(update){
	    	refreshTab({tabTitle:title,url:href});  //是否重复刷新
	    }
    }else{  
    	 tt.tabs('add',options);  
    	 //setTimeout(window.open(href,"mainFrame"),10000);
    }  
	
}

 function refreshTab(cfg) {
	  var refresh_tab = cfg.tabTitle ? $('#tabs').tabs('getTab',cfg.tabTitle) : $('#tabs').tabs('getSelected');  
	  if(refresh_tab && refresh_tab.find('iframe').length > 0){  
		    var _refresh_ifram = refresh_tab.find('iframe')[0];  
		    var refresh_url = cfg.url?cfg.url:_refresh_ifram.src;  
		    _refresh_ifram.contentWindow.location.href=refresh_url;  
	  }  
}
 


function closeSelf(){
	var thisTab=$("#tabs").tabs("getSelected");
	var thisTabIndex=$("#tabs").tabs("getTabIndex",thisTab);
	$("#tabs").tabs("close",thisTabIndex);
}
function closeThenAddTab(title,href,update){
	closeSelf();
	addTab(title,href,update);
}
function createFrame(url) {
	return '<iframe name="mainFrame" scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%;"></iframe>';
}

jQuery(function($) {
	
	 $("#mainPanle_frame").html('<iframe class="iframe-main" scrolling="auto" frameborder="0"  src="" style="width:100%;height:100%;"></iframe>');
	 
	$(".iframe-main").load(function(){         
		 $(this).height($(window).height()-100);  
	});
	
	$('.set-skin a').click(function(){
		switchStylestyle(this.getAttribute("rel"));
		return false;
	});
	var c = readCookie('style');
	if (c){
		switchStylestyle(c);	
	}
});
</script>
</body>
</html>



