<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>编辑客户端</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
<link rel="stylesheet" href="${ctx}/css/bootstrap-multiselect.css"
	type="text/css">
<link rel="stylesheet" href="${ctx}/jstree/default/style.min.css">
</head>
<body>

	<div class="main user-editor">

		<div class="main-top">
			<h3>编辑客户端</h3>
		</div>
		<!--main-top-->

		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation"><a onclick="javascript:returnToList();"
				href="javascript:;"><span class="glyphicon glyphicon-arrow-left"></span>
					返回列表</a></li>
			<li role="presentation" class="active"><a href="#">${client.clientName}</a></li>
		</ul>
		<form id="editForm" role="form" action="${ctx}/client/save"
			method="post">
			<div class="panel-group" id="accordion" role="tablist"
				aria-multiselectable="true">

				<!--基本信息-->
				<div class="panel panel-info">

					<div class="panel-heading" data-toggle="collapse"
						data-parent="#accordion" href="#collapseAPK" aria-expanded="true"
						aria-controls="collapseThree">
						<h4 class="panel-title">基本信息</h4>
					</div>
					<div id="collapseAPK" class="panel-collapse collapse in"
						role="tabpanel">
						<table class="table">
							<tbody>
								<tr>
									<th class="active" width="15%">客户端名</th>
									<input type="hidden" name="id" class="form-control"
										value="${client.id}">
									<td width="85%"><input type="text" name="clientName"
										class="form-control" value="${client.clientName}" /> <input
										type="hidden" id="clientId" name="checkedClientId"
										class="form-control" value="${client.id}"></td>
								</tr>
								<tr>
									<th class="active">客户端KEY</th>
									<td><input type="text" class="form-control"
										name="clientKey" value="${client.clientKey}" /></td>
								</tr>
								<tr>
									<th class="active">客户端secret</th>
									<td><input type="text" class="form-control"
										name="clientSecret" value="${client.clientSecret}" /></td>
								</tr>

								<tr>
									<th class="active">描述</th>
									<td><input type="text" class="form-control"
										name="description" value="${client.description}" /></td>
								</tr>

								<tr>
									<th class="active">生效时间</th>
									<td><input type="text" class="form-control Wdate "
										name="effectiveTime"
										onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"
										value="${client.effectiveTime}" /></td>
								</tr>
								<tr>
									<th class="active">过期时间</th>
									<td><input type="text" class="form-control Wdate"
										name="expirationTime"
										onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"
										value="${client.expirationTime}" /></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!--基本信息 结束-->
				<!--权限设置-->
				<div class="panel panel-info">
					<div class="panel-heading" data-toggle="collapse"
						data-parent="#accordion" href="#collapseTwo" aria-expanded="true"
						aria-controls="collapseTwo">
						<h4 class="panel-title">接口关联</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse in"
						role="tabpanel">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<table class="table table-hover">
											<thead>
												<tr>
													<th class="th-checkbox"><input type="checkbox"
														id="check-btn" class="tag" title=""
														data-original-title="全选/反选" onclick="bindCheckAll()"></th>
													<th>接口名称</th>
												</tr>
											</thead>
											<tbody id="moreUserListDiv" nextUrl="${nextUrl}">
												<c:forEach items="${interfaceList}" var="inface">
													<tr>
														<td><input type="checkbox" name="list-checkbox"
															<c:forEach items="${clientIntefaceList}" var="clientInteface">
														<c:if test="${inface.id==clientInteface.interfaceId}">
															checked="checked"
														</c:if>
														</c:forEach>
															value="${inface.id}"></td>
														<td>${inface.name}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</td>
								</tr>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="submit">
				<button type="button" id="formButton"
					class="btn btn-success loading-btn" data-loading-text="Loading..."
					autocomplete="off">
					<span class="glyphicon glyphicon-ok"></span> 确认提交
				</button>
				<button type="button" id="formButton"
					class="btn btn-success loading-btn" data-loading-text="Loading..."
					autocomplete="off" onclick="javascript:returnToList();">
					<span class="glyphicon glyphicon-ok"></span>返回列表
				</button>
			</div>
		</form>
	</div>
	<!--main end-->
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
	<script src="${ctx}/jstree/jstree.js" type="text/javascript"></script>
	<script src="${ctx}/js/x_jstree.js" type="text/javascript"></script>
	<script type="text/javascript" src="${ctx}/js/bootstrap-multiselect.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="${ctx}/js/My97DatePicker/WdatePicker.js"></script>

	<script type="text/javascript">
		function returnToList() {
			parent.closeThenAddTab("客户端管理", "${ctx}/client/list");
		}
		$(function() {

			$('#example-dropUp').multiselect({
				enableFiltering : true,
				includeSelectAllOption : true,
				maxHeight : 400,
				dropUp : true,
				nonSelectedText : '--请选择功能--',
				selectAllText : '全选'
			});

			$("#formButton").click(function() {
				$.ajax({
					cache : true,
					type : "POST",
					url : $("#editForm").attr("action"),
					data : $("#editForm").serialize(),// 你的formid
					async : false,
					error : function(request) {
						alert("返回异常");
					},
					success : function(data) {
						alert('修改成功!');
						parent.location.reload();
						parent.close();
					}
				});
			});

		});
	</script>
</body>
</html>