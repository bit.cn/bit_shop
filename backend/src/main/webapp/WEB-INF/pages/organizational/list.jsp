<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>组织管理</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
<link rel="stylesheet" href="${ctx}/jstree/default/style.min.css">
</head>
<body>
	<div class="main user-management">
		<div class="main-top"></div>
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="pin">
					<form class="form-inline" role="form">组织管理</form>
				</div>
			</div>
			<div class="breadcrumb">
				<button type="button" class="btn btn-default"
					onclick="javascript:addTopMenu('${ctx}/org/addChildOrg');">添加顶级机构</button>
				<button type="button" class="btn btn-default"
					onclick="javascript:addChildMenu('${ctx}/org/addChildOrg');">添加子构</button>
				<button type="button" class="btn btn-default"
					onclick="javascript:deleteMenu('tree','${ctx}/org/deleteOrg');">删除构</button>
				<input id="id" type="hidden" class="form-control" name="id"
					value="0" />
			</div>
			<table class="table table-hover">
				<tbody id="moreUserListDiv">
					<tr>
						<th>
							<div class="tree" id="tree"></div>
						</th>
						<td><iframe src="" id="menuEditPanel"
								style="border: 0; display: block; height: 450px; width: 100%; border-radius: 5px; box-shadow: 0 0 5px #CCCCCC; padding: 10px;"
								frameborder="0"> </iframe></td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script src="${ctx}/js/jquery.min.js"></script>
	<script src="${ctx}/jstree/jstree.min.js"></script>
	<script src="${ctx}/js/x_jstree.js"></script>
	<script type="text/javascript">
		bindJsTree("tree", "${ctx}/org/getTopOrgList", false,
				null,"${ctx}/org/loadOrg");
	</script>
</body>
</html>
