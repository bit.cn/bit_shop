<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>编辑组织信息</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
<link rel="stylesheet" href="${ctx}/css/bootstrap-multiselect.css"
	type="text/css">
</head>
<body>
	<div class="main user-editor">
		<div class="panel-group" id="accordion" role="tablist"
			aria-multiselectable="true">
			<!--基本信息-->
			<div class="panel panel-info">
				<div class="panel-heading" data-toggle="collapse"
					data-parent="#accordion" href="#collapseAPK" aria-expanded="true"
					aria-controls="collapseThree">
					<h4 class="panel-title">组织信息</h4>
				</div>
				<div id="collapseAPK" class="panel-collapse collapse in"
					role="tabpanel">
					<form name="editForm" id="editForm" method="post"
						action="${ctx}/org/saveOrg">
						<c:choose>
							<c:when test="${empty parentId}">
								<input type="hidden" class="form-control" name="parentId"
									value="0" />
							</c:when>
							<c:otherwise>
								<input type="hidden" class="form-control" name="parentId"
									value="${parentId}" />
							</c:otherwise>
						</c:choose>
						<c:if test="${not empty org}">
							<input type="hidden" class="form-control" name="id"
								value="${org.id}" />
						</c:if>
						<table class="table">
							<tbody>
								<tr>
									<th class="active">组织名称</th>
									<td><input type="text" class="form-control" name="name"
										value="${org.name}" /></td>
								</tr>
								<tr>
									<th class="active">组织编码</th>
									<td><input type="text" class="form-control" name="code"
										value="${org.code}" /></td>
								</tr>
								<tr>
									<th class="active">组织描述</th>
									<td><input type="text" class="form-control"
										name="description" value="${org.description}" /></td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
		<div class="submit">
			<c:choose>
				<c:when test="${empty org}">
					<button type="button" id="formButton"
						onclick="javascript:saveOrg();"
						class="btn btn-success loading-btn" data-loading-text="Loading..."
						autocomplete="off">
						<span class="glyphicon glyphicon-ok"></span> 确认提交
					</button>
				</c:when>
				<c:otherwise>
					<button type="button" id="formButton"
						onclick="javascript:updateOrg();"
						class="btn btn-success loading-btn" data-loading-text="Loading..."
						autocomplete="off">
						<span class="glyphicon glyphicon-ok"></span> 确认提交
					</button>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<!--main end-->
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script type="text/javascript" src="${ctx}/js/bootstrap-multiselect.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example-dropUp').multiselect({
				enableFiltering : true,
				includeSelectAllOption : true,
				maxHeight : 400,
				dropUp : true,
				nonSelectedText : '--请选择功能--',
				selectAllText : '全选'
			});
		});

		//保存组织信息
		function saveOrg() {
			$("#editForm").ajaxForm({
				success : function(event, param) {
					var apiRsp = getApiJson(param.data);
					if (apiRsp) {
						if (apiRsp.isSuccess) {
							alert('修改成功!');
							parent.location.reload();
							parent.close();
						} else {
							alert(apiRsp.msg);
						}
					} else {
						alert("返回异常");
					}
				}
			});
		}

		//更新组织信息
		function updateOrg() {
			$("#editForm").attr("action", "${ctx}/org/updateOrg");
			$("#editForm").ajaxForm({
				success : function(event, param) {
					var apiRsp = getApiJson(param.data);
					if (apiRsp) {
						if (apiRsp.isSuccess) {
							alert('修改成功!');
							parent.location.reload();
							parent.close();
						} else {
							alert(apiRsp.msg);
						}
					} else {
						alert("返回异常");
					}
				}
			});
		}
	</script>
</body>
</html>