<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<div region="west" title="导航菜单" class="left">
    	<div class="set-skin" id="set-skin">
            <a class="s1" href="javascript:;" rel="skin1" title="黑"></a>
            <a class="s2" href="javascript:;" rel="skin2" title="灰"></a>
            <a class="s3" href="javascript:;" rel="skin3" title="红"></a>
            <a class="s4" href="javascript:;" rel="skin4" title="紫"></a>
            <a class="s5" href="javascript:;" rel="skin5" title="蓝"></a>
            <a class="s6" href="javascript:;" rel="skin6" title="绿"></a>
        </div>
        <div id="aa" class="easyui-accordion">
		<c:forEach items="${sessionScope.backend_session_user_info.menuList}" var="menu">  
		 <c:if test="${menu.parentId==0}">
		  <div title="${menu.name}">
                <ul class="easyui-tree">
                <c:forEach items="${sessionScope.backend_session_user_info.menuList}" var="childmenu">  
		 		<c:if test="${childmenu.parentId==menu.menuId}">
		 			<li><span><a onclick="javascript:addTab('${childmenu.name}','${ctx}/${childmenu.menuUrl}?menuId=${childmenu.menuId}');" href="javascript:;" title="${childmenu.name}">${childmenu.name}</a></span></li>
		 		</c:if>
		 		</c:forEach>
                </ul>
            </div>
		 </c:if>
		</c:forEach> 
		<!--  
            <div title="用户管理">
                <ul class="easyui-tree">
                          <li><span><a onclick="javascript:addTab('用户管理','${ctx}
                          /user/search');" href="javascript:;" title="用户管理">用户管理</a></span></li>
                          <li><span><a onclick="javascript:addTab('组织管理','${ctx}/org/list');" href="javascript:;" title="组织管理">组织管理</a></span></li>
                          <li><span><a onclick="javascript:addTab('角色管理','${ctx}/roles/search');" href="javascript:;" title="用户管理">角色管理</a></span></li>
                          <li><span><a onclick="javascript:addTab('权限管理','${ctx}/function/search');" href="javascript:;" title="用户管理">权限管理</a></span></li>
                          <li><span><a onclick="javascript:addTab('菜单管理','${ctx}/menu/list');" href="javascript:;" title="菜单管理">菜单管理</a></span></li>
                </ul>
            </div>
            <div title="系统设置">
                <ul class="easyui-tree">
                          <li><span><a onclick="javascript:addTab('客户端管理','${ctx}/client/list');" href="javascript:;" title="客户端管理">客户端管理</a></span></li>
                          <li><span><a onclick="javascript:addTab('接口管理','${ctx}/interfaces/list');" href="javascript:;" title="接口管理">接口管理</a></span></li>
                          
                          <li><span><a onclick="javascript:addTab('后台管理员','${ctx}/system/admin/list');" href="javascript:;" title="后台管理员">后台管理员</a></span></li>
                          <li><span><a onclick="javascript:addTab('数据统计','${ctx}/home');" href="javascript:;" title="数据统计">数据统计</a></span></li>
                          <li><span><a onclick="javascript:addTab('nginx cache','${ctx}/system/cache/nginxKeyList');" href="javascript:;" title="nginx cache">nginx cache</a></span></li>
                		  <li><span><a onclick="javascript:addTab('配置项管理','${ctx}/system/setting/list?page_size=20');" href="javascript:;" title="配置项管理">配置项管理</a></span></li>
                </ul>
            </div>
            -->
        </div>
    </div>