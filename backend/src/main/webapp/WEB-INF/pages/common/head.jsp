<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<div region="north">
	<span>后台用户控制中心</span>
	<div class="user">
		你好，<b>${(not empty
			(sessionScope.backend_session_user_info.realName))?(sessionScope.backend_session_user_info.realName):(sessionScope.backend_session_user_info.username)}</b><span>|</span><a
			onclick="javascript:addTab('修改密码','${ctx}/system/admin/editPassword');"
			href="javascript:;" title="修改密码">修改密码</a><span>|</span><a
			href="${ctx}/logout" target="_top">退出</a>
	</div>
</div>