<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>代码生成器</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
<link rel="stylesheet" href="${ctx}/css/bootstrap-multiselect.css"
	type="text/css">
<link rel="stylesheet" href="${ctx}/jstree/default/style.min.css">
<style type="text/css">
</style>
</head>
<body>
	<div class="main user-editor">
		<div class="main-top">
			<h3>代码生成器</h3>
		</div>
		<!--main-top-->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation"><a onclick="javascript:returnToList();"
				href="javascript:;"><span class="glyphicon glyphicon-arrow-left"></span>
					返回列表</a></li>
			<li role="presentation"><a onclick="javascript:returnToList();"
				href="javascript:;"><span class="glyphicon"></span> 数据库连接</a></li>
			<li role="presentation"><a onclick="javascript:returnToList();"
				href="javascript:;"><span class="glyphicon"></span> 代码生成配置</a></li>
			<li role="presentation" class="active"><a href="#">${user.username}</a></li>
		</ul>
		<form id="editForm" role="form" action="${ctx}/codeGenerator/generateSource" method="post">
			<input id="id" type="hidden" class="form-control" name="id" value="0" />
			<div class="panel-group" id="accordion" role="tablist"
				aria-multiselectable="true">
				<!--基本信息-->
				<div class="panel panel-info">

					<div class="panel-heading" data-toggle="collapse"
						data-parent="#accordion" href="#collapseAPK" aria-expanded="true"
						aria-controls="collapseThree">
						<h4 class="panel-title">基本信息</h4>
					</div>
					<div class="container">
						<div class="col-md-3">
							<div class="tree" id="tree"></div>
						</div>
						<div class="col-md-9">
							<iframe src="" id="menuEditPanel"
								style="border: 0; display: block; height: 450px; width: 100%; border-radius: 5px; box-shadow: 0 0 5px #CCCCCC; padding: 10px;"
								frameborder="0"></iframe>
						</div>
					</div>
				</div>
		</form>
	</div>
	<!--main end-->
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script src="${ctx}/js/jquery.min.js"></script>
	<script src="${ctx}/jstree/jstree.min.js"></script>
	<script src="${ctx}/js/x_jstree.js"></script>
	<script type="text/javascript">
	    //得到数据源树形结构数据
		bindJsTree("tree", "${ctx}/codeGenerator/getDatabaseList", false,
				"${ctx}/codeGenerator/getDatabaseList", "${ctx}/codeGenerator/loadDatabase");
	</script>
</body>
</html>