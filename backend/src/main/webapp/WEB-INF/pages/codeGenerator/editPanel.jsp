<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>查看用户</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
</head>
<body>
<form id="editForm" role="form" action="${ctx}/codeGenerator/generateSource" method="post">
	<div id="collapseAPK" class="panel-collapse collapse in"
		role="tabpanel">
		<table class="table">
			<tbody>
				<tr>
					<th class="active text-right" width="30%">数据库类型</th>
					<td><input type="text" name="dbType" class="input-sm"
						placeholder="MySQL" required autofocus value="${databaseConfig.dbType}" /></td>
					<th class="active text-right">表名称</th>
					<td><input type="text" name="tableName" class="input-sm"
						placeholder="product" required autofocus
						value="${generatorConfig.tableName}" /></td>
				</tr>
				<tr>
					<th class="active  text-right">主键</th>
					<td><input type="text" class="input-sm" name="generateKeys"
						placeholder="id" required autofocus value="${generatorConfig.generateKeys}" /></td>
					<th class="active text-right">项目所在文件夹路径</th>
					<td><input type="text" class="input-sm" name="projectFolder"
						placeholder="D:\myBatisGenerator" required autofocus
						value="${generatorConfig.projectFolder}" /></td>
				</tr>
				<tr>
					<th class="active text-right">实体类Model名称</th>
					<td><input type="text" name="domainObjectName"
						class="input-sm" placeholder="Product" required autofocus
						value="${generatorConfig.domainObjectName}" /></td>
					<th class="active text-right">Dao类名称</th>
					<td><input type="text" class="input-sm" name="mapperName"
						placeholder="ProductMapper" required autofocus
						value="${generatorConfig.mapperName}" /></td>
				</tr>
				<tr>
					<th class="active text-right">Model实体类的包名</th>
					<td><input type="text" class="input-sm" name="modelPackage"
						placeholder="com.bit.product.model" required autofocus
						value="${generatorConfig.modelPackage}" /></td>
					<th class="active text-right">目标文件夹</th>
					<td><input type="text" class="input-sm"
						name="modelPackageTargetFolder" placeholder="src/main/java"
						required autofocus value="${generatorConfig.modelPackageTargetFolder}" /></td>
				</tr>
				<tr>
					<th class="active text-right">Dao包名</th>
					<td><input type="text" class="input-sm" name="daoPackage"
						placeholder="com.bit.product.dao" required autofocus
						value="${generatorConfig.daoPackage}" /></td>
					<th class="active text-right">目标文件夹</th>
					<td><input type="text" class="input-sm" name="daoTargetFolder"
						placeholder="src/main/java" required autofocus
						value="${generatorConfig.daoTargetFolder}" /></td>
				</tr>
				<tr>
					<th class="active text-right">Mapping XML包名</th>
					<td><input type="text" class="input-sm"
						name="mappingXMLPackage" placeholder="com.bit.product.sqlmap"
						required autofocus value="${generatorConfig.mappingXMLPackage}" /></td>
					<th class="active text-right">目标文件夹</th>
					<td><input type="text" class="input-sm"
						name="mappingXMLTargetFolder" placeholder="src/main/java" required
						autofocus value="${generatorConfig.mappingXMLTargetFolder}" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!--基本信息 结束-->
	<div class="submit">
		<c:choose>
			<c:when test="${empty user}">
				<button type="button" id="formButton"
					onclick="javascript:generateSource();"
					class="btn btn-success loading-btn" data-loading-text="Loading..."
					autocomplete="off">
					<span class="glyphicon glyphicon-ok"></span> 生成源码
				</button>
			</c:when>
			<c:otherwise>
				<button type="button" id="formButton"
					onclick="javascript:updateUser();"
					class="btn btn-success loading-btn" data-loading-text="Loading..."
					autocomplete="off">
					<span class="glyphicon glyphicon-ok"></span> 保存配置
				</button>
			</c:otherwise>
		</c:choose>
		<button type="button" id="formButton"
			class="btn btn-success loading-btn" data-loading-text="Loading..."
			autocomplete="off" onclick="javascript:returnToList();">
			<span class="glyphicon glyphicon-ok"></span>返回列表
		</button>
	</div>
	</form>
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script type="text/javascript">
		//保存生成代码的配置信息
		function generateSource(){
			$.ajax({
					cache : true,
					type : "POST",
					url : $("#editForm").attr("action"),
					data : $("#editForm").serialize(),// 你的formid
					async : false,
					error : function(request) {
						alert("返回异常");
					},
					success : function(data) {
						alert('保存成功!');
					}
				});
		}
		
	</script>
</body>
</html>