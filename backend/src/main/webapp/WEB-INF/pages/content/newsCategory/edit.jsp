<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>编辑资讯分类</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>
<div class="container">
	<form id="editForm" role="form" action="${ctx}/content/newsCategory/save" method="post">
		<input type="hidden" name="id" value="${newscategory.id}">
		<input type="hidden" name="parentId" value="${parentId}">
		<div class="form-group has-feedback">
			<label class="control-label" for="category_Name"><strong class="necessary">*</strong>分类名称</label>
			<input type="text" class="form-control" id="categoryName" name="categoryName" value="${newscategory.categoryName}" placeholder="请输入分类名称" required="required">
		</div>
		<div class="form-group has-feedback">
			<label class="control-label" for="category_Slug">分类别名</label>
			<c:choose>
				<c:when test="${not empty newscategory}">
					<input type="text" class="form-control" id="categorySlug" name="categorySlug" value="${newscategory.categorySlug}" readonly="readonly" placeholder="请输入分类别名" >
				</c:when>
				<c:otherwise><input type="text" class="form-control" id="categorySlug" name="categorySlug" value="${newscategory.categorySlug}" placeholder="请输入分类别名" ></c:otherwise>
			</c:choose>

			
			
		</div>
		<div class="form-group has-feedback">
			<label class="control-label" for="category_Summary">分类摘要</label>
			<input type="text" class="form-control" id="categorySummary" name="categorySummary" value="${newscategory.categorySummary}" placeholder="请输入分类摘要" >
		</div>
		<div class="form-group has-feedback">
			<label class="control-label" for="ordering">排序编号</label>
		    <c:if test="${newscategory.id!=null}"><input type="number" class="form-control" name="ordering" value="${newscategory.ordering}"  required="required"></c:if>
            <c:if test="${newscategory.id==null}"><input type="number" class="form-control" name="ordering" value="0"  required="required"></c:if>
		</div>
		<div class="modal-footer">
	  		<button type="button" class="btn btn-default" data-dismiss="modal" id="diglog_close_btn-js" onclick="javascript:closeDialog();">关闭</button>
	  		<button type="submit" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-ok"></span> 确认提交</button>		
		</div>
	</form>
</div>
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
$(function(){
	$("#editForm").bind('submit', function(event) {
		ajaxFormSubmit(this,reloadParent,null,null);
		event.preventDefault();
	});	
});
</script>
</body>
</html>
