<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>资讯分类</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>
	

<div class="main game-class">

	<div class="main-top">    
            <h3> <small>所有资讯分类 共<span>${apiRsp.count}</span>个</small></h3>  
            <form id="categorySearchForm" class="form-inline pull-right search-box" role="form" action="${ctx}/content/newsCategory/list">
            	<input type="hidden" id="parentId" name="parentId" value="${parentId}">
                <input id="categoryName" type="text" class="form-control" placeholder="名称" name="categoryName" value="${categoryName}">
                <button id="searchBtn" type="button" class="btn btn-default">搜索</button>
                <button id="searchAllBtn" type="button" class="btn btn-primary">显示全部</button>
            </form>
    </div><!--main-top-->




	<div class="panel panel-info">
	  <div class="panel-heading">
	  		<div class="pin">
	          <form class="form-inline" role="form">
	            <div class="form-group">
	                  <select class="form-control" id="batchOperateType">
	                            <option value="">批量操作</option>
	                            <!-- <option value="updateOrder">排序</option> -->
	                            <option value="delete">删除</option>
	                  </select>
	            </div>
	            <div class="btn-group">
	            	<button id="batchOperateBtn" type="button" class="btn btn-default">确认</button>
	            </div>
	          </form>
	          
	          <button id="addCategoryBtn" type="button" class="btn btn-primary" data-toggle="modal" data-target=".add-box"><span class="glyphicon glyphicon-plus"></span>添加分类</button>
	        </div>
	  </div>
	    <ol class="breadcrumb">
	      <c:if test="${not empty firstCategoryId}">
	      <li><a href="javascript:doCategorySearch('${firstCategoryId}','${firstCategoryId}','${firstCategoryName}','','','','');">${firstCategoryName}</a></li>
	      </c:if>
	      <c:if test="${not empty secondCategoryId}">
	      <li><a href="javascript:doCategorySearch('${secondCategoryId}','${firstCategoryId}','${firstCategoryName}','${secondCategoryId}','${secondCategoryName}','','');">${secondCategoryName}</a></li>
	      </c:if>
	    </ol>
	          <table class="table table-hover">
	            <thead>
	              <tr>
	                <th class="th-checkbox"><input type="checkbox" id="check-btn" class="tag" title="" data-original-title="全选/反选"></th>
	                <th class="th-sorting">分类ID</th>
	                <th class="th-name">名称</th>
	                <th class="th-name">别名</th>
	                <th class="th-name">摘要</th>
	                 <th width="70">资讯数</th>
	                <th class="th-sorting">排序</th>
	                <th width="50">状态</th>
	                <th class="th-time">更新时间</th>
	                <th class="th-time">创建时间</th>
	              </tr>
	            </thead>
	       
	      <c:set var="pagingUrl" value="${ctx}/content/newsCategory/list"/>
		  <c:set var="nextUrl" value=""/>
		  <c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
		  <c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
		  </c:if>
		  
	            <tbody class="class-list">
				<c:forEach items="${apiRsp.results}" var="item">
					<tr>
						<td><input type="checkbox" name="list-checkbox" value="${item.id}"></td>
						<td>${item.id}</td>
						<td><strong>${item.categoryName}</strong>
							<!-- <div class="text-muted">子分类</div> -->
							<span class="row-actions">
								<a href="javascript:editCategory('${item.id}');"><span class="glyphicon glyphicon-edit"></span>编辑</a>
								<!-- <a href="javascript:addCategory('${item.id}','${item.categoryName}','${item.id}');"><span class="glyphicon glyphicon-edit"></span>添加子分类</a> -->
							</span>
						</td>
						<td>${item.categorySlug}</td>
						<td>${item.categorySummary}</td>
						<td>${item.newsCount}</td>
						<td>${item.ordering}</td>
						<td><span class="glyphicon glyphicon-ok-sign text-success" title="显示"></span></td>
						<td>${item.updateTime}</td>
						<td>${item.createTime}</td>
					</tr>
				</c:forEach>
	            </tbody>
	          </table>
	
	</div><!--panel-info-->
	
        <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="apiRsp" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${pagingUrl}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div><!--main-bottom-->


</div><!--main end-->
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
function doCategorySearch(parentId,firstCategoryId,firstCategoryName,secondCategoryId,secondCategoryName,thirdCategoryId,thirdCategoryName){
	$("#categorySearchForm #parentId").val(parentId);
	$("#categorySearchForm #firstCategoryId").val(firstCategoryId);
	$("#categorySearchForm #firstCategoryName").val(firstCategoryName);
	$("#categorySearchForm #secondCategoryId").val(secondCategoryId);
	$("#categorySearchForm #secondCategoryName").val(secondCategoryName);
	$("#categorySearchForm #thirdCategoryId").val(thirdCategoryId);
	$("#categorySearchForm #thirdCategoryName").val(thirdCategoryName);
	$("#categorySearchForm #category_name").val("");
	$("#categorySearchForm").submit();
}
function doPackageSearch(category_id,firstCategoryId,firstCategoryName,secondCategoryId,secondCategoryName,thirdCategoryId,thirdCategoryName){
	$("#packageSearchForm #category_id").val(category_id);
	$("#packageSearchForm #firstCategoryId").val(firstCategoryId);
	$("#packageSearchForm #firstCategoryName").val(firstCategoryName);
	$("#packageSearchForm #secondCategoryId").val(secondCategoryId);
	$("#packageSearchForm #secondCategoryName").val(secondCategoryName);
	$("#packageSearchForm #thirdCategoryId").val(thirdCategoryId);
	$("#packageSearchForm #thirdCategoryName").val(thirdCategoryName);
	$("#packageSearchForm").submit();
}
function editCategory(id){
	openDialog({
		frame:true,
		title:"修改资讯分类",
	    height:400,
	    width:850,
		url:"${ctx}/content/newsCategory/edit?id="+id
	});
}

function addCategory(id,categoryName,parentId){
	openDialog({
		frame:true,
		title:"添加"+categoryName+"的子分类",
	    height:400,
	    width:850,
		url:"${ctx}/content/newsCategory/edit?parentId="+parentId
	});
}


function bindInitEvents(){
	$(".pin").pin();
	bindCheckAll();
}
$(function(){
	bindInitEvents();
	$("#addCategoryBtn").on("click",function(){
		openDialog({
			frame:true,
			title:"添加资讯分类",
		    height:400,
		    width:850,
			url:"${ctx}/content/newsCategory/edit?parentId=${parentId}"
		});
	});
	$("#searchBtn").click(function(){
		$("#categorySearchForm").submit();
	});
	$("#searchAllBtn").click(function(){
		$("#categorySearchForm #category_name").val("");
		$("#categorySearchForm").submit();
	});
	$("#batchOperateBtn").click(function(){
		var batchOperateType=$("#batchOperateType").val();
		var check_name = document.getElementsByName("list-checkbox");
		//var orders = document.getElementsByName("orders");
		var idArr=new Array();
		//var orderArr=new Array();
		for(var i=0;i<check_name.length;i++){
			if(check_name[i].checked){
				idArr.push(check_name[i].value);
				//orderArr.push(orders[i].value);
			}
		}
		if(batchOperateType=="delete" && idArr.length>0){
			ajaxSubmit("${ctx}/content/newsCategory/delete",{parentId:'${parentId}', ids:idArr.join(",")},reload,"删除成功","确认批量删除？");
		}
		if(batchOperateType=="updateOrder" && idArr.length>0 && orderArr.length>0){
			ajaxSubmit("${ctx}/content/newsCategory/updateOrder",{parentId:'${parentId}', ids:idArr.join(","), orders:orderArr.join(",")},reload,"排序成功","确认批量排序？");
		}
	});	
});
</script>
</body>
</html>
