<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
	<title>标签管理</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">

</head>
<body>
	
<div class="main debris">
	<div class="main-top">    
            <h3>标签管理
            <small>共<span>${tagList.count}</span>个</small></h3> 
            <form class="form-inline pull-right search-box" role="form" action="${ctx}/content/tag/tagList" >
                <input type="text" class="form-control" name="name" value="${keyword}" placeholder="名称">
                <button type="submit" class="btn btn-default">搜索</button>
                <a href="${ctx}/content/tag/tagList" class="btn btn-primary">显示全部</a>
            </form>
    </div><!--main-top-->

<div class="panel panel-info">
  <div class="panel-heading">
  		<div class="pin">
          <button type="button" class="btn btn-primary" onclick="add();" ><span class="glyphicon glyphicon-plus"></span>添加标签</button>
        </div>
  </div>
  <ol class="breadcrumb">
      <li><a href="${ctx}/content/tag/tagList">标签管理</a></li>
  </ol>
    <table class="table table-hover" id="table">
     <thead>
       <tr>
         <th class="th-checkbox">
                  	<input type="checkbox" id="check-btn" class="tag" 
          	          data-container="body" 
				      data-title="<h5>友情提示</h5>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h5>请选择要删除的应用</h5></div>"
          	>
         </th>
         <th class="th-name">名称</th>
         <th class="th-time">更新日期</th>
       </tr>
     </thead>
		<tbody id="morePackageListDiv">
     	<jsp:include page="/WEB-INF/pages/content/tag/moreTagList.jsp" flush="true"/>
     </tbody>
   </table>

</div><!--panel-info-->
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="tagList" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${ctx}/content/tag/tagList?name=${keyword}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div>
</div><!--main end-->
 
 
<div id="dialogId"></div>
<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.pin.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/common.js" type="text/javascript"></script>
<script src="${ctx}/js/dialog.js"></script>
<script>


var dialog =new dialogBox("dialogId");
dialog.initComponent({url:"${ctx}/content/tag/getTag",title:'添加标签'});
function add(){
	dialog.setUrl("${ctx}/content/tag/getTag");
	dialog.setTitle("添加标签");
	dialog.show();
}
function edit(id){
	dialog.setUrl("${ctx}/content/tag/getTag?id="+id);
	dialog.setTitle("编辑标签");
	dialog.show();
}

function selectedTopic(submit){
	var check_name = document.getElementsByName("list-checkbox");
	var packageId="";
	if(submit){
		for(var i=0; i<check_name.length; i++){
			if(check_name[i].checked){
				packageId=packageId+check_name[i].value+",";
			}
		}
		$("#topicId").val(packageId);
	}else{
		var check=document.getElementById("check-btn").checked;
		for(var i=0; i<check_name.length; i++){
			check_name[i].checked = check;
		}
	}
	if(submit&&packageId==''){
		$("#check-btn").popover('show');
		setTimeout(function(){
			$("#check-btn").popover('destroy');
		},2000);
		return false;
	}else{
	    return true;
	}
}

$(function(){
	/*锁定操作栏*/
	$(".pin").pin();
	/*全选 取消全选*/	
	$("#check-btn").on("click",function(){
		selectedTopic(false);
	});
});

</script>

</body>
</html>




