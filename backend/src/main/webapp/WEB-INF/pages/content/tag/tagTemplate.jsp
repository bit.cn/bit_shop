<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<form role="form" id="topicFormId" action="${ctx}/content/tag/editTag" method="post" >
<input type="hidden" name="id" value="${tag.id}" class="form-control">   

    <div class="modal-body">      
	<div class="form-group">
	  <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>标签名称</label>
	  <input type="text" id="inputSuccess1" name="name" class="form-control" value="${tag.name}" required autofocus>
	  
	</div>
    </div>
    <div class="modal-footer">
      <button data-dismiss="modal" class="btn btn-default" type="button">关闭</button>
      <button autocomplete="off" data-loading-text="Loading..." class="btn btn-success loading-btn" type="submit"><span class="glyphicon glyphicon-ok"></span> 确认提交</button>
    </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
	$("#topicFormId").submit(function(event) {
		$(this).ajaxForm({
			success:function(event,param){
				var data=param.data;
				for(var index in data){
					var apiRsp=data[index];
					if(apiRsp && apiRsp.isSuccess){
						location.reload();
					}else{
						alert(apiRsp.msg);
					}
				}
			}
		});
		event.preventDefault();
	});	
});
</script>


