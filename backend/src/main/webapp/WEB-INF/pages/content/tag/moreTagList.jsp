<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
 <c:forEach var="item" items="${tagList.results}">
              <tr id="${item.id}">
                <td><input type="checkbox" name="list-checkbox" value="${item.id}"></td>
                <td class="td-name"><strong>${item.name}</strong>
                	<span class="row-actions">
                		<a href="javascript:void(0);" onclick="edit(${item.id});"><span class="glyphicon glyphicon-edit"></span>编辑</a>
                	</span>
                </td>
                <td>${item.updatedDatetime}</td>
              </tr>
</c:forEach>