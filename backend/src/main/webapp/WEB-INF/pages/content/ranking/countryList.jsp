<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
	<title>专区管理</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">

</head>
<body>
	
<div class="main debris">
	<div class="main-top">    
            <h3>
           	  ${title}国家管理
            <small>共<span>${categoryList.count}</span>个</small></h3> 
            <form class="form-inline pull-right search-box" role="form" action="${ctx}/content/rank/countryList" >
            	<input type="hidden" name="ranking_type_id" value="${ranking_type_id}" >
            	<input type="hidden" name="title" value="${title}" >
            	<input type="hidden" name="rankingBy" value="${rankingBy}">
                <a href="${ctx}/content/rank/countryList?ranking_type_id=${ranking_type_id}&title=${title}&rankingBy=${rankingBy}" class="btn btn-primary">显示全部</a>
            </form>
    </div><!--main-top-->

<div class="panel panel-info">
  <div class="panel-heading">
  		<div class="pin">
          <form class="form-inline" id="optionFormId" method="post" >
            <input type="hidden" name="ranking_type_id" value="${ranking_type_id}" >
            <input type="hidden" name="title" value="${title}" >
            <input type="hidden" name="ids" id="categoryItemId" value="" >
            <input type="hidden" name="status" id="statusId" value="" >
            <input type="hidden" name="rankingBy" value="${rankingBy}">
               <div class="form-group">
                     <select class="form-control" id="optionTypeId">
                     		<option value="order" selected="selected">排序</option>
                            <option value="2">发布</option>
                            <option value="1">草稿</option>
                            <option value="del">删除</option>
                     </select>
               </div>
               <div class="btn-group">
                 <button type="submit" class="btn btn-default" id="submitButtonId"
                      data-container="body" 
				      data-title="<h4>友情提示</h4>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h3>${message}</h3></div>" 
                 >确认</button>
               </div>
             </form>
             
          <button type="button" class="btn btn-primary" id="addAppButtonId" ><span class="glyphicon glyphicon-plus"></span>添加榜单</button>
        </div>
  </div>
  <ol class="breadcrumb">
      <li><a href="${ctx}/content/rank/rankList">榜单管理</a></li>
      <li>${title}</li>
  </ol>
   	<table class="table" id="table_drag" style="display: none;"></table>

    <table class="table table-hover" id="table">
     <thead>
       <tr>
         <th class="th-checkbox">
                  	<input type="checkbox" id="check-btn" class="tag" 
          	          data-container="body" 
				      data-title="<h5>友情提示</h5>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h5>请选择要删除的应用</h5></div>"
          	>
         </th>
          <th class="th-sorting">排序</th>
         <th class="th-name">国家名称</th>
         <th class="th-sorting">应用数</th>
         <th class="th-sorting">状态</th>
       </tr>
     </thead>
       <c:set var="nextUrl" value=""/>
      	<c:if test="${not empty rankList and rankList.curPage!=rankList.totalPages}">
      	<c:set var="nextUrl" value="${ctx}/content/rank/countryList?ranking_type_id=${ranking_type_id}&rankingBy=${rankingBy}&page=${rankList.curPage+1}"/>
      	</c:if>
		<tbody id="morePackageListDiv" nextUrl="${nextUrl}">
 		<c:forEach var="item" items="${categoryList.results}">
              <tr id="${item.id}">
                <td><input type="checkbox" name="list-checkbox" value="${item.id}"></td>
                <td><input type="number"  value="${item._order}" class="form-control"/></td>
                <td><strong>${item.country_name}</strong>
			   		<a href="${ctx}/content/rank/packageList?ranking_id=${item.id}&ranking_type_id=${item.ranking_type_id}&package_status=published&rankingBy=${rankingBy}&country=${item.country_code}" target="mainFrame"><span class="glyphicon glyphicon-th-list"></span>应用管理</a>
                </td>
                <td>${item.total}</td>
                 <td>

                	<c:if test="${item.status eq '1'}">
                		<span title="草稿" class="glyphicon glyphicon-remove-sign text-danger"></span>
                	</c:if>
                	<c:if test="${item.status eq '2'}">
                		<span title="已发布" class="glyphicon glyphicon-ok-sign text-success"></span>
                	</c:if>
                </td>
              </tr>
		</c:forEach>
     </tbody>
   </table>

</div><!--panel-info-->


    <c:if test="${not empty nextUrl}">
	    <div class="load_more">
	    	<button id="loadingDiv" style="display:none" type="button" class="btn btn-default btn-block loading-btn">Loading...</button>
	    	<button id="moreDiv" style="display:" type="button" class="btn btn-default btn-block loading-btn" data-loading-text="Loading..." autocomplete="off">加载更多</button>
	    </div>
    </c:if>
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="rankList" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${ctx}/content/rank/countryList?ranking_type_id=${ranking_type_id}&rankingBy=${rankingBy}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div>
</div><!--main end-->
 
 
<div id="dialogId"></div>

<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.pin.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/common.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.tableDnD.js" type="text/javascript"></script>
<script src="${ctx}/js/dialog.js"></script>
<script>
function selectedCategory(submit){
	var check_name = document.getElementsByName("list-checkbox");
	var packageId="";
	if(submit){
		for(var i=0; i<check_name.length; i++){
			if(check_name[i].checked){
				packageId=packageId+check_name[i].value+",";
			}
		}
		$("#categoryItemId").val(packageId);
	}else{
		var check=document.getElementById("check-btn").checked;
		for(var i=0; i<check_name.length; i++){
			check_name[i].checked = check;
		}
	}
	if(submit&&packageId==''){
		$("#check-btn").popover('show');
		setTimeout(function(){
			$("#check-btn").popover('destroy');
		},2000);
		return false;
	}else{
	    return true;
	}
}
function orderItem(){
	var topicItem="";
	var table = document.getElementById("table");
	var tr = table.getElementsByTagName("tr");
	for (var i = 1; i < tr.length; i++) {
		var rowid = tr[i].getAttribute("id")+","+i;
		topicItem=topicItem+rowid+"#";
	}
	$("#categoryItemId").val(topicItem);
	if(topicItem==''){
		return false;
	}else{
	    return true;
	}
}

$(document).ready(function() {
	$("#table").tableDnD();
	/*锁定操作栏*/
	$(".pin").pin();
	$("#moreDiv").loadingMore({
		dataDivId:'morePackageListDiv',
		successCallback:function(){
			$(".pin").pin();
			$("#table").tableDnD();
		}
	});
	$("#addAppButtonId").on("click",function(){
		var dialog =new dialogBox("dialogId");
		dialog.initComponent({url:"${ctx}/content/rank/addCountryList?ranking_type_id=${ranking_type_id}&title=${title}&rankingBy=${rankingBy}",frame:true,title:'添加国家',height:700,width:900});
		dialog.show();
	});
	$("#optionFormId").submit(function(){
		if($("#optionTypeId").val()=="1"||$("#optionTypeId").val()=="2"){
			$("#statusId").val($("#optionTypeId").val());
			$(this).attr("action","${ctx}/content/rank/updateCategoryStauts");
			return selectedCategory(true);
		}else if($("#optionTypeId").val()=="order"){
			$(this).attr("action","${ctx}/content/rank/rankCategoryOrder");
			return orderItem();
		}else{
			$(this).attr("action","${ctx}/content/rank/delRankCategory");
			return selectedCategory(true);
		}
		return false;
	});
	$("#check-btn").on("click",function(){
		selectedCategory(false);
	});
	if("${searchType}"!=''){
		$("#searchType").val("${searchType}");	
	}
	if("${message}"!=''){
		$("#submitButtonId").popover('show');
		setTimeout(function(){
			$("#submitButtonId").popover('destroy');
		},2000);
	}
});
</script>

</body>
</html>




