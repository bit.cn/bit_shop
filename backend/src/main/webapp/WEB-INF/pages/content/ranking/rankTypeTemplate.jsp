<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>


<form role="form" id="rankFormId" action="${ctx}/content/rank/editRank">
<input type="hidden" name="id" value="${rank.id}" >
      <div class="modal-body">      
      
<div class="form-group">
  <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>榜单名称</label>
  <input type="text" id="inputSuccess1" name="title" value="${rank.title}" class="form-control" required="required">
</div>

<div class="form-group">
  <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>排行维度</label>
	<input type="radio" name="ranking_by" id="ranking_by_category" value="category" autocomplete="off" required="required" <c:if test="${rank.rankingBy eq 'category'}">checked="checked" disabled="disabled"</c:if>>分类
	<input type="radio" name="ranking_by" id="ranking_by_country" value="country" autocomplete="off" required="required" <c:if test="${rank.rankingBy eq 'country'}">checked="checked" disabled="disabled"</c:if>>国家
</div>

      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">关闭</button>
        <button autocomplete="off" data-loading-text="Loading..." class="btn btn-success loading-btn" type="submit"><span class="glyphicon glyphicon-ok"></span> 确认提交</button>
      </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
	$("#rankFormId").submit(function(event) {
		$(this).ajaxForm({
			success:function(event,param){
				var data=param.data;
				for(var index in data){
					var apiRsp=data[index];
					if(apiRsp && apiRsp.isSuccess){
						location.reload();
					}else{
						alert(apiRsp.msg);
					}
				}
			}
		});
		event.preventDefault();
	});	
});
</script>

