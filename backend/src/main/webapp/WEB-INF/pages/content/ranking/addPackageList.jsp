<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>未加入榜单的应用</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">
</head>

<body>
<div class="main game-list">

	<div class="main-top">
    
            <h3>可添加的游戏 <small>共<span>${packageList.count}</span>个</small></h3>
            
            <form class="form-inline pull-right search-box" role="form" action="${ctx}/content/rank/addPackageList" >
                <input type="hidden" name="ranking_id" value="${ranking_id}" >
            	<input type="hidden" name="category" value="${category}" >
            	<input type="hidden" name="country" value="${country}" >
            	<select class="form-control" name="searchType" id="searchType">
            		<option value="package_id" selected="selected">应用ID</option>
                    <option value="title">应用名称</option>
                    <option value="package_name">应用包名</option>
                    <option value="category_name" >分类名称</option>
                    <option value="tags" >标签名称</option>
                </select>
                <input type="text" class="form-control" name="searchValue" value="${searchValue}" placeholder="关键词">
                <button type="submit" class="btn btn-default">搜索</button>
                <a href="${ctx}/content/rank/addPackageList?ranking_id=${ranking_id}&category=${category}&country=${country}" class="btn btn-primary">显示全部</a>
            </form>
            
    </div><!--main-top-->
    
    
    
<div class="panel panel-info">
  <div class="panel-heading">
  <!--查看 开始-->
  <div class="pin">
  	<form  action="${ctx}/content/rank/addPackageToRank" id="submitFormId" >
      <input type="hidden" name="ranking_id" value="${ranking_id}" >
  	<input type="hidden" name="category" value="${category}" >
    <input type="hidden" name="country" value="${country}" >
  	<input type="hidden" name="package_ids" id="packageId" value="" >
        <div class="btn-group">
          <button type="submit"  class="btn btn-default" id="submitButtonId"   
      data-container="body" 
      data-title="<h4>友情提示</h4>"
      data-toggle="popover" 
      data-html="true"
      data-content="<div><h3>成功添加<span>${addPackageCounter}</span>个应用</h3></div>" >加入到${category}榜单</button>
        </div>
	</form>
  </div>

  </div>
  
    <table class="table table-hover" id="table">
      <thead>
        <tr>
          <th class="th-check"><input type="checkbox" id="check-btn" class="tag" title="全选/反选"></th>
          <th class="th-name">名称</th>
          <th class="th-package">版本/包名</th>
          <th class="th-time">发布日期</th>
        </tr>
      </thead>
      <tbody>
		<c:forEach items="${packageList.results}" var="item">
	        <tr>
	          <td><input type="checkbox" name="list-checkbox" value="${item.package_id}"></td>
	          <td class="td-name">
	          		<img src="${item.icon}" class="img-rounded game-icon"/><strong>${item.title}</strong><br/>
	          		<!-- <span class="en-name">Subway Surfers</span> -->
	          		<span class="row-actions"><a href="http://android.ccplay.com.cn/package/${item.package_id}" target="_blank"><span class="glyphicon glyphicon-eye-open"></span>预览</a></span>
	          </td>
	            <td>${item.version_name} <br/> ${item.package_name}</td>
	            <td>${item.released_datetime}</td>
	        </tr>
		</c:forEach>
      </tbody>
    </table>
</div><!--panel-info-->
 
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="packageList" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${ctx}/content/rank/addPackageList?ranking_id=${ranking_id}&category=${category}&country=${country}&searchType=${searchType}&searchValue=${searchValue}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div>

</div>



<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.pin.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function selectedPackage(submit){
	var check_name = document.getElementsByName("list-checkbox");
	var packageId="";
	if(submit){
		for(var i=0; i<check_name.length; i++){
			if(check_name[i].checked){
				packageId=packageId+check_name[i].value+",";
			}
		}
		$("#packageId").val(packageId);
	}else{
		var check=document.getElementById("check-btn").checked;
		for(var i=0; i<check_name.length; i++){
			check_name[i].checked = check;
		}
	}
	if(submit&&packageId==''){
		return false;
	}else{
	    return true;
	}
}
$(document).ready(function() {
	/*锁定操作栏*/
	$(".pin").pin();
	/*全选 取消全选*/	
	$("#check-btn").on("click",function(){
		selectedPackage(false);
	});
	$("#submitFormId").submit(function(){
		return selectedPackage(true);
	});
	if("${searchType}"!=''){
		$("#searchType").val("${searchType}");	
	}
	if("${addPackageCounter}"!=''){
		$("#submitButtonId").popover('show');
		setTimeout(function(){
			$("#submitButtonId").popover('destroy');
		},2000);
	}
});

</script>
</body>
</html>
