<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>专题下的应用</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">
</head>

<body>
<div class="main game-list">

	<div class="main-top">
    
            <h3>可添加的分类<small>共<span>${categoryList.count}</span>个</small></h3> 
            <form class="form-inline pull-right search-box" role="form" action="${ctx}/content/rank/addCategoryList?ranking_type_id=${ranking_type_id}" >
            	<input type="hidden" name="ranking_type_id" value="${ranking_type_id}" >
            	<input type="hidden" name="title" value="${title}" >
                <input type="text" class="form-control" name="category_name" value="${category_name}" placeholder="分类名称">
                <button type="submit" class="btn btn-default">搜索</button>
                <a href="${ctx}/content/rank/addCategoryList?ranking_type_id=${ranking_type_id}&title=${title}" class="btn btn-primary">显示全部</a>
            </form>
            
    </div><!--main-top-->
    
    
    
<div class="panel panel-info">
  <div class="panel-heading">
  <!--查看 开始-->
  <div class="pin">
  	<form  action="${ctx}/content/rank/addCategoryToRank" id="submitFormId" >
	   	<input type="hidden" name="ranking_type_id" value="${ranking_type_id}" >
	  	<input type="hidden" name="title" value="${title}" >
	  	<input type="hidden" name="category_name" value="${category_name}" >
	  	<input type="hidden" name="ids" id="packageId" value="" >
        <div class="btn-group">
          <button type="submit"  class="btn btn-default" id="submitButtonId"   
      data-container="body" 
      data-title="<h4>友情提示</h4>"
      data-toggle="popover" 
      data-html="true"
      data-content="<div><h3><span>${message}</span></h3></div>" >加入到${title}榜单</button>
        </div>
	</form>
  </div>

  </div>
  
    <table class="table table-hover" id="table">
      <thead>
        <tr>
          <th class="th-check"><input type="checkbox" id="check-btn" class="tag" title="全选/反选"></th>
          <th class="th-name">分类名称</th>
        </tr>
      </thead>
      <tbody>
 			<c:forEach var="item" items="${categoryList.results}">
              <tr>
                <td><input type="checkbox" name="list-checkbox" value="${item.category_id}"></td>
                <td><strong>${item.category_name}</strong></td>
              </tr>
			</c:forEach>
      </tbody>
    </table>
</div><!--panel-info-->
 
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="categoryList" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${ctx}/content/rank/addCategoryList?ranking_type_id=${ranking_type_id}&title=${title}&category_name=${category_name}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div>

</div>



<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.pin.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function selectedPackage(submit){
	var check_name = document.getElementsByName("list-checkbox");
	var packageId="";
	if(submit){
		for(var i=0; i<check_name.length; i++){
			if(check_name[i].checked){
				packageId=packageId+check_name[i].value+",";
			}
		}
		$("#packageId").val(packageId);
	}else{
		var check=document.getElementById("check-btn").checked;
		for(var i=0; i<check_name.length; i++){
			check_name[i].checked = check;
		}
	}
	if(submit&&packageId==''){
		return false;
	}else{
	    return true;
	}
}
$(document).ready(function() {
	/*锁定操作栏*/
	$(".pin").pin();
	/*全选 取消全选*/	
	$("#check-btn").on("click",function(){
		selectedPackage(false);
	});
	$("#submitFormId").submit(function(){
		return selectedPackage(true);
	});
	if("${searchType}"!=''){
		$("#searchType").val("${searchType}");	
	}
	if("${addPackageCounter}"!=''){
		$("#submitButtonId").popover('show');
		setTimeout(function(){
			$("#submitButtonId").popover('destroy');
		},2000);
	}
});

</script>
</body>
</html>
