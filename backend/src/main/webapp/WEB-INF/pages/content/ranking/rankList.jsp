<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
	<title>榜单管理</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">

</head>
<body>
	
<div class="main debris">
	<div class="main-top">    
            <h3>
           	  榜单管理
            <small>共<span>${rankList.count}</span>个</small></h3> 
            <form class="form-inline pull-right search-box" role="form" action="${ctx}/content/rank/rankList" >
                <input type="text" class="form-control" name="title" value="${title}" placeholder="标题">
                <button type="submit" class="btn btn-default">搜索</button>
                <a href="${ctx}/content/rank/rankList" class="btn btn-primary">显示全部</a>
            </form>
    </div><!--main-top-->

<div class="panel panel-info">
  <div class="panel-heading">
  		<div class="pin">
          <form class="form-inline" id="optionFormId" method="post" >
            <input type="hidden" name="ids" id="rankId" value="" >
               <div class="form-group">
                     <select class="form-control" id="optionTypeId">
                            <option value="del">删除</option>
                     </select>
               </div>
               <div class="btn-group">
                 <button type="submit" class="btn btn-default" id="submitButtonId"
                      data-container="body" 
				      data-title="<h4>友情提示</h4>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h3>${message}</h3></div>" 
                 >确认</button>
               </div>
             </form>
             
          <button type="button" class="btn btn-primary" onclick="add();" ><span class="glyphicon glyphicon-plus"></span>添加榜单</button>
        </div>
  </div>
  <ol class="breadcrumb">
      <li><a href="${ctx}/content/rank/rankList">榜单管理</a></li>
  </ol>
   <table class="table table-hover">
     <thead>
       <tr>
         <th class="th-checkbox">
                  	<input type="checkbox" id="check-btn" class="tag" 
          	          data-container="body" 
				      data-title="<h5>友情提示</h5>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h5>请选择要删除的榜单</h5></div>"
          	>
         </th>
         <th class="th-name">名称</th>
         <th width="200">别名</th>
         <th class="th-sorting">搜索关键字</th>
         <th class="th-sorting">排行维度</th>
         <th width="50">状态</th>
         <th class="th-time">更新日期</th>
       </tr>
     </thead>
     <tbody class="class-list">
 		<c:forEach var="rank" items="${rankList.results}">
              <tr>
                <td><input type="checkbox" name="list-checkbox" value="${rank.id}"></td>
                <td><strong>${rank.title}</strong>
                	<div class="text-muted">${rank.description}</div>
                	<c:choose>
                		<c:when test="${rank.rankingBy eq 'category'}">
                			<a href="${ctx}/content/rank/categoryList?ranking_type_id=${rank.id}&title=${rank.title}&rankingBy=${rank.rankingBy}" target="mainFrame"><span class="glyphicon glyphicon-th-list"></span>分类</a>
                		</c:when>
                		<c:otherwise>
                			<a href="${ctx}/content/rank/countryList?ranking_type_id=${rank.id}&title=${rank.title}&rankingBy=${rank.rankingBy}" target="mainFrame"><span class="glyphicon glyphicon-th-list"></span>国家</a>
                		</c:otherwise>
                	</c:choose>
                	<a href="javascript:void(0);" onclick="edit(${rank.id});"><span class="glyphicon glyphicon-edit"></span>编辑</a></span>
                </td>
                <td>${rank.slug}</td>
                <td>${rank.keywordsString}</td>
                <td>${rank.rankingBy eq 'category'?'分类':'国家'}</td>
                 <td>

                	<c:if test="${rank.status eq '1'}">
                		<span title="草稿" class="glyphicon glyphicon-remove-sign text-danger"></span>
                	</c:if>
                	<c:if test="${rank.status eq '2'}">
                		<span title="已发布" class="glyphicon glyphicon-ok-sign text-success"></span>
                	</c:if>
                </td>
                <td>${rank.updated}</td>
              </tr>
		</c:forEach>
     </tbody>
   </table>

</div><!--panel-info-->
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="rankList" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${ctx}/content/rank/rankList?title=${title}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div>
</div><!--main end-->
 
 
<div id="dialogId"></div>

<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.pin.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/common.js" type="text/javascript"></script>
<script src="${ctx}/js/dialog.js"></script>
<script>


var dialog =new dialogBox("dialogId");
dialog.initComponent({url:"${ctx}/content/rank/getRankType",frame:false,title:'添加榜单'});
function add(){
	dialog.load("${ctx}/content/rank/getRankType");
	dialog.setTitle("添加榜单");
	dialog.show();
}
function edit(id){
	dialog.reload("${ctx}/content/rank/getRankType?id="+id);
	dialog.setTitle("编辑榜单");
	dialog.show();
}

function selectedRank(submit){
	var check_name = document.getElementsByName("list-checkbox");
	var packageId="";
	if(submit){
		for(var i=0; i<check_name.length; i++){
			if(check_name[i].checked){
				packageId=packageId+check_name[i].value+",";
			}
		}
		$("#rankId").val(packageId);
	}else{
		var check=document.getElementById("check-btn").checked;
		for(var i=0; i<check_name.length; i++){
			check_name[i].checked = check;
		}
	}
	if(submit&&packageId==''){
		$("#check-btn").popover('show');
		setTimeout(function(){
			$("#check-btn").popover('destroy');
		},2000);
		return false;
	}else{
	    return true;
	}
}

$(function(){
	/*锁定操作栏*/
	$(".pin").pin();
	/*全选 取消全选*/	
	$("#check-btn").on("click",function(){
		selectedRank(false);
	});
	$("#optionFormId").submit(function(){
		$(this).attr("action","${ctx}/content/rank/delRank");
		return selectedRank(true);
	});
	if("${message}"!=''){
		$("#submitButtonId").popover('show');
		setTimeout(function(){
			$("#submitButtonId").popover('destroy');
		},2000);
	}
});

</script>

</body>
</html>




