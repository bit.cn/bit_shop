<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>编辑厂商</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>
<div class="container">

<form id="editForm" role="form" action="${ctx}/content/author/save" method="post">
	<input type="hidden" name="id" value="${tc.id}">
	<input type="hidden" id="icon" name="icon" value="">
	<input type="hidden" id="icon_file_size" name="icon_file_size" value="">
	<input type="hidden" id="icon_file_md5" name="icon_file_md5" value="">
	<input type="hidden" id="cover" name="cover" value="">
	<input type="hidden" id="cover_file_size" name="cover_file_size" value="">
	<input type="hidden" id="cover_file_md5" name="cover_file_md5" value="">
      <div class="modal-body">      

 
		<div class="form-group">
		  <label class="control-label" for="author_name"><strong class="necessary">*</strong>厂商名称</label>
		  <input type="text" class="form-control" id="author_name" name="author_name" value="${tc.name}" placeholder="请输入厂商名称" required="required">  
		</div>
		<div class="form-group">
		  <label class="control-label" for="email">邮箱</label>
		  <input type="email" class="form-control" id="email" name="email" value="${tc.email}" placeholder="请输入邮箱">  
		</div>
		<div class="form-group">
		  <label class="control-label" for="phone">电话</label>
		  <input type="text" class="form-control" id="phone" name="phone" value="${tc.phone}" placeholder="请输入电话">  
		</div>
		<div class="form-group">
		    <div class="row">
		          <div class="col-xs-3">
		                <label class="control-label" for="">LOGO</label>
		                <div>
					               <!-- <span class="btn btn-primary btn-xs fileinput-button">
					                   <i class="glyphicon glyphicon-plus"></i>
					                   <span>上传LOGO</span>
					                   <input id="iconUpload" type="file" name="fileObj">
					               </span> -->
					               <button id="chooseIconBtn" type="button" class="btn btn-primary loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-plus"></span> 选择ICON</button>
		                </div>
		                <div id="iconPreviewDiv" style="margin-top:10px">
				            <c:if test="${not empty tc.icon}">
				            	<a href='${tc.iconStaticUrl}' target='_blank'><img src="${tc.iconStaticUrl}" width="72" height="72"></a>
				            </c:if>
		                </div>
		          </div>
		          <div class="col-xs-9">
		                <label class="control-label" for="">封面</label>
						<div>
					               <!-- <span class="btn btn-primary btn-xs fileinput-button">
					                   <i class="glyphicon glyphicon-plus"></i>
					                   <span>上传封面</span>
					                   <input id="coverUpload" type="file" name="fileObj">
					               </span>					          -->
					               <button id="chooseCoverBtn" type="button" class="btn btn-primary loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-plus"></span> 选择封面</button>
						</div>
						<div id="coverPreviewDiv" style="margin-top:10px">
				            <c:if test="${not empty tc.cover}">
				            	<a href='${tc.coverStaticUrl}' target='_blank'><img src="${tc.coverStaticUrl}" width="450" height="215"></a>
				            </c:if>
						</div>
		          </div>
		    </div>
		</div><!--form-group-->



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="javascript:closeDialog();">关闭</button>
        <button type="submit" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-ok"></span> 确认提交</button>
      </div>
</form>

</div>
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
function chooseIconCallback(id,fileType,fileKey,fileName,cdnUrl){
	$("#icon").val(fileKey);
	$("#iconPreviewDiv").html("<img src='"+cdnUrl+"' width='72' height='72'>");
	closeDialog();
}
function chooseCoverCallback(id,fileType,fileKey,fileName,cdnUrl){
	$("#cover").val(fileKey);
	$("#coverPreviewDiv").html("<img src='"+cdnUrl+"' width='450' height='215'>");
	closeDialog();
}
$(function () {
    'use strict';
	$("#editForm").bind('submit', function(event) {
		ajaxFormSubmit(this,reloadParent,"提交成功！",null);
		event.preventDefault();
	});
    $("#chooseIconBtn").on("click",function(){
    	openCommonDialog("选择ICON","${ctx}/system/cdn/list?contentTypeId=15&chooseCallback=chooseIconCallback&limitFileType=image");
    });
    $("#chooseCoverBtn").on("click",function(){
    	openCommonDialog("选择封面","${ctx}/system/cdn/list?contentTypeId=15&chooseCallback=chooseCoverCallback&limitFileType=image");
    });
});
</script>
</body>
</html>
