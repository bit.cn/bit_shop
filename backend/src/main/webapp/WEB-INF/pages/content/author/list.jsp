<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>厂商</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>
	

<div class="main vendors">



	<div class="main-top">    
            <h3>厂商管理 <small>共<span>${apiRsp.count}</span>个</small></h3> 
            <form id="searchForm" action="${ctx}/content/author/list" method="post" class="form-inline pull-right search-box" role="form">
                <input type="text" class="form-control" placeholder="名称" id="author_name" name="author_name" value="${author_name}">
                <button id="searchBtn" type="button" class="btn btn-default">搜索</button>
                <button id="searchAllBtn" type="button" class="btn btn-primary">显示全部</button>
            </form>
              
    </div><!--main-top-->





<div class="panel panel-info">
  <div class="panel-heading">
  		<div class="pin">
          <form class="form-inline" role="form">
            <div class="form-group">
                  <select id="batchOperateType" class="form-control">
                            <option value="">批量操作</option>
                            <!-- <option>排序</option>
                            <option>显示</option>
                            <option>隐藏</option> -->
                            <option value="publish">发布</option>
                            <option value="unpublish">取消发布</option>
                            <option value="delete">删除</option>
                  </select>
            </div>
                  <div class="btn-group">
                    <button id="batchOperateBtn" type="button" class="btn btn-default">确认</button>
                  </div>
          </form>
          
          <button id="addAuthorBtn" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>添加厂商</button>
        </div>
  </div>
          <table class="table table-hover">
            <thead>
              <tr>
                <th class="th-checkbox"><input type="checkbox" id="check-btn" class="tag" title="" data-original-title="全选/反选"></th>
                <!-- <th class="th-sorting">排序</th> -->
                <th width="100">ID</th>
                <th class="th-name">名称</th>
                <th width="270">邮箱</th>
                <th width="270">电话</th>
                <th width="170">LOGO</th>
                <th width="170">封面</th>
                <th width="100">应用数</th>
                <th width="50">状态</th>
              </tr>
            </thead>
		  <c:set var="pagingUrl" value="${ctx}/content/author/list?author_name=${author_name}"/>
		  <c:set var="nextUrl" value=""/>
		  <c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
		  <c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
		  </c:if>
            <tbody id="moreAuthorListDiv" nextUrl="${nextUrl}" class="class-list">
            <c:forEach items="${apiRsp.results}" var="item">
              <tr>
                <td><input type="checkbox" name="list-checkbox" value="${item.id}"></td>
                <td>${item.id}</td>
                <!-- <td><input type="number"  value="" class="form-control"/></td> -->
                <td class="td-name"><strong>${item.name}</strong>
                	<span class="row-actions">
                		<a href="javascript:editAuthor('${item.id}');"><span class="glyphicon glyphicon-edit"></span>编辑</a>
                	</span>
                </td>
                <td>${item.email}</td>
                <td>${item.phone}</td>
                <td>
                	<c:if test="${not empty item.icon}">
                		<img src="${item.iconStaticUrl}" class="logo" width="48px" height="48px"></td>
                	</c:if>
                <td>
                	<c:if test="${not empty item.cover}">
                		<img src="${item.coverStaticUrl}" class="cover">
                	</c:if>
                </td>
                <td>
                	<a href="javascript:;" onclick="javascript:parent.addTab('所有应用','${ctx}/content/package/list?searchType=author_id&searchValue=${item.id}',true);">${item.packageCount}</a>
                </td>
                <td>
                	<c:choose>
                	<c:when test="${item.status eq 'activated'}">
                		<span class="glyphicon glyphicon-ok-sign text-success" title="激活"></span>
                	</c:when>
                	<c:otherwise>
                		<span class="glyphicon glyphicon-remove-sign text-danger" title="草稿"></span>
                	</c:otherwise>
                	</c:choose>
                </td>
              </tr>
            </c:forEach>  
            </tbody>
          </table>

</div><!--panel-info-->
    
    <c:if test="${not empty nextUrl}">
	    <div class="load_more">
	    	<button id="loadingDiv" style="display:none" type="button" class="btn btn-default btn-block loading-btn">Loading...</button>
	    	<button id="moreDiv" style="display:" type="button" class="btn btn-default btn-block loading-btn" data-loading-text="Loading..." autocomplete="off">加载更多</button>
	    </div>
    </c:if>
    
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="apiRsp" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${pagingUrl}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div><!--main-bottom-->

</div><!--main end-->
 

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
function editAuthor(id){
	openDialog({
		frame:true,
		title:"修改厂商",
	    height:800,
	    width:900,
		url:"${ctx}/content/author/edit?id="+id
	});
}
function batchOperate(batchOperateType,idArr){
	var operateTitle="";
	var url="";
	if(batchOperateType=="delete"){
		operateTitle="批量删除";
		url="${ctx}/content/author/delete";
	}else if(batchOperateType=="publish"){
		operateTitle="批量发布";
		url="${ctx}/content/author/publish";
	}else if(batchOperateType=="unpublish"){
		operateTitle="批量取消发布";
		url="${ctx}/content/author/unpublish";
	}
	ajaxSubmit(url,{ids:idArr.join(",")},reload,operateTitle+"成功","确认"+operateTitle+"？");
}
function bindInitEvents(){
	/*锁定操作栏*/
	$(".pin").pin();
	/*全选 取消全选*/	
	bindCheckAll();
}
$(function(){
	bindInitEvents();
	$("#moreDiv").loadingMore({
		dataDivId:'moreAuthorListDiv',
		successCallback:function(){
			bindInitEvents();
		}
	});	
	$("#addAuthorBtn").on("click",function(){
		openDialog({
			frame:true,
			title:"添加厂商",
		    height:800,
		    width:900,
			url:"${ctx}/content/author/edit"
		});
	});
	$("#searchBtn").click(function(){
		$("#searchForm").submit();
	});
	$("#searchAllBtn").click(function(){
		$("#searchForm #author_name").val("");
		$("#searchForm").submit();
	});
	$("#batchOperateBtn").click(function(){
		var batchOperateType=$("#batchOperateType").val();
		var check_name = document.getElementsByName("list-checkbox");
		var idArr=new Array();
		for(var i=0;i<check_name.length;i++){
			if(check_name[i].checked){
				idArr.push(check_name[i].value);
			}
		}
		if(batchOperateType!="" && idArr.length>0){
			batchOperate(batchOperateType,idArr);
		}
	});	
});
</script>
</body>
</html>
