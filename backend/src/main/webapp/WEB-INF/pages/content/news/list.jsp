<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
	<title>资讯管理</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/js/jQueryFileUpload/jquery.fileupload.css">
</head>
<body>
	
<div class="main debris">
	<div class="main-top">    
            <h3>
          	所有资讯
            <small>共<span>${apiRsp.count}</span>个</small></h3> 
            <form id="certificateForm" class="form-inline pull-right search-box" role="form" action="${ctx}/content/news/list" >
	            <div class="form-group">
		             <span>分类:</span>
				 <select name="categoryId" id="categoryId" class="form-control">
				    <option value="">---请选择---</option>
				    <c:forEach items="${newscategorylist}" var="obj">
				        <option value="${obj.id}" <c:if test="${obj.id == categoryId}"> selected="selected" </c:if> >${obj.categoryName}</option>
				    </c:forEach>
				</select>
				</div>
				
			  <div class="form-group">
		         <span>状态:</span>
				 <select name="status" id="status" class="form-control">
				    <option value="">---请选择---</option>
                         <option value="publish">发布</option>
                         <option value="draft">草稿</option>
				</select>
			 </div>
			                
             <input type="text" class="form-control" name="newsTitle" value="${newsTitle}" placeholder="关键字">
             <button type="submit" class="btn btn-default">搜索</button>
             <a href="${ctx}/content/news/list?categoryId=${categoryId}&newsTitle=${newsTitle}" class="btn btn-primary">显示全部</a>
            </form>
    </div><!--main-top-->

<div class="panel panel-info">
  <div class="panel-heading">
  		<div class="pin">
  		   <form class="form-inline" id="optionFormId" method="post" >
            <input type="hidden" name="title" value="${title}" >
               <div class="form-group">
                     <select id="batchOperateType" class="form-control">
                         <option value="">批量操作</option>
                         <option value="publish" selected="selected">发布</option>
                         <option value="draft">草稿</option>
                         <option value="delete">删除</option>
                     </select>
               </div>
               <div class="btn-group">
         		<button id="batchOperateBtn" type="button" class="btn btn-default">确认</button>
               </div>
             </form>
             <!-- 
          <button type="button" class="btn btn-primary" onclick="MyFun.add('${parentId}','${newsTitle}');" ><span class="glyphicon glyphicon-plus"></span>添加资讯</button>
           -->
        </div>
  </div>
   
   	    <ol class="breadcrumb">
   	      <li><a href="javascript:MyFun.search('','');">所有资讯</a></li>
	    </ol>
	    
   
   <table class="table table-hover">
     <thead>
       <tr>
         <th class="th-checkbox">
                  	<input type="checkbox" id="check-btn" class="tag" 
          	          data-container="body" 
				      data-title="<h5>友情提示</h5>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h5>请选择要删除的资讯</h5></div>"
          	>
         </th>
         <th class="th-sorting">ID</th>
         <th class="th-name">资讯标题</th>
         <th width="100">资讯分类</th>
         <th width="100">资讯来源</th>
         <th width="100">查看</th>
         <th width="150">封面</th>
         <th width="65">状态</th>
         <th class="th-time">发布日期</th>
       </tr>
     </thead>
     
       <c:set var="pagingUrl" value="${ctx}/content/news/list?categoryId=${categoryId}&newsTitle=${newsTitle}"/>
		  <c:set var="nextUrl" value=""/>
		  <c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
		  <c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
		  </c:if>
            <tbody id="moreListDiv" nextUrl="${nextUrl}" class="class-list">
			<c:forEach items="${apiRsp.results}" var="item">
              <tr>
                <td><input type="checkbox" name="list-checkbox" value="${item.id}" /></td>
                <td>${item.id}</td>
                <td>
               <strong>${item.newsTitle}</strong>
               <c:if test="${item.isTop==1}">
               	<span class="label label-success">顶</span>
               </c:if>
               <br/>
               <div class="text-muted">${item.newsSummary}</div>
             	<span class="row-actions">
             	    <a target="_blank" href="http://ios-api.ccplay.com.cn/news/detail/${item.id}"><span class="glyphicon glyphicon-eye-open"></span>预览</a>
					<a href="javascript:MyFun.edit('${item.id}');"><span class="glyphicon glyphicon-edit"></span>编辑</a>
					 <c:choose>
		          		<c:when test="${item.isTop==1}"><a href="javascript:MyFun.changeTop('${item.id}',0);"><span class="glyphicon glyphicon-arrow-down"></span>取消置顶</a></c:when>
		          		<c:otherwise><a href="javascript:MyFun.changeTop('${item.id}',1);"><span class="glyphicon glyphicon-arrow-up"></span>置顶</a></c:otherwise>
		          	</c:choose>
		          	<c:if test="${not empty item.packageId}">
			          	<a href="javascript:MyFun.promotionGame('${item.news_promotion_game_id}');"><span class="glyphicon glyphicon-arrow-down"></span>取消推广游戏</a>
		          	</c:if>
				</span>
                </td>
                <td>${item.categoryName}</td>
                <td>${item.source}</td>
                <td>${item.visitsnum}</td>
                <td>
                  <c:if test="${not empty item.newsCover }">
              	 	<img class="cover"  src="${item.coverStaticUrl}"   />
              	 </c:if>
                </td>
                <td>
		             <c:choose>
		          		<c:when test="${item.status=='publish'}"><span class="glyphicon glyphicon-ok-sign text-success" title="已发布"></span></c:when>
		          		<c:otherwise><span class="glyphicon glyphicon-remove-sign text-danger" title="草稿"></span></c:otherwise>
		          	</c:choose>
                </td>
                <td>${item.updatedDatetime}</td>
              </tr> 
			</c:forEach>        
            </tbody>
   </table>

</div><!--panel-info-->


    <c:if test="${not empty nextUrl}">
	    <div class="load_more">
	    	<button id="loadingDiv" style="display:none" type="button" class="btn btn-default btn-block loading-btn">Loading...</button>
	    	<button id="moreDiv" style="display:" type="button" class="btn btn-default btn-block loading-btn" data-loading-text="Loading..." autocomplete="off">加载更多</button>
	    </div>
    </c:if>
    
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="apiRsp" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${pagingUrl}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div><!--main-bottom-->
 
 
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>

<script>
$(function(){
	
	if("${status}"!=null && "${status}"!=""){
		$("#status").val("${status}");
	}
	
	$("#moreDiv").loadingMore({
		dataDivId:'moreListDiv',
		successCallback:function(){
			//bindInitEvents();
		}
	});	
	
	
	/*全选 取消全选	*/	
	var check_btn = document.getElementById("check-btn");
	var check_name = document.getElementsByName("list-checkbox");
	check_btn.onclick = function(){
		for(var i=1; i<=check_name.length; i+=1){
			if(check_name[i-1].checked){
				check_name[i-1].checked = false;
			}else{
				check_name[i-1].checked = true;
			}
		}
	};

	
	
	$("#batchOperateBtn").click(function(){
		var batchOperateType=$("#batchOperateType").val();
		var check_name = document.getElementsByName("list-checkbox");
		var idArr=new Array();
		for(var i=0;i<check_name.length;i++){
			if(check_name[i].checked){
				idArr.push(check_name[i].value);
			}
		}
		
		if(batchOperateType=="delete" && idArr.length>0){
			ajaxSubmit("${ctx}/content/news/delete",{ids:idArr.join(",")},reload,"删除成功！","确认批量删除？",null);
		}else{
			ajaxSubmit("${ctx}/content/news/changeStatus",{ids:idArr.join(","),status:batchOperateType},reload,"操作成功!","",null);
		}
	});
});

var MyFun = (function(){
	return{
		
		add : function(parentId,title) {
			if(parentId!='' && parentId>0){
				parent.addTab("添加["+title+"]的子资讯","${ctx}/content/news/create?parentId="+parentId);
			}else{
				parent.addTab("添加主资讯","${ctx}/content/news/create?parentId="+parentId);
			}
			
		},
		
		edit : function(id) {
			parent.addTab("编辑资讯("+id+")","${ctx}/content/news/edit?id="+id);
		},
		
		changeTop : function(id,isTop) {
			ajaxSubmit("${ctx}/content/news/changeTop",{id:id,isTop:isTop},reload,"操作成功!","",null);
		},
		
		promotionGame : function(id) {
			ajaxSubmit("${ctx}/content/news/promotionGameDelete",{id:id},reload,"操作成功!","",null);
		}
		

	};
})();

</script>

</body>
</html>




