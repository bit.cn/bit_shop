<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>发布资讯</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>
<body>

<div class="main add-announcement">
<div class="main-top">    
<h3>发布资讯</h3>    
</div><!--main-top-->
    

<form id="editForm" action="${ctx}/content/news/save" method="post">
<input type="hidden" name="id" value="${news.id}">
<input type="hidden" id="newsCover" name="newsCover" value="${news.newsCover}">
<input type="hidden" name=news_content_category_id value="${news.news_content_category_id}">
<input type="hidden" name="old_isTop" value="${news.isTop}">
<input type="hidden" id="cover_file_size" name="cover_file_size" value="">
<input type="hidden" id="cover_file_md5" name="cover_file_md5" value="">
	
  <div class="panel panel-info">
    <div class="panel-heading">*认真核对信息</div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel">
      <table class="table">
        <tbody>
           <tr id="lotteryDiv" >
            <th class="active"><strong class="necessary">*</strong>资讯分类</th>
            <td>
              <div class="row">
                <div class="col-xs-3">
					 <select name="categoryId" id="categoryId" required="required" class="form-control">
					    <option value="">---请选择---</option>
					    <c:forEach items="${newscategorylist}" var="obj">
					        <option value="${obj.id}" <c:if test="${obj.id == news.categoryId}"> selected="selected" </c:if> >${obj.categoryName}</option>
					    </c:forEach>
					</select>
			</div>
			</div>
            </td>
          </tr>
          <tr>
            <th class="active" width="15%"><strong class="necessary">*</strong>资讯标题</th>
            <td width="85%">
            <div class="input-group">
              <input type="text" class="form-control" id="NewsTitle"  name="newsTitle" value="${news.newsTitle}" required="required" >
              <span class="input-group-btn">
                <input type="text" id="color" name="bgColor"  class="form-control" style="width:95px" placeholder="无颜色" title="双击取消颜色"/>
              </span>
            </div>
            </td>
          </tr>
          <tr>
            <th class="active"><strong class="necessary"> </strong>资讯摘要</th>
            <td>
				<textarea class="form-control" rows="3" name="newsSummary">${news.newsSummary}</textarea>
            </td>
          </tr>
          <tr>
            <th class="active"><strong class="necessary"> </strong>新闻封面</th>
            <td style="height:100px;">
			   <button id="chooseCoverBtn" type="button" class="btn btn-primary loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-plus"></span> 选择封面</button>
			   <div id="coverPreviewDiv" style="margin-top:10px">
				   <c:if test="${not empty news.newsCover}">
				   	<a href="${news.coverStaticUrl}" target="_blank"><img src='${news.coverStaticUrl}' width='450' height='215'></a>
				   </c:if>
			   </div>
            </td>
          </tr>
          <tr>
            <th class="active"><strong class="necessary">*</strong>状态</th>
            <td>
                  <label class="radio-inline">
                    <input type="radio" name="status" id="status1" value="draft" checked> 草稿
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="status" id="status2" value="publish"> 定稿（直接发布)
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="isTop" id="isTop" value="1"> 置顶
                  </label>
            </td>
          </tr>
          <tr>
            <th class="active"><strong class="necessary">*</strong>资讯来源</th>
            <td>
            <c:if test="${news.source!='' and news.source!=null}">
            	 <input type="text" id="source" name="source"  class="form-control" value="${news.source}" required="required" placeholder="资讯来源" />
            </c:if>
            <c:if test="${news.source=='' || news.source==null}">
            	 <input type="text" id="source" name="source"  class="form-control" value="虫虫游戏" required="required" placeholder="资讯来源" />
            </c:if>
           
            </td>
          </tr>          
          <tr>
            <th class="active">&nbsp;&nbsp;关联应用</th>
            <td>
           <div class="row">
                  <div class="col-xs-3">
                    <div class="input-group">
              	<input type="hidden" class="form-control" id="package_id" name="packageId" value="${news.packageId}">
          	    <input class="form-control" id="package_title" name="packageTitle" value="${news.packageTitle}" placeholder="应用名称" >
  				<input type="hidden" id="package_name" name="packageName" value="${news.packageName}"  class="form-control"  >
              <span class="input-group-btn">
                <button class="btn btn-default" id="package_click" type="button"><span class="glyphicon glyphicon-plus"></span> 关联应用</button>
              </span>
            </div>
                  </div>
                </div>
            </td>
          </tr>
          <tr>
            <th class="active"><strong class="necessary">*</strong>详细内容</th>
            <td>
            	<textarea class="form-control" rows="15" name="newsContent">${news.newsContent}</textarea>
            </td>
          </tr>
        </tbody>
        
        
      </table>

    </div>
  </div>
  
  
    

<div class="submit">
      <button type="submit" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-ok"></span> 确认提交</button>
</div>
</form>



</div><!--main end-->

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript" src="${ctx}/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="${ctx}/js/kindeditor-4.1.10/kindeditor-min.js"></script>
<script type="text/javascript" src="${ctx}/js/kindeditor-4.1.10/lang/zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/js/kindeditor.js"></script>
<script type="text/javascript">
var editor=new EditorComponent('textarea[name="newsContent"]');
editor.initComponent({
	cdnListUrl:'${ctx}/system/cdn/list?contentTypeId=67&chooseCallback=chooseImageCallback&limitFileType=image',
	filterMode : false
});


function chooseImageCallback(id,fileType,fileKey,fileName,cdnUrl){
	editor.insertHtml("<img src='"+cdnUrl+"' data-ke-src='"+cdnUrl+"'>");
	closeDialog();
}
function returnToList(){
	parent.closeThenAddTab("所有资讯","${ctx}/content/news/list",true);
}
function choosePackageCallback(id,name,title){
	$("#package_id").val(id);
	$("#package_name").val(name);
	$("#package_title").val(title);
	closeDialog();
}
function chooseCoverCallback(id,fileType,fileKey,fileName,cdnUrl){
	$("#newsCover").val(fileKey);
	$("#coverPreviewDiv").html("<img src='"+cdnUrl+"' width='450' height='215'>");
	closeDialog();
}
$(function(){
	$("#package_click").click(function(){
		openDialog({
			frame:true,
			title:"选择应用",
		    height:800,
		    width:1000,
			url:"${ctx}/content/package/listForChoose?chooseCallback=choosePackageCallback"
		});
	});
    $("#chooseCoverBtn").on("click",function(){
    	openCommonDialog("选择封面","${ctx}/system/cdn/list?contentTypeId=75&chooseCallback=chooseCoverCallback&limitFileType=image");
    });
    
    
	$("#editForm").bind('submit', function(event) {
		$('#editForm textarea[name="newsContent"]').val(editor.html());
		ajaxFormSubmit(this,returnToList,"提交成功！",null);
		event.preventDefault();
	});	
	
	  /*双击取消颜色*/
	  $("#color").dblclick(function(){
		$("#NewsTitle").css({"color":"#333","background-color":""});
		$("#color").val("");
	  });
	
	
	  var color = '${news.bgColor}';
	  if(color!=''){
			$("#NewsTitle").css({"color":"#fff","background-color":"${news.bgColor}"});
			$("#color").val("${news.bgColor}");
	  }else{
			$("#NewsTitle").css({"color":"#333","background-color":""});
			$("#color").val("");
	  }
	  
	  
	  (function(){
		  
			$("input[name='status']").each(function (){
			 	if($(this).val() == "${news.status}"){
			 		$(this).click();
			 	}     
			});
			 
			$("input[name='isTop']").each(function (){
	      		  var indexCode = "${news.isTop}".indexOf($(this).val());
	      		  if(indexCode>-1){
	      		  	 $(this).attr("checked","checked"); 
	      		  }
	       });
		  
	  }());
});
</script>


		
		
<script>
var editor;
KindEditor.ready(function(K) {
	var colorpicker;
	K('#color').bind('click', function(e) {
		e.stopPropagation();
		if (colorpicker) {
			colorpicker.remove();
			colorpicker = null;
			return;
		}
		var colorpickerPos = K('#color').pos();
		colorpicker = K.colorpicker({
			x : colorpickerPos.x,
			y : colorpickerPos.y + K('#color').height(),
			z : 19811214,
			selectedColor : 'default',
			click : function(color) {
				K('#color').val(color);
				K('#NewsTitle').css({"color":"#fff","background-color":color});
				colorpicker.remove();
				colorpicker = null;
			}
		});
	});
	
	K(document).click(function() {
		if (colorpicker) {						
			colorpicker.remove();
			colorpicker = null;
		}
	});
});
</script>
</body>
</html>