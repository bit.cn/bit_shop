<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>破解包批量签名</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>
<body>

<div class="main add-announcement">
<div class="main-top">    
<h3>破解包批量签名</h3>    
</div><!--main-top-->
    

<form id="editForm" action="${ctx}/content/packageversionCertificate/batchSignning" method="post">
  <div class="panel panel-info">
    <div class="panel-heading">*认真核对信息</div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel">
    <table class="table">
        <tbody>
           <tr>
            <th class="active" width="15%"><strong class="necessary">*</strong>选择签名对象</th>
            <td  width="85%" >
				<select class="form-control" name="packageversion_certificate_status" style="width:50%">
				 <option value="">所有游戏包</option>
				 <option value="no_sign">未签名的游戏包</option>
				 <c:forEach items="${packageversionCertificateMap }" var="obj">
				 	 <option value="${obj.key}">${obj.value}(游戏包)</option>
				 </c:forEach>
				</select>
            </td>
          </tr>
          <tr>
            <th class="active"  ><strong class="necessary">*</strong>选择签名证书</th>
            <td>
              <div style="float: left;width:50%;">
            	<p><input id="certificateId" name="certificateId"  class="form-control"  placeholder="证书编号" readonly="readonly"></p> 
            	<p><input id="abbreviation" name="abbreviation" class="form-control" placeholder="证书简称" readonly="readonly"></p>
  				<p><input id="certificateName" name="certificateName"  class="form-control" placeholder="证书名称"  required="required" ></p>
  				</div>
            </td>
          </tr>
           <tr>
            <td></td>
            <td>
              <button id="chooseCertificateBtn" type="button" class="btn btn-primary loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-plus"></span> 选择证书</button>
            </td>
          </tr>
          <tr>
            <th class="active"><strong class="necessary"></strong>覆盖CDN上文件</th>
            <td>
				<input type="checkbox" value="1" name="remove_remote_exists">是 (覆盖七牛已存在的ipa文件)
            </td>
          </tr>
          <tr>
            <th class="active"><strong class="necessary"></strong>删除本地签名后文件</th>
            <td>
				<input type="checkbox" value="1" name="remove_local_signed" checked="checked">是  (删除签名后生成的ipa文件)
            </td>
          </tr>
        </tbody>
      </table>

    </div>
  </div>
    
<div class="submit">
      <button type="submit" class="btn btn-success loading-btn" id="sub_btn"  data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-ok"></span> 批量签名任务开始 </button>
</div>

</form>



</div><!--main end-->

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>

<script type="text/javascript">
function chooseCertificateCallback(certificateId,certificateName,abbreviation){
	$("#certificateId").val(certificateId);
	$("#certificateName").val(certificateName);
	$("#abbreviation").val(abbreviation);
	closeDialog();
}

$(function(){
	
	$("#editForm").bind('submit', function(event) {
		ajaxFormSubmit(this,closeDialog,"批量签名任务提交成功！","确定要执行【批量签名】吗？");
		event.preventDefault();
		//$("#sub_btn").attr("disabled","disabled");
	});	
	
	
	$("#chooseCertificateBtn").on("click",function(){
    	var url = "${ctx}/operation/certificate/chooseList?status=publish&tagType=crack_package&chooseCallback=chooseCertificateCallback";
    	openDialog({frame:true,title:"选择签名证书", height:650,  width:1200,	url:url	});
	});
});
</script>
</body>
</html>