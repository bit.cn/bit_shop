<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>版本签名信息</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>

<div class="main add-game">

    <table class="table table-hover" id="table">
      <thead>
        <tr>
          <th>版本ID</th>
          <th>版本名称</th>
          <th>证书编号</th>
          <th>渠道代码</th>
          <th>bundleId</th>
          <th>创建日期</th>
          <th>更新日期</th>
          <th>签名状态</th>
        </tr>
      </thead>
      <tbody>
      	<c:forEach items="${apiRsp.results}" var="item">
      		<tr>
      			<td>${item.packageversiongId}</td>
      			<td>${item.versionName}</td>
      			<td>${item.certificateId}</td>
      			<td>${item.chanleCode}</td>
      			<td>${item.bundleId}</td>
      			<td>${item.createTime}</td>
      			<td>${item.updateTime}</td>
      			<td>
                	<c:choose>
	                	<c:when test="${item.status eq 'sign_start'}">
	                	  <span class="label label-info">签名开始</span>
	                		
	                	</c:when>
	                	<c:when test="${item.status eq 'sign_error'}">
	                		<span class="label label-danger">签名异常</span>
	                	</c:when>
	                	<c:when test="${item.status eq 'upload_error'}">
	                		<span class="label label-danger">上传异常</span>
	                	</c:when>
	                	<c:when test="${item.status eq 'success'}">
	                		<span class="label label-success">签名成功</span>
	                	</c:when>	                		                	
	                	<c:otherwise>
	                	<span class="label label-warning">未知错误</span>
	                	</c:otherwise>
                	</c:choose>      
                				
      			</td>
      		</tr>
      	</c:forEach>
      </tbody>
   </table>

</div><!--main end-->

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
</body>
</html>
