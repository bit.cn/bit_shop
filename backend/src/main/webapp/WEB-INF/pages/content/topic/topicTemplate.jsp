<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>发布专题</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>
<body>

<form role="form" id="topicFormId" action="${ctx}/content/topic/editTopic" >
<input type="hidden" name="id" value="${topic.id}" class="form-control">   
<input type="hidden" name="parentId" value="${topic.parentId}" class="form-control"> 
<input type="hidden" name="oldCover" value="${topic.oldCover}" class="form-control">  
      <div class="modal-body">      


<div class="form-group row">
      <div class="col-xs-6">
            <label for="" class="control-label">名称</label>
            <input type="text" name="name" value="${topic.name}" class="form-control" placeholder="专题名称不能为空" required autofocus>       		
      </div>
</div>
        <div class="row">
          <div class="col-xs-6">
                <label for="" class="control-label"><strong class="necessary">*</strong>封面</label>
			   <button id="chooseCoverBtn" type="button" class="btn btn-primary loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-plus"></span> 选择封面</button>
			   <div id="coverPreviewDiv">
			   	 <c:if test="${not empty topic.cover}">
			     	<img width='520' height='215' src="${topic.staticCoverUrl}">
			     </c:if>
			   </div>  
            	<input type="hidden" name="cover" id="cover" class="form-control" value="${topic.cover}">
            	<input type="hidden" name="workspace" class="form-control" value="">
            	<input type="hidden" name="fileSize" id="cover_file_size" class="form-control" value="">
            	<input type="hidden" name="fileMd5" id="cover_file_md5" class="form-control" value="">
            	<input type="hidden" name="mimeType" id="mimeTypeId" class="form-control" value="">
          </div>
    </div>

<div class="form-group row">
      <div class="col-xs-6">
            <label for="" class="control-label">内容的类型</label>
            <select class="form-control" name="contentType" required>
				<c:forEach items="${topic.typeList}" var="type">
		 				<option value="${type.code}"  <c:if test="${not empty type.selected}">selected="selected"</c:if> >${type.name}</option>
		         </c:forEach>
            </select>	
      </div>
</div>


<div class="form-group row">
            <label for="" class="control-label">限制渠道</label>
            <select class="form-control" id="limitChannel" name="limitChannel">
				<option value=""></option>
				<c:forEach items="${channelMap}" var="channel">
					<option value="${channel.key}">${channel.value}</option>
		        </c:forEach>
            </select>
</div>

<div class="form-group row">
      <label for="" class="control-label">别名</label>
      <input type="text" name="slug" value="${topic.slug}" class="form-control" placeholder="别名不能为空" required>       
      <p class="text-muted">是对于 URL 友好的一个别称。它通常为小写并且只能包含字母，数字和连字符（-）</p>		
</div>

<div class="form-group">
  <label for="" class="control-label">描述</label>
  <textarea  name="summary" oninput="this.style.height = this.scrollHeight + 'px';" onpropertychange="this.style.height = this.scrollHeight + 'px';" class="form-control description" style="height: 36px;">${topic.summary}</textarea>
</div>

   </div>
   <div class="modal-footer">
   <button type="button" class="btn btn-default" onclick="javascript:closeDialog();">关闭</button>
   <button type="submit"  class="btn btn-success loading-btn"><span class="glyphicon glyphicon-ok"></span>提交</button>
   </div>
</form>
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript" src="${ctx}/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
function chooseCoverCallback(id,fileType,fileKey,fileName,cdnUrl){
	$("#cover").val(fileKey);
	$("#coverPreviewDiv").html("<img src='"+cdnUrl+"' width='520' height='215'>");
	closeDialog();
}
function refreshList(){
	parent.location.href='${ctx}/content/topic/topicList?parent_id=${topic.parentId}';
}
$(document).ready(function() {
	if("${topic.limitChannel}"!=""){
		$("#limitChannel").val("${topic.limitChannel}");
	}
	$("#topicFormId").submit(function(event) {
		ajaxFormSubmit(this,refreshList,null,null);
		event.preventDefault();
	});
    $("#chooseCoverBtn").on("click",function(){
    	openCommonDialog("选择封面","${ctx}/system/cdn/list?contentTypeId=20&chooseCallback=chooseCoverCallback&limitFileType=image");
    });
});
</script>
</body>
</html>


