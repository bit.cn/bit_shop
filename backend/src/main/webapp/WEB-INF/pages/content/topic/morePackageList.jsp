<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<c:forEach items="${packageList.results}" var="item">
        <tr id="${item.id}">
          <td><input type="checkbox" name="list-checkbox" value="${item.id}"></td>
          <td><input type="number"  value="${item.ordering}" class="form-control"/></td>
          <td>${item.package_id}</td>
          <td class="td-name">
          		<img src="${item.icon}" class="img-rounded game-icon"/><strong>${item.title}</strong><br/>
          		<!-- <span class="en-name">Subway Surfers</span> -->
          		<span class="row-actions">
          			<a href="javascript:editPackage('应用','${item.package_id}','${item.version_id}');"><span class="glyphicon glyphicon-edit"></span>编辑</a>
          			<a href="${item.preview_url}" target="_blank"><span class="glyphicon glyphicon-eye-open"></span>预览</a>
          			<a href="${item.download_url}" target="_blank"><span class="glyphicon glyphicon-floppy-save"></span>下载</a>
          		</span>
          </td>
          <td>${item.version_name} <br/> ${item.package_name}</td>
          <td>${item.fileSize} <br/> ${item.award_coin}金币</td>
          <td>${item.tags_text}<br/>${item.main_category_names}</td>
          <td>${item.released_datetime}</td>
        </tr>
</c:forEach>