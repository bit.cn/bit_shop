<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>专题下的应用</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">
</head>

<body>
<div class="main game-list">

	<div class="main-top">
    
            <h3>${topic_name} <small>共<span>${authorList.count}</span>个</small></h3>
            
            <form id="searchForm" class="form-inline pull-right search-box" role="form" action="${ctx}/content/topic/authorList" >
            	<input type="hidden" name="topic_id" value="${topic_id}" >
            	<input type="hidden" name="topic_name" value="${topic_name}" >
                <input type="text" class="form-control" name="author_name" value="${author_name}" placeholder="名称">
                <button type="submit" class="btn btn-default">搜索</button>
                <a href="${ctx}/content/topic/authorList?topic_id=${topic_id}&topic_name=${topic_name}" class="btn btn-primary">显示全部</a>
            </form>
            
    </div><!--main-top-->
    
    
    
<div class="panel panel-info">
  <div class="panel-heading">
  <!--查看 开始-->
  <div class="pin">
           <form class="form-inline" id="optionFormId" >
            <input type="hidden" name="topic_id" value="${topic_id}" >
            <input type="hidden" name="topic_name" value="${topic_name}" >
           <input type="hidden"   name="topicItem" id="topicItemId" value="" >
               <div class="form-group">
                     <select class="form-control" id="optionTypeId">
                       <option value="order" selected="selected" >排序</option>
                       <option value="del">移出</option>
                     </select>
               </div>
               <div class="btn-group">
                 <button type="submit" class="btn btn-default" id="submitButtonId"
                      data-container="body" 
				      data-title="<h4>友情提示</h4>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h3>成功删除${message}个应用</h3></div>" 
                 >确认</button>
               </div>
             </form>
          <button type="button" class="btn btn-primary" id="addAppButtonId" ><span class="glyphicon glyphicon-plus"></span>添加厂商</button>
   </div>
  </div>


    <ol class="breadcrumb">
      <li><a href="${ctx}/content/topic/topicList"> 专题管理</a></li>
      <li>${topic_name}</li>
    </ol>
    
	<table class="table" id="table_drag" style="display: none;"></table>

    <table class="table table-hover" id="table">
      <thead>
        <tr>
          <th class="th-check">
          	<input type="checkbox" id="check-btn" class="tag" 
          	          data-container="body" 
				      data-title="<h5>友情提示</h5>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h5>请选择要删除的应用</h5></div>"
          	>
          </th>
		        <th class="th-sorting">排序</th>
				<th class="th-name">名称</th>
				<th width="150">LOGO</th>
				<th width="150">封面</th>
        </tr>
      </thead>
        <c:set var="nextUrl" value=""/>
      	<c:if test="${not empty authorList and authorList.curPage!=authorList.totalPages}">
      	<c:set var="nextUrl" value="${ctx}/content/topic/authorList?topic_id=${topic_id}&topic_name=${topic_name}&author_name=${author_name}&page=${authorList.curPage+1}"/>
      	</c:if>
		<tbody id="morePackageListDiv" nextUrl="${nextUrl}">
      	<jsp:include page="/WEB-INF/pages/content/topic/moreAuthorList.jsp" flush="true"/>
      </tbody>
    </table>

</div><!--panel-info-->
    <c:if test="${not empty nextUrl}">
	    <div class="load_more">
	    	<button id="loadingDiv" style="display:none" type="button" class="btn btn-default btn-block loading-btn">Loading...</button>
	    	<button id="moreDiv" style="display:" type="button" class="btn btn-default btn-block loading-btn" data-loading-text="Loading..." autocomplete="off">加载更多</button>
	    </div>
    </c:if>
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="authorList" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${ctx}/content/topic/authorList?topic_id=${topic_id}&topic_name=${topic_name}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div>

</div><!--main end-->	
<div id="dialogId"></div>

<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.pin.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/common.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.tableDnD.js" type="text/javascript"></script>
<script src="${ctx}/js/dialog.js"></script>
<script type="text/javascript">
function selectedPackage(submit){
	var check_name = document.getElementsByName("list-checkbox");
	var packageId="";
	if(submit){
		for(var i=0; i<check_name.length; i++){
			if(check_name[i].checked){
				packageId=packageId+check_name[i].value+",";
			}
		}
		$("#topicItemId").val(packageId);
	}else{
		var check=document.getElementById("check-btn").checked;
		for(var i=0; i<check_name.length; i++){
			check_name[i].checked = check;
		}
	}
	if(submit&&packageId==''){
		$("#check-btn").popover('show');
		setTimeout(function(){
			$("#check-btn").popover('destroy');
		},2000);
		return false;
	}else{
	    return true;
	}
}
function orderTopicItem(){
	var topicItem="";
	var table = document.getElementById("table");
	var tr = table.getElementsByTagName("tr");
	for (var i = 1; i < tr.length; i++) {
		var rowid = tr[i].getAttribute("id")+","+i;
		topicItem=topicItem+rowid+"#";
	}
	$("#topicItemId").val(topicItem);
	if(topicItem==''){
		return false;
	}else{
	    return true;
	}
}
$(document).ready(function() {
	$("#table").tableDnD();
	/*锁定操作栏*/
	$(".pin").pin();
	$("#moreDiv").loadingMore({
		dataDivId:'morePackageListDiv',
		successCallback:function(){
			$(".pin").pin();
			$("#table").tableDnD();
		}
	});
	$("#addAppButtonId").on("click",function(){
		var dialog =new dialogBox("dialogId");
		dialog.initComponent({url:"${ctx}/content/topic/addAuthorList?topic_id=${topic_id}&topic_name=${topic_name}",frame:true,title:'添加厂商',height:770,width:1000});
		dialog.show();
	});
	$("#optionFormId").submit(function(){
		if($("#optionTypeId").val()=="order"){
			$(this).attr("action","${ctx}/content/topic/authorTopicItemOrder");
			return orderTopicItem();
		}else{
			$(this).attr("action","${ctx}/content/topic/delTopicAuthorItem");
			return selectedPackage(true);
		}
		return false;
	});
	$("#check-btn").on("click",function(){
		selectedPackage(false);
	});
	if("${searchType}"!=''){
		$("#searchType").val("${searchType}");	
	}
	if("${message}"!=''){
		$("#submitButtonId").popover('show');
		setTimeout(function(){
			$("#submitButtonId").popover('destroy');
		},2000);
	}
});
</script>
</body>
</html>
