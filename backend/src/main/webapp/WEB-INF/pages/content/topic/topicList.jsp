<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
	<title>专区管理</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">
     <link rel="stylesheet" type="text/css" href="${ctx}/js/jQueryFileUpload/jquery.fileupload.css">

</head>
<body>
	
<div class="main debris">
	<div class="main-top">    
            <h3>
            ${(not empty parent_id)?(topic_name):'专区管理'}
            <small>共<span>${topicList.count}</span>个</small></h3> 
            <form class="form-inline pull-right search-box" role="form" action="${ctx}/content/topic/topicList" >
            	<div class="form-group" style="margin-left:20px;">
            		<span>限制渠道</span>
					<select class="form-control" id="limit_channel" name="limit_channel">
						<option value="">所有</option>
						<c:forEach items="${channelMap}" var="channel">
							<option value="${channel.key}">${channel.value}</option>
				        </c:forEach>
					</select>
            	</div>
            	<div class="form-group" style="margin-left:20px;">                        
                	<input type="text" class="form-control" name="name" value="${param_name}" placeholder="名称">
                </div>
                
                <button type="submit" class="btn btn-default">搜索</button>
                <a href="${ctx}/content/topic/topicList?parent_id=${parent_id}" class="btn btn-primary">显示全部</a>
            </form>
    </div><!--main-top-->

<div class="panel panel-info">
  <div class="panel-heading">
  		<div class="pin">
          <form class="form-inline" id="optionFormId" method="post" >
            <input type="hidden" name="name" value="${param_name}" >
            <input type="hidden" name="parent_id" value="${parent_id}" >
            <input type="hidden" name="status" value="" id="statusId" >
           <input type="hidden" name="topicId" id="topicId" value="" >
               <div class="form-group">
                     <select class="form-control" id="optionTypeId">
                            <option value="published" selected="selected">发布</option>
                            <option value="draft">草稿</option>
                            <option value="del">删除</option>
                     </select>
               </div>
               <div class="btn-group">
                 <button type="submit" class="btn btn-default" id="submitButtonId"
                      data-container="body" 
				      data-title="<h4>友情提示</h4>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h3>${message}</h3></div>" 
                 >确认</button>
               </div>
             </form>
             
          <button type="button" class="btn btn-primary" onclick="add();" ><span class="glyphicon glyphicon-plus"></span>添加专区</button>
        </div>
  </div>
  <ol class="breadcrumb">
      <li><a href="${ctx}/content/topic/topicList">专区管理</a></li>
      <c:if test="${not empty parent_id}">
      	<li>${topic_name}</li>
      </c:if>
  </ol>
   <table class="table table-hover">
     <thead>
       <tr>
         <th class="th-checkbox">
                  	<input type="checkbox" id="check-btn" class="tag" 
          	          data-container="body" 
				      data-title="<h5>友情提示</h5>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h5>请选择要操作的项目</h5></div>"
          	>
         </th>
         <th class="th-sorting">排序</th>
         <th width="150">封面</th>
         <th class="th-name">名称</th>
         <th width="200">别名</th>
         <th width="100">限制渠道</th>
         <!-- <th width="100">站点</th> -->
         <th width="100">应用数</th>
         <th width="50">状态</th>
         <th class="th-time">更新日期</th>
       </tr>
     </thead>
     <tbody class="class-list">
     	<jsp:include page="/WEB-INF/pages/content/topic/moreTopicList.jsp" flush="true"/>
     </tbody>
   </table>

</div><!--panel-info-->
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="topicList" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${ctx}/content/topic/topicList?parent_id=${parent_id}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div>
</div><!--main end-->
 
 
<div id="dialogId"></div>

<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.pin.js" type="text/javascript"></script>
<script src="${ctx}/js/jQueryFileUpload/jquery.ui.widget.js" type="text/javascript"></script>
<script src="${ctx}/js/jQueryFileUpload/jquery.iframe-transport.js" type="text/javascript"></script>
<script src="${ctx}/js/jQueryFileUpload/jquery.fileupload.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/common.js" type="text/javascript"></script>
<script src="${ctx}/js/dialog.js"></script>
<script>

function add(){
	openDialog({
		frame:true,
		title:"添加专题",
	    height:800,
	    width:900,
		url:"${ctx}/content/topic/getTopic?parentId=${parent_id}"
	});
}
function edit(id){
	openDialog({
		frame:true,
		title:"编辑专题",
	    height:800,
	    width:900,
		url:"${ctx}/content/topic/getTopic?parentId=${parent_id}&id="+id
	});	
}
function selectedTopic(submit){
	var check_name = document.getElementsByName("list-checkbox");
	var packageId="";
	if(submit){
		for(var i=0; i<check_name.length; i++){
			if(check_name[i].checked){
				packageId=packageId+check_name[i].value+",";
			}
		}
		$("#topicId").val(packageId);
	}else{
		var check=document.getElementById("check-btn").checked;
		for(var i=0; i<check_name.length; i++){
			check_name[i].checked = check;
		}
	}
	if(submit&&packageId==''){
		$("#check-btn").popover('show');
		setTimeout(function(){
			$("#check-btn").popover('destroy');
		},2000);
		return false;
	}else{
	    return true;
	}
}

$(function(){
	/*锁定操作栏*/
	$(".pin").pin();
	/*全选 取消全选*/	
	$("#check-btn").on("click",function(){
		selectedTopic(false);
	});
	if("${limit_channel}"!=""){
		$("#limit_channel").val("${limit_channel}");
	}
	$("#optionFormId").submit(function(){
		if($("#optionTypeId").val()=="published"||$("#optionTypeId").val()=="draft"){
			$("#statusId").val($("#optionTypeId").val());
			$(this).attr("action","${ctx}/content/topic/updateTopicStatus");
			return selectedTopic(true);
		}else{
			$(this).attr("action","${ctx}/content/topic/delTopic");
			return selectedTopic(true);
		}
		return false;
	});
	if("${message}"!=''){
		$("#submitButtonId").popover('show');
		setTimeout(function(){
			$("#submitButtonId").popover('destroy');
		},2000);
	}
});

</script>

</body>
</html>




