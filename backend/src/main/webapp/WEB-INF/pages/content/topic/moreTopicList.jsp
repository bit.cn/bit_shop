<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
 <c:forEach var="topic" items="${topicList.results}">
              <tr>
                <td><input type="checkbox" name="list-checkbox" value="${topic.id}"></td>
                <td><input type="number"  value="${topic.ordering}" class="form-control"/></td>
                <td><c:if test="${not empty topic.cover}"><img class="cover" src="${topic.cover}"></c:if></td>
                <td><strong>${topic.name}</strong>
                	<div class="text-muted">${topic.summary}</div>
                	<span class="row-actions"><a href="http://android.ccplay.com.cn/collections/" target="_blank">
                	<span class="glyphicon glyphicon-eye-open"></span>预览</a>
			        <c:choose>
						<c:when test="${topic.contentType eq 'topic'}">
								<a href="${ctx}/content/topic/topicList?parent_id=${topic.id}&topic_name=${topic.name}" target="mainFrame"><span class="glyphicon glyphicon-th-list"></span>${topic.typeTitle}</a>
						</c:when>
						<c:when test="${topic.contentType eq 'author'}">
								<a href="${ctx}/content/topic/authorList?topic_id=${topic.id}&topic_name=${topic.name}" target="mainFrame"><span class="glyphicon glyphicon-th-list"></span>${topic.typeTitle}</a>
						</c:when>
						<c:otherwise>
								<a href="${ctx}/content/topic/packageList?topic_id=${topic.id}&parent_id=${parent_id}&name=${topic.name}&topic_name=${topic_name}&package_status=published"><span class="glyphicon glyphicon-th-list"></span>${topic.typeTitle}</a>
						</c:otherwise>
					</c:choose>

                	<a href="javascript:void(0);" onclick="edit(${topic.id});"><span class="glyphicon glyphicon-edit"></span>编辑</a>
		          	<%-- <a href="javascript:void(0);" onclick="publishcdn(${topic.id});"><span class="glyphicon glyphicon-edit"></span>同步SDN</a>
		          	<a href="javascript:void(0);" onclick="selectCdnSyncStatus(${topic.id});"><span class="glyphicon glyphicon-edit"></span>查看SDN状态</a> --%>
                	</span>
                </td>
                <td>${topic.slug}</td>
                <td><c:if test="${not empty topic.limitChannel}">${channelMap[topic.limitChannel]}</c:if></td>
                <%-- <td>${topic.site}</td> --%>
                <td>${topic.counter}</td>
                <td>

                	<c:if test="${topic.status eq 'draft'}">
                		<span title="草稿" class="glyphicon glyphicon-remove-sign text-danger"></span>
                	</c:if>
                	<c:if test="${topic.status eq 'published'}">
                		<span title="已发布" class="glyphicon glyphicon-ok-sign text-success"></span>
                	</c:if>
                </td>
                <td>${topic.updatedDatetime}</td>
              </tr>
</c:forEach>