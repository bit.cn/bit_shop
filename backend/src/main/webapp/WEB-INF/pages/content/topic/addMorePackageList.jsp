<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<c:forEach items="${packageList.results}" var="item">
        <tr>
          <td><input type="checkbox" name="list-checkbox" value="${item.package_id}"></td>
          <td>${item.package_id}</td>
          <td class="td-name">
          		<img src="${item.icon}" class="img-rounded game-icon"/><strong>${item.title}</strong><br/>
          		<span class="en-name">Subway Surfers</span>
          		<span class="row-actions"><a href="http://android.ccplay.com.cn/package/${item.package_id}" target="_blank"><span class="glyphicon glyphicon-eye-open"></span>预览</a></span>
          </td>
            <td>${item.version_name} <br/> ${item.package_name}</td>
            <td>${item.released_datetime}</td>
        </tr>
</c:forEach>