<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<c:forEach items="${authorList.results}" var="item">
        <tr id="${item.id}">
                <td><input type="checkbox" value="${item.id}" name="list-checkbox"></td>
                <td><input type="number" value="${item.ordering}" class="form-control"></td>
                <td><strong>${item.name}</strong>
                </td>
                <td><c:if test="${not empty item.logo}"><img class="logo" height="77px"width="150px"  src="${item.logo}"></c:if></td>
                <td><c:if test="${not empty item.cover}"><img class="logo" height="77px"width="150px" src="${item.cover}"></c:if></td>
           </tr>
</c:forEach>