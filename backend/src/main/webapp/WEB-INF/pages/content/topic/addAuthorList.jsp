<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>专题下的应用</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">
</head>

<body>
<div class="main game-list">

	<div class="main-top">
    
            <h3>可添加的厂商 <small>共<span>${authorList.count}</span>个</small></h3>
            
            <form class="form-inline pull-right search-box" role="form" action="${ctx}/content/topic/addAuthorList" >
            	<input type="hidden" name="topic_id" value="${topic_id}" >
            	<input type="hidden" name="topic_name" value="${topic_name}" >
                <input type="text" class="form-control" name="author_name" value="${author_name}" placeholder="关键词">
                <button type="submit" class="btn btn-default">搜索</button>
                <a href="${ctx}/content/topic/addAuthorList?topic_id=${topic_id}&topic_name=${topic_name}" class="btn btn-primary">显示全部</a>
            </form>
            
    </div><!--main-top-->
    
    
    
<div class="panel panel-info">
  <div class="panel-heading">
  <!--查看 开始-->
  <div class="pin">
  	<form  action="${ctx}/content/topic/addAuthorToTopic" id="submitFormId" >
  	<input type="hidden" name="topic_id" value="${topic_id}" >
  	<input type="hidden" name="topic_name" value="${topic_name}" >
  	<input type="hidden" name="authorId" id="packageId" value="" >
        <div class="btn-group">
          <button type="submit"  class="btn btn-default" id="submitButtonId"   
      data-container="body" 
      data-title="<h4>友情提示</h4>"
      data-toggle="popover" 
      data-html="true"
      data-content="<div><h3>成功添加<span>${addAuthorCounter}</span>个厂商</h3></div>" >加入到${topic_name}</button>
        </div>
	</form>
  </div>

  </div>
  
    <table class="table table-hover" id="table">
      <thead>
        <tr>
            <th class="th-check"><input type="checkbox" id="check-btn" class="tag" title="全选/反选"></th>
			<th class="th-name">名称</th>
			<th width="150">LOGO</th>
        </tr>
        
      </thead>
      <tbody>
        <jsp:include page="/WEB-INF/pages/content/topic/addMoreAuthorList.jsp" flush="true"/>
      </tbody>
    </table>
</div><!--panel-info-->
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="authorList" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${ctx}/content/topic/addAuthorList?topic_id=${topic_id}&topic_name=${topic_name}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div>
</div>



<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.pin.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function selectedPackage(submit){
	var check_name = document.getElementsByName("list-checkbox");
	var packageId="";
	if(submit){
		for(var i=0; i<check_name.length; i++){
			if(check_name[i].checked){
				packageId=packageId+check_name[i].value+",";
			}
		}
		$("#packageId").val(packageId);
	}else{
		var check=document.getElementById("check-btn").checked;
		for(var i=0; i<check_name.length; i++){
			check_name[i].checked = check;
		}
	}
	if(submit&&packageId==''){
		return false;
	}else{
	    return true;
	}
}
$(document).ready(function() {
	/*锁定操作栏*/
	$(".pin").pin();
	/*全选 取消全选*/	
	$("#check-btn").on("click",function(){
		selectedPackage(false);
	});
	$("#submitFormId").submit(function(){
		return selectedPackage(true);
	});

	if("${addAuthorCounter}"!=''){
		$("#submitButtonId").popover('show');
		parent.location.href='${ctx}/content/topic/authorList?topic_id=${topic_id}&topic_name=${topic_name}';
		//parent.location.reload();
	}
});

</script>
</body>
</html>
