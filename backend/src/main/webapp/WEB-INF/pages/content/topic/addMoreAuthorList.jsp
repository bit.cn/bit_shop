<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<c:forEach items="${authorList.results}" var="item">
        <tr id="${item.authorId}">
                <td><input type="checkbox" value="${item.authorId}" name="list-checkbox"></td>
                <td><strong>${item.name}</strong>
                </td>
                <td><img class="logo" height="77px"width="150px"  src="${item.logo}"></td>
           </tr>
</c:forEach>