<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>查看CDN同步状态</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>
<div class="main add-game">
	
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		  <div class="panel panel-info">
		    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse_${syncqueue.latest_item_id}" aria-expanded="true" aria-controls="collapse_${syncqueue.latest_item_id}">
		      <h4 class="panel-title">${topic.name}</h4>
		    </div>
		    <div id="collapse_${syncqueue.latest_item_id}" class="panel-collapse collapse in" role="tabpanel">
				<table class="table">
					<thead>
						<th width="5%">状态</th>
						<th width="10%">文件别名</th>						
						<th width="15%">最后提交时间</th>
						<th width="10%">最后提交结果</th>
						<th width="15%">最后反馈时间</th>
						<th width="10%">最后反馈结果</th>
						<th width="15%">原文件</th>
						<th width="15%">预览</th>
					</thead>
					<tbody>
							<tr>
								<td>
				                	<c:choose>
				                	<c:when test="${syncqueue.latest_fb_result eq 'SUCCESS'}">
				                		<span class="glyphicon glyphicon-ok-sign text-success" title="成功"></span>
				                	</c:when>
				                	<c:otherwise>
				                		<span class="glyphicon glyphicon-remove-sign text-danger" title="失败"></span>
				                	</c:otherwise>
				                	</c:choose>
								</td>
								<td>${topic.name}</td>
								<td><fmt:formatDate value="${syncqueue.latest_op_datetime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								<td>${syncqueue.latest_op_result}</td>
								<td><fmt:formatDate value="${syncqueue.latest_fb_datetime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								<td>${syncqueue.latest_fb_result}</td>
								<td>
									<c:if test="${not empty topic.cover}">
										<a href="${fileServer}${topic.cover}" target="_blank">
										<img alt="" src="${fileServer}${topic.cover}" width="100" height="100">${topic.name}</a>
									</c:if>
								</td>
								<td>
									<c:if test="${not empty syncqueue.operations}">
										<c:set var="publish_path" value="${syncqueue.operations[0].publish_path}"/>
										<a href="${publish_path}" target="_blank"><img alt="" src="${publish_path}" width="100" height="100"></a>
									</c:if>
								</td>
							</tr>
					</tbody>
				</table>
		    </div>
		  </div>
	</div>
	<!--  
	<div class="submit">
		<button id="publishBtn" type="button" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-ok"></span> 确认上架</button>
	</div> 
-->
</div>

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
$(function(){
	$("#publishBtn").on("click",function(){
		if(confirm("确认上架？")){
			$.ajax({
				url:"${ctx}/content/topic/publish?status=published&topicId=${topic_id}",
				success:function(event,param){
					var apiRsp=getApiJson(param.data);
					if(apiRsp){
						if(apiRsp.isSuccess){
							alert("上架成功");
						}else{
							alert(apiRsp.msg);
						}
					}else{
						alert("返回异常");
					}
				}
			});
		}		
	});
});
</script>
</body>
</html>
