<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>分类下的应用</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>
<div class="main game-list">

	<div class="main-top">
    
            <h3>${(not empty thirdCategoryName)?(thirdCategoryName):((not empty secondCategoryName)?(secondCategoryName):(firstCategoryName))} <small>共<span>${apiRsp.count}</span>个</small></h3>
            
            <form id="categorySearchForm" class="form-inline pull-right search-box" role="form" action="${ctx}/content/category/list">
            	<input type="hidden" id="parent_id" name="parent_id" value="">
            	<input type="hidden" id="firstCategoryId" name="firstCategoryId" value="${firstCategoryId}">
            	<input type="hidden" id="firstCategoryName" name="firstCategoryName" value="${firstCategoryName}">
            	<input type="hidden" id="secondCategoryId" name="secondCategoryId" value="${secondCategoryId}">
            	<input type="hidden" id="secondCategoryName" name="secondCategoryName" value="${secondCategoryName}">
            	<input type="hidden" id="thirdCategoryId" name="thirdCategoryId" value="${thirdCategoryId}">
            	<input type="hidden" id="thirdCategoryName" name="thirdCategoryName" value="${thirdCategoryName}">
            </form>
                        
            <form id="packageSearchForm" class="form-inline pull-right search-box" role="form" action="${ctx}/content/category/packageList">
            	<input type="hidden" id="category_id" name="category_id" value="${category_id}">
            	<input type="hidden" id="firstCategoryId" name="firstCategoryId" value="${firstCategoryId}">
            	<input type="hidden" id="firstCategoryName" name="firstCategoryName" value="${firstCategoryName}">
            	<input type="hidden" id="secondCategoryId" name="secondCategoryId" value="${secondCategoryId}">
            	<input type="hidden" id="secondCategoryName" name="secondCategoryName" value="${secondCategoryName}">
            	<input type="hidden" id="thirdCategoryId" name="thirdCategoryId" value="${thirdCategoryId}">
            	<input type="hidden" id="thirdCategoryName" name="thirdCategoryName" value="${thirdCategoryName}">
            	<div class="form-group">
            		<span>应用状态</span>
					<select class="form-control" id="package_status" name="package_status">
						<option value="">所有</option>
						<option value="published">已上架</option>
						<option value="unPublish">未上架</option>
					</select>
            	</div>
            	<div class="form-group" style="margin-left:10px;">
	            	<select class="form-control" id="searchType" name="searchType">
	                    <option value="title">应用名称</option>
	                    <option value="package_name">应用包名</option>
	                    <option value="package_id">应用ID</option>
	                    <option value="category_name">分类名称</option>
	                    <option value="tags">标签名称</option>
	                </select>
                </div>
                <input type="text" class="form-control" placeholder="关键词" id="searchValue" name="searchValue" value="${searchValue}">
                <button id="searchBtn" type="button" class="btn btn-default">搜索</button>
                <button id="searchAllBtn" type="button" class="btn btn-primary">显示全部</button>
            </form>
            
    </div><!--main-top-->
    
    
    
<div class="panel panel-info">
  <div class="panel-heading">
  <!--查看 开始-->
  <div class="pin">
                  
                  <form class="form-inline" role="form">
                    <div class="form-group">
                          <select class="form-control" id="batchOperateType">
                            <option>批量操作</option>
                            <option value="updatePackageOrder">排序</option>
                            <option value="removePackage">移出</option>
                          </select>
                    </div>
                          <div class="btn-group">
                            <button id="batchOperateBtn" type="button" class="btn btn-default">确认</button>
                          </div>
                  </form>
          
          		<button id="addPackageBtn" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>添加应用</button>
                  
                  
        </div>
            <!--查看 结束-->
  </div>


    <ol class="breadcrumb">
	      <c:if test="${not empty firstCategoryId}">
	      <li><a href="javascript:doCategorySearch('${firstCategoryId}','${firstCategoryId}','${firstCategoryName}','','','','');">${firstCategoryName}</a></li>
	      </c:if>
	      <c:if test="${not empty secondCategoryId}">
	      <li><a href="javascript:doCategorySearch('${secondCategoryId}','${firstCategoryId}','${firstCategoryName}','${secondCategoryId}','${secondCategoryName}','','');">${secondCategoryName}</a></li>
	      </c:if>
	      <c:if test="${not empty thirdCategoryId}">
	      <li>${thirdCategoryName}</li>
	      </c:if>
	      <li>应用管理</li>
    </ol>
    


	<table class="table" id="table_drag" style="display: none;"></table>

    <table class="table table-hover" id="table">
      <thead>
        <tr>
          <th class="th-check"><input type="checkbox" id="check-btn" class="tag" title="全选/反选"></th>
          <th class="th-sorting">排序</th>
          <th width="100">ID</th>
          <th class="th-name">名称</th>
          <th class="th-package">版本/包名</th>
          <th class="th-size">大小/金币</th>
          <th class="th-tag">标签/分类</th>
          <th class="th-time">发布日期</th>
        </tr>
      </thead>
	  <c:set var="pagingUrl" value="${ctx}/content/category/packageList?category_id=${category_id}&searchType=${searchType}&searchValue=${searchValue}&firstCategoryId=${firstCategoryId}&firstCategoryName=${firstCategoryName}&secondCategoryId=${secondCategoryId}&secondCategoryName=${secondCategoryName}&thirdCategoryId=${thirdCategoryId}&thirdCategoryName=${thirdCategoryName}"/>
	  <c:set var="nextUrl" value=""/>
	  <c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
	  <c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}"/>
	  </c:if>
		<tbody id="morePackageListDiv" nextUrl="${nextUrl}">
		<c:forEach items="${apiRsp.results}" var="item">
		        <tr id="${item.id}">
		          <td><input type="checkbox" name="list-checkbox" value="${item.id}"></td>
		          <td>${item.ordering}</td>
		          <td>${item.package_id}</td>
		          <td class="td-name">
		          		<img src="${item.icon}" class="img-rounded game-icon"/><strong>${item.title}</strong><br/>
		          		<!-- <span class="en-name">Subway Surfers</span> -->
		          		<span class="row-actions">
		          			<a href="javascript:editPackage('${firstCategoryName}','${item.package_id}','${item.version_id}');"><span class="glyphicon glyphicon-edit"></span>编辑</a>
		          			<a href="${item.preview_url}" target="_blank"><span class="glyphicon glyphicon-eye-open"></span>预览</a>
		          			<a href="${item.download_url}" target="_blank"><span class="glyphicon glyphicon-floppy-save"></span>下载</a>
		          		</span>
		          </td>
		          <td>${item.version_name} <br/> ${item.package_name}</td>
		          <td>${item.fileSize} <br/> ${item.award_coin}金币</td>
		          <td>${item.tags_text}<br/>${item.main_category_names}</td>
		          <td>${item.released_datetime}</td>
		        </tr>
		</c:forEach>
		</tbody>
    </table>

</div><!--panel-info-->

    <c:if test="${not empty nextUrl}">
	    <div class="load_more">
	    	<button id="loadingDiv" style="display:none" type="button" class="btn btn-default btn-block loading-btn">Loading...</button>
	    	<button id="moreDiv" style="display:" type="button" class="btn btn-default btn-block loading-btn" data-loading-text="Loading..." autocomplete="off">加载更多</button>
	    </div>
    </c:if>
    
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="apiRsp" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${pagingUrl}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div><!--main-bottom-->


</div><!--main end-->	

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
function doCategorySearch(parent_id,firstCategoryId,firstCategoryName,secondCategoryId,secondCategoryName,thirdCategoryId,thirdCategoryName){
	$("#categorySearchForm #parent_id").val(parent_id);
	$("#categorySearchForm #firstCategoryId").val(firstCategoryId);
	$("#categorySearchForm #firstCategoryName").val(firstCategoryName);
	$("#categorySearchForm #secondCategoryId").val(secondCategoryId);
	$("#categorySearchForm #secondCategoryName").val(secondCategoryName);
	$("#categorySearchForm #thirdCategoryId").val(thirdCategoryId);
	$("#categorySearchForm #thirdCategoryName").val(thirdCategoryName);
	//$("#categorySearchForm #category_name").val("");
	$("#categorySearchForm").submit();
}
function bindInitEvents(){
	$(".pin").pin();
	$("#table").tableDnD();
	bindCheckAll();
}
$(function(){
	bindInitEvents();
	$("#moreDiv").loadingMore({
		dataDivId:'morePackageListDiv',
		successCallback:function(){
			bindInitEvents();
		}
	});
	if("${package_status}"!=null && "${package_status}"!=""){
		$("#package_status").val("${package_status}");
	}
	if("${searchType}"!=null && "${searchType}"!=""){
		$("#searchType").val("${searchType}");
	}
	$("#searchBtn").click(function(){
		$("#packageSearchForm").submit();
	});
	$("#searchAllBtn").click(function(){
		$("#package_status").val("");
		$("#searchValue").val("");
		$("#packageSearchForm").submit();
	});
	$("#addPackageBtn").click(function(){
		openDialog({
			frame:true,
			title:"添加应用",
		    height:800,
		    width:1000,
			url:"${ctx}/content/category/packageListForAdd?root_category_id=${firstCategoryId}&root_category_name=${firstCategoryName}&category_id=${category_id}"
		});		
	});
	$("#batchOperateBtn").click(function(){
		var batchOperateType=$("#batchOperateType").val();
		if(batchOperateType=="removePackage"){
			var check_name = document.getElementsByName("list-checkbox");
			var idArr=new Array();
			for(var i=0;i<check_name.length;i++){
				if(check_name[i].checked){
					idArr.push(check_name[i].value);
				}
			}
			if(idArr.length==0){
				return;
			}
			ajaxSubmit("${ctx}/content/category/removePackage",{category_id:'${category_id}', ids:idArr.join(",")},reload,null,"确认批量移出？");
		}
		if(batchOperateType=="updatePackageOrder"){
			var table = document.getElementById("table");
			var tr = table.getElementsByTagName("tr");
			var idArr=new Array();
			var orderArr=new Array();
			for (var i = 1; i < tr.length; i++) {
				idArr.push(tr[i].getAttribute("id"));
				orderArr.push(i);
			}
			if(idArr.length==0 || orderArr.length==0){
				return;
			}
			ajaxSubmit("${ctx}/content/category/updatePackageOrder",{category_id:'${category_id}', ids:idArr.join(","), orders:orderArr.join(",")},reload,null,"确认批量排序？");
		}
	});	
});
</script>
</body>
</html>
