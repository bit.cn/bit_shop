<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>编辑分类</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>
<div class="container">
	<form id="editForm" role="form" action="${ctx}/content/category/save" method="post">
		<input type="hidden" name="id" value="${tc.id}">
		<input type="hidden" name="parent_id" value="${parent_id}">
		<input type="hidden" id="icon" name="icon" value="${tc.icon}">
		
		<div class="form-group has-feedback">
			<label class="control-label" for="category_name"><strong class="necessary">*</strong>分类名称</label>
			<input type="text" class="form-control" id="category_name" name="category_name" value="${tc.name}" placeholder="请输入分类名称" required="required">
		</div>
		<div class="form-group has-feedback">
			<label class="control-label" for="category_slug">分类别名</label>
			<input type="text" class="form-control" id="category_slug" name="category_slug" value="${tc.slug}" placeholder="请输入分类别名" <c:if test="${not empty tc.id}">readonly="readonly"</c:if>>
		</div>
		<div class="form-group has-feedback">
			<label class="control-label" for="category_slug">ICON</label>
			   <button id="chooseIconBtn" type="button" class="btn btn-primary loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-plus"></span> 选择ICON</button>
			   <div id="iconPreviewDiv" style="margin-top:10px">
				   <c:if test="${not empty tc.staticIcon}">
				   	<a href="${tc.staticIcon}" target="_blank"><img src='${tc.staticIcon}' width='72' height='72'></a>
				   </c:if>
			   </div>			
		</div>
		<div class="modal-footer">
	  		<button type="button" class="btn btn-default" data-dismiss="modal" id="diglog_close_btn-js" onclick="javascript:closeDialog();">关闭</button>
	  		<button type="submit" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-ok"></span> 确认提交</button>		
		</div>
	</form>
</div>
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
function chooseIconCallback(id,fileType,fileKey,fileName,cdnUrl){
	$("#icon").val(fileKey);
	$("#iconPreviewDiv").html("<a href='"+cdnUrl+"' target='_blank'><img src='"+cdnUrl+"' width='72' height='72'></a>");
	closeDialog();
}
$(function(){
	$("#editForm").bind('submit', function(event) {
		ajaxFormSubmit(this,reloadParent,null,null);
		event.preventDefault();
	});	
    $("#chooseIconBtn").on("click",function(){
    	openCommonDialog("选择ICON","${ctx}/system/cdn/list?contentTypeId=75&chooseCallback=chooseIconCallback&limitFileType=image");
    });
});
</script>
</body>
</html>
