<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>分类下可添加的应用</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>


<div class="main game-list">

	<div class="main-top">
    
            <h3>所有${root_category_name} <small>共<span>${apiRsp.count}</span>个</small></h3>
            
            <form id="packageSearchForm" class="form-inline pull-right search-box" role="form" method="post" action="${ctx}/content/category/packageListForAdd">
            	<input type="hidden" id="root_category_id" name="root_category_id" value="${root_category_id}">
            	<input type="hidden" id="category_id" name="category_id" value="${category_id}">
            	<select class="form-control" id="searchType" name="searchType">
                    <option value="title">应用名称</option>
                    <option value="package_name">应用包名</option>
                    <option value="package_id">应用ID</option>
                    <option value="category_name">分类名称</option>
                    <option value="tags">标签名称</option>
                </select>
                <input type="text" class="form-control" placeholder="关键词" id="searchValue" name="searchValue" value="${searchValue}">
                <button id="searchBtn" type="button" class="btn btn-default">搜索</button>
                <button id="searchAllBtn" type="button" class="btn btn-primary">显示全部</button>
            </form>
            
    </div><!--main-top-->
    
    
    
<div class="panel panel-info">
  <div class="panel-heading">
  <!--查看 开始-->
  <div class="pin">
  
                  <form class="form-inline" role="form">
                    <div class="form-group">
                          <select class="form-control" id="batchOperateType">
                            <option value="">批量操作</option>
                            <option value="addPackage">加入</option>
                          </select>
                    </div>
                          <div class="btn-group">
                            <button id="batchOperateBtn" type="button" class="btn btn-default">确认</button>
                          </div>
                  </form>
                  
                  
        </div>
            <!--查看 结束-->
  </div>
  
  <!-- <ol class="breadcrumb">
      <li><a href="game-list.html">所有游戏</a></li>
      <li>下架/有金币/搜索：破解</li>
  </ol> -->

    <table class="table table-hover" id="table">
      <thead>
        <tr>
          <th class="th-check"><input type="checkbox" id="check-btn" class="tag" title="全选/反选"></th>
          <th width="100">ID</th>
          <th class="th-name">名称</th>
          <th class="th-package">版本/包名</th>
          <th class="th-time">发布日期</th>
        </tr>
      </thead>
      	<c:set var="nextUrl" value=""/>
      	<c:if test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
      	<c:set var="nextUrl" value="${ctx}/content/category/packageListForAdd?root_category_id=${root_category_id}&category_id=${category_id}&searchType=${searchType}&searchValue=${searchValue}&page=${apiRsp.curPage+1}"/>
      	</c:if>      
      <tbody id="packageListForAddDiv" nextUrl="${nextUrl}">
      	<c:forEach items="${apiRsp.results}" var="item">
        <tr>
          <td><input type="checkbox" name="list-checkbox" value="${item.package_id}"></td>
          <td>${item.package_id}</td>
          <td class="td-name"><img src="${item.icon}" class="img-rounded game-icon"/><strong>${item.title}</strong><br/>
          		<!-- <span class="en-name">Subway Surfers</span> -->
          		<span class="row-actions"><a href="${item.preview_url}" target="_blank"><span class="glyphicon glyphicon-eye-open"></span>预览</a></span>
          </td>
          <td>${item.version_name} <br/> ${item.package_name}</td>
          <td>${item.released_datetime}</td>
        </tr>
        </c:forEach>
      </tbody>
    </table>

</div><!--panel-info-->
 

    <c:if test="${not empty nextUrl}">
	    <div class="load_more">
	    	<button id="loadingDiv" style="display:none" type="button" class="btn btn-default btn-block loading-btn">Loading...</button>
	    	<button id="moreDiv" style="display:" type="button" class="btn btn-default btn-block loading-btn" data-loading-text="Loading..." autocomplete="off">加载更多</button>
	    </div>
    </c:if>




		<div class="modal-footer">
	  		<button type="button" class="btn btn-default" data-dismiss="modal" id="diglog_close_btn-js" onclick="javascript:closeDialog();">关闭</button>
	  		<button id="submitBtn" type="button" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-ok"></span> 确认提交</button>		
		</div>
</div><!--main end-->

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
function bindInitEvents(){
	$(".pin").pin();
	bindCheckAll();
}
$(function(){
	bindInitEvents();
	$("#moreDiv").loadingMore({
		dataDivId:'packageListForAddDiv',
		successCallback:function(){
			bindInitEvents();
		}
	});
	if("${searchType}"!=null && "${searchType}"!=""){
		$("#searchType").val("${searchType}");
	}
	$("#searchBtn").click(function(){
		$("#packageSearchForm").submit();
	});
	$("#searchAllBtn").click(function(){
		$("#searchValue").val("");
		$("#packageSearchForm").submit();
	});
	$("#submitBtn").click(function(){
		closeDialog();
		reloadParent();
	});
	$("#batchOperateBtn").click(function(){
		var batchOperateType=$("#batchOperateType").val();
		var check_name = document.getElementsByName("list-checkbox");
		var idArr=new Array();
		for(var i=0;i<check_name.length;i++){
			if(check_name[i].checked){
				idArr.push(check_name[i].value);
			}
		}
		if(batchOperateType=="addPackage" && idArr.length>0){
			ajaxSubmit("${ctx}/content/category/addPackage",{category_id:"${category_id}", package_ids:idArr.join(",")},reload,null,"确认批量加入？");
		}
	});	
});
</script>
</body>
</html>
