<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>分类</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>
	

<div class="main game-class">



	<div class="main-top">    
            <h3>${(not empty thirdCategoryName)?(thirdCategoryName):((not empty secondCategoryName)?(secondCategoryName):(firstCategoryName))} <small>共<span>${apiRsp.count}</span>个</small></h3>  
            
            
            <form id="categorySearchForm" class="form-inline pull-right search-box" role="form" action="${ctx}/content/category/list">
            	<input type="hidden" id="parent_id" name="parent_id" value="${parent_id}">
            	<input type="hidden" id="firstCategoryId" name="firstCategoryId" value="${firstCategoryId}">
            	<input type="hidden" id="firstCategoryName" name="firstCategoryName" value="${firstCategoryName}">
            	<input type="hidden" id="secondCategoryId" name="secondCategoryId" value="${secondCategoryId}">
            	<input type="hidden" id="secondCategoryName" name="secondCategoryName" value="${secondCategoryName}">
            	<input type="hidden" id="thirdCategoryId" name="thirdCategoryId" value="${thirdCategoryId}">
            	<input type="hidden" id="thirdCategoryName" name="thirdCategoryName" value="${thirdCategoryName}">
                <input id="category_name" type="text" class="form-control" placeholder="名称" name="category_name" value="${category_name}">
                <button id="searchBtn" type="button" class="btn btn-default">搜索</button>
                <button id="searchAllBtn" type="button" class="btn btn-primary">显示全部</button>
            </form>
            
            <form id="packageSearchForm" class="form-inline pull-right search-box" role="form" action="${ctx}/content/category/packageList">
            	<input type="hidden" id="category_id" name="category_id" value="">
            	<input type="hidden" id="package_status" name="package_status" value="published">
            	<input type="hidden" id="firstCategoryId" name="firstCategoryId" value="${firstCategoryId}">
            	<input type="hidden" id="firstCategoryName" name="firstCategoryName" value="${firstCategoryName}">
            	<input type="hidden" id="secondCategoryId" name="secondCategoryId" value="${secondCategoryId}">
            	<input type="hidden" id="secondCategoryName" name="secondCategoryName" value="${secondCategoryName}">
            	<input type="hidden" id="thirdCategoryId" name="thirdCategoryId" value="${thirdCategoryId}">
            	<input type="hidden" id="thirdCategoryName" name="thirdCategoryName" value="${thirdCategoryName}">
            </form>            
              
    </div><!--main-top-->




	<div class="panel panel-info">
	  <div class="panel-heading">
	  		<div class="pin">
	          <form class="form-inline" role="form">
	            <div class="form-group">
	                  <select class="form-control" id="batchOperateType">
	                            <option value="">批量操作</option>
	                            <option value="updateOrder">排序</option>
	                            <!-- <option value="display">显示</option>
	                            <option value="hidden">隐藏</option> -->
	                            <option value="delete">删除</option>
	                  </select>
	            </div>
	            <div class="btn-group">
	            	<button id="batchOperateBtn" type="button" class="btn btn-default">确认</button>
	            </div>
	          </form>
	          
	          <button id="addCategoryBtn" type="button" class="btn btn-primary" data-toggle="modal" data-target=".add-box"><span class="glyphicon glyphicon-plus"></span>添加分类</button>
	        </div>
	  </div>
	    <ol class="breadcrumb">
	      <%-- <li><a href="${ctx}/content/category/list?parent_id=${firstCategoryId}&firstCategoryId=${firstCategoryId}&firstCategoryName=${firstCategoryName}&secondCategoryId=${secondCategoryId}&secondCategoryName=${secondCategoryName}">${firstCategoryName}分类</a></li> --%>
	      <c:if test="${not empty firstCategoryId}">
	      <li><a href="javascript:doCategorySearch('${firstCategoryId}','${firstCategoryId}','${firstCategoryName}','','','','');">${firstCategoryName}</a></li>
	      </c:if>
	      <c:if test="${not empty secondCategoryId}">
	      <li><a href="javascript:doCategorySearch('${secondCategoryId}','${firstCategoryId}','${firstCategoryName}','${secondCategoryId}','${secondCategoryName}','','');">${secondCategoryName}</a></li>
	      </c:if>
	    </ol>
	          <table class="table table-hover">
	            <thead>
	              <tr>
	                <th class="th-checkbox"><input type="checkbox" id="check-btn" class="tag" title="" data-original-title="全选/反选"></th>
	                <th class="th-sorting">排序</th>
	                <th width="50">ICON</th>
	                <th class="th-name">名称</th>
	                <th class="th-name">别名</th>
	                <th width="100">应用数</th>
	                <th width="50">状态</th>
	              </tr>
	            </thead>
	            <tbody class="class-list">
				<c:forEach items="${apiRsp.results}" var="item">
					<tr>
						<td><input type="checkbox" name="list-checkbox" value="${item.id}"></td>
						<td><input type="number" name="orders" value="${item.ordering}" class="form-control"/></td>
						<td><c:if test="${not empty item.staticIcon}"><img src="${item.staticIcon}" width="48" height="48"/></c:if></td>
						<td><strong>${item.name}</strong>
							<div class="text-muted">子分类：${not empty item.childrenNames?(item.childrenNames):'无'}</div>
							<span class="row-actions">
								<c:choose>
								<c:when test="${item.mpttLevel==1}">
								<a href="javascript:doCategorySearch('${item.id}','${firstCategoryId}','${firstCategoryName}','${item.id}','${item.name}','','');"><span class="glyphicon glyphicon-th-list"></span>子分类管理</a>
								<%-- <a href="javascript:doPackageSearch('${item.id}','${firstCategoryId}','${firstCategoryName}','${item.id}','${item.name}','','');"><span class="glyphicon glyphicon-th"></span>应用管理</a> --%>
								</c:when>
								<c:when test="${item.mpttLevel==2}">
								<a href="javascript:doPackageSearch('${item.id}','${firstCategoryId}','${firstCategoryName}','${secondCategoryId}','${secondCategoryName}','${item.id}','${item.name}');"><span class="glyphicon glyphicon-th"></span>应用管理</a>
								</c:when>
								</c:choose>
								<a href="javascript:editCategory('${item.id}');"><span class="glyphicon glyphicon-edit"></span>编辑</a>
							</span>
						</td>
						<td>${item.slug}</td>
						<td>${item.packageCount}</td>
						<td><span class="glyphicon glyphicon-ok-sign text-success" title="显示"></span></td>
					</tr>
				</c:forEach>
	            </tbody>
	          </table>
	
	</div><!--panel-info-->
    


</div><!--main end-->
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
function doCategorySearch(parent_id,firstCategoryId,firstCategoryName,secondCategoryId,secondCategoryName,thirdCategoryId,thirdCategoryName){
	$("#categorySearchForm #parent_id").val(parent_id);
	$("#categorySearchForm #firstCategoryId").val(firstCategoryId);
	$("#categorySearchForm #firstCategoryName").val(firstCategoryName);
	$("#categorySearchForm #secondCategoryId").val(secondCategoryId);
	$("#categorySearchForm #secondCategoryName").val(secondCategoryName);
	$("#categorySearchForm #thirdCategoryId").val(thirdCategoryId);
	$("#categorySearchForm #thirdCategoryName").val(thirdCategoryName);
	$("#categorySearchForm #category_name").val("");
	$("#categorySearchForm").submit();
}
function doPackageSearch(category_id,firstCategoryId,firstCategoryName,secondCategoryId,secondCategoryName,thirdCategoryId,thirdCategoryName){
	$("#packageSearchForm #category_id").val(category_id);
	$("#packageSearchForm #firstCategoryId").val(firstCategoryId);
	$("#packageSearchForm #firstCategoryName").val(firstCategoryName);
	$("#packageSearchForm #secondCategoryId").val(secondCategoryId);
	$("#packageSearchForm #secondCategoryName").val(secondCategoryName);
	$("#packageSearchForm #thirdCategoryId").val(thirdCategoryId);
	$("#packageSearchForm #thirdCategoryName").val(thirdCategoryName);
	$("#packageSearchForm").submit();
}
function editCategory(id){
	openDialog({
		frame:true,
		title:"修改分类",
	    height:650,
	    width:950,
		url:"${ctx}/content/category/edit?id="+id
	});
}
function bindInitEvents(){
	$(".pin").pin();
	bindCheckAll();
}
$(function(){
	bindInitEvents();
	$("#addCategoryBtn").on("click",function(){
		openDialog({
			frame:true,
			title:"添加分类",
		    height:650,
		    width:950,
			url:"${ctx}/content/category/edit?parent_id=${parent_id}"
		});
	});
	$("#searchBtn").click(function(){
		$("#categorySearchForm").submit();
	});
	$("#searchAllBtn").click(function(){
		$("#categorySearchForm #category_name").val("");
		$("#categorySearchForm").submit();
	});
	$("#batchOperateBtn").click(function(){
		var batchOperateType=$("#batchOperateType").val();
		var check_name = document.getElementsByName("list-checkbox");
		var orders = document.getElementsByName("orders");
		var idArr=new Array();
		var orderArr=new Array();
		for(var i=0;i<check_name.length;i++){
			if(check_name[i].checked){
				idArr.push(check_name[i].value);
				orderArr.push(orders[i].value);
			}
		}
		if(batchOperateType=="delete" && idArr.length>0){
			ajaxSubmit("${ctx}/content/category/delete",{parent_id:'${parent_id}', ids:idArr.join(",")},reload,"删除成功","确认批量删除？");
		}
		if(batchOperateType=="updateOrder" && idArr.length>0 && orderArr.length>0){
			ajaxSubmit("${ctx}/content/category/updateOrder",{parent_id:'${parent_id}', ids:idArr.join(","), orders:orderArr.join(",")},reload,"排序成功","确认批量排序？");
		}
	});	
});
</script>
</body>
</html>
