<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>CDN管理</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>
	

<div class="main comments">



	<div class="main-top">    
            <h3>CDN文件记录 </h3> 
            <form id="searchForm" action="${ctx}/system/cdn/list" method="post" class="form-inline pull-right search-box" role="form">
            	<input type="hidden" name="page_size" value="20">
            	<input type="hidden" name="contentTypeId" value="${contentTypeId}">
            	<input type="hidden" name="chooseCallback" value="${chooseCallback}">
            	<input type="hidden" name="limitFileType" value="${limitFileType}">
            	<input type="hidden" name="marker" value="">
                    <div class="form-group">
                          <span>文件前缀</span>
                          <input type="text" name="filePrefix" value="${filePrefix}" size="65" required="required">
                    </div>
                <button type="submit" class="btn btn-default">搜索</button>
            </form>
              
    </div><!--main-top-->




<div class="panel panel-info">
  <div class="panel-heading">
        <div class="pin">     
        </div>
        
  </div>
      
          <table class="table table-hover">
            <thead>
              <tr>
                <th>KEY</th>
                <th width="100">大小</th>
                <th class="th-time">上传时间</th>
                <th width="80">预览</th>
              	<th width="40">选择</th>
              </tr>
            </thead>
	      	<c:set var="pagingUrl" value="${ctx}/system/cdn/list?contentTypeId=${contentTypeId}&chooseCallback=${chooseCallback}&limitFileType=${limitFileType}&filePrefix=${filePrefix}&marker=${ret.marker}"/>
	      	<c:if test="${not empty ret.marker}">
	      	<c:set var="nextUrl" value="${pagingUrl}"/>
	      	</c:if>
			<tbody id="moreItemListDiv" nextUrl="${nextUrl}">
			<c:forEach items="${ret.itemList}" var="item">
              <tr>
                <td>${item.key}</td>
                <td align="right">${item.sizeStr}</td>
                <td>${myFn:formatDate(item.putMillis,'yyyy-MM-dd HH:mm:ss')}</td>
                <td>
                	<c:if test="${not empty item.cdnUrl and item.fileType eq 'image'}">
                		<a href="${item.cdnUrl}" target="_blank"><img src="${item.cdnUrl}" width="72" height="72"></a>
                	</c:if>
                </td>
              	<td>
              		<c:if test="${empty limitFileType or limitFileType eq item.fileType}">
              			<a onclick="javascript:chooseFileKey('${item.hash}','${item.fileType}','${fn:replace(item.key,'media/','')}','${item.fileName}','${item.cdnUrl}','${item.fsize}','${item.sizeStr}','${item.apkInfo.packageName}','${item.apkInfo.versionCode}','${item.apkInfo.versionName}','${item.apkInfo.label}','${item.apkInfo.appMd5}','${fn:replace(item.apkInfo.workspace,'media/','')}');" href="javascript:;" class="btn btn-primary" role="button">选择</a></td>
              		</c:if>
              </tr>
            </c:forEach>  
            </tbody>
          </table>

</div><!--panel-info-->
    

    <c:if test="${not empty nextUrl}">
	    <div class="load_more">
	    	<button id="loadingDiv" style="display:none" type="button" class="btn btn-default btn-block loading-btn">Loading...</button>
	    	<button id="moreDiv" style="display:" type="button" class="btn btn-default btn-block loading-btn" data-loading-text="Loading..." autocomplete="off">加载更多</button>
	    </div>
    </c:if>



      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="diglog_close_btn-js" onclick="javascript:closeDialog();">关闭</button>
      </div>

</div><!--main end-->

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
function doCallback(fn,args){
    fn.apply(this,args);
}
function chooseFileKey(id,fileType,fileKey,fileName,cdnUrl,fileSize,fileSizeStr,packageName,versionCode,versionName,label,appMd5,workspace){
	var limitFileType="${limitFileType}";
	if(limitFileType!=null && limitFileType!="" && limitFileType!=fileType){
		if(limitFileType=="image"){
			alert("请选择图片");
		}else if(limitFileType=="package"){
			alert("请选择应用包");
		}else if(limitFileType=="flash"){
			alert("请选择flash");
		}
		return;
	}
	var chooseCallback="parent.${chooseCallback}";
	doCallback(eval(chooseCallback),[id,fileType,fileKey,fileName,cdnUrl,fileSize,fileSizeStr,packageName,versionCode,versionName,label,appMd5,workspace]);
}
$(function(){
	$("#moreDiv").loadingMore({
		dataDivId:'moreItemListDiv'
	});	
});
</script>
</body>
</html>
