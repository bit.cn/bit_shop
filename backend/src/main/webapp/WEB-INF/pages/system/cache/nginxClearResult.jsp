<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
    <table class="table table-hover">
     <thead>
       <tr>
         <th>名称</th>
         <th>类型</th>
         <th>key</th>
         <th>状态</th>
         <th>内容</th>
       </tr>
     </thead>
		 <c:forEach var="item" items="${nginxList.results}">
              <tr>
                <td>${item.name}</td>
                <td><strong>${item.cacheType}</strong></td>
                <td>${item.cacheKey}</td>
               <td><strong>${item.status}</strong></td>
               <td><strong>${item.content}</strong></td>
              </tr>
		</c:forEach>
     </tbody>
   </table>
   
