<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
	<title>专区管理</title>
    <link rel="stylesheet" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/css/site.css">

</head>
<body>
	
<div class="main debris">
	<div class="main-top">    
            <h3>nginx cache key 管理
            <small>共<span>${nginxList.count}</span>个</small></h3> 
            <form class="form-inline pull-right search-box" role="form" action="${ctx}/system/cache/nginxKeyList" >
                <input type="text" class="form-control" name="cacheType" value="${cacheType}" placeholder="类型">
                <button type="submit" class="btn btn-default">搜索</button>
                <a href="${ctx}/system/cache/nginxKeyList" class="btn btn-primary">显示全部</a>
            </form>
    </div><!--main-top-->

<div class="panel panel-info">
  <div class="panel-heading">
  		<div class="pin">
          <form class="form-inline" id="optionFormId" method="post" >
            <input type="hidden" name="cacheType" value="${cacheType}" >
           <input type="hidden" name="itmeId" id="topicId" value="" >
               <div class="btn-group">
                 <button type="submit" class="btn btn-default" id="submitButtonId"
                      data-container="body" 
				      data-title="<h4>友情提示</h4>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h3>${message}</h3></div>" 
                 >删除</button>
               </div>
             </form>
             
          <button type="button" class="btn btn-primary" onclick="add();" ><span class="glyphicon glyphicon-plus"></span>添加key</button>
        </div>
  </div>
  <ol class="breadcrumb">
      <li><a href="${ctx}/system/cache/nginxKeyList">nginx cache key 管理</a></li>
  </ol>
  	<table class="table" id="table_drag" style="display: none;"></table>

    <table class="table table-hover" id="table">
     <thead>
       <tr>
         <th class="th-checkbox">
                  	<input type="checkbox" id="check-btn" class="tag" 
          	          data-container="body" 
				      data-title="<h5>友情提示</h5>"
				      data-toggle="popover" 
				      data-html="true"
				      data-content="<div><h5>请选择要删除的项目</h5></div>"
          	>
         </th>
         <th>名称</th>
         <th>站点</th>
         <th>类型</th>
         <th>类型值</th>
         <th>接口代码</th>
         <th>nginx服务</th>
         <th>key</th>
         <th>操作</th>
       </tr>
     </thead>
        <c:set var="nextUrl" value=""/>
      	<c:if test="${not empty nginxList and nginxList.curPage!=nginxList.totalPages}">
      	<c:set var="nextUrl" value="${ctx}/system/cache/nginxKeyList?cacheType=${cacheType}&page=${nginxList.curPage+1}"/>
      	</c:if>
		<tbody id="morePackageListDiv" nextUrl="${nextUrl}">
     	<jsp:include page="/WEB-INF/pages/system/cache/moreNginxKeyList.jsp" flush="true"/>
     </tbody>
   </table>

</div><!--panel-info-->
    <c:if test="${not empty nextUrl}">
	    <div class="load_more">
	    	<button id="loadingDiv" style="display:none" type="button" class="btn btn-default btn-block loading-btn">Loading...</button>
	    	<button id="moreDiv" style="display:" type="button" class="btn btn-default btn-block loading-btn" data-loading-text="Loading..." autocomplete="off">加载更多</button>
	    </div>
    </c:if>
    <div class="main-bottom">
		<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
			<jsp:param name="paginationObjectName" value="nginxList" />
			<jsp:param name="pageNoName" value="" />
			<jsp:param name="requestUrl" value="${ctx}/system/cache/nginxKeyList?cacheType=${cacheType}" />
			<jsp:param name="refreshDiv" value="" />
		</jsp:include>
    </div>
</div><!--main end-->
 
 
<div id="dialogId"></div>
<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.pin.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/common.js" type="text/javascript"></script>
<script src="${ctx}/js/dialog.js?=<%=new Date() %>>"></script>
<script>

function add(){
	var dialog =new dialogBox("dialogId");
	dialog.initComponent({url:"${ctx}/system/cache/get",title:'添加nginx cache key '});
	dialog.setTitle("添加nginx cache key ");
	dialog.show();
}
function edit(id){
	var dialog =new dialogBox("dialogId");
	dialog.initComponent({url:"${ctx}/system/cache/get?id="+id,title:'编辑nginx cache key'});
	dialog.show();
}
function clearNginx(id){
    var dialog =new dialogBox("dialogId");
	dialog.initComponent({ url:"${ctx}/system/cache/clearNginx?cacheKeyId="+id,title:'清空缓存状态',
	    width:1000});
	dialog.show();
}
function selectedTopic(submit){
	var check_name = document.getElementsByName("list-checkbox");
	var packageId="";
	if(submit){
		for(var i=0; i<check_name.length; i++){
			if(check_name[i].checked){
				packageId=packageId+check_name[i].value+",";
			}
		}
		$("#topicId").val(packageId);
	}else{
		var check=document.getElementById("check-btn").checked;
		for(var i=0; i<check_name.length; i++){
			check_name[i].checked = check;
		}
	}
	if(submit&&packageId==''){
		$("#check-btn").popover('show');
		setTimeout(function(){
			$("#check-btn").popover('destroy');
		},2000);
		return false;
	}else{
	    return true;
	}
}
function orderTopicItem(){
	var topicItem="";
	var table = document.getElementById("table");
	var tr = table.getElementsByTagName("tr");
	for (var i = 1; i < tr.length; i++) {
		var rowid = tr[i].getAttribute("id")+","+i;
		topicItem=topicItem+rowid+"#";
	}
	$("#topicId").val(topicItem);
	if(topicItem==''){
		return false;
	}else{
	    return true;
	}
}
$(function(){
	/*锁定操作栏*/
	$(".pin").pin();
	$("#moreDiv").loadingMore({
		dataDivId:'morePackageListDiv',
		successCallback:function(){
			$(".pin").pin();
		}
	});
	/*全选 取消全选*/	
	$("#check-btn").on("click",function(){
		selectedTopic(false);
	});
	$("#optionFormId").submit(function(){
			$(this).attr("action","${ctx}/system/cache/del");
			return selectedTopic(true);
	});
	if("${message}"!=''){
		$("#submitButtonId").popover('show');
		setTimeout(function(){
			$("#submitButtonId").popover('destroy');
		},2000);
	}
});

</script>

</body>
</html>




