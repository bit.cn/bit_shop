<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>

<form role="form" id="topicFormId" action="${ctx}/system/cache/edit" method="post" >
<input type="hidden" name="id" value="${nginx.id}" class="form-control">   

    <div class="modal-body">      
	<div class="form-group">
	  <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>名称</label>
	  <input type="text" id="inputSuccess1" name="name" class="form-control" value="${nginx.name}" required autofocus>
	  <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>类型</label>
	  <input type="text" id="inputSuccess1" name="cacheType" class="form-control" value="${nginx.cacheType}" required >
	  <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>类型值</label>
	    <input type="text" id="inputSuccess2" name="typeValue" class="form-control" value="${nginx.typeValue}" required >
	    <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>接口代码</label>
	      <input type="text" id="inputSuccess3" name="codes" class="form-control" value="${nginx.codes}">
	  	  <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>站点</label>
             <select class="form-control" name="cacheSite" id="cacheSiteId" required>
             		<option value="android_client" selected="selected" >安卓客户端</option>
             		<option value="ios_client">苹果客户端</option>
                    <option value="android_web">安卓主站</option>
                    <option value="android_wap">安卓wap</option>
             </select>
     <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>nginx 服务地址</label>
	  <input type="text" id="inputSuccess1" name="nginxServer" class="form-control" value="${nginx.nginxServer}" required >
	 <label for="inputSuccess1" class="control-label"><strong class="necessary">*</strong>key</label>
	  <input type="text" id="inputSuccess1" name="cacheKey" class="form-control" value="${nginx.cacheKey}" required >
	</div>
    </div>
    <div class="modal-footer">
      <button data-dismiss="modal" class="btn btn-default" type="button">关闭</button>
      <button autocomplete="off" data-loading-text="Loading..." class="btn btn-success loading-btn" type="submit"><span class="glyphicon glyphicon-ok"></span> 确认提交</button>
    </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
	if("${nginx.cacheSite}"!=''){
		$("#cacheSiteId").val("${nginx.cacheSite}");	
	}
	$("#topicFormId").submit(function(event) {
		$(this).ajaxForm({
			success:function(event,param){
				var data=param.data;
				for(var index in data){
					var apiRsp=data[index];
					if(apiRsp && apiRsp.isSuccess){
						location.reload();
					}else{
						alert(apiRsp.msg);
					}
				}
			}
		});
		event.preventDefault();
	});	
});
</script>


