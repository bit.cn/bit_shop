<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
 <c:forEach var="item" items="${nginxList.results}">
              <tr id="${item.id}">
              <td><input type="checkbox" name="list-checkbox" value="${item.id}"></td>
                 <td>${item.name}
                    <span class="row-actions">
                		<a href="javascript:void(0);" onclick="edit(${item.id});"><span class="glyphicon glyphicon-edit"></span>编辑</a>
                	</span>
                 </td>
                <td><strong>${item.cacheSite}</strong></td>
                <td><strong>${item.cacheType}</strong></td>
                <td><strong>${item.typeValue}</strong></td>
                <td><strong>${item.codes}</strong></td>
                <td><strong>${item.nginxServer}</strong></td>
                
                <td class="td-name">
	               ${item.cacheKey}
                </td>
                 <td>
                	 <button id="deleteVersionBtn" class="btn btn-danger loading-btn" onclick="clearNginx(${item.id});" data-loading-text="Loading..." type="button" style="margin-left:20px;">
						<span class="glyphicon glyphicon-remove"></span>
						更新缓存
					</button>
                 </td>
              </tr>
</c:forEach>