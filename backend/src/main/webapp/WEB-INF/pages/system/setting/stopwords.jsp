<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>词语过滤</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>

<body>

<div class="main irrigation">



	<div class="main-top">    
            <h3>词语过滤</h3>    
    </div><!--main-top-->
    
  

<form id="editForm" action="${ctx}/system/setting/setStopwords" method="post">
<input type="hidden" name="setting_name" value="${setting_name}">
  <div class="panel panel-info">
    <div class="panel-heading">*认真核对各关键词</div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel">

      <table class="table">
        <tbody>
          <tr>
            <th class="active" width="15%">关键词<p class="text-muted">多个关键词以换行隔开</p></th>
            <td width="85%"><textarea name="setting_value" required="required" class="form-control description" onpropertychange="this.style.height = this.scrollHeight + 'px';" oninput="this.style.height = this.scrollHeight + 'px';">${apiRsp.results.value}</textarea></td>
          </tr>
        </tbody>
        
        
      </table>
      
      
    </div>
  </div>
  
  
    

<div class="submit">
      <button type="submit" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-ok"></span> 确认提交</button>
</div>
</form>



</div><!--main end-->

<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript">
$(function(){
	/*textarea高度自适应*/
	$('.description').each(function(){
   		$(this).height($(this)[0].scrollHeight);
	});
	$("#editForm").bind('submit', function(event) {
		ajaxFormSubmit(this,null,"保存成功",null);
		event.preventDefault();
	});	
});
</script>
</body>
</html>
