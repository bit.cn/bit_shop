<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>角色管理</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
</head>
<body>
	<div class="main user-management">
		<div class="main-top">
			<h3>
				角色管理 <small>共搜索到<span>${apiRsp.count}</span>件符合条件的用户
				</small>
			</h3>
			<a
				href="${ctx}/org/search?user_names=${user_names}&user_ids=${user_ids}&user_phones=${user_phones}&user_emails=${user_emails}&user_level_start=${user_level_start}&user_level_end=${user_level_end}&signup_date_start=${signup_date_start}&signup_date_end=${signup_date_end}&login_date_start=${login_date_start}&login_date_end=${login_date_end}&user_sinupIps=${user_sinupIps}&user_loginIps=${user_loginIps}"
				class="btn btn-default pull-right"><span
				class="glyphicon glyphicon-search"></span> 重新搜索</a>
		</div>
		<!--main-top-->
		<!--用户列表-->
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="pin">
					<form class="form-inline" role="form">
						<div class="form-group">
							<select id="batchOperateType" class="form-control">
								<option value="">批量操作</option>
								<option value="disable">禁用</option>
								<option value="enable">恢复正常</option>
								<option value="delete">删除</option>
							</select>
						</div>
						<div class="btn-group">
							<button id="batchOperateBtn" type="button"
								class="btn btn-default">确认</button>
						</div>
						<div class="btn-group">
							<button id="addOperateBtn" type="button" onclick="addRole()"
								class="btn btn-default">新增</button>
						</div>
					</form>

				</div>

			</div>
			<ol class="breadcrumb">
				<li><a
					href="${ctx}/org/search?user_names=${user_names}&user_ids=${user_ids}&user_phones=${user_phones}&user_emails=${user_emails}&user_level_start=${user_level_start}&user_level_end=${user_level_end}&signup_date_start=${signup_date_start}&signup_date_end=${signup_date_end}&login_date_start=${login_date_start}&login_date_end=${login_date_end}&user_sinupIps=${user_sinupIps}&user_loginIps=${user_loginIps}">用户搜索</a></li>
				<li>角色管理</li>
			</ol>
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="th-checkbox"><input type="checkbox" id="check-btn"
							class="tag" title="" data-original-title="全选/反选"></th>
						<th>ID</th>
						<th>名称</th>
						<th>编号</th>
						<th>描述</th>
						<th>操作</th>
					</tr>
				</thead>
				<c:if
					test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
					<c:set var="nextUrl" value="${pagingUrl}&page=${apiRsp.curPage+1}" />
				</c:if>
				<tbody id="moreUserListDiv" nextUrl="${nextUrl}">
					<c:forEach items="${apiRsp.results}" var="item">
						<tr>
							<td><input type="checkbox" name="list-checkbox"
								value="${item.id}"></td>
							<td>${item.id}</td>
							<td>${item.name}</td>
							<td>${item.code}</td>
							<td>${item.description}</td>
							<td>
							<c:forEach
									items="${sessionScope.backend_session_user_info.funList}"
									var="fun">
									<c:if test="${fun.menuId==menuId}">
										<x:button buttonId="addOperateBtn" type="button"
											onClick="${fun.funCode}('${item.id}')"
											name="${fun.funName}" style="" funcationCode="${fun.funId}"
											menuId="${fun.menuId}"></x:button>
									</c:if>
								</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</div>
		<!--用户列表结束-->
		<c:if test="${not empty nextUrl}">
			<div class="load_more">
				<button id="loadingDiv" style="display: none" type="button"
					class="btn btn-default btn-block loading-btn">Loading...</button>
				<button id="moreDiv" style="display:" type="button"
					class="btn btn-default btn-block loading-btn"
					data-loading-text="Loading..." autocomplete="off">加载更多</button>
			</div>
		</c:if>

		<div class="main-bottom">
			<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
				<jsp:param name="paginationObjectName" value="apiRsp" />
				<jsp:param name="pageNoName" value="" />
				<jsp:param name="requestUrl" value="${pagingUrl}" />
				<jsp:param name="refreshDiv" value="" />
			</jsp:include>
		</div>
		<!--main-bottom-->
	</div>
	<!--main end-->

	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script type="text/javascript">
		function batchOperate(batchOperateType, idArr) {
			var operateTitle = "";
			var url = "";
			if (batchOperateType == "delete") {
				operateTitle = "批量删除";
				url = "${ctx}/org/delete";
			} else if (batchOperateType == "disable") {
				operateTitle = "批量禁用";
				url = "${ctx}/org/disable";
			} else if (batchOperateType == "enable") {
				operateTitle = "批量启用";
				url = "${ctx}/org/enable";
			}
			ajaxSubmit(url, {
				ids : idArr.join(",")
			}, reload, operateTitle + "成功", "确认" + operateTitle + "？");
		}
		function resetPassword(id) {
			var successCallback = function(event, param) {
				var apiRsp = getApiJson(param.data);
				alert("重置成功，新密码为：" + apiRsp.results);
			};
			ajaxSubmit("${ctx}/org/resetPassword?id=" + id, {},
					successCallback, null, "确认重置密码？");
		}
		
		function bindEvents() {
			$(".pin").pin();
			/*全选 取消全选*/
			var check_btn = document.getElementById("check-btn");
			var check_name = document.getElementsByName("list-checkbox");
			check_btn.onclick = function() {
				for ( var i = 1; i <= check_name.length; i += 1) {
					if (check_name[i - 1].checked) {
						check_name[i - 1].checked = false;
					} else {
						check_name[i - 1].checked = true;
					}
				}
			};
		}
		$(function() {
			$("#moreDiv").loadingMore({
				dataDivId : 'moreUserListDiv',
				successCallback : function() {
					bindEvents();
				}
			});
			$("#batchOperateBtn").click(function() {
				var batchOperateType = $("#batchOperateType").val();
				var check_name = document.getElementsByName("list-checkbox");
				var idArr = new Array();
				for ( var i = 0; i < check_name.length; i++) {
					if (check_name[i].checked) {
						idArr.push(check_name[i].value);
					}
				}
				if (batchOperateType != "" && idArr.length > 0) {
					batchOperate(batchOperateType, idArr);
				}
			});
		});
		
		//新增角色
		function addRole(){
			location.href="${ctx}/roles/add";
		}
		
		//编辑角色
		function edit(id) {
			parent.addTab("编辑角色", "${ctx}/roles/edit/" + id);
		}
		
		//删除角色
		function deleteRole(id, username){
			ajaxSubmit("${ctx}/roles/delete", {id : id}, reload, "操作成功!","确认删除？");
		}
		
	</script>
</body>
</html>
