<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>角色搜索</title>
    <%@ include file="/WEB-INF/pages/common/mainCss.jsp" %>
</head>
<body>
<div class="main user-search">
	<div class="main-top">    
            <h3>角色搜索</h3>              
    </div><!--main-top-->

<form id="searchForm" action="${ctx}/roles/list?menuId=${menuId}" method="post">
<!--用户搜索-->
<div class="panel panel-info">
  <div class="panel-heading">请先根据条件搜索角色，然后选择相应的操作。</div>
  <table class="table">
  		<tbody>
                <tr>
                    <th width="15%" class="active">名称</th>
                    <td width="85%"><input type="text" class="form-control" name="user_names" value="${user_names}"/><span class="text-muted">多个用户名用半角<code>,</code>隔开</span></td>
                </tr>
                <tr>
                    <th class="active">编号</th>
                    <td><input type="text" class="form-control" name="user_ids" value="${user_ids}"/><span class="text-muted">多个用户ID用半角<code>,</code>隔开</span></td>
                </tr>
        </tbody>
  </table>
</div><!--用户搜索 结束-->
<div class="submit">
      <button type="submit" class="btn btn-success loading-btn" data-loading-text="Loading..." autocomplete="off"><span class="glyphicon glyphicon-search"></span> 搜索</button>
      <button type="button" class="btn btn-primary" onclick="location='${ctx}/roles/list?menuId=${menuId}'">显示全部</button>
</div>
</form>
</div><!--main end-->
<%@ include file="/WEB-INF/pages/common/mainFooter.jsp" %>
<script type="text/javascript" src="${ctx}/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
</script>
</body>
</html>
