<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>菜单关联的功能列表</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
</head>
<body>
	<div class="main user-editor">
	<form id="editForm" role="form" action="${ctx}/roles/saveMenuFun"
			method="post">
			<input type="hidden" value="${roleId}" name="roleId" id="roleId"/>
			<input type="hidden" value="${menuId}" name="menuId" id="menuId"/>
		<div class="panel-group" id="accordion" role="tablist"
			aria-multiselectable="true">
			<!--基本信息-->
			<div class="panel panel-info">
				<div class="panel-heading" data-toggle="collapse"
					data-parent="#accordion" href="#collapseAPK" aria-expanded="true"
					aria-controls="collapseThree">
					<h4 class="panel-title">基本信息</h4>
				</div>
				<div id="collapseAPK" class="panel-collapse collapse in"
					role="tabpanel">
					<table class="table table-hover">
						<thead>
							<tr>
								<th class="th-checkbox"><input type="checkbox"
									id="check-btn" class="tag" title="" data-original-title="全选/反选"
									onclick="bindCheckAll()"></th>
								<th>名称</th>
							</tr>
						</thead>
						<c:if
							test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
							<c:set var="nextUrl"
								value="${pagingUrl}&page=${apiRsp.curPage+1}" />
						</c:if>
						<tbody id="moreUserListDiv" nextUrl="${nextUrl}">
							<c:forEach items="${apiRsp.results}" var="item">
								<tr>
									<td><input type="checkbox" name="list-checkbox" onchange="getMenuFunctionIds(this)"
										<c:forEach items="${permissionRelateRoleList}" var="fun">
														<c:if test="${fun.funId==item.funId}">
															checked="checked"
														</c:if>
														</c:forEach>
										value="${item.funId}"></td>
									<td>${item.funName}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</form>
		<div class="submit">
		 <button type="button" class="btn btn-default" onclick="javascript:saveMenuFun();">保存</button>
		</div>
	</div>
	<!--main end-->
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script type="text/javascript">
	//得到每个菜单选中的功能的信息
	function getMenuFunctionIds(who){
		$("input[name='list-checkbox']:checked").each(function() {
			this.checked = true;
		});
	}
	
	//保存菜单的关联功能
	function saveMenuFun(){
		$.ajax({
			cache : true,
			type : "POST",
			url : $("#editForm").attr("action"),
			data : $("#editForm").serialize(),// 你的formid
			async : false,
			error : function(request) {
				alert("返回异常");
			},
			success : function(data) {
				alert('新增成功!');
				closeDialog();
			}
		});
	}
	
</script>
</body>
</html>