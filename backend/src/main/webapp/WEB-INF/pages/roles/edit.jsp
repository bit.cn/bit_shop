<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>编辑角色</title>
<%@ include file="/WEB-INF/pages/common/mainCss.jsp"%>
<link rel="stylesheet" href="${ctx}/css/bootstrap-multiselect.css"
	type="text/css">
<link rel="stylesheet" href="${ctx}/jstree/default/style.min.css">
</head>
<body>
	<div class="main user-editor">
		<div class="main-top">
			<h3>编辑角色</h3>
		</div>
		<!--main-top-->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation"><a onclick="javascript:returnToList();"
				href="javascript:;"><span class="glyphicon glyphicon-arrow-left"></span>
					返回列表</a></li>
			<li role="presentation" class="active"><a href="#">${roles.id}</a></li>
		</ul>
		<form id="editForm" role="form" action="${ctx}/roles/save"
			method="post">
			<div class="panel-group" id="accordion" role="tablist"
				aria-multiselectable="true">

				<!--基本信息-->
				<div class="panel panel-info">

					<div class="panel-heading" data-toggle="collapse"
						data-parent="#accordion" href="#collapseAPK" aria-expanded="true"
						aria-controls="collapseThree">
						<h4 class="panel-title">基本信息</h4>
					</div>
					<div id="collapseAPK" class="panel-collapse collapse in"
						role="tabpanel">
						<table class="table">
							<tbody>
								<tr>
									<th width="10%" class="active" width="15%">角色名称：</th>
									<td width="30%"><input type="text" name="name"
										class="form-control" value="${roles.name}" /> <c:choose>
											<c:when test="${empty roles}">
											</c:when>
											<c:otherwise>
												<input type="hidden" name="roleId" class="form-control" id="roleId"
													value="${roles.id}" />
											</c:otherwise>
										</c:choose> 
										<input type="hidden" id="checkedMenuId" name="checkedMenuId"
										class="form-control" value="${roles.checkedMenuId}">
										<input type="hidden" id="id" name="id"
										class="form-control" value="${roles.checkedMenuId}">
										</td>
								</tr>
								<tr>
									<th class="active">角色编号：</th>
									<td><input type="text" class="form-control" name="code"
										value="${roles.code}" /></td>
								</tr>

								<tr>
									<th class="active">角色描述：</th>
									<td><input type="text" class="form-control"
										name="description" value="${roles.description}" /></td>
								</tr>
								<tr>
									<th class="active">关联菜单：</th>
									<td>
										<div class="tree" id="tree"></div> <input type="hidden"
										id="ids" name="ids" class="form-control" value="">
									</td>
								</tr>
								<tr>
									<th class="active">角色关联功能：</th>
									<td>
										<table class="table table-hover">
											<thead>
												<tr>
													<th class="th-checkbox"><input type="checkbox"
														id="check-btn" class="tag" title=""
														data-original-title="全选/反选" onclick="bindCheckAll()"></th>
													<th>名称</th>
												</tr>
											</thead>
											<c:if
												test="${not empty apiRsp and apiRsp.curPage!=apiRsp.totalPages}">
												<c:set var="nextUrl"
													value="${pagingUrl}&page=${apiRsp.curPage+1}" />
											</c:if>
											<tbody id="moreUserListDiv" nextUrl="${nextUrl}">
												<c:forEach items="${apiRsp.results}" var="item">
													<tr>
														<td><input type="checkbox" name="list-checkbox"
															<c:forEach items="${roles.funList}" var="fun">
														<c:if test="${fun.funId==item.id}">
															checked="checked"
														</c:if>
														</c:forEach>
															value="${item.id}"></td>
														<td>${item.name}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<div class="main-bottom">
											<jsp:include page="/WEB-INF/pages/common/pagination.jsp" flush="true">
												<jsp:param name="paginationObjectName" value="apiRsp" />
												<jsp:param name="pageNoName" value="" />
												<jsp:param name="requestUrl" value="${pagingUrl}" />
												<jsp:param name="refreshDiv" value="" />
											</jsp:include>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</form>
		<div class="submit">
			<c:choose>
				<c:when test="${empty roles}">
					<button type="button" id="formButton"
						onclick="javascript:saveRole();"
						class="btn btn-success loading-btn" data-loading-text="Loading..."
						autocomplete="off">
						<span class="glyphicon glyphicon-ok"></span> 确认提交
					</button>
				</c:when>
				<c:otherwise>
					<button type="button" id="formButton"
						onclick="javascript:updateRole();"
						class="btn btn-success loading-btn" data-loading-text="Loading..."
						autocomplete="off">
						<span class="glyphicon glyphicon-ok"></span> 确认提交
					</button>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<!--main end-->
	<div id="dialogId"></div>
	<%@ include file="/WEB-INF/pages/common/mainFooter.jsp"%>
	<script type="text/javascript" src="${ctx}/js/bootstrap-multiselect.js"
		type="text/javascript"></script>
	<script src="${ctx}/js/jquery.min.js" type="text/javascript"></script>
	<script src="${ctx}/jstree/jstree.min.js" type="text/javascript"></script>
	<script src="${ctx}/js/x_jstree.js" type="text/javascript"></script>
	<script type="text/javascript">
		var checkedMenuId = $("#checkedMenuId").val();
		var checkId = checkedMenuId.split(",");
		bindJsTree("tree", "${ctx}/menu/getTopMenuList", true, checkId,
				"${ctx}/roles/getFunctionListByMenuId");
	</script>
	<script type="text/javascript">
		//保存角色信息	var apiRsp = getApiJson(param.data);
		function saveRole() {
			$.ajax({
				cache : true,
				type : "POST",
				url : $("#editForm").attr("action"),
				data : $("#editForm").serialize(),// 你的formid
				async : false,
				error : function(request) {
					alert("返回异常");
				},
				success : function(data) {
					alert('新增成功!');
					parent.location.reload();
					parent.close();
				}
			});
		}

		//更新角色信息
		function updateRole() {
			$("#editForm").attr("action", "${ctx}/roles/update");
			$.ajax({
				cache : true,
				type : "POST",
				url : $("#editForm").attr("action"),
				data : $("#editForm").serialize(),// 你的formid
				async : false,
				error : function(request) {
					alert("返回异常");
				},
				success : function(data) {
					alert('修改成功!');
					parent.location.reload();
					parent.close();
				}
			});
		}
	</script>
</body>
</html>