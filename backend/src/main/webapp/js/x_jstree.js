//以指定的Json数据，初始化JStree控件
//treeName为树div名称，url为数据源地址，checkbox为是否显示复选框，checkId为后台选中的节点ID数组
function bindJsTree(treeName, url, checkbox, checkId, editUrl) {
	var control = $('#' + treeName);
	var edit = editUrl;
	control.data('jstree', false);   // 清空数据，必须
	var isCheck = checkbox || false; // 设置checkbox默认值为false
	if (isCheck) {
		// 复选框树的初始化
		$.getJSON(url, function(data) {
			control.jstree({
				'plugins' : [ "wholerow", "types", "checkbox" ],
				'checkbox' : {
					cascade : "",
					three_state : true
				}, // 不级联
				'root' : {
					"icon" : "/static/3.3.1/assets/images/tree_icon.png",
					"valid_children" : [ "default" ]
				},
				'core' : {
					'data' : data,
					"themes" : {
						"responsive" : false,
						"stripes" : true
					}
				}
			})
			// 绑定load时间，初始化数据显示
			.bind('loaded.jstree', function(e, data) {
				control.jstree("open_all");
				$("#menuEditPanel").attr("src", null);
				control.find("li").each(function() {
					for ( var i = 0; i < checkId.length; i++) {
						if (jQuery(this).attr("id") == checkId[i]) {
							control.jstree("check_node", jQuery(this));
						}
					}
				});
			})
			// 每一个子节点添加点击事件
			.on(
					'changed.jstree',
					function(e, data) {
						var node = data.instance.get_node(data.selected[0]);
						$("#id").val(node.id);
						if (event != null && data.node.state.selected == true
								&& data.node.parent != '#') {
							if($("#id").val()!=null && $("#id").val()!=''){
								editUrl = edit + "?id=" + data.node.id+"&roleId="+$("#id").val();
							}
							else{
								editUrl = edit + "?id=" + data.node.id;
							}
							$("#menuEditPanel").attr("src", editUrl);
							var dialog =new dialogBox("dialogId");
							dialog.initComponent({url:editUrl,frame:true,title:'关联菜单功能',height:670,width:1000});
							dialog.show();
							
							editUrl = null;
						} else {
							$("#menuEditPanel").attr("src", null);
						}
						var ids = getCheckboxTreeSelNode(treeName);
						$("#ids").val(ids);
					});

		});
	}
	// 普通树列表的初始化
	else {
		$.getJSON(url, function(data) {
			control.jstree({
				'core' : {
					'data' : data,
					"themes" : {
						"responsive" : false
					}
				}
			}).bind('loaded.jstree')
			// 为每一个节点添加点击事件
			.on('click.jstree', function(e, data) {
				var id = $(event.target).parents('li').attr('id');
				var parent=$(event.target).parents('li').parents('li');
				$("#id").val(id);
				editUrl = edit + "?id=" + id+"&parentId="+parent.attr('id');
				$("#menuEditPanel").attr("src", editUrl);
				editUrl = null;
			});
		});
	}
}

// 添加顶级菜单
function addTopMenu(url) {
	$("#menuEditPanel").attr("src", url);
};

// 添加子菜单
function addChildMenu(url) {
	var id = $("#id").val();
	url = url + "?parentId=" + id;
	$("#menuEditPanel").attr("src", url);
};

// 删除菜单
function deleteMenu(treeName, url) {
	var id = $("#id").val();
	if (id == null) {
		alert("请选择待删除的菜单！");
		return;
	}
	ajaxSubmit(url, {
		id : id
	}, reload, "操作成功!", "确认删除？");
}

// 得到所有选中的节点的ID集合
function getCheckboxTreeSelNode(treeid) {
	var ids = Array();
	jQuery("#" + treeid).find("li").each(
			function() {
				var liid = jQuery(this).attr("id");
				if (jQuery("#" + liid + ">a").hasClass("jstree-clicked")
						|| jQuery("#" + liid + ">a>i").hasClass(
								"jstree-undetermined")) {
					ids.push(liid);
				}
			});
	return ids;
}