package com.business.plat.sa.controller;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.business.dto.UserDto;
import com.google.gson.reflect.TypeToken;
import com.oxygen.dto.ApiFinalResponse;
import com.oxygen.enums.BackendApiMethodEnum;
import com.oxygen.util.JsonUtil;
import com.soloyogame.anitoys.util.constants.ManageContainer;

/**
 * 商家登录action
 * 
 * @author shaojian
 */
@Controller
@RequestMapping("/manage/user")
public class LoginAction extends BaseAction {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LoginAction.class);
	private static final long serialVersionUID = 1L;
	private static final String page_input = "/manage/system/login";
	private static final String page_home = "/manage/system/home";
	private static final String page_toList = "/manage/system/user/userList";
	private static final String page_toAdd = "/manage/system/user/editUser";
	private static final String page_toEdit = "/manage/system/user/editUser";
	private static final String page_toChangePwd = "/manage/system/user/toChangePwd";
	private static final String page_changePwd_result = "/manage/system/user/changePwd";
	private static final String page_show = "/manage/system/user/show";
	private static final String page_initManageIndex = page_home;
	private String businessName = null;

	/**
	 * 商家后台进入登录页面
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpServletRequest req) {
		if (this.isLogon(req)) {
			return "redirect:/manage/user/home";
		}
		req.setAttribute("username", req.getParameter("username"));
		return page_input;
	}

	/**
	 * 商家后台登录
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/doLogin", method = RequestMethod.POST)
	public String login(HttpSession session, HttpServletRequest req, HttpServletResponse rsp, ModelMap model) throws Exception {
		String errorMsg;
		if (session.getAttribute(ManageContainer.manage_session_user_info) != null) {
			return "redirect:/manage/user/home";
		}

		String userName = req.getParameter("username");
		String password = req.getParameter("password");
		if (StringUtils.isBlank(userName) || StringUtils.isBlank(password)) {
			model.addAttribute("errorMsg", "账户和密码不能为空!");
			return page_input;
		}

		logger.info("用户登录:{}", userName);
		// 校验登陆状态
		Map<String, Object> appendMap = new HashMap<String, Object>();
		String json = this.callApi(BackendApiMethodEnum.USER_LOGIN, appendMap, req);
		Type type = new TypeToken<Map<String, ApiFinalResponse<UserDto>>>() {
		}.getType();
		Map<String, ApiFinalResponse<UserDto>> retMap = JsonUtil.jsonToObject(json, type);
		if (retMap == null) {
			return "redirect:login";
		}
		UserDto user = retMap.get(BackendApiMethodEnum.USER_LOGIN.getCode()).getResults();
		if (user == null) {
			errorMsg = "登陆失败，账户或密码错误！";
			logger.error("登陆失败，账户或密码错误,{}", userName);
			model.addAttribute("errorMsg", errorMsg);
			return page_input;
		}
		logger.error("登录了没有", userName);
		try {
			// TODO 用户登录日志
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "redirect:/manage/user/home";
	}
}
