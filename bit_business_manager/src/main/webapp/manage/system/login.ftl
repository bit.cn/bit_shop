<#import "/manage/tpl/htmlBase.ftl" as html>
<link rel="stylesheet" href="/bit_business/resource/login/css/login.css" />
<@html.htmlBase checkLogin=false>
<script type="text/javascript" src="/bit_business/resource/js/login.js"></script>
<body  onkeydown='if(event.keyCode==13){submitFun()}' onload="getvalcode()">
 <form form role="form" id="formLogin" action="http://localhost/bit_business/manage/user/doLogin" method="post">
<div class="loginMain">
 <input type="hidden" value="" name="verify" id="verify"/>
  <table width="100%" border="0" class="loginTable" cellspacing="0" cellpadding="0">
  <tr height="43">
     <td width="82" align="right">用户名：</td>
    <td colspan="2"><input type="text" value="" placeholder="账号" name="username"
                                           class="len form-control" id="username" autofocus/></td>
  </tr>
  <tr height="43">
    <td align="right">密&nbsp;&nbsp;&nbsp;&nbsp;码：</td>
    <td colspan="2">
    	<input type="password" name="password" id="password" placeholder="密码" class="len form-control" label="密码"/>
    </td>
  </tr>

</table>
<table width="100%" border="0" class="loginTable2"  cellspacing="0" cellpadding="0">
  <tr>
     <td width="220" align="right" > 
     	<input type="submit" id="btnLogin" class="but01"  value="" onclick="return submitFun()">
     </td>
     <td width="238" class="findPassword">
     <#if errorMsg??>
       <div class="alert alert-danger alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         	${errorMsg}
       </div>
     </#if>                                                                                                                
     </td>
  </tr>
</table>
</div>
</form>
</body>
</@html.htmlBase>