<#import "/manage/tpl/htmlBase.ftl" as html>
<@html.htmlBase>
	<SCRIPT type="text/javascript">
		<!--
		$(function(){
	 		$("#input_menu_name").focus();
	 		$('input:radio[name="parentOrChildRadio"]').click(function(){
	 			var parentOrChild =$(this).val();//子菜单还是根节点
	 			if(parentOrChild==0 || parentOrChild==1){
	 				//添加顶级菜单
	 				$("#result_table1").hide();
	 			}else{
	 				$("#result_table1").show();
	 			}
	 		});
	 		
	 		//添加子菜单/修改父菜单
	 		$("#add").click(function(){
	 			var id = $("#id").val();
	 			var pid = $("#pid").val();
	 			var dic_id = $("#dic_id", parent.document).val();
	 			var item_code = $("#item_code").val();
	 			var item_name = $("#item_name").val();
	 			var item_value = $("#item_value").val();
	 			var core_value = $("#core_value").val();
	 			var item_extra = $("#item_extra").val();
	 			var order_no = $("#order_no").val();
	 			
	 			var childrenid = $("#childrenid").val();
	 			var childrenpid = $("#childrenpid").val();
	 			var childrendic_id = $("#dic_id", parent.document).val();
	 			var childrenitem_code = $("#childrenitem_code").val();
	 			var childrenitem_name = $("#childrenitem_name").val();
	 			var childrenitem_value = $("#childrenitem_value").val();
	 			var childrencore_value = $("#childrencore_value").val();
	 			var childrenitem_extra = $("#childrenitem_extra").val();
	 			var childrenorder_no = $("#childrenorder_no").val();
	 			$.ajax({
					url:"${basepath}/manage/dict/doAddOrUpdate",
					type:"post",
					data:{
						//父菜单信息
						id:id,
						pid:pid,
						dic_id:dic_id,
						item_code:item_code,
						item_name:item_name,
						item_value:item_value,
						core_value:core_value,
						item_extra:item_extra,
						order_no:order_no,
						childrenid:childrenid,
						childrenpid:id,
						childrendic_id:childrendic_id,
						childrenitem_code:childrenitem_code,
						childrenitem_name:childrenitem_name,
						childrenitem_value:childrenitem_value,
						childrencore_value:childrencore_value,
						childrenitem_extra:childrenitem_extra,
						childrenorder_no:childrenorder_no
					},
					dataType:"text",
					success:function(data, textStatus){
						if(data=='0'){
							$("#showMsgDiv").html("操作成功!");
						}else if(data=="-1"){
							$("#showMsgDiv").html("非法请求!");
						}else{
							$("#showMsgDiv").html("操作失败!");
						}
						
						setTimeout(function(){$("#showMsgDiv").html("");},2000);
					},
					error:function(){
						alert("修改资源菜单失败!");
					}
				});
	 		});
	 		
		});
		//-->
		
		function changeType(){
			var _type = $("#input_area_type").val();
			console.log(_type);
			if(_type=='top'){console.log("is here");
				$("#children_pcode").attr("disabled","disabled").val("0");
			}else{
				$("#children_pcode").attr("disabled",false).val($("#input_parent_code").val());
			}
			
		}
	</SCRIPT>
</head>

<body style="text-align: center;">
<form action="/manage/dict" name="form1" namespace="/manage" theme="simple">
		<div id="showMsgDiv" style="text-align: center;"></div>
		<#if e??>
		<table id="result_table1" class="table table-bordered" style="width: 98%;margin: auto;margin-top: 20px;">
			<tr>
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>选中的项</strong>
				</td>
			</tr>
			<tr style="display:none;">
				<th>id</th>
				<td style="text-align: left;">
					<input id="id" name="id" value='${e.id!""}'/>
				</td>
			</tr>
			<tr style="display:none;">
				<th>pid</th>
				<td style="text-align: left;">
					<input id="pid" name="pid" value='${e.pid!""}'/>
				</td>
			</tr>
			<tr>
				<th>字典项名称：</th>
				<td style="text-align: left;">
					<input id="item_name" size="80" name="item_name" value='${e.item_name!""}'/>
				</td>
			</tr>
			<tr>
				<th>字典项代码：</th>
				<td style="text-align: left;">
					<input id="item_code" name="item_code" value='${e.item_code!""}'/>
				</td>
			</tr>
			<tr>
				<th>字典项值：</th>
				<td style="text-align: left;">
					<input id="item_value" name="item_value" value='${e.item_value!""}'/>
				</td>
			</tr>
			<tr>
				<th>排序：</th>
				<td style="text-align: left;">
					<input id="order_no" name="order_no" value='${e.order_no!""}'/>
				</td>
			</tr>
			<tr>
				<th>核心对应值 ：</th>
				<td style="text-align: left;">
					<input id="core_value" name="core_value" value='${e.core_value!""}'/>
				</td>
			</tr>
			<tr>
				<th>字典项扩展值 ：</th>
				<td style="text-align: left;">
					<textarea name="item_extra" id="item_extra" style="width: 80%">${e.item_extra!""}</textarea>
				</td>
			</tr>
		</table>
		<!-- item -->
		<table class="table table-bordered" style="width: 98%;margin: auto;margin-top: 20px;">
			<tr>
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>子项</strong>
				</td>
			</tr>
			<tr style="display: none;">
				<td>id</td>
				<td>
					<input id="input_children_id" readonly="readonly"/>
				</td>
			</tr>
			<tr>
				<th>类型</th>
				<td style="text-align: left;">
					<#assign y_n = {'children':'子项','top':'顶级'}>
                    <select id="input_children_type" name="e.type" class="input-medium" onchange="changeType()">
						<#list y_n?keys as key>
                            <option value="${key}" <#if e.type?? && e.type==key>selected="selected" </#if>>${y_n[key]}</option>
						</#list>
                    </select>
				</td>
			</tr>
			<tr style="display:none;">
				<td>id</td>
				<td style="text-align: left;">
					<input id="childrenid" name="childrenid" value=''/>
				</td>
			</tr>
			<tr style="display:none;">
				<td>pid</td>
				<td style="text-align: left;">
					<input id="childrenpid" name="childrenpid" value='0'/>
				</td>
			</tr>
			<tr>
				<th>字典项名称：</th>
				<td style="text-align: left;">
					<input id="childrenitem_name" size="80" name="childrenitem_name" value=''/>
				</td>
			</tr>
			<tr>
				<th>字典项代码：</th>
				<td style="text-align: left;">
					<input id="childrenitem_code" name="childrenitem_code" value=''/>
				</td>
			</tr>
			<tr>
				<th>字典项值：</th>
				<td style="text-align: left;">
					<input id="childrenitem_value" name="childrenitem_value" value=''/>
				</td>
			</tr>
			<tr>
				<th>排序：</th>
				<td style="text-align: left;">
					<input id="childrenorder_no" name="childrenorder_no" value=''/>
				</td>
			</tr>
			<tr>
				<th>核心对应值 ：</th>
				<td style="text-align: left;">
					<input id="childrencore_value" name="childrencore_value" value=''/>
				</td>
			</tr>
			<tr>
				<th>字典项扩展值 ：</th>
				<td style="text-align: left;">
					<textarea name="childrenitem_extra" id="childrenitem_extra" style="width: 80%"></textarea>
				</td>
			</tr>
			<tr>
				<td style="text-align: center;" colspan="2">
					<input type="button" id="add" value="修改或添加" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
		<#else>
		<table id="result_table1" class="table table-bordered" style="width: 98%;margin: auto;margin-top: 20px;">
			<tr>
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>添加字典项</strong>
				</td>
			</tr>
			<tr style="display:none;">
				<td>id</td>
				<td style="text-align: left;">
					<input id="id" name="id" value=''/>
				</td>
			</tr>
			<tr style="display:none;">
				<td>pid</td>
				<td style="text-align: left;">
					<input id="pid" name="pid" value='0'/>
				</td>
			</tr>
			<tr>
				<th>字典项名称：</th>
				<td style="text-align: left;">
					<input id="item_name" size="80" name="item_name" value=''/>
				</td>
			</tr>
			<tr>
				<th>字典项代码：</th>
				<td style="text-align: left;">
					<input id="item_code" name="item_code" value=''/>
				</td>
			</tr>
			<tr>
				<th>字典项值：</th>
				<td style="text-align: left;">
					<input id="item_value" name="item_value" value=''/>
				</td>
			</tr>
			<tr>
				<th>排序：</th>
				<td style="text-align: left;">
					<input id="order_no" name="order_no" value=''/>
				</td>
			</tr>
			<tr>
				<th>核心对应值 ：</th>
				<td style="text-align: left;">
					<input id="core_value" name="core_value" value=''/>
				</td>
			</tr>
			<tr>
				<th>字典项扩展值 ：</th>
				<td style="text-align: left;">
					<textarea name="item_extra" id="item_extra" style="width: 80%"></textarea>
				</td>
			</tr>
			<tr>
				<td style="text-align: center;" colspan="2">
					<input type="button" id="add" value="修改或添加" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
		</#if>
</form>
</@html.htmlBase>