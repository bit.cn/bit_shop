<#import "/manage/tpl/htmlBase.ftl" as html>
<@html.htmlBase>
	<SCRIPT type="text/javascript">
		<!--
		$(function(){
	 		$("#input_menu_name").focus();
	 		$('input:radio[name="parentOrChildRadio"]').click(function(){
	 			var parentOrChild =$(this).val();//子菜单还是根节点
	 			if(parentOrChild==0 || parentOrChild==1){
	 				//添加顶级菜单
	 				$("#result_table1").hide();
	 			}else{
	 				$("#result_table1").show();
	 			}
	 		});
	 		
	 		//添加子菜单/修改父菜单
	 		$("#add").click(function(){
	 			var id = $("#id").val();
	 			var parentId = $("#parentId").val();
	 			var channelUrl = $("#channelUrl").val();
	 			var channelName = $("#channelName").val();
	 			var sortOrder = $("#sortOrder").val();
	 			var businessId = $("#businessId").val();
	 			
	 			var childrenid = $("#childrenid").val();
	 			var childrenparentId = $("#childrenparentId").val();
	 			var childrenchannelUrl = $("#childrenchannelUrl").val();
	 			var childrenchannelName = $("#childrenchannelName").val();
	 			var childrensortOrder = $("#childrensortOrder").val();
	 			var childrenbusinessId = $("#childrenbusinessId").val();
	 			$.ajax({
					url:"${systemSetting().center}/manage/channel/doAddOrUpdate",
					type:"post",
					data:{
						//父菜单信息
						id:id,
						parentId:parentId,
						channelUrl:channelUrl,
						channelName:channelName,
						sortOrder:sortOrder,
						businessId:businessId,
						
						childrenid:childrenid,
						childrenparentId:id,
						childrenchannelUrl:childrenchannelUrl,
						childrenchannelName:childrenchannelName,
						childrensortOrder:childrensortOrder,
						childrenbusinessId:childrenbusinessId
					},
					dataType:"text",
					success:function(data, textStatus){
						if(data=='0'){
							$("#showMsgDiv").html("操作成功!");
							location.reload();
						}else if(data=="-1"){
							$("#showMsgDiv").html("非法请求!");
						}else{
							$("#showMsgDiv").html("操作失败!");
						}
						setTimeout(function(){$("#showMsgDiv").html("");},2000);
						top.location.reload();
					},
					error:function(){
						alert("修改资源菜单失败!");
					}
				});
	 		});
	 		
		});
		//-->
		
		function changeType(){
			var _type = $("#input_area_type").val();
			console.log(_type);
			if(_type=='top'){console.log("is here");
				$("#children_pcode").attr("disabled","disabled").val("0");
			}else{
				$("#children_pcode").attr("disabled",false).val($("#input_parent_code").val());
			}
			
		}
	</SCRIPT>
</head>

<body style="text-align: center;">
<form action="/manage/dict" name="form1" namespace="/manage" theme="simple">
		<div id="showMsgDiv" style="text-align: center;"></div>
		<#if e??>
		<#if e.parentId=="0">
		<table id="result_table1" class="table table-bordered" style="width: 98%;margin: auto;margin-top: 20px;">
			<tr>
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>添加顶级栏目</strong>
				</td>
			</tr>
			<tr style="display:none;">
				<td>id</td>
				<td style="text-align: left;">
					<input id="id" name="id" value='${e.id!""}'/>
				</td>
			</tr>
			<tr style="display:none;">
				<td>parentId</td>
				<td style="text-align: left;">
					<input id="parentId" name="parentId" value='${e.parentId!""}'/>
				</td>
			</tr>
			<tr style="display:none;">
				<th>businessId：</th>
				<td style="text-align: left;">
					<input id="businessId" name="businessId" value='${e.businessId!""}'/>
				</td>
			</tr>
			<tr>
				<th>栏目名称：</th>
				<td style="text-align: left;">
					<input id="channelName" name="channelName" data-rule="栏目名称:required;" value='${e.channelName!""}'/>
				</td>
			</tr>
			<tr>
				<th>栏目链接：</th>
				<td style="text-align: left;">
					<input id="channelUrl" size="80" name="channelUrl" data-rule="栏目链接:required;url;"value='${e.channelUrl!""}'/>
				</td>
			</tr>
			<tr>
				<th>排序：</th>
				<td style="text-align: left;">
					<input id="sortOrder" name="sortOrder" data-rule="排序:required;integer[+];"value='${e.sortOrder!""}'/>
				</td>
			</tr>
		</table>
		<!-- item -->
		<table class="table table-bordered" style="width: 98%;margin: auto;margin-top: 20px;">
			<tr>
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>子项</strong>
				</td>
			</tr>
			<tr style="display: none;">
				<td>id</td>
				<td>
					<input id="input_children_id" readonly="readonly"/>
				</td>
			</tr>
			<tr>
				<th>类型</th>
				<td style="text-align: left;">
					<#assign y_n = {'children':'子项','top':'顶级'}>
                    <select id="input_children_type" name="e.type" class="input-medium" onchange="changeType()">
						<#list y_n?keys as key>
                            <option value="${key}" <#if e.type?? && e.type==key>selected="selected" </#if>>${y_n[key]}</option>
						</#list>
                    </select>
				</td>
			</tr>
			<tr style="display:none;">
				<td>id</td>
				<td style="text-align: left;">
					<input id="childrenid" name="childrenid" value=''/>
				</td>
			</tr>
			<tr style="display:none;">
				<td>parentId</td>
				<td style="text-align: left;">
					<input id="childrenparentId" name="childrenparentId" value=''/>
				</td>
			</tr>
			<tr style="display:none;">
				<th>businessId：</th>
				<td style="text-align: left;">
					<input id="childrenbusinessId" name="childrenbusinessId" value=''/>
				</td>
			</tr>
			<tr>
				<th>栏目名称：</th>
				<td style="text-align: left;">
					<input id="childrenchannelName" name="childrenchannelName" data-rule="栏目名称:required;" value=''/>
				</td>
			</tr>
			<tr>
				<th>栏目链接：</th>
				<td style="text-align: left;">
					<input id="childrenchannelUrl" size="80" name="childrenchannelUrl" data-rule="栏目链接:required;url;" value=''/>
				</td>
			</tr>
			<tr>
				<th>排序：</th>
				<td style="text-align: left;">
					<input id="childrensortOrder" name="childrensortOrder" data-rule="排序:required;integer[+];" value=''/>
				</td>
			</tr>
			<tr>
				<td style="text-align: center;" colspan="2">
					<input type="button" id="add" value="修改或添加" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
		<#else >
		<table id="result_table1" class="table table-bordered" style="width: 98%;margin: auto;margin-top: 20px;">
			<tr>
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>添加顶级栏目</strong>
				</td>
			</tr>
			<tr style="display:none;">
				<td>id</td>
				<td style="text-align: left;">
					<input id="id" name="id" value='${e.id!""}'/>
				</td>
			</tr>
			<tr style="display:none;">
				<td>parentId</td>
				<td style="text-align: left;">
					<input id="parentId" name="parentId" value='${e.parentId!""}'/>
				</td>
			</tr>
			<tr style="display:none;">
				<th>businessId：</th>
				<td style="text-align: left;">
					<input id="businessId" name="businessId" value='${e.businessId!""}'/>
				</td>
			</tr>
			<tr>
				<th>栏目名称：</th>
				<td style="text-align: left;">
					<input id="channelName" name="channelName" data-rule="栏目名称:required;" value='${e.channelName!""}'/>
				</td>
			</tr>
			<tr>
				<th>栏目链接：</th>
				<td style="text-align: left;">
					<input id="channelUrl" size="80" name="channelUrl" data-rule="栏目链接:required;url;"value='${e.channelUrl!""}'/>
				</td>
			</tr>
			<tr>
				<th>排序：</th>
				<td style="text-align: left;">
					<input id="sortOrder" name="sortOrder" data-rule="排序:required;integer[+];"value='${e.sortOrder!""}'/>
				</td>
			</tr>
			<tr>
				<td style="text-align: center;" colspan="2">
					<input type="button" id="add" value="修改或添加" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
		</#if>
		<#else>
		<table id="result_table1" class="table table-bordered" style="width: 98%;margin: auto;margin-top: 20px;">
			<tr>
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong>添加顶级栏目</strong>
				</td>
			</tr>
			<tr style="display:none;">
				<td>id</td>
				<td style="text-align: left;">
					<input id="id" name="id" value=''/>
				</td>
			</tr>
			<tr style="display:none;">
				<td>parentId</td>
				<td style="text-align: left;">
					<input id="parentId" name="parentId" value='0'/>
				</td>
			</tr>
			<tr style="display:none;">
				<th>businessId：</th>
				<td style="text-align: left;">
					<input id="businessId" name="businessId" value=''/>
				</td>
			</tr>
			<tr>
				<th>栏目名称：</th>
				<td style="text-align: left;">
					<input id="channelName" name="channelName" data-rule="栏目名称:required;" value=''/>
				</td>
			</tr>
			<tr>
				<th>栏目链接：</th>
				<td style="text-align: left;">
					<input id="channelUrl" size="80" name="channelUrl" data-rule="栏目链接:required;url;" value=''/>
				</td>
			</tr>
			<tr>
				<th>排序：</th>
				<td style="text-align: left;">
					<input id="sortOrder" name="sortOrder" data-rule="排序:required;integer[+];" value=''/>
				</td>
			</tr>
			<tr>
				<td style="text-align: center;" colspan="2">
					<input type="button" id="add" value="修改或添加" class="btn btn-primary"/>
				</td>
			</tr>
		</table>
		</#if>
</form>
</@html.htmlBase>