
<link rel="stylesheet" href="${systemSetting().staticSource}/bootstrap/css/bootstrap.min.css"  type="text/css">
<link rel="stylesheet" href="${systemSetting().staticSource}/css/base.css"  type="text/css">


<script type="text/javascript" src="${systemSetting().staticSource}/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="${systemSetting().staticSource}/js/manage.js"></script>


