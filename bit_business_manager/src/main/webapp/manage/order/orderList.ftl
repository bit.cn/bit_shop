<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="售后订单">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<script type="text/javascript">
	$(function() {
		function c1(f) {
			$(":checkbox").each(function() {
				$(this).attr("checked", f);
			});
		}
		$("#firstCheckbox").click(function() {
			if ($(this).attr("checked")) {
				c1(true);
			} else {
				c1(false);
			}
		});
	});
	function deleteSelect() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定删除选择的记录?");
	}
	function updateInBlackList() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定将选择的记录拉入新闻黑名单吗?");
	}
</script>
	<form action="${systemSetting().center}/manage/order" method="post" theme="simple">
		<table class="table table-bordered">
			<tr>
				<td style="width: 8%;">订单编号</td>
				<td><input type="text" value="${e.id!""}" name="id" class="search-query input-small"/></td>
			</tr>
			<tr>
				<td style="width: 8%;">售后状态</td>
				<td colspan="1">
					<#assign map = {'0':'','1':'申诉中','2':'已受理','3':'已完成'}>
                    <select id="orderRepairsStatus" name="orderRepairsStatus" class="search-query input-medium">
						<#list map?keys as key>
                            <option value="${key}" <#if e.status?? && e.status==key>selected="selected" </#if>>${map[key]}</option>
						</#list>
                    </select></td>
			</tr>
			<tr>
				<td colspan="14">
						<button method="selectList?isSaleorder=1" class="btn btn-primary" onclick="selectList(this)">
							<i class="icon-search icon-white"></i> 查询
						</button>
					<div style="float: right;vertical-align: middle;bottom: 0px;top: 10px;">
						<#include "/manage/system/pager.ftl"/>
					</div>
				</td>
			</tr>
		</table>
		<table class="table table-bordered table-hover">
			<tr style="background-color: #dff0d8">
				<th width="20"><input type="checkbox" id="firstCheckbox" /></th>
				<th>订单号</th>
				<th>订单总金额</th>
				<th>售后类型</th>
				<th>问题描述</th>
				<th>用户基本信息</th>
				<th>会员</th>
				<th>申请售后日期</th>
				<th>订单状态</th>
				<th>售后状态</th>
				<th width="120px">操作</th>
			</tr>
			<#list pager.list as item>
				<tr>
					<td><input type="checkbox" name="ids"
						value="${item.id!""}" /></td>
					<td>
						${item.orderCode!""}
						<!--<#if item.lowStocks?? && item.lowStocks=="y"><font color="red">【缺货】</font></#if>-->
					</td>
					<td>${item.amount!""}
						<!--<#if item.updateAmount?? && item.updateAmount=="y"><font color="red">【修】</font></#if>-->
					</td>
					<td>
					<#if item.repairType?? && item.repairType==1>
						换货
						<#elseif item.repairType?? && item.repairType==2>
						退款
					</#if>
					</td>
					<td>${item.userInfo!""}</td>
					<td align="center">${item.repairDescription!""}</td>
					<td><a target="_blank" href="${systemSetting().center}/manage/account/show?account=${item.accountName!""}">${item.accountName!""}</a></td>
					<td>
						<#if item.applyTime??>
							${item.applyTime?string("yyyy-MM-dd HH:mm:ss")}
						</#if>
					</td>
					<td>${item.statusStr!""}
						<#if item.status?? && item.status=="cancel">
							<img src="${systemSetting().staticSource}/static/frontend/v1/images/action_delete.gif">
						<#elseif  item.status?? && item.status=="finish">
							<img src="${systemSetting().staticSource}/static/frontend/v1/images/action_check.gif">
						<#elseif  item.status?? && item.status=="init">
							<img src="${systemSetting().staticSource}/static/frontend/v1/images/action_add.gif">
						</#if>
					</td>
					<td>
						<#if item.orderRepairsStatus?? && item.orderRepairsStatus==2>
							已受理
						<#elseif  item.orderRepairsStatus?? && item.orderRepairsStatus==3>
							已完成
						</#if>
					</td>
					<td><a target="_blank" href="toEdit?id=${item.id}">编辑</a>|
					<a target="_blank" href="${systemSetting().center}/manage/orderRepairs/toEdit?orderId=${item.id}">售后处理</a>
					</td>
				</tr>
			</#list>
			<tr>
				<td colspan="55" style="text-align: center;">
					<#include "/manage/system/pager.ftl"/></td>
			</tr>
		</table>
		
		<div class="alert alert-info" style="text-align: left;font-size: 14px;margin: 2px 0px;">
			图标含义：<BR>
			<img alt="新增" src="${systemSetting().staticSource}/static/frontend/v1/images/action_add.gif">：未支付
			<img alt="已上架" src="${systemSetting().staticSource}/static/frontend/v1/images/action_check.gif">：交易完成
			<img alt="已下架" src="${systemSetting().staticSource}/static/frontend/v1/images/action_delete.gif">：已取消
		</div>

	</form>
</@page.pageBase>