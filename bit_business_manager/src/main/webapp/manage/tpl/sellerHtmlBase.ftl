<!--页面引入的基础资源-->
<#macro sellerHtmlBase title="" jsFiles=[] cssFiles=[] staticJsFiles=[] staticCssFiles=[] checkLogin=true>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <#assign non_responsive2>y</#assign>
    <#assign responsive>${Session["responsive"]!""}</#assign>
    <#if responsive == "y">
        <#assign non_responsive2>n</#assign>
    <#elseif systemSetting().openResponsive == "n">
        <#assign non_responsive2>y</#assign>
    <#else >
        <#assign non_responsive2>n</#assign>
    </#if>
   <script>
        var basepath = "${systemSetting().center}";
        var staticpath = "${staticpath}";
        var non_responsive2 = "${non_responsive2}";
        <#if currentUser()??>
            var login = true;
        var currentUser = "${currentUser().username}";
        <#else >
        var login = false;
        var currentUser = "";
            <#if checkLogin>
                top.location = "${systemSetting().center}/manage/user/logout";
            </#if>
        </#if>
    </script>
    <meta name="description" content="${systemSetting().description}"/>
    <meta name="keywords" content="${systemSetting().keywords}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${(title?? && title!="")?string("Anitoys - "+ title , "Anitoys")}</title>
    <link rel="shortcut icon" type="image/x-icon" href="${systemSetting().shortcuticon}">
    
    <!--卖家样式资源-->
    <link rel="stylesheet" href="${systemSetting().staticSource}/common.seller/css/style.css"  type="text/css">
    <link rel="stylesheet" href="${systemSetting().staticSource}/common.seller/css/omt.css"  type="text/css">

</head>
<#nested />
</html>
</#macro>
