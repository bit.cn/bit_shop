<#macro menu menus=[] topMenu="" currentMenu="首页">
		<ul class="head_my_ul">
		 		<#list menus as menu>
			 		<li class="ord">
						<img src="${systemSetting().manageHttp}/resource/common.seller/image/left_text.png" />
						&nbsp;
						${menu.name!""}
					</li>
		 			 <#if menu.children?? && menu.children?size gt 0>
                            <ul class="nav nav-second-level">
                            <#list menu.children as menuchild>
                            	<li>
									<a href="${menuchild.url}">${menuchild.name!""}</a>
								</li>
                            </#list>
                            </ul>
                    </#if>
                 </#list>
		  </ul>
</#macro>