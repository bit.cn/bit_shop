<#macro sellerFoot>
		<div class="footer">
		  <div class="footer_top">
		    <div class="ftmain">
		      <div class="ftleft">
		        <ul>
		          <li>
		            <h3>购物指南</h3>
		            <a href="#" target="_blank">注册登录</a><a href="#" target="_blank">现货购买</a><a href="#" target="_blank">预约购买</a><a href="#" target="_blank">常见问题</a></li>
		          <li>
		            <h3>支付方式</h3>
		            <a href="#" target="_blank">在线支付</a><a href="#" target="_blank">用户积分</a><a href="#" target="_blank">优惠券</a></li>
		          <li>
		            <h3>配送方式</h3>
		            <a href="#" target="_blank">配送服务详情</a></li>
		          <li>
		            <h3>售后服务</h3>
		            <a href="#" target="_blank">售后政策</a><a href="#" target="_blank">售后流程</a></li>
		          <li>
		            <h3>关于anitoys</h3>
		            <a href="#" target="_blank">关于我们</a><a href="#" target="_blank">联系我们</a></li>
		        </ul>
		      </div>            
	      <div class="ftphone">
        <div class="ftp"></div>
      </div>
      <div class="clear"></div>
    </div>
</#macro>