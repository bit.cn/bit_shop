<#import "sellerHtmlBase.ftl" as sellerhtml />
<#--currentMenu:当前菜单项-->
<#macro sellerPageBase currentMenu topMenu="" title="" jsFiles=[] cssFiles=[] staticJsFiles=[] staticCssFiles=[] checkLogin=true>
<@sellerhtml.sellerHtmlBase title=title jsFiles=jsFiles cssFiles=cssFiles staticJsFiles=staticJsFiles staticCssFiles=staticCssFiles checkLogin=checkLogin>
<body>
	<div class="omt_top">
	<div class="omt_top_use">
			<p class="ont_logo">
			</p>
			 <ul>
			 	<li> <img src="${systemSetting().manageHttp}/resource/common.seller/image/logo.png"/> </li>
			 	<li class="user">
					<a href="${systemSetting().center}/manage/user/logout">欢迎回来，${currentUser().nickname!currentUser().username}[退出]</a>
				</li>
				<li>
					<a href="#">用户中心</a>
				</li>
				<li>
					<a href="#">网站导航</a>
				</li>
				<li>
					<a href="#">客户服务</a>
				</li>
				<li>
					<a href="#">我的订单</a>
				</li>
				<li>
					<img src="${systemSetting().manageHttp}/resource/common.seller/image/shop.png"/> 
				</li>
			 </ul>
			 <ul class="inf">
			 	<li>
					<a href="#">
						<img src="${systemSetting().manageHttp}/resource/common.seller/image/pom.png"  />
						最新情报
					</a>
				</li>
				<li class="inf_new">
					2015/10/25
					<img src="${systemSetting().manageHttp}/resource/common.seller/image/vp.png"  />
					<a href="#">
						客服于2月18日-24日暂停服务
					</a>
				</li>
				<li class="int_left">
					<img  src="${systemSetting().manageHttp}/resource/common.seller/image/sud.png"/>
					<img  src="${systemSetting().manageHttp}/resource/common.seller/image/left.png" />
					<img  src="${systemSetting().manageHttp}/resource/common.seller/image/rig.png" class="int_rig"/>
				</li>
				<li>
					<img src="${systemSetting().manageHttp}/resource/common.seller/image/buf.png" />
				</li>
			 </ul>
		</div>
	<div class="omt_head">
    <#nested />
</body>
</@sellerhtml.sellerHtmlBase>
</#macro>