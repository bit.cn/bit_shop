<#macro htmlBase title="" jsFiles=[] cssFiles=[] staticJsFiles=[] staticCssFiles=[] checkLogin=true>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <#assign non_responsive2>y</#assign>
    <#assign responsive>${Session["responsive"]!""}</#assign>
    <script>
        var basepath = "${basepath}";
        var staticSource = "${basepath}";
        var non_responsive2 = "${non_responsive2}";
    </script>
    <#if non_responsive2 != "y">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </#if>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${(title?? && title!="")?string("Anitoys - "+ title , "Anitoys")}</title>
    <link rel="shortcut icon" type="image/x-icon" href="">

    <link rel="stylesheet" href="/bit_business/resource/zTree3.5/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <link rel="stylesheet" href="/bit_business/resource/bootstrap3.3.4/css/bootstrap.min.css"  type="text/css">
    <link rel="stylesheet" href="/bit_business/resource/jquery-ui-1.11.2/jquery-ui.css">
    <link rel="stylesheet" href="/bit_business/resource/validator-0.7.0/jquery.validator.css" />

    <script type="text/javascript" src="/bit_business/resource/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/bit_business/resource/zTree3.5/js/jquery.ztree.all-3.5.min.js"></script>

    <script type="text/javascript" src="/bit_business/resource/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/bit_business/resource/bootstrap3.3.4/js/bootstrap.min.js"></script>
    <!-- sb admin -->
    <link rel="stylesheet" href="/bit_business/resource/sb-admin/css/sb-admin-2.css" />
    <script src="/bit_business/resource/sb-admin/js/sb-admin-2.js" ></script>

    <link href="/bit_business/resource/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src="/bit_business/resource/jquery-ui-1.11.2/jquery-ui.js"></script>
    <!-- jquery validator -->

	<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/bit_business/resource/validator-0.7.0/jquery.validator.js"></script>
    <script type="text/javascript" src="/bit_business/resource/validator-0.7.0/local/zh_CN.js"></script>

  <!--日期控件调用本地资源-->
    <script type="text/javascript" src="${basepath}/resource/My97DatePicker/WdatePicker.js"></script>

    <!--文件上传调用本地资源-->
    <link rel="stylesheet" href="${basepath}/resource/kindeditor-4.1.7/themes/default/default.css" />
    <script charset="utf-8" src="${basepath}/resource/kindeditor-4.1.7/kindeditor-min.js"></script>
    <script charset="utf-8" src="${basepath}/resource/kindeditor-4.1.7/lang/zh_CN.js"></script>

    <!-- datatables -->
    <link rel="stylesheet" href="/bit_business/resource/datatables/css/jquery.dataTables.css" />
    <script charset="utf-8" src="/bit_business/resource/datatables/js/jquery.dataTables.js"></script>
    <link rel="stylesheet" href="/bit_business/resource/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" />
    <script charset="utf-8" src="/bit_business/resource/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <!-- metisMenu -->
    <link href="/bit_business/resource/metisMenu/metisMenu.min.css" rel="stylesheet">
    <script src="/bit_business/resource/metisMenu/metisMenu.min.js"></script>
    <#--<link rel="stylesheet" href="/bit_business/resource/datatables-responsive/css/dataTables.responsive.css" />-->
    <#--<script charset="utf-8" src="/bit_business/resource/datatables-responsive/js/dataTables.responsive.js"></script>-->
    <script type="text/javascript" src="/bit_business/resource/manage.js"></script>
    <#list staticJsFiles as jsFile>
        <script src="/${jsFile}"></script>
    </#list>
    <#list staticCssFiles as cssFile>
        <link rel="stylesheet" href="/${cssFile}" />
    </#list>

    <#list jsFiles as jsFile>
        <script src="/resource/manage/${jsFile}"></script>
    </#list>
    <#list cssFiles as cssFile>
        <link rel="stylesheet" href="/resource/manage/${cssFile}" />
    </#list>
</head>
<#nested />
</html>
</#macro>
