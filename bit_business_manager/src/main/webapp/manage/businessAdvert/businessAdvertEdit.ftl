<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="店铺首页广告图管理">
	<form action="${systemSetting().center}/manage/businessAdvert/" theme="simple" enctype="multipart/form-data">
		<span id="pifeSpan" class="input-group-addon" style="display:none">${systemSetting().imageRootPath}</span>
		<table class="table table-bordered">
			<tr style="background-color: #dff0d8">
				<td colspan="2" style="background-color: #dff0d8;text-align: center;">
					<strong> 图 片 编 辑 </strong>
				</td>
			</tr>
			<tr style="display: none;">
				<th>id</th>
				<td><input type="hidden" value="${e.id!""}" name="id" label="id" id="idd"/></td>
			</tr>
			<tr>
				<th class="right">广告标题</th>
				<td style="text-align: left;"><input type="text"  value="${e.title!""}" name="title"  data-rule="标题:required;title;length[1~45];"
						id="title" /></td>
			</tr>
			<tr>
				<th class="right">广告链接</th>
				<td style="text-align: left;">
				<input type="text"  value="${e.advertLink!""}" name="advertLink" 
						id="advertLink"  data-rule="链接:required;url;length[1~100];"/></td>
			</tr>
			<tr>
				<th>广告图片</th>
				<td style="text-align: left;" colspan="3">
					<input type="button" name="filemanager" value="浏览图片" class="btn btn-warning"/>
					<input type="text"  value="${e.advertImage!""}" name="advertImage"  id="advertImage" ccc="imagesInput" style="width: 600px;" data-rule="图片地址:required;picture;" />
					<#if e.picture??>
						<a target="_blank" href="${systemSetting().imageRootPath}/..${e.advertImage!""}">
							<img style="max-width: 50px;max-height: 50px;" alt="" src="${systemSetting().imageRootPath}/..${e.advertImage!""}">
						</a>
					</#if>
				</td>
			</tr>
			<tr>
				<th>广告位</th>
				<td style="text-align: left;" >
                    <#assign map = {'':'','y':'是','n':'否'}>
                    <select id="advertPosition" name="advertPosition" class="input-medium">
           
                         <option value="1" >1</option>
                         <option value="2" >2</option>
                         <option value="3" >3</option>
                     
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;">
					<#if e.id??>
                        <button method="updateApps" class="btn btn-success">
                            <i class="icon-ok icon-white"></i> 保存
                        </button>
					<#else>
                        <button method="insertApp" class="btn btn-success">
                            <i class="icon-ok icon-white"></i> 新增
                        </button>
					</#if>
			</tr>
		</table>
	</form>
<script>
//删除图片主路径
function clearRootImagePath(picInput){
	var _pifeSpan = $("#pifeSpan").text();
	var _imgVal = picInput.val();
	picInput.val(_imgVal.substring(_imgVal.indexOf("/attached/")));
	//if(_imgVal && _imgVal.length>0 && _imgVal.indexOf(_pifeSpan)==0){
		//picInput.val(_imgVal.substring(_pifeSpan.length));
	//}
}

KindEditor.ready(function (K) {
        var editor = K.editor({
            fileManagerJson: '${systemSetting().staticSource}/kindeditor-4.1.7/jsp/file_manager_json.jsp',
            allowFileManager: true
        });
	 //上传图片
        K('input[name=filemanager]').click(function () {
            var imagesInputObj = $(this).parent().children("input[ccc=imagesInput]");
            editor.loadPlugin('image', function () {
                editor.plugin.imageDialog({
                    viewType: 'VIEW',
                    dirName: 'image',
                    clickFn: function (url, title) {
                        imagesInputObj.val(url);
                        editor.hideDialog();
                        clearRootImagePath(imagesInputObj);//$("#picture"));
                    }
                });
            });
        });
	
});
</script>
</@page.pageBase>