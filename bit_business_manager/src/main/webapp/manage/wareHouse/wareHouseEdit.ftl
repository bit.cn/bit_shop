<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="仓库管理">
<form action="${systemSetting().center}/manage/wareHouse" id="form" name="form">
			<table class="table table-bordered">
				<tr style="background-color: #dff0d8">
					<td colspan="2" style="background-color: #dff0d8;text-align: center;">
						<strong>仓库编辑</strong>
					</td>
				</tr>
				<tr style="display: none;">
					<td>id</td>
					<td><input type="hidden" name="id" value="${e.id!""}" id="id"></td>
				</tr>
				<tr>
					<td style="text-align: right;">仓库名</td>
					<td style="text-align: left;"><input type="text" name="wareHouseName" value="${e.wareHouseName!""}" id="wareHouseName" data-rule="仓库名:required;wareHouseName;length[1~45];"/></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;">
						<#if e.id??>
							<button method="update" class="btn btn-success" >
								<i class="icon-ok icon-white"></i> 保存
							</button>
							<#else>
								<button method="insert" class="btn btn-success" >
									<i class="icon-ok icon-white"></i> 新增
								</button>
						</#if>
					</td>
				</tr>
			</table>
	</form>

</@page.pageBase>