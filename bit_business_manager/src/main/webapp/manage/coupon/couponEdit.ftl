<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="优惠券管理">
<style>
    #insertOrUpdateMsg {
        border: 0px solid #aaa;
        margin: 0px;
        position: fixed;
        top: 0;
        width: 100%;
        background-color: #d1d1d1;
        display: none;
        height: 30px;
        z-index: 9999;
        font-size: 18px;
        color: red;
    }

    .btnCCC {
        background-image: url("../img/glyphicons-halflings-white.png");
        background-position: -288px 0;
    }

    .li_style_10 {
        margin-top: 30px;
        font-size: 13px;
    }

    .li_style_11 {
        margin-top: 20px;
        margin-left: 113px;
        font-size: 13px;
    }

    .ul_style_3 {
        margin-left: 30px;
        margin-top: -220px;
    }

    .li_style_13 {
        margin-top: 30px;
        margin-left: 113px;
        font-size: 13px;
    }

    .li_style_16 {
        margin-left: 18px;
        float: left;
    }

    .li_style_15 {
        float: left;
    }
</style>
<script>
    $(function () {
        <#if e.id??>
            var id = "${e.id}";
            $("#btnStatic").click(function () {
                $.post("${systemSetting().center}/freemarker/create?method=staticNewsByID&id=" + id, null, function (response) {
                    alert(response == "success" ? "操作成功！" : "操作失败!");
                });
            });
        </#if>
    });
</script>
<form action="${systemSetting().center}/manage/coupon" namespace="/manage" theme="simple" name="form" id="form"
      method="post">
    <input type="hidden" value="${e.id!""}" id="id"/>
    <table class="table table-bordered">
        <tr>
            <td colspan="4" style="text-align: right;width: 20%;">
                <#if e.id??>
                    优惠券ID：<span class="badge badge-success">${e.id!""}</span>
                    <button method="update" class="btn btn-success">
                        <i class="icon-ok icon-white"></i> 保存
                    </button>
                <#else>
                    <button method="insert" class="btn btn-success">
                        <i class="icon-ok icon-white"></i> 新增
                    </button>
                </#if>
            </td>
        </tr>
        <tr style="background-color: #dff0d8">
            <td colspan="6" style="background-color: #dff0d8;text-align: center;">
                <strong>优惠券编辑 </strong>
            </td>
        </tr>
        <tr style="display: none;">
            <td>id</td>
            <td><input type="hidden" value="${e.id!""}" name="id" label="id"/></td>
            <td style="text-align: right;width:20%;" colspan="3">优惠券号码</td>
            <td style="text-align: left;">
                <input type="text" value="${e.couponSn!""}" name="couponSn" id="couponSn"/>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">优惠券名称</td>
            <td style="text-align: left;">
                <input type="text" value="${e.couponName!""}" data-rule="优惠券名称:required;" name="couponName" id="couponName1"/><br>
            </td>
            <td style="text-align: right;width: 20%;">优惠券种类</td>
            <td style="text-align: left;" colspan="3">
                <#assign map = {'1':'抵价券','2':'折价券'}>
                <select name="couponType" id="couponType"
                        data-rule="优惠券种类:required;" onChange="checkCouponValue();">
                    <#list map?keys as key>
                        <option value="${key}"
                                <#if e.couponType?? && e.couponType==key?number>selected="selected" </#if>>${map[key]}</option>
                    </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">抵价值/折扣值</td>
            <td style="text-align: left;">
                <input type="text" value="${e.couponValue!""}" data-rule="抵价值/折扣值:required;" name="couponValue" id="couponValue" onblur="checkCouponValue()">
            </td>
            <td style="text-align: right;">生成数量</td>
            <td colspan="4"><input type="text" data-rule="生成数量:required;integer[+];" value="${e.couponCount!""}" name="couponCount" id="couponCount"></td>
        </tr>
        <tr>
            <td style="text-align: right;">有效期</td>
            <td style="text-align: left;">
                <input id="startTime" class="Wdate search-query input-small" type="text" name="startTime"
                       value="${e.startTime!""}" data-rule="有效期:required;"
                       onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'endTime\')||\'2020-10-01\'}'})"/>~
                <input id="endTime" class="Wdate search-query input-small" type="text" name="endTime"
                       value="${e.endTime!""}" data-rule="有效期:required;"
                       onFocus="WdatePicker({minDate:'#F{$dp.$D(\'startTime\')||\'2020-10-01\'}'})"/></td>
            <td style="text-align: right;">是否启用</td>
            <td style="text-align: left;" colspan="3">
                <ul class="ul_style_5" style="list-style-type:none">
                    <li class="li_style_16">
                        <input type="radio" name="enableStatus" value="1"
                               <#if e.enableStatus?? && e.enableStatus==1>checked="checked"</#if>> 是
                    </li>
                    <li class="li_style_15">
                        <input type="radio" name="enableStatus" value="0"
                               <#if e.enableStatus?? && e.enableStatus==0>checked="checked"</#if>> 否
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    function doSubmitFunc(obj) {
        var m = $(obj).attr("name");
        $("#form").on("valid.form", function (e, form) {
            var _formAction = $("#form").attr("action");
            var aa = _formAction.substring(0, _formAction.lastIndexOf("/") + 1);
            var lastFormAction = aa + m.split(":")[1] + ".action";
            _formAction.attr("action", lastFormAction);
        });
    }
    function doSubmitFuncByLink(obj) {
        var _href = $(obj).attr("href");
        var _form = $("#form");
        _form.attr("action", _href);
        _form.on("valid.form", function (e, form) {
            console.log("this.isValid=" + this.isValid);
        });
        return false;
    }
    //校验优惠券
    function checkCouponValue(){
    	var couponValue=$('#couponValue').val();
    	if($('#couponType').val() == 1){//抵价券
    	  	var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,9})?$/;
    	    if(!exp.test(couponValue) || parseFloat(couponValue) <=0 ){
    	    	alert('抵价券应该是一个数字');
    	    	$('#couponValue').val('');
    	    }
    		
    	}
    	if($('#couponType').val() == 2){//折扣券
    		var exp = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;
    	    if(!exp.test(couponValue) || couponValue>10 || couponValue < 0){
    	    	alert('折扣券的折扣应   大于 0 小于 10');
    	    	$('#couponValue').val('');
    	    }
    	}
    
    }
</script>
</@page.pageBase>