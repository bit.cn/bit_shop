<#import "/manage/tpl/pageBase.ftl" as page>
<@page.pageBase currentMenu="现货出单">
<style type="text/css">
.titleCss {
	background-color: #e6e6e6;
	border: solid 1px #e6e6e6;
	position: relative;
	margin: -1px 0 0 0;
	line-height: 32px;
	text-align: left;
}

.aCss {
	overflow: hidden;
	word-break: keep-all;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: left;
	font-size: 12px;
}

.liCss {
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 30px;
	text-align: left;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<script type="text/javascript">
	$(function() {
		$("#treegrid").treegrid({"treeColumn":1});
		
		function c1(f) {
			$(":checkbox").each(function() {
				$(this).attr("checked", f);
			});
		}
		$("#firstCheckbox").click(function() {
			if ($(this).attr("checked")) {
				c1(true);
			} else {
				c1(false);
			}
		});
	});
	function deleteSelect() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定删除选择的记录?");
	}
	function updateInBlackList() {
		if ($("input:checked").size() == 0) {
			return false;
		}
		return confirm("确定将选择的记录拉入新闻黑名单吗?");
	}
	
	//导出Excel
	function exportExcel(){
		var _url = "${systemSetting().center}/manage/orderManage/exportExcels";
		var list=$("#htmlTable").val($("#treegrid").prop("outerHTML"));
		var _form = $("form");
		_form.attr("action",_url);
		_form.submit();
	}
</script>
	<form action="${systemSetting().center}/manage/orderManage" method="post" theme="simple">
		<table class="table table-bordered">
		<tr>
				
				<td style="width: 8%;">时间范围</td>
				<td><input id="d4311" class="Wdate search-query input-small" type="text" name="startDate"
					value="${e.startDate!""}" value="${e.startDate!""}" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2100-10-01 01:00:00'})"/>
					~ 
					<input id="d4312" class="Wdate search-query input-small" type="text" name="endDate"
					value="${e.endDate!""}"
					onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2100-10-01 24:00:00'})"/>
				</td>
			</tr>
		
			<tr>
				<td colspan="14">
						
						<button type="button" method="exportExcels" class="btn btn-primary" onclick="exportExcel(this)">
							<i class="icon-search icon-white"></i> 导出
						</button>
	
				</td>
			</tr>
		</table>
		
	
	</form>
	
	
	<link rel="stylesheet" type="text/css" href="${systemSetting().staticSource}/jquery-treegrid/css/jquery.treegrid.css">
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.js"></script>
	<script type="text/javascript" src="${systemSetting().staticSource}/jquery-treegrid/jquery.treegrid.bootstrap3.js"></script>
</@page.pageBase>