package com.soloyogame.anitoys.my.web.listener;

public interface CallBack {
	String callback() throws Exception;
}
