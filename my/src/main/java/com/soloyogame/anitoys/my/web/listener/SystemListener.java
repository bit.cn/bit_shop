package com.soloyogame.anitoys.my.web.listener;

import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.soloyogame.anitoys.db.dao.OrderDetailDao;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 系统配置加载监听器
 * @author shaojian
 */
public class SystemListener implements ServletContextListener
{
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SystemListener.class);
	public void contextDestroyed(ServletContextEvent arg0)
	{

	}

	public void contextInitialized(ServletContextEvent arg0)
	{
		try 
		{
			WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(arg0.getServletContext());
//			OrderDetailDao orderdetailDao = (OrderDetailDao)app.getBean("orderdetailDaoFront");
//			System.out.println("orderdetailDao"+orderdetailDao.toString());
		} 
		catch (Throwable e) 
		{
			e.printStackTrace();
			logger.error("System load faild!"+e.getMessage());
			try 
			{
				throw new Exception("系统初始化失败！");
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
	}

}
