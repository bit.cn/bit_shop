package com.soloyogame.anitoys.my.web.controller;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

import java.util.List;

import com.soloyogame.anitoys.db.commond.KeyValueHelper;

/**
 * freemark 全局变量
 * @author 索罗游
 */
public class KeyValueGetter implements TemplateMethodModelEx {
    @Override
    public Object exec(List arguments) throws TemplateModelException {
        return KeyValueHelper.get(arguments.get(0).toString());
    }
}
