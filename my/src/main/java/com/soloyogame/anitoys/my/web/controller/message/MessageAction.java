package com.soloyogame.anitoys.my.web.controller.message;


import javax.servlet.http.HttpServletRequest;

import com.soloyogame.anitoys.core.Services;
import com.soloyogame.anitoys.db.commond.*;
import com.soloyogame.anitoys.db.page.PagerModel;
import com.soloyogame.anitoys.my.web.controller.front.FrontBaseController;
import com.soloyogame.anitoys.service.HelpService;
import com.soloyogame.anitoys.service.MessageService;
import com.soloyogame.anitoys.service.ProductService;
import com.soloyogame.anitoys.util.cache.RedisCacheProvider;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;



/**
 * 娑堟伅 Controller
 *
 * @author ZGY
 */
@Controller("frontMessageController")
@RequestMapping("messageAction")
public class MessageAction extends FrontBaseController<Message> {
    private static final org.slf4j.Logger logger = LoggerFactory
            .getLogger(MessageAction.class);
    @Autowired
    MessageService messageService;
    @Autowired
    private ProductService productService;
    @Autowired
    private RedisCacheProvider redisCacheProvider;
    @Autowired
    private HelpService helpService;

    @Override
    public Services<Message> getService() {
        return messageService;
    }

    /**
     * 我的消息
     * @param request
     * @param model
     * @param e
     * @return
     */
    @RequestMapping("my_information")
    public String my_information(HttpServletRequest request, ModelMap model, Message e) 
    {
        logger.error("----------------------->message->my message");
        // 获取用户ID
        Account account = getLoginAccount();
        int offset = 0;// 分页偏移量
        if (request.getParameter("pager.offset") != null) 
        {
            offset = Integer.parseInt(request.getParameter("pager.offset"));
        }
        if (offset < 0) 
        {
            offset = 0;
        }
        e.setAccountId(account.getId());
        PagerModel pager = messageService.selectFrontPageList(e);
        pager.setPageSize(5);
        pager.setPagerSize((pager.getTotal() + pager.getPageSize() - 1)
                / pager.getPageSize());
        pager.setPagerUrl("my_information");
        model.addAttribute("pager", pager);

        if (account == null || StringUtils.isBlank(account.getAccount())) 
        {
            return page_toLoginRedirect;
        }
        int productCount = productService.selectProductCount();
        model.addAttribute("productCount", productCount);
        return "/account/my_information";
    }

}
