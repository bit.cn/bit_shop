<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/account/myLeft.ftl" as myLeft>
<#import "/ftl/top3.ftl" as myTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
 	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/bootstrap.min.css">
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/pay.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/downOrder.css" rel="stylesheet" type="text/css" />
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/shoppingCart1.css" rel="stylesheet" type="text/css" />
	<title>补款订单</title>
		<style type="text/css">
		.addMySiteButton{
			width:120px;
			height:30px;
			border-style: none;
		    padding: 8px 30px;
		    line-height: 24px;
		    font: 12px "Microsoft YaHei", Verdana, Geneva, sans-serif;
		    cursor: pointer;
		    -webkit-box-shadow: inset 0px 0px 1px #fff;
		    -moz-box-shadow: inset 0px 0px 1px #fff;
		    box-shadow: inset 0px 0px 1px #fff;background-color: #FF6600;
		    color: #FFFFFF;
		    border-radius: 4px;
		}
.aft_sum{
			color:#FF8000; 
			font-weight:bold; 
			font-size:16px; 
			margin-right:40px;
		}
</style>
</head>

<body>
	<!--商城主页top页-->
	<@myTop.shopTop/>
	<div class="div_t4">
		<div>
			<img src="${systemSetting().staticSource}/plac/image/payment.png" />
		</div>
		<div width="100%" style="padding-bottom: 10px">
			<hr color="#CCCCCC">
		</div>
		<div>
			<table>
				<tr>
					<td class="td_t10" style="color: #FFFFFF;">1.确认订单信息</td>
					<td class="td_t11">2.付款</td>
					<td class="td_t12">3.购物成功</td>
				</tr>
			</table>
		</div>
		<div class="div_style_4" style="margin-top:10px">
					<p>确认收货地址&nbsp;</p>
					<input type="button" class="addMySiteButton" value="添加新地址" onclick="addMysite()">
				<div class="site_save" width="100%" style="padding-left: 5px;">
					<input type="hidden" id="adds" value="${addressListSize!0}"/>
					<#list addressList as address> 
						<#if address_index <=3 >
							<div class="inputa" style=" width:800px ;">
								<input class="addressId" name="Fruit" type="radio" value="${address.id}" ${(address.isdefault=="y")?string("checked","") } />
									&nbsp;${address.pcadetail!''}&nbsp;${address.address!''}&nbsp;(${address.name!''} 收)&nbsp;${address.phone!''}&nbsp;${address.mobile!''}
							</div><br> 
						<#else>
							<div name="addressxs" class="inputa" style="visibility: none;display: none; width:800px ;">
								<input name="Fruit" type="radio" value="${address.id}" ${(address.isdefault=="y")?string("checked","") } />&nbsp;${address.pcadetail!''}&nbsp;${address.address!''}&nbsp;(${address.name!''} 收)&nbsp;${address.phone!''}&nbsp;${address.mobile!''}
							</div>
							<br name="addressxs" style="visibility: none;display: none;">
						</#if> 
					</#list> 
					<a herf="" name="addressxs" onclick="valid1()">更多</a>
					<a herf="" name="addressyc" style="visibility: none;display: none;" onclick="none()">收起</a>
				</div>
		<div style="border:solid 0px; height: 30px; line-height: 20px; padding-top:5px; padding-left: 5px">
			<p>
				商品清单&nbsp;<span class="span_style_2">当下单的商品包含预售商品和现货商品，以及商品来自不同的仓库的时候会被拆分为数个订单，请注意</span>
			</p>
		</div>
		<div class="div_style_4">
			<div width="100%" style="background: #F2F2F2; margin-bottom: 5px;">
				<#if orderDetail??>
				<input type="hidden" value="${order.orderCode}" name="orderCode" id="orderCode" />
				<table class="table_style_4" width="100%">
					<tr class="tr_t1">
							<td style="text-align:center;width:40%;font-weight: bold;">商品</td>
							<td style="text-align:left;width:15%;font-weight: bold">款式</td>
							<td style="text-align:left;width:15%;font-weight: bold">成交单价</td>
							<td style="text-align:left;width:15%;font-weight: bold">数量</td>
							<td style="text-align:left;width:15%;font-weight: bold;padding-left: 40px">小计</td>
						</tr>
					<tr class="head_tb_tr" style="border:solid 0px;">
						<td colspan="5" class="left" style="padding-left: 5px;vertical-align: middle; height:30px">
							<span style=""> 商家名称: ${order.businessName!""} </span>
						</td>
					</tr>
					<tr style="border-bottom:1px #C2C2C2 dashed;">
						<td style="text-align:center;width:40%;font-weight: bold;">
							<div width="100%" height="100%" style="padding: 5px;position:relative">
								<table>
									<tr>
										<td>
										<a style="float:left;width:27%;margin-right:2%;margin-top:2%;margin-bottom:2%;text-align:center;" 
												href="${systemSetting().item}/product/${orderDetail.productID!""}.html" target="_blank"
												title="${orderDetail.productName!""}">
												<img src="${systemSetting().imageRootPath}${orderDetail.picture!""}" width="60px" height="70px" />
											</a>
										</td>
										<td style="text-align:center">
											<a class="td_t3" style="position:absolute;align:middle;margin: auto;">
											</a>
											<div style="padding-left: 10px; text-align: left;">
														<a href="${systemSetting().item}/product/${orderDetail.productID!""}.html" target="_blank" title="${orderDetail.productName!""}">
															<#if orderDetail.productName??>
																<#if orderDetail.productName?length gte 50>
																	${orderDetail.productName?substring(0,50)}...
																<#else>
																	${orderDetail.productName!""}
																</#if>
															</#if>
														</a>
														<br/>
														<br/>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</td>
						<td style="text-align:left;width:15%;font-weight: bold">
							<br>${orderDetail.specSize!""}<br />
							<p class="p_style_2">${orderDetail.specColor!""}</p> 
						</td>
						<td style="text-align:left;width:15%;font-weight: bold">￥${orderDetail.price}</td>
						<td style="text-align:left;width:15%;font-weight: bold">${orderDetail.number}</td>
						<td class="td_t3"><b style="font-size:18px;">￥${orderDetail.total0!"0.0"}</b></td>
					</tr>
					<tr>
						<td colspan="5">
							<table width="100%" cellpadding="0" cellspacing="0">
											<tbody><tr><td width="64%">&nbsp;</td>
											<td width="6%" align="right">快递方式:&nbsp;</td>
											<td width="10%">
												<select style="margin-left:13px;" class="ipt-text-sm shipType" name="sel" id="shipping${order.orderCodeView}" onchange="changeShipping('${order.orderCodeView}')">
													<option value="-1">请选择快递方式</option>
													<#list order.businessShippingList as   businessShippingItem>
														<option value="${businessShippingItem.shippingId!""}">${businessShippingItem.shippingName!""}</option>
													</#list>
												</select>
											</td>
											<td width="5%">运费:</td>
											<td style="padding-left: 40px">
												<label class="aft_sum">￥<span id="shipmoney${order.orderCodeView!""}">0.0</span></label>
											</td>
										</tr></tbody></table>
						</td>
					</tr>
					<tr>
						<td colspan="5">
										<table width="100%" cellpadding="0" cellspacing="0">
											<tbody><tr><td width="61%">&nbsp;</td>
											<td width="10%" align="right">商家优惠券:</td>
											<td width="10%">
												<select id="couponSn${order.orderCodeView}" class="ipt-text-sm" name="sel" onchange="changeCouponSn('${order.orderCodeView}')">
													<option value="-1">未使用优惠券</option>
													<#list order.accountCouponList as accountCoupon>
														<option value="${accountCoupon.id!""}">${accountCoupon.couponName!""}</option>
													</#list>
												</select>
											</td>
											<td width="5%">&nbsp;&nbsp;&nbsp;</td>
											<td class="td_t3">
												<label class="aft_sum">￥<span id="money${order.orderCodeView!""}"><#if order.fee??>${order.fee!""}<#else>0.0</#if></span></label>
											</td>
										</tr>
									</tbody>
								</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="left" width="64%">
							<p>
								<a class="td_t3" style="margin-left:13px;">订单备注：</a> 
										<input id="otherRequirement" class="textarea_style_1" 
												type="text" name="otherRequirement" placeholder="请输入与商家达成一致的要求！"
												value="${order.otherRequirement!''}"/>
							</p>
						</td>
						<td colspan="2" class="td_t6">${order.quantity}件商品&nbsp;&nbsp;合计补款金额：</td>
						<td class="td_t3" style="padding-left: 40px">
							<input id="ptotal${order.orderCodeView}" value="${order.balancePrice!0}" style="display: none;" />
							<input id="sontotal${order.orderCodeView}" value="${order.amount!""}" style="display: none;" />
							<label class="aft_sum" style="color:red"> ￥<span name="heji" id="ptotalVal${order.orderCodeView}">${order.balancePrice!0}</span></label>
						</td>
					</tr>
					<tr style="height:8px;border-bottom:solid 1px #C1C1C1;">
									
					</tr>
					<tr>
						<td colspan="5" height="5" style="background-color: white"></td>
					</tr>
					</#if>
				</table>
			</div>
			<div width="100%" style="background: #F2F2F2; margin-bottom: 5px;">
				<table width="100%">
					<tr> 
								<td style="width:40%"></td> 
								<td style=""></td> 
								<td style="width:15%"></td> 
								<td class="td_t6" style="width:10%" colspan="1"></td> 
								<td class="td_t3" style="text-align:left;width:11%"></td>
							</tr> 
					<tr>
						<td colspan="4" class="td_t6">（当前用户积分剩余<label id="scoreLabel">${rank!0}</label>分）使用
							<input id="accountScore" type="text" style="width:50px"
									onchange="changeAccountScore();" /> 积分
						</td>
						<td class="td_t3" style="padding-left: 20px;">
							<label class="aft_sum" style="color:red;margin-left:40px;">￥ <span
								id="accountScoreValue">0</span>
							</label>
						</td>
					</tr>
					<tr>
						<td colspan="4" class="td_t6">平台抵扣券：
							<select id="accountVoucher" name="sel" onchange="changeAccountVoucher();"> 
								<#list order.accountVoucherList as accountVoucherItem>
									<option value="${accountVoucherItem.id!""},${accountVoucherItem.couponValue!""}">${accountVoucherItem.couponName!""}</option>
								</#list>
							</select>
						</td>
						<td class="td_t3" style="padding-left: 20px;"> 
							<label class="aft_sum" style="color:red;margin-left:40px;">￥
								<span class="span_style_2"></span> 
								<#if order.accountVoucherList[0]?? > 
								<span class="span_style_2" id="voucherMoney">${order.accountVoucherList[0].couponValue!0}</span>
								</#if>
							</label>
						</td>
					</tr>
				</table>
			</div>
			<p><span class="span_style_2" style="padding-left: 5px">下单后如果取消订单，所使用的积分和优惠券将不再恢复</span></p>
			<table class="table_style_3">
				<tr>
					<td style="width: 200px">应付金额: 
						<span style="font-size:18px;color:red;font-weight:bold"> ￥ 
							<span style="font-size:18px;color:red;font-weight:bold"
									id="parentAmount">${order.amount!0}
							</span>
						</span>
					</td>
					<td class="td_t1" style="padding-right:20px;">
					<input type="button" class="btn-style-01" value="确认支付" onclick="saveOrder('${order.orderCodeView}')" /></td>
				</tr>
				<tr>
					<td colspan="2" style="height:20px"></td>
				</tr>
			</table>
		</div>
	</div>
	<form id="formToPlacPay" method="post" action="${systemSetting().orders}/order/plac_pay">
		<input type="hidden" name="placPayInput" />
	</form>
	</div>
<!-- Modal 添加收获地址弹出层 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>添加收货地址:</b></h4>
            </div>
            <div class="modal-body" style="color: #7ABD54;font:normal 24px">
                <h3>
                <div class="head_add">
				<p>
					<label id="labelIN" class="site_add_text">新增收货地址</label> 
					
					<label class="site_add_text1">电话号码、手机号选填一项，其余均为必填项</label>
				</p>
 				<form role="form" id="form1" class="form-horizontal" method="post" action="" theme="simple">
				<input type="text" id="id" name="id"   style="visibility: none;display: none;" 	/>
				<ul class="add_site">
					<li>
						所在地区 <label class="site_add_text">*</label> 
						 <select name="province" id="province" class="form-control1" onchange="changeProvince()">
						<option value="">--选择省份--</option>
							<#list provinces as item>
								<option value="${item.code}"  >${item.name}</option>
						</#list>
						</select>	
						<select class="form-control1" id="citySelect" name="city"  onchange="changeCity()">
								<option value="">--选择城市--</option>
									<#list cities as item>
										<option value="${item.code}"  >${item.name}</option>
									</#list>
								</select>
						  <select class="form-control1" id="areaSelect" name="area">
		                                <option value="">--选择区县--</option>
										<#list areas as item>
											<option value="${item.code}"  >${item.name}</option>
										</#list>
		                  </select>
		                  <span id="areaSelectV" style="visibility: none;display: none;color:red">&nbsp;请正确选择所在地区</span>		
					</li>
					<li>
						<div class="add_site_d">
							详细地址<label class="site_add_text"> *</label> 
						</div>
						<textarea id="address" maxlength="100" name="address" rows="5" cols="50" class="ta"   placeholder="建议你如实填写详细的收货地址，例如街道名称，门牌号，楼层和房间号等信息"  ></textarea>
						
						
						<span id="addressV" style="visibility: none;display: none;color:red">&nbsp;请正确填写你详细地址</span>		
					</li>
					<li>
						邮政编码<label class="site_add_text"> *</label>  
						<input type="text" id="zip" name="zip"   style="width:360px;height:20px"  maxlength="7"   placeholder="如果你不清楚邮递区号，请填写000000"/>
						<span id="zipV" style="visibility: none;display: none;color:red">&nbsp;请正确填写邮政编码</span>		
					</li>
					<li class="site_name">
						收货人姓名
						<label class="site_add_text">
							*
						</label> 
						<input id="name" name="name" type="text"  style="width:360px;height:20px"   placeholder="长度不超过25个字符"   maxlength="25" />
						<span id="nameV" style="visibility: none;display: none;color:red">&nbsp;请正确填写收货人姓名</span>		
						 
					</li>
					<li>
						手机号码
						<label class="site_add_text">
							*
						</label> 
						<input  id="phone" name="phone"  type="text"   style="width:360px;height:20px"   
						placeholder="电话号码。手机必须填一项" />
						 <span id="phoneV" style="visibility: none;display: none;color:red">&nbsp;电话号码，手机必须填一项</span>		
						 <span id="phoneV2" style="visibility: none;display: none;color:red">&nbsp; 请正确填写手机号码</span>
					</li>
					<li>
						电话号码
						<label class="site_add_text">
							*
						</label> 
						<input id="mobile" name="mobile" type="text"   style="width:360px;height:20px"      placeholder="电话号码，手机必须填一项" />
					 	<span id="mobileV" style="visibility: none;display: none;color:red">&nbsp;电话号码，手机必须填一项</span>		
					</li>
					<li>
						<p style="height:12px;">
						</p>
						<input type="checkbox"  id="isdefault" name="isdefault" class="site_ck"/>
						设置为默认收货地址
						<br />
						<input  id="bt" type="button" class="sit_ok" value="保存" onclick ="valid();" />
					</li>
				</ul>
				</form>
			</div>
			</div>
                </h3>
            </div>
        </div>
    </div>
</div>
 <!-- 商城主页footer页 -->
  <@shopFooter.shopFooter/>
  <script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
  <script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/hover.js"></script>
<script type="text/javascript">

	var shippingFlag = "Y";

 	function toPlacPay() {
//		$("#formPlacPay :hidden[name=placPayInput]").val(productId);
 		$("#formToPlacPay").submit();
 	}

	//序列話
	$.fn.serializeObject = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	function saveOrder(sonorderCode) {
		var orderCode = eval(document.getElementById("orderCode")).value;
		var accountVoucher = $('#accountVoucher option:selected').val();
		var parentAmount = $("#parentAmount").html();
		// 配送方式ID
		var expressCode = $("#shipping" + sonorderCode).val();
		var expressName = $("#shipping" + sonorderCode).find("option:selected").text();
		var fee = $("#shipmoney"+ sonorderCode).text();
		//优惠券信息
		var couponsId = $("#couponSn"+ sonorderCode).val();
		var couponValue = $("#money"+ sonorderCode).text();
		
		var otherRequirement =$("#otherRequirement").val();
		var ptotal=$("#ptotal"+ sonorderCode).text();
		
		var addressId = $("input[name='Fruit']:checked").val();
		var point = $("#accountScoreValue").text();
		var usedScore = $("#accountScore").val();
		$.ajax({
			url : "${basepath}/order/pay",
			type : 'POST',
			//		dataType:'JSON',
			data : {
				orderCode : orderCode,
				expressCode : expressCode,
				expressName : expressName,
				fee:fee,
				couponsId:couponsId,
				couponValue:couponValue,
				otherRequirement:otherRequirement,
				accountVoucher : accountVoucher,
				ptotal,ptotal,
				parentAmount : parentAmount,
				intoType : "placAdd",
				addressId:addressId,
				point:point,
				usedScore:usedScore
				
			},
			success : function(data) {
				//alert("success");
				toPlacPay();
			},
			error : function(error) {
				console.log(error);
			}
		})
	}

	function valid1() {
		obj = document.getElementsByName("addressxs");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "visible";//显示;
			obj[i].style.display = "";//显示
		}
		obj = document.getElementsByName("addressyc");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "visible";//显示;
			obj[i].style.display = "";//显示
		}
	}
	
	function none() {
		obj = document.getElementsByName("addressxs");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "addressyc";//显示;
			obj[i].style.display = "";//显示
		}
		obj = document.getElementsByName("addressyc");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "";//显示;
			obj[i].style.display = "none";//显示
		}
	}
	
	//添加新地址
	function addMysite(){
		$('#myModal').modal('toggle');
	}

	//  选择优惠券
	function changeCouponSn(sonorderCode) {

		var couponId = $("#couponSn" + sonorderCode).val();
		var oldSonAmount = parseFloat($("#sontotal" + sonorderCode).val());
		var orderCode = $("#orderCode").val();
		if(couponId==-1){
			var discountFee = 0;
			var shipmoney = 0;
			if($("#shipmoney" + sonorderCode).text()!="") {
				var shipmoney = parseFloat($("#shipmoney" + sonorderCode).text());
			}
			var sonAmount = oldSonAmount+shipmoney-parseFloat(discountFee);
			$("#ptotalVal" + sonorderCode).text(sonAmount.toFixed(2));
			$("#money" + sonorderCode).text(parseFloat(discountFee).toFixed(2));
			$("#tab_cpValue" + sonorderCode).html("");
			$("#tab_cpSn" + sonorderCode).html("");
			//重新计算合计金额
			heji();
		}
		// 优惠券使用check
		var couponTabArr = $(".tab_cpSnId");
		for (var i = 0; i < couponTabArr.length; i++) {
			var couponTab = couponTabArr[i];
			if($(couponTab).text()==couponId) {
				alert("优惠券已被使用！");
				return;
			}
		}
		
		$.ajax({
			url : "${systemSetting().my}/order/getCoupon",
			type : 'POST',
			dataType : 'JSON',
			data : {
				couponId : couponId,
				sonorderCode : sonorderCode,
				orderCode:orderCode
			},
			success : function(data) {
				
				if(data.result=="needLogin") {
					alert("登录失效，请重新登录!");
				} else if (data.result=="couponUsed"){
					alert("优惠券已被使用!");
					return;
				}
				var shipmoney = 0;
				if($("#shipmoney" + sonorderCode).text()!="") {
					var shipmoney = parseFloat($("#shipmoney" + sonorderCode).text());
				}
				var sonAmount = parseFloat(data.result)+shipmoney;
				$("#ptotalVal" + sonorderCode).text(sonAmount.toFixed(2));
				$("#money" + sonorderCode).text((oldSonAmount-parseFloat(data.result)).toFixed(2));
				$("#tab_cpValue" + sonorderCode).html(oldSonAmount-parseFloat(data.result));
				$("#tab_cpSn" + sonorderCode).html(couponId);
				//重新计算合计金额
				heji();
			},
			error : function(error) {
				alert("error:" + error);
			}
		})
	}

	 //切换运送方式
	function changeShipping(sonorderCode) {
		
		// 配送方式ID
		var shippingId = $("#shipping" + sonorderCode).val();
		//子订单号
		var orderCode = $("#orderCode").val();
		// 订单原金额
		var oldSonAmount = parseFloat($("#sontotal" + sonorderCode).val());
		var addressId = $("input[name='Fruit']:checked").val();
		if (addressId == "" || addressId==null) {
			alert("请选择配送地址!");
			return;
		}
	
		if(shippingId==-1){
			var shipFee = 0;
			$("#shipmoney" + sonorderCode).text(shipFee.toFixed(2));
			var discountFee = parseFloat($("#money" + sonorderCode).text());
			$("#ptotalVal" + sonorderCode).text((oldSonAmount+shipFee-discountFee).toFixed(2));
			$("#tab_fee" + sonorderCode).text(shipFee);
			$("#tab_spId" + sonorderCode).html("");
			$("#tab_spName" + sonorderCode).html("");
			//重新计算合计金额
			heji();
			return;
		}
		$.ajax({
			url : "${systemSetting().my}/order/getShiping",
			type : 'POST',
			dataType : 'JSON',
			data : {
				sonorderCode : sonorderCode,
				shippingId : shippingId,
				addressId : addressId,
				orderCode:orderCode
			},
			success : function(data) {
				//alert("success" + data);
				if(data.result == "noMatch") {
					//alert("noMatch");
					$("#shipmoney" + sonorderCode).text("0");
                    $("#shipping" + sonorderCode +" option:first").prop("selected", 'selected');
					shippingFlag = "noMatch";
					alert("收货地址不在商家配送范围内！请重新选择配送方式！");
				} else {
					shippingFlag = "Y";
					var shipFee = parseFloat(data.result);
					$("#shipmoney" + sonorderCode).text(shipFee.toFixed(2));
					var discountFee = parseFloat($("#money" + sonorderCode).text());
					$("#ptotalVal" + sonorderCode).text((oldSonAmount+shipFee-discountFee).toFixed(2));
					$("#tab_fee" + sonorderCode).text(shipFee);
					$("#tab_spId" + sonorderCode).html(shippingId);
					$("#tab_spName" + sonorderCode).html($("#shipping"+sonorderCode).find("option:selected").text());
					heji();
				}
			},
			error : function(error) {
				alert("系统异常请重新加载此页面，或联系客服！");
			}
		})
		
	}

	//订单备注输入change函数
	function changeOtherRequirement(id) {
		var otherRequirementId = "otherRequirement" + id;
		var otherRequirement = document.getElementById(otherRequirementId).value;
		var table_or = "tab_or" + id;
		document.getElementById(table_or).innerHTML = otherRequirement;
	}

	//积分输入change函数
	function changeAccountScore() {
		var accountScore = $("#accountScore").val();
		var orderId = $("#orderCode").val();
		if(isNaN(accountScore)){
			alert("请输入数字！");
			accountScore = 0;
			$("#accountScore").val("");
			return;
		}
		if(!(/^(\+|-)?\d+$/.test(accountScore))){
    		alert("积分必须是正整数！");
    		accountScore = 0;
    		$("#accountScore").val("");   
    		return ;  
    	}
		if(parseInt(accountScore)<0){
			accountScore = 0;
 			$("#accountScore").val("");
			alert("积分必须输入正数！");
			return;
		}
		var score = $("#scoreLabel").text();
 		if(parseInt(accountScore) > parseInt(score)) {
 			alert("使用积分已超过您拥有积分");
 			accountScore = 0;
 			$("#accountScore").val("");
 		}
 		if(accountScore == null || accountScore == "") {
			accountScore = 0;
		}
		var accountScoreValue = parseFloat(accountScore) / 100;
		$("#accountScoreValue").text(accountScoreValue.toFixed(2));
		heji();
		
		//ajax锁定用户下单的积分
		$.ajax({
			url : "${systemSetting().my}/order/bindingAccountScore",
			type : 'POST',
			dataType : 'JSON',
			data : {
				accountScore : accountScore,
				orderId : orderId
			},
			success : function(data) {
				
				if(data.result=="needLogin") {
					alert("登录失效，请重新登录!");
				} else if (data.result=="couponUsed"){
					alert("抵扣券已被使用!");
					return;
				}
				else if(data.result =="ordercacel"){
					alert("订单失效。请回到购物车重新提交订单！");
					location.href="${systemSetting().card}";
				}
				else{
					$("#scoreLabel").text(data.result);
				}
				//重新计算合计金额
				heji();
			},
			error : function(error) {
				alert("error:" + error);
			}
		})
	}

	//平台抵扣券change函数
	function changeAccountVoucher() {
		var accountVoucherValue = $("#accountVoucher").val();
		if(accountVoucherValue==-1) {
			$("#voucherMoney").text(0);
            heji();
		}
		
		var strs= new Array();
		strs = accountVoucherValue.split(",");
		var deductibleId = strs[0];
		var orderId = $("#orderCode").val();
		
		//ajax得到平台抵扣券
		$.ajax({
			url : "${systemSetting().my}/order/getVoucher",
			type : 'POST',
			dataType : 'JSON',
			data : {
				deductibleId : deductibleId,
				orderId : orderId
			},
			success : function(data) {
				
				if(data.result=="needLogin") {
					alert("登录失效，请重新登录!");
				} else if (data.result=="couponUsed"){
					alert("抵扣券已被使用!");
					return;
				}
				$("#voucherMoney").text( parseFloat(data.result).toFixed(2));
				//重新计算合计金额
				heji();
			},
			error : function(error) {
				alert("error:" + error);
			}
		})
		heji();
	}
	
   //计算合计费用
	function heji() {
		var sonHj = document.getElementsByName("heji");
		var voucherMoney = $("#voucherMoney").text();
		var accountScoreValue = $("#accountScoreValue").text();
		var money = 0;
		var sonTotal = 0;
		for (i = 0; i < sonHj.length; i++) {
			sonTotal = sonTotal + parseFloat(sonHj[i].innerHTML);
		}
		money = money + sonTotal - accountScoreValue - voucherMoney;
		if(parseFloat(money) < 0) {
			$("#parentAmount").text("0");
		} else {
			$("#parentAmount").text( money.toFixed(2));
		}
	}
	
	function changeProvince(){
	var selectVal = $("#province").val();
	if(!selectVal){
		//console.log("return;");
		return;
	}
	var _url = "selectCitysByProvinceCode?provinceCode="+selectVal;
	$("#citySelect").empty().show().append("<option value=''>--选择城市--</option>");
	$("#areaSelect").empty().show().append("<option value=''>--选择区县--</option>");
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  dataType: "json",
	  success: function(data){
		  $.each(data,function(index,value){
			  $("#citySelect").append("<option value='"+value.code+"'>"+value.name+"</option>");
		  });
	  },
	  error:function(er){
	  }
	});
}


function changeCity(){
	var selectProvinceVal = $("#province").val();
	var selectCityVal = $("#citySelect").val();
	if(!selectProvinceVal || !selectCityVal){
		return;
	}
	var _url = "selectAreaListByCityCode?provinceCode="+selectProvinceVal+"&cityCode="+selectCityVal;
	//console.log("_url="+_url);
	$("#areaSelect").empty().show().append("<option value=''>--选择区县--</option>");
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  dataType: "json",
	  success: function(data){
		  $.each(data,function(index,value){
			  $("#areaSelect").append("<option value='"+value.code+"'>"+value.name+"</option>");
		  });
	  },
	  error:function(er){
	  }
	});
}



function valid(){
	 $("#accountV,#trueNameV,#birthdayV,#areaSelectV,#addressDetailV,#txV,#phoneV,#mobileV,#phoneV2,#sexV").hide();
     var province = $("#province").val();
     var citySelect =  $("#citySelect").val();
     var areaSelect = $("#areaSelect").val();
     var zip =$("#zip").val();
     var name =$("#name").val();
     var address =$("#address").val();
     var phone = $("#phone").val();
     var mobile = $("#mobile").val();
     var isSuccess =1;
      if(province=="" ||citySelect==""   ){
	    $("#areaSelectV").show();	//显示
	    isSuccess =0;
     }
     if(address=="" || address==undefined ){
        $("#addressV").show();	//显示
        isSuccess =0;
     }
     var re= /^[1-9][0-9]{5}$/ ;
     if(zip!='000000') {
	     if(zip=="" || !re.test(zip)){
	        $("#zipV").show();	//显示
	        isSuccess =0;
	     }  
     }
     if(name=="" || name==undefined  ){
        $("#nameV").show();	//显示
        isSuccess =0;
     }
     var mobile=mobile.replace(/^\s*|\s*$/g,'');
	 var length=mobile.length;
	 var a=/^(1[3|4|5|8|7])[0-9]{9}$/;
     if(phone!="" &&!a.test(phone)){
        $("#phoneV2").show();	//显示
        $("#phoneV").hide();	// 隐藏
        $("#mobileV").hide();	// 隐藏
        isSuccess =0;
     }
     else if(phone=="" &&mobile =="" ){
        $("#phoneV2").show();	//显示
        $("#phoneV").show();	// 隐藏
        $("#mobileV").hide();	// 隐藏

        isSuccess =0;
     }
     if($("#isdefault").is(':checked')){
     	$("#isdefault").val("y") ;
     }
     
     $.ajax({
	  type: 'POST',
	  url: "${basepath}/order/checkMysite",
	  data: {},
	  dataType: "json",
	  success: function(data){
		if(data.result=="ok"&&isSuccess == 1) {
		  	$("#bt").attr({"disabled":"disabled"})
		  	$("#form1").attr("action","${basepath}/order/saveMysite")
			form1.submit();
		} else {
			alert("您的保存的收货地址已达上限！");
		}
	  },
	  error:function(er){
		  console.log("changeCity error!er = "+er);
	  }
	});
   }
</script>
</@htmlBase.htmlBase>
</body>
</html>