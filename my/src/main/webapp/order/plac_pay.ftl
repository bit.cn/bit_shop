<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/account/myLeft.ftl" as myLeft>
<#import "/ftl/top3.ftl" as myTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
<@htmlBase.htmlBase>
    <link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet"
          type="text/css">
    <link href="${systemSetting().staticSource}/static/frontend/v1/css/shoppingCart1.css" rel="stylesheet"
          type="text/css"/>
    <link href="${systemSetting().staticSource}/static/frontend/v1/css/pay.css" rel="stylesheet" type="text/css">
    <link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
    <link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
    <title>订单详情</title>
    <style type="text/css">
        .hideDlg {
            display: none;
        }

        .showDlg {
            border-width: 1px;
            height: 200px;
            width: 320px;
            position: absolute;
            display: block;
            z-index: 5;
            font-size: 12px;
            padding: 10px;
        }

        .showDeck {
            display: block;
            top: 0px;
            left: 0px;
            margin: 0px;
            padding: 0px;
            width: 100%;
            height: 100%;
            position: absolute;
            z-index: 3;
            background: #cccccc;
        }

        .hideDeck {
            display: none;
        }

        .button_zf {
            width: 100px;
            height: 35px;
            font-size: 14px;
            background-color: #F30;
            color: #FFF;
        }

        .showTop {
            background-image: url("${systemSetting().staticSource}/static/frontend/v1/images/zhezhao_03.png");
            background-repeat: no-repeat;
            width: 557px;
            height: 43px;
            color: #FFF;
            font-size: 16px;
            line-height: 43px;
            padding-left: 15px;

        }

        .showMiddle {
            height: 216px;
            width: 557px;
            background-color: #FFF;
            font-size: 14px;
            color: #696666;
            padding-top: 15px;
        }

        .showMiddle_left {
            float: left;
            margin-left: 60px;
            margin-top: 15px;
            overflow: hidden;
        }

        .showMiddle_right {
            float: left;
            margin-left: 40px;
            margin-top: 10px;
            overflow: hidden;
        }

        .showButton {
            clear: both;
            padding-top: 40px;
            width: 300px;
            margin: 0 auto;
        }

        .showButton1 {
            background-image: url("${systemSetting().staticSource}/static/frontend/v1/images/zhezhao_09.png") no-repeat;
            background-repeat: no-repeat;
            width: 132px;
            height: 40px;
            font-size: 17px;
            color: #696666;
            border: 0px;
            line-height: 40px;
            cursor: pointer;
        }

        .showButton2 {
            background-image: url("${systemSetting().staticSource}/static/frontend/v1/images/zhezhao_11.png");
            background-repeat: no-repeat;
            width: 132px;
            height: 40px;
            font-size: 17px;
            color: #FFFFFF;
            border: 0px;
            line-height: 40px;
            cursor: pointer;
        }

        .showBottom {
            background-image: url("${systemSetting().staticSource}/static/frontend/v1/images/zhezhao_06.jpg");
            background-repeat: no-repeat;
            width: 557px;
            height: 6px;
        }
    </style>
</head>
<body onresize="adjustLocation();">
<!--商城主页top页-->
    <@myTop.shopTop/>
<div class="div_t4">
    <div>
        <p>
            <span class="span_style_1"
                  style="padding-bottom: 100px">当下单的商品包含预售商品和现货商品，以及商品来自不同的仓库的时候会被拆分为数个订单，请注意</span>
        </p>
        <div class="p_style_1">
            <table class="table_style_4" width="100%" border=0>
                <tr class="tr_t1">
                    <th class="td_t3">订单号</th>
                    <th class="td_t4">订单总价</th>
                    <th>订单详情</th>
                </tr>
            </table>
        </div>
        <div class="div_style_4">
            <table width="100%" height="50px" border=0>
                <#if mainOrder??>
                    <tr>
                        <td class="td_t3">${mainOrder.orderCode}</td>
                        <td class="td_t4">￥${mainOrder.amount!"0"}</td>
                        <td style="text-align:center">${mainOrder.remark!""}</td>
                    </tr>
                </#if>
            </table>
        </div>
        <p height="100px" style="padding-top: 10px">
            支付总额&nbsp;￥<span class="span_style_2" id="orderAmount">${mainOrder.amount!"0.00"}</span>
        </p>
        <p>
            <span class="span_style_1">订单将在3-6个小时后自动过期，请及时付款</span>
        </p>
        <div class="div_t3">
            <div>
                <table height="80px">
                    <tr>
                        <td>
                            <p>选择支付方式</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_t5">
                            <input type="checkbox" id="nowMoney" onchange="checkMoney()" name="payType"/>&nbsp;&nbsp;
                            应支付:￥<span class="span_style_2" id="payAccountAmount">${mainOrder.amount!"0.00"}</span>
                            （账户余额:￥<span class="span_style_2" id="accountAmount">${accountAmount!"0.00"}</span>元）
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <table height="80px">
                    <tr>
                        <td>
                            <p>第三方支付方式</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_t5"><input type="checkbox" id="payBao" name="payType"/>&nbsp;&nbsp;
                            支付宝 &nbsp;<img
                                    align="center" src="${systemSetting().staticSource}/plac/image/Alipay.jpg"/>
                            支付:￥<span
                                    class="span_style_2 payBao">${mainOrder.amount}</span>元
                        </td>
                    </tr>
                </table>
                <table class="table_style_1">
                    <tr>
                        <td>应付金额:￥
                            <span class="span_style_2" id="yingfuMoney"
                                  style="font-size:20px">${mainOrder.amount}</span>
                        </td>
                        <td class="td_t2">
                            <input type="submit" class="btn-style-01" value="立即支付" onclick="toPay();"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <input type="hidden" value="${mainOrder.orderCode}" name="orderCode" id="orderCode"/>
    <a href="" target="_blank" id="payState"></a>
</div>
<!-- 支付遮罩层 -->
<div id="divBox" class="hideDlg" style="" onResize="adjustLocation();">
    <div class="showTop">网上支付提示</div>
    <div class="showMiddle">
        <div class="showMiddle_left"><img src="${systemSetting().staticSource}/zhezhao/lodin.gif"></div>
        <div class="showMiddle_right">
            <p>支付完成前，请不要关闭此支付验证窗口。</p>
            <p>支付完成后，请根据您支付的情况点击下面按钮。</p>
        </div>
        <div class="showButton">
            <input type="button" class="showButton1" style="margin-right:20px;" value="支付遇到问题" onClick="cancel();"/>
            <input type="button" class="showButton2" value="支付完成" onClick="cancel();"/>
        </div>
    </div>
    <div class="showBottom"></div>
</div>
<form id="toPaySuccess" method="post" action="${systemSetting().my}/order/paySuccessAgain">
    <input type="hidden" name="paySuccessBtn"/>
</form>
</body>
<!-- 商城主页footer页 -->
    <@shopFooter.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/hover.js"></script>
<script>
    function toPay() {
        if ($("#nowMoney").is(':checked') == false && $("#payBao").is(':checked') == false) {
            alert("请选择支付方式");
            return false;
        }
        var payBao = $(".payBao").text();
        var money = $("#accountAmount").text();
        var yingPay = $("#orderAmount").text();
        if ($("#nowMoney").is(':checked')) {
            if ($("#payBao").is(':checked') == false) {
                if (parseInt(money) < parseInt(yingPay)) {
                    alert("请选择支付宝支付剩余金额");
                    return false;
                }
            }
        } else {
            $(".payBao").html(${mainOrder.amount!'0.00'});
        }

        showDlg();

        var payWay = "accAmount";
        var payBaoAmount = 0;
        var payAccountAmount = 0;
        if ($("#nowMoney").is(':checked') == true && $("#payBao").is(':checked') == false) {
            payWay = "amtWay";
            payAccountAmount = $("#payAccountAmount").text();
        } else if ($("#nowMoney").is(':checked') == false && $("#payBao").is(':checked') == true) {
            payWay = "baoWay";
            payBaoAmount = $(".payBao").text();
        } else if ($("#nowMoney").is(':checked') == true && $("#payBao").is(':checked') == true) {
            payWay = "allWay";
            payAccountAmount = $("#payAccountAmount").text();
            payBaoAmount = $(".payBao").text();
        }

        var orderCode = $("#orderCode").val();
        $.ajax({
            url: "${systemSetting().my}/order/toPay",
            type: 'POST',
            data: {
                orderCode: orderCode,
                payBaoAmount: payBaoAmount,
                payAccountAmount: payAccountAmount,
                payWay: payWay
            },
            success: function (data) {
                var payLink = document.getElementById("payState");
                if (data == "alipayapi") {
                    payLink.href = "${systemSetting().pay}/alipayapi.jsp";
                } else if (data == "paySuccess") {
                    payLink.href = "${systemSetting().pay}/order/paySuccess.html";
                } else {
                    alert("余额积分不够或优惠券已被使用！");
                    return;
                }
                payLink.click();
            },
            error: function (error) {
                alert("网络异常,请重试! ");
                console.log(error);
            }
        })
    }

    function checkMoney() {
        var payBao = $(".payBao").text();
        var money = $("#accountAmount").text();
        var yingPay = $("#orderAmount").text();
        if ($("#nowMoney").is(':checked')) {
            if (parseFloat(yingPay) > parseFloat(money)) {
                $(".payBao").html((yingPay - money).toFixed(2));
                $("#payAccountAmount").html(money);
                return;
            }
            $(".payBao").html(0.00);
            $("#payAccountAmount").html(yingPay);
            return;
        }
        $(".payBao").html((${mainOrder.amount!'0.00'}).toFixed(2));
    }


    function showDlg() {
        //显示遮盖的层
        var objDeck = document.getElementById("deck");
        if (!objDeck) {
            objDeck = document.createElement("div");
            objDeck.id = "deck";
            document.body.appendChild(objDeck);
        }
        objDeck.className = "showDeck";
        objDeck.style.filter = "alpha(opacity=50)";
        objDeck.style.opacity = 40 / 100;
        objDeck.style.MozOpacity = 40 / 100;
        //显示遮盖的层end

        //禁用select
        hideOrShowSelect(true);

        //改变样式
        document.getElementById('divBox').className = 'showDlg';

        //调整位置至居中
        adjustLocation();

    }

    function cancel() {
        document.getElementById('divBox').className = 'hideDlg';
        document.getElementById("deck").className = "hideDeck";
        var orderCode = $("#orderCode").val();
        hideOrShowSelect(false);
        $.ajax({
            url: "${systemSetting().my}/order/getPayStatus",
            type: 'POST',
            //dataType:'JSONp',
            data: {orderCode: orderCode},
            success: function (data) {
                if (data == 1) {
                    $.ajax({
                        url: "${systemSetting().my}/order/paySuccessAgain",
                        type: 'POST',
                        data: {},
                        success: function (data) {
                            var payLink = document.getElementById("payState");
                    		payLink.href = "${systemSetting().pay}/order/paySuccess.html";
                    		payLink.click();
                        },
                        error: function (error) {
                            payFlag = "fail";
                            console.log(error);
                        }
                    });
                }else {
                    alert("支付异常，请重新支付！");
                    return;
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function hideOrShowSelect(v) {
        var allselect = document.getElementsByTagName("select");
        for (var i = 0; i < allselect.length; i++) {
            //allselect[i].style.visibility = (v==true)?"hidden":"visible";
            allselect[i].disabled = (v == true) ? "disabled" : "";
        }
    }

    function adjustLocation() {
        var obox = document.getElementById('divBox');
        if (obox != null && obox.style.display != "none") {
            var w = 557;
            var h = 265;
            var oLeft, oTop;

            if (window.innerWidth) {
                oLeft = window.pageXOffset + (window.innerWidth - w) / 2 + "px";
                oTop = window.pageYOffset + (window.innerHeight - h) / 2 + "px";
            }
            else {
                var dde = document.documentElement;
                oLeft = dde.scrollLeft + (dde.offsetWidth - w) / 2 + "px";
                oTop = dde.scrollTop + (dde.offsetHeight - h) / 2 + "px";
            }

            obox.style.left = oLeft;
            obox.style.top = oTop;
        }
    }
    //支付失败
</script>
</@htmlBase.htmlBase>
</body>
</html>