<#macro shopTop checkLogin=true>
		<div class="topNav">
			<div class="top_main clearfix">
				<div class="top_left">
					<a href="${systemSetting().www}">"话不多说买买买"——欢迎来到anitoys</a>
					 <#if currentAccount()??>
					     <span class="sn-login"">欢迎回来，${currentAccount().account}</span>
					     <a href="${systemSetting().my}/account/exit" class="sn-regist">退出系统</a>
				    <#else >
					 <span>
						 <a href="${systemSetting().my}/account/login">请登陆</a>
						 <a href="${systemSetting().my}/account/register" class="regist">免费注册</a>
					 </span>
					</#if>
				</div>
				<div class="top_right">
					<ul>
                <li>
						<span>
							<a href="${systemSetting().my}/account/orders">我的订单</a>
						</span>
                </li>
                <li>
						<span><a href="${systemSetting().my}/account/orders?status=dbk">待补款订单
                            <div style="position: absolute;top: -5px;left: 85px;background: #ff6600;border-radius: 3px;color: #FFF;padding: 2px 3px;text-align: center;font: 200 8px Arial, Helvetica, sans-serif;">
                            <#if waitOrder?exists>
                            ${waitOrder!""}
                            <#else >
                                0
                            </#if>
                            </div>
                        </a></span>
                </li>
                <li>
						<span>
							<a href="${systemSetting().my}/account/orders?" target="_self" class="tran">我的anitoys</a>
							<div class="top_right_nav">
                                <a href="${systemSetting().my}/account/orders?status=dsh" target="_blank">已发货订单</a>
                                <a href="${systemSetting().my}/account/custService" target="_blank">售后订单</a>
                                <a href="${systemSetting().my}/coupon/coupons?status=0" target="_blank">优惠券</a>
                                <a href="${systemSetting().my}/favorite/searchUserFavoritePro" target="_blank">收藏的商品</a>
                                <a href="${systemSetting().my}/favorite/searchUserFavoriteBusi" target="_blank">收藏的商品</a>
                            </div>
						</span>
                </li>
                <li>
						<span>
							<a href="javascript:void(0);" target="_self" class="tran">帮助支持</a>
							<div class="top_right_nav">
                            <#if helpList??> 
								    <#list helpList as item>
											<a href="${systemSetting().doc}/front/helpsmanage/${item.id}.html" target="_self">
											<#if item.title?length gte 7>
												${item.title?substring(0,7)}...
											<#else>
												${item.title!""}
											</#if>
											
											</a>
									</#list>
                   		 		<#else >
                   		 		</#if>
                            </div>
						</span>
                </li>
                <li>
						<span>
							<a href="javascript:void(0);" target="_blank" class="tran">网站导航</a>
							<div class="top_right_nav imgS">
							<#if businessList??>
                            <#list businessList as item>
                                    <a href="${systemSetting().business}/business/businessSelect?businessId=${item.id!""}"
                                       target="_blank">
                                        <img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"/>
                                    </a>
                            </#list>
                            </#if>
                            </div>
						</span>
                </li>
                <li>
						<span>
						<a href="javascript:void(0);" target="_blank" class="tran">关注anitoys</a>
							<div class="top_right_nav w100">
                                <a href="javascript:void(0);" target="_blank">
                                    <img src="${systemSetting().staticSource}/shopindex/layout/img/pic/towcode.png"/>
                                </a>
                            </div>
						</span>
            	    </li>
            		</ul>
				</div>
			</div>
		</div>
</#macro>