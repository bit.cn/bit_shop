<%@page import="com.soloyogame.anitoys.util.front.FrontContainer"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.soloyogame.anitoys.util.constants.ManageContainer"%>
<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<%=request.getContextPath()%>/resource/404/css/404.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resource/js/jquery-1.8.2.min.js"></script>
<title>您找的页面不存在</title>
<style type="text/css">




.top {
	background: #f2f2f2;
	height: 28px;
	border-bottom: 1px solid #e5e5e5;
	position: relative!important;
	min-width: 990px;
	width: auto;
	z-index: 100000;
}
.top_main {
	width: 1200px;
	margin: 0 auto;
	position: relative;
	clear: both;
}
.tm_left {
	height: 28px;
	line-height: 28px;
	margin-right: 20px;
	color: #999;
	float: left;
}
.tm_left a {
	padding-right: 35px;
	text-align: right;
}

.sn-login {
	margin-left: 250px;
}
.sn-regist {
	color:#F00;
}
.tm_right {
	position: absolute;
	right: 0px;
	top: 5px;
	-webkit-backface-visibility: hidden;
	height: 28px;
	line-height:22px!important;
	
}
.tm_right li {
	float: left;
	position: relative;
}
.tm_right li.grzx {
	padding-left: 24px;
	padding-right: 10px;
	background-position: 0px 0px;
	
}
.tm_right li.order
{padding-right: 10px; position:relative;}

.tm_right li.scj {
	padding-left: 24px;
	padding-right: 10px;
	background-position: 0px -25px;
}

a.xljt
{
	background:url(resource/404/img/jt.png) no-repeat 64px 7px;
	display:block;
	padding-right:10px;
}

.scjtk
{
	display:none;
	position:absolute; 
	left:10px; 
	top:-4px; 
}

.tm_right li.scj:hover .scjtk
{ display:block;}

p.xljt02
{
	background:url(resource/404/img/jt02.png) #fff no-repeat 65px 14px;
	height:29px;
	padding:10px 20px 6px 0px !important;
	line-height:30px;
	border-left:1px solid #e5e5e5;
	border-right:1px solid #e5e5e5;
	margin-left:13px;
	display:inline;
}

.scjtk ul
{
	border-left:1px solid #e5e5e5;
	background:#FFF; 
	border-right:1px solid #e5e5e5; 
	border-bottom:1px solid #e5e5e5;
	padding:0px 5px 10px 10px !important; 
	width:75px; 
	margin-left:5px;
	margin-top:-2px;
}

.bztk ul
{
	border-left:1px solid #e5e5e5;
	background:#FFF; 
	border-right:1px solid #e5e5e5; 
	border-bottom:1px solid #e5e5e5;
	padding:0px 25px 10px 10px !important; 
	width:55px; 
	margin-left:5px;
	margin-top:-2px;
}

.gzani ul
{
	border-left:1px solid #e5e5e5;
	background:#FFF; 
	border-right:1px solid #e5e5e5; 
	border-bottom:1px solid #e5e5e5;
	padding:10px 25px 10px 10px !important; 
	width:85px; 
	margin-left:-25px;
	margin-top:-2px;
}

.gzani ul li
{
	line-height:24px;
	float:none;
	text-align:center;
}

.scjtk ul li,.bztk ul li
{line-height:24px; float:none;}

.tm_right li.bzzc:hover .bztk
{display:block;}

a.xljt02
{
	background:url(resource/404/img/jt.png) no-repeat 50px 7px; 
	display:block; 
	padding-right:10px;
}

.bztk
{
	display:none; 
	position:absolute; 
	left:0px; 
	top:-4px;
}

p.xljt03
{
	background:url(resource/404/img/jt02.png) #fff no-repeat 60px 14px;
	padding:10px 24px 6px 10px!important;
	line-height:30px;
	border-left:1px solid #e5e5e5;
	border-right:1px solid #e5e5e5;
	margin-left:13px;
	display:inline;
}

p.guide
{
	background:url(resource/404/img/jt02.png) #fff no-repeat 60px 14px; 
	height:29px; 
	padding:10px 24px 6px 10px!important; 
	line-height:30px; 
	border-left:1px solid #e5e5e5; 
	border-right:1px solid #e5e5e5; 
	margin-left:13px; 
	display:inline;
}

.tm_right li.wzdh:hover .dhtk
{ display:block;}

.dhtk
{
	display:none;
	position:absolute; 
	left:0px; 
	top:-4px; 
}

.dhtk dl
{ 
	border-left:1px solid #e5e5e5;
	background:#FFF; 
	border-right:1px solid #e5e5e5; 
	border-bottom:1px solid #e5e5e5;
	height:250px;
	width:380px;
	margin-left:-285px;
	padding:10px 0px 10px 0px!important; 
	margin-top:-2px;
}

.dhtk dl dd
{
	line-height:24px;
	float:left;
	position:relative;
	height:150px;
	margin-left:20px;
}

.tm_right li.wzdh:hover .gzani
{ display:block;}

.gzani
{
	display:none; 
	position:absolute; 
	left:10px; 
	top:-4px; 
}

.tm_right li.bzzc {
	padding-left: 24px;
	padding-right: 10px;
	background-position: 0px -50px;
}
.tm_right li.wzdh {
	padding-left: 24px;
	padding-right: 10px;
	background-position: 0px -70px;
}
.tm_right li.zj {
	padding-left: 24px;
	padding-right: 10px;
	background-position: 0px -90px;
}
.grzx, .scj, .bzzc, .wzdh, .zj {
	position:relative;
}










.footer_top a:link,.footer_top a:visited{
	text-decoration: none;
	
}
.footer_top a:hover{
	text-decoration:underline;
}
LI {
	FONT-SIZE: 12px; LIST-STYLE-TYPE: none
}
IMG {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px
}
*{ 
	padding:0px;
	margin:0px;
}
body {
	background: #f3f3f3;
}
.footer_top {
	background: #5c5c5c;
	padding: 10px 0;
	clear: both;
	overflow:hidden;
}
.ftmain {
	width: 1200px;
	margin: 0 auto;
}
.ftleft {
	float: left;
	width: 785px;
}
.ftleft ul li {
	float: left;
	margin-left: 10px;
	margin-right: 75px;
	
}
.ftleft ul li h3 {
	color: #dedede;
	font-size: 12px;
	font-family: Verdana, Geneva, sans-serif;
	padding-bottom: 10px;
	padding-top: 0px;
}
.ftleft ul li a {
	display: block;
	line-height: 18px;
	color: #cccccc;
}
.ftphone {
	width: 412px;
	height:110px;
	float:right;
}
.ftp {
	background: url(<%=request.getContextPath()%>/resource/404/img/logo.gif) no-repeat left top;
	background-position:left 10px;
	width: 412px;
	height:100px;
}
.footer_bottom {
	background: #2b2a2a;
	text-align: center;
	padding: 15px 0;
	color: #898989;
	clear:both;
	font-size:12px;
}
.footer02 .footer_top {
	width:1200px;
	margin:0 auto;
	background: #ffffff;
	padding: 25px 0;
	clear: both;
	border-top:1px solid #dddddd;
	border-bottom:1px solid #dddddd;
}
.footer02 .ftleft ul li h3 {
	color: #666666;
	font-size: 12px;
	font-family: Verdana, Geneva, sans-serif;
	padding-bottom: 10px;
	padding-top: 10px;
	font-weight:bold;
}
.footer02 .ftleft ul li a {
	display: block;
	line-height: 18px;
	color: #999999;
}
.footer02 .ftphone {
	float: left;
	width: 200px;
	background: #ffffff;
	padding: 10px 15px;
	border:1px solid #dddddd;
}
.footer02 .ftp {
	background: url(<%=request.getContextPath()%>/resource/404/img/tel02.gif) no-repeat left top;
	padding-left: 60px;
	padding-bottom: 5px;
}
.footer02 .ftp p {
	color: #666666;
	padding-top: 10px;
}
.footer02 .ftp strong {
	color: #333333;
	font-weight: lighter;
	font-size: 18px;
}
.footer02 .footer_bottom {
	background: #ffffff;
	text-align: center;
	padding: 15px 0;
	color: #666666;
}
</style>
</head>

<body>
    <div class="top">
  <div class="top_main">
    <p class="tm_left"><em><a href="#" target="_balnk">&quot;话不多说买买买&quot;——欢迎来到anitoys</a></em><a href="#" target="_blank" class="sn-login">请登录</a><a href="#" target="_blank" class="sn-regist">免费注册</a></p>
    <div class="tm_right">
      <ul>
        <li class="order"><a href="#">我的订单</a></li>
        <li class="grzx"><a href="#">待补款订单<div class="bkddnum">2</div></a></li>
        <li class="scj"><a href="#" class="xljt">我的anitoys</a>
          <div class="scjtk">
            <p class="xljt02">我的anitoys</p>
            <ul>
              <li><a href="#" target="_blank">已发货订单</a></li>
              <li><a href="#" target="_blank">售后订单</a></li>
              <li><a href="#" target="_blank">优惠券</a></li>
              <li><a href="#" target="_blank">收藏夹</a></li>
            </ul>
          </div>
        </li>
        <li class="bzzc"><a href="#" class="xljt02">帮助支持</a>
          <div class="bztk">
            <p class="xljt03">帮助支持</p>
            <ul>
              <li><a href="#" target="_blank">帮助中心</a></li>
              <li><a href="#" target="_blank">在线客服</a></li>
              <li><a href="#" target="_blank">客服邮箱</a></li>
            </ul>
          </div>
        </li>
        <li class="wzdh"><a href="#" class="xljt02">网站导航</a>
          <div class="dhtk">
            <p class="guide">网站导航</p>
            <dl>
              <dd><a href="#" target="_blank"><img src="<%=request.getContextPath()%>/resource/404/img/pic/anitoysimg_11.jpg" width="100" height="250"></a></dd>
              <dd><a href="#" target="_blank"><img src="<%=request.getContextPath()%>/resource/404/img/pic/anitoysimg_07.jpg" width="100" height="250"></a></dd>
              <dd><a href="#" target="_blank"><img src="<%=request.getContextPath()%>/resource/404/img/pic/anitoysimg_13.jpg" width="100" height="250"></a></dd>
            </dl>
          </div>
        </li>
        <li class="wzdh"><a href="#" class="xljt">关注anitoys</a>
          <div class="gzani">
            <p class="xljt02">关注anitoys</p>
            <ul>
              <li><img src="<%=request.getContextPath()%>/resource/404/img/pic/tj05.jpg" width="100" height="100"></li>
              <br/>
              <li><img src="<%=request.getContextPath()%>/resource/404/img/pic/tj05.jpg" width="100" height="100"></li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="headbg">
  <div class="header">
    <div class="head_img"><img src="<%=request.getContextPath()%>/resource/404/img/topimg.jpg" width="207" height="145"></div>
    <div class="h_logo"><a class="fp-logo fp-iconfont" href="#" title="anitoys"><img src="<%=request.getContextPath()%>/resource/404/img/logo.gif"></a></div>
    <div class="h_input">
      <div class="h_itop">
        <input type="text" name="q" accesskey="s" autocomplete="off" x-webkit-speech="" x-webkit-grammar="builtin:translate" id="mq" role="combobox" aria-haspopup="true" class="combobox-input" onfocus="if(this.value=='ANI大搜查') {this.value='';}this.style.color='#333';" onblur="if(this.value=='') {this.value='ANI大搜查';this.style.color='#999999';}" value="ANI大搜查"/  maxlength="24">
        <button type="submit">搜整站</button>
      </div>
      <div class="h_iul"> <span>热门作品：</span>
        <li><a href="#">高达00</a></li>
        <li><a href="#">东京食尸鬼</a></li>
        <li><a href="#">Free！</a></li>
        <li><a href="#">钢铁意志</a></li>
        <li><a href="#">魔法少女小圆</a></li>
        <li><a href="#">MS78</a></li>
        <li><a href="#">RX93</a></li>
        <li><a href="#">夏亚</a></li>
      </div>
    </div>
    <div class="clear"></div>
  </div>
 <div class="menu">
  </div>
</div>
<div class="404jpg" style="width:330px; margin:0 auto;"><img src="<%=request.getContextPath()%>/resource/404/img/404.png" /></div>
<div style="text-align:center; margin-bottom:50px; font-size:36px; margin-top:10px;">您找的页面丢失了....</div>
<div class="footer">
  <div class="footer_top">
    <div class="ftmain">
      <div class="ftleft">
        <ul>
          <li>
            <h3>购物指南</h3>
            <a href="#" target="_blank">注册登录</a><a href="#" target="_blank">现货购买</a><a href="#" target="_blank">预约购买</a><a href="#" target="_blank">常见问题</a></li>
          <li>
            <h3>支付方式</h3>
            <a href="#" target="_blank">在线支付</a><a href="#" target="_blank">用户积分</a><a href="#" target="_blank">优惠券</a></li>
          <li>
            <h3>配送方式</h3>
            <a href="#" target="_blank">配送服务详情</a></li>
          <li>
            <h3>售后服务</h3>
            <a href="#" target="_blank">售后政策</a><a href="#" target="_blank">售后流程</a></li>
          <li>
            <h3>关于anitoys</h3>
            <a href="#" target="_blank">关于我们</a><a href="#" target="_blank">联系我们</a></li>
        </ul>
      </div>            
      <div class="ftphone">
        <div class="ftp"><img src="<%=request.getContextPath()%>/resource/404/img/logo.gif" /></div>
      </div>
    </div>
  </div>
  <div class="footer_bottom">@2014 阿尼托 上海索罗游信息技术有限公司 anitoys.com 版权所有 沪ICP备11047967号-18</div>
</div>

</body>
</html>
