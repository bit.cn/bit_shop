<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>注册</title>
		<link href="${systemSetting().staticSource}/static/frontend/v1/css/regist/regist.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<div class="bg">
		<div class="top">
			<a href="${systemSetting().www}"><img src="${systemSetting().staticSource}/static/frontend/v1/images/jshop/logo.gif" border="0"></a>
		</div>
		<div class="header clearfix">
				<div class="zc">注册
					<span class="regAready">我已注册，现在就<a class="new_a_style" href="${systemSetting().passport}/account/login">登录</a></span>
				</div>
				<div class="regLeft">
				<form role="form" id="form" method="post" class="form-horizontal" action="${systemSetting().passport}/account/doRegister.html" theme="simple">
						<div class="text">
							<span class="yhm">用户名：</span>
							<input type="text" name="account" id="account" placeholder="用户名/手机号" class="password" data-rule="用户名;required;account;length[0~18];"/>
							<span id="accountV" style="color: red;display:none;margin-left: 180px;">&nbsp;用户名已经被占用!</span>
						</div>
						<div class="text"><span class="dlmm">登录密码：</span>
							<input type="password" name="password" id="password" placeholder=" 密码" class="passwords" />
						</div>
						<div class="text"><span class="qrmm">确认密码：</span>
							<input type="password" name="password2" id="password2" placeholder=" 密码" class="passwordss" />
						</div>
						<div class="text"><span class="dlmms">请输入手机号：</span>
							<input type="text" name="phone" id="phone" placeholder="手机号" class="password3" />
							<input type="button" value="获取验证码" id="second" class="code_style" />
						</div>
						<div class="text"><span class="dlmms">请输入验证码：</span>
							<input type="text" name="code" id="code" placeholder="验证码" class="password4"/><strong class="new_strong"></strong>
							<span id="phoneV" style="color: red;display:none;margin-left: 180px;">&nbsp;手机号已经被占用!</span>
							<input type="hidden" value="${systemSetting().my}" id="mypagebaseUrl"/>
						</div>
						<div class="text"><span>&nbsp;</span>
							<input id="submit" type="button" class="sbtn" value="同意协议并注册"/>
						</div>
						<div class="text last"><span>&nbsp;</span>
							<a href="javaScript:alertInfo()" class="zxfwxy">《  aniToys在线服务协议  》</a>
						</div>
					</form>
					</div>
				<div class="regRight">
					<img src="${systemSetting().staticSource}/static/frontend/v1/images/jshop/registerlogo.png">
				</div>
			</div>
			<div class="bottum"></div>
		</div>
	</body>
</html>
		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/account/code.js"></script>
		<script type="text/javascript">
			$(function() {
			
			    //校验用户名
			    $("#account").blur(function(){
			    	var account=$("#account").val();
			    	if  (account=="" || account==null) {
						alert("用户名不能为空");
			            return false;
					}
			        if(account.replace(/\s+/g, "").length>18){
			        	alert("用户名长度不能超过18！");
			            return false;
			        }
			        //验证用户名的唯一性
			        $.ajax({
					  type: 'POST',
					  url: "${systemSetting().my}/account/checkUnique.html",
					  data: {"account":account},
					  dataType: "json",
					  success: function(data){
						if(data.result=="error"){
							alert("用户名已经被占用！");
							return false;
						}
					  },
					  error:function(er){
						 alert(data);
					  }
					});
			    })
			
			    //注册发送验证码
			    $("#second").click(function() {
			    	var account=$("#account").val();
					if  (account=="" || account==null) {
						alert("用户名不能为空");
			            return false;
					}
			        if(account.replace(/\s+/g, "").length>18){
			        	alert("用户名长度不能超过18！");
			            return false;
			        }
			        
			        //验证手机号的唯一性
			        var phone = $("#phone").val();
			        $.ajax({
					  type: 'POST',
					  url: "${systemSetting().my}/account/checkUnique.html",
					  data: {"phone":phone},
					  dataType: "json",
					  success: function(data){
						if(data.result=="error"){
							alert("手机号已经被占用！");
							return false;
						}
						else if(data.result=="ok"){
						 sendCode($("#second"), "${systemSetting().passport}/account/getCode");
						}
					  },
					  error:function(er){
						 alert(data);
					  }
					});
			    });
			    
			    v = getCookieValue("secondsremained");//获取cookie值
			    if (v > 0) {
			        settime($("#second"));//开始倒计时
			    }
			
			    $(".sbtn").click(function (){
			    	if (ismobile()==false) {
						return false;
					};
					
					var account=$("#account").val();
					if  (account=="" || account==null) {
						alert("用户名不能为空");
			            return false;
					}
			        if(account.replace(/\s+/g, "").length>18){
			        	alert("用户名长度不能超过18！");
			            return false;
			        }
					var password=$("#password").val();
			    	var password2=$("#password2").val();
			    	if (password!=password2) {
						alert("两次输入的密码不一致");
			                 return false;
					}
			    	if (password=="" || password2=="") {
			    		alert("密码不能为空");
			    		 return false;
					}
			    	var code=$("#code").val();
			       if(code==""){
			    	alert("请获取验证码");
					 return false;
			       }
			       var pcode=$("#pcode").val();
			       if(pcode=="验证码不正确或者超时"){
			    	   alert("验证码不正确或者超时");
			    	   return false;
			       }
			       
			       //验证用户名的唯一性
			        $.ajax({
					  type: 'POST',
					  url: "${systemSetting().my}/account/checkUnique.html",
					  data: {"account":account},
					  dataType: "json",
					  success: function(data){
						if(data.result=="error"){
							alert("用户名已经被占用！");
							return;
						}
					  },
					  error:function(er){
						 alert(data);
						 return;
					  }
					});
			       
			        //验证手机号的唯一性
			        var phone = $("#phone").val();
			        $.ajax({
					  type: 'POST',
					  url: "${systemSetting().my}/account/checkUnique.html",
					  data: {"phone":phone},
					  dataType: "json",
					  success: function(data){
						if(data.result=="error"){
							alert("手机号已经被占用！");
							return;
						}
					  },
					  error:function(er){
						 alert(data);
						 return;
					  }
					});
			       
			       ajaxRegister();
			       return;
			    });
			
			
				function ajaxRegister(){
					var account = $("#account").val();
					var password = $("#password").val();
					var password2 = $("#password2").val();
					var phone = $("#phone").val();
					var code = $("#code").val();
					$.ajax({
					  type: 'POST',
					  url: "${systemSetting().passport}/account/doRegister.html",
					  data: {"account":account,"password":password,"password2":password2,"phone":phone,"code":code},
					  dataType: "text",
					  success: function(data){
						 if(data=="注册成功")
						{
							window.location.href='${systemSetting().passport}/account/login.html';
						}else
						{
							alert(data+"!");
							 return;
						}
					  },
					  error:function(er){
						  console.log("changeProvince error!er = "+er);
					  }
					});
				}
			
			})
			
			function reloadImg2() {
				document.getElementById("codes2").src ="${systemSetting().passport}/ValidateImage.do?" +"radom="
						+ Math.random();
				$(" #vcode2").focus();
			}
			
			function alertInfo() {
				//window.open("treaty.ftl","");
				window.open ("treaty.ftl","newwindow","status=no,resizable=yes,top=200,left=300,width=800,height=500,scrollbars=yes,center:Yes");
			}
			
		</script>
