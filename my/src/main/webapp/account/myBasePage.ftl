<#macro myBasePage title="" jsFiles=[] cssFiles=[] staticJsFiles=[] staticCssFiles=[] checkLogin=true>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <!--通用CSS-->
    <link href="${systemSetting().staticSource}/shopindex/common/css/style.css" rel="stylesheet" type="text/css">
    <!--通用的JS-->
</head>
<#nested />
</html>
</#macro>
