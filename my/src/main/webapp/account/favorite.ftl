<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/common.css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css"rel="stylesheet"type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css"rel="stylesheet"type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css"rel="stylesheet"type="text/css">
	<title>我的收藏</title>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
	<div class="omt_head clearfix">
		<@myLeft.myLeft/>
		<div class="head_rig">
			<p class="head_my">
				我收藏的商品 <br />
			</p>
			<div class="favorite_product">
			<form method="post" theme="simple" action="${basepath}/favorite/deleteFavoriteProductList">
				<#if isDeleteSuccess?? && isDeleteSuccess>
							<script type="text/javascript">
								alert("已取消收藏");
							</script>
				</#if>
				<div class="favorite_operate_all">
					<div style="cursor:pointer;margin-top:3px;">
					<div style="display:inline;color:#BEBFC3;"><input type=checkbox  id="firstCheckbox" />&nbsp&nbsp全选&nbsp&nbsp&nbsp&nbsp</div>						
<!-- 						<a><span style="color:#3399FF;">加入购物车</span></a>										 -->
						<a href="javascript:deleteToFavorites()" ><span style="color:#346373;">取消收藏</span></a> 													
					</div>
				</div>

				<div class="favorite_product_show" style="margin-top:15px;width:900px;float:left;">
					<#list favPro as favProList>
					<div style="display:inline;border:1px solid #D6D6D6;height:275px;float:left;margin:0px 0px 10px 15px;">
						<!-- margin:上右下左  border:1px solid #EFEFEF; -->
						<div class="show_one_picture" >
							<a  href="${systemSetting().item}/product/${favProList.productID!""}.html" target="_blank" >
								<img style="height:200px;max-width:200px;"  alt="" src="${systemSetting().imageRootPath}${favProList.picture!""}" onerror="nofind()"/>
							</a>													
						</div>
						<div class="show_one_name" style="color:#346373;margin-top:5px;margin-left:5px;height:20px;">
							<input type=checkbox  name="ids" id="ids" value="${favProList.productID!""}">&nbsp
							<#if favProList??>
							<#if favProList.name??>
									<#if favProList.name?length gte 14>
										${favProList.name?substring(0,14)}...
									<#else>
										${favProList.name!""}
									</#if>
								 </#if>
						</#if>
						</div>							
						<div class="show_one_price" style="color:#C0732F;margin-left:5px;margin-top:2px;">${favProList.nowPrice!""}</div>
						<div class="show_one_operate" style="cursor:pointer;margin-top:1px;margin-left:5px;" >
							<a style="border:1px solid #D6D6D6;float:right ;background-color:#FAFAFA;margin-right:5px;margin-bottom:5px; border-radius: 0px;"  href="${basepath}/favorite/deleteFavoriteProduct?productId=${favProList.productID!""}">&nbsp取消&nbsp</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<!-- 							<a   href="#" onclick="javascript:addFromCart('${favProList.productID!""}')"  style="border:1px solid #D6D6D6;border-radius: 0px;" >加入购物车</a>						 -->
						</div>
					</div>
					</#list>
				</div>
				</form>
				<form action="${basepath}/favorite/addFavoriteProductToCart" method="POST" id="formAdd">
					<input type="hidden" name="productID">
				<input type="hidden" name="buyCount" value="1">     <!--从商品收藏页面添加到购物车的商品一律默认购买数量为1 -->
				</form>
			</div>
</div>
</div>			
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
			<script src="${systemSetting().staticSource}/static/frontend/v1/js/product.js"></script>
			<script type="text/javascript">
			$(function() {
				//后台查询页面 全选/全不选 功能	jQuery v1.9
				$("#firstCheckbox").on("click",function(){
					console.log("check="+$(this).prop("checked"));
					if($(this).prop("checked")){
						$("input[type=checkbox]").prop("checked",true);
					}else{
						$("input[type=checkbox]").prop("checked", false);
					}
				});

			});

				//批量取消商品收藏
				function deleteToFavorites() {
					$("form[theme=simple]").attr("action","${basepath}/favorite/deleteFavoriteProductList");
					$("form[theme=simple]").submit();
				}
				
								
				//单个添加购物车商品
				function addFromCart(productId){
					if(productId){
						$("#formAdd :hidden[name=productID]").val(productId);
						$("#formAdd").submit();
					}
				}
				//批量添加购物车商品
				function addFromCarts(){
				    $("form[theme=simple]").attr("action","${basepath}cart/addToCarts");
					$("form[theme=simple]").submit();
				}
			</script>
</@htmlBase.htmlBase>
</body>
</html>