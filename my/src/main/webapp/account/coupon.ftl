<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/account/myLeft.ftl" as myLeft>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/couponList.css" rel="stylesheet" type="text/css">
	<title>我的优惠券</title>
	<style type="text/css">
			.pageLink {
				border: 1px solid #dddddd;
				padding: 4px 12px;
				text-decoration: none;
			}
			
			.selectPageLink {
				border: 1px solid #0088cc;
				padding: 4px 12px;
				color: #0088cc;
				background-color: #dddddd;
				text-decoration: none;
			}
	</style>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
	<div class="omt_head clearfix">
		<@myLeft.myLeft/>
		<div class="head_rig" style="width: 950px;">
			<p class="head_my">
			优惠券管理
			<br />
			</p>
		  <div class="bd_di">
			<a href="${basepath}/coupon/coupons"><span class="wd" style="margin-left: 40px;" onclick="line_left('r_line')">我的优惠券</span></a>
			<a href="${basepath}/coupon/couponReceive" ><span class="lq" style="margin-left: 110px;" onclick="line_middle('r_line')">领取优惠券</span></a>
			<a href="${basepath}/coupon/couponsDue"><span class="dq" style="margin-left: 110px;" onclick="line_right('r_line')">到期优惠券</span></a><br/>
			<img src="${systemSetting().staticSource}/shopindex/layout/image/xx.png" class="xx" id="r_line" style = "margin-left:${style};">

			<ul class="bd_ul2">
			<#list pager.list as list>
			<li class="bd_til_li2">
					<div class="coupon_bg">
						<div <#if list.status=='1'>class="coupon_bg_img"</#if> <#if list.status=='2'>style="background-color: #BCB4B4;"</#if>>
							<br><br>
							<p class="p_w_25">
								<#if list.couponType??&&list.couponType==1> 
									￥${list.couponValue!""}
								<#elseif list.couponType??&&list.couponType==2> 
                                    ${list.couponValue!""}折
								<#elseif list.couponType??&&list.couponType==3> 
									￥${list.couponValue!""}
								<#else>
									￥${list.couponValue!""}
								</#if>
							</p>
							<span class="span_w_18"><#if list.startTime??>${list.startTime}</#if></span>
							<span class="span_w_18_2">--</span>
							<span class="span_w_18_2"><#if list.endTime??>${list.endTime}</#if></span>
						</div>
						<div class="wdyhq">
							<div class="fxq"><label>发行方：</label>${list.businessName!""}</div>
							<#if list.couponType??&&list.couponType==1> 
								<a class="jrdp" href="${systemSetting().businessFront}/business/businessSelect?businessId=${list.businessId!""}">进入店铺</a>
								<#elseif list.couponType??&&list.couponType==2> 
								<a class="jrdp" href="${systemSetting().businessFront}/business/businessSelect?businessId=${list.businessId!""}">进入店铺</a>
								</#if>
							<div class="type"><label>优惠券种类：</label>
								<#if list.couponType??&&list.couponType==1> 商家抵价券
								<#elseif list.couponType??&&list.couponType==2> 商家折扣券
								<#elseif list.couponType??&&list.couponType==3> 平台折扣券
								<#else> ${list.couponType!""}
								</#if>
							</div>
							<#if list.couponType??&&list.couponType!=3>
							<div><label>使用店铺：</label>anitoys</div>
							<#if list.status=='2'><span style="width:165px;">已使用</span></#if>
							<#else>
							<#if list.status=='2'><span style="width:165px;">已使用</span></#if>
							<span style="width:65px;"></span>
							</#if>
						</div>
					</div>
				</li>
			</#list>
			</ul>
		</div>
		<div>
			<#include "/shopindex/common/ftl/pager.ftl"/>
		</div>
	</div>
  </div>
  <!-- 商城主页footer页 -->
  <@shopFooter.shopFooter/>
  <script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>
