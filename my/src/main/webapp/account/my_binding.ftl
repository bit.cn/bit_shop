<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
	<title>账户安全</title>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig">
		<p class="head_my">
			账号安全
			<br />
		</p>
		<div class="bd_cen">
		<table style=" margin-left:15px; margin-top:30px; border:1px #CECECE solid; height:320px; width:920px;">
		  	<tr>
				<td class="left">
					<#if accSecurity??>
						<#if accSecurity.userPassword??&&accSecurity.userPassword!="">
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/lock.png"class="gou" style="padding:10px;" height="50" width="60"/>
						<#else>
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/no.png"class="gou"/>
						</#if>
					</#if>
					<strong>修改密码</strong>  
					<span>定期的修改密码可以有效的保障账户安全</span>
					<input id="getdate" class="s_btn" type="button" value="修改密码"
					onclick="location.href='${basepath}/accountSecurity/my_up_pwd'"/>
				</td>
		 	</tr>
		  	<tr>
				<td class="left">
					<#if accSecurity??>
						<#if accSecurity.email??&&accSecurity.email!="">
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/yes.png"class="gou"/>
						<#else>
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/no.png"class="gou"/>
						</#if>
					</#if>
					<strong>绑定邮箱</strong>  
					<span>验证后，可用于快速找回登录密码，接收账户余额变动提醒。</span>
						<#if accSecurity??>
							<#if accSecurity.email??&&accSecurity.email!="">
							  <input id="getdate" type="button" class="s_btn" value="解绑邮箱" 
							onclick="location.href='${basepath}/accountSecurity/my_binding_mal'"/>
							<#else>
							  <input id="getdate" type="button" class="s_btn" value="绑定邮箱" 
								onclick="location.href='${basepath}/accountSecurity/my_binding_mal'"/>
							</#if>
					</#if>
				</td>
					
		 	</tr>
		  	<tr>
				<td class="left">
					<#if accSecurity??>
						<#if accSecurity.userPhone??&&accSecurity.userPhone!="">
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/yes.png"class="gou"/>
						<#else>
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/no.png"class="gou"/>
						</#if>
					</#if>
					<strong>绑定手机</strong>  
					<span>验证后，可用于快速找回登录密码及支付密码，接收账户余额变动提醒。</span>
					<#if accSecurity??>
						<#if accSecurity.userPhone??&&accSecurity.userPhone!="">	
							<div class="cen_unw_d">
								<a href="${basepath}/accountSecurity/my_binding_tel"  class="cen_unw" >解绑手机</a>
							</div>
						<#else>
						  <input id="getdate" type="button" class="s_btn" value="绑定手机"  
						onclick="location.href='${basepath}/accountSecurity/my_binding_tel'"/></a>
						</#if>
					</#if>
				</td>
		 	</tr>
		 	<tr>
				<td class="left">
					<#if accSecurity??>
						<#if accSecurity.isUpPwdSend??&&accSecurity.isUpPwdSend!=""&&accSecurity.isUpTelSend??&&accSecurity.isUpTelSend!="">
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/yes.png"class="gou"/>
						<#else>
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/no.png"class="gou"/>
						</#if>
					</#if>
					<strong>安全提醒</strong>  
					<span>设置账户安全提醒,防止骚扰。</span>
					<input id="getdate" type="button" class="s_btn" value="提醒设置"
					onclick="location.href='${basepath}/accountSecurity/my_remind'" />
				</td>
		  	</tr>
		  	<tr>
				<td class="left">
					<#if accSecurity??>
						<#if accSecurity.cardNO??&&accSecurity.cardNO!="">
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/yes.png"class="gou"/>
						<#else>
						  <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/no.png"class="gou"/>
						</#if>
					</#if>
					<strong>证件信息</strong>  
					<span>设置证件,提高账户安全系数。</span>
					<#if accSecurity??>
						<#if accSecurity.cardNO??&&accSecurity.cardNO!="">
						  <input id="getdate" type="button" class="s_btn" value="已设置" onclick="location.href='${basepath}/accountSecurity/my_cert'"/>
						<#else>
						  <input id="getdate" type="button" class="s_btn" value="证件设置"
							onclick="location.href='${basepath}/accountSecurity/my_cert'" />
						</#if>
					</#if>
				</td>
		 	</tr>
    	</table>
		</div>	
	</div>
</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>