<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/bootstrap.min.css">
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
	<title>通过手机找回密码</title>
	</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
	<@myLeft.myLeft/>
    <div class="head_rig" style="width: 950px;">
	        <p class="head_my">
	           	手机找回密码
	            <br />
	        </p>
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12" style="font-size: 14px;font-weight: normal;">
						<span class="label label-default" style="font-size:100%;">
							1.填写账户信息
						</span>
						&nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span>
						<span class="label label-success" style="font-size:100%;">
							2.身份验证
						</span>
						&nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span>
						<span class="label label-default" style="font-size:100%;">
							3.设置新密码
						</span>
						&nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span>
						<span class="label label-default" style="font-size:100%;">
							4.完成
						</span>
					</div>
				</div>
				<hr>
				
				<div class="panel panel-success">
					<div class="panel-heading" style="text-align: left;">
		                <h3 class="panel-title">
		                	找回密码
		                </h3>
		              </div>
		              <div class="panel-body" style="font-size: 16px;font-weight: normal;text-align: center;">
		              	 <span class="glyphicon glyphicon-ok"></span>
		              	 <span class="text-success">验证手机已发送，请您查看手机短信完成身份验证！</span>
		              </div>
		              <form role="form" name="form" method="post" id="form" class="form-horizontal" action="${basepath}/account/checkUserPhone.html" theme="simple">
						  <div class="form-group">
						    <div class="col-lg-4" style="float:left;margin-left: 300px;">
							    <input  id="code" name="code" type="text" class="form-control n-valid"  style="width:260px;" placeholder="请输入验证码" />
						    	<input  name="phone" type="hidden" class="form-control"  
						    	style="width:260px;" value="${phone!""}"/>
						    </div>
						  </div>
						  <div class="form-group" style="padding-left:215px;margin-left: 300px;">
						    <div class="">
						      <button type="submit" class="btn btn-success btn-sm" value="提交信息"/>
						      	<span class="glyphicon glyphicon-ok"></span>&nbsp;提交信息
						      </button>
						      <caption>
									<#if errorMsg??>
									<div class="bs-callout bs-callout-danger author" style="text-align: left;font-size: 16px;margin-left:100px;margin-top:-20px; margin-bottom:10px;">
										<strong>${errorMsg}</strong>
									</div>
									</#if>
							 </caption>
						    </div>
						  </div>
						</form>
				</div>
				<hr>
			</div>
		</div>
	</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/product.js"></script>
</@htmlBase.htmlBase>
</body>
</html>