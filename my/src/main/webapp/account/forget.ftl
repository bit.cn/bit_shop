<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/bootstrap.min.css">
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
	<title>通过邮箱找回密码</title>
	</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
	<@myLeft.myLeft/>
    <div class="head_rig" style="width: 950px;">
	        <p class="head_my">
	           	邮箱找回密码
	            <br />
	        </p>
			<div class="col-xs-12">
				<div class="row" style=" padding:20px 0;">
					<div class="col-xs-12" style="font-size: 14px;font-weight: normal;">
						<span class="label label-success" style="font-size:100%;">
							1.填写邮箱信息
						</span>
						&nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span>
						<span class="label label-default" style="font-size:100%;">
							2.身份验证
						</span>
						&nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span>
						<span class="label label-default" style="font-size:100%;">
							3.设置新密码
						</span>
						&nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span>
						<span class="label label-default" style="font-size:100%;">
							4.完成
						</span>
					</div>
				</div>
				<hr>
				<div class="panel panel-success">
					<div class="panel-heading" style="text-align: left;">
		                <h3 class="panel-title">
		                	<span class="glyphicon glyphicon-user"></span>
		                </h3>
		              </div>
		              <div class="panel-body">
		              	<form role="form" name="form" method="post" id="form" class="form-horizontal" action="${basepath}/account/doForgetFromEmail.html" theme="simple">
						  <div class="form-group">
						    <label style="float:left; width:200px; text-align:right" for="account" class="col-lg-2 control-label">邮箱：</label>
						    <div class="col-lg-4" style=" float:left">
							    <input  id="mail_address" name="email" type="text" class="form-control"  style="width:260px;" placeholder="请输入邮箱"
							    data-rule="邮箱地址:required;email"/>
							    <span style="padding-left:50px;" id="mailErMessage" ></span>
						    </div>
						  </div>
						  <div class="form-group" style=" padding-left:215px;">
						    <div class="">
						      <button type="submit" class="btn btn-success btn-sm" value="提交信息"/>
						      	<span class="glyphicon glyphicon-ok"></span>&nbsp;提交信息
						      </button>
						      <caption>
									<#if errorMsg??>
									<div class="bs-callout bs-callout-danger author" style="text-align: left;font-size: 16px;margin-left:100px;margin-top:-20px; margin-bottom:10px;">
										<strong>${errorMsg}</strong>
									</div>
									</#if>
							 </caption>
						    </div>
						  </div>
						</form>
		              </div>
				</div>
			</div>
		</div>
	</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript">
	function reloadImg2() {
		document.getElementById("codes2").src = "${systemSetting().www}/ValidateImage.do?random="
				+ Math.random();
		$("#vcode2").focus();
	}
	
	$(function(){
		var mail = "";
		//失去焦点
		<#if userEmail??&&userEmail!="">
			if('${userEmail}'!=""){
				mail = '${userEmail}'+"";
			}
		<#else>
			
		</#if>
		$('#mail_address').blur(function() { 
			mail = $("#mail_address").val();
			check_email(mail);
		});
		//获得焦点
		$('#mail_address').focus(function() { 
			mail = $("#mail_address").val();
			check_email(mail);
		}); 
	});
	
	//检测邮箱
	function check_email(email){
		var email=email.replace(/^\s*|\s*$/g,'');
		var length=email.length;
		var a=/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
		if (length==0)//用email为空的时候
		{
			$("#mailErMessage").html("<font color=red>邮箱不能为空!</font>");
			$("#sbumit").attr('disabled','disabled');
			return false;
		}else{
			if(a.test(email)) {
				$("#mailErMessage").html("<font color=green>邮箱格式正确,请继续!</font>");
				$("#sbumit").attr('disabled','');
				return true;
			}
			else{
				$("#mailErMessage").html("<font color=red>邮箱格式不正确！</font>");
				$("#sbumit").attr('disabled','disabled');
			 return false;
		}
		}
	}
	
	function checkEmail(mail){
		$.ajax({
				url:"${basepath}/accountSecurity/changeEmailCheck",
				type:"post",
				dataType:"json",
				data:{'newEmail':mail},
				success:function(result){
					console.log(result);
					if(result.ok=="系统认为此邮箱可用!"){
						$("#mailErMessage").html("<font color='#339933' style='padding-left:20px'>√ "+result.ok+"</font>");
						return true;
					}else{
						$("#mailErMessage").html("<font color=red>"+result.error+"</font>");
					}
				}
			});
		}
	function submit_mail(){
		var mail_address = $("#mail_address").val();
		<#if userEmail??&&userEmail!="">
			if('${userEmail}'!=""){
			mail_address = '${userEmail}'+"";
			}
		<#else>
			
		</#if>
		var checkCode = $("#checkCode").val();
		console.log(checkCode);
		if(checkCode!=""&&code!=""){
			if(code==checkCode){
				
					$.ajax({
						url:"${basepath}/accountSecurity/doChangeEmail",
						type:"post",
						dataType:"json",
						data:{'newEmail':mail_address},
						success:function(data){
							alert(data.ok);
							 location.href="${basepath}/accountSecurity/my_binding";  跳转到账户安全初始化
						}
					});
			  }
		}else{
			$("#mailErMessage").html("<font color='red' style='padding-left:40px'>× 请输入验证码</font>");
		}
}
		
</script>

<script>
var InterValObj; //timer变量，控制时间
var count = 60; //间隔函数，1秒执行
var curCount;//当前剩余秒数
var code = ""; //验证码
var codeLength = 6;//验证码长度

function sendMessage() {
  	var mail_address = $("#mail_address").val();
  	<#if userEmail??&&userEmail!="">
		if('${userEmail}'!=""){
			mail_address = '${userEmail}'+"";
			$('#mailErMessage').html("<font color=green style='padding-left:40px'>邮箱格式正确,请继续!</font>");
		}
	<#else>
			
	</#if>
  	var mailErMessage = $("#mailErMessage").text();
  	curCount = count;
	if (mailErMessage != "") {
		if(mailErMessage == "邮箱格式正确,请继续!"||mailErMessage=="√ 系统认为此邮箱可用!"){
			// 产生验证码
			for ( var i = 0; i < codeLength; i++) {
				code += parseInt(Math.random() * 9).toString();
			}
			// 设置button效果，开始计时
			$("#btnSendCode").attr("disabled", "true");
			$("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
			InterValObj = window.setInterval(SetRemainTime, 1000); // 启动计时器，1秒执行一次
			// 向后台发送处理数据
			$.ajax({
				type: "POST", // 用POST方式传输
				//dataType: "json", // 数据格式:JSON
				url: "${basepath}/account/doForgetMailCheckNo", // 目标地址
				data: {'newEmail':mail_address,'vcode':code},
				error: function (XMLHttpRequest, textStatus, errorThrown) { 
					$("#mailErMessage").html("<font color='red' style='padding-left:40px'>× 请输入验证码</font>");
				},
				success: function (data){ 
					console.log(data);
					$("#sbumit").attr('disabled',false);
					if(data.msg == "ok"){
						$("#mailErMessage").html("<font color='#339933' style='padding-left:40px'>√ 邮件已发送,请查收</font>");
					}
				}
			});
		}
	}else{
		$('#mailErMessage').html("<font color=red style='padding-left:40px'>× 邮箱不能为空</font>");
		//$('#sbumit').attr('disabled','disabled');
	}
}
//timer处理函数
function SetRemainTime() {
	if (curCount == 0) {                
		window.clearInterval(InterValObj);// 停止计时器
		$("#btnSendCode").removeAttr("disabled");// 启用按钮
		$("#btnSendCode").val("重新发送验证码");
		code = ""; // 清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
	}else {
		curCount--;
		$("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
	}
}
</script>
</@htmlBase.htmlBase>
</body>
</html>