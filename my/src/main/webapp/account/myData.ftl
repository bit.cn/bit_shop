<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/account/myLeft.ftl" as myLeft>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/footer/footer.css" />
	<link rel="stylesheet" href="${systemSetting().staticSource}/validator-0.7.0/jquery.validator.css" />
	<title>个人资料</title>
</head>
<body>
<!--商城主页top页-->
<!--@shopTop.shopTop/-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig" style="width: 950px;">
		<p class="head_my">
			个人资料
			<br />
		</p>
		<div class="head_ul">
		<form role="form" id="form1" class="form-horizontal" method="post" action="saveMyData" theme="simple">
		 <input type="hidden" id ="isUpdate"    value ="${(myData.isUpdate)!''}" />
			<table class="cen_table" style="height:640px"   cellSpacing=0 cellPadding=0   border=0 >
		    <tr>
				<td>
				<b>基本信息</b>
				<b>&nbsp;(*为必填)</b>
				</td>
			</tr>
		  <tr>
				<td class="right p>
					<span class="gery">*</span><span>用户名：</span>
				</td>
				<td class="left">
				<span id="acc" ondblclick=update(); style="font-size:15px">&nbsp;${(myData.account)!''}</span>

				<input type="text"  style="visibility: none;display: none;" id="account" name ="account" class="tx3 ipt-text" value ="${(myData.account)!''}"  /> 
				<span id ="ondblclickV" style="color:red">&nbsp;双击可修改用户名，只能修改一次</span>
				<span id ="ondblclickV2" style="visibility: none;display: none;color:red">&nbsp;用户名只能修改一次，您已经修改过!</span>
				<span id="accountV" style="visibility: none;display: none;color:red">&nbsp;请正确填写用户名</span>
				</td>
		  </tr>		
		  <tr>
				<td class="right p"><span class="gery">*</span><span>真实姓名：</span></td>
				<td class="left">
					<input type="text" id="trueName" name ="trueName" class="tx3 ipt-text" value ="${(myData.trueName)!''}"  maxlength="15" /> 
					<span id="trueNameV" style="visibility: none;display: none;color:red">&nbsp;请正确填写真实姓名</span>
				</td>
		  </tr>
		  <tr>
				<td class="right p"><span class="gery">*</span><span>性别：</span></td>
				<td class="left">
					<input type="radio" id="sex" name="sex" value="m" ${(myData.sex=="m")?string("checked","")}>男
                    <input type="radio" id="sex" name="sex" value="f" ${(myData.sex=="f")?string("checked","")}>女
                    <span id="sexV" style="visibility: none;display: none;color:red">&nbsp;请正确选择性别</span>
				</td>
		  </tr>
		   <tr>
				<td class="right p"><span class="gery">*</span><span>生日：</span></td>
				<td class="left">
					<input onClick="WdatePicker()" class="ipt-text" id="birthday" name="birthday"  value="${(myData.birthday)!''}" maxlength="10">		
					<span id="birthdayV" style="visibility: none;display: none;color:red">&nbsp;请正确填写生日，如：1970-01-01</span>		
				</td>
		  </tr>
		  <tr>
				<td><b>联系信息</b></td>
		  </tr>
		  <tr>
			<td class="right p"><span class="org">*</span><span>所在地区：</span></td>
			<td class="left">
			   <select name="province" id="province" class="form-control ipt-text-sm" onchange="changeProvince()">
						<option value="">--选择省份--</option>
							<#list provinces as item>
								<option value="${item.code}" ${(myData.province??&&myData.province==item.code)?string("selected", "")}>${item.name}</option>
						</#list>
				</select>	
				<select class="form-control ipt-text-sm" id="citySelect" name="city"  onchange="changeCity()">
						<option value="">--选择城市--</option>
							<#list cities as item>
								<option value="${item.code}" ${(myData.city??&&myData.city==item.code)?string("selected", "")}>${item.name}</option>
							</#list>
						</select>
				  <select class="form-control ipt-text-sm" id="areaSelect" name="area">
                                <option value="">--选择区县--</option>
								<#list areas as item>
									<option value="${item.code}" ${(myData.area??&&myData.area==item.code)?string("selected", "")}>${item.name}</option>
								</#list>
                  </select>
                  <span id="areaSelectV" style="visibility: none;display: none;color:red">&nbsp;请正确选择所在地区</span>		
			</td>
		  </tr>
		  <tr>
			<td class="right p"><span class="adress"></span><span class="org">*</span><span>详细地址：</span></td>
			<td class="left">
				<textarea id ="addressDetail" name="address" rows="5" cols="50" class="ta" maxlength="100" placeholder="建议你如实填写你详细地址">${(myData.address)!''}</textarea>						
				<span id="addressDetailV" style="visibility: none;display: none;color:red" >&nbsp;请正确填写你详细地址</span>		
			</td>
		  </tr>
		  <tr>
			<td class="right p"><span class="org">*</span><span>邮政编码：</span></td>
			<td class="left"> 
				<input type="text" class="ipt-text" style="width:240px" maxlength="6" name = "zip" placeholder="如果你不清楚邮递区号，请填写000000" id="tx" value="${(myData.zip)!''}"/> 
				<span id="txV" style="visibility: none;display: none;color:red">&nbsp;请正确填写邮政编码</span>		
			</td>
		  </tr>
		  <tr>
			<td class="right p"><span class="org">*</span><span>手机号码：</span></td>
			<td class="left">
				<input id ="phone" type="text" class="ipt-text" style="width:240px"  name = "phone" placeholder="电话号码，手机必须填一项" class="p_num"  value="${(myData.phone)!''}"/>
				 <span id="phoneV" style="visibility: none;display: none;color:red">&nbsp;电话号码，手机必须填一项</span>		
				 <span id="phoneV2" style="visibility: none;display: none;color:red">&nbsp; 请正确填写手机号码</span>		
			</td>
		  </tr>
		  <tr>
			<td class="right p"><span>*电话号码：</span></td>
			<td class="left">
				<input id ="mobile" type="text" class="ipt-text" style="width:240px"  name = "tel"  placeholder="电话号码"    value="${(myData.tel)!''}"/>
				<span id="mobileV" style="visibility: none;display: none;color:red">&nbsp;电话号码。手机必须填一项</span>		
			</td>
		  </tr>
		  <tr>
			<td><b>其他联系方式</b></td>
		  </tr>
		  <tr>
			<td class="right p">
				<img src="${systemSetting().staticSource}/shopindex/layout/image/qq.png" />
			</td>
			<td class="left"><input type="text" class="ipt-text" id="qq" name="qq" value="${(myData.qq)!''}"/><br/></td>
			</tr>
			<tr>
				<td class="right p">
					<img src="${systemSetting().staticSource}/shopindex/layout/image/xinlang.png"/>
				</td>
				<td class="left"><input type="text" class="ipt-text"id="microblog" name="microblog" value="${(myData.microblog)!''}"/><br/></td>
			</tr>
		  <tr>
		  	<td class="right p">&nbsp;</td>
			<td class="left"><input class="up_but" type="button" align="left" style="height:30px;width:294px;margin-left:0px;" id="bt" value="保存" onclick=" valid();"></td>
		  </tr>
		</table> 
		</from>
		</div>
	</div>
</div>
<!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>
 <script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
 <!--日期控件调用本地资源-->
 <script type="text/javascript" src="${basepath}/resource/My97DatePicker/WdatePicker.js"></script>
 <script type="text/javascript" src="${systemSetting().staticSource}/validator-0.7.0/jquery.validator.js"></script>
<script type="text/javascript" >
function changeProvince(){

	var selectVal = $("#province").val();
	if(!selectVal){
		//console.log("return;");
		return;
	}
	var _url = "selectCitysByProvinceCode?provinceCode="+selectVal;
	//console.log("_url="+_url);
	$("#citySelect").empty().append("<option value=''>--选择城市--</option>");
	$("#areaSelect").empty().show().append("<option value=''>--选择区县--</option>");
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  dataType: "json",
	  success: function(data){
		  ////console.log("changeProvince success!data = "+data);
		  $.each(data,function(index,value){
			  ////console.log("index="+index+",value="+value.code+","+value.name);
			  $("#citySelect").append("<option value='"+value.code+"'>"+value.name+"</option>");
		  });
	  },
	  error:function(er){
		  //console.log("changeProvince error!er = "+er);
	  }
	});
}


function changeCity(){
	var selectProvinceVal = $("#province").val();
	var selectCityVal = $("#citySelect").val();
	if(!selectProvinceVal || !selectCityVal){
		//console.log("return;");
		return;
	}
	var _url = "selectAreaListByCityCode?provinceCode="+selectProvinceVal+"&cityCode="+selectCityVal;
	//console.log("_url="+_url);
	$("#areaSelect").empty().show().append("<option value=''>--选择区县--</option>");
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  dataType: "json",
	  success: function(data){
		  ////console.log("changeProvince success!data = "+data);
		  $.each(data,function(index,value){
			  ////console.log("index="+index+",value="+value.code+","+value.name);
			  $("#areaSelect").append("<option value='"+value.code+"'>"+value.name+"</option>");
		  });
	  },
	  error:function(er){
		  //console.log("changeCity error!er = "+er);
	  }
	});
}

function update(){
	var isUpdate = $("#isUpdate").val();
	if(isUpdate=="y"){
	 alert("用户名你已经修改过一次");
	//	document.getElementById("ondblclickV").style.visibility="none";
    	document.getElementById("ondblclickV").style.display="none";
    	document.getElementById("ondblclickV2").style.visibility="visible";
  		document.getElementById("ondblclickV2").style.display="";
		return;
	}
	//document.getElementById("acc").style.visibility="none";//
    document.getElementById("acc").style.display="none";
	//document.getElementById("ondblclickV").style.visibility="none";//
    document.getElementById("ondblclickV").style.display="none";//
   // document.getElementById("ondblclickV2").style.visibility="none";//
    document.getElementById("ondblclickV2").style.display="none";//
    　document.getElementById("account").style.visibility="visible";//显示
    document.getElementById("account").style.display="";//显示
}

function valid(){
	
	 $("#accountV,#trueNameV,#birthdayV,#areaSelectV,#addressDetailV,#txV,#phoneV,#mobileV,#phoneV2,#sexV").hide();
     var trueName = $("#trueName").val();
     var sex = $('input[name="sex"]:checked ').val();
     var birthday = $("#birthday").val();
     var province = $("#province").val();
     var citySelect =  $("#citySelect").val();
     var areaSelect = $("#areaSelect").val();
     var addressDetail =$("#addressDetail").val();
     var tx = $("#tx").val()
     var phone = $("#phone").val();
     var mobile = $("#mobile").val();
     var account =  $("#account").val();
     var isSuccess =1;
     // 用户名验证
	 if($("#account").is(':visible')) {  
	     if(account=="" || account==undefined){
	        $("#accountV").show();	//显示
	         isSuccess =0;
	     }
	  }else {
	  	 $("#account").val("");
	  }
	 // 真实姓名验证
     if(trueName=="" || trueName==undefined){
	        $("#trueNameV").show();	//显示
	         isSuccess =0;
	     }
     if(birthday=="" || birthday==undefined){
        $("#birthdayV").show();	//显示
        isSuccess =0;
     } 
 	 // 性别验证
 	 if(sex=="" || sex==undefined) {
 	    $("#sexV").show();	//显示
        isSuccess =0;
 	 
 	 }
     
     if(province=="" ||citySelect=="" ){
        $("#areaSelectV").show();	//显示
        isSuccess =0;
     }
     if(addressDetail=="" || addressDetail==undefined ){
         $("#addressDetailV").show();	//显示
        isSuccess =0;
     }
     var re= /^[1-9][0-9]{5}$/ ;
     if(tx!='000000') {
	     if(tx=="" || !re.test(tx) ){
	        $("#txV").show();	//显示
	        isSuccess =0;
	     }
     }
     if(phone=="" &&mobile =="" ){
        $("#phoneV").show();	//显示
        $("#mobileV").show();	//显示
        isSuccess =0;
     }
     var mobile=mobile.replace(/^\s*|\s*$/g,'');
	 var length=mobile.length;
	 var a=/^(1[3|4|5|8|7])[0-9]{9}$/;
     if(phone!="" &&!a.test(phone)){
         $("#phoneV2").show();	//显示
         isSuccess =0;
     }
     
     if(isSuccess == 1){ 
     	$("#bt").attr({"disabled":"disabled"})
		form1.submit(); 
	 } 
   }
</script>
</@htmlBase.htmlBase>
</body>
</html>