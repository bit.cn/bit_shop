<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/bootstrap.min.css">
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css"rel="stylesheet"type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css"rel="stylesheet"type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css"rel="stylesheet"type="text/css">
	<title>我的订单</title>
	<style type="text/css">
			.pageLink {
				border: 1px solid #dddddd;
				padding: 4px 12px;
				text-decoration: none;
			}
			
			.selectPageLink {
				border: 1px solid #0088cc;
				padding: 4px 12px;
				color: #0088cc;
				background-color: #dddddd;
				text-decoration: none;
			}

			.picScroll-top {
				overflow: hidden;
				position: relative;
				border: 1px solid #ccc;
				min-height: 300px;
			}
			
			.picScroll-top .hd {
				overflow: hidden;
				height: 30px;
				background: #f4f4f4;
				padding: 0 10px;
			}
			
			.picScroll-top .hd .prev,
			.picScroll-top .hd .next {
				display: block;
				width: 9px;
				height: 5px;
				float: right;
				margin-right: 5px;
				margin-top: 10px;
				overflow: hidden;
				cursor: pointer;
				background: url("${systemSetting().staticSource}/resource/js/superSlide/demo/images/icoUp.gif") no-repeat;
			}
			
			.picScroll-top .hd .next {
				background: url("${systemSetting().staticSource}/resource/js/superSlide/demo/images/icoDown.gif") no-repeat;
			}
			
			.picScroll-top .hd ul {
				float: right;
				overflow: hidden;
				zoom: 1;
				margin-top: 10px;
				zoom: 1;
			}
			
			.picScroll-top .hd ul li {
				float: left;
				width: 9px;
				height: 9px;
				overflow: hidden;
				margin-right: 5px;
				text-indent: -999px;
				cursor: pointer;
				background: url("${systemSetting().staticSource}resource/js/superSlide/demo/images/icoCircle.gif") 0 -9px no-repeat;
			}
			
			.picScroll-top .hd ul li.on {
				background-position: 0 0;
			}
			
			.picScroll-top .bd {
				padding: 10px;
			}
			
			.picScroll-top .bd ul {
				overflow: hidden;
				zoom: 1;
			}
			
			.picScroll-top .bd ul li {
				text-align: center;
				zoom: 1;
			}
			
			.picScroll-top .bd ul li .pic {
				text-align: center;
				margin: auto;
			}
			
			.picScroll-top .bd ul li .pic img {
				max-width: 200px;
				max-height: 200px;
				display: block;
				padding: 0px;
				border: 0px solid #ccc;
			}
			
			.picScroll-top .bd ul li .pic a:hover img {
				border-color: #999;
			}
			
			.picScroll-top .bd ul li .title {
				line-height: 24px;
				text-align: left;
			}
			
			table tr td{
				text-align:center;
			}
			
			table tr td.left{
				text-align:left;
			}
			
			table tr td.right{
				text-align:right;
			}
		
		</style>
</head>
<body>
<#setting datetime_format="yyyy-MM-dd HH:mm"/>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig">
	<div class="dingdan-add-top">
			<div class="dingdan-add-pd">	
			<img src="${systemSetting().imageRootPath}/${newAcc.head!""}" style="width:110px; height:110px;"/>
			</div>
			<div  class="dingdan-add-pd widthbd"><h1>${newAcc.account!""}</h1><p>积分:<span>${newAcc.rank!""}</span></p></div>
			<div class="dingdan-add-pd" ><h2>账户余额：<strong>${newAcc.amount!""}</strong></h2></div>
		</div>
		<p class="head_my">
			订单管理中心
			<br/>
		</p>
		<div class="head_ul">
			<ul class="cent">
				<p class="cent_p"></p>
				<li class="myomt">
					我的订单
				</li>
				<br/><br/>
				<li style="line-height:50px;">
					
					订单提醒：
					<span>
						<#if orderSimpleReport.orderWaitPayCount!=0>
						<a href="${basepath}/account/orders?status=dfk">
							待付款（${orderSimpleReport.orderWaitPayCount!""}）
						</a>
						<#else>
						<a href="${basepath}/account/orders?status=dfk">
							待付款（0）
						</a>
						</#if>
					</span>
					<span>
						<#if orderSimpleReport.orderPassCount!=0>
						<a href="${basepath}/account/orders?status=dbk">
							待补款（${orderSimpleReport.orderPassCount!""}）
						</a>
						<#else>
						<a href="${basepath}/account/orders?status=dbk">
							待补款（0）
						</a>
						</#if>
					</span>
					<span>
						<#if orderSimpleReport.orderCompleteCount!=0>
							<a href="${basepath}/account/orders?status=dsh">
								等待确认收货（${orderSimpleReport.orderCompleteCount!""}）
							</a>
						<#else>
							<a href="${basepath}/account/orders?status=dsh">
								等待确认收货（0）
							</a>
						</#if>
					</span>
					<span>
						<a href="${basepath}/account/orders?status=finish">
							已完成
						</a>	
					</span>
					<span>
						<a href="${basepath}/account/orders?status=cancel">
							失效订单
						</a>
					</span>
				</li>
			</ul>
		</div>
		<div class="head_mk">
			<div class="head_mk_pak">
				<table class="head_pak_id">
				<tr class="head_pak">
					<td width="340px;" class="left" style="padding-left:15px;">订单信息</td>
					<td>商品状态</td>
					<td>成交单价</td>
					<td>数量</td>
					<td>售后服务</td>
					<td>下单时间</td>
					<td>支付时间</td>
					<td>订单状态</td>
					<td class="aft_t2">操作</td>
				</tr>
				<tr class="head_pak_new">
					<td colspan="9" class="left">
					当下单的商品包含预售商品和现货商品，以及商品来自不同的仓库时，会被拆分成数个订单，请注意！
					<br />
					订单下单后，您有1-3个小时进行付款，过期订单将取消，请注意！  
				</tr>
			<#list pager.list as itemt>
				<tr class="aft_tr2"><td colspan="10"></td></tr>
				<tr class="head_tb_tr">
						<td colspan="9" class="left">
							<span class="tr_id"> 订单编号:
                                 <a target="_blank" href="${systemSetting().orders}/order/${itemt.id!""}">
									${itemt.orderCode!""} 
								</a>
                            </span>
							<span class="tr_sto"><img src="${systemSetting().staticSource}/shopindex/layout/img/omt/sto.png"/> 
								<a target="_blank" href="${systemSetting().businessFront}/business/businessSelect?businessId=${itemt.businessId!""}">
									${itemt.businessName!""}
								</a>
							</span>
							<#if itemt.customerAddress??>
								<span class="tr_sto" style="margin-left: 50px;">
									<a target="_blank" href="${itemt.customerAddress?if_exists}">
										<img src="${systemSetting().staticSource}/shopindex/layout/img/omt/cuss.png" height="25"/>
									</a>
								</span>
							</#if>
						</td>
					</tr>
				<#list itemt.orders as item>
				<tr class="aft_tr1">
					<td class="aft_t">
						<#-- 商品图片路径
						<a href="${systemSetting().item}/product/${item.productID!""}.html" target="_blank" title="${item.productName!""}">
							<img style="width: 100%;height: 100%;border: 0px;" alt="" src="${systemSetting().imageRootPath}${item.picture!""}" onerror="nofind()"/>
						</a>
						-->
							<a style="float:left;width:27%;margin-right:2%;margin-top:2%;margin-bottom:2%;text-align:center;" href="${systemSetting().item}/product/${item.productID!""}.html" target="_blank"
											title="${item.productName!""}">
								<img class="lazy" style="border:1px solid #E8E8E8;max-height:100px;max-width:80px;" data-reactid=".0.3.2.$0.0.0.1.1.1:$0.0.0.0.0"
										border="0" src="${systemSetting().imageRootPath}${item.picture!""}" width="80" height="100">
							</a>
							<div class="aft_d">
								<a style="cursor: pointer;margin: auto;" href="${systemSetting().item}/product/${item.productID!""}.html" target="_blank" title="${item.productName!""}" width="250">
									<#if item.productName??>
											<#if item.productName?length gte 40>
												${item.productName?substring(0,40)}...
											<#else>
												${item.productName!""}
											</#if>
									</#if>
								</a>
							</div>
							<div class="aft_d1">
								${item.specInfo!""}
							</div>
					</td>
					<td class="aft_t1">
						<#if item.productStatus?? && item.productStatus=="1">
							即将上线
						<#elseif item.productStatus?? && item.productStatus=="2">
							售卖中
						<#elseif item.productStatus?? && item.productStatus=="3">
							已下架
						</#if>
					</td>
					<td class="aft_t1">${item.price?if_exists}</td>
					<td class="aft_t1">x${item.number!""}</td>
					<td class="aft_t1">
						<#if item.status?? && item.status =="finish">
							<#if item.repairsStatus?? && item.repairsStatus == 1>
								[<a href="${basepath}/account/custServiceDetail?id=${item.repairsId!""}&productID=${item.productID!""}">申诉中</a>]
							<#elseif item.repairsStatus?? && item.repairsStatus == 2>
								<!-- [<a href="${basepath}/account/alert_sure?id=${item.repairsId!""}">确认完成</a>] -->
								[<a href="${basepath}/account/custServiceDetail?id=${item.repairsId!""}&productID=${item.productID!""}">处理中</a>]
							<#elseif item.repairsStatus?? && item.repairsStatus == 3>
								[<a href="${basepath}/account/custServiceDetail?id=${item.repairsId!""}&productID=${item.productID!""}">已完成</a>]
							<#else>
								[<a href="${basepath}/account/custService?id=${item.id!""}&productID=${item.productID!""}">申请售后</a>]
							</#if>
						<#else>&nbsp;</#if>
					</td>
					<#if item.index?? && item.index==0>
					<td class="aft_t3" rowspan="${itemt.indextotalNum?if_exists}">
						<#if item.addTime??>
							${item.addTime?string('yyyy-MM-dd')}</br>
							${item.addTime?string('HH:mm:ss')}
						<#else>
						 	&nbsp;
						</#if>
					</td>
					<td class="aft_t3" rowspan="${itemt.indextotalNum?if_exists}">
						<#if item.finishPayTime??>
							${item.finishPayTime?string('yyyy-MM-dd')}</br>
							${item.finishPayTime?string('HH:mm:ss')}
						<#else>
						 	&nbsp;
						</#if>
					</td>
					<td class="aft_t3" rowspan="${itemt.indextotalNum?if_exists}">
					<#--<#if item.paystatus?? && item.paystatus=="y"> -->
						<#if item.status?? && item.status =="init">
							待付款
						<#elseif item.status?? && item.status =="pass">
							待补款
						<#elseif item.status?? && item.status =="stay">
							待发货
						<#elseif item.status?? && item.status =="send">
							待收货<br/>
						  <a href="#" onclick="qryApiStoreInfo(${item.expressNo!""})">查看物流</a>
						<#elseif item.status?? && item.status =="sign">
							已签收<br/>
						  <a href="#" onclick="qryApiStoreInfo(${item.expressNo!""})">查看物流</a>
						<#elseif item.status?? && item.status =="finish">
							交易完成<br/>
						  <a href="#" onclick="qryApiStoreInfo(${item.expressNo!""})">查看物流</a>
						 <#elseif item.status?? && item.status =="cancel">
							已失效<br/>
						</#if>
						<#--<#elseif item.status?? && item.status =="cancel">
							已退单
						<#else> &nbsp;</#if>-->
						
					</td>
					<td class="aft_t3" rowspan="${itemt.indextotalNum?if_exists}">
						<div>
                            <a target="_blank" href="${systemSetting().my}/order/${item.id!""}">订单详情</a>
					 		<#--<#if item.status?? && item.isComment?? && item.closedComment?? && item.status =="finish" && item.isComment !="1" && item.closedComment !="y">-->
					 		
					 		<#if  item.status?? && item.status =="init">
					 			<br/>
					 			<a target="_blank" href="${systemSetting().my}/order/toOrderPay?id=${item.id!""}">
					 				<input type="button" value="立即付款" class="head_td_pay"/>
					 			</a><br/>
					 			<a href="${systemSetting().orders}/order/delOrder?orderid=${item.id!""}" onclick="return confirm('确定取消?');">
					 				取消订单
					 			</a>
					 		<#elseif  item.status?? && item.status =="pass" && item.passIsStart?? && item.passIsStart =="1">
					 			<br/>
					 			<a target="_blank" href="${systemSetting().my}/order/plac_add?id=${item.id!""}">
					 				<input type="button" value="去补款" class="head_td_pay"/>
					 			</a>
							<#elseif (item.status?? && item.status =="sign") || (item.status?? && item.status =="send")>
								<br/>
								<a id="confirm" href="${systemSetting().my}/order/confirmReceipt?orderid=${item.id!""}">
									<input type="button" value="确认收货" class="head_td_pay"/>
								</a>
							</#if>
							
						</div>
					</td>
					<#else>
						<#--<td class="aft_t3"></td>
						<td class="aft_t3"></td>
						<td class="aft_t3"></td>
						<td class="aft_t3"></td>-->
					</#if>
				</tr>
				</#list>
				<tr class="aft_tr">
						<td colspan="9" class="right">
							<label class="aft_lb">
								订单类型:
								<#if itemt.orderType==2>
									预售订单
								<#elseif itemt.orderType==1>
									现货订单
								</#if>
							</label>
							<label class="aft_lb">
								商品总价:
									<#if itemt.orderType==1>
										${itemt.ptotal?number+itemt.fee?number}（包含运费）
									<#elseif itemt.orderType==2 && itemt.status =="pass">
										${itemt.ptotal?number}(不包含运费)
									<#elseif itemt.orderType==2 && itemt.status =="stay">
										${itemt.ptotal?number+itemt.fee?number}(包含运费)
									</#if>
							</label>
							<label class="aft_lb">
								已支付:
									<#if itemt.orderType==1>
										<#if itemt.coupons??>
											${itemt.ptotal?number+itemt.fee?number-itemt.coupons?number}
										<#else>
											${itemt.ptotal?number+itemt.fee?number}
										</#if>
									<#elseif itemt.orderType==2 && itemt.status =="pass">
										${itemt.advancePrice?number}
									<#elseif itemt.orderType==2 && itemt.status =="stay">
										<#if itemt.coupons??>
											${itemt.ptotal?number+itemt.fee?number-itemt.coupons?number}
										<#else>
											${itemt.ptotal?number+itemt.fee?number}
										</#if>
									</#if>
							</label>
							<label class="aft_lb">
								<#if itemt.status?? && itemt.status =="pass">
									待补款：
								<#else>
									<#if itemt.orderType==1 && itemt.status =="init">
									待支付：
									<#elseif itemt.orderType==2 && itemt.status =="init">
									待支付：
									<#else>
									</#if>
								</#if>
								<#if itemt.status?? && itemt.status =="init">
									<#if itemt.orderType==2>
										${itemt.advancePrice?number}
									<#else>
										${itemt.ptotal?number+itemt.fee?number-itemt.orderpay.alreadyPayAmount?number}
									</#if>
								<#else>
									<#if itemt.orderType==1 && itemt.status !="init">
									<#elseif itemt.orderType==2 && itemt.status=="pass">
										${itemt.ptotal?number-itemt.advancePrice?number}
									<#elseif itemt.orderType==2 && itemt.status=="stay">
									<#elseif itemt.orderType==2 && itemt.status=="finish">
									<#else>
									${itemt.ptotal?number+itemt.fee?number-itemt.orderpay.alreadyPayAmount?number}
									</#if>
								</#if>
							</label>
								订单总额:
								<label class="aft_sum">
								￥
								<#if itemt.coupons??>
									<#if itemt.deductible??>
									${itemt.ptotal?number+itemt.fee?number-itemt.coupons?number-itemt.deductible?number}
									<#else>
									${itemt.ptotal?number+itemt.fee?number-itemt.coupons?number}
									</#if>
								 <#else>
									 <#if itemt.deductible??>
										${itemt.ptotal?number+itemt.fee?number-itemt.coupons?number-itemt.deductible?number}
										<#else>
										${itemt.ptotal?number+itemt.fee?number}
									  </#if>
								</#if>
								</label>						
						</td>
					</tr>
				</#list>
				</table>
			</div>
		</div>
		<div style="height:50px;" align="right">
		
			<span style=" margin-right:10px;">
				<#if pager??>
					<#include "/shopindex/common/ftl/pager.ftl"/>
				</#if>
			</span>
		</div>
	
	</div>
</div>
<#if orderCancel??>
<script type="text/javascript">
	alert("订单已取消！");
</script>
</#if>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script type="text/javascript"src="${systemSetting().staticSource}/static/frontend/v1/js/new.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery.Jcrop.js"></script>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript">
function qryApiStoreInfo(id) {
	var url="ApiStoreInfo.jsp?id="+id; //转向网页的地址;
	var name="订单物流"; //网页名称，可为空;
	var iWidth="800"; //弹出窗口的宽度;
	var iHeight="400"; //弹出窗口的高度;
	var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
	var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;
	window.open(url,name,'height='+iHeight+',innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',top='+iTop+',left='+iLeft+',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no');
	console.log("_url="+_url);
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  success: function(data){
		  alert(data);
	  },
	  dataType: "text",
	  error:function(er){
		  console.log("addToFavorite.er="+er);
	  }
	});
 }

</script>
</@htmlBase.htmlBase>
</body>
</html>