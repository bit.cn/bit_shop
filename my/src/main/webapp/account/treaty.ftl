<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>aniToys在线服务协议</title>
        <style>

        	*{margin:0px;padding:0px;font-size: 12px;line-height: 14px;font-weight: normal;color:#333333;list-style: none;font-family:Arial, "宋体";}
        	a{text-decoration: underline;color: #0000cc;}
        	.mod-header,
        	.mod-bread-wrapper,
        	.mod-useragreement,
        	.mod-footer{
        		width:960px;
        		margin:0 auto;
        	}
        	.mod-header{
        		padding:15px;
        		height:55px;
        	}

        	.mod-bread{
        		height:40px;
        		border-top:1px solid #eeeeee;
        		border-bottom:1px solid #eeeeee;
        		background-color: #fafafa;
        		box-shadow: 1px 1px 2px #f7f7f7;
        	}
        	.mod-bread .bread-left{
        		text-indent: 2em;
        		line-height: 40px;
				font-size: 14px;
				font-weight: bold;
        	}

			.mod-useragreement{
				padding:20px 0px 16px 0px;
				line-height: 22px;
			}
			.useragreement-content{
				width:700px;
				margin-bottom: 20px;
				margin:0 auto;
			}
			.useragreement-content h5{
				font-size: 14px;
				font-weight: bold;
				padding:1em 0px;
			}
			.useragreement-content h3,
			.useragreement-content p,
			.useragreement-content li{
				line-height: 1.8em;
			}
			.useragreement-content li{
				text-indent: 2em;
			}
			.mod-footer{
				text-align: center;
				color: #7a77c8;
				margin: 40px auto;
			}
        </style>
    </head>
    <body>
        <div class="mod-header grid-96">
            <div class="logo">
                <!--  <a href="http://www.baidu.com"><img alt="baidu" src="https://passport.baidu.com/img/logo.gif"></a> -->
            </div>
        </div>
      
        <div class="mod-bread clearfix">
            <div class="mod-bread-wrapper">
                <div class="bread-left">
                   aniToys在线服务协议
                </div>
            </div>
        </div>
        
        <div class="mod-useragreement rid-96 useragreement-content">
        	一、总则
			1．1　用户应当同意本协议的条款并按照页面上的提示完成全部的注册程序。用户在进行注册程序过程中点击"同意"按钮即表示用户与本公司达成协议，
				完全接受本协议项下的全部条款。
			1．2　用户注册成功后，商城将给予每个用户一个用户帐号及相应的密码，该用户帐号和密码由用户负责保管；用户应当对以其用户帐号进行的所有活动
				和事件负法律责任。
			1．3　用户一经注册商城帐号，除非子频道要求单独开通权限，用户有权利用该账号使用商城各个频道的单项服务，当用户使用商城各单项服务时，用户
				的使用行为视为其对该单项服务的服务条款以及商城在该单项服务中发出的各类公告的同意。
			1．4　商城会员服务协议以及各个频道单项服务条款和公告可由商城公司随时更新，且无需另行通知。您在使用相关服务时,应关注并遵守其所适用的相关条款。
			您在使用商城提供的各项服务之前，应仔细阅读本服务协议。如您不同意本服务协议及/或随时对其的修改，您可以主动取消商城提供的服务；您一旦使用
				商城服务，即视为您已了解并完全同意本服务协议各项内容，包括商城对服务协议随时所做的任何修改，并成为商城用户。<div>
		<div class="mod-footer grid-96">
           <spen> &copy;2016 aniToys 使用动漫商城必读 </spen>
        </div>
    </body>
</html>
