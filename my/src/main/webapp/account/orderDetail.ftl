<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/shoppingCart.css" rel="stylesheet" type="text/css">
	<title>订单详情</title>
	<style type="text/css">
		.STYLE3 {
					font-size: 15px;
					font-weight: 700;
					margin-right: 50px;
				}
				.STYLE4 {
					color: #FF6600;
				}
				
				.STYLE5 {
					font-size: 14px;
					font-weight: bold;
				}
				
				.STYLE6 {
					font-size: 14px
				}
				ul,
				li {
					list-style: none;
				}
				
				#tab {
					min-height: 200px;
				}
				#tab .tabList{
					height:30px;
				}
				#tab .tabList ul li {
					float: left;
					background: #fefefe;
					background: -moz-linear-gradient(top, #fefefe, #ededed);
					background: -o-linear-gradient(left top, left bottom, from(#fefefe), to(#ededed));
					background: -webkit-gradient(linear, left top, left bottom, from(#fefefe), to(#ededed));
					border: 1px solid #E5E5E5;
					padding: 5px 0;
					width: 100px;
					text-align: center;
					position: relative;
					cursor: pointer;
				}
				
				#tab .tabCon {
					border: 1px solid #E5E5E5;
					background: #FAFAFA;
					border-collapse: collapse;
					width: 100%;
					min-height: 150px;
				}
				
				#tab .tabCon div {
					padding: 10px;
					opacity: 0;
					display:none;
					filter: alpha(opacity=0);
				}
				
				#tab .tabList li.cur {
					border-bottom: none;
					background: #fff;
				}
				
				#tab .tabCon div.cur {
					opacity: 1;
					display:block;
					filter: alpha(opacity=100);
				}
				
				.aft_tra {
					width: 1200px;
					border-left: 1px #E5E5E5 solid;
					border-top: 1px #E5E5E5 solid;
					border-right: 1px #E5E5E5 solid;
					border-bottom: 1px #E5E5E5 solid;
				}
				
				.aft_tra td{border-top:1px solid #eee;}
				
				.aft_ta {
					width: 350px;
					border-top: 1px #E5E5E5 solid;
				}
			</style>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
	<div style="margin-top:10px;">
		<blockquote>
		  <p><a href="${systemSetting().my}/account/orders?"  target="_self"><span class="STYLE3">我的anitoys</span></a>	-- <a href="${basepath}/account/orders" class="STYLE3">订单管理中心</a> -- <span class="STYLE3">订单：</span>${e.orderCode?if_exists}	
		      	<#if e.status?? && e.status =="init">
		      		<a target="_blank" href="${systemSetting().my}/order/toOrderPay?id=${e.id?if_exists}">
					<input type="submit" class="btn-style-01" value="继续支付"/>
					</a>
				<#elseif e.status?? && e.status =="pass" && e.passIsStart?? && e.passIsStart =="1">
					<a target="_blank" href="${systemSetting().orders}/order/plac_add?id=${e.id?if_exists}">
					<input type="submit" class="btn-style-01" value="继续支付"/>
					</a>
				<#elseif e.status?? && e.status =="send">
					<input type="submit" class="btn-style-01" value="确认收货"/>
				<#elseif e.status?? && e.status =="sign">
					<input type="submit" class="btn-style-01" value="确认收货"/>
				<#elseif e.status?? && e.status =="finish">
					
				</#if>
		      
          </p>
	  </blockquote>
	</div>
    <div style="margin-top:10px;border:1px #E5E5E5 solid;background:#fafafa;">
		<div style="margin-left:20px;min-height: 80px;">
			<div style="margin-top: 10px;">
	  		订单编号： ${e.orderCode?if_exists}<span style="margin-left: 20px;">状态：</span>  
	  		<span class="STYLE4">
				<#if e.status?? && e.status =="init">
					待付款
				<#elseif e.status?? && e.status =="pass">
					待补款
				<#elseif e.status?? && e.status =="stay">
					待发货
				<#elseif e.status?? && e.status =="send">
					待收货
				<#elseif e.status?? && e.status =="sign">
					已签收
				<#elseif e.status?? && e.status =="finish">
					完成
				<#elseif e.status?? && e.status =="cancel">
					失效
				</#if>
			</span>
			</div>
			<div style="border:1px solid #E5E5E5;border-collapse:collapse;width: 98%;height: 0px;margin-bottom: 10px; margin-top: 10px;"></div>
		<span class="STYLE6">订单已经完成，感谢您在anitoys购物，欢迎您对本次交易及所购商品进行评价。发表评价</span>	  	
		</div>
	</div>
  <div style="width:100%;height:auto;position: relative;overflow: hidden;border:0px groove;text-align: center;"><!--border-top:10px groove;-->
		<div style="margin-right:20px; margin-top:25px; margin-left:20px;display:inline-block;white-spacing:nowrap;border: 0px solid;overflow: hidden;">
				<#if e.receiptTime??>
					<img alt="" src="${systemSetting().staticSource}/account/img/lc04.png">
				<#elseif e.status?? && e.status == "stay">
					<img alt="" src="${systemSetting().staticSource}/account/img/lc03.png">
				<#elseif e.payTime?? || e.finishPayTime?? || e.replenishmentTime??>
					<img alt="" src="${systemSetting().staticSource}/account/img/lc02.png">
				<#else>
					<img alt="" src="${systemSetting().staticSource}/account/img/lc01.png">
				</#if>
		</div>
		<div style="width:100%;margin-top:10px;float:left;overflow-x: hidden; overflow-y: hidden;border: 0px solid;">
			<div style="float:left;overflow-x: hidden; overflow-y: hidden;min-width:320px; max-width:320px;">
				<div style="margin-left:15px;">
				<#if e.addTime??>
					提交订单<br />
					${e.addTime?string('yyyy-MM-dd')}<br />
					${e.addTime?string('HH:mm:ss')}
				</#if>
				</div>
			</div>
			<div style="float:left;min-height:150px; text-align:center;min-width:300px; max-width:300px;">
				<#if e.payTime?? || e.finishPayTime?? || e.replenishmentTime??>
					<#if e.finishPayTime??>
					付款成功<br />
					${e.finishPayTime?string('yyyy-MM-dd')}<br />
					${e.finishPayTime?string('HH:mm:ss')}<br />
					</#if>
					<#if e.payTime??>
					支付首款<br />
					${e.payTime?string('yyyy-MM-dd')}<br />
					${e.payTime?string('HH:mm:ss')}<br />
					</#if>
					<#if e.replenishmentTime??>
					支付尾款<br />
					${e.replenishmentTime?string('yyyy-MM-dd')}<br />
					${e.replenishmentTime?string('HH:mm:ss')}<br />
					</#if>
				<#else>
					付款成功
				</#if>
			</div>
			<div style="float:left; text-align:center;min-width:250px; max-width:250px;">
				<#if e.status?? && e.status == "stay">
					等待发货<br />
					2015-12-24<br />
					11:15:30
				<#else>
					等待发货
				</#if>
			</div>
			<div style="float:left; text-align:center;min-width:310px; max-width:310px;">
				<#if e.receiptTime??>
					完成<br />
					${e.receiptTime?string('yyyy-MM-dd')}<br />
					${e.receiptTime?string('HH:mm:ss')}<br />
				<#else>
					完成
				</#if>
			</div>
		</div>
	</div>
	<!-- 订单跟踪/付款信息 -->
	<div id="tab" style="margin-top:20px;width:100%;margin-top:10px;float:left; height:100%;">
	  <div class="tabList">
		<ul>
			<li class="cur">订单跟踪</li>
			<li>付款信息</li>
		</ul>
	  </div>
	  <div class="tabCon">
		<!--<div class="cur" style="width:100%;float:left; border:0px #E5E5E5 solid;margin-bottom:10px;">-->
		<div class="tabLi cur">
			<table style="width: 95%;margin-bottom:10px;border-collapse:separate; border-spacing:10px;">
					<tr style="font-weight: bold;">
						<td>处理时间</td>
						<td>处理信息</td>
						<td>操作人</td>
					</tr>
					<tr style="font-weight: bold;">
						<td colspan="3" style="border-top:1px #E5E5E5 solid;">
						</td>
					</tr>
					<#if logList??>
						<#list logList as item>
						<tr>
							<td>
								<#if item.createdate??>
									${item.createdate}
								</#if>
							</td>
							<td>${item.content?if_exists}</td>
							<td>${item.accountName?if_exists}</td>
						</tr>
						</#list>
					<#else>
						<tr>
							<td colspan="3">无订单记录！</td>
						</tr>
					</#if>
				</table>
		
		</div>
		<div class="tabLi">
				<table style="width: 95%;border-collapse:separate; border-spacing:10px;">
					<tr style="font-weight: bold;">
						<td>时间</td>
						<td>金额</td>
						<td>备注</td>
					</tr>
					<tr style="font-weight: bold;">
						<td colspan="4" style="border-top:1px #E5E5E5 solid;">
						</td>
					</tr>
					<!--支付信息-->
					<#if payList??>
						<#list payList as item>
						<tr>
							<td><#if item.createtime??>${item.createtime!""}</#if></td>
							<td>
							${item.payamount?if_exists}
							</td>
							<td>
							<#if item.paymethod="amtWay">
								<#if item.payType??>
									<#if item.payType="1">
									        定金支付，支付方式：余额支付
									<#elseif item.payType="2">
										补款支付，支付方式：余额支付
									<#else>
									现货支付，支付方式：余额支付
									</#if>
								<#else>
									现货支付，支付方式：余额支付
								</#if>
							<#elseif item.paymethod="baoWay">
								<#if item.payType??>
									<#if item.payType="1">
									        定金支付，支付方式：支付宝支付
									<#elseif item.payType="2">
										补款支付，支付方式：支付宝支付
									<#else>
										现货支付，支付方式：支付宝支付
									</#if>
								<#else>
										现货支付，支付方式：支付宝支付
								</#if>
							<#else>
								<#if item.payType??>
									<#if item.payType="1">
									        定金支付，支付方式：现金支付
									<#elseif item.payType="2">
										补款支付，支付方式：现金支付
									<#else>
										现货支付，支付方式：现金支付
									</#if>
								<#else>
									现货支付，支付方式：支付宝支付
								</#if>
							</#if>
							</td>
						</tr>
						</#list>
					<#else>
						<tr>
							<td colspan="4">无支付记录！</td>
						</tr>
					</#if>
					<!--积分使用 日志-->
						<tr>
							<td><#if e.addTime??>${e.addTime?string('yyyy-MM-dd HH:mm:ss')}</#if></td>
							<td>
							${e.amountExchangeScore?if_exists}
							</td>
							<td>
								当前订单消费积分
							</td>
						</tr>
					<#if pointsLogList??>
						<#list pointsLogList as item>
						<tr>
							<td><#if item.addTime??>${item.addTime!""}</#if></td>
							<td>
							${item.rankCount?if_exists}
							</td>
							<td>
							${item.description?if_exists}
							</td>
						</tr>
						</#list>
					<#else>
						<tr>
							<td colspan="4">无积分使用记录！</td>
						</tr>
					</#if>
					<!--余额使用 日志-->
					<#if accountRankLogList??>
						<#list accountRankLogList as item>
						<tr>
							<td><#if item.addTime??>${item.addTime}</#if></td>
							<td>
							${item.amountCount?if_exists}
							</td>
							<td>
							${item.description?if_exists}
							</td>
						</tr>
						</#list>
					<#else>
						<tr>
							<td colspan="4">无积分使用记录！</td>
						</tr>
					</#if>
					<!--优惠券信息-->
					<#if e.coupons??>
					<tr>
							<td><#if e.finishPayTime??>${e.finishPayTime?datetime!""}</#if></td>
							<td>
							${e.coupons?if_exists}
							</td>
							<td>
							商家优惠券
							</td>
						</tr>
					</#if>
					<!--抵扣券信息-->
					<!--优惠券信息-->
					<#if e.coupons??>
					<tr>
							<td><#if e.finishPayTime??>${e.finishPayTime?datetime!""}</#if></td>
							<td>
							${e.deductible?if_exists}
							</td>
							<td>
							平台抵扣券
							</td>
						</tr>
					</#if>
				</table>
		</div>
	  </div>
	</div>
	<!-- 代码 结束 -->
	<#--<div style="clear:both"></div><br /><br /><br /><br /><br /><br />
	<div style="text-align:center">
	<p></p>
	</div>
	-->
	<!-- 订单信息 -->
	<div style="width:100%;margin-top:10px;float:left; height:100%;background:#fafafa;">
		<div style="margin-top:10px;margin-left:10px;">
			<span class="STYLE3">订单信息</span>
		</div>
		<div style="width:98%;margin-top:10px;margin-left:10px;background:#ffffff;">
			<div style="width:98%;margin-top:10px;margin-left:15px">
				<br/><span class="STYLE5">收货人信息</span>
				<div style="min-height: 40px;margin-top:10px;">
					<#if e.ordership??>
						${e.ordership.shipname?if_exists } &nbsp;&nbsp;&nbsp;&nbsp;  ${e.ordership.phone?if_exists } <br />
					${e.ordership.province?if_exists }&nbsp;${e.ordership.city?if_exists }&nbsp;${e.ordership.area?if_exists }&nbsp;${e.ordership.shipaddress?if_exists }&nbsp;
					</#if>
				</div>
			</div>
			<div style="width:98%;margin-left:15px;margin-top:10px;border-top: 1px #FAFAFA solid;margin-bottom: 10px;">
				<div style="margin-top:10px;margin-bottom: 5px;">
				<span class="STYLE5">配送方式</span>
				</div>
				<div style="min-height: 30px;">
					快递方式：${e.expressName?if_exists }  &nbsp;&nbsp;&nbsp;&nbsp;  运费：<#if e.fee?? && e.fee =="0.00" >全国包邮 <#else>${e.fee?if_exists }</#if> <br />
				</div>
				
			</div>
			<div style="width:98%;margin-left:15px;margin-top:10px;border-top: 1px #FAFAFA solid;margin-bottom: 10px;">
				<span class="STYLE5">商品清单</span>
			</div>
		</div>
		<div style="margin-top:10px;margin-left:10px;height:100%;">
			<div style="height:99%; border:0px #E5E5E5 solid;margin-bottom:10px;">
						<table style="width: 99%;background-color:#FFFFFF;margin-bottom:10px;margin-top:3px;">
							<tr style="background-color:#fafafa;height:30px;border:0px #E5E5E5 solid;" >
								<td width="340px;" style="padding-left: 15px;">商品信息</td>
								<td>商品状态</td>
								<td>数量</td>
								<td>商品定金</td>
								<td>商品尾款</td>
								<td>商品全额</td>
							</tr>
							<#list e.orders as item>
							<tr class="aft_tra">
								<td class="aft_ta">
								<input type="hidden" name="productId" value="${item.productID?if_exists }"/> 
								<input type="hidden" name="productName" value="${item.productName?if_exists }"/> 
								<input type="hidden" name="repairCount" value="${item.number?if_exists }"/> 
									<a style="float:left;width:27%;margin-right:2%;margin-top:2%;margin-bottom:2%;text-align:center;" href="${systemSetting().item}/product/${item.productID?if_exists}.html" target="_blank"
											title="${item.productName?if_exists}">
								<img class="lazy" style="border:1px solid #E8E8E8;max-height:100px;max-width:80px;" data-reactid=".0.3.2.$0.0.0.1.1.1:$0.0.0.0.0"
										src="${systemSetting().imageRootPath}${item.picture?if_exists}" width="80" height="100">
							</a>
							<div class="aft_d">
								<a style="cursor: pointer;margin: auto;" href="${systemSetting().item}/product/${item.productID?if_exists}.html" target="_blank" title="${item.productName?if_exists}" width="250">
									<#if item.productName??>
											<#if item.productName?length gte 40>
												${item.productName?substring(0,40)}...
											<#else>
												${item.productName!""}
											</#if>
									</#if>
								</a>
							</div>
							<div class="aft_d1">
								${item.specInfo!""}
							</div>
								</td>
								<td><#if item.productStatus?? &&
									item.productStatus=="1"> 即将上线 <#elseif item.productStatus?? &&
									item.productStatus=="2"> 售卖中 <#elseif item.productStatus?? &&
									item.productStatus=="2"> 已下架 </#if></td>
								<td>x${item.number?if_exists}</td>
								<td>
									<#if item.depositPrice??>
										${item.depositPrice?if_exists}
									<#else>
									 	0.00
									</#if>
								</td> 
								<td>
									<#if item.detailBalancePrice??>
										${item.detailBalancePrice?if_exists}
									<#else>
									 	0.00
									</#if>
								</td>
								<td>
									<#if item.total0??>
									 	${item.total0?if_exists}
									<#else>
										0.00
									</#if>
								<#--	<#if item.amount?? && item.fee??>
									${item.amount?number + item.fee?number}
									<#elseif item.amount??>
									 	${item.amount?if_exists}
									<#elseif item.fee??>
									 	${item.fee?if_exists}
									<#else>
										0.00
									</#if>
								-->
								</td> 
							</tr>
							</#list>
						</table>
			</div>
		</div>
	<!-- 买家备注 -->
	<div style="width:100%;margin-top:10px;float:left;width:100%;margin-bottom: 20px;">
		<span class="STYLE3" style="margin-left: 10px;">买家备注</span><br />
		<div style="width:98%; margin-top:5px; border:1px #E5E5E5 solid; margin-left:10px;font-size: 14px; height:150px;">&nbsp;&nbsp;
			<#if e.otherRequirement??>
				${e.otherRequirement?if_exists}
			</#if>
		</div>
	</div>
	<!-- 按钮操作 -->
	<div style="width:100%;margin-top:10px;margin-right:30px;text-align:right;margin-bottom: 20px;"><br/>
				<#if e.status?? && e.status =="init">
		      		<a target="_blank" href="${systemSetting().my}/order/toOrderPay?id=${e.id?if_exists}">
					<input type="submit" class="btn-style-01" value="继续支付"/>
					</a>
				<#elseif e.status?? && e.status =="pass" && e.passIsStart?? && e.passIsStart =="1">
					<a target="_blank" href="${systemSetting().orders}/order/plac_add?id=${e.id?if_exists}">
					<input type="submit" class="btn-style-01" value="继续支付"/>
					</a>
				<#elseif e.status?? && e.status =="send">
					<input type="submit" class="btn-style-01" value="确认收货"/>
				<#elseif e.status?? && e.status =="sign">
					<input type="submit" class="btn-style-01" value="确认收货"/>
				<#elseif e.status?? && e.status =="finish">
					
				</#if>
	</div>
	</div>
</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript">
$(function(){
				$(".tabList").find("li").bind("click",function(){
					var index = $(this).index();
					$(this).addClass("cur").siblings().removeClass("cur");
					$(".tabCon >div").eq(index).addClass("cur").siblings().removeClass("cur");
				});
			});
</script>
</@htmlBase.htmlBase>
</body>
</html>