<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
    <title>证件设置</title>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
    <div class="head_rig">
        <p class="head_my">
            账号安全
            <br />
        </p>
        <div class="bd_di">
            <ul class="bd_ul">
                <li class="bd_til_li">
					<span class="bd_til">
						证件信息
					</span>
                </li>
			<#if cardNo?? && cardNo!="">
                <li class="up_pwd_li">
                    真实姓名：
                    <input id="trueName" type="text" class="up_pwd_atp" value="${accSecurity.trueName!""}"/>
                </li>
                <li class="up_pwd_li">
                    证件号码：
                    <input id="jbCredentialsCode" type="text" class="up_pwd_atp" value="${cardNo!""}" disabled="disabled" />
                    <br/><span style="color:red;padding-left:45px;font-size:13px;">身份证件号很重要,是找回密码的重要依据。</span>
                </li>
			<#else>
                <li class="up_pwd_li">
                    真实姓名：
                    <input id="trueName" type="text" class="up_pwd_atp"/>
                </li>
                <li class="up_pwd_li">
                    证件号码：
                    <input id="jbCredentialsCode" type="text" class="up_pwd_atp"/>
							<span id="jbCredentialsCodeTip">
		                                <s:fielderror cssStyle="color:red;padding-left:10px;">
                                            <s:param></s:param>
                                        </s:fielderror>
		                     </span>
                    <br/><span style="color:red;padding-left:45px;font-size:13px;">身份证件号很重要,是找回密码的重要依据。</span>
                </li>
                <input type="button" onclick="checkjbCredentialsCode()" class="up_but" value="提交"/>
			</#if>
            </ul>
        </div>
    </div>
</div>
</body>
<script>
    $(function(){
        //失去焦点
        $('#jbCredentialsCode').blur(function() {
            var jbCredentialsCode = $("#jbCredentialsCode").val();
            checkCarNo(jbCredentialsCode);
        });
        //获得焦点
        $('#jbCredentialsCode').focus(function() {
            var jbCredentialsCode = $("#jbCredentialsCode").val();
            checkCarNo(jbCredentialsCode);
        });
    });

    function checkCarNo(jbCredentialsCode){

        var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        if (trim(jbCredentialsCode) == "") {
            document.getElementById("jbCredentialsCodeTip").innerHTML = "<font color='red'>× 证件号码不能为空</font>";
            return false;
        } else if(trim(jbCredentialsCode) != ""){
            if(!reg.test(jbCredentialsCode)){
                document.getElementById("jbCredentialsCodeTip").innerHTML = "<font color='red'>× 请输入合法的证件号码</font>";
                return false;
            }else{
                document.getElementById("jbCredentialsCodeTip").innerHTML = "<font color='#339933'>√ 证件号码输入正确</font>";
                return true;
            }
        }
    }
    //去掉前后空格
    function trim(str) {
        var strnew = str.replace(/^\s*|\s*$/g, "");
        return strnew;
    }
    // 证件号码
    function checkjbCredentialsCode() {
        var jbCredentialsCode = $("#jbCredentialsCode").val();
        if(checkCarNo(jbCredentialsCode)){
            // 向后台发送处理数据
            var trueName = $("#trueName").val();
            $.ajax({
                url : "${basepath}/accountSecurity/doChangeCert",// 目标地址
                data : {cardNO : jbCredentialsCode,trueName:trueName}, // 目标参数
                type : "POST", // 用POST方式传输
                dataType : "json", // 数据格式:json
                success : function(data) {
                    alert("修改证件号码成功");
                    location.href="${basepath}/accountSecurity/my_binding";  跳转到账户安全初始化
                }
            });
        }
    }
</script>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>