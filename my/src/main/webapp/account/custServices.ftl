<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<title>我的订单</title>
	<style type="text/css">
	.STYLE1 {font-size: 12px;color: #ffffff;}
	.STYLE2 {font-size: 12px;}
	</style>
</head>
<body>
<!--商城主页top页-->
 <@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig">
		<p class="head_my">
			我的售后订单
			<br />
		</p>
		<div class="head_ul">
			<ul class="centcustservice">
				<p class="cent_p"></p>
				<li class="myomt">
					<span>
						<a href="${basepath}/account/custService?status=0">
							全部<#if repairsSimpleReport??>
								(${repairsSimpleReport.repairAllcount?if_exists })
							   <#else>(0)</#if>
							   
						</a>
					</span>
					<span>
						<a href="${basepath}/account/custService?status=1">
							换货<#if repairsSimpleReport??>
								(${repairsSimpleReport.repairBartercount?if_exists })
							   <#else>(0)</#if>
						</a>
					</span>
					<span>
						<a href="${basepath}/account/custService?status=2">
							退款<#if repairsSimpleReport??>
								(${repairsSimpleReport.repairRefundcount?if_exists })
							   <#else>(0)</#if>
						</a>
					</span>
				</li>
			</ul>
		</div>
		<div class="head_mk">
			<div class="head_mk_pak">
				<table class="head_pak_id">
			<#if pager??>
			<#if pager?size != 0>
				<#list pager as myorder>
					<tr class="aft_tr2"><td colspan="8"></td></tr>
					<tr class="head_tb_tr"  align="left">
							<td width="340px;">
								<span class="tr_id"> 订单编号:
	                                 <a target="_blank" href="${systemSetting().orders}/order/${myorder.id!""}">
										${myorder.orderCode!""}
									</a>
	                            </span>
								<span class="tr_sto"><img src="${systemSetting().staticSource}/shopindex/layout/img/omt/sto.png"/> 
									<a target="_blank" href="${systemSetting().business}/business/businessSelect?businessId=${myorder.businessId!""}">
									${myorder.businessName!""}
									</a>							
								</span>						
							</td>
							<td>商品状态</td>
						<td>成交单价</td>
						<td>数量</td>
						<td>下单时间</td>
						<td>支付时间</td>
						<td>订单状态</td>
						<td class="aft_t2">操作</td>
						</tr>
					<#list myorder.orders as item>
					<tr class="aft_tr1">
					
						<td class="aft_t">
							<#-- 商品图片路径
							<a href="${systemSetting().item}/product/${item.productId!""}.html" target="_blank" title="${item.productName!""}">
								<img style="width: 100%;height: 100%;border: 0px;" alt="" src="${systemSetting().imageRootPath}${item.picture!""}" onerror="nofind()"/>
							-->
							</a>
								<a style="float:left;width:27%;margin-right:2%;margin-top:2%;margin-bottom:2%;text-align:center;" href="${systemSetting().item}/product/${item.productId!""}.html" target="_blank"
												title="${item.productName!""}">
									<img class="lazy" style="border:1px solid #E8E8E8;max-height:100px;max-width:80px;" data-reactid=".0.3.2.$0.0.0.1.1.1:$0.0.0.0.0"
											border="0" src="${systemSetting().imageRootPath}${item.picture!""}" width="80" height="100">
								</a>
								<div class="aft_d">
									<a style="cursor: pointer;margin: auto;" href="${systemSetting().item}/product/${item.productId!""}.html" target="_blank" title="${item.productName!""}" width="250">
										${item.productName!""}
									</a>
								</div>
								<div class="aft_d1">
									${item.specInfo!""}
								</div>
						</td>
						<td class="aft_t1">
							<#if item.productStatus?? && item.productStatus=="1">
								即将上线
							<#elseif item.productStatus?? && item.productStatus=="3">
								已下架
						<#--	<#elseif item.productStatus?? && item.productStatus=="3">  -->
							<#else>
								售卖中
							</#if>
						</td>
						<td class="aft_t1"><#if item.price??>
										${item.price?string.currency}  
									<#else>
									 	￥0.00
									</#if></td>
						<td class="aft_t2">x${item.number!""}</td>
						<#if item_index == 0>
							<td class="aft_t2" rowspan="${myorder.orders?size}">
								<#if myorder.addTime??>
									${myorder.addTime?string('yyyy-MM-dd')}</br>
									${myorder.addTime?string('HH:mm:ss')}
								<#else>
								 	&nbsp;
								</#if>
							</td>
							
							<td class="aft_t2" rowspan="${myorder.orders?size}">
								<#if myorder.finishPayTime??>
									${myorder.finishPayTime?string('yyyy-MM-dd')}</br>
									${myorder.finishPayTime?string('HH:mm:ss')}
								<#else>
								 	&nbsp;
								</#if>
							</td>
							<td class="aft_t2" rowspan="${myorder.orders?size}">
								<#if myorder.status?? && myorder.status =="init">
									待付款
								<#elseif myorder.status?? && myorder.status =="pass">
									待补款
								<#elseif myorder.status?? && myorder.status =="stay">
									待发货
								<#elseif myorder.status?? && myorder.status =="send">
									待收货<br/>
								<#elseif myorder.status?? && myorder.status =="sign">
									已签收<br/>
								<#elseif myorder.status?? && myorder.status =="finish">
									交易完成<br/>
								 <#elseif myorder.status?? && myorder.status =="cancel">
									已失效<br/>
								</#if>
							</td>
							<td class="aft_t2" rowspan="${myorder.orders?size}" style="line-height:2em;">
								<a target="_blank" href="${basepath}/account/custServiceDetail?id=${myorder.repairsId!""}">
						 			售后详情
						 		</a>
							<#--
								<#if item.repairsStatus?? && myorder.repairsStatus ==1>
									<a href="#">退单中</a> 
								<#elseif item.repairsStatus?? && myorder.repairsStatus ==2>
									<a href="#">售后中</a></br> &nbsp;
									<a target="_blank" href="${basepath}/account/alert_sure?id=${myorder.repairsId!""}">
						 			<input type="button" value="确认完成" class="head_td_pay"/>
						 		</a>
								<#elseif item.repairsStatus?? && item.repairsStatus ==3>
									<a href="#">已处理</a>
								<#else>
									&nbsp;
								</#if>
							-->
							</td>
						</#if>
						
					</tr>
					</#list>
					<tr class="aft_tr">
							<td colspan="8" align="right">
									应支付订单金额:
								<label class="aft_sum">
									￥
									<#if myorder.amount?? && myorder.fee??>
											${myorder.amount?number + myorder.fee?number}
									<#elseif item.amount??>
									 	${myorder.amount!""}
									<#elseif myorder.fee??>
									 	${myorder.fee!""}
									<#else>
										0.00
									</#if>
								</label>						
							</td>
						</tr>
						</#list>
						<#else>
						<span style="height:55px;margin-top:6px;margin-left:30px;font-size:14px;color:#FF0000;background-color:#fafafa; ">
							暂时无售后订单申请信息！
						</span>
						</#if>
						<#else>
						<span style="height:55px;margin-top:6px;margin-left:30px;font-size:14px;color:#FF0000;background-color:#fafafa; ">
							暂时无售后订单申请信息！
						</span>
					</#if>
				</table>
			</div>
		</div>
	</div>
</div>
  <!-- 商城主页footer页 -->
  <@footer.shopFooter/>
  <script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>