<#import "/manage/tpl/sellerPageBase.ftl" as sellerPage>
<#import "/account/myBasePage.ftl" as myBasePage>
<#import "/account/myTop.ftl" as myTop>
<#import "/account/myLeft.ftl" as myLeft>
<@myBasePage.myBasePage/>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${systemSetting().staticSource}/shopindex/layout/css/omt.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery.Jcrop.js"></script>
<title>头像设置</title>
</head>
<body>
<@myTop.myTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig">
		<p class="head_my">
			头像设置
			<br />
			<img src="${systemSetting().staticSource}/shopindex/layout/img/omt/tr.png" style="margin-top:-25px;"/>
		</p>
		<div class="bd_di">
			<form role="form" id="form1" class="form-horizontal" method="post" action="uploadHeadImage" theme="simple"enctype="multipart/form-data"> 
			
			   <div class="modal-body text-center">
		        <div class="zxx_main_con">
		            <div class="zxx_test_list">
		                <input class="photo-file" type="file" name="imgFile" id="fcupload" onchange="readURL(this);"/>
		                <img alt="" src="" id="cutimg"/>
		                <input type="hidden" id="x" name="x"/>
		                <input type="hidden" id="y" name="y"/>
		                <input type="hidden" id="w" name="w"/>
		                <input type="hidden" id="h" name="h"/>
		            </div>
		        </div>
		    </div>
		     
		    <div class="modal-footer">
		        <button id="submit" onclick="">上传</button>
		    </div>

			</from>
		</div>	
		
	</div>
</div>
</body>
<script type="text/javascript">
      //定义一个全局api，这样操作起来比较灵活
        var api = null;
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = function (e) {
                    $('#cutimg').removeAttr('src');
                    $('#cutimg').attr('src', e.target.result);
                    api = $.Jcrop('#cutimg', {
                        setSelect: [ 20, 20, 200, 200 ],
                        aspectRatio: 1,
                        onSelect: updateCoords
                    });
                };
                if (api != undefined) {
                    api.destroy();
                }
            }
            function updateCoords(obj) {
                $("#x").val(obj.x);
                $("#y").val(obj.y);
                $("#w").val(obj.w);
                $("#h").val(obj.h);
            };
        }
    </script>
