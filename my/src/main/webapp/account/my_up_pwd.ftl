<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
    <title>修改密码</title>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
    <div class="head_rig">
        <p class="head_my">
            账号安全
            <br />
        </p>
        <div class="bd_di">
            <ul class="bd_ul">
                <li class="bd_til_li">
					<span class="bd_til">
						修改密码
					</span>
                </li>
                <li class="up_pwd_li">
                    	当前密码：
                    <input id="now_pwd" type="password" class="up_pwd_atp"/><span id="errorMeg"></span>
                </li>
                <li class="up_pwd_li1">
                   	新密码：
                    <input id="new_pwd" type="password" class="up_pwd_atp"/>
                </li>
                <li class="up_pwd_li">
                  	  确认密码：
                    <input id="qr_pwd" type="password" class="up_pwd_atp"/>
                    <span id="newPwdMeg"></span>
                </li>
                <input type="button" onclick="submit()" class="up_but" value="提交"/>
            </ul>
        </div>
    </div>
</div>
</body>
<script>

    function submit(){
        //先检查原密码是否正确
        var now_pwd = $("#now_pwd").val();
        var new_pwd = $("#new_pwd").val();
        var qr_pwd = $("#qr_pwd").val();
        if(new_pwd!=qr_pwd){
            $("#newPwdMeg").html("<font color=red>两次密码输入不一致！</font>");
            return;
        }else{
            $("#newPwdMeg").html("");
        }
        $.ajax({
            url:"${basepath}/accountSecurity/checkPassword",
            type:"post",
            data:{'password2':now_pwd},
            success:function(result){
                if(result==0){
                    //如果正确，进行修改
                    console.log(result);
                    $.ajax({
                        url:"${basepath}/accountSecurity/changePwd",
                        type:"post",
                        data:{'password2':now_pwd,'newPassword':new_pwd,'newPassword2':qr_pwd},
                        success:function(result){
                            alert("修改成功！");
                            location.href="${basepath}/account/exit";
                        }
                    });
                }else{
                    $("#errorMeg").html("<font color=red>密码不正确</font>");
                }
            }
        });
    }
</script>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>