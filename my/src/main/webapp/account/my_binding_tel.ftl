<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
	<title>绑定手机</title>
 </head>
  
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	
	<div class="head_rig">
		<p class="head_my">
			账号安全
			<br />
		</p>
		<div class="bd_di">
			<ul class="bd_ul">
				<li class="bd_til_li">
					<span class="bd_til">
						 <#if userPhone??&&userPhone!="">
						 	解绑手机
						 <#else>
						 	绑定手机
						  </#if>
					</span>
				</li>
				<li>
					手机号：
					 <#if userPhone??&&userPhone!="">
					 		<@phone '${userPhone}'/>
					 		<#macro phone idcardNum><#if idcardNum?length gte 11>${idcardNum[0..2]}*******${idcardNum[7..10]}<#else>${idcardNum}</#if></#macro>  
						<#else>
						 	<input id="telNum" type="text" class="bd_tel" />
					 </#if>
					<input id="btnSendCode" type="button" onclick="sendMessage()" class="bd_but" value="获取验证码"/>
				</li>
				<li>
					<span id="jbPhoneTip">  
                                <s:fielderror style="color:red;padding-left:40px;">  
                                    <s:param></s:param>  
                                </s:fielderror>  
                            </span>  
				</li>
				<li class="bd_til_li1">
					验证码：
					<input id="checkCode" type="text" class="bd_tel"/>
				</li>
				<li>
					<input id="sbumit" disabled='disabled' type="button" onclick="submit_tel()" class="bd_ok" value="提交"/>
				</li>
			</ul>
		</div>	
	</div>
</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script>
	$(function(){
		//失去焦点
		var telNum = "";
		<#if userPhone??&&userPhone!="">
			if(${userPhone}!=null){
				telNum = ${userPhone}+"";
			}
		<#else>
			
		</#if>
		$('#telNum').blur(function() { 
			telNum = $("#telNum").val();
			check_mobile(telNum);
		});
		//获得焦点
		$('#telNum').focus(function() { 
			telNum = $("#telNum").val();
			check_mobile(telNum);
		}); 
	});
	//验证手机号是否正确
	function check_mobile(mobile){
		<#if userPhone??&&userPhone!="">
			if(${userPhone}!=null){
			mobile = ${userPhone}+"";
			}
		<#else>
			
		</#if>
		var mobile=mobile.replace(/^\s*|\s*$/g,'');
		var length=mobile.length;
		var a=/^(1[3|4|5|8])[0-9]{9}$/;
		if (length==0)
		{
			$('#jbPhoneTip').html("<font color=red style='padding-left:40px'>× 手机号码不能为空</font>");
			$('#sbumit').attr('disabled','disabled');
			 return false;
		}
		else{
		if(a.test(mobile)) {
			$('#jbPhoneTip').html("<font color=green style='padding-left:40px'>√ 该手机号码，输入正确</font>");
			$('#sbumit').attr('disabled','');
			return true;
		}
		else{
			$('#jbPhoneTip').html("<font color=red style='padding-left:40px'>× 手机号码格式不正确</font>");
			$('#sbumit').attr('disabled','disabled');
			 return false;
			}
		}
	}
</script>

<script>

	var InterValObj; //timer变量，控制时间
var count = 60; //间隔函数，1秒执行
var curCount;//当前剩余秒数
var code = ""; //验证码
var codeLength = 6;//验证码长度

function sendMessage() {
	curCount = count;
	var jbPhone = $("#telNum").val();
	<#if userPhone??&&userPhone!="">
		if(${userPhone}!=null){
			jbPhone = ${userPhone}+"";
			$('#jbPhoneTip').html("<font color=green style='padding-left:40px'>√ 短信验证码已发到您的手机,请查收</font>");
		}
	<#else>
		
	</#if>
	var jbPhoneTip = $("#jbPhoneTip").text();
	if (jbPhone != "") {
		if(jbPhoneTip == "√ 该手机号码，输入正确" || jbPhoneTip == "√ 短信验证码已发到您的手机,请查收"){
			// 产生验证码
			for ( var i = 0; i < codeLength; i++) {
				code += parseInt(Math.random() * 9).toString();
			}
			// 设置button效果，开始计时
			$("#btnSendCode").attr("disabled", "true");
			$("#btnSendCode").val("请" + curCount + "秒后重新发送");
			InterValObj = window.setInterval(SetRemainTime, 1000); // 启动计时器，1秒执行一次
			setTimeout(setCodeTimeOut,600000);
			// 向后台发送处理数据
			$.ajax({
				type: "POST", // 用POST方式传输
				dataType: "json", // 数据格式:JSON
				url: "${basepath}/accountSecurity/getCheckNo", // 目标地址
				data: {'userPhone':jbPhone,'vcode':code},
				error: function (XMLHttpRequest, textStatus, errorThrown) { 
					$("#jbPhoneTip").html("<font color='red' style='padding-left:40px'>× 请输入验证码</font>");
				},
				success: function (data){ 
					console.log(data);
					$("#sbumit").attr('disabled',false);
					if(data.msg == "ok"){
						$("#jbPhoneTip").html("<font color='#339933' style='padding-left:40px'>√ 短信验证码已发到您的手机,请查收</font>");
					}
				}
			});
		}
	}else{
		$('#jbPhoneTip').html("<font color=red style='padding-left:40px'>× 手机号码不能为空</font>");
		$('#sbumit').attr('disabled','disabled');
	}
}

//timer处理函数
function SetRemainTime() {
	if (curCount == 0) {                
		window.clearInterval(InterValObj);// 停止计时器
		$("#btnSendCode").removeAttr("disabled");// 启用按钮
		$("#btnSendCode").val("重新发送验证码");
	}else {
		curCount--;
		$("#btnSendCode").val("请" + curCount + "秒后重新发送");
	}
}

// code  时间过期
function setCodeTimeOut() {
	code = ""; // 清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
}
function submit_tel(){
		var checkcode = $("#checkCode").val();
		var jbPhone = $("#telNum").val();
		if(check_mobile(jbPhone)){
			if(checkcode!=""&&code!=""){
				if(code==checkcode){
					 $("#jbPhoneTip").html("<font color='#339933' style='padding-left:40px'>√ 短信验证码正确，请继续</font>");
					 $.ajax({
						url:"${basepath}/accountSecurity/doChangeTel",
						type:"post",
						dataType:"json",
						data:{'userPhone':jbPhone},
						success:function(data){
							alert(data.ok);
							 location.href="${basepath}/accountSecurity/my_binding";  跳转到账户安全初始化
						}
					});  
					 
				 }else{
				 	$("#jbPhoneTip").html("<font color='red' style='padding-left:40px'>× 验证码错误</font>");
				 }
			}else{
				$("#jbPhoneTip").html("<font color='red' style='padding-left:40px'>× 请输入验证码</font>");
			}
		}
}
</script>
</@htmlBase.htmlBase>
</body>
</html>