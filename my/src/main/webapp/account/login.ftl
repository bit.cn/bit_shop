<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="anitoys">
		<meta name="keywords" content="anitoys">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>登录</title>
		<link rel="shortcut icon" type="image/x-icon" href="${systemSetting().www}">
		<link href="${systemSetting().staticSource}/static/frontend/v1/css/login/login.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div style="width: 1200px; margin: 0 auto;">
			<a href="${systemSetting().www}">
				<img src="${systemSetting().staticSource}/static/frontend/v1/images/jshop/log_top.png" id="logo">
			</a>
		</div>
		<div class="bg">
			<img src="${systemSetting().staticSource}/static/frontend/v1/images/jshop/log_deco.png" class="b_img">

			<div class="site_register">
				<strong id="anitoys">anitoys会员</strong> 
				<a href="${basepath}/account/register" id="register">新用户注册</a>
				<caption>
					<#if errorMsg??>
					<div class="bs-callout bs-callout-danger author" style="text-align: left;font-size: 16px;margin-left:50px;margin-top:-20px; margin-bottom:10px;">
						<strong>登陆失败!</strong> ${errorMsg}
					</div>
					</#if>
				</caption>
				<form method="post" id="form" role="form" action="${basepath}/account/doLogin" theme="simple">
					<input type="text" name="account" id="account" data-rule="账号:required;account;" placeholder="  用户名/手机号码">
					<br>
					<input type="password" name="password" id="password" data-rule="密码:required;password;" placeholder="  密码">
					<br>

					<div style="vertical-align: middle; height: 20px">
						<div style="vertical-align: middle; height:30px">
							<input id="logincheck" type="checkbox" value="7" class="sex">
						</div>
						<p class="p_style_new" height="100%" style="vertical-align: middle">
							<span class="f_size" onclick="getcheck()">一周内免登录 |</span>
							<a href="http:${basepath}/account/forgetByPhone" class="forget_password">忘记密码？ </a>
						</p>
					</div>
					<br>
					<input type="submit" class="sbtn" value="登  录">
				</form>
			</div>
		</div>

		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery-1.11.1.min.js">
			< script type = "text/javascript" >
				function _login3213123() {
					WB2.login(function() {
						console.log("登陆成功");
					});
				}
			//一周内免登
			function getcheck() {
				if ($("#logincheck").prop("checked")) {
					$("#logincheck").prop("checked", false);
				} else {
					$("#logincheck").prop("checked", true);
				}
			}
		</script>
	</body>
</html>

<script type="text/javascript">
function _login3213123(){
	WB2.login(function(){
		console.log("登陆成功");
	});
}

//一周内免登
function getcheck(){
	if($("#logincheck").prop("checked")){
		$("#logincheck").prop("checked", false);
		}else{
		$("#logincheck").prop("checked",true);
		}
}
</script>