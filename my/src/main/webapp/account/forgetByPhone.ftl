<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="${systemSetting().staticSource}/static/frontend/v1/css/common/bootstrap.min.css">
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
	<title>通过手机找回密码</title>
	</head>
	<body>
	<!--商城主页top页-->
	<@myTop.shopTop/>
	<div class="omt_head clearfix">
		<div class="row">
	     <@myLeft.myLeft/>
	     <div class="head_rig" style="width: 950px;">
		     <p class="head_my">
				订单管理中心
				<br/>
			</p>
			<div class="col-xs-12">
				<div class="row" style=" padding:20px 0;">
					<div class="col-xs-12" style="font-size: 14px;font-weight: normal;">
						<span class="label label-success" style="font-size:100%;">
							1.填写手机号
						</span>
						&nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span>
						<span class="label label-default" style="font-size:100%;">
							2.获取验证码
						</span>
						&nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span>
						<span class="label label-default" style="font-size:100%;">
							3.设置新密码
						</span>
						&nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span>
						<span class="label label-default" style="font-size:100%;">
							4.完成
						</span>
					</div>
				 </div>		
				<hr>
				
				<div class="panel panel-success">
					<div class="panel-heading" style="text-align: left;">
		                <h3 class="panel-title">
		                	<span class="glyphicon glyphicon-user"></span>&nbsp;手机找回密码
		                </h3>
		              </div>
		              <div class="panel-body">
		              	<form role="form" name="form" method="post" id="form" class="form-horizontal" action="${basepath}/account/doForgetFromPhone.html" theme="simple">
						  <div class="form-group">
						    <label style=" float:left; width:200px; text-align:right" for="account" class="col-lg-2 control-label">手机号</label>
						    <div class="col-lg-4" style=" float:left">
							    <input  name="phone" type="text" class="form-control"  style="width:260px;" placeholder="请输入绑定的手机号"
							    data-rule="手机:required;length[10~15];mobile;"/>
						     </div>
						  </div>
											 			  
						  <div class="form-group" style=" padding-left:215px;">
						    <div class="">
						      <button type="submit" class="btn btn-success btn-sm" value="提交信息"/>
						      	<span class="glyphicon glyphicon-ok"></span>&nbsp;提交信息
						      </button>
						    </div>
						  </div>
						</form>
		              </div>
				</div>
				</hr>
			</div>
		</div>
	</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/product.js"></script>
<script type="text/javascript">
function reloadImg2() {
	document.getElementById("codes2").src = "${systemSetting().www}/ValidateImage.do?random="
			+ Math.random();
	$("#vcode2").focus();
}
</script>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>