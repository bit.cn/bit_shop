<#import "/shopindex/common/ftl/shopBasePage.ftl" as shopBasePage>
<#import "/shopindex/common/ftl/shopTop.ftl" as shopTop>
<#import "/account/myTop.ftl" as myTop>
<#import "/shopindex/common/ftl/shopFooters.ftl" as shopFooters>
<#import "/account/myLeft.ftl" as myLeft>

<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as shopFooters>
<@shopBasePage.shopBasePage/>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--link href="${systemSetting().staticSource}/shopindex/common/css/style.css" rel="stylesheet" type="text/css"-->
	<!--link href="${systemSetting().staticSource}/shopindex/layout/css/omt.css" rel="stylesheet" type="text/css"-->
	<!--script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery-1.8.2.min.js"></script-->
	<title>安全提醒</title>
	
	<link rel="stylesheet" href="${systemSetting().staticSource}/css/common/common.css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/css/header/header.css">
	<link rel="stylesheet" href="${systemSetting().staticSource}/css/footer/footer.css">
	<link href="${systemSetting().staticSource}/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="http://112.124.52.129:9990/jshop">
	<link href="${systemSetting().staticSource}/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/css/omt.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
 </head>
  
<body>
<!--商城主页top页-->
<!--@shopTop.shopTop/-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig">
		<p class="head_my">
			账号安全
			<br />
			<img src="${systemSetting().staticSource}/shopindex/layout/img/omt/tr.png" />
		</p>
		<div>
			<label class="rem_til">
				安全提醒
			</label>
			<br/>
			<label class="rem_text">
				当账户进行重要操作时接受提醒消息，保障账号安全！
			</label>
			<ul class="rem_update">
				<li class="rem_update_text">
					修改密码
				</li>
				<li class="rem_update_text">
					<input id="pwd_check" type="radio" name="pwd_mail" value="1"/>
					邮件提醒
					<div class="rem_rd">
						<input id="pwd_nocheck" type="radio" name="pwd_mail" value="0"/>
						邮件不提醒
					</div>
				</li>
			</ul>
			<ul class="rem_update">
				<li class="rem_update_text">
					更改手机
				</li>
				<li class="rem_update_text">
					<input id="tel_check" type="radio" name="tel_mail" value="1"/>
					邮件提醒
					<div class="rem_rd">
						<input id="tel_nocheck" type="radio" name="tel_mail" value="0"/>
						邮件不提醒
					</div>
				</li>
				
			</ul>
		</div>
		<div style="margin-left:40px;margin-top:10px;">
			<input type="button" onclick="submit()" class="up_but" value="确认修改"/>
		</div>
	</div>
</div>
<!-- 商城主页footer页 -->
<@shopFooters.shopFooter/>
</body>
<script>
$(function(){
	<#if isPwdSend??&&isPwdSend!="0">
	   $('#pwd_check').attr("checked","checked");
	<#else>
		$('#pwd_nocheck').attr("checked","checked");
	</#if>
	<#if isTelSend??&&isTelSend!="0">
	  $('#tel_check').attr("checked","checked");
   <#else>
		$('#tel_nocheck').attr("checked","checked");
	</#if>
});
function submit() {  
    // 向后台发送处理数据  
   	var pwd_val = $('input:radio[name="pwd_mail"]:checked').val();
    var tel_val = $('input:radio[name="tel_mail"]:checked').val();
    if(pwd_val==null){
    	return;
    }
    if(tel_val==null){
    	return;
    }
    $.ajax({
    	url:'${basepath}/accountSecurity/doChangeRemind',
    	dateType:'json',
    	type:'POST',
    	data:{'isUpPwdSend':pwd_val,'isUpTelSend':tel_val},
    	success:function(data){
    		console.log(data);
    		alert("修改成功");
    		 location.href="${basepath}/accountSecurity/my_binding";  跳转到账户安全初始化
    	}
    	
    })
}  
</script>
