<#import "/resource/common_html_front.ftl" as html>
<#import "/indexMenu.ftl" as menu>
<#import "/account/accountMenu.ftl" as accountMenu>
<#import "/ftl/footer.ftl" as footer>
<#import "/ftl/top2.ftl" as top2>
<!DOCTYPE html>
<html>
<head>
<@html.htmlBase>
    <link rel="stylesheet" href="${systemSetting().staticSource}/css/common/common.css">
    <link rel="stylesheet" href="${systemSetting().staticSource}/css/header/header.css">
    <link rel="stylesheet" href="${systemSetting().staticSource}/css/footer/footer.css">
    <link href="${systemSetting().staticSource}/css/common/lanrenzhijia.css"rel="stylesheet"type="text/css">
    <link href="${systemSetting().staticSource}/css/style.css"rel="stylesheet"type="text/css">
    <link href="${systemSetting().staticSource}/css/omt.css"rel="stylesheet"type="text/css">
<title>我的收货地址</title>
</head>
<body>
<!--商城主页top页-->
	<@top2.top2/>
<div class="omt_head">
<@myLeft.myLeft/>
	<div class="head_rig">
		<p class="head_my">
			我的收货地址
			<br />
		</p>
                <div  style="font-size: 14px;font-weight: normal; height:500px">
                    <div  style ="height:60px;border:1px solid #ddd;border-color:#ddd;margin-bottom:0;overflow:hidden ">
                        <div style="font-size: 16px;font-weight: normal;vertical-align:middle;text-align: center;margin-top:15px;">
                             	恭喜您：收货地址修改成功！
                        </div>
                    </div>
                    <hr>
                </div>
		
	</div>
</div>
<!-- 商城主页footer页 -->
<@footer.footer/>
</@html.htmlBase>
</body>
</html>