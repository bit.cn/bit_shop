<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
	<title>头像设置</title>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig" >
		<p class="head_my">
			头像设置
			<br />
		</p>
		<div class="bd_di" style="height:650px ;  ">
			<form role="form" id="form1" class="form-horizontal" method="post" action="uploadHeadImage" theme="simple" enctype="multipart/form-data">
			 
			
			
			  <div class="example">
		        <img src="${systemSetting().imageRootPath}/${e.head}" id="target" alt="[Jcrop Example]" >
		
		        <div id="preview-pane">
		          <div class="preview-container">
		            <img src="${systemSetting().imageRootPath}/${e.head}" class="jcrop-preview" alt="Preview"   height="140px"  width="140px">
		          </div>
		        </div>
		      </div>
			<br>
			<ul class="bd_ul" style=" margin-top:30px ">
						<input type="hidden" id="head" name="target" value="${systemSetting().staticSource}${e.head}"/>
				   		<input type="hidden" id="x" name="x"/>
		                <input type="hidden" id="y" name="y"/>
		                <input type="hidden" id="w" name="w"/>
		                <input type="hidden" id="h" name="h"/>
				<input  id ="bt" type="button"  class="up_but"  style=" height:30px;width:140px;margin-left:75px ;"  value="保存" onclick="submit_img()" />
				
			</ul>
			</from>
			<div >
		</div>	
		
	</div>
</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery.Jcrop.js"></script>
<style type="text/css">
/* Apply these styles only when #preview-pane has
   been placed within the Jcrop widget */
.jcrop-holder #preview-pane {
  display: block;
  position: absolute;
  z-index: 2000;
  top: 10px;
  right: -280px;
  padding: 6px;
  border: 1px rgba(0,0,0,.4) solid;
  background-color: white;

  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;

  -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
}

/* The Javascript code will set the aspect ratio of the crop
   area based on the size of the thumbnail preview,
   specified here */
#preview-pane .preview-container {
  width: 250px;
  height: 170px;
  overflow: hidden;
}

</style>
<script type="text/javascript">
function submit_img(){
		
		 
			document.getElementById("bt").disabled=true;
			form1.submit();
	 
		
	}

  jQuery(function($){

    // Create variables (in this scope) to hold the API and image size
    var jcrop_api,
        boundx,
        boundy,

        // Grab some information about the preview pane
        $preview = $('#preview-pane'),
        $pcnt = $('#preview-pane .preview-container'),
        $pimg = $('#preview-pane .preview-container img'),

        xsize = $pcnt.width(),
        ysize = $pcnt.height();
    
    
    $('#target').Jcrop({
      onChange: updatePreview,
      onSelect: updatePreview,
      setSelect: [ 20, 20, 140, 140 ],
      aspectRatio: xsize / ysize
    },function(){
      // Use the API to get the real image size
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];
      // Store the API in the jcrop_api variable
      jcrop_api = this;

      // Move the preview into the jcrop container for css positioning
      $preview.appendTo(jcrop_api.ui.holder);
    });

    function updatePreview(c)
    {
      if (parseInt(c.w) > 0)
      {
        var rx = xsize / c.w;
        var ry = ysize / c.h;
				$("#x").val(c.x);
                $("#y").val(c.y);
                $("#w").val(c.w);
                $("#h").val(c.h);
        $pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * c.x) + 'px',
          marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
      }
    };

  });
</script>
</@htmlBase.htmlBase>
</body>
</html>
