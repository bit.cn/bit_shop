<#macro myTop checkLogin=true>
<div class="omt_top" style="position: relative;">
	<div class="omt_top_use">
			<p class="ont_logo">
			</p>
			 <ul>
			 <a href="${systemSetting().www}">
			 	<li> <img src="${systemSetting().staticSource}/shopindex/layout/img/omt/logo.png"/> </li></a>
			 	<li class="int_left" style="float:right">
			 		<form id="searchForm" method="post" action="${systemSetting().search}/search.html" target="_blank">
						<input type="text" name="key" class="inputtext" value="${key!""}" id="mq" value="ANI大搜查" maxlength="24" onfocus="if(this.value=='ANI大搜查') {this.value='';}this.style.color='#333';" onblur="if(this.value=='') {this.value='ANI大搜查';this.style.color='#999999';}" />
						<button type="submit" onclick="search();" class="subt-btn">搜整站</button>
					</form>
				</li>
			 </ul>
			 <ul class="inf">
<!-- 			 	<li> -->
<!-- 					<a href="${systemSetting().doc}/news/list"> -->
<!-- 						<img src="${systemSetting().staticSource}/shopindex/layout/img/omt/pom.png"  /> -->
<!-- 						最新情报 -->
<!-- 					</a> -->
<!-- 				</li> -->
<!-- 				<li class="inf_new"> -->
<!-- 					2015/10/25 -->
<!-- 					<img src="${systemSetting().staticSource}/shopindex/layout/img/omt/vp.png"  /> -->
<!-- 					<a href="#"> -->
<!-- 						客服于2月18日-24日暂停服务 -->
<!-- 					</a> -->
<!-- 				</li> -->
				<li style="float:right">
					 <div class="menuc_right">
					<!--购物车-->
					<div class="car">
						<div class="carDesc">
							<span class="carlogo"><i>${productList?size}</i></span>
							<span class="carm"><a href="${systemSetting().card}/cart/cart.html">我的购物车</a></span>
						</div>
						<div class="carDetail">
            			<#if productList?? && productList?size gt 0>
          					<#list productList as item>
							<div class="carItem">
								<div class="img"><img src="${systemSetting().imageRootPath}/${item.picture}" /></div>

								<div class="itemDetail">
									<a href="${systemSetting().www}/cart/cart.html">${item.name!""}</a>
									<span>有特典</span>
									<span>¥ ${item.nowPrice}</span>
									<a href="#" onclick="deleteFromCart('${item.id!""}')">删除</a>
								</div>
							</div>
				  			</#list>
            			</#if>
							<div class="total">
								<span>共件${productList?size}商品 <b>共计¥${cartInfo.amount!""}</b></span>
								<a href="${systemSetting().www}/cart/cart.html">查看购物车</a>
							</div>
						</div>
					</div>
				      </div>
				</li>
			 </ul>
	</div>
</div>
  <script type="javascript">
//搜索商品
function search(){
	var _key = $.trim($("#mq").val());
	if(_key==''){
		return false;
	}
	$("#searchForm").submit();
}
</script>
</#macro>
