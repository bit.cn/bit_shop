<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
<title>头像设置</title>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head">
<@myLeft.myLeft/>
	<div class="head_rig" style="width: 950px;">
		<p class="head_my">
			我的地址
			<br />
		     </p>
                <div  style="font-size: 14px;font-weight: normal; height:500px">
                    <div  style ="height:60px;border:1px solid #ddd;border-color:#ddd;margin-bottom:0;overflow:hidden ">
                        <div style="font-size: 16px;font-weight: normal;vertical-align:middle;text-align: center;margin-top:15px;">
                             	恭喜您：收货地址新增成功！
                        </div>
                    </div>
                    <hr>
                </div>
		
	</div>
</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>