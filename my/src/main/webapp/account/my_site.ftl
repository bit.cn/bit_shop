<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<@htmlBase.htmlBase>
<head>
<title>我的收货地址</title>
<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix" style="margin-top:15px;">
<@myLeft.myLeft/>
	<div class="head_rig" style="width: 950px;">
		<p class="head_my">
			我的收货地址
			<br />
		</p>
			<div class="head_add">
				<p>
					<label id="labelIN" class="site_add_text">新增收货地址</label> 
					
					<label class="site_add_text1">电话号码、手机号选填一项，其余均为必填项</label>
				</p>
 				<form role="form" id="form1" class="form-horizontal" method="post" action="saveMysite" theme="simple">
				<input type="text" id="id" name="id"   style="visibility: none;display: none;" 	/>
				<ul class="add_site">
					<li>
						所在地区 <label class="site_add_text">*</label> 
						 <select name="province" id="province" class="form-control" onchange="changeProvince()">
						<option value="">--选择省份--</option>
							<#list provinces as item>
								<option value="${item.code}"  >${item.name}</option>
						</#list>
						</select>	
						<select class="form-control" id="citySelect" name="city"  onchange="changeCity()">
								<option value="">--选择城市--</option>
									<#list cities as item>
										<option value="${item.code}"  >${item.name}</option>
									</#list>
								</select>
						  <select class="form-control" id="areaSelect" name="area">
		                                <option value="">--选择区县--</option>
										<#list areas as item>
											<option value="${item.code}"  >${item.name}</option>
										</#list>
		                  </select>
		                  <span id="areaSelectV" style="visibility: none;display: none;color:red">&nbsp;请正确选择所在地区</span>		
					</li>
					<li>
						<div class="add_site_d">
							详细地址<label class="site_add_text"> *</label> 
						</div>
						<textarea id="address" maxlength="100" name="address" rows="5" cols="50" class="ta"   placeholder="建议你如实填写详细的收货地址，例如街道名称，门牌号，楼层和房间号等信息"  ></textarea>
						
						
						<span id="addressV" style="visibility: none;display: none;color:red">&nbsp;请正确填写你详细地址</span>		
					</li>
					<li>
						邮政编码<label class="site_add_text"> *</label>  
						<input type="text" id="zip" name="zip"   style="width:360px;height:20px"  maxlength="7"   placeholder="如果你不清楚邮递区号，请填写000000"/>
						<span id="zipV" style="visibility: none;display: none;color:red">&nbsp;请正确填写邮政编码</span>		
					</li>
					<li class="site_name">
						收货人姓名
						<label class="site_add_text">
							*
						</label> 
						<input id="name" name="name" type="text"  style="width:360px;height:20px"   placeholder="长度不超过25个字符"   maxlength="25" />
						<span id="nameV" style="visibility: none;display: none;color:red">&nbsp;请正确填写收货人姓名</span>		
						 
					</li>
					<li>
						手机号码
						<label class="site_add_text">
							*
						</label> 
						<input  id="phone" name="phone"  type="text"   style="width:360px;height:20px"   
						placeholder="电话号码。手机必须填一项" />
						 <span id="phoneV" style="visibility: none;display: none;color:red">&nbsp;电话号码，手机必须填一项</span>		
						 <span id="phoneV2" style="visibility: none;display: none;color:red">&nbsp; 请正确填写手机号码</span>
					</li>
					<li>
						电话号码
						<label class="site_add_text">
							*
						</label> 
						<input id="mobile" name="mobile" type="text"   style="width:360px;height:20px"      placeholder="电话号码，手机必须填一项" />
					 	<span id="mobileV" style="visibility: none;display: none;color:red">&nbsp;电话号码，手机必须填一项</span>		
					</li>
					<li>
						<p style="height:12px;">
						</p>
						<input type="checkbox"  id="isdefault" name="isdefault" class="site_ck"/>
						设置为默认收货地址
						<br />
						<input  id="bt" type="button" class="sit_ok" value="保存" onclick ="valid();" />
					</li>
				</ul>
				</form>
			</div>
			<p class="save_text">
				已保存了${saveCnt}条地址，还能保存${cnt}条地址
			</p>
		</div>
		<div class="site_save" style="margin-top:-20px;">
			
			<table class="save_tb" id ="tab1">
			<#if addressList?? && addressList?size gt 0>
			<#list addressList as address>
				<tr style="height:45px; background-color:#fafafa; border-bottom:1px #E3E3E3 solid;">
					<th>收货人</th>
					<th>所在地</th>
					<th>详细地址</th>
					<th>邮编</th>
					<th>电话</th>
					<th>手机</th>
					<th>操作</th>
				</tr>
				<tr style="height:45px; border-bottom:1px #E3E3E3 solid;" align="center">
					<td>${address.name!""}</td>
					<td>${address.pcadetail!""}</td>
					<td>${address.address!""}</td>
					<td>${address.zip!""}</td>
					<td>${address.mobile!""}</td>
					<td>${address.phone!""}</td>
					<td>
						<label style="margin-left:40px;">
							<a href="${basepath}/account/my_site_up?id=${address.id}" class="site_up" >修改</a>|
							<label  onclick=del(${address.id}) class="site_up">删除</label>
						</label>
					<#if address.isdefault ?? &&address.isdefault!="y" >
						<input type="button" value="设为默认" class="site_def" onclick="updefault(${address.id})"/>
						<#else><input type="button" value="默认地址" class="site_def" disabled  style="background-color:#21C32B;"/> 
					</#if>
					</td>
				</tr>
			</#list>
			<#else>
			<span style="margin-left:20px;font-size:16px;color:red;">你还没有添加地址，请及时添加自己的地址信息！</span>
			</#if>
			</table>
		</div>
	</div>
</body>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript" >
function changeProvince(){
	var selectVal = $("#province").val();
	if(!selectVal){
		//console.log("return;");
		return;
	}
	var _url = "selectCitysByProvinceCode?provinceCode="+selectVal;
	
	//console.log("_url="+_url);
	$("#citySelect").empty().show().append("<option value=''>--选择城市--</option>");
	$("#areaSelect").empty().show().append("<option value=''>--选择区县--</option>");
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  dataType: "json",
	  success: function(data){
		  ////console.log("changeProvince success!data = "+data);
		  $.each(data,function(index,value){
			  ////console.log("index="+index+",value="+value.code+","+value.name);
			  $("#citySelect").append("<option value='"+value.code+"'>"+value.name+"</option>");
		  });
	  },
	  error:function(er){
		  //console.log("changeProvince error!er = "+er);
	  }
	});
}


function changeCity(){
	var selectProvinceVal = $("#province").val();
	var selectCityVal = $("#citySelect").val();
	if(!selectProvinceVal || !selectCityVal){
		//console.log("return;");
		return;
	}
	var _url = "selectAreaListByCityCode?provinceCode="+selectProvinceVal+"&cityCode="+selectCityVal;
	//console.log("_url="+_url);
	$("#areaSelect").empty().show().append("<option value=''>--选择区县--</option>");
	$.ajax({
	  type: 'POST',
	  url: _url,
	  data: {},
	  dataType: "json",
	  success: function(data){
		  ////console.log("changeProvince success!data = "+data);
		  $.each(data,function(index,value){
			  ////console.log("index="+index+",value="+value.code+","+value.name);
			  $("#areaSelect").append("<option value='"+value.code+"'>"+value.name+"</option>");
		  });
	  },
	  error:function(er){
		  //console.log("changeCity error!er = "+er);
	  }
	});
}



function valid(){
	 $("#accountV,#trueNameV,#birthdayV,#areaSelectV,#addressDetailV,#txV,#phoneV,#mobileV,#phoneV2,#sexV").hide();
     var province = $("#province").val();
     var citySelect =  $("#citySelect").val();
     var areaSelect = $("#areaSelect").val();
     var zip =$("#zip").val();
     var name =$("#name").val();
     var address =$("#address").val();
     var phone = $("#phone").val();
     var mobile = $("#mobile").val();
     var isSuccess =1;
      if(province=="" ||citySelect==""   ){
	    $("#areaSelectV").show();	//显示
	    isSuccess =0;
     }
     if(address=="" || address==undefined ){
        $("#addressV").show();	//显示
        isSuccess =0;
     }
     var re= /^[1-9][0-9]{5}$/ ;
     if(zip!='000000') {
	     if(zip=="" || !re.test(zip)){
	        $("#zipV").show();	//显示
	        isSuccess =0;
	     }  
     }
     if(name=="" || name==undefined  ){
        $("#nameV").show();	//显示
        isSuccess =0;
     }
     var mobile=mobile.replace(/^\s*|\s*$/g,'');
	 var length=mobile.length;
	 var a=/^(1[3|4|5|8|7])[0-9]{9}$/;
     if(phone!="" &&!a.test(phone)){
        $("#phoneV2").show();	//显示
        $("#phoneV").hide();	// 隐藏
        $("#mobileV").hide();	// 隐藏
        isSuccess =0;
     }
     else if(phone=="" &&mobile =="" ){
        $("#phoneV2").show();	//显示
        $("#phoneV").show();	// 隐藏
        $("#mobileV").hide();	// 隐藏

        isSuccess =0;
     }
     if($("#isdefault").is(':checked')){
     	$("#isdefault").val("y") ;
     }
     
     $.ajax({
	  type: 'POST',
	  url: "${basepath}/account/checkMysite",
	  data: {},
	  dataType: "json",
	  success: function(data){
	  
		if(data.result=="ok"&&isSuccess == 1) {
		  	$("#bt").attr({"disabled":"disabled"})
			form1.submit();
		} else {
			alert("您的保存的收货地址已达上限！");
		}
	  },
	  error:function(er){
		  console.log("changeCity error!er = "+er);
	  }
	});
   }
	

function updefault(id){
     $("#id").val(id);
     form1.action = "udpateDefault";
     form1.submit();
   }
	
function del(id){
        if(window.confirm("确实要删除吗？")){
            $("#id").val(id);
		    form1.action = "delAddress";
		    form1.submit();
        }else{
            return;
        }
    }

</script>
</body>
</@htmlBase.htmlBase>