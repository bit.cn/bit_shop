<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/account/myLeft.ftl" as myLeft>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<title>我的收藏</title>
</head>
<body>
<!--商城主页top页-->
<!--@shopTop.shopTop/-->
<@myTop.shopTop/>
	<div class="omt_head clearfix">
		<@myLeft.myLeft/>
		<div class="head_rig">
			<p class="head_my">
				我收藏的店铺 <br /> 
			</p>
			<div class="favorite_product">
				<div class="favorite_operate_all">
					<div style="cursor:pointer;margin-top:3px;">
					<div style="display:inline;color:#BEBFC3;"><input type=checkbox  id="firstCheckbox"  name="kkHandler" >&nbsp&nbsp全选&nbsp&nbsp&nbsp&nbsp</div>						
						<a href="javascript:deleteBusinessToFavorites()" ><span style="color:#346373;">取消收藏</span></a>
					</div>
				</div>
				<form method="post" theme="simple" action="${basepath}/favorite/deleteFavoriteBusinessses">
					<#if isDeleteSuccess?? && isDeleteSuccess>
						<script type="text/javascript">
							alert("已取消收藏");
						</script>
					</#if>
				<div class="favorite_product_show"
					style="margin-top:15px;width:900px;float:left;display:inline;">

					<#list favBusiShowList as item>
						<div style="display:inline;float:left;height:270px;margin:0px 0px 10px 15px;border:1px solid #D6D6D6;">
							<div class="show_one_picture">
								<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${item.businessId!""}" target="_blank" >
									<img style="height:200px;max-width:200px;" alt="" src="${systemSetting().imageRootPath}/${item.businessPicture!""}" onerror="nofind()"/>
								</a>
						 	</div>
							<div class="show_one_name" style="color:#346373;margin-left:5px;margin-top:5px;height:30px;">
								<input type=checkbox  name="ids" id="ids" value="${item.businessId!""}">&nbsp${item.businessName!""}
							</div>		
							<div class="show_one_operate2" style="cursor:pointer;display:inline;margin-left:145px;" >
								<a  style="border:1px solid #D6D6D6;float:right ;background-color:#FAFAFA;margin-right:5px;margin-top:5px;margin-bottom:5px;border-radius: 0px;" href="${basepath}/favorite/deleteUserFavoriteBusiness?businessId=${item.businessId!""}" >&nbsp取消&nbsp</a>						
							</div>
<!-- 							<div class="show_one_operate3" style="cursor:pointer;margin-top:3px;" >							 -->
<!-- 								<a  style="border:1px solid #D6D6D6; background-color:#FAFAFA; border-radius: 0px;" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp进入店铺&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a>						 -->
<!-- 							</div> -->
						</div>	
					</#list>
				</div>
				</form>
				
			</div>
		</div>
	</div>
<!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>
 <script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/product.js"></script>
<script type="text/javascript">
			$(function() {
				//后台查询页面 全选/全不选 功能	jQuery v1.9
				$("#firstCheckbox").on("click",function(){
					console.log("check="+$(this).prop("checked"));
					if($(this).prop("checked")){
						$("input[type=checkbox]").prop("checked",true);
					}else{
						$("input[type=checkbox]").prop("checked", false);
					}
				});

			});
            
				//批量取消商品收藏
				function deleteBusinessToFavorites() {
					$("form[theme=simple]").attr("action","${basepath}/favorite/deleteFavoriteBusinessses");
					$("form[theme=simple]").submit();
				}
			</script>
</@htmlBase.htmlBase>
</body>
</html>