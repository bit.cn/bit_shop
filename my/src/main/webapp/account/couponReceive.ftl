<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/account/myLeft.ftl" as myLeft>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
		<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
		<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
		<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
		<link href="${systemSetting().staticSource}/static/frontend/v1/css/couponList.css" rel="stylesheet" type="text/css">
		<title>我的优惠券</title>
		<style type="text/css">
			.pageLink {
				border: 1px solid #dddddd;
				padding: 4px 12px;
				text-decoration: none;
			}
			
			.selectPageLink {
				border: 1px solid #0088cc;
				padding: 4px 12px;
				color: #0088cc;
				background-color: #dddddd;
				text-decoration: none;
			}
		</style>
</head>
<body>
<!--商城主页top页-->
<!--@shopTop.shopTop/-->
<@myTop.shopTop/>
	<div class="omt_head clearfix">
		<@myLeft.myLeft/>
		<div class="head_rig" style="width: 950px;">
			<p class="head_my">
			优惠券管理
			</p>
		  <div class="bd_di">
			<a href="${basepath}/coupon/coupons"><span class="wd" style="margin-left: 40px;" onclick="line_left('r_line')">我的优惠券</span></a>
			<a href="${basepath}/coupon/couponReceive" ><span class="lq" style="margin-left: 62px;" onclick="line_middle('r_line')">领取优惠券</span></a>
			<a href="${basepath}/coupon/couponsDue"><span class="dq" style="margin-left: 110px;" onclick="line_right('r_line')">到期优惠券</span></a><br/>
			<img src="${systemSetting().staticSource}/shopindex/layout/image/xx.png" class="xx" id="r_line" style = "margin-left: 150px;">
			<ul class="bd_ul2">
			<#list pager.list as list>
				<li class="bd_til_li2">
					<div class="coupon_bg">
						<div class="coupon_bg_img">
							<br><br>
							<p class="p_w_25">
								<#if list.couponType??&&list.couponType==1> 
									￥${list.couponValue!""}
								<#elseif list.couponType??&&list.couponType==2> 
                                    ${list.couponValue!""}折
								<#elseif list.couponType??&&list.couponType==3> 
									￥${list.couponValue!""}
								<#else>
									￥${list.couponValue!""}
								</#if>
							</p>
							<span class="span_w_18"><#if list.startTime??>${list.startTime}</#if></span>
							<span class="span_w_18_2">--</span>
							<span class="span_w_18_2"><#if list.endTime??>${list.endTime}</#if></span>
						</div>
						<div class="wdyhq">
							<div class="fxq"><label>发行方：</label>${list.businessName!""}</div>
							<a class="jrdp" href="${systemSetting().businessFront}/business/businessSelect">进入店铺</a>
							<div class="type"><label>优惠券种类：</label>
								<#if list.couponType??&&list.couponType==1> 商家抵价券
								<#elseif list.couponType??&&list.couponType==2> 商家折扣券
								<#elseif list.couponType??&&list.couponType==3> 平台折扣券
								<#else> ${list.couponType!""}
								</#if>
							</div>
							<#if list.couponType??&&list.couponType!=3>
							<div><label>使用店铺：</label>anitoys</div>
							</#if>
							<div>
							<a style="margin-left:60px;" class="lqyhq" href="javascript:void(0);" couponSn=${list.couponSn!""} couponType=${list.couponType!""}
                                     endTime=${list.endTime} couponId=${list.couponId!""} onclick="receiveCoupon(this)" >
								立即领取
								</a>
							</div>
						</div>
					</div>
				</li>
			</#list>
			</ul>			
		</div>
		<div>
		    <tr>
				<td colspan="16" style="text-align: center;">
					<#if pager??>
						<#include "/shopindex/common/ftl/pager.ftl"/>
					</#if>
				</td>
			</tr>
		</div>
	</div>
  </div>
 <!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>

<script type="text/javascript">

    function receiveCoupon(obj) {
        var couponSn = $(obj).attr("couponSn");
        var couponType = $(obj).attr("couponType");
        var endTime = $(obj).attr("endTime");
        var couponId = $(obj).attr("couponId");

        var _url = "${basepath}/coupon/receiveCoupon?couponSn="+couponSn+"&couponType="+couponType+
				"&endTime="+endTime+"&couponId="+couponId;
        console.log("_url="+_url);
        $.ajax({
            type: 'POST',
            url: _url,
            data: {},
            dataType: "json",
            success: function(data){
				if(data=="1") {
					window.location.href = "${basepath}/coupon/coupons?status=0"
				} else {
					alert("优惠券不足，领取失败！")
				}
            },
            error:function(er){
                console.log("receiveCoupon error!er = "+er);
            }
        });
    }
</script>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>