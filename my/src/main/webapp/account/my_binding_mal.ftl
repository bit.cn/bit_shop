<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
	<title>绑定邮箱</title>
 </head>
  
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig">
		<p class="head_my">
			账号安全
			<br />
		</p>
		<div class="bd_di">
			<ul class="bd_ul">
				<li class="bd_til_li">
					<span class="bd_til">
						<#if userEmail??&&userEmail!="">
						 	解绑邮箱
						 <#else>
						 	绑定邮箱
						  </#if>
						
					</span>
				</li>
				<li>
					邮箱地址：
						 <#if userEmail??&&userEmail!="">
					 		<span>${userEmail}</span>
						<#else>
						 	<input id="mail_address" type="text" class="bd_tel"/>
					 	</#if>
						<input id="btnSendCode" type="button" onclick="sendMessage()" class="bd_but" value="获取验证码"/>
						<span style="padding-left:50px;" id="mailErMessage" ></span>
				</li>
				<li class="bd_til_li1">
					验证码：&nbsp&nbsp&nbsp
					<input id="checkCode" type="text" class="bd_tel"/>
				</li>
				<li>
					<input id="sbumit"  type="button" onclick="submit_mail()" class="bd_ok" value="提交"/>
				</li>
			</ul>
		</div>	
	</div>
</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script>
	$(function(){
		var mail = "";
		//失去焦点
		<#if userEmail??&&userEmail!="">
			if('${userEmail}'!=""){
				mail = '${userEmail}'+"";
			}
		<#else>
			
		</#if>
		$('#mail_address').blur(function() { 
			mail = $("#mail_address").val();
			check_email(mail);
		});
		//获得焦点
		$('#mail_address').focus(function() { 
			mail = $("#mail_address").val();
			check_email(mail);
		}); 
	});
	
	
	//检测邮箱
	function check_email(email){
		var email=email.replace(/^\s*|\s*$/g,'');
		var length=email.length;
		var a=/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
		if (length==0)//用email为空的时候
		{
			$("#mailErMessage").html("<font color=red>邮箱不能为空!</font>");
			$("#sbumit").attr('disabled','disabled');
			return false;
		}
		else
		{
			if(a.test(email)) 
			{
				$("#mailErMessage").html("<font color=green>邮箱格式正确,请继续!</font>");
				$("#sbumit").attr('disabled','');
				//校验邮箱是否已经被绑定
				 $.ajax({
					  type: 'POST',
					  url:"${basepath}/accountSecurity/checkUnique",
					  data: {"email":email},
					  dataType: "json",
					  success: function(data){
						if(data.result=="error"){
							$("#mailErMessage").html("<font color=green>邮箱已经被占用！</font>");
							return;
						}
					  },
					  error:function(er){
						 alert(data);
						 return;
					  }
					});
				return true;
			}
			else
			{
				$("#mailErMessage").html("<font color=red>邮箱格式不正确！</font>");
				$("#sbumit").attr('disabled','disabled');
			 return false;
			}
		}
	}
	
	function checkEmail(mail){
		$.ajax({
				url:"${basepath}/accountSecurity/changeEmailCheck",
				type:"post",
				dataType:"json",
				data:{'newEmail':mail},
				success:function(result){
					console.log(result);
					if(result.ok=="系统认为此邮箱可用!"){
						$("#mailErMessage").html("<font color='#339933' style='padding-left:20px'>√ "+result.ok+"</font>");
						return true;
					}else{
						$("#mailErMessage").html("<font color=red>"+result.error+"</font>");
					}
				}
			});
		}
	function submit_mail(){
		var mail_address = $("#mail_address").val();
		<#if userEmail??&&userEmail!="">
			if('${userEmail}'!=""){
			mail_address = '${userEmail}'+"";
			}
		<#else>
			
		</#if>
		var checkCode = $("#checkCode").val();
		console.log(checkCode);
		if(checkCode!=""&&code!=""){
			if(code==checkCode){
				
					$.ajax({
						url:"${basepath}/accountSecurity/doChangeEmail",
						type:"post",
						dataType:"json",
						data:{'newEmail':mail_address},
						success:function(data){
							alert(data.ok);
							 location.href="${basepath}/accountSecurity/my_binding";  跳转到账户安全初始化
						}
					});
			  }
		}else{
			$("#mailErMessage").html("<font color='red' style='padding-left:40px'>× 请输入验证码</font>");
		}
}
		
</script>

<script>
var InterValObj; //timer变量，控制时间
var count = 60; //间隔函数，1秒执行
var curCount;//当前剩余秒数
var code = ""; //验证码
var codeLength = 6;//验证码长度

function sendMessage() {
  	var mail_address = $("#mail_address").val();
  	<#if userEmail??&&userEmail!="">
		if('${userEmail}'!=""){
			mail_address = '${userEmail}'+"";
			$('#mailErMessage').html("<font color=green style='padding-left:40px'>邮箱格式正确,请继续!</font>");
		}
	<#else>
			
	</#if>
  	var mailErMessage = $("#mailErMessage").text();
  	curCount = count;
	if (mailErMessage != "") {
		if(mailErMessage == "邮箱格式正确,请继续!"||mailErMessage=="√ 系统认为此邮箱可用!"){
			//校验邮箱是否已经被绑定
				 $.ajax({
					  type: 'POST',
					  url:"${basepath}/accountSecurity/checkUnique",
					  data: {"email":mail_address},
					  dataType: "json",
					  success: function(data){
						if(data.result=="error"){
							$("#mailErMessage").html("<font color=green>邮箱已经被占用！</font>");
							return;
						}
					  },
					  error:function(er){
						 alert(data);
						 return;
					  }
					});
			
			// 产生验证码
			for ( var i = 0; i < codeLength; i++) {
				code += parseInt(Math.random() * 9).toString();
			}
			// 设置button效果，开始计时
			$("#btnSendCode").attr("disabled", "true");
			$("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
			InterValObj = window.setInterval(SetRemainTime, 1000); // 启动计时器，1秒执行一次
			// 向后台发送处理数据
			$.ajax({
				type: "POST", // 用POST方式传输
				//dataType: "json", // 数据格式:JSON
				url: "${basepath}/accountSecurity/getMailCheckNo", // 目标地址
				data: {'newEmail':mail_address,'vcode':code},
				error: function (XMLHttpRequest, textStatus, errorThrown) { 
					$("#mailErMessage").html("<font color='red' style='padding-left:40px'>× 请输入验证码</font>");
				},
				success: function (data){ 
					console.log(data);
					$("#sbumit").attr('disabled',false);
					if(data.msg == "ok"){
						$("#mailErMessage").html("<font color='#339933' style='padding-left:40px'>√ 邮件已发送,请查收</font>");
					}
				}
			});
		}
	}else{
		$('#mailErMessage').html("<font color=red style='padding-left:40px'>× 邮箱不能为空</font>");
		//$('#sbumit').attr('disabled','disabled');
	}
}
//timer处理函数
function SetRemainTime() {
	if (curCount == 0) {                
		window.clearInterval(InterValObj);// 停止计时器
		$("#btnSendCode").removeAttr("disabled");// 启用按钮
		$("#btnSendCode").val("重新发送验证码");
		code = ""; // 清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
	}else {
		curCount--;
		$("#btnSendCode").val("请在" + curCount + "秒内输入验证码");
	}
}
</script>
</@htmlBase.htmlBase>
</body>
</html>