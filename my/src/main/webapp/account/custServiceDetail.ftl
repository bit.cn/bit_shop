<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
    <link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css"rel="stylesheet"type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css"rel="stylesheet"type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css"rel="stylesheet"type="text/css">
	<title>我的订单</title>
<style type="text/css">
.STYLE1 {font-size: 12px;color: #ffffff;}
.STYLE2 {font-size: 12px;}
</style>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
	<div class="omt_head">
		<@myLeft.myLeft/>
		<div class="head_rig">
			<p class="head_my">
				我的售后订单 
				<br /> 
			</p>
				<div class="head_mk">
					<div class="head_mk_pak">
						<table class="head_pak_id">
						  <#if orderList??>
							<input type="hidden" name="orderId" value="${orderInfo.id?if_exists }"/>
							<tr class="aft_tr2">
								<td colspan="7"></td>
							</tr>
							<tr class="head_tb_tr" >
								<td width="340px;"><span class="tr_id"> 订单编号: <a
										target="_blank" href="${systemSetting().orders}/order/${orderInfo.id!""}">
											${orderInfo.orderCode!""} </a>
								</span> <span class="tr_sto"><img
										src="${systemSetting().staticSource}/shopindex/layout/img/omt/sto.png" /> 
										<a target="_blank" href="${systemSetting().business}/business/businessSelect?businessId=${orderInfo.businessId!""}">
										${orderInfo.businessName!""}
								</a> </span></td>
								<td>商品状态</td>
								<td>成交单价</td>
								<td>数量</td>
								<td>下单时间</td>
								<td>支付时间</td>
								<td class="aft_t2">订单状态</td>
							</tr>
							<#list orderList as item>
							<tr class="aft_tr1">
								<td class="aft_t"><#-- 商品图片路径 <a
									href="${systemSetting().item}/product/${item.productId!"
									"}.html" target="_blank" title="${item.productName!""}"> <img
										style="width: 100%;height: 100%;border: 0px;" alt=""
										src="${systemSetting().imageRootPath}${item.picture!"
										"}" onerror="nofind()" />
								</a>--> 
								<input type="hidden" name="productId" value="${item.productId?if_exists }"/> 
								<input type="hidden" name="productName" value="${item.productName?if_exists }"/> 
								<input type="hidden" name="repairCount" value="${item.number?if_exists }"/> 
									<a style="float:left;width:27%;margin-right:2%;margin-top:2%;margin-bottom:2%;text-align:center;" href="${systemSetting().item}/product/${item.productId!""}.html" target="_blank"
											title="${item.productName!""}">
								<img class="lazy" style="border:1px solid #E8E8E8;max-height:100px;max-width:80px;" data-reactid=".0.3.2.$0.0.0.1.1.1:$0.0.0.0.0"
										border="0" src="${systemSetting().imageRootPath}${item.picture!""}" width="80" height="100">
							</a>
							<div class="aft_d">
								<a style="cursor: pointer;margin: auto;" href="${systemSetting().item}/product/${item.productId!""}.html" target="_blank" title="${item.productName!""}" width="250">
									${item.productName!""}
								</a>
							</div>
							<div class="aft_d1">
								${item.specInfo!""}
							</div>
								</td>
								<td class="aft_t1"><#if item.productStatus?? &&
									item.productStatus=="1"> 即将上线 <#elseif item.productStatus?? &&
									item.productStatus=="2"> 售卖中 <#elseif item.productStatus?? &&
									item.productStatus=="2"> 已下架 </#if></td>
								<td class="aft_t1">
									<#if item.price??>
										${item.price?string.currency}
									<#else>
									 	￥0.00
									</#if>
								</td>
								<td class="aft_t2">x${item.number!""}</td> 
								<#if item_index ==0>
								<td class="aft_t2" rowspan="${orderList?size}">
									<#if orderInfo.addTime??>
										${orderInfo.addTime?string('yyyy-MM-dd')}</br>
										${orderInfo.addTime?string('HH:mm:ss')}
									<#else>
									 	&nbsp;
									</#if>
								</td>
								<td class="aft_t2" rowspan="${orderList?size}">
									<#if orderInfo.finishPayTime??>
										${orderInfo.finishPayTime?string('yyyy-MM-dd')}</br>
										${orderInfo.finishPayTime?string('HH:mm:ss')}
									<#else>
									 	&nbsp;
									</#if>
								</td>
							<td class="aft_t2" rowspan="${orderList?size}"><#if orderInfo.status?? && orderInfo.status =="init">
								待付款
							<#elseif orderInfo.status?? && orderInfo.status =="pass">
								待补款
							<#elseif orderInfo.status?? && orderInfo.status =="stay">
								待发货
							<#elseif orderInfo.status?? && orderInfo.status =="send">
								待收货<br/>
							<#elseif orderInfo.status?? && orderInfo.status =="sign">
								已签收<br/>
							<#elseif orderInfo.status?? && orderInfo.status =="finish">
								交易完成<br/>
							 <#elseif orderInfo.status?? && orderInfo.status =="cancel">
								已失效<br/>
							</#if></td>  
							</#if>
							</tr>
							</#list>
							
							<#--
							<tr class="aft_tr">
								<td colspan="7" align="right"><label class="aft_lb">
										下单金额:${item.amount!""} </label> <label class="aft_lb">
										已支付:${item.amount!""} </label> <label class="aft_lb">
										待支付：${item.amount!""} </label> 应支付订单金额: <label class="aft_sum">
										￥${item.amount!""} </label></td>
							</tr>
							-->
							
							</#if>
						</table>
					</div>
				</div>
				<div class="head_mk">
					<ul class="custservice_chage">
						<li class="myomtservice_chage">
							<table width="100%" class="head_pak_id">
								<tr>
									<td style="line-height:2em;"><span> 申请服务： 
										<#if e.repairType?? && e.repairType ==1>
											换货
										<#elseif e.repairType?? && e.repairType ==2>
											退款
										</#if>
									</span></td>
								</tr>
								<tr>
									<td><span>
										<div style="margin-top:3px;margin-bottom:3px;display:inline-block;white-spacing:nowrap;border: 0px solid;overflow: hidden;">
											<div style="width: 300px;float:left;overflow-x: hidden; overflow-y: hidden;position:relative;">
												<img alt="" src="${systemSetting().staticSource}/business/image/serven.png" style="width: 300px;">
												<div style="position:absolute; left:0; top:0;text-align: center;width: 100%;">
												<span class="STYLE1">
												<#if e.repairType?? && e.repairType ==1>
													1、买家申请换货
												<#elseif e.repairType?? && e.repairType ==2>
													1、买家申请退款
												</#if>
												</span></div>
											</div>
											<div style="width: 300px;float:left;overflow-x: hidden; overflow-y: hidden;position:relative;">
												<#if e.status?? && (e.status ==2 || e.status ==3)>
													<img alt="" src="${systemSetting().staticSource}/business/image/six_b.png" style="width: 300px;">
												<#else>
													<img alt="" src="${systemSetting().staticSource}/business/image/six.png" style="width: 300px;">
												</#if>
												<div style="position:absolute; left:0; top:0;text-align: center;width: 100%;">
												
												<#if e.repairType?? && e.repairType ==1>
													<#if e.status?? && (e.status ==2 || e.status ==3)>
														<span class="STYLE1">2、卖家处理换货申请</span>
													<#else>
														<span class="STYLE2">2、卖家处理换货申请</span>
													</#if>
												<#elseif e.repairType?? && e.repairType ==2>
													<#if e.status?? && (e.status ==2 || e.status ==3)>
														<span class="STYLE1">2、卖家处理退款申请</span>
													<#else>
														<span class="STYLE2">2、卖家处理退款申请</span>
													</#if>
													
												</#if>
												
												</div>
											</div>
											<div style="width: 300px;float:left;overflow-x: hidden; overflow-y: hidden;position:relative;">
												<#if e.status?? && (e.status ==3)>
													<img alt="" src="${systemSetting().staticSource}/business/image/five_b.png" style="width: 290px;">
												<#else>
													<img alt="" src="${systemSetting().staticSource}/business/image/five.png" style="width: 290px;">
												</#if>
												<div style="position:absolute; left:0; top:0;text-align: center;width: 100%;">
												<#if e.repairType?? && e.repairType ==1>
													<#if e.status?? && (e.status ==3)>
														<span class="STYLE1">3、换货完成</span>
													<#else>
														<span class="STYLE2">3、换货完成</span>
													</#if>
												<#elseif e.repairType?? && e.repairType ==2>
													<#if e.status?? && (e.status ==3)>
														<span class="STYLE1">3、退款完成</span>
													<#else>
														<span class="STYLE2">3、退款完成</span>
													</#if>
													
												</#if>
												</span>
											</div>
										</div>
											
									</span></td>
								</tr>
								<tr>
									<td style="line-height:2em;"><span> 说明：</span>
										 	<textarea class="form-control" name="repairDescription"
												rows="5" cols="100" disabled="disabled" readonly="readonly">${e.repairDescription!"" }</textarea>
									</td>
								</tr>
								<tr>
									<td style="line-height:2em;">
										<#if e.repairImg??>
										<span> 凭证：</span>
										<a href="${systemSetting().imageRootPath}/${e.repairImg!""}"  target="_blank">
										<img class="lazy" style="border:1px solid #E8E8E8;max-height:100px;max-width:80px;" data-reactid=".0.3.2.$0.0.0.1.1.1:$0.0.0.0.0"
										border="0" src="${systemSetting().imageRootPath}/${e.repairImg!""}" width="80" height="100">
										</a>
										</#if>
									</td>
								</tr>
								<tr>
									<td style="line-height:2em;">
										<span> 用户信息：</span>
											${e.userInfo!"" }
									</td>
								</tr>
							</table>
						</li>
					</ul>
				</div>
		</div>
	</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>
