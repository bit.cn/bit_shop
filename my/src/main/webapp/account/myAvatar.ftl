<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
	<title>头像设置</title>
		<style type="text/css">
			/* Apply these styles only when #preview-pane has
   been placed within the Jcrop widget */
			
			.jcrop-holder #preview-pane {
				display: block;
				position: absolute;
				z-index: 2000;
				top: 10px;
				right: -280px;
				padding: 6px;
				border: 1px rgba(0, 0, 0, .4) solid;
				background-color: white;
				-webkit-border-radius: 6px;
				-moz-border-radius: 6px;
				border-radius: 6px;
				-webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
				-moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
				box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
			}
			/* The Javascript code will set the aspect ratio of the crop
   area based on the size of the thumbnail preview,
   specified here */
			
			#preview-pane .preview-container {
				width: 140px;
				height: 140px;
				overflow: hidden;
			}
		</style>
</head>
<body>
<!--商城主页top页-->
<!--@shopTop.shopTop/-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig" >
		<p class="head_my">
			头像设置
			<br />
		</p>
		<div class="bd_di" style="height:650px ;  ">
			<form role="form" id="form1" class="form-horizontal" method="post" action="saveAvatar" theme="simple" enctype="multipart/form-data"  >
			<br>
			<ul class="bd_ul" style=" margin-top:30px ">
				<li class="up_pwd_li" style="margin-left:75px">
					<img src="${systemSetting().imageRootPath}/${(imgSrc)!''}"   height="140px"  width="140px" id="headimg"/>
				<BR>
				</li>
				<br>
				<li class="up_pwd_li" id ="headli" >
					请选择上传的头像:
					<input type="file"  id ="imgFile"   name="imgFile"  onchange="javascirpt:filefujianChange(this)"   /> 
				</li>
				<input type="hidden" id="head" name="head"/>
				<input type="hidden" id="uid" name="uid" value="71"/>
				   		<input type="hidden" id="x" name="x"/>
		                <input type="hidden" id="y" name="y"/>
		                <input type="hidden" id="w" name="w"/>
		                <input type="hidden" id="h" name="h"/>
				<input  id ="bt" type="button"  class="up_but"  style=" height:30px;width:140px;margin-left:75px ;"  value="点击图片裁剪" onclick="submit_img(this)" />
				
			</ul>
			</from>
			<div >
		</div>
	</div>
	</div>
</div>
</body>
<style type="text/css">

/* Apply these styles only when #preview-pane has
   been placed within the Jcrop widget */
.jcrop-holder #preview-pane {
  display: block;
  position: absolute;
  z-index: 2000;
  top: 10px;
  right: -280px;
  padding: 6px;
  border: 1px rgba(0,0,0,.4) solid;
  background-color: white;

  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;

  -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
}

/* The Javascript code will set the aspect ratio of the crop
   area based on the size of the thumbnail preview,
   specified here */
#preview-pane .preview-container {
  width: 140px;
  height: 140px;
  overflow: hidden;
}

</style>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
 <script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<SCRIPT type="text/javascript">
var path ="";
function submit_img(obj){
		
		if(	document.getElementById("imgFile")==null ||	document.getElementById("imgFile").value ==""){
			alert("请选择需要修改的图片！");
		}else{
			 var name=$("#imgFile").val();
	        var fileName = name.substring(name.lastIndexOf(".")+1).toLowerCase();
	        if(fileName !="jpg" && fileName !="jpeg" && fileName !="pdf" && fileName !="png" && fileName !="dwg" && fileName !="gif" ){
	          alert("请选择图片格式文件上传(jpg,png,gif,dwg,pdf,gif等)！");
	          
	            return;
	        }
			
		
		  document.getElementById("head").value =path;
			form1.submit();
		}
		
		
}
function getPath(obj) 
{ 
  if(obj) 
    { 
    if (window.navigator.userAgent.indexOf("MSIE")>=1) 
      { 
        obj.select(); 
        path=document.selection.createRange().text; 
      return document.selection.createRange().text; 
      } 
    else if(window.navigator.userAgent.indexOf("Firefox")>=1) 
      { 
      if(obj.files) 
        { 
        	 path=obj.files.item(0).getAsDataURL(); 
        return obj.files.item(0).getAsDataURL(); 
        } 
        path=obj.value; 
      return obj.value; 
      } 
        path=obj.value; 
    return obj.value; 
    } 
} 
function fileChange(target) {
     var fileSize = 0;         
     if (isIE && !target.files) {     
       var filePath = target.value;     
       var fileSystem = new ActiveXObject("Scripting.FileSystemObject");        
       var file = fileSystem.GetFile (filePath);     
       fileSize = file.Size;    
     } else {    
      fileSize = target.files[0].size;     
      }   
      var size = fileSize / 1024;    
      if(size>2000){  
       alert("附件不能大于2M");
       target.value="";
       return
      }
      var name=target.value;
      var fileName = name.substring(name.lastIndexOf(".")+1).toLowerCase();
      if(fileName !="xls" && fileName !="xlsx"){
          alert("请选择execl格式文件上传！");
          target.value="";
          return
      }
    } 

   function filefujianChange(target) {
        var name=target.value;
        var fileName = name.substring(name.lastIndexOf(".")+1).toLowerCase();
        if(fileName !="jpg" && fileName !="jpeg" && fileName !="pdf" && fileName !="png" && fileName !="dwg" && fileName !="gif" ){
          alert("请选择图片格式文件上传(jpg,png,gif,dwg,pdf,gif等)！");
             $("#imgFile").val("") ;
            return
        }
      }
</SCRIPT>
</@htmlBase.htmlBase>
</body>
</html>