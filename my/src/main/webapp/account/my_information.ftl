<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css" rel="stylesheet" type="text/css">
 	<title>消息通知</title>
 </head>
 <body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head clearfix">
<@myLeft.myLeft/>
	<div class="head_rig" style="width: 950px;">
		<p class="head_my">
			消息通知
			<br />
		</p>
		<#if pager??>
			<#list pager.list as item>
			<div class="info_list">
				<p style="line-height:30px; padding-left:20px;  text-align:left">
					${(item.title)!''}
				</p>
				<p style="line-height:24px; padding-left:20px;  text-align:left">
					${(item.content)!''} 
				</p>
				<p style="line-height:24px; padding-right:20px; text-align:right; color:#666666">
					消息来源${(item.fromId)!""}  消息时间:${(item.addTime)!''}
				</p>
			</div>
			</#list>
			<#include "/shopindex/common/ftl/pager.ftl"/>
		<#else>
			没有消息。。。
		</#if>
		
	</div>
</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
</@htmlBase.htmlBase>
</body>
</html>
