<#import "/resource/common_html_front.ftl" as html>
<#import "/indexMenu.ftl" as menu>
<#import "/account/accountMenu.ftl" as accountMenu>
<#import "/ftl/footer.ftl" as footer>
<#import "/ftl/top2.ftl" as top2>
<!DOCTYPE html>
<html>
<head>
<@html.htmlBase>
    <link rel="stylesheet" href="${systemSetting().staticSource}/css/common/common.css">
    <link rel="stylesheet" href="${systemSetting().staticSource}/css/header/header.css">
    <link rel="stylesheet" href="${systemSetting().staticSource}/css/footer/footer.css">
    <link href="${systemSetting().staticSource}/css/common/lanrenzhijia.css"rel="stylesheet"type="text/css">
    <link href="${systemSetting().staticSource}/css/style.css"rel="stylesheet"type="text/css">
    <link href="${systemSetting().staticSource}/css/omt.css"rel="stylesheet"type="text/css">
<title>个人资料</title>
</head>
<body>
<@menu.menu selectMenu=""/>
<!--商城主页top页-->
<@top2.top2/>
<div class="omt_head">
<@myLeft.myLeft/>
	<div class="head_rig">
		<p class="head_my">
			个人资料
			<br />
			<img src="${systemSetting().staticSource}/shopindex/layout/img/omt/tr.png" style="margin-top:-25px;"/>
		</p>
                <div  style="font-size: 14px;font-weight: normal; height:500px">
                    <div  style ="height:60px;border:1px solid #ddd;border-color:#ddd;margin-bottom:0;overflow:hidden ">
                        <div style="font-size: 16px;font-weight: normal;vertical-align:middle;text-align: center;margin-top:15px;">
                             	警告：个人资料-基本信息-用户名 存在重复请重新修改！
                        </div>
                    </div>
                    <hr>
                </div>
		
	</div>
</div>
<!-- 商城主页footer页 -->
<@footer.footer/>
</@html.htmlBase>
</body>
</html>