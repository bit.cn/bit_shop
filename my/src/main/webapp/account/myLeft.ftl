<#macro myLeft checkLogin=true>
<div class="head_left">
		<p class="head_my">
			<a href="${systemSetting().my}/account/orders?"  target="_self">我的anitoys</a>
		</p>
		<ul class="head_my_ul">
			<li class="ord">
				<img src="${systemSetting().staticSource}/shopindex/layout/img/omt/left_text.png" />
				&nbsp;
				订单中心
			</li>
			<li><a href="${basepath}/account/orders">我的订单</a></li>
			<li><a href="${basepath}/account/orders?status=dfk">待付款</a></li>
			<li><a href="${basepath}/account/orders?status=dbk">待补款</a></li>
			<li><a href="${basepath}/account/orders?status=dsh">等待确认收货</a></li>
			<li><a href="${basepath}/account/orders?status=finish">已完成</a></li>
			<li><a href="${basepath}/account/orders?status=cancel">失效订单</a></li>
			<li class="ord"><img src="${systemSetting().staticSource}/shopindex/layout/img/omt/left_text.png" />&nbsp;售后中心</li>
			<li><a href="${basepath}/account/custService">我的售后订单</a></li>
			<li class="ord"><img src="${systemSetting().staticSource}/shopindex/layout/img/omt/left_text.png" />&nbsp;优惠中心</li>
			<li><a href="${basepath}/coupon/coupons?status=0">我的优惠券</a></li>
			<li class="ord"><img src="${systemSetting().staticSource}/shopindex/layout/img/omt/left_text.png" />&nbsp;收藏中心</li>
			<li><a href="${basepath}/favorite/searchUserFavoritePro">我收藏的商品</a></li>
			<li><a href="${basepath}/favorite/searchUserFavoriteBusi">我收藏的商铺</a></li>
			<li class="ord"><img src="${systemSetting().staticSource}/shopindex/layout/img/omt/left_text.png" />&nbsp;个人中心</li>
			<li><a href="${basepath}/account/myData">个人资料</a></li>
			<li><a href="${basepath}/account/myAvatar">头像设置</a></li>
			<li><a href="${basepath}/accountSecurity/my_binding">账号安全</a></li>
			<li><a href="${basepath}/messageAction/my_information">消息通知</a></li>
			<li><a href="${basepath}/account/my_site">我的地址</a></li>
		</ul>
	</div>
</#macro>
