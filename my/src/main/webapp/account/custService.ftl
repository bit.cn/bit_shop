<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/ftl/top2.ftl" as myTop>
<#import "/ftl/footer.ftl" as footer>
<#import "/account/myLeft.ftl" as myLeft>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
    <link href="${systemSetting().staticSource}/static/frontend/v1/css/common/lanrenzhijia.css"rel="stylesheet"type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/style.css"rel="stylesheet"type="text/css">
	<link href="${systemSetting().staticSource}/static/frontend/v1/css/omt.css"rel="stylesheet"type="text/css">
	<title>我的订单</title>
    <style type="text/css">
        .STYLE1 {font-size: 12px;color: #ffffff;}
        .STYLE2 {font-size: 12px;}
    </style>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="omt_head">
	<@myLeft.myLeft/>
    <div class="head_rig">
        <p class="head_my">
            我的售后订单 <br />
        </p>
        <div class="head_ul">
            <ul class="centcustservice">
                <p class="cent_p"></p>
                <li class="myomt"><span> <a
                        href="${basepath}/account/custService"> 全部 </a>
					</span> <span> <a href="${basepath}/account/custService"> 退货 </a>
					</span> <span> <a href="${basepath}/account/custService"> 换货 </a>
					</span>
                </li>
                <li style="margin-left:400px;"><a href="#"> <img
                        src="${systemSetting().staticSource}/shopindex/layout/img/omt/cuss.png" />
                </a></li>
            </ul>
        </div>
            <form role="form" id="saveCustServiceform" class="form-horizontal" method="post" action="saveCustService" theme="simple" enctype="multipart/form-data"  >
            <div class="head_mk">
                <div class="head_mk_pak">
                    <table class="head_pak_id">
						<#list orderList as item>
                            <input type="hidden" name="orderId" value="${item.id?if_exists }"/>
                            <tr class="aft_tr2">
                                <td colspan="7"></td>
                            </tr>
                            <tr class="head_tb_tr" >
                                <td width="340px;"><span class="tr_id"> 订单编号: <a
                                        target="_blank" href="${systemSetting().orders}/order/${item.id!""}">
								${item.id!""} </a>
								</span> <span class="tr_sto"><img
                                        src="${systemSetting().staticSource}/shopindex/layout/img/omt/sto.png" />
										<a target="_blank" href="${systemSetting().business}/business/businessSelect?businessId=${item.businessId!""}">
										${item.businessName!""}
                                        </a> </span></td>
                                <td>商品状态</td>
                                <td>成交单价</td>
                                <td>数量</td>
                                <td>下单时间</td>
                                <td>支付时间</td>
                                <td class="aft_t2">订单状态</td>
                            </tr>
							<#list item.orders as item>
                                <tr class="aft_tr1">
                                    <td class="aft_t"><#-- 商品图片路径 <a
									href="${systemSetting().item}/product/${item.productID!"
									"}.html" target="_blank" title="${item.productName!""}"> <img
										style="width: 100%;height: 100%;border: 0px;" alt=""
										src="${systemSetting().imageRootPath}${item.picture!"
										"}" onerror="nofind()" />
								</a>-->
                                        <input type="hidden" name="productId" value="${item.productID?if_exists }"/>
                                        <input type="hidden" name="productName" value="${item.productName?if_exists }"/>
                                        <input type="hidden" name="repairCount" value="${item.number?if_exists }"/>
                                        <a style="float:left;width:27%;margin-right:2%;margin-top:2%;margin-bottom:2%;text-align:center;" href="${systemSetting().item}/product/${item.productID!""}.html" target="_blank"
                                           title="${item.productName!""}">
                                            <img class="lazy" style="border:1px solid #E8E8E8;max-height:100px;max-width:80px;" data-reactid=".0.3.2.$0.0.0.1.1.1:$0.0.0.0.0"
                                                 border="0" src="${systemSetting().imageRootPath}${item.picture!""}" width="80" height="100">
                                        </a>
                                        <div class="aft_d">
                                            <a style="cursor: pointer;margin: auto;" href="${systemSetting().item}/product/${item.productID!""}.html" target="_blank" title="${item.productName!""}" width="250">
											${item.productName!""}
                                            </a>
                                        </div>
                                        <div class="aft_d1">
										${item.specInfo!""}
                                        </div>
                                    </td>
                                    <td class="aft_t1"><#if item.productStatus?? &&
									item.productStatus=="1"> 即将上线 <#elseif item.productStatus?? &&
									item.productStatus=="2"> 售卖中 <#elseif item.productStatus?? &&
									item.productStatus=="2"> 已下架 </#if></td>
                                    <td class="aft_t1">
										<#if item.price??>
										${item.price?string.currency}
										<#else>
                                            ￥0.00
										</#if>
                                    </td>
                                    <td class="aft_t2">x${item.number!""}</td>
									<#if item.index?? && item.index==0>
                                        <td class="aft_t3">
											<#if item.addTime??>
												${item.addTime?string('yyyy-MM-dd')}</br>
											${item.addTime?string('HH:mm:ss')}
											<#else>
                                                &nbsp;
											</#if>
                                        </td>
                                        <td class="aft_t3">
											<#if item.finishPayTime??>
												${item.finishPayTime?string('yyyy-MM-dd')}</br>
											${item.finishPayTime?string('HH:mm:ss')}
											<#else>
                                                &nbsp;
											</#if>
                                        </td>
                                        <td class="aft_t3">交易完成</td> <#else>
                                        <td class="aft_t3"></td>
                                        <td class="aft_t3"></td>
                                        <td class="aft_t3"></td>
                                        <td class="aft_t3"></td> </#if>
                                </tr>
							</#list>
						<#--
                        <tr class="aft_tr">
                            <td colspan="7" align="right"><label class="aft_lb">
                                    下单金额:${item.amount!""} </label> <label class="aft_lb">
                                    已支付:${item.amount!""} </label> <label class="aft_lb">
                                    待支付：${item.amount!""} </label> 应支付订单金额: <label class="aft_sum">
                                    ￥${item.amount!""} </label></td>
                        </tr>
                        -->
						</#list>
                    </table>
                </div>
            </div>
            <div class="head_mk">
                <ul class="custservice_chage">
                    <li class="myomtservice_chage">
                        <table width="100%" class="head_pak_id">
                            <tr>
                                <td style="line-height:2em;"><span> 申请服务： <#assign
								map = {"2":'退款',"1":'换货'}/> <select
                                        id="repairType" name="repairType" onchange="change_repairType();"/> <#list map?keys as key>
                                    <option value="${key}"<#if repairType?? &&
									repairType==key?eval>selected="selected" </#if>>${map[key]}</option>
								</#list>
									</span></td>
                            </tr>
                            <tr>
                                <td style="line-height:2em;"><span>
										<div style="margin-top:3px;margin-bottom:3px;display:inline-block;white-spacing:nowrap;border: 0px solid;overflow: hidden;">
                                            <div style="width: 300px;float:left;overflow-x: hidden; overflow-y: hidden;position:relative;">
                                                <img alt="" src="${systemSetting().staticSource}/account/img/servens.png" style="width: 300px;">
                                                <div style="position:absolute; left:0; top:0;text-align: center;width: 100%;">
                                                    <span class="STYLE1" id="sp1">1、买家申请换货</span>
                                                    <span class="STYLE1" id="sp4">1、买家申请退款</span>
                                                </div>
                                            </div>
                                            <div style="width: 300px;float:left;overflow-x: hidden; overflow-y: hidden;position:relative;">
                                                <img alt="" src="${systemSetting().staticSource}/account/img/sixs.png" style="width: 300px;">
                                                <div style="position:absolute; left:0; top:0;text-align: center;width: 100%;">
                                                    <span class="STYLE2" id="sp2">2、卖家处理换货申请</span>
                                                    <span class="STYLE2" id="sp5">2、卖家处理退款申请</span>
                                                </div>
                                            </div>
                                            <div style="width: 300px;float:left;overflow-x: hidden; overflow-y: hidden;position:relative;">
                                                <img alt="" src="${systemSetting().staticSource}/account/img/fives.png" style="width: 290px;">
                                                <div style="position:absolute; left:0; top:0;text-align: center;width: 100%;">
                                                    <span class="STYLE2" id="sp3">3、换货完成</span>
                                                    <span class="STYLE2" id="sp6">3、退款完成</span>
                                                </div>
                                            </div>
                                        </div>
									</span></td>
                            </tr>
                            <tr>
                                <td style="line-height:2em;"><span> <img alt=""
                                                                         src="${systemSetting().staticSource}/shopindex/layout/image/wh.png">温馨提示：您只有一次售后维权的机会哦！
									</span></td>
                            </tr>
                            <tr>
                                <td style="line-height:3em;"><span> 说明：</span><span>
											<textarea class="form-control" name="repairDescription"
                                                      rows="5" cols="100"></textarea>
									</span></td>
                            </tr>
                            <tr>
                                <td style="line-height:2em;"><span> 上传凭证：</span>
                                    <input type="file" name="repairImg" id="repairImg" value=" 选择凭证图片   " accept="image/jpeg,image/gif,image/x-ms-bmp,image/x-png" >
									<input value="" name="repairImg" id="repairImg" style="display: none;"/> <br>
									<span style="font-size: 10px;" title="最多1张，不能超过5M，支持GIF,JPEG,JPG,PNG,BMP格式"> <img
                                            alt="" src="${systemSetting().staticSource}/shopindex/layout/image/wh.png">上传帮助？
									</span></td>
                            </tr>
                            <tr>
                                <td style="line-height:2em;">
                                    <span>申请人：</span>
                                    <input type="text" name="addAcount" value=""/>
                                </td>
                            </tr>
                            <tr>
                                <td style="line-height:2em;">
                                    <span>手机号：</span>
                                    <input type="text" name="addTel" value=""/>
                                </td>
                            </tr>
                            <tr>
                                <td style="line-height:2em;">
                                    <span>联系qq：</span>
                                    <input type="text" name="tencentQQ" value=""/>
                                </td>
                            </tr>
                            <tr>
                                <td style="line-height:2em;">
                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;地址：</span>
                                    <input type="text" name="addAdr" value=""/>
                                </td>
                            </tr>
                            <tr>
                                <td style="line-height:3em;">
									<span>
									<button style="margin-left:60px;" class="tp-common-btn tp-common-btnKind tp-common-btnKind_secondary tp-common-btnSize tp-common-btnSize_normal" onclick="subOrder()">提交订单
                                    </button>
											&nbsp;&nbsp;&nbsp;<a href="${basepath}/account/orders"
                                                                 style="color:ff6820;">取消并返回</a>
									</span></td>
                            </tr>
                        </table>
                    </li>
                </ul>
            </div>
        </form>
    </div>
</div>
<!-- 商城主页footer页 -->
<@footer.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript">
    window.onload = function() {
        change_repairType();  //初始化
    }
    function change_repairType(){
        if(document.getElementById("repairType").value =="1"){
            document.getElementById("sp1").hidden=null;
            document.getElementById("sp2").hidden=null;
            document.getElementById("sp3").hidden=null;
            document.getElementById("sp4").hidden="hidden";
            document.getElementById("sp5").hidden="hidden";
            document.getElementById("sp6").hidden="hidden";
        }else if(document.getElementById("repairType").value =="2"){
            document.getElementById("sp4").hidden=null;
            document.getElementById("sp5").hidden=null;
            document.getElementById("sp6").hidden=null;
            document.getElementById("sp1").hidden="hidden";
            document.getElementById("sp2").hidden="hidden";
            document.getElementById("sp3").hidden="hidden";
        }
    }
    
     function subOrder(){
        var _form = $("#saveCustServiceform");
		_form.submit();
    }
</script>
</@htmlBase.htmlBase>
</body>
</html>