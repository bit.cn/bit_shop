<!--导航栏-->
<#macro shopChannel checkLogin=true>
			<div class="typeNav clearfix">
				<div class="type">
					<ul>
					<#if catalogs??>
					<#list catalogs as item>
						
						<li class="sb">
							<label><a class="sb" href="${systemSetting().lists}/catalog/${item.code}.html">${item.name}</a></label>
							<span>
								<#if item.children??>
        	  					<#list item.children as sItem>
        	  					<#if (sItem_index<4)>
        	  					<a href="${systemSetting().lists}/catalog/${sItem.code}.html">${sItem.name!""}</a>
								<i>/</i>
								</#if>
								</#list>
								</#if>
							</span>
							<div class="typeAll clearfix">
								<div class="types">
									<div class="type_all">
									<#if item.children??>
		        	  					<#list item.children as sItem>
		        	  						<a href="${systemSetting().lists}/catalog/${sItem.code}.html">${sItem.name}</a>
										</#list>
									</#if>
									</div>
									<div class="type_img">
									</div>
								</div>
								<div class="tyesImage">
								</div>
							</div>
						</li> 
						<#if (item_index>9)>
							<li class="sb"> 
								<div><a href="${systemSetting().search}/search.html?key=ANI大搜查" target="_blank" style="font-size:16px;color:red">更多&gt;&gt;</a></div>
							</li>
							<#break>
						</#if>
						</#list>
						</#if>
					</ul>
				</div>
				<div class="scrollImage">
					<a class="prev"></a>
					<a class="next"></a>
					<ul id="scroll" style="width: 4438px; left: -631.822166654002px;">
					
					<#if indexImages??>
					<#list indexImages as item>
					   <li <#if item_index==0>class="active"</#if>>
					          <#if item.link??>
						          <a href="http://${item.link!""}" target="_blank">
						          	<img src="${systemSetting().imageRootPath}/${item.picture!""?replace("/","%5C")}" >
						          </a>
					          <#else>
			                      <img style="max-width: 100%;" src="${systemSetting().imageRootPath}/${item.picture!""}" >
					          </#if>
					    </li>
					</#list>
					</#if>
					</ul>
					<div class="scollIndex">
						<#list indexImages as item>
							<a></a>
						</#list>
					</div>
				</div>
				<div class="notice">
					<div class="notice_title"><i></i>最新情报<span>NEWS</span>
						<a class="more" href="${systemSetting().doc}/platnews/list?type=notice" target="_Blank">更多 &gt;</a>
					</div>
					<ul>
						<#if noticeList??>
							<#list noticeList as item>
							<li><span>${item.createtime!""}</span><a href="${systemSetting().doc}/platnews/${item.id}" title="${item.title!""}" target="_Blank">${item.title!""}</a></li>
							</#list>
						</#if>
					</ul>
				</div>
			</div>
			<input type="hidden" value="${indexImages?size}" id="scrollImages"/>
  </#macro>