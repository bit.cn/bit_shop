<#macro shopTop checkLogin=true>
<div class="topNav">
			<div class="top_main clearfix">
				<div class="top_left">
				<a href="${systemSetting().www}">"话不多说买买买"——欢迎来到anitoys</a>
				 <#if currentAccount()??>
				     <span class="sn-login"">欢迎回来，${currentAccount().account}</span>
				     <a href="${systemSetting().my}/account/exit" class="sn-regist">退出系统</a>
			    <#else >
				 <span>
					 <a href="${systemSetting().my}/account/login">请登陆</a>
					 <a href="${systemSetting().my}/account/register" class="regist">免费注册</a>
				 </span>
				</#if>
				</div>
				<div class="top_right">
					<ul>
					<#if currentAccount()??>
				         <li><span><a href="${systemSetting().my}/account/orders">我的订单</a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?status=dbk">待补款订单<div class="bkddnum">
				  <#if waitOrder?exists>   
                     ${waitOrder!""}
                   <#else >
                      0
                   </#if>
				         </div></a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?" class="tran" target="_self">我的anitoys</a>

				          <div class="top_right_nav">
				              <a href="${systemSetting().my}/account/orders?status=dsh" target="_self">已发货订单</a>
				              <a href="${systemSetting().my}/account/custService" target="_self">售后订单</a>
				              <a href="${systemSetting().my}/coupon/coupons?status=0" target="_self">优惠券</a>
				              <a href="${systemSetting().my}/favorite/searchUserFavoritePro" target="_self">收藏的商品</a>
				              <a href="${systemSetting().my}/favorite/searchUserFavoriteBusi" target="_self">收藏的店铺</a>
				          </div>
				          </span>
				        </li>
				       <#else >
				         <li><span><a href="${systemSetting().my}/account/login">我的订单</a></span></li>
				         <li><span><a href="${systemSetting().my}/account/login">待补款订单<div class="bkddnum">
				         <#if waitOrder?exists>   
                     		${waitOrder!""}
                   		 <#else >
                      		0
                   		 </#if>
				         </div></a></span></li>
				         <li><span><a href="${systemSetting().my}/account/login?"  target="_self" class="xljt">我的anitoys</a>
				          <div class="top_right_nav">
				            <ul>
				              <a href="${systemSetting().my}/account/login" target="_self">已发货订单</a>
				              <a href="${systemSetting().my}/account/login" target="_self">售后订单</a>
				              <a href="${systemSetting().my}/account/login" target="_self">优惠券</a>
				              <a href="${systemSetting().my}/account/login" target="_self">收藏的商品</a>
				              <a href="${systemSetting().my}/account/login" target="_self">收藏的店铺</a>
				          </div>
				          </span>
				        </li>
						</#if>
						<li>
							<span>
								<a href="#" target="_self" class="tran">帮助支持</a>
								<div class="top_right_nav">
								 <#if helpList??> 
								    <#list helpList as item>
											<a href="${systemSetting().doc}/front/helpsmanage/${item.id}.html" target="_self">${item.title}</a>
									</#list>
                   		 		<#else >
                   		 		</#if>
								</div>
							</span>
						</li>
						<li>
							<span>
								<a href="${basepath}/search.html" target="_Blank" class="tran">网站导航</a>
								<!--
								<div class="company_all clearfix" style="height:200px;">
									 <#list systemManager().businessList as item>
											<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${item.id!""}" style="height:80px;" target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"  width="80" height="60"/>
											</a>
									</#list>
								</div>
								-->
								<div class="top_right_nav imgS">
									<#list systemManager().businessList as item>
												<a href="${systemSetting().businessFront}/business/businessSelect?businessId=${item.id!""}"  target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"  style="width:100px;height:150px;"/>
												</a>
									</#list>
								</div>
								
							<!--
								<div class="top_right_nav imgS">
									<#list systemManager().businessList as item>
										<#if item_index<3>
												<a href="${systemSetting().business}/business/businessSelect?businessId=${item.id!""}"  target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"  style="width:100px;height:150px;"/>
												</a>
										</#if>
									</#list>
								</div>
								-->
								
							</span>
						</li>
						<li>
						<span>
							<a href="#" target="_blank" class="tran">关注anitoys</a>
							<div class="top_right_nav w100">
								<a href="#" target="_blank">
									<img src="${systemSetting().staticSource}/shopindex/layout/img/pic/towcode.png"/>
								</a>
							</div>
						</span>
					</li>
					</ul>
				</div>
			</div>
		</div>
</#macro>
