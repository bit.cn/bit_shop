<#macro shopFooter title="" checkLogin=true>
<!-- 友情链接 -->
<div class="ani_product clearfix">
<hr style="margin: 0px;">
	<div class="row" >
	<div class="col-xs-12" style="text-align: center;">
		<div style="text-align: center;margin: auto;">
						<#if systemManager().navigations??>
						    <#list systemManager().navigations as item>
                                <div style="float: left;margin: 5px;">
                                    <a href="http://${item.http!""}" target="_blank">${item.name!""}</a>
                                </div>
						    </#list>
						</#if>
					</div>				
				</div>
			 </div>
			</div>
			<div class="footer">
			<div class="foot-top">
			<!--帮助中心-->
			<ul class="clearfix">
					<#list systemManager().newsCatalogs as item>
						<li>
							<h3>${item.name!""}</h3>
							<#if item.news??>
								<#list item.news as item>
									<a href="${systemSetting().doc}/help/${item.code}.html" target="_blank">${item.title}</a>
								</#list>
							</#if>
						</li>
					</#list>
				</ul>
				<a href="${systemSetting().www}" class="logo"><img src="${systemSetting().staticSource}/shopindex/common/img/logo(beifen).gif" /></a>
			</div>
			<div class="foot-bottom">@2014 阿尼托 上海索罗游信息技术有限公司 anitoys.com 版权所有 沪ICP备11047967号-18</div>
		</div>
		<!-- cnzz站点统计 -->
		${systemSetting().statisticsCode!""}
		<script type="text/javascript" src="${systemSetting().staticSource}/shopindex/common/js/jquery-1.11.1.min.js"></script>
	    <script type="text/javascript" src="${systemSetting().staticSource}/resource/bootstrap/js/bootstrap.min.js"></script>
	    
	    
	    <script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery.lazyload.min.js"></script>
	    <script type="text/javascript" src="${systemSetting().staticSource}/resource/js/superMenu/js/new.js"></script>
	    
	    <script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery.blockUI.js"></script>
	    <script type="text/javascript" src="${systemSetting().staticSource}/resource/validator-0.7.0/jquery.validator.js"></script>
	    <script type="text/javascript" src="${systemSetting().staticSource}/resource/validator-0.7.0/local/zh_CN.js"></script>
		<script type="text/javascript" src="${systemSetting().staticSource}/resource/js/slideTab2/js/lanrenzhijia.js"></script>
		<script src="${systemSetting().staticSource}/shopindex/common/js/index.js"></script>
</#macro>
