<!--搜索栏-->
<#macro shopHeadbg404>
<div class="header" style="margin-top:0px;">
			<div class="header_main clearfix">
				<div class="logo">
					<a href="${systemSetting().www}"><img src="${systemSetting().staticSource}/shopindex/common/img/logo.gif" border="0" /></a>
				</div>
				<!--search查询输入框-->
				<div class="search clearfix">
					<div class="searchInput">
						<form id="searchForm" method="get" action="${systemSetting().search}/search.html" target="_blank">
						<input type="text" name="key"  value="${key!""}" id="mq" value="ANI大搜查" maxlength="24" onfocus="if(this.value=='ANI大搜查') {this.value='';}this.style.color='#333';" onblur="if(this.value=='') {this.value='ANI大搜查';this.style.color='#999999';}" />
						<button type="submit" onclick="search();" target="_Blank">搜整站</button>
						</form>
					</div>
					<div class="hotSearch">
						<span>
						热门搜索：
						<#list systemManager().hotqueryList as item>
	      					<a href="${item.url}">${item.key1!""}</a>
						</#list>
						</span>
					</div>
				</div>
				<div class="header_nav">
					<!--大首页导航-->
					<div class="nav_box" style="display:none;">
						<ul>
							<li><a href="#" target="_top" class="home">首页</a></li>
							<li><a href="#" target="_top">折扣大卖场</a></li>
							<li><a href="#" target="_top">限定旗舰店</a></li>
							<li><a href="#" target="_top">活动抢先看</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
  <script type="javascript">
//搜索商品
function search(){
	var _key = $.trim($("#mq").val());
	if(_key==''){
		return false;
	}
	$("#searchForm").submit();
}
</script>
</#macro>
