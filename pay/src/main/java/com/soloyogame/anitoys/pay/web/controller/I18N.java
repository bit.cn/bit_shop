package com.soloyogame.anitoys.pay.web.controller;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

import java.util.List;

import com.soloyogame.anitoys.util.i18n.MessageLoader;

/**
 * 国际化配置
 * @author shaojian
 */
public class I18N implements TemplateMethodModelEx 
{
    @Override
    public Object exec(List arguments) throws TemplateModelException 
    {
        return MessageLoader.instance().getMessage(arguments.get(0).toString());
    }

}
