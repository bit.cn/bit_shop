<#import "/account/myTop.ftl" as myTop>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>补款订单</title>
<script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery-1.8.2.min.js"></script>
<link href="${systemSetting().staticSource}/plac/css/replenishment.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${systemSetting().staticSource}/shopindex/cart/css/shoppingCart1.css" />
	<script type="text/javascript" src="${systemSetting().staticSource}/business/common/js/hover.js"></script>
<style type="text/css">
.aft_sum{
			color:#FF8000; 
			font-weight:bold; 
			font-size:16px; 
			margin-right:40px;
		}
</style>
</head>

<body>
	<div class="div_t1">
		<div class="topNav">
			<div class="top_main clearfix">
				<div class="top_left">
				<a href="${systemSetting().www}">"话不多说买买买"——欢迎来到anitoys</a>
				 <#if currentAccount()??>
				     <span class="sn-login"">欢迎回来，${currentAccount().account}</span>
				     <a href="${basepath}/account/exit" class="sn-regist">退出系统</a>
			    <#else >
				 <span>
					 <a href="${basepath}/account/login">请登陆</a>
					 <a href="${basepath}/account/register" class="regist">免费注册</a>
				 </span>
				</#if>
				</div>
				<div class="top_right">
					<ul>
					<#if currentAccount()??>
				         <li><span><a href="${basepath}/account/orders">我的订单</a></span></li>
				         <li><span><a href="${basepath}/account/orders?status=dbk">待补款订单<div class="bkddnum">
				<#if waitOrder?exists>   
                     ${waitOrder!""}
                   <#else >
                      0
                   </#if>
				         </div></a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?"  target="_self">我的anitoys</a>
				          <div class="top_right_nav">
				              <a href="${basepath}/account/orders?status=dsh" target="_blank">已发货订单</a>
				              <a href="${basepath}/account/custService" target="_blank">售后订单</a>
				              <a href="${basepath}/coupon/coupons?status=0" target="_blank">优惠券</a>
				              <a href="${basepath}/favorite/searchUserFavoritePro" target="_blank">收藏的商品</a>
				              <a href="${basepath}/favorite/searchUserFavoriteBusi" target="_blank">收藏的店铺</a>
				          </div>
				          </span>
				        </li>
				       <#else >
				         <li><span><a href="${basepath}/account/login">我的订单</a></span></li>
				         <li><span><a href="${basepath}/account/login">待补款订单<div class="bkddnum">
				         <#if waitOrder?exists>   
                     ${waitOrder!""}
                   <#else >
                      0
                   </#if>
				         </div></a></span></li>
				         <li><span><a href="${systemSetting().my}/account/orders?"  target="_self" class="xljt">我的anitoys</a>
				          <div class="top_right_nav">
				            <ul>
				              <a href="${basepath}/account/login" target="_blank">已发货订单</a>
				              <a href="${basepath}/account/login" target="_blank">售后订单</a>
				              <a href="${basepath}/account/login" target="_blank">优惠券</a>
				              <a href="${basepath}/account/login" target="_blank">收藏的商品</a>
				              <a href="${basepath}/account/login" target="_blank">收藏的店铺</a>
				          </div>
				          </span>
				        </li>
						</#if>
						<li>
							<span>
								<a href="${systemSetting().doc}/help/xsbz.html" target="_blank" class="tran">帮助支持</a>
								<div class="top_right_nav">
									<a href="${systemSetting().doc}/help/xsbz.html" target="_blank">帮助中心</a>
								</div>
							</span>
						</li>
						<li>
							<span>
								<a href="${basepath}/search.html" target="_blank" class="tran">网站导航</a>
								<div class="top_right_nav imgS">
								<#list systemManager().businessList as item>
										<#if item_index<3>
												<a href="${systemSetting().business}/business/businessSelect?businessId=${item.id!""}"  target="_blank">
												<img src="${systemSetting().imageRootPath}/${item.maxPicture!""}"  style="width:100px;height:150px;"/>
												</a>
										</#if>
											
									</#list>
							</div>
							</span>
						</li>
						<li>
						<span>
							<a href="#" target="_blank" class="tran">关注anitoys</a>
							<div class="top_right_nav w100">
								<a href="#" target="_blank">
									<img src="${systemSetting().staticSource}/shopindex/layout/img/pic/towcode.png"/>
								</a>
							</div>
						</span>
					</li>
					</ul>
				</div>
			</div>
		</div>
		<div><img src="${basepath}/plac/image/payment.png" /></div>
		<div>
			<table>
				<tr>
					<td class="td_t10">1.确认订单信息</td>
					<td class="td_t11">2.付款</td>
					<td class="td_t12">3.购物成功</td>
				</tr>
			</table>
		</div>
		<div style="padding-left: 5px;padding-bottom:5px;">
					<p>确认收货地址&nbsp;</p>
				</div>
				<div class="site_save" width="100%" style="padding-left: 5px;">
					<#list addressList as address> 
						<#if address_index <=3 >
							<div class="inputa" style=" width:800px ;">
								<input class="addressId" name="Fruit" type="radio" value="${address.id}" ${(address.isdefault=="y")?string("checked","") } />
									&nbsp;${address.pcadetail!''}&nbsp;${address.address!''}&nbsp;(${address.name!''} 收)&nbsp;${address.phone!''}&nbsp;${address.mobile!''}
							</div><br> 
						<#else>
							<div name="addressxs" class="inputa" style="visibility: none;display: none; width:800px ;">
								<input name="Fruit" type="radio" value="${address.id}" ${(address.isdefault=="y")?string("checked","") } />&nbsp;${address.pcadetail!''}&nbsp;${address.address!''}&nbsp;(${address.name!''} 收)&nbsp;${address.phone!''}&nbsp;${address.mobile!''}
							</div>
							<br name="addressxs" style="visibility: none;display: none;">
						</#if> 
					</#list> 
					<a herf="" name="addressxs" onclick="valid()">更多</a>
					<a herf="" name="addressyc" style="visibility: none;display: none;" onclick="none()">收起</a>
				</div>
		<div style="border:solid 0px; height: 30px; line-height: 20px; padding-top:5px; padding-left: 5px">
			<p>
				商品清单&nbsp;<span class="span_style_2">当下单的商品包含预售商品和现货商品，以及商品来自不同的仓库的时候会被拆分为数个订单，请注意</span>
			</p>
		</div>
		<div class="div_style_4">
			<table class="table_style_4" width="100%" border=0>
				<tr class="tr_t1">
					<td style="text-align:center;width:400px;font-weight: bold; ">商品</td>
					<td style="text-align:center;font-weight: bold">款式</td>
					<td style="text-align:center;width:170px;font-weight: bold">成交单价</td>
					<td style="text-align:center;width:170px;font-weight: bold">数量</td>
					<td style="text-align:center;width:170px;font-weight: bold">小计</td>
				</tr>
			</table>
			<div width="100%" style="background: #F2F2F2; margin-bottom: 5px;">
				<#if orderDetail??>
				<input type="hidden" value="${order.orderCode}" name="orderCode" id="orderCode" />
				<table class="table_style_4" width="100%">
					<tr class="head_tb_tr" style="border:solid 0px;">
						<td colspan="5"
							style="padding-left: 5px;vertical-align: middle; height:30px">
							<span style=""> 商家名称: ${order.businessName!""} </span>
						</td>
					</tr>
					<tr style="border-bottom:1px #C2C2C2 dashed;">
						<td class="td_t3" style="width:400px; padding-left: 5px">
							<div width="100%" height="100%" style="padding: 5px;position:relative">
								<table>
									<tr>
										<td><img src="${basepath}${orderDetail.picture!""}" width="60px" height="70px" /></td>
										<td style="text-align:center">
											<a class="td_t3" style="position:absolute;align:middle;margin: auto;">${orderDetail.productName}</a>
										</td>
									</tr>
								</table>
							</div>
						</td>
						<td class="td_t3" style="text-align:center">
							<br>${orderDetail.specSize!""}<br />
							<p class="p_style_2">${orderDetail.specColor!""}</p> 
						</td>
						<td class="td_t3" style="text-align:center;width:170px">￥${orderDetail.price}</td>
						<td class="td_t3" style="text-align:center;width:170px">${orderDetail.number}</td>
						<td class="td_t3" style="text-align:center;width:170px">￥${orderDetail.total0!"0.0"}</td>
					</tr>
					<tr>
						<td class="td_t3" style="vertical-align:middle;padding-left: 5px">快递方式： 
							<select style="margin-left:13px;" name="sel" id="shipping${order.orderCodeView}" onchange="changeShipping(${order.orderCodeView})"> 
								<#list order.businessShippingList as businessShippingItem>
									<option value="${businessShippingItem.shippingId!""}">${businessShippingItem.shippingName!""}</option>
								</#list>
							</select>
						</td>
						<td></td>
						<td></td>
						<td class="td_t6">运费：</td>
						<td style="padding-left: 60px">
							<label class="aft_sum">￥<span id="shipmoney${order.orderCodeView}">0</span></label>
						</td>
					</tr>
					<tr>
						<td class="td_t3" style="padding-left: 5px">商家优惠券： 
							<select id="couponSn${order.orderCodeView}" name="sel" onchange="changeCouponSn(${order.orderCodeView})"> 
								<#list order.accountCouponList as accountCouponItem>
									<option value="${accountCouponItem.id!""},${accountCouponItem.couponValue!""},${accountCouponItem.couponType!""}">${accountCouponItem.couponName!""}</option>
								</#list>
							</select>
						</td>
						<td></td>
						<td></td>
						<td class="td_t6"></td>
						<td class="td_t3" style="padding-left: 60px">
							<#if order.accountCouponList[0]??> 
								<label class="aft_sum">￥<span id="money${order.orderCodeView}">${order.accountCouponList[0].couponValue!0} </span></label>
							</#if>
						</td>
					</tr>
					<tr>
						<td colspan="1" style="padding-left: 5px">
							<p>
								<a class="td_t3" style="margin-left:13px;">订单备注：</a> 
										<input id="otherRequirement" class="textarea_style_1" 
												type="text" name="otherRequirement" placeholder="请输入与商家达成一致的要求！"
												value="${order.otherRequirement!''}"/>
							</p>
						</td>
						<td></td>
						<td></td>
						<td class="td_t6">${order.quantity}件商品&nbsp;&nbsp;合计：</td>
						<td class="td_t3" style="padding-left: 60px">
							<input id="ptotal${order.orderCodeView}" value="${order.balancePrice!0}" style="display: none;" />
							<label class="aft_sum" style="color:red"> ￥<span name="heji" id="ptotalVal${order.orderCodeView}">${order.balancePrice!0}</span></label>
						</td>
					</tr>
					<tr style="height:8px;border-bottom:solid 1px #C1C1C1;">
									
					</tr>
					<tr>
						<td colspan="5" height="5" style="background-color: white"></td>
					</tr>
					</#if>
				</table>
			</div>
			<div width="100%" style="background: #F2F2F2; margin-bottom: 5px;">
				<table width="100%">
					<!-- 		<tr> -->
					<!-- 			<td style="width:400px"></td> -->
					<!-- 			<td></td> -->
					<!-- 			<td class="td_t6" style="width:170px" colspan="2">已付定金：</td> -->
					<!-- 			<td class="td_t3" style="text-align:center;width:170px">￥</td> -->
					<!-- 		</tr> -->
					<tr>
						<td class="td_t3"></td>
						<td></td>
						<td class="td_t6" colspan="2">（当前用户积分剩余<label id="scoreLabel">${rank!0}</label>分）使用
							<input id="accountScore" type="text" style="width:50px"
									onchange="changeAccountScore();" /> 积分
						</td>
						<td class="td_t3" style="text-align:center;">
							<label class="aft_sum" style="color:red;margin-left:40px;">￥ <span
								id="accountScoreValue">0</span>
							</label>
						</td>
					</tr>
					<tr>
						<td class="td_t3" style="padding-left: 5px">平台抵扣券：
							<select id="accountVoucher" name="sel" onchange="changeAccountVoucher();"> 
								<#list order.accountVoucherList as accountVoucherItem>
									<option value="${accountVoucherItem.id!""},${accountVoucherItem.couponValue!""}">${accountVoucherItem.couponName!""}</option>
								</#list>
							</select>
						</td>
						<td></td>
						<td></td>
						<td class="td_t6"></td>
						<td class="td_t3" style="text-align:center;width:170px"> 
							<label class="aft_sum" style="color:red;margin-left:40px;">￥
								<span class="span_style_2"></span> 
								<#if order.accountVoucherList[0]?? > 
								<span class="span_style_2" id="voucherMoney">${order.accountVoucherList[0].couponValue!0}</span>
								</#if>
							</label>
						</td>
					</tr>
				</table>
			</div>
			<p><span class="span_style_2" style="padding-left: 5px">下单后如果取消订单，所使用的积分和优惠券将不再恢复</span></p>
			<table class="table_style_3">
				<tr>
					<td style="width: 200px">应付金额: 
						<span style="font-size:18px;color:red;font-weight:bold"> ￥ 
							<span style="font-size:18px;color:red;font-weight:bold"
									id="parentAmount">${order.amount!0}
							</span>
						</span>
					</td>
					<td class="td_t1" style="padding-right:20px;">
					<input type="button" class="btn-style-01" value="确认支付" onclick="saveOrder()" /></td>
				</tr>
				<tr>
					<td colspan="2" style="height:20px"></td>
				</tr>
			</table>
		</div>
	</div>
	<form id="formToPlacPay" method="post" action="${systemSetting().orders}/order/plac_pay">
		<input type="hidden" name="placPayInput" />
	</form>
</body>
<script type="text/javascript">

	var shippingFlag = "Y";

 	function toPlacPay() {
//		$("#formPlacPay :hidden[name=placPayInput]").val(productId);
 		$("#formToPlacPay").submit();
 	}

	//序列話
	$.fn.serializeObject = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	function saveOrder() {
		var orderCode = eval(document.getElementById("orderCode")).value;
		var accountVoucher = $('#accountVoucher option:selected').val();
		var parentAmount = $("#parentAmount").html();
// 		var addressId = "";
// 		var temp = document.getElementsByName("Fruit");
// 		for (var i = 0; i < temp.length; i++) {
// 			if (temp[i].checked)
// 				addressId = temp[i].value;
// 		}

		$.ajax({
			url : "${basepath}/order/pay",
			type : 'POST',
			//		dataType:'JSON',
			data : {
				orderCode : orderCode,
			//	parentId : parentId,
				accountVoucher : accountVoucher,
				parentAmount : parentAmount,
			//	addressId : addressId
				intoType : "placAdd"
			},
			success : function(data) {
				//alert("success");
				toPlacPay();
			},
			error : function(error) {
				console.log(error);
			}
		})
	}

	function valid() {
		obj = document.getElementsByName("addressxs");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "visible";//显示;
			obj[i].style.display = "";//显示
		}
		obj = document.getElementsByName("addressyc");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "visible";//显示;
			obj[i].style.display = "";//显示
		}
	}
	
	function none() {
		obj = document.getElementsByName("addressxs");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "addressyc";//显示;
			obj[i].style.display = "";//显示
		}
		obj = document.getElementsByName("addressyc");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "";//显示;
			obj[i].style.display = "none";//显示
		}
	}

	function changeCouponSn(id) {
		var couponSnId = "couponSn" + id;
		var moneyId = "money" + id;
		var ptotalId = "ptotal" + id;
		var ptotalValId = "ptotalVal" + id;
		var shipmoneyId = "shipmoney" + id;
		couponValue = document.getElementById(couponSnId).value;
		
		var strArr = new Array(); //定义一数组
		strArr = couponValue.split(",");
		if(parseInt(strArr[2]) == 1) {	//折价券
			couponValue = strArr[1];
		} else if (parseInt(strArr[2]) == 2) {		//折扣券
			var ccc = document.getElementsByName("heji");
			var ddd = 0;
			for (i = 0; i < ccc.length; i++) {
				ccc[i].innerHTML;//显示;
				ddd = ddd + parseFloat(ccc[i].innerHTML)
			}
			couponValue = (parseFloat(strArr[2])/100) * parseFloat(ddd);
		} else {
			couponValue = parseFloat(strArr[1]);
		}
		
		ptotalValue = document.getElementById(ptotalId).value;
		shipmoney = document.getElementById(shipmoneyId).innerHTML;
		document.getElementById(moneyId).innerHTML = couponValue;

		document.getElementById(ptotalValId).innerHTML = (ptotalValue
				- couponValue + parseFloat(shipmoney));
		if (ptotalValue - couponValue + parseFloat(shipmoney) < 0) {
			document.getElementById(ptotalValId).innerHTML = 0;
		}

		heji();
	}

	function changeShipping(id) {
		var shippingId = "shipping" + id;
		var selectedIndex = document.getElementById(shippingId).selectedIndex;
		var shipmoney = 0;
		var shipmoneyId = "shipmoney" + id;
		var moneyId = "money" + id;
		var ptotalId = "ptotal" + id;
		var ptotalValId = "ptotalVal" + id;
		shippingId = document.getElementById(shippingId).value;
		var temp = document.getElementsByName("Fruit");
		var addressId = "";
		for (var i = 0; i < temp.length; i++) {
			if (temp[i].checked)
				addressId = temp[i].value;
		}
		if (addressId == "") {
			alert("请选择配送地址!");
			document.getElementById(shippingId).selectedIndex = 0;
			return;
		}
		if (selectedIndex > 0) {
			$.ajax({
						url : "${systemSetting().orders}/order/getShiping",
						type : 'POST',
						dataType : 'JSON',
						data : {
							id : id,
							shippingId : shippingId,
							addressId : addressId
						},
						success : function(data) {
							if(data == "noMatch") {
								document.getElementById(shipmoneyId).innerHTML = "0";
								shippingFlag = "noMatch";
							} else {
								shippingFlag = "Y";
								shipmoney = data;
								shipmoney = new Number(shipmoney);
								document.getElementById(shipmoneyId).innerHTML = shipmoney.toFixed(2);
								var money = document.getElementById(moneyId).innerHTML;
								ptotalValue = document.getElementById(ptotalId).value;
								var hejijine = parseFloat(ptotalValue)
										+ parseFloat(shipmoney) - parseFloat(money);
								if (hejijine < 0) {
									document.getElementById(ptotalValId).innerHTML = 0;
								} else {
									document.getElementById(ptotalValId).innerHTML = hejijine.toFixed(2);
								}
								heji();
							}
						}
					})
		} else {
			document.getElementById(shipmoneyId).innerHTML = shipmoney.toFixed(2);
			var money = document.getElementById(moneyId).innerHTML;
			ptotalValue = document.getElementById(ptotalId).value;
			var hejijine = parseFloat(ptotalValue) + parseFloat(shipmoney)
					- parseFloat(money);
			if (hejijine < 0) {
				document.getElementById(ptotalValId).innerHTML = 0;
			} else {
				document.getElementById(ptotalValId).innerHTML = hejijine;
			}
			heji();
		}
	}

	//积分输入change函数
	function changeAccountScore() {
		var accountScore = document.getElementById("accountScore").value;
		var score = document.getElementById("scoreLabel").innerHTML;
		if(parseInt(accountScore) > parseInt(score)) {
 			alert("使用积分已超过您拥有积分");
 			accountScore = 0;
 		}
		if(accountScore == null || accountScore == "") {
			accountScore = 0;
		}
		var accountScoreValue = parseFloat(accountScore) / 100;
		//alert((Math.round(accountScoreValue*Math.pow(10,2))/Math.pow(10,2)).toFixed(2));
		document.getElementById("accountScoreValue").innerHTML = accountScoreValue.toFixed(2);
		heji();
	}

	//平台抵扣券change函数
	function changeAccountVoucher() {
		var accountVoucherValue = document.getElementById("accountVoucher").value;
		accountVoucherValue = accountVoucherValue.substr(accountVoucherValue.indexOf(",") + 1);
		document.getElementById("voucherMoney").innerHTML = accountVoucherValue;
		heji();
	}

	function heji() {
		var ccc = document.getElementsByName("heji");
		var voucherMoney = document.getElementById("voucherMoney").innerHTML;
		var accountScoreValue = document.getElementById("accountScoreValue").innerHTML;
		var money = 0;
		for (i = 0; i < ccc.length; i++) {
			ccc[i].innerHTML;//显示;
			money = money + parseFloat(ccc[i].innerHTML) - accountScoreValue - voucherMoney;
		}
		document.getElementById("parentAmount").innerHTML = money.toFixed(2);
	}
</script>