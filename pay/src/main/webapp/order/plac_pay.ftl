<#import "/shopindex/common/ftl/shopBasePage.ftl" as shopBasePage>
<#import "/shopindex/common/ftl/shopTop.ftl" as shopTop>
<#import "/account/myTop.ftl" as myTop>
<#import "/shopindex/common/ftl/shopFooters.ftl" as shopFooters>
<@shopBasePage.shopBasePage/>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>订单详情</title>
	<link href="${systemSetting().staticSource}/plac/css/pay.css" rel="stylesheet" type="text/css" />
	<link href="${systemSetting().staticSource}/shopindex/common/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/shopindex/layout/css/omt.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery-1.8.2.min.js"></script>
	<link rel="stylesheet" href="${systemSetting().staticSource}/shopindex/cart/css/shoppingCart1.css" />
	<script type="text/javascript" src="${systemSetting().staticSource}/business/common/js/hover.js"></script>
	<style type="text/css">
		 .hideDlg
    {
        display:none;
    }
    .showDlg
    {
        background-color:#ffffdd;
        border-width:1px;
        border-style:solid;
        height:200px;width:320px;
        position: absolute; 
        display:block;
        z-index:5;
		font-size:12px;
		padding:10px;
    }
    .showDeck {
        display:block;
        top:0px;
        left:0px;
        margin:0px;
        padding:0px;
        width:100%;
        height:100%;
        position:absolute;
        z-index:3;
        background:#cccccc;
    }
    .hideDeck 
    {
        display:none;
    }
.button_zf{
	width:100px;
	height:35px;
	font-size:14px;
	background-color:#F30;
	color:#FFF;
	}
.showTop{
	background-image:url("${basepath}/zhezhao/zhezhao_03.png");
	background-repeat:no-repeat;
	width:557px;
	height:43px;
	color:#FFF;
	font-size:16px;
	line-height:43px;
	padding-left:15px;
	
	}
.showMiddle{
	height:216px;
	width:557px;
	background-color:#FFF;
	font-size:14px;
	color:#696666;
	padding-top:15px;
	}
.showMiddle_left{
	float:left;
	margin-left:60px;
	margin-top:15px;
	overflow:hidden;
	}
.showMiddle_right{
	float:left;
	margin-left:40px;
	margin-top:10px;
	overflow:hidden;
	}
.showButton{
	clear:both;
	padding-top:40px;
	width:300px;
	margin:0 auto;
	}
.showButton1{
	background-image:url("${basepath}/zhezhao/zhezhao_09.png") no-repeat;
	background-repeat:no-repeat;
	width:132px;
	height:40px;
	font-size:17px;
	color:#696666;
	border:0px;
	line-height:40px;
	cursor:pointer;
	}
.showButton2{
	background-image:url("${basepath}/zhezhao/zhezhao_11.png");
	background-repeat:no-repeat;
	width:132px;
	height:40px;
	font-size:17px;
	color:#FFFFFF;
    border:0px;
	line-height:40px;
	cursor:pointer;
	}
.showBottom{
	background-image:url("${basepath}/zhezhao/zhezhao_06.png");
	background-repeat:no-repeat;
	width:557px;
	height:6px;
	}
	</style>
</head>

<body style="padding-bottom:0px" onresize="adjustLocation();">
<!--商城主页top页-->
	<@shopTop.shopTop/>
	<div class="div_t4">
		<div>
			<p>
				<span class="span_style_1" style="padding-bottom: 100px">当下单的商品包含预售商品和现货商品，以及商品来自不同的仓库的时候会被拆分为数个订单，请注意</span>
			</p>
			<div class="p_style_1">
				<table class="table_style_4" width="100%" border=0>
					<tr class="tr_t1">
						<th class="td_t3">订单号</th>
						<th class="td_t4">订单总价</th>
						<th>订单详情</th>
					</tr>
				</table>
			</div>
			<div class="div_style_4">
				<table width="100%" height="50px" border=0>
					<#if mainOrder??>
					<tr>
						<td class="td_t3">${mainOrder.orderCode}</td>
						<td class="td_t4">￥${mainOrder.amount!"0"}</td>
						<td style="text-align:center">${mainOrder.remark!""}</td>
					</tr>
					</#if>
				</table>
			</div>
			<p height="100px" style="padding-top: 10px">
				支付总额&nbsp;￥<span class="span_style_2" id="orderAmount">${mainOrder.amount!"0.00"}</span>
			</p>
			<p>
				<span class="span_style_1">订单将在3-6个小时后自动过期，请及时付款</span>
			</p>
			<div class="div_t3">
				<div>
					<table height="80px">
						<tr>
							<td>
								<p>选择支付方式</p>
							</td>
						</tr>
						<tr>
							<td class="td_t5">
								<input type="checkbox" id="nowMoney" onchange="checkMoney()" name="payType" />&nbsp;&nbsp;
								 应支付:￥<span class="span_style_2" id="payAccountAmount">${mainOrder.amount!"0.00"}</span>
								（账户余额:￥<span class="span_style_2" id="accountAmount">${accountAmount!"0.00"}</span>元）
							</td>
						</tr>
					</table>
				</div>
				<div>
					<table height="80px">
						<tr>
							<td>
								<p>第三方支付方式</p>
							</td>
						</tr>
						<tr>
							<td class="td_t5"><input type="checkbox" id="payBao" name="payType" />&nbsp;&nbsp;
							            支付宝 &nbsp;<img
								align="center" src="${basepath}/plac/image/Alipay.jpg" /> 支付:￥<span
								class="span_style_2 payBao">${mainOrder.amount}</span>元</td>
						</tr>
					</table>
						<table class="table_style_1">
							<tr>
								<td>应付金额:￥
									<span class="span_style_2" id="yingfuMoney" style="font-size:20px">${mainOrder.amount}</span>
								</td>
								<td class="td_t2">
									<input type="submit" class="btn-style-01" value="立即支付" onclick="toPay();"/>
								</td>
							</tr>
						</table>
				</div>
			</div>
		</div>
	<input type="hidden" value="${mainOrder.orderCode}" name="orderCode" id="orderCode"/>
	</div>
	<!-- 商城主页footer页 -->
<@shopFooters.shopFooters/>
<!-- 支付遮罩层 -->
<div id="divBox" class="hideDlg" style="" onResize="adjustLocation();">
        <div class="showTop">网上支付提示</div>
        <div class="showMiddle">
          <div class="showMiddle_left"><img src="${basepath}/zhezhao/lodin.gif"></div>
          <div class="showMiddle_right">
             <p>支付完成前，请不要关闭此支付验证窗口。</p><p>支付完成后，请根据您支付的情况点击下面按钮。</p>
          </div>
          <div class="showButton">
           <input type="button" class="showButton1" style="margin-right:20px;" value="支付遇到问题" onClick="alert(TextBox1.value)"/>
           <input type="button" class="showButton2" value="支付完成" onClick="cancel();"/>
          </div>
        </div>
        <div class="showBottom"></div>
    </div>
    <form id="toPaySuccess" method="post" action="${systemSetting().pay}/order/paySuccessAgain">
		<input type="hidden" name="paySuccessBtn" />
	</form>
</body>
<script>
	
	var payFlag = "success";

	function toPay(){
		if($("#nowMoney").is(':checked')==false&&$("#payBao").is(':checked')==false){
			alert("请选择支付方式");
			return false;
		}
		var payBao = $(".payBao").text();
		var money = $("#accountAmount").text();
		var yingPay = $("#orderAmount").text();
		if($("#nowMoney").is(':checked')){
			if($("#payBao").is(':checked')==false){
				if(parseInt(money)<parseInt(yingPay)){
					alert("请选择支付宝支付剩余金额");
					return false;
				}
			}
		}else{
			$(".payBao").html(${mainOrder.amount!'0.00'});
		}
		
		showDlg();
		
		var payWay = "accAmount";
		var payBaoAmount = 0;
		var payAccountAmount = 0;
		if($("#nowMoney").is(':checked') == true && $("#payBao").is(':checked') == false){
			payWay = "amtWay";
			payAccountAmount = $("#payAccountAmount").text();
		} else if($("#nowMoney").is(':checked') == false && $("#payBao").is(':checked') == true) {
			payWay = "baoWay";
			payBaoAmount = $(".payBao").text();
		} else if($("#nowMoney").is(':checked') == true && $("#payBao").is(':checked') == true) {
			payWay = "allWay";
			payAccountAmount = $("#payAccountAmount").text();
			payBaoAmount = $(".payBao").text();
		}
		
// 		var url = "${basepath}/order/toPay?orderCode=" + $("#orderCode").val() + 
// 				"&payBaoAmount=" + payBaoAmount + 
// 				"&payAccountAmount=" + payAccountAmount + 
// 				"&payWay=" + payWay;		
// 		document.payForm.action = url;
// 	 	$("#payForm").submit();
	 	var orderCode = $("#orderCode").val();
	 	$.ajax({
			url : "${systemSetting().pay}/order/toPay",
			type : 'POST',
			//dataType:'JSONp',
			data : {
				orderCode : orderCode,
				payBaoAmount : payBaoAmount,
				payAccountAmount : payAccountAmount,
				payWay : payWay
			},
			success : function(data) {
				if(payWay == "baoWay" || payWay== "allWay") {
					  document.body.addEventListener('click', function() {
            			window.open('${systemSetting().pay}/alipayapi.jsp', '_blank');
       				  });
				
				} else {
				 	document.body.addEventListener('click', function() {
            			window.open('paySuccess.ftl', '_blank');
       				  });
				}
				payFlag = "success";
			},
			error : function(error) {
				payFlag = "fail";
				console.log(error);
			}
		})
	}
	
// 	function makePay(){
// 		if($("#nowMoney").is(':checked')==false&&$("#payBao").is(':checked')==false){
// 			alert("请选择支付方式");
// 			return false;
// 		}
// 		var payBao = $(".payBao").text();
// 		var money = $("#accountAmount").text();
// 		var yingPay = $("#orderAmount").text();
// 		if($("#nowMoney").is(':checked')){
// 			if($("#payBao").is(':checked')==false){
// 				if(parseInt(money)<parseInt(yingPay)){
// 					alert("请选择支付宝支付剩余金额");
// 					return false;
// 				}
// 			}
// 		}else{
// 			$(".payBao").html(${mainOrder.amount!'0.00'});
// 		}
// 		return true;
// 	}
	function checkMoney(){
		var payBao = $(".payBao").text();
		var money = $("#accountAmount").text();
		var yingPay = $("#orderAmount").text();
		if($("#nowMoney").is(':checked')){
			if(parseFloat(yingPay) > parseFloat(money)) {
				$(".payBao").html((yingPay- money).toFixed(2));
				$("#payAccountAmount").html(money);
			} else {
				$(".payBao").html(0);
				$("#payAccountAmount").html(yingPay);
			}
		}else{
			$(".payBao").html((${mainOrder.amount!'0.00'}).toFixed(2));
		}
	}
	
	
	function showDlg()
    {
        //显示遮盖的层
        var objDeck = document.getElementById("deck");
        if(!objDeck)
        {
            objDeck = document.createElement("div");
            objDeck.id="deck";
            document.body.appendChild(objDeck);
        }
        objDeck.className="showDeck";
        objDeck.style.filter="alpha(opacity=50)";
        objDeck.style.opacity=40/100;
        objDeck.style.MozOpacity=40/100;
        //显示遮盖的层end
        
        //禁用select
        hideOrShowSelect(true);
        
        //改变样式
        document.getElementById('divBox').className='showDlg';
        
        //调整位置至居中
        adjustLocation();
        
    }
    
    function cancel()
    {
        document.getElementById('divBox').className='hideDlg';
        document.getElementById("deck").className="hideDeck";
        hideOrShowSelect(false);
        if(payFlag == "success") {
        	
        	$.ajax({
				url : "${basepath}/order/paySuccess",
				type : 'POST',
				//dataType:'JSONp',
				data : {},
				success : function(data) {
				//alert(data==1);
				$("#toPaySuccess").submit();
				},
				error : function(error) {
					payFlag = "fail";
					console.log(error);
				}
			});
        } else {
        	alert("支付异常，请重新支付！");
        }
        
    }
    
    function hideOrShowSelect(v)
    {
        var allselect = document.getElementsByTagName("select");
        for (var i=0; i<allselect.length; i++)
        {
            //allselect[i].style.visibility = (v==true)?"hidden":"visible";
            allselect[i].disabled =(v==true)?"disabled":"";
        }
    }
    
   function adjustLocation()
    {
        var obox=document.getElementById('divBox');
        if (obox !=null && obox.style.display !="none")
        {
            var w=557;
            var h=265;
            var oLeft,oTop;
            
            if (window.innerWidth)
            {
                oLeft=window.pageXOffset+(window.innerWidth-w)/2 +"px";
                oTop=window.pageYOffset+(window.innerHeight-h)/2 +"px";
            }
            else
            {
                var dde=document.documentElement;
                oLeft=dde.scrollLeft+(dde.offsetWidth-w)/2 +"px";
                oTop=dde.scrollTop+(dde.offsetHeight-h)/2 +"px";
            }
            
            obox.style.left=oLeft;
            obox.style.top=oTop;
        }
    }
</script>