<#import "/shopindex/common/ftl/shopBasePage.ftl" as shopBasePage>
<#import "/shopindex/common/ftl/shopTop.ftl" as shopTop>
<#import "/account/myTop.ftl" as myTop>
<#import "/shopindex/common/ftl/shopFooters.ftl" as shopFooters>
<@shopBasePage.shopBasePage/>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>订单确认</title>
	<link href="${systemSetting().staticSource}/shopindex/common/css/style.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/shopindex/layout/css/omt.css" rel="stylesheet" type="text/css">
	<link href="${systemSetting().staticSource}/plac/css/pay.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${systemSetting().staticSource}/resource/js/jquery-1.8.2.min.js"></script>
	<link href="${systemSetting().staticSource}/plac/css/downOrder.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${systemSetting().staticSource}/shopindex/cart/css/shoppingCart1.css" />
	<script type="text/javascript" src="${systemSetting().staticSource}/business/common/js/hover.js"></script>
	<style type="text/css">
		.aft_sum{
			color:#FF8000; 
			font-weight:bold; 
			font-size:16px; 
			margin-right:40px;
		}
		
/* 		table {border: 1px solid #000;} */
/* 		td {border: 1px solid #000;} */
	</style>
</head>
<body style="padding-bottom:0px;">
<!--商城主页top页-->
	<@shopTop.shopTop/>
	<div class="div_t4">
		<div>
			<img src="${systemSetting().staticSource}/plac/image/settlement.png" />
		</div>
		<div width="100%" style="padding-bottom: 10px">
			<hr color="#CCCCCC">
		</div>
		<div>
			<table border="0">
				<tr style="">
					<td class="td_t10" style="color: #FFFFFF;">1.确认订单信息</td>
					<td class="td_t11">2.付款</td>
					<td class="td_t12">3.购物成功</td>
				</tr>
			</table>
		</div>
		<div class="div_style_4" style="margin-top:10px">
			<form class="form-horizontal" method="post">
				<input type="hidden" value="${parentOrder.orderCode}" name="orderCode" id="orderCode" />
				<div style="border:1px solid #CCCCCC;padding-bottom:5px">
					<div style="padding-left: 5px;padding-bottom:5px;">
						<p>确认收货地址&nbsp;</p>
					</div>
					<div class="site_save" width="100%" style="padding-left: 5px;">
		             <input type="hidden" id="adds" value="${addressListSize!0}"/>
						<#list addressList as address> 
							<#if address_index <=3 >
								<div style=" width:800px ;">
									<input class="addressId" name="Fruit" type="radio" value="${address.id}" ${(address.isdefault=="y")?string("checked","") } />
										&nbsp;${address.pcadetail!''}&nbsp;${address.address!''}&nbsp;(${address.name!''} 收)&nbsp;${address.phone!''}&nbsp;${address.mobile!''}
								</div><br> 
							<#else>
								<div name="addressxs" class="inputa" style="visibility: none;display: none; width:800px ;">
									<input name="Fruit" type="radio" value="${address.id}" ${(address.isdefault=="y")?string("checked","") } />&nbsp;${address.pcadetail!''}&nbsp;${address.address!''}&nbsp;(${address.name!''} 收)&nbsp;${address.phone!''}&nbsp;${address.mobile!''}
								</div>
								<br name="addressxs" style="visibility: none;display: none;">
							</#if> 
						</#list> 
						<a herf="" name="addressxs" onclick="valid()">更多</a>
						<a herf="" name="addressyc" style="visibility: none;display: none;" onclick="none()">收起</a>
					</div>
				</div>
				<div style="border:solid 0px; height: 30px; line-height: 20px; padding-top:5px; padding-left: 5px">
					<p>
						商品清单&nbsp;<span class="span_style_2">当下单的商品包含预售商品和现货商品，以及商品来自不同的仓库的时候会被拆分为数个订单，请注意</span>
					</p>
				</div>
				<div class="div_style_4">
<!-- 					<table class="table_style_4" width="100%"> -->
<!-- 						<tr class="tr_t1"> -->
<!-- 							<td style="text-align:center;width:45%;font-weight: bold;">商品</td> -->
<!-- 							<td style="text-align:left;font-weight: bold">款式</td> -->
<!-- 							<td style="text-align:left;width:15%;font-weight: bold">成交单价</td> -->
<!-- 							<td style="text-align:left;width:10%;font-weight: bold">数量</td> -->
<!-- 							<td style="text-align:left;width:5%;font-weight: bold">小计</td> -->
<!-- 						</tr> -->
<!-- 					</table> -->
					<div width="100%" style="margin-bottom: 5px;">
						<table class="table_style_4" width="100%">
						<tr class="tr_t1">
							<td style="text-align:center;width:40%;font-weight: bold;">商品</td>
							<td style="text-align:left;font-weight: bold">款式</td>
							<td style="text-align:left;width:15%;font-weight: bold">成交单价</td>
							<td style="text-align:left;width:10%;font-weight: bold">数量</td>
							<td style="text-align:left;width:1%;font-weight: bold;padding-left: 40px">小计</td>
						</tr>
						<!-- ----------------------- 预售商品子订单---------------------------- -->
							<#list sonOrderList as item> 
							<#if item.orderType == 2>
							<tr class="head_tb_tr">
								<td colspan="5" style="padding-left: 5px;vertical-align: middle; height:30px">
									<span>${item.businessName!""}</span>					
								</td>
							</tr>
							<#list item.orderdetail as orderdetailItem>
							<tr style="border-bottom:1px #C2C2C2 dashed;">
								<td class="td_t3" style="padding-left: 5px">
									<div width="100%" height="100%" style="padding: 5px;position:relative">
										<table>
											<tr>
												<td>
													<a style="float:left;width:27%;margin-right:2%;margin-top:2%;margin-bottom:2%;text-align:center;" 
														href="${systemSetting().item}/product/${orderdetailItem.productID!""}.html" target="_blank"
														title="${item.productName!""}">
														<img src="${systemSetting().imageRootPath}${orderdetailItem.picture}"
															width="60px" height="70px" />
													</a>
												</td>
												<td>
													<div style="padding-left: 10px; text-align: left;">
														<a href="${systemSetting().item}/product/${orderdetailItem.productID!""}.html" target="_blank" title="${item.productName!""}">${orderdetailItem.productName}</a>
														<br/>
														<br/>
														<a>预售</a>
													</div>
												</td>
											</tr>
										</table>
									</div>
								</td>
								<td class="td_t3" style="text-align:left;">
									<br>${orderdetailItem.specSize!""}<br/>
									<p class="p_style_2">${orderdetailItem.specColor!""}</p> 
								</td>
								<td class="td_t3" style="text-align:left">￥${orderdetailItem.price}</td>
								<td class="td_t3" style="text-align:left">${orderdetailItem.number}</td>
								<td class="td_t3" style="text-align:left;padding-left: 40px">￥${orderdetailItem.total0}</td>
							</tr>
							</#list>
							<tr>
								<td colspan="2" style="padding-left: 5px">
									<p>
										<a class="td_t3" style="margin-left:13px;">订单备注：</a> 
										<input id="otherRequirement${item.orderCodeView}" class="ipt-text"
												type="text" name="otherRequirement" placeholder="请输入与商家达成一致的要求！"
												value="${item.otherRequirement!''}"
												onchange="changeOtherRequirement(${item.orderCodeView});"/>
									</p>
								</td>
								<td colspan="2" class="td_t6">${item.quantity}件商品&nbsp;&nbsp;定金：</td>
								<td class="td_t3" style="padding-left: 10px">
									<input id="ptotal${item.orderCodeView}" value="${item.advancePrice!""}" style="display: none;" />
									<label class="aft_sum" style="color:red"> ￥<span name="heji" id="ptotalVal${item.orderCodeView}">${item.advancePrice}</span></label>
								</td>
							</tr>
							<tr style="height:8px;border-bottom:solid 1px #C1C1C1;">
									
							</tr>
							</#if>
							</#list>
						<!-- ----------------------- ------------------------------------------- -->
						<!-- ----------------------- 现售商品子订单---------------------------- -->
							<#list sonOrderList as item> 
							<#if item.orderType == 1>
							<tr class="head_tb_tr" style="border:solid 0px;">
								<td colspan="5" style="padding-left: 5px;vertical-align: middle; height:30px">
									<span style="">
											${item.businessName!""}
		                            </span>					
								</td>
							</tr>
							<#list item.orderdetail as orderdetailItem>
							<tr style="border-bottom:1px #C2C2C2 dashed;">
								<td class="td_t3" style="padding-left: 5px">
									<div width="100%" height="100%" style="padding: 5px;position:relative">
										<table>
											<tr>
												<td>
													<a style="float:left;width:27%;margin-right:2%;margin-top:2%;margin-bottom:2%;text-align:center;" 
														href="${systemSetting().item}/product/${orderdetailItem.productID!""}.html" target="_blank"
														title="${item.productName!""}">
														<img src="${systemSetting().imageRootPath}${orderdetailItem.picture}"
															width="60px" height="70px" />
													</a>
												</td>
												<td>
													<div style="padding-left: 10px; text-align: left;">
														<a href="${systemSetting().item}/product/${orderdetailItem.productID!""}.html" target="_blank" title="${item.productName!""}">${orderdetailItem.productName}</a>
														<br/>
														<br/>
														<a>现货</a>
													</div>
												</td>
											</tr>
										</table>
									</div>
								</td>
								<td class="td_t3" style="text-align:left">
									<br>${orderdetailItem.specSize!""}<br/>
									<p class="p_style_2">${orderdetailItem.specColor!""}</p> 
								</td>
								<td class="td_t3" style="text-align:left">￥${orderdetailItem.price}</td>
								<td class="td_t3" style="text-align:left">${orderdetailItem.number}</td>
								<td class="td_t3" style="text-align:left;padding-left: 40px">￥${orderdetailItem.total0}</td>
							</tr>
							</#list>
							<tr height="50px">
								<td colspan="4" class="td_t3" style="vertical-align:middle;text-align: right;">
									快递方式：
									<select style="margin-left:13px;" class="ipt-text-sm" name="sel" id="shipping${item.orderCodeView}" onchange="changeShipping(${item.orderCodeView})">
										<#list item.businessShippingList as   businessShippingItem>
											<option value="${businessShippingItem.shippingId!""}">${businessShippingItem.shippingName!""}</option>
										</#list>
									</select>
									<lable>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运费：</lable>
								</td>
<!-- 								<td class="td_t6" style="width:10%"></td> -->
								<td style="padding-left: 40px">
									<label class="aft_sum">￥<span id="shipmoney${item.orderCodeView!""}">0.0</span></label>
								</td>
							</tr>
							<tr height="50px">
								<td colspan="4" class="td_t3" style="text-align: right;">商家优惠券： 
									<select id="couponSn${item.orderCodeView}" class="ipt-text-sm" name="sel" onchange="changeCouponSn(${item.orderCodeView})">
										<#list item.accountCouponList as accountCouponItem>
											<option value="${accountCouponItem.id!""},${accountCouponItem.couponValue!""},${accountCouponItem.couponType!""}">${accountCouponItem.couponName!""}</option>
										</#list>
									</select>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td class="td_t3">
									<#if item.accountCouponList[0]??> 
										<label class="aft_sum">￥<span id="money${item.orderCodeView!""}">${item.accountCouponList[0].couponValue!0}</span></label>
									</#if>
								</td>
							</tr>
							<tr height="50px">
								<td colspan="2" colspan="1" style="padding-left: 5px">
									<p>
										<a class="td_t3" style="margin-left:13px;">订单备注：</a> 
										<input id="otherRequirement${item.orderCodeView}" class="ipt-text" 
												type="text" name="otherRequirement" placeholder="请输入与商家达成一致的要求！"
												value="${item.otherRequirement!''}"
												onchange="changeOtherRequirement(${item.orderCodeView});"/>
									</p>
								</td>
								<td colspan="2" class="td_t6">${item.quantity}件商品&nbsp;&nbsp;合计：</td>
								<td class="td_t3" style="padding-left: 40px">
									<input id="ptotal${item.orderCodeView}" value="${item.ptotal!""}" type="hidden"/>
									<label class="aft_sum" style="color:red">￥<span name="heji" id="ptotalVal${item.orderCodeView}">${item.ptotal!0}</span></label>
								</td>
							</tr>
							<tr style="height:8px;border-bottom:solid 1px #C1C1C1;">
									
							</tr>
							<tr>
								<td colspan="5" height="5" style="background-color: white"></td>
							</tr>
							</#if>
							</#list>
						<!-- ----------------------- ------------------------------------------- -->
						</table>
					</div>
					<div width="100%" style="background: #F2F2F2; margin-bottom: 5px;">
						<table width="100%">
							<tr> 
								<td style="width:40%"></td> 
								<td style=""></td> 
								<td style="width:15%"></td> 
								<td class="td_t6" style="width:10%" colspan="1"></td> 
								<td class="td_t3" style="text-align:left;width:11%"></td>
							</tr> 
							<tr>
								<td colspan="4" class="td_t6">（当前用户积分剩余
									<label id="scoreLabel">${rank!0}</label>分）使用
								 	<input id="accountScore" type="text" style="width:50px"
										onchange="changeAccountScore();" />积分
								</td>
								<td class="td_t3" style="padding-left: 20px;">
									<label class="aft_sum" style="color:red;">￥ 
										<span id="accountScoreValue">0.0</span>
									</label>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="td_t6">平台抵扣券： 
									<select id="accountVoucher"  class="ipt-text-sm" name="sel" onchange="changeAccountVoucher();"> 
										<#list parentOrder.accountVoucherList as accountVoucherItem>
											<option value="${accountVoucherItem.id!""},${accountVoucherItem.couponValue!""}">${accountVoucherItem.couponName!""}</option>
										</#list>
									</select>
								</td>
								<td class="td_t3" style="padding-left: 20px;">
									<label class="aft_sum" style="color:red;">￥
									<span class="span_style_2"></span> 
									<#if parentOrder.accountVoucherList[0]?? > 
										<span class="span_style_2" id="voucherMoney">${parentOrder.accountVoucherList[0].couponValue!0}</span> 
									</#if>
									</label>
								</td>
							</tr>
							<tr ><td style="height:5px"></td></tr>
						</table>
					</div>
					<span class="span_style_2" style="padding-left: 5px; padding-top:15px;">下单后如果取消订单，所使用的积分和优惠券将不再恢复</span>
					<table class="table_style_3">
						<tr>
							<td style="width: 200px">应付金额:
								<span style="font-size:18px;color:red;font-weight:bold">
									￥
									<span style="font-size:18px;color:red;font-weight:bold" id="parentAmount">${parentOrder.amount!0}</span>
								</span>
							</td>
							<td class="td_t1" style="padding-right:20px;">
								<input type="button" class="btn-style-01" value="确认支付" onclick="saveOrder()" />
							</td>
						</tr>
						<tr>
							<td colspan="2" style="height:20px"></td>
						</tr>
					</table>
					<table id="tab" style="display :none">
						<tr style="background-color: #dff0d8">
							<th width="100">订单Id</th>
							<th width="100">快递方式Name</th>
							<th width="100">快递方式Code</th>
							<th width="100">运费</th>
							<th width="100">商家优惠券</th>
							<th width="100">商家优惠券Code</th>
							<th width="100">订单备注</th>
							<th width="100">合计</th>
						</tr>
						<#list sonOrderList as item>
							<tr>
								<td>${item.orderCode!""}</td> 
								<#if item.businessShippingList[0]??>
									<td id="tab_spName${item.orderCodeView}">${item.businessShippingList[0].shippingName!""}</td>
									<td id="tab_spId${item.orderCodeView}">${item.businessShippingList[0].shippingId!""}</td> 
								<#else>
									<td id="tab_spName${item.orderCodeView}"></td>
									<td id="tab_spId${item.orderCodeView}"></td> 
								</#if>
								<td id="tab_fee${item.orderCodeView}">${item.fee!"0"}</td> 
								<#if item.accountCouponList[0]??>
									<td id="tab_cpValue${item.orderCodeView}">${item.accountCouponList[0].couponValue!"0"}</td>
									<td id="tab_cpSn${item.orderCodeView}">${item.accountCouponList[0].couponSn!""}</td> 
								<#else>
									<td id="tab_cpValue${item.orderCodeView}"></td>
									<td id="tab_cpSn${item.orderCodeView}"></td> 
								</#if>
								<td id="tab_or${item.orderCodeView}">${item.otherRequirement!""}</td>
								<td id="tab_ptotal${item.orderCodeView}">${item.advancePrice!"0"}</td>
							</tr>
						</#list>
					</table>
			</form>
		</div>
	</div>
	<form id="formPlacPay" method="post" action="${systemSetting().pay}/order/plac_pay">
		<input type="hidden" name="placPayInput" />
	</form>
	</div>
	<!-- 商城主页footer页 -->
<@shopFooters.shopFooters/>
</body>
<script type="text/javascript">

	var shippingFlag = "Y";

	function toPlacPay() {
		//$("#formPlacPay :hidden[name=placPayInput]").val(productId);
		$("#formPlacPay").submit();
	}

	//序列話
	$.fn.serializeObject = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	function saveOrder() {
		var radios = document.getElementsByName("Fruit");
		//若未选中收货地址，则弹出提示
  		for ( var i = 0; i < radios.length; i++) {
  			if (radios[i].checked==false) {
    			alert("请选择收货地址！");
    			return;
   			}
  		}
		
//   		var a=document.getElementById("adds").value;
		
//        	if(a==0){
//        		alert("请填写您的收货地址！");
//        		return;
//        	}
       	
       	if(shippingFlag == "noMatch") {
       		alert("收货地址不在商家配送范围内！请重新选择配送方式！");
       		return;
       	}
       	
		var objList = new Array();
		var rows = tab.rows;
		for (var i = 1; i < rows.length; i++) {
			var obj = new Object();
			obj["orderCode"] = rows[i].cells[0].innerHTML;
			obj["expressName"] = rows[i].cells[1].innerHTML;
			obj["expressCode"] = rows[i].cells[2].innerHTML;
			obj["fee"] = rows[i].cells[3].innerHTML;
			obj["couponValue"] = rows[i].cells[4].innerHTML;
			obj["couponsId"] = rows[i].cells[5].innerHTML;
			obj["otherRequirement"] = rows[i].cells[6].innerHTML;
			obj["ptotal"] = rows[i].cells[7].innerHTML;
			objList.push(obj);
		}
		var parentId = $("#orderCode").val();
		var accountVoucher = $('#accountVoucher option:selected').val();
		var parentAmount = $("#parentAmount").html();
		var addressId = "";
		var temp = document.getElementsByName("Fruit");
		for (var i = 0; i < temp.length; i++) {
			if (temp[i].checked)
				addressId = temp[i].value;
		}
		var point = document.getElementById("accountScoreValue").innerHTML
		var usedScore = document.getElementById("accountScore").value;
		$.ajax({
			url : "${systemSetting().pay}/order/pay",
			type : 'POST',
			//		dataType:'JSONp',
			data : {
				intoType : "placPay",
				orderCust : JSON.stringify(objList),
				parentId : parentId,
				accountVoucher : accountVoucher,
				parentAmount : parentAmount,
				addressId : addressId,
				point : point,
				usedScore : usedScore
			},
			success : function(data) {
				toPlacPay();
			},
			error : function(error) {
				console.log(error);
			}
		})
	}

	function valid() {
		obj = document.getElementsByName("addressxs");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "visible";//显示;
			obj[i].style.display = "";//显示
		}
		obj = document.getElementsByName("addressyc");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "visible";//显示;
			obj[i].style.display = "";//显示
		}
	}
	
	function none() {
		obj = document.getElementsByName("addressxs");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "addressyc";//显示;
			obj[i].style.display = "";//显示
		}
		obj = document.getElementsByName("addressyc");
		for (i = 0; i < obj.length; i++) {
			obj[i].style.visibility = "";//显示;
			obj[i].style.display = "none";//显示
		}
	}

	function changeCouponSn(id) {
		var couponSnId = "couponSn" + id;
		var moneyId = "money" + id;
		var ptotalId = "ptotal" + id;
		var ptotalValId = "ptotalVal" + id;
		var shipmoneyId = "shipmoney" + id;
		couponValue = document.getElementById(couponSnId).value;
		var strArr = new Array(); //定义一数组
		strArr = couponValue.split(",");
		//alert(couponValue + " : " + strArr[0] + " : " + strArr[1] + " : " + strArr[2]);
		if(parseInt(strArr[2]) == 1) {	//折价券
			couponValue = strArr[1];
		} else if (parseInt(strArr[2]) == 2) {		//折扣券
			var ccc = document.getElementsByName("heji");
			var ddd = 0;
			for (i = 0; i < ccc.length; i++) {
				ccc[i].innerHTML;//显示;
				ddd = ddd + parseFloat(ccc[i].innerHTML)
			}
			//alert(ddd);
			couponValue = (parseFloat(strArr[2])/100) * parseFloat(ddd);
		} else {
			couponValue = parseFloat(strArr[1]);
		}
		
		ptotalValue = document.getElementById(ptotalId).value;
		shipmoney = document.getElementById(shipmoneyId).innerHTML;
		document.getElementById(moneyId).innerHTML = couponValue;

		document.getElementById(ptotalValId).innerHTML = (ptotalValue
				- couponValue + parseFloat(shipmoney));
		if (ptotalValue - couponValue + parseFloat(shipmoney) < 0) {
			document.getElementById(ptotalValId).innerHTML = 0;
		}
		//重新计算合计金额
		heji();
		
		var coupon = document.getElementById(couponSnId).value;
		var table_cpValue = "tab_cpValue" + id;
		var table_cpSn = "tab_cpSn" + id;
		document.getElementById(table_cpValue).innerHTML = coupon.substr(couponValue.indexOf(",") + 0);
		document.getElementById(table_cpSn).innerHTML = coupon.substr(couponValue.indexOf(",") + 1);
	}

	function changeShipping(id) {
		var shippingId = "shipping" + id;
		var selectedIndex = document.getElementById(shippingId).selectedIndex;
		//alert(document.getElementById(shippingId));
		var shipmoney = 0;
		var shipmoneyId = "shipmoney" + id;
		var moneyId = "money" + id;
		var ptotalId = "ptotal" + id;
		var ptotalValId = "ptotalVal" + id;
		shippingId = document.getElementById(shippingId).value;
		var temp = document.getElementsByName("Fruit");
		var addressId = "";
		for (var i = 0; i < temp.length; i++) {
			if (temp[i].checked)
				addressId = temp[i].value;
		}
		if (addressId == "") {
			alert("请选择配送地址!");
			document.getElementById(shippingId).selectedIndex = 0;
			return;
		}
		if (selectedIndex > 0) {
			$.ajax({
						url : "${systemSetting().orders}/order/getShiping",
						type : 'POST',
						dataType : 'JSON',
						data : {
							id : id,
							shippingId : shippingId,
							addressId : addressId
						},
						success : function(data) {
							//alert("success" + data);
							if(data == "noMatch") {
								//alert("noMatch");
								document.getElementById(shipmoneyId).innerHTML = "0";
								shippingFlag = "noMatch";
							} else {
								shippingFlag = "Y";
								shipmoney = data;
								//alert(shipmoneyId);
								shipmoney = new Number(shipmoney);
								document.getElementById(shipmoneyId).innerHTML = shipmoney.toFixed(2);
								var money = document.getElementById(moneyId).innerHTML;
								ptotalValue = document.getElementById(ptotalId).value;
								var hejijine = parseFloat(ptotalValue)
										+ parseFloat(shipmoney) - parseFloat(money);
								if (hejijine < 0) {
									document.getElementById(ptotalValId).innerHTML = 0;
								} else {
									document.getElementById(ptotalValId).innerHTML = hejijine.toFixed(2);
								}
								heji();
								
								var tab_fee = "tab_fee" + id;
								document.getElementById(tab_fee).innerHTML = shipmoney.toFixed(2);
								var tab_ptotal = "tab_ptotal" + id;
								document.getElementById(tab_ptotal).innerHTML = document.getElementById(ptotalValId).innerHTML;
								var table_spName = "tab_spName" + id;
								var tab_spId = "tab_spId" + id;
								document.getElementById(tab_spId).innerHTML = shippingId;
							}
						},
						error : function(error) {
							alert("error:" + error);
						}
					})
		} else {
			document.getElementById(shipmoneyId).innerHTML = shipmoney.toFixed(2);
			var money = document.getElementById(moneyId).innerHTML;
			ptotalValue = document.getElementById(ptotalId).value;
			var hejijine = parseFloat(ptotalValue) + parseFloat(shipmoney)
					- parseFloat(money);
			if (hejijine < 0) {
				document.getElementById(ptotalValId).innerHTML = 0;
			} else {
				document.getElementById(ptotalValId).innerHTML = hejijine.toFixed(2);
			}
			heji();
			
			var tab_fee = "tab_fee" + id;
			document.getElementById(tab_fee).innerHTML = shipmoney.toFixed(2);
			var tab_ptotal = "tab_ptotal" + id;
			document.getElementById(tab_ptotal).innerHTML = document.getElementById(ptotalValId).innerHTML;
			var table_spName = "tab_spName" + id;
			var tab_spId = "tab_spId" + id;
			document.getElementById(tab_spId).innerHTML = shippingId;
		}
	}
	
	//订单备注输入change函数
	function changeOtherRequirement(id) {
		var otherRequirementId = "otherRequirement" + id;
		var otherRequirement = document.getElementById(otherRequirementId).value;
		var table_or = "tab_or" + id;
		document.getElementById(table_or).innerHTML = otherRequirement;
	}

	//积分输入change函数
	function changeAccountScore() {
		var accountScore = document.getElementById("accountScore").value;
		var score = document.getElementById("scoreLabel").innerHTML;
 		if(parseInt(accountScore) > parseInt(score)) {
 			alert("使用积分已超过您拥有积分");
 			accountScore = 0;
 		}
 		if(accountScore == null || accountScore == "") {
			accountScore = 0;
		}
		var accountScoreValue = parseFloat(accountScore) / 100;
		document.getElementById("accountScoreValue").innerHTML = accountScoreValue.toFixed(2);
		heji();
	}

	//平台抵扣券change函数
	function changeAccountVoucher() {
		var accountVoucherValue = document.getElementById("accountVoucher").value;
		var strs= new Array();
		strs = accountVoucherValue.split(",");
		accountVoucherValue = strs[1];
		document.getElementById("voucherMoney").innerHTML = parseFloat(accountVoucherValue).toFixed(2);
		heji();
	}

	function heji() {
		var ccc = document.getElementsByName("heji");
		var voucherMoney = document.getElementById("voucherMoney").innerHTML;
		var accountScoreValue = document.getElementById("accountScoreValue").innerHTML;
		var money = 0;
		var ddd = 0;
		for (i = 0; i < ccc.length; i++) {
			ccc[i].innerHTML;//显示;
			ddd = ddd + parseFloat(ccc[i].innerHTML)
		}
		money = money + ddd - accountScoreValue - voucherMoney;
		if(parseInt(money) < 0) {
			document.getElementById("parentAmount").innerHTML = 0;
		} else {
			document.getElementById("parentAmount").innerHTML = money.toFixed(2);
		}
	}
</script>