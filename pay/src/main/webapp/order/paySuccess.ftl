<#import "/ftl/htmlBase.ftl" as htmlBase>
<#import "/account/myLeft.ftl" as myLeft>
<#import "/ftl/top3.ftl" as myTop>
<#import "/ftl/footer.ftl" as shopFooter>
<!DOCTYPE html>
<html>
<head>
	<@htmlBase.htmlBase>
	<title>支付成功</title>
	<style type="text/css">
		.glyphicon {
		    position: relative;
		    top: 1px;
		    display: inline-block;
		    font-family: "Glyphicons Halflings";
		    font-style: normal;
		    font-weight: normal;
		    line-height: 1;
		}
		.glyphicon-ok:before {
			content:"\e084"
		}
		.text-success {
			font-size: 16px;
			font-weight: bold;
		}
		.div_t4{
			margin:0 auto;
			width:1250px;
		}
		.aft_sum{
				color:#FF8000; 
				font-weight:bold; 
				font-size:16px; 
				margin-right:40px;
			}
	</style>
</head>
<body>
<!--商城主页top页-->
<@myTop.shopTop/>
<div class="div_t4">
	<div style="">
		<a href="${systemSetting().www}">
			<img src="${systemSetting().staticSource}/shopindex/common/img/logo.gif" border="0" />
		</a>
	</div>
	<div class="container" width="100%" style="border:1px solid #F2F2F2">
		<table width="100%">
			<tr>
				<td>
					<div style="width: 98%;padding-left: 30px;background-color: #E8F2FE;padding-bottom: 10px;padding-top:10px">
						<span class="text-success">您已支付成功</span>
					</div>
				</td>
			</tr>
			
				<td>
					<div class="panel-body" style="font-size: 16px;font-weight: normal;padding-left: 30px;padding-top:10px">
						订单号：<label>&nbsp;
							<span>${Session.orderCode!""}</span>
						</label>
					</div>
				</td>
			</tr>
				<tr>
				<td>
					<div class="panel-body" style="font-size: 16px;font-weight: normal;padding-left: 30px;padding-top:10px">
						订单金额：<label>&nbsp;
						<span>
							${payAmount!""}
						</span>
						</label>
					</div>
				</td>
				</tr>
                    <tr>
					<td>
						<div class="panel-body" style="font-size: 16px;font-weight: normal;padding-left: 30px;padding-top:10px">
							收货地址：<label>&nbsp;
							<span><#if orderShip??>${orderShip.shipaddress!""}</#if></span>
							</label>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="panel-body" style="font-size: 16px;font-weight: normal;padding-left: 30px;padding-top:10px">
							收货人：<label>&nbsp;
							<span><#if orderShip??>${orderShip.shipname!""}</#if></span>
							</label>
						</div>
					</td>
				</tr>
			<tr>
				<td>
						<div class="panel-body" style="font-size: 16px;font-weight: normal;padding-left: 30px;padding-bottom: 10px;padding-top:10px">
							实付款：<label class="aft_sum" style="color:red">&nbsp;￥
								<span>
								<#if Session.payWay=="baoWay">
								     ${Session.payBaoAmount!""}
								<#elseif Session.payWay=="amtWay">
									${Session.payAccountAmount!""}
								<#elseif Session.payWay=="allWay">
									${Session.payBaoAmount?number+Session.payAccountAmount?number}
								</#if>
								</span>
							</label>
						</div>
					</td>
				</tr>
		</table>
	</div>
</div>
<div style="padding-top:200px;" >
<!-- 商城主页footer页 -->
<@shopFooter.shopFooter/>
<script src="${systemSetting().staticSource}/static/frontend/v1/js/index.js"></script>
<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/hover.js"></script>
</div>
</@htmlBase.htmlBase>
</body>
</html>