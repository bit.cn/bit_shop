<#macro shopFooter title="" checkLogin=true>
		<div class="footer">
			<div class="foot-top">
				<!--帮助中心-->
				<ul class="clearfix">
				<#list newsCatalogs as item>
						<li>
							<h3>${item.name!""}</h3>
							<#if item.news??>
								<#list item.news as item>
									<a href="${systemSetting().doc}/help/${item.id}.html" target="_blank">${item.title}</a>
								</#list>
							</#if>
						</li>
				</#list>
				</ul>
				<a href="${systemSetting().www}" class="logo"><img src="${systemSetting().staticSource}/shopindex/common/img/logo(beifen).gif" /></a>
			</div>
			<div class="foot-bottom">@2014 阿尼托 上海索罗游信息技术有限公司 anitoys.com 版权所有 沪ICP备11047967号-18</div>
		</div>
	<!-- cnzz站点统计 -->
	    <p style="display:none;">${systemSetting().statisticsCode!""}</p>
		<script src="${systemSetting().staticSource}/static/frontend/v1/js/common/z_stat.php" type="text/javascript"></script>
		<script src="${systemSetting().staticSource}/static/frontend/v1/js/common/core.php" charset="utf-8" type="text/javascript"></script>
		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/bootstrap.min.js"></script>
		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery.blockUI.js"></script>
		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery.validator.js"></script>
		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/zh_CN.js"></script>
		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${systemSetting().staticSource}/static/frontend/v1/js/common/lanrenzhijia.js"></script>
</#macro>