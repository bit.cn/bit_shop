<%@page import="com.soloyogame.anitoys.pay.web.util.pay.alipay.alipayescow.config.AlipayConfig" %>
<%@page import="org.slf4j.LoggerFactory" %>
<%@page import="org.slf4j.Logger" %>
<%@page import="com.soloyogame.anitoys.db.commond.PayInfo" %>
<%@page import="com.soloyogame.anitoys.pay.web.util.pay.*" %>
<%@page import="com.alibaba.fastjson.*" %>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@page import="org.springframework.web.context.WebApplicationContext" %>
<%@page import="com.soloyogame.anitoys.service.SystemSettingService" %>
<%@page import="com.soloyogame.anitoys.db.commond.SystemSetting" %>

<%
    /**
     *功能：即时到账交易接口接入页
     *版本：3.3
     *日期：2012-08-14
     *说明：
     *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
     *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。

     *************************注意*****************
     *如果您在接口集成过程中遇到问题，可以按照下面的途径来解决
     *1、商户服务中心（https://b.alipay.com/support/helperApply.htm?action=consultationApply），提交申请集成协助，我们会有专业的技术工程师主动联系您协助解决
     *2、商户帮助中心（http://help.alipay.com/support/232511-16307/0-16307.htm?sh=Y&info_type=9）
     *3、支付宝论坛（http://club.alipay.com/read-htm-tid-8681712.html）
     *如果不想使用扩展功能请把扩展功能参数赋空值。
     **********************************************
     */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>支付宝即时到账交易接口</title>
</head>
<%
	WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
    SystemSettingService systemSettingService = (SystemSettingService) app.getBean("systemSettingService");
    SystemSetting systemSetting = systemSettingService.selectOne(new SystemSetting());
    String url = systemSetting.getPay();
    //String url = "pay.rc.anitoys.com";
    //String url ="192.168.3.31:8080/pay";
    ////////////////////////////////////请求参数//////////////////////////////////////
    PayInfo payInfo = JSON.parseObject((String) session.getAttribute("payInfo"), PayInfo.class);
    if(payInfo==null){
    	payInfo= (PayInfo)session.getAttribute("payInfo");
    }

	String payType = payInfo.getPayType();  //支付方式（1:定金支付2：补款支付）现货订单此值为null
    session.setAttribute("payType", payType);
    
    //支付类型
    String payment_type = "1";
    //必填，不能修改
    //服务器异步通知页面路径
    String notify_url = url + "/alipayapi_notify_url.jsp";

    //页面跳转同步通知页面路径
    String return_url = url + "/alipayapi_return_url.jsp";

    //平台支付宝帐户（钱统一到平台）
    String seller_email = AlipayConfig.seller_email;//payInfo.getWIDseller_email();

    //商户订单号
    String out_trade_no = payInfo.getWIDout_trade_no();//new String(request.getParameter("WIDout_trade_no").getBytes("ISO-8859-1"),"UTF-8");
    //商户网站订单系统中唯一订单号，必填

    //订单名称
    String subject = "anitoys" + payInfo.getWIDout_trade_no();//payInfo.getWIDsubject();//new String(request.getParameter("WIDsubject").getBytes("ISO-8859-1"),"UTF-8");
    //必填

    //付款金额
    String total_fee = String.valueOf(payInfo.getWIDprice());//new String(request.getParameter("WIDtotal_fee").getBytes("ISO-8859-1"),"UTF-8");
    //必填

    //订单描述

    String body = "";//new String(payInfo.getWIDbody().getBytes("ISO-8859-1"),"UTF-8");//new String(request.getParameter("WIDbody").getBytes("ISO-8859-1"),"UTF-8");
    //商品展示地址 （默认跳转至我的订单中心页面）
    String show_url = url + "/account/orders";//new String((request.getContextPath()+"/account/orders").getBytes("ISO-8859-1"),"UTF-8");
    //需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html

    //防钓鱼时间戳
    String anti_phishing_key = "";
    //若要使用请调用类文件submit中的query_timestamp函数

    //客户端的IP地址
	String exter_invoke_ip = "";
	//非局域网的外网IP地址，如：221.0.0.1

    //////////////////////////////////////////////////////////////////////////////////

    //把请求参数打包成数组
    Map<String, String> sParaTemp = new HashMap<String, String>();
    sParaTemp.put("service", "create_direct_pay_by_user");
    sParaTemp.put("partner", AlipayConfig.partner);
    sParaTemp.put("seller_email", seller_email);
    sParaTemp.put("_input_charset", AlipayConfig.input_charset);
    sParaTemp.put("payment_type", payment_type);
    sParaTemp.put("notify_url", notify_url);
    sParaTemp.put("return_url", return_url);
    sParaTemp.put("out_trade_no", out_trade_no);
    sParaTemp.put("subject", subject);
    sParaTemp.put("total_fee", total_fee);
    sParaTemp.put("body", body);
    sParaTemp.put("show_url", show_url);
    sParaTemp.put("anti_phishing_key", anti_phishing_key);
    sParaTemp.put("exter_invoke_ip", exter_invoke_ip);

    //建立请求
    String sHtmlText = AlipaySubmit.buildRequest(sParaTemp, "get", "确认");
    out.println(sHtmlText);
%>
<body>
</body>
</html>
